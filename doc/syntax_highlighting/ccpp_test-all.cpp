/*
    komentarz
 */

#include "plik.h"
#include <plik_a.h>
#include <nowy.h>  // koment

// test tekstu zwyklego
text zwykly
text zwykly_2
98aaa981
zzzzzzz

// test komentarzy jednoliniowych
// komentarz
// test ciagow
"o"tekst  "a"	"1"2"3""\4"5
"ciag" tekst
"ciag \t tekstowy"
     "abc\n\tde\by\r"
"a\t\t\t\t\t\tbcd\bu\r\r"
"ab\nd\f\tgh\b"
"ala\nola\c"
"ciag_1","ciag_2"
"aaaaaaa
"

// test znakow
'1''2''3', '5'	'4' '6' '8'	'a','6','8'
'a''xad' 't'
'z'tekst
'a' '\b' '\c'	'\n''\y''t'
'asd'tekst
'\q'
'aasd
'a
'\tt

// test wyrazenia zlozonego
for(int i=0; i<5; i++) ;

// testowa klasa
class Q_EXPORT Klasa public QWidget
{
    Q_OBJECT
    Q_PROPERTY( QString text READ text WRITE setText )

public:
    klasa()
    {
	QTimer *timer = new QTimer( this );
	connect( mTimer, SIGNAL(timeout()), this, SLOT(slotTimerDone()) );
    }
    ~klasa()
    {
	disconnect( timer, SIGNAL(timeout()), this, SLOT(slotTimerDone()) );
    }

protected:
int
b;
    uint funkcja( const char *buf, int u, bool b=true, bool c=TRUE)
    {
	for (int i=0; i<10; i++) ;
	int s = b ?13:15;
	int a=5;
	char c0='a', c1 = '\n',

	switch( u ) {
	    case 1: ; break;

	    default:
	    break;
	}
	
	(void) new Object()
	char c2 = (int)c0;
	int a5 = (char)a;
	int bl = 0L;

	while( TRUE ) ;

	debug("as\nd\t\bfg\qjk");

	QString str, str1;
	str->setText( tr("QString") );
	str->setText( i18n("QString") );

	return 1;
    }

    void fun( bool d=true) { return; }

private slots:
    void slotTimerDone()
    {
	debug("timer done");
    }

signals:
    void sygnal();

};

// test slow kluczowych
Q_OBJECTS for
slot SLOT signal SIGNAL SLOTs
signalss  while
voida void*
privates publics
truefalse

// test typow
void fun()
bool b=TRUE;  BOOL C= TRUE ;  bool a=true;  bool u= true
char c = 'a'; int as=3;  uint fd = 12;  long aq= 421234;   float pi =3.14;
LONG INT CHAR

// test liczb
1 2 3   4.6 1234 0232  0L 0K

3.3\3   0.5 04 15       1.5,05,25
1.43    \12 /10
432 012 05b 023cd 023cd01 1.8d

98aa9814 \41    c1.4 \12        e3.14 \23
98aa901 \41     98aa901 \01     aa901 \01

a541 b1 b0123 c14.56 \12        ab12 98aa981 1_2
98aa981 \41     d31_23
3c ab012 32cd fg19      c3.2\12
98aaa981
