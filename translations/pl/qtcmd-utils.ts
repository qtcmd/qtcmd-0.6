<!DOCTYPE TS><TS>
<context>
    <name>Filters</name>
    <message>
        <source>files and directories</source>
        <translation>pliki i katalogi</translation>
    </message>
    <message>
        <source>directories only</source>
        <translation>tylko katalogi</translation>
    </message>
    <message>
        <source>files only</source>
        <translation>tylko pliki</translation>
    </message>
    <message>
        <source>links</source>
        <translation>linki</translation>
    </message>
    <message>
        <source>hanging links</source>
        <translation>wiszące linki</translation>
    </message>
</context>
</TS>
