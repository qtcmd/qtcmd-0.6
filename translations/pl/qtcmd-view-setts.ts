<!DOCTYPE TS><TS>
<context>
    <name>FilesAssociationPage</name>
    <message>
        <source>FilesAssociation</source>
        <translation>FilesAssociation</translation>
    </message>
    <message>
        <source>Known kind of files</source>
        <translation>Znane rodzaje plików</translation>
    </message>
    <message>
        <source>Internal viewer</source>
        <translation>Podgląd wewnętrzny</translation>
    </message>
    <message>
        <source>External viewer</source>
        <translation>Podgląd zewnętrzny</translation>
    </message>
    <message>
        <source>Description :</source>
        <translation>Opis :</translation>
    </message>
    <message>
        <source>File templates :</source>
        <translation>Wzorce plików :</translation>
    </message>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>Application :</source>
        <translation>Aplikacja :</translation>
    </message>
    <message>
        <source>&amp;Add a new kind</source>
        <translation>&amp;Dodaj nowy rodzaj</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>text</source>
        <translation>tekst</translation>
    </message>
    <message>
        <source>documents</source>
        <translation>dokument</translation>
    </message>
    <message>
        <source>image</source>
        <translation>obrazek</translation>
    </message>
    <message>
        <source>sound</source>
        <translation>dźwięki</translation>
    </message>
    <message>
        <source>video</source>
        <translation>wideo</translation>
    </message>
    <message>
        <source>files</source>
        <translation>pliki</translation>
    </message>
    <message>
        <source>sources</source>
        <translation>źródła</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Wszystkie pliki</translation>
    </message>
    <message>
        <source>Select viewer</source>
        <translation>Wybrana przeglądarka</translation>
    </message>
    <message>
        <source>This file is&apos;nt executable !</source>
        <translation>Ten plik nie jest uruchamialny !</translation>
    </message>
    <message>
        <source>Add a new kind of file</source>
        <translation>Dodaj nowy rodzaj pliku</translation>
    </message>
    <message>
        <source>Please to give a name:</source>
        <translation>Proszę podać nazwę:</translation>
    </message>
    <message>
        <source>new kind</source>
        <translation>nowy rodzaj</translation>
    </message>
    <message>
        <source>C or C++ source file</source>
        <translation>Plik źródłowy C lub C++</translation>
    </message>
    <message>
        <source>C or C++ header file</source>
        <translation>Nagłówek pliku źródłowego C lub C++</translation>
    </message>
    <message>
        <source>Script in Perl</source>
        <translation>Skryp w Perlu</translation>
    </message>
    <message>
        <source>Shell script</source>
        <translation>Skrypt shelowy</translation>
    </message>
    <message>
        <source>Python script </source>
        <translation>Skrypt w Python-ie</translation>
    </message>
    <message>
        <source>Java source file</source>
        <translation>Plik źródłowy Java</translation>
    </message>
    <message>
        <source>PHP script</source>
        <translation>Skrypt PHP</translation>
    </message>
    <message>
        <source>Cascade Style Sheet file</source>
        <translation>Plik zawierający kaskadowy arkusz styli</translation>
    </message>
    <message>
        <source>XML script</source>
        <translation>Skrypt w języku XML</translation>
    </message>
    <message>
        <source>SQL script</source>
        <translation>Skrypt w języku SQL</translation>
    </message>
    <message>
        <source>Assembler source file</source>
        <translation>Plik źródłowy w jęzuku Assembler</translation>
    </message>
    <message>
        <source>WML script</source>
        <translation>Skrypt w języku WML</translation>
    </message>
    <message>
        <source>AWK script</source>
        <translation>Skrypt w języku AWK</translation>
    </message>
    <message>
        <source>Pascal source file</source>
        <translation>Plik źródłowy w języku Pascal</translation>
    </message>
    <message>
        <source>Qt source MOC file</source>
        <translation>Plik źródłowy meta pliku MOC</translation>
    </message>
    <message>
        <source>TCL script</source>
        <translation>Skrypt w języku TCL</translation>
    </message>
    <message>
        <source>TK script</source>
        <translation>Skrypt w języku TK</translation>
    </message>
    <message>
        <source>ADA source file</source>
        <translation>Plik źródłowy w języku ADA</translation>
    </message>
    <message>
        <source>SGML script</source>
        <translation>Skrypt w SGML</translation>
    </message>
    <message>
        <source>RUBY script</source>
        <translation>Skrypt w języku RUBY</translation>
    </message>
    <message>
        <source>Cascade Style Sheet</source>
        <translation>Kaskadowy arkusz styli</translation>
    </message>
    <message>
        <source>Differents between two files</source>
        <translation>Plik zawieający różnice pomiędzy dwoma plikami</translation>
    </message>
    <message>
        <source>Source translations for Qt&apos;s program</source>
        <translation>Plik źródłowy tłumaczeń programów opartych na bibliotekach Qt</translation>
    </message>
    <message>
        <source>HTML document</source>
        <translation>Dokument HTML</translation>
    </message>
    <message>
        <source>RTF document</source>
        <translation>Dokument w formacie RTF</translation>
    </message>
    <message>
        <source>LaTeX document</source>
        <translation>Dokument w formacie LaTeX</translation>
    </message>
    <message>
        <source>TeX document</source>
        <translation>Dokument w formacie TeX</translation>
    </message>
    <message>
        <source>DocBook document</source>
        <translation>Dokument DocBook</translation>
    </message>
    <message>
        <source>DVI TeX file</source>
        <translation>Plik w formacie TeX DVI</translation>
    </message>
    <message>
        <source>Source of User Interface for Qt&apos;s programm</source>
        <translation>Plik źródłowy interfejsu probramu opartego na bibliotekach Qt</translation>
    </message>
    <message>
        <source>UML Umbrello modeler file</source>
        <translation>Plik modelera UML Umbrello</translation>
    </message>
    <message>
        <source>Manual textbook document</source>
        <translation>Dokument w formacie manuala</translation>
    </message>
    <message>
        <source>PDF document</source>
        <translation>Dokument w formacie PDF</translation>
    </message>
    <message>
        <source>PostScript document</source>
        <translation>Dokument w formacie PostScript</translation>
    </message>
    <message>
        <source>Microsoft Word document</source>
        <translation>Dokument programu Microsoft Word</translation>
    </message>
    <message>
        <source>OpenOffice.org text document</source>
        <translation>Dokument tekstowy programu OpenOffice.org</translation>
    </message>
    <message>
        <source>KWord text document</source>
        <translation>Dokument tekstowy pakietu KOffice</translation>
    </message>
    <message>
        <source>Microsoft Excel document</source>
        <translation>Dokument programu Microsoft Excel</translation>
    </message>
    <message>
        <source>OpenOffice.org sheet</source>
        <translation>Dokument arkusza kalkulacyjnego programu OpenOffice.org</translation>
    </message>
    <message>
        <source>KWord sheet</source>
        <translation>Dokument arkusza styli pakietu KOffice</translation>
    </message>
    <message>
        <source>Binary translations for Qt&apos;s program</source>
        <translation>Tłumaczenia w formacie binarnym dla programów opartych o bibliotekę Qt</translation>
    </message>
    <message>
        <source>BMP image</source>
        <translation>Plik w formacie BMP</translation>
    </message>
    <message>
        <source>GIF image</source>
        <translation>Plik w formacie grafiki GIF</translation>
    </message>
    <message>
        <source>JPEG image</source>
        <translation>Plik w formacie grafiki JPEG</translation>
    </message>
    <message>
        <source>PNG image</source>
        <translation>Plik w formacie grafiki PNG</translation>
    </message>
    <message>
        <source>JPEG 2000 image</source>
        <translation>Plik w formacie grafiki JPEG 2000</translation>
    </message>
    <message>
        <source>X PixMap image</source>
        <translation>Plik w formacie grafiki X PixMap</translation>
    </message>
    <message>
        <source>Scalable Vector Graphics</source>
        <translation>Plik grafiki wektorower typu SVG</translation>
    </message>
    <message>
        <source>PCX image</source>
        <translation>Plik w formacie grafiki PCX</translation>
    </message>
    <message>
        <source>D&apos;Javu image</source>
        <translation>Plik w formacie grafiki D&apos;Javu</translation>
    </message>
    <message>
        <source>TIFF image</source>
        <translation>Plik w formacie grafiki TIFF</translation>
    </message>
    <message>
        <source>Truevision Targa image</source>
        <translation>Plik w formacie grafiki Targa</translation>
    </message>
    <message>
        <source>X BitMap image</source>
        <translation>Plik w formacie grafiki X BitMap</translation>
    </message>
    <message>
        <source>Computer Graphics Metafile</source>
        <translation>Plik w formacie CGM</translation>
    </message>
    <message>
        <source>EPS image</source>
        <translation>Plik w formacie grafiki EPS</translation>
    </message>
    <message>
        <source>Microsoft Windows icons</source>
        <translation>Plik Microsoft Windows zawięrający ikonę</translation>
    </message>
    <message>
        <source>Portable BitMap image</source>
        <translation>Plik w formacie grafiki Portable BitMap</translation>
    </message>
    <message>
        <source>Portable GrayMap image</source>
        <translation>Plik w formacie grafiki Portable BitMap</translation>
    </message>
    <message>
        <source>Portable PixMap image</source>
        <translation>Plik w formacie grafiki Portable PixMap</translation>
    </message>
    <message>
        <source>Microsoft Windows Metafile</source>
        <translation>Plik Microsoft Windows zawierający meta informacje</translation>
    </message>
    <message>
        <source>Shockwave Flash Media</source>
        <translation>Plugin Shockwave Flash</translation>
    </message>
    <message>
        <source>Netscape Shockwave Flash</source>
        <translation>Plugin Shockwave Flash dla programu Netscape</translation>
    </message>
    <message>
        <source>MNG image</source>
        <translation>Plik w formacie grafiki MNG</translation>
    </message>
    <message>
        <source>WAV file</source>
        <translation>Plik dźwiękowy formatu WAV</translation>
    </message>
    <message>
        <source>MPEG Layer3 sound</source>
        <translation>Plik dźwiękowy formatu MPEG Layer3</translation>
    </message>
    <message>
        <source>OGG file</source>
        <translation>Plik dźwiękowy formatu OGG</translation>
    </message>
    <message>
        <source>Audio track of Compact Disk</source>
        <translation>Ścieżka dźwiękowa płyty kompaktowej</translation>
    </message>
    <message>
        <source>MIDI file</source>
        <translation>Plik dźwiękowy formatu MDI</translation>
    </message>
    <message>
        <source>Amiga sound file</source>
        <translation>Plik dźwiękowy formatu Amigi</translation>
    </message>
    <message>
        <source>ULAW (Sun) audio file</source>
        <translation>Plik dźwiękowy formatu ULAW (Sun)</translation>
    </message>
    <message>
        <source>Microsoft Media Format</source>
        <translation>Plik dźwiękowy w formacie Microsoft Media</translation>
    </message>
    <message>
        <source>Microsoft AVI Video</source>
        <translation>Plik wideo w formacie Microsoft AVI</translation>
    </message>
    <message>
        <source>DIVX video format</source>
        <translation>Plik w formacie wideo DIVX</translation>
    </message>
    <message>
        <source>MPEG video format</source>
        <translation>Plik wideo w formacie MPEG</translation>
    </message>
    <message>
        <source>Quicktime video format</source>
        <translation>Plik wideo w formacie Quicktime</translation>
    </message>
    <message>
        <source>IFF (Animation) video format</source>
        <translation>Plik wideo w formacie IFF (animacja)</translation>
    </message>
    <message>
        <source>Autodesk FLIC file</source>
        <translation>Plik formatu Autodesc FLIC</translation>
    </message>
</context>
<context>
    <name>FontViewPage</name>
    <message>
        <source>FontViewPage</source>
        <translation>FontViewPage</translation>
    </message>
    <message>
        <source>Test font</source>
        <translation>Font testowy</translation>
    </message>
    <message>
        <source>&amp;Italic</source>
        <translation>Po&amp;chylony</translation>
    </message>
    <message>
        <source>Alt+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <source>&amp;Bold</source>
        <translation>Po&amp;grubiony</translation>
    </message>
    <message>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <source>Size :</source>
        <translation>Rozmiar :</translation>
    </message>
    <message>
        <source>Set &amp;default font</source>
        <translation>Ustaw &amp;domyślny font</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Helvetica, 10</source>
        <translation>Helvetica, 10</translation>
    </message>
    <message>
        <source>Set &amp;original font</source>
        <translation>Ustaw &amp;pierwotny font</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
</context>
<context>
    <name>KeyShortcutsViewPage</name>
    <message>
        <source>KeyShortcutsView</source>
        <translation>KeyShortcutsView</translation>
    </message>
    <message>
        <source>Action</source>
        <translation>Akcja</translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation>Skrót klawiszowy</translation>
    </message>
    <message>
        <source>Own</source>
        <translation>Własny</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Domyślny</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Brak</translation>
    </message>
    <message>
        <source>Default shortcut</source>
        <translation>Domyślny skrót klawiszowy</translation>
    </message>
    <message>
        <source>&amp;Defaults settings</source>
        <translation>Ustawienia &amp;domyślne</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>DefaultColors</source>
        <translation>DomyślneKolory</translation>
    </message>
    <message>
        <source>&amp;Edit current shortcut</source>
        <translation>&amp;Edytuj bieżący skrót klawiszowy</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
    <message>
        <source>The &apos;%1&apos; key combination has already been allocated to the action</source>
        <translation>Skrót klawiszowy &apos;%1&apos; jest już powiązany z działaniem</translation>
    </message>
    <message>
        <source>Do you want to reassign it from that action to the current one ?</source>
        <translation>Czy chcesz zrezygnować z niego i zastąpić go bieżącym ?</translation>
    </message>
    <message>
        <source>Set a new key shortcut</source>
        <translation>Ustaw nowy skrót klawiszowy</translation>
    </message>
    <message>
        <source>The action &apos;%1&apos; has not been allocated to the key shortcut</source>
        <translation>Działanie &apos;%1&apos; zostało powiązane ze skrótem klawiszowym</translation>
    </message>
    <message>
        <source>Do you want to set it ?</source>
        <translation>Czy chcesz go ustawić ?</translation>
    </message>
    <message>
        <source>Key shortcut conflict</source>
        <translation>Konflikt skrótu klawiszowego</translation>
    </message>
</context>
<context>
    <name>OtherTextViewPage</name>
    <message>
        <source>Other</source>
        <translation>Inne</translation>
    </message>
    <message>
        <source>&amp;Tabulator size :</source>
        <translation>Rozmiar &amp;tabulatora :</translation>
    </message>
    <message>
        <source>&amp;Use external editor</source>
        <translation>Użyj &amp;zewnętrznego edytora</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <source>GetFile</source>
        <translation>PobierzPlik</translation>
    </message>
</context>
<context>
    <name>SettingsViewDialog</name>
    <message>
        <source>SettingsViewDialog</source>
        <translation>SettingsViewDialog</translation>
    </message>
    <message>
        <source>Text view</source>
        <translation>Podgląd tekstowy</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Font</translation>
    </message>
    <message>
        <source>Syntax highlighting</source>
        <translation>Poświetlanie składni</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>Inne</translation>
    </message>
    <message>
        <source>Image view</source>
        <translation>Podgląd obrazów</translation>
    </message>
    <message>
        <source>Video view</source>
        <translation>Podgląd wideo</translation>
    </message>
    <message>
        <source>Sound view</source>
        <translation>Podgląd dźwięku</translation>
    </message>
    <message>
        <source>Binary view</source>
        <translation>Podgląd binarny</translation>
    </message>
    <message>
        <source>Files association</source>
        <translation>Skojażenia plików</translation>
    </message>
    <message>
        <source>Keys shortcuts</source>
        <translation>Skróty klawiszowe</translation>
    </message>
    <message>
        <source>Tool bar</source>
        <translation>Paski narzędziowe</translation>
    </message>
    <message>
        <source>O&amp;K</source>
        <translation>O&amp;K</translation>
    </message>
    <message>
        <source>Alt+K</source>
        <translation>Alt+K</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Zapisz</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>Za&amp;stosuj</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>View settings</source>
        <translation>Ustawienia podglądu</translation>
    </message>
</context>
<context>
    <name>SyntaxHighlightingPage</name>
    <message>
        <source>SyntaxHighlighting</source>
        <translation>SyntaxHighlighting</translation>
    </message>
    <message>
        <source>Context</source>
        <translation>Kontekst</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation>Pogrubienie</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation>Pochylenie</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>Kolor</translation>
    </message>
    <message>
        <source>Use default</source>
        <translation>Użyj domyślnego</translation>
    </message>
    <message>
        <source>&amp;Highlighting :</source>
        <translation>&amp;Podświetlanie :</translation>
    </message>
    <message>
        <source>Set &amp;default values for current highlighting</source>
        <translation>Ustaw domyślne &amp;wartości dla bieżącego podświetlania</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
</context>
<context>
    <name>ToolBarViewPage</name>
    <message>
        <source>ToolBarViewPage</source>
        <translation>ToolBarViewPage</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>&amp;Usuń</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>Bars :</source>
        <translation>Paski :</translation>
    </message>
    <message>
        <source>&gt;&gt;</source>
        <translation>&gt;&gt;</translation>
    </message>
    <message>
        <source>Ins</source>
        <translation>Wst</translation>
    </message>
    <message>
        <source>/\</source>
        <translation>/\</translation>
    </message>
    <message>
        <source>\/</source>
        <translation>\/</translation>
    </message>
    <message>
        <source>Actions :</source>
        <translation>Działania :</translation>
    </message>
    <message>
        <source>Open in new window</source>
        <translation>Otwóż w nowym oknie</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Otwóż</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Drukuj</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation>Przeładuj</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation>Cofnij</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation>Przywróć</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation>Wytnij</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Kopiuj</translation>
    </message>
    <message>
        <source>Pase</source>
        <translation>Wstaw</translation>
    </message>
    <message>
        <source>Find</source>
        <translation>Znajdź</translation>
    </message>
    <message>
        <source>Find next</source>
        <translation>Znajdź następny</translation>
    </message>
    <message>
        <source>Replace</source>
        <translation>Zamień</translation>
    </message>
    <message>
        <source>Go to line</source>
        <translation>Skocz do linii</translation>
    </message>
    <message>
        <source>Change kind of view to</source>
        <translation>Zmień rodzaj widoku na</translation>
    </message>
    <message>
        <source>Zoom in</source>
        <translation>Powiększ</translation>
    </message>
    <message>
        <source>Zoom out</source>
        <translation>Pomniejsz</translation>
    </message>
    <message>
        <source>Configure view</source>
        <translation>Konfiguracja podglądu</translation>
    </message>
    <message>
        <source>Show/Hide side list</source>
        <translation>Pokaż/Schowaj boczną listę</translation>
    </message>
    <message>
        <source>global icons</source>
        <translation>globalne ikony</translation>
    </message>
    <message>
        <source>text view</source>
        <translation>podgląd tekstowy</translation>
    </message>
    <message>
        <source>image view</source>
        <translation>podgląd grafiki</translation>
    </message>
    <message>
        <source>sound view</source>
        <translation>podgląd dźwiękowy</translation>
    </message>
    <message>
        <source>video view</source>
        <translation>podgląd wideo</translation>
    </message>
</context>
</TS>
