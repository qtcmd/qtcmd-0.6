<!DOCTYPE TS><TS>
<context>
    <name>LocationChooser</name>
    <message>
        <source>&amp;Current directory</source>
        <translation>&amp;Bieżący katalog</translation>
    </message>
    <message>
        <source>&amp;Original directory</source>
        <translation>&amp;Pierwotny katalog</translation>
    </message>
    <message>
        <source>&amp;Home directory</source>
        <translation>Katalog &amp;domowy</translation>
    </message>
    <message>
        <source>&amp;Root directory</source>
        <translation>Katalog &amp;korzenia</translation>
    </message>
    <message>
        <source>&amp;Previous directory</source>
        <translation>&amp;Poprzeni katalog</translation>
    </message>
    <message>
        <source>&amp;Next directory</source>
        <translation>&amp;Następny katalog</translation>
    </message>
    <message>
        <source>&amp;Select directory</source>
        <translation>&amp;Wybór katalogu</translation>
    </message>
    <message>
        <source>Clear &amp;all URLs</source>
        <translation>Czyść &amp;wszystkie URLe</translation>
    </message>
    <message>
        <source>Clear current &amp;URL</source>
        <translation>Czyść &amp;bieżący URL</translation>
    </message>
    <message>
        <source>Clear history pathes</source>
        <translation>Czyść historię ścieżek</translation>
    </message>
    <message>
        <source>History contains</source>
        <translation>Historia zawiera</translation>
    </message>
    <message>
        <source>items</source>
        <translation>elementów</translation>
    </message>
    <message>
        <source>Do you really want to clear it</source>
        <translation>Czy naprawdę chcesz wyczyścić ją</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Tak</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Nie</translation>
    </message>
    <message>
        <source>Select target directory</source>
        <translation>Wybór katalogu docelowego</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Wszystkie pliki</translation>
    </message>
    <message>
        <source>Select target file</source>
        <translation>Wybór pliku docelowego</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <source>MessageBox</source>
        <translation>MessageBox</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Tak</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Nie</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>&amp;All</source>
        <translation>&amp;Wszystkie</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation>Zmiana &amp;nazwy</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
</context>
<context>
    <name>StatusBar</name>
    <message>
        <source>Selected</source>
        <translation>Zaznaczono</translation>
    </message>
    <message>
        <source>files, from</source>
        <translation>plików, z</translation>
    </message>
    <message>
        <source>about total size</source>
        <translation>o całkowitym rozmiarze</translation>
    </message>
    <message>
        <source>bytes</source>
        <translation>bajtów</translation>
    </message>
    <message>
        <source>size</source>
        <translation>rozmiar</translation>
    </message>
</context>
</TS>
