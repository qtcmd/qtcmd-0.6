<!DOCTYPE TS><TS>
<context>
    <name>FindTextDialog</name>
    <message>
        <source>FindTextDialog</source>
        <translation>FindTextDialog</translation>
    </message>
    <message>
        <source>&amp;New text:</source>
        <translation>&amp;Następny tekst:</translation>
    </message>
    <message>
        <source>&amp;Text:</source>
        <translation>&amp;Tekst:</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Opcje</translation>
    </message>
    <message>
        <source>&amp;Backward</source>
        <translation>Do &amp;tyłu</translation>
    </message>
    <message>
        <source>C&amp;ase sensitive</source>
        <translation>Rozróżniaj wielkość &amp;liter</translation>
    </message>
    <message>
        <source>F&amp;orward</source>
        <translation>Do &amp;przodu</translation>
    </message>
    <message>
        <source>&amp;Whole words only</source>
        <translation>Tylko całe &amp;słowa</translation>
    </message>
    <message>
        <source>From c&amp;ursor</source>
        <translation>Od &amp;kursora</translation>
    </message>
    <message>
        <source>&amp;Prompt on replace</source>
        <translation>Pytaj przed &amp;zmianą</translation>
    </message>
    <message>
        <source>&amp;Find</source>
        <translation>&amp;Szukaj</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Replace text</source>
        <translation>Zamień tekst</translation>
    </message>
    <message>
        <source>&amp;Replace</source>
        <translation>&amp;Zamień</translation>
    </message>
    <message>
        <source>Find text</source>
        <translation>Szuaj tekst</translation>
    </message>
</context>
<context>
    <name>TextView</name>
    <message>
        <source>Find</source>
        <translation>Szukaj</translation>
    </message>
    <message>
        <source>Find next</source>
        <translation>Szukaj następnego</translation>
    </message>
    <message>
        <source>Replace</source>
        <translation>Zamień</translation>
    </message>
    <message>
        <source>Go to line</source>
        <translation>Skocz do linii</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation>Cofnij</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation>Przywróć</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation>Wytnij</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Kopiuj</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>Wklej</translation>
    </message>
    <message>
        <source>Select all</source>
        <translation>Zaznacz wszystko</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Drukuj</translation>
    </message>
    <message>
        <source>Zoom in</source>
        <translation>Powiększ</translation>
    </message>
    <message>
        <source>Zoom out</source>
        <translation>Pomniejsz</translation>
    </message>
    <message>
        <source>Text completion</source>
        <translation>Kompletowanie tekstu</translation>
    </message>
    <message>
        <source>&amp;Undo</source>
        <translation>&amp;Cofnij</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>&amp;Przywróć</translation>
    </message>
    <message>
        <source>C&amp;ut</source>
        <translation>&amp;Wytnij</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>&amp;Kopiuj</translation>
    </message>
    <message>
        <source>&amp;Pase</source>
        <translation>&amp;Wklej</translation>
    </message>
    <message>
        <source>&amp;Find</source>
        <translation>&amp;Szukaj</translation>
    </message>
    <message>
        <source>Find &amp;next</source>
        <translation>Szukaj &amp;następny</translation>
    </message>
    <message>
        <source>&amp;Replace</source>
        <translation>&amp;Zamień</translation>
    </message>
    <message>
        <source>&amp;Go to line</source>
        <translation>Skocz do &amp;linii</translation>
    </message>
    <message>
        <source>&amp;Print</source>
        <translation>&amp;Drukuj</translation>
    </message>
    <message>
        <source>Zoom &amp;in</source>
        <translation>&amp;Powiększ</translation>
    </message>
    <message>
        <source>Zoom &amp;out</source>
        <translation>Po&amp;mniejsz</translation>
    </message>
    <message>
        <source>&amp;Unix</source>
        <translation>&amp;Unix</translation>
    </message>
    <message>
        <source>&amp;Windows and Dos</source>
        <translation>&amp;Windows i Dos</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Brak</translation>
    </message>
    <message>
        <source>C/C++</source>
        <translation>C/C++</translation>
    </message>
    <message>
        <source>HTML/XML</source>
        <translation>HTML/XML</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation>Automatycznie</translation>
    </message>
    <message>
        <source>Central European</source>
        <translation>Europa środkowa</translation>
    </message>
    <message>
        <source>End of line</source>
        <translation>Koniec linii</translation>
    </message>
    <message>
        <source>Highlighting &amp;mode</source>
        <translation>Tryb &amp;podświetlenia</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Źródło</translation>
    </message>
    <message>
        <source>Markup</source>
        <translation>Języki oznaczeń</translation>
    </message>
    <message>
        <source>Set encoding</source>
        <translation>Ustaw kodowanie</translation>
    </message>
    <message>
        <source>Manually</source>
        <translation>Ręcznie</translation>
    </message>
    <message>
        <source>Current font size</source>
        <translation>Rozmiar bieżącego fontu</translation>
    </message>
    <message>
        <source>Cannot write to this file</source>
        <translation>Nie można zapisywać do tego pliku</translation>
    </message>
    <message>
        <source>bytes</source>
        <translation>bajtów</translation>
    </message>
    <message>
        <source>row</source>
        <translation>wiersz</translation>
    </message>
    <message>
        <source>column</source>
        <translation>kolumna</translation>
    </message>
    <message>
        <source>byte</source>
        <translation>bajt</translation>
    </message>
    <message>
        <source>Find text</source>
        <translation>Szukaj tekst</translation>
    </message>
    <message>
        <source>Text not found</source>
        <translation>Tekst nie znaleziono</translation>
    </message>
    <message>
        <source>end</source>
        <translation>koniec</translation>
    </message>
    <message>
        <source>start</source>
        <translation>rozpocznij</translation>
    </message>
    <message>
        <source>begin</source>
        <translation>początek</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>Kontynuuj</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>Zatrzymaj</translation>
    </message>
    <message>
        <source>Search reached</source>
        <translation>Wyszukiwanie bogate</translation>
    </message>
    <message>
        <source>of the document</source>
        <translation>dokumentu</translation>
    </message>
    <message>
        <source>Continue from</source>
        <translation>Kontynuuj od</translation>
    </message>
    <message>
        <source>Enter new line number:</source>
        <translation>Podaj nowy numer lini:</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Tak</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Nie</translation>
    </message>
    <message>
        <source>&amp;All</source>
        <translation>&amp;Wszystko</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Replace text</source>
        <translation>Zamień tekst</translation>
    </message>
    <message>
        <source>Replace this occurence ?</source>
        <translation>Czy zamienić to wystąpienie ?</translation>
    </message>
    <message>
        <source>replaced made</source>
        <translation>wykonano zamian</translation>
    </message>
    <message>
        <source>Printing...</source>
        <translation>Drukowanie...</translation>
    </message>
    <message>
        <source>Printing completed</source>
        <translation>Drukowanie zakończono</translation>
    </message>
    <message>
        <source>Printing aborted</source>
        <translation>Drukowanie przerwano</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation>Zaznacz &amp;wszystko</translation>
    </message>
    <message>
        <source>Copy &amp;link location</source>
        <translation>Kopiuj &amp;link</translation>
    </message>
</context>
</TS>
