<!DOCTYPE TS><TS>
<context>
    <name>ArchiveFS</name>
    <message>
        <source>Reading an archive</source>
        <translation>Czytanie archiwum</translation>
    </message>
    <message>
        <source>Listing an archive</source>
        <translation>Listowanie archiwum</translation>
    </message>
    <message>
        <source>Can not open an archive</source>
        <translation>Nie można otworzyć archiwum</translation>
    </message>
</context>
<context>
    <name>DevicesMenu</name>
    <message>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <source>Cannot open file</source>
        <translation>Nie można otworzyć pliku</translation>
    </message>
    <message>
        <source>You don&apos;t know is devices is mounted</source>
        <translation>Nie wiesz czy urządzenie jest zamontowane</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Otwórz</translation>
    </message>
    <message>
        <source>Unmount</source>
        <translation>Odmontuj</translation>
    </message>
    <message>
        <source>Could not start</source>
        <translation>Nie można uruchomić</translation>
    </message>
    <message>
        <source>Mounting</source>
        <translation>Montowanie</translation>
    </message>
    <message>
        <source>Unmounting</source>
        <translation>Odmontowywanie</translation>
    </message>
    <message>
        <source>Please wait.</source>
        <translation>Proszę czekać.</translation>
    </message>
    <message>
        <source>still is process</source>
        <translation>wciąż trwa</translation>
    </message>
    <message>
        <source>&amp;Break</source>
        <translation>&amp;Przerwij</translation>
    </message>
    <message>
        <source>No medium found.</source>
        <translation>Nie znaleziono nośnika.</translation>
    </message>
    <message>
        <source>Device is busy.</source>
        <translation>Urządzenie jest zajęte.</translation>
    </message>
    <message>
        <source>Error no.</source>
        <translation>Numer błędu.</translation>
    </message>
    <message>
        <source>Could not</source>
        <translation>Nie można</translation>
    </message>
    <message>
        <source>device</source>
        <translation>urządzenia</translation>
    </message>
    <message>
        <source>mount</source>
        <translation>zamontować</translation>
    </message>
    <message>
        <source>umount</source>
        <translation>odmontować</translation>
    </message>
</context>
<context>
    <name>FileOverwriteDialog</name>
    <message>
        <source>FileOverwriteDialog</source>
        <translation>FileOverwriteDialog</translation>
    </message>
    <message>
        <source>This file already exists !</source>
        <translation>Ten plik już istnieje !</translation>
    </message>
    <message>
        <source>23 september 2003, 19:42</source>
        <translation>23 september 2003, 19:42</translation>
    </message>
    <message>
        <source>9999.99 GB</source>
        <translation>9999.99 GB</translation>
    </message>
    <message>
        <source>Source file date :</source>
        <translation>Czas pliku źródłowego :</translation>
    </message>
    <message>
        <source>Target file date :</source>
        <translation>Czas pliku docelowego :</translation>
    </message>
    <message>
        <source>Overwrite it ?</source>
        <translation>Nadpisać go ?</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Tak</translation>
    </message>
    <message>
        <source>Alt+Y</source>
        <translation>Alt+Y</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>N&amp;one</source>
        <translation>&amp;Nic nie robić</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>&amp;All</source>
        <translation>&amp;Wszystkie</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>&amp;Update all</source>
        <translation>&amp;Uaktualnij wszystkie</translation>
    </message>
    <message>
        <source>Alt+U</source>
        <translation>Alt+U</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation>Zmień &amp;nazwę</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <source>If &amp;different size</source>
        <translation>Jeśli różny &amp;rozmiar</translation>
    </message>
    <message>
        <source>Alt+D</source>
        <translation>Alt+D</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Nie</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>File overwriting</source>
        <translation>Nadpisywanie pliku</translation>
    </message>
    <message>
        <source>and size :</source>
        <translation>i rozmiar :</translation>
    </message>
</context>
<context>
    <name>FileView</name>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Lokalizacja</translation>
    </message>
    <message>
        <source>Open in a &amp;new window</source>
        <translation>Otwórz w &amp;nowym oknie</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation>&amp;Otwórz</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Zapisz</translation>
    </message>
    <message>
        <source>&amp;Reload</source>
        <translation>&amp;Przeładuj</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Plik</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Edycja</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Widok</translation>
    </message>
    <message>
        <source>&amp;Options</source>
        <translation>&amp;Opcje</translation>
    </message>
    <message>
        <source>&amp;Load old files list</source>
        <translation>Wczytaj &amp;starą listę plików</translation>
    </message>
    <message>
        <source>Save current files list</source>
        <translation>Zapisz bieżącą listę plików</translation>
    </message>
    <message>
        <source>Open &amp;recent files</source>
        <translation>Otwórz &amp;niedawne pliki</translation>
    </message>
    <message>
        <source>Save &amp;as</source>
        <translation>Zapisz &amp;jako</translation>
    </message>
    <message>
        <source>Side files &amp;list</source>
        <translation>&amp;Boczna lista plików</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Wyjście</translation>
    </message>
    <message>
        <source>Show/Hide side &amp;list</source>
        <translation>Pokaż/schowaj boczą &amp;listę</translation>
    </message>
    <message>
        <source>Show/Hide &amp;menu bar</source>
        <translation>Pokaż/schowaj pasek &amp;menu</translation>
    </message>
    <message>
        <source>Show/Hide &amp;tool bar</source>
        <translation>Pokaż/schowaj pasek &amp;narzędziowy</translation>
    </message>
    <message>
        <source>Kind of view</source>
        <translation>Rodzaj widoku</translation>
    </message>
    <message>
        <source>Ra&amp;w</source>
        <translation>&amp;Surowy</translation>
    </message>
    <message>
        <source>&amp;Render</source>
        <translation>&amp;Renderowany</translation>
    </message>
    <message>
        <source>&amp;Hex</source>
        <translation>&amp;Heksadecymalny</translation>
    </message>
    <message>
        <source>&amp;Configure view</source>
        <translation>&amp;Konfiguracja podglądu</translation>
    </message>
    <message>
        <source>Save settings</source>
        <translation>Zapisz ustawienia</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Otwórz</translation>
    </message>
    <message>
        <source>Change kind of view to</source>
        <translation>Zmień rodzaj widoku na</translation>
    </message>
    <message>
        <source>Close view</source>
        <translation>Zamkni widok</translation>
    </message>
    <message>
        <source>File view error</source>
        <translation>Błąd podglądu pliku</translation>
    </message>
    <message>
        <source>Plugin&apos;s path</source>
        <translation>Ścieżka do plugina</translation>
    </message>
    <message>
        <source>Plugin</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <source>not exists in foregoing location !</source>
        <translation>nie istnieje w wyzaczonym miejscu !</translation>
    </message>
    <message>
        <source>Try to open into binary view ?</source>
        <translation>Spróbować otworzyć w podglądzie binarnym ?</translation>
    </message>
    <message>
        <source>Plugins path</source>
        <translation>Ścieżka do plugina</translation>
    </message>
    <message>
        <source>Loading file in progress ...</source>
        <translation>Trwa ładowanie pliku ...</translation>
    </message>
    <message>
        <source>Open file into new window</source>
        <translation>Otwórz plik w nowym oknie</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Otwórz plik</translation>
    </message>
    <message>
        <source>Loading aborted</source>
        <translation>Ładowanie przerwano</translation>
    </message>
    <message>
        <source>File has been saved.</source>
        <translation>Plik został zapisany.</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation>Zapisz jako</translation>
    </message>
    <message>
        <source>Saving aborted</source>
        <translation>Zapisywanie przerwano</translation>
    </message>
    <message>
        <source>The files list has been loaded</source>
        <translation>Lista plików została załadowana</translation>
    </message>
    <message>
        <source>The files list has been saved</source>
        <translation>Lista plików została zapisana</translation>
    </message>
    <message>
        <source>Save file</source>
        <translation>Zapisz plik</translation>
    </message>
    <message>
        <source>Do you want to save changed document ?</source>
        <translation>Czy chcesz zapisać zmieniony dokument ?</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <source>Reload</source>
        <translation>Przeładuj</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Zapisz</translation>
    </message>
    <message>
        <source>Open in new window</source>
        <translation>Otwórz w nowym oknie</translation>
    </message>
    <message>
        <source>Show/hide menu bar</source>
        <translation>Pokaż/schowaj pasek menu</translation>
    </message>
    <message>
        <source>Show/hide tool bar</source>
        <translation>Pokaż/schowaj pasek narzędziowy</translation>
    </message>
    <message>
        <source>Show/hide side list</source>
        <translation>Pokaż/schowaj boczną listę</translation>
    </message>
    <message>
        <source>Toggle to Raw mode</source>
        <translation>Przełącz na tryb surowy</translation>
    </message>
    <message>
        <source>Toggle to Edit mode</source>
        <translation>Przełącz na tryb edycji</translation>
    </message>
    <message>
        <source>Toggle to Render mode</source>
        <translation>Przełącz na tryb renderowany</translation>
    </message>
    <message>
        <source>Toggle to Hex mode</source>
        <translation>Przełącz na tryb Hex</translation>
    </message>
    <message>
        <source>Show settings</source>
        <translation>Pokaż ustawienia</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Wyjście</translation>
    </message>
</context>
<context>
    <name>FilesPanel</name>
    <message>
        <source>Archives</source>
        <translation>Archiwa</translation>
    </message>
    <message>
        <source>are not supported !</source>
        <translation>nie są obsługiwane !</translation>
    </message>
    <message>
        <source>Cannot open this archive</source>
        <translation>Nie można otworzyć tego archiwum</translation>
    </message>
    <message>
        <source>Permission denied.</source>
        <translation>Dostęp niedozwolony.</translation>
    </message>
    <message>
        <source>Cannot to embedded an external viewer !</source>
        <translation>Nie można osadzić zewnętrznego podglądu !</translation>
    </message>
    <message>
        <source>Free bytes on current disk</source>
        <translation>Wolnych bajtów na bieżącym dysku</translation>
    </message>
    <message>
        <source>Total bytes on current disk</source>
        <translation>Wszystkich bajtów na bieżącym dysku</translation>
    </message>
    <message>
        <source>More information ...</source>
        <translation>Więcej informacji ...</translation>
    </message>
    <message>
        <source>&amp;Configure filters</source>
        <translation>Konfiguracja &amp;filtrów</translation>
    </message>
    <message>
        <source>&amp;Configure favorites</source>
        <translation>Konfiguracja &amp;ulubionych</translation>
    </message>
    <message>
        <source>&amp;Add current URL</source>
        <translation>&amp;Dodaj bieżący URL</translation>
    </message>
</context>
<context>
    <name>FindFileDialog</name>
    <message>
        <source>FindFileDialog</source>
        <translation>FindFileDialog</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Zamknij</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>&amp;Find</source>
        <translation>&amp;Szukaj</translation>
    </message>
    <message>
        <source>Alt+F</source>
        <translation>Alt+F</translation>
    </message>
    <message>
        <source>&amp;Name/Location</source>
        <translation>&amp;Nazwa/Lokalizacja</translation>
    </message>
    <message>
        <source>&amp;Location :</source>
        <translation>&amp;Lokalizacja :</translation>
    </message>
    <message>
        <source>C&amp;ontents</source>
        <translation>&amp;Zawartość</translation>
    </message>
    <message>
        <source>Include &amp;binary files</source>
        <translation>Sprawdź też pliki &amp;binarne</translation>
    </message>
    <message>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <source>A&amp;ttributs</source>
        <translation>&amp;Atrybuty</translation>
    </message>
    <message>
        <source>&amp;Take into consideration last modified time</source>
        <translation>&amp;Uwzględnij czas modyfikacji</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>Bytes</source>
        <translation>Bajtów</translation>
    </message>
    <message>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <source>between :</source>
        <translation>pomiędzy :</translation>
    </message>
    <message>
        <source>and</source>
        <translation>a</translation>
    </message>
    <message>
        <source>is equal</source>
        <translation>jest równy</translation>
    </message>
    <message>
        <source>at least</source>
        <translation>ma co najmniej</translation>
    </message>
    <message>
        <source>maximum</source>
        <translation>ma maksymalnie</translation>
    </message>
    <message>
        <source>The file size :</source>
        <translation>Rozmiar pliku :</translation>
    </message>
    <message>
        <source>The file owner :</source>
        <translation>Właściciel pliku :</translation>
    </message>
    <message>
        <source>and group :</source>
        <translation>i grupy :</translation>
    </message>
    <message>
        <source>C&amp;ustomize</source>
        <translation>&amp;Dostosuj</translation>
    </message>
    <message>
        <source>Stop &amp;after</source>
        <translation>&amp;Zatrzymaj po</translation>
    </message>
    <message>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <source>matches</source>
        <translation>dopasowaniach</translation>
    </message>
    <message>
        <source>Search into &amp;selected files only</source>
        <translation>Szukaj &amp;tylko w zaznaczonych plikach</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>Find &amp;recursive</source>
        <translation>Szukaj &amp;rekursywnie</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <source>directories</source>
        <translation>katalogów</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Czas</translation>
    </message>
    <message>
        <source>Permission</source>
        <translation>Prawa dostępu</translation>
    </message>
    <message>
        <source>Owner</source>
        <translation>Właściciel</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Grupa</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Lokalizacja</translation>
    </message>
    <message>
        <source>&amp;Stop</source>
        <translation>&amp;Zatrzymaj</translation>
    </message>
    <message>
        <source>&amp;Pause</source>
        <translation>&amp;Przerwa</translation>
    </message>
    <message>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <source>Clear &amp;list</source>
        <translation>&amp;Czyść listę</translation>
    </message>
    <message>
        <source>Alt+L</source>
        <translation>Alt+L</translation>
    </message>
    <message>
        <source>Find file(s)</source>
        <translation>Znajdź pliki</translation>
    </message>
    <message>
        <source>Ready.</source>
        <translation>Gotowy.</translation>
    </message>
    <message>
        <source>Matched:</source>
        <translation>Dopasowano :</translation>
    </message>
    <message>
        <source>from</source>
        <translation>z</translation>
    </message>
    <message>
        <source>files and</source>
        <translation>plików i</translation>
    </message>
    <message>
        <source>Searching</source>
        <translation>Szukanie</translation>
    </message>
    <message>
        <source>Pause.</source>
        <translation>Przerwa.</translation>
    </message>
    <message>
        <source>&amp;Go to file</source>
        <translation>&amp;Przejdź do pliku</translation>
    </message>
    <message>
        <source>&amp;View file</source>
        <translation>P&amp;odgląd pliku</translation>
    </message>
    <message>
        <source>&amp;Edit file</source>
        <translation>&amp;Edytuj plik</translation>
    </message>
    <message>
        <source>&amp;Delete file</source>
        <translation>&amp;Usuń plik</translation>
    </message>
    <message>
        <source>Don&apos;t gived a name to find</source>
        <translation>Nie podano nazwy do szukania</translation>
    </message>
    <message>
        <source>Don&apos;t gived a location for searching !</source>
        <translation>Nie podano lokalizacji wyszukiwania !</translation>
    </message>
    <message>
        <source>Filter :</source>
        <translation>Filtr :</translation>
    </message>
    <message>
        <source>Case se&amp;nsitive</source>
        <translation>Rozróżniaj wielkość &amp;liter</translation>
    </message>
    <message>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <source>Na&amp;me :</source>
        <translation>Naz&amp;wa :</translation>
    </message>
    <message>
        <source>Contains &amp;text :</source>
        <translation>Zawierające &amp;tekst :</translation>
    </message>
    <message>
        <source>Regular &amp;expression</source>
        <translation>Wyrażenie &amp;regularne</translation>
    </message>
    <message>
        <source>Alt+E</source>
        <translation>Alt+E</translation>
    </message>
</context>
<context>
    <name>FtpConnectDialog</name>
    <message>
        <source>FtpConnectDialog</source>
        <translation>FtpConnectDialog</translation>
    </message>
    <message>
        <source>&amp;Password:</source>
        <translation>&amp;Hasło:</translation>
    </message>
    <message>
        <source>&amp;Directory:</source>
        <translation>&amp;Katalog:</translation>
    </message>
    <message>
        <source>&amp;Host:</source>
        <translation>&amp;Serwer:</translation>
    </message>
    <message>
        <source>P&amp;ort:</source>
        <translation>&amp;Port:</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>&amp;Save this session</source>
        <translation>&amp;Zapisz tę sesję</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>Connect to</source>
        <translation>Połącz z</translation>
    </message>
    <message>
        <source>Do not passed an user name !</source>
        <translation>Nie podano nazwy użytkownika !</translation>
    </message>
    <message>
        <source>Do not passed a password !</source>
        <translation>Nie podano hasła !</translation>
    </message>
    <message>
        <source>new session</source>
        <translation>nowa sesja</translation>
    </message>
    <message>
        <source>&amp;User:</source>
        <translation>&amp;Użytkownik :</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
</context>
<context>
    <name>FtpFS</name>
    <message>
        <source>FTP error</source>
        <translation>Błąd FTP</translation>
    </message>
    <message>
        <source>Cannot write to this directory</source>
        <translation>Nie można zapisywać do tego katalogu</translation>
    </message>
    <message>
        <source>FTP connection</source>
        <translation>Połączenie FTP</translation>
    </message>
    <message>
        <source>You are unconnected to the server.</source>
        <translation>Nie jesteś połączony z serwerem.</translation>
    </message>
    <message>
        <source>Could you connect again ?</source>
        <translation>Czy chcesz się połączyć ponownie ?</translation>
    </message>
    <message>
        <source>Cannot remove this file.</source>
        <translation>Nie można usunąć tego pliku.</translation>
    </message>
    <message>
        <source>Connected. Try logged in as </source>
        <translation>Połączony. Próba zalogowania jako</translation>
    </message>
    <message>
        <source>Getting the list of files...</source>
        <translation>Pobieranie listy plików...</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <source>Skip all next errors</source>
        <translation>Ominąć wszystkie następne błędy</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Serwer</translation>
    </message>
    <message>
        <source>Directory</source>
        <translation>Katalog</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Użytkownik</translation>
    </message>
    <message>
        <source>Host not found</source>
        <translation>Nie znaleziono serwera</translation>
    </message>
    <message>
        <source>Try again</source>
        <translation>Spróbować ponownie</translation>
    </message>
    <message>
        <source>Changing directory failed</source>
        <translation>Błąd przy zmianie katalogu</translation>
    </message>
    <message>
        <source>The server refused the connection</source>
        <translation>Serwer odrzucił połączenie</translation>
    </message>
    <message>
        <source>There is no connection to a server</source>
        <translation>Brak połączenie z serwerem</translation>
    </message>
    <message>
        <source>The requested name is valid but does not have an IP address</source>
        <translation>Żądana nazwa jest poprawna, ale nie podano adresu IP</translation>
    </message>
    <message>
        <source>A non-recoverable name server error occurred</source>
        <translation>Nie rozpoznawalna nazwa serwera</translation>
    </message>
    <message>
        <source>Login failed</source>
        <translation>Błąd podczas logowania</translation>
    </message>
    <message>
        <source>User or password incorrect</source>
        <translation>Nieprawidłowa nazwa użytkownika lub hasła</translation>
    </message>
    <message>
        <source>Cannot remove this file</source>
        <translation>Nie można usunąć tego pliku</translation>
    </message>
    <message>
        <source>Cannot read this file</source>
        <translation>Nie można czytać tego pliku</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>Nieznany błąd</translation>
    </message>
    <message>
        <source>File not exists</source>
        <translation>Plik nie istnieje</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation>Połączono</translation>
    </message>
    <message>
        <source>Unconnected</source>
        <translation>Rozłączono</translation>
    </message>
    <message>
        <source>Host lookup</source>
        <translation>Wyszukiwanie serwera</translation>
    </message>
    <message>
        <source>Connecting</source>
        <translation>Łączenie się</translation>
    </message>
    <message>
        <source>Logged in as</source>
        <translation>Zalogowany jako</translation>
    </message>
    <message>
        <source>Closing</source>
        <translation>Zamykanie</translation>
    </message>
</context>
<context>
    <name>ListViewExt</name>
    <message>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Czas</translation>
    </message>
    <message>
        <source>Permission</source>
        <translation>Prawa dostępu</translation>
    </message>
    <message>
        <source>Owner</source>
        <translation>Właściciel</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Grupa</translation>
    </message>
    <message>
        <source>UnSelect</source>
        <translation>Odznacz</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Zaznacz</translation>
    </message>
    <message>
        <source>group of files</source>
        <translation>grupa plików</translation>
    </message>
    <message>
        <source>Please to give new template:</source>
        <translation>Proszę podać nowy wzorzec:</translation>
    </message>
</context>
<context>
    <name>ListViewFindItem</name>
    <message>
        <source>Search</source>
        <translation>Szukaj</translation>
    </message>
</context>
<context>
    <name>LocalFS</name>
    <message>
        <source>Cannot to read this file</source>
        <translation>Nie można czytać tego pliku</translation>
    </message>
    <message>
        <source>Cannot rename more than one file</source>
        <translation>Nie można zmienić nazwy tego pliku</translation>
    </message>
    <message>
        <source>Cannot to copy more than one file to this same directory</source>
        <translation>Nie można więcej niż jednego pliku do tego samego katalogu</translation>
    </message>
    <message>
        <source>files selected</source>
        <translation>plików zaznaczonych</translation>
    </message>
    <message>
        <source>Cannot remove this file</source>
        <translation>Nie można usunąć tego pliku</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation>Dostęp niedozwolony</translation>
    </message>
    <message>
        <source>move</source>
        <translation>przenieś</translation>
    </message>
    <message>
        <source>copy</source>
        <translation>kopiuj</translation>
    </message>
    <message>
        <source>Source file :</source>
        <translation>Plik źródłowy :</translation>
    </message>
    <message>
        <source>Target file :</source>
        <translation>Plik docelowy :</translation>
    </message>
    <message>
        <source>Cannot</source>
        <translation>Nie można</translation>
    </message>
    <message>
        <source>this same to this same</source>
        <translation>tego samego do tego samego</translation>
    </message>
    <message>
        <source>This file will be passing over.</source>
        <translation>Ten plik będzie ominięty.</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <source>This file not exists !</source>
        <translation>Ten plik nie istnieje !</translation>
    </message>
    <message>
        <source>Cannot open this file !</source>
        <translation>Nie można otworzyć tego pliku !</translation>
    </message>
    <message>
        <source>file</source>
        <translation>plik</translation>
    </message>
    <message>
        <source>directory</source>
        <translation>katalogu</translation>
    </message>
    <message>
        <source>This</source>
        <translation>Ten</translation>
    </message>
    <message>
        <source>not exists</source>
        <translation>nie istnieje</translation>
    </message>
    <message>
        <source>Not enough free space in target directory</source>
        <translation>Nie ma wystarczającej ilości miejsca w tym katalogu</translation>
    </message>
    <message>
        <source>Read error</source>
        <translation>Błąd odczytu</translation>
    </message>
    <message>
        <source>Cannot to remove this file</source>
        <translation>Nie można usunąć tego pliku</translation>
    </message>
    <message>
        <source>already exists</source>
        <translation>już istnieje</translation>
    </message>
    <message>
        <source>Error number: </source>
        <translation>Błąd numer:</translation>
    </message>
    <message>
        <source>Operation not permitted</source>
        <translation>Operacja nie dozwolona</translation>
    </message>
    <message>
        <source>Overwrite file</source>
        <translation>Nadpisywanie pliku</translation>
    </message>
    <message>
        <source>Overwrite it by</source>
        <translation>Nadpisywać go przez</translation>
    </message>
    <message>
        <source>Is to show next error announcement</source>
        <translation>Czy pokazać następny komunikat błędu</translation>
    </message>
    <message>
        <source>This directory not exists !</source>
        <translation>Ten katalog nie istnieje !</translation>
    </message>
    <message>
        <source>This is not directory !</source>
        <translation>To nie jest katalog !</translation>
    </message>
    <message>
        <source>Cannot read this</source>
        <translation>Nie można czytać tego</translation>
    </message>
    <message>
        <source>Cannot write to this</source>
        <translation>Nie można zapisywać do tego</translation>
    </message>
    <message>
        <source>Cannot remove this</source>
        <translation>Nie można usunąć tego</translation>
    </message>
    <message>
        <source>Cannot set attributs for this</source>
        <translation>Nie można ustawić atrybutów dla tego</translation>
    </message>
    <message>
        <source>Cannot create this link</source>
        <translation>Nie można utworzyć tego linku</translation>
    </message>
    <message>
        <source>Cannot write to this directory !</source>
        <translation>Nie można zapisywać do tego katalogu !</translation>
    </message>
</context>
<context>
    <name>PathView</name>
    <message>
        <source>Go to &amp;home directory</source>
        <translation>Idź do &amp;domowego katalogu</translation>
    </message>
    <message>
        <source>Go to &amp;root directory</source>
        <translation>Idź do katalogu &amp;korzenia</translation>
    </message>
    <message>
        <source>Go to &amp;up directory</source>
        <translation>Idź do katalogu &amp;wyżej</translation>
    </message>
    <message>
        <source>&amp;Edit history</source>
        <translation>&amp;Edycja historii</translation>
    </message>
    <message>
        <source>&amp;Clear history</source>
        <translation>&amp;Czyść historię</translation>
    </message>
    <message>
        <source>Go to &amp;previous directory</source>
        <translation>Przejdź do &amp;poprzedniego katalogu</translation>
    </message>
    <message>
        <source>Go to &amp;next directory</source>
        <translation>Przejdź do &amp;następnego katalogu</translation>
    </message>
    <message>
        <source>C&amp;lear current</source>
        <translation>Czyść &amp;bieżący</translation>
    </message>
    <message>
        <source>Clear history pathes</source>
        <translation>Czyść historię ścieżek</translation>
    </message>
    <message>
        <source>History contains</source>
        <translation>Historia zawiera</translation>
    </message>
    <message>
        <source>items</source>
        <translation>elementów</translation>
    </message>
    <message>
        <source>Do you really want to clear it</source>
        <translation>Czy na prawdę chcesz wyczyścić ją</translation>
    </message>
</context>
<context>
    <name>PopupMenu</name>
    <message>
        <source>&amp;Open</source>
        <translation>&amp;Otwórz</translation>
    </message>
    <message>
        <source>Open in panel &amp;beside</source>
        <translation>Otwórz w &amp;panelu obok</translation>
    </message>
    <message>
        <source>Re&amp;name</source>
        <translation>Zmień &amp;nazwę</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>&amp;Usuń</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>&amp;Kopiuj</translation>
    </message>
    <message>
        <source>&amp;Make a link</source>
        <translation>Utwórz &amp;link</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation>Zaznacz &amp;wszystkie</translation>
    </message>
    <message>
        <source>&amp;UnSelect All</source>
        <translation>O&amp;dznacz wszystkie</translation>
    </message>
    <message>
        <source>Select with this &amp;same ext</source>
        <translation>Zaznacz z tym samym &amp;roszerzeniem</translation>
    </message>
    <message>
        <source>UnSelect with this same ext</source>
        <translation>Odznacz z tym samym rozszrzeniem</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation>&amp;Właściwości</translation>
    </message>
</context>
<context>
    <name>PreOperationDialog</name>
    <message>
        <source>PreOperationDialog</source>
        <translation>PreOperationDialog</translation>
    </message>
    <message>
        <source>Save file &amp;permission</source>
        <translation>Zachowaj &amp;prawa dostępu</translation>
    </message>
    <message>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <source>Always over&amp;write</source>
        <translation>Zawsze &amp;nadpisuj</translation>
    </message>
    <message>
        <source>Alt+W</source>
        <translation>Alt+W</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>In &amp;background</source>
        <translation>W &amp;tle</translation>
    </message>
    <message>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <source>Hard link</source>
        <translation>Twardy link</translation>
    </message>
    <message>
        <source>copy to</source>
        <translation>skopiować do</translation>
    </message>
    <message>
        <source>move to</source>
        <translation>przenosić do</translation>
    </message>
    <message>
        <source>new name for current file</source>
        <translation>nowa nazwa dla bieżącego pliku</translation>
    </message>
    <message>
        <source>new path for current link</source>
        <translation>nowa ścieżka dla bieżącego linku</translation>
    </message>
    <message>
        <source>new link name</source>
        <translation>nowa nazwa linku</translation>
    </message>
    <message>
        <source>Copying files to</source>
        <translation>Kopiowanie plików do</translation>
    </message>
    <message>
        <source>Moving files to</source>
        <translation>Przenoszenie plików do</translation>
    </message>
    <message>
        <source>Rename copied file</source>
        <translation>Zmiana nazwy kopiowanych plików</translation>
    </message>
    <message>
        <source>Editing link</source>
        <translation>Edytowanie linku</translation>
    </message>
    <message>
        <source>Making directory</source>
        <translation>Tworzenie katalogu</translation>
    </message>
    <message>
        <source>Making empty file</source>
        <translation>Tworzenie pustego pliku</translation>
    </message>
    <message>
        <source>Making link</source>
        <translation>Tworzenie linku</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <source>ProgressDialog</source>
        <translation>ProgressDialog</translation>
    </message>
    <message>
        <source>Source :</source>
        <translation>Źródło :</translation>
    </message>
    <message>
        <source>Target :</source>
        <translation>Cel :</translation>
    </message>
    <message>
        <source>Close after &amp;finished</source>
        <translation>Zamknij po &amp;zakończeniu</translation>
    </message>
    <message>
        <source>In &amp;background</source>
        <translation>W &amp;tle</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Weighed :</source>
        <translation>Zważono :</translation>
    </message>
    <message>
        <source>Transfer :</source>
        <translation>Transfer :</translation>
    </message>
    <message>
        <source>Time :</source>
        <translation>Czas :</translation>
    </message>
    <message>
        <source>Estimated time :</source>
        <translation>Szacowany czas :</translation>
    </message>
    <message>
        <source>Moving</source>
        <translation>Przenoszenie</translation>
    </message>
    <message>
        <source>Copying</source>
        <translation>Kopiowanie</translation>
    </message>
    <message>
        <source>Removing</source>
        <translation>Usuwanie</translation>
    </message>
    <message>
        <source>Weighing</source>
        <translation>Ważenie</translation>
    </message>
    <message>
        <source>Setting the attributs</source>
        <translation>Ustawianie atrybutów</translation>
    </message>
    <message>
        <source>files</source>
        <translation>plików</translation>
    </message>
    <message>
        <source>bytes per second</source>
        <translation>bajtów na sekundę</translation>
    </message>
    <message>
        <source>per second</source>
        <translation>na sekundę</translation>
    </message>
    <message>
        <source>directories</source>
        <translation>katalogów</translation>
    </message>
    <message>
        <source>Copied</source>
        <translation>Skopiowano</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation>Usunięto</translation>
    </message>
    <message>
        <source>Weighed</source>
        <translation>Zważono</translation>
    </message>
    <message>
        <source>Set the attributs</source>
        <translation>Ustawiono atrybutów</translation>
    </message>
</context>
<context>
    <name>PropertiesDialog</name>
    <message>
        <source>PropertiesDialog</source>
        <translation>PropertiesDialog</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Alt+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <source>Type :</source>
        <translation>Typ :</translation>
    </message>
    <message>
        <source>Location :</source>
        <translation>Położenie :</translation>
    </message>
    <message>
        <source>&amp;Name :</source>
        <translation>&amp;Nazwa :</translation>
    </message>
    <message>
        <source>Size :</source>
        <translation>Rozmiar :</translation>
    </message>
    <message>
        <source>Change access and modified &amp;time</source>
        <translation>Zmień &amp;czas dostępu i modyfikacji</translation>
    </message>
    <message>
        <source>Alt+T</source>
        <translation>Alt+T</translation>
    </message>
    <message>
        <source>&amp;Refresh</source>
        <translation>&amp;Odśwież</translation>
    </message>
    <message>
        <source>Alt+R</source>
        <translation>Alt+R</translation>
    </message>
    <message>
        <source>Permission</source>
        <translation></translation>
    </message>
    <message>
        <source>Change &amp;owner and group</source>
        <translation>Zmień &amp;nazwę użytkownika i grupy</translation>
    </message>
    <message>
        <source>Class</source>
        <translation>Klasa</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Użytkownik</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>Grupa</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>Pozostali</translation>
    </message>
    <message>
        <source>Read</source>
        <translation>Odczyt</translation>
    </message>
    <message>
        <source>U</source>
        <translation>U</translation>
    </message>
    <message>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <source>O</source>
        <translation>P</translation>
    </message>
    <message>
        <source>Write</source>
        <translation>Zapis</translation>
    </message>
    <message>
        <source>Executable</source>
        <translation>Uruchamianie</translation>
    </message>
    <message>
        <source>Special</source>
        <translation>Specjalne</translation>
    </message>
    <message>
        <source>SUID</source>
        <translation>SUID</translation>
    </message>
    <message>
        <source>SGID</source>
        <translation>SGID</translation>
    </message>
    <message>
        <source>SVTX</source>
        <translation>SVTX</translation>
    </message>
    <message>
        <source>Change &amp;permission</source>
        <translation>Zmień &amp;prawa dostępu</translation>
    </message>
    <message>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <source>Group :</source>
        <translation>Grupa :</translation>
    </message>
    <message>
        <source>Owner :</source>
        <translation>Właściciel :</translation>
    </message>
    <message>
        <source>Customize</source>
        <translation>Dostosuj</translation>
    </message>
    <message>
        <source>&amp;Recursively changes</source>
        <translation>Zmiany &amp;rekursywne</translation>
    </message>
    <message>
        <source>ALL SELECTED</source>
        <translation>WSZYSTKIE ZAZNACZONE</translation>
    </message>
    <message>
        <source>directories and</source>
        <translation>katalogów i</translation>
    </message>
    <message>
        <source>files</source>
        <translation>pliki</translation>
    </message>
    <message>
        <source>directory</source>
        <translation>katalog</translation>
    </message>
    <message>
        <source>symbolic/hard link</source>
        <translation>link symboliczny/link twardy</translation>
    </message>
    <message>
        <source>file</source>
        <translation>plik</translation>
    </message>
    <message>
        <source>The file(s) properties</source>
        <translation>Właściwości pliku(ów)</translation>
    </message>
    <message>
        <source>Changes for :</source>
        <translation>Zmiany dla :</translation>
    </message>
    <message>
        <source>Last &amp;access time :</source>
        <translation>Czas ostatniego &amp;odczytu :</translation>
    </message>
    <message>
        <source>Last &amp;modified time :</source>
        <translation>Czas ostatniej &amp;modyfikacji :</translation>
    </message>
    <message>
        <source>&amp;Stop</source>
        <translation>&amp;Zatrzymaj</translation>
    </message>
    <message>
        <source>Alt+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <source>Weighing</source>
        <translation>Ważenie</translation>
    </message>
</context>
<context>
    <name>QtCmd</name>
    <message>
        <source>List View</source>
        <translation>Widok listy</translation>
    </message>
    <message>
        <source>Tree View</source>
        <translation>Widok drzewa</translation>
    </message>
    <message>
        <source>User defined</source>
        <translation>Zdefiniowane przez użytkownika</translation>
    </message>
    <message>
        <source>width of left panel</source>
        <translation>szerokość lewego panela</translation>
    </message>
    <message>
        <source>width of right panel</source>
        <translation>szerokość prawego panela</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Plik</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Widok</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>&amp;Ustawienia</translation>
    </message>
    <message>
        <source>&amp;Tools</source>
        <translation>&amp;Narzędzia</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Pomoc</translation>
    </message>
    <message>
        <source>Show/Hide &amp;menu bar</source>
        <translation>Pokaż/schowaj pasek &amp;menu</translation>
    </message>
    <message>
        <source>Show/Hide &amp;tool bar</source>
        <translation>Pokaż/schowaj pasek &amp;narzędziowy</translation>
    </message>
    <message>
        <source>Configure </source>
        <translation>Konfiguracja</translation>
    </message>
    <message>
        <source>FTP connection manager</source>
        <translation>Menadżer połączeń FTP</translation>
    </message>
    <message>
        <source>Show free space on current disk</source>
        <translation>Pokaż ilość wolnego miejsca na bieżącym dysku</translation>
    </message>
    <message>
        <source>Show storage devices list</source>
        <translation>Pokaż listę urządzeń masowych</translation>
    </message>
    <message>
        <source>Reread current directory</source>
        <translation>Odczytaj ponownie bieżący katalog</translation>
    </message>
    <message>
        <source>Show favorites</source>
        <translation>Pokaż ulubione</translation>
    </message>
    <message>
        <source>Fin&amp;d file</source>
        <translation>&amp;Znajdź plik</translation>
    </message>
    <message>
        <source>Quick get out from file system</source>
        <translation>Wyskocz z systemu plików</translation>
    </message>
    <message>
        <source>Kind of &amp;view for current panel</source>
        <translation>Rodzaj &amp;widoku dla bieżącego panela</translation>
    </message>
    <message>
        <source>&amp;Proportions of panels</source>
        <translation>&amp;Proporcje paneli</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>O &amp;programie</translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation>O &amp;Qt</translation>
    </message>
    <message>
        <source>Change kind of list view</source>
        <translation>Zmień rodzaj widoku listy</translation>
    </message>
    <message>
        <source>Proportions of panels</source>
        <translation>Proporcje paneli</translation>
    </message>
    <message>
        <source>Twin panels files manager</source>
        <translation>Dwupanelowy menadżer plików</translation>
    </message>
    <message>
        <source>Plugins path</source>
        <translation>Ścieżka do plugina</translation>
    </message>
    <message>
        <source>Plugin</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <source>not exists in foregoing location !</source>
        <translation>nie istnieje w wyzaczonym miejscu !</translation>
    </message>
    <message>
        <source>Show/Hide &amp;button bar</source>
        <translation>Pokaż/schowaj pasek z &amp;guzikami</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Pomoc</translation>
    </message>
    <message>
        <source>Change view</source>
        <translation>Zmień widok</translation>
    </message>
    <message>
        <source>View</source>
        <translation>Podgląd</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Edytuj</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Kopiuj</translation>
    </message>
    <message>
        <source>Move</source>
        <translation>Przenieś</translation>
    </message>
    <message>
        <source>Make dir</source>
        <translation>Utwórz katalog</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Wyjście</translation>
    </message>
    <message>
        <source>Do you really want to quit</source>
        <translation>Czy na prawdę chcesz wyjść</translation>
    </message>
    <message>
        <source>Quit an application</source>
        <translation>Zakończ aplikację</translation>
    </message>
    <message>
        <source>View file</source>
        <translation>Podgląd pliku</translation>
    </message>
    <message>
        <source>Edit file</source>
        <translation>Edycja pliku</translation>
    </message>
    <message>
        <source>Copy files</source>
        <translation>Kopiowanie plików</translation>
    </message>
    <message>
        <source>Move files</source>
        <translation>Przenoszenie plików</translation>
    </message>
    <message>
        <source>Make directory</source>
        <translation>Tworzenie katalogu</translation>
    </message>
    <message>
        <source>Delete files</source>
        <translation>Usuwanie plików</translation>
    </message>
    <message>
        <source>Make an empty file</source>
        <translation>Tworzenie pustego pliku</translation>
    </message>
    <message>
        <source>Copy files to current directory</source>
        <translation>Kopiowanie pliku do bieżącego katalogu</translation>
    </message>
    <message>
        <source>Quick file rename</source>
        <translation>Szybka zmiana nazwy</translation>
    </message>
    <message>
        <source>Quick change of file modified date</source>
        <translation>Szybka zmiana czasu modyfikacji</translation>
    </message>
    <message>
        <source>Quick change of file permission</source>
        <translation>Szybka zmiana praw dostępu</translation>
    </message>
    <message>
        <source>Quick change of file owner</source>
        <translation>Szybka zmiana właściciela</translation>
    </message>
    <message>
        <source>Quick change of file group</source>
        <translation>Szybka zmiana grupy</translation>
    </message>
    <message>
        <source>Go to up level</source>
        <translation>Przejdź na wyższy poziom</translation>
    </message>
    <message>
        <source>Refresh directory</source>
        <translation>Odśwież katalog</translation>
    </message>
    <message>
        <source>Make a link</source>
        <translation>Utwórz link</translation>
    </message>
    <message>
        <source>Edit current link</source>
        <translation>Edytuj bieżący link</translation>
    </message>
    <message>
        <source>Select files with this same extention</source>
        <translation>Zaznacz pliki z tym samym roszerzeniem</translation>
    </message>
    <message>
        <source>Deselect files with this same extention</source>
        <translation>Odznacz pliki z tym samym roszerzeniem</translation>
    </message>
    <message>
        <source>Select all files</source>
        <translation>Zaznacz wszystkie pliki</translation>
    </message>
    <message>
        <source>Deselect all files</source>
        <translation>Odznacz wszystkie pliki</translation>
    </message>
    <message>
        <source>Select group of files</source>
        <translation>Zaznacz grupę plików</translation>
    </message>
    <message>
        <source>Deselect group of files</source>
        <translation>Odznacz grupę plików</translation>
    </message>
    <message>
        <source>Change view to list</source>
        <translation>Zmień widok na listę</translation>
    </message>
    <message>
        <source>Change view to tree</source>
        <translation>Zmień widok na drzewo</translation>
    </message>
    <message>
        <source>Quick file search</source>
        <translation>Szybkie szukanie pliku</translation>
    </message>
    <message>
        <source>Show history&apos;s menu</source>
        <translation>Pokaż menu historii</translation>
    </message>
    <message>
        <source>Show history</source>
        <translation>Pokaż historię</translation>
    </message>
    <message>
        <source>Go to previous URL</source>
        <translation>Przejdź do poprzedniego URLa</translation>
    </message>
    <message>
        <source>Go to next URL</source>
        <translation>Przejdź do następnego URLa</translation>
    </message>
    <message>
        <source>Move cursor to path view</source>
        <translation>Przenieś kursor do widoku ścieżki</translation>
    </message>
    <message>
        <source>Show free space</source>
        <translation>Pokaż ilość wolnego miejsca</translation>
    </message>
    <message>
        <source>Find file</source>
        <translation>Znajdź plik</translation>
    </message>
    <message>
        <source>Quick go out from current file system</source>
        <translation>Wyskocz z bieżącego systemu plików</translation>
    </message>
    <message>
        <source>Open current file or directory in left panel</source>
        <translation>Otwórz bieżący plik lub katalog w lewym panelu</translation>
    </message>
    <message>
        <source>Open current file or directory in right panel</source>
        <translation>Otwórz bieżący plik lub katalog w prawym panelu</translation>
    </message>
    <message>
        <source>Force open current directory in left panel</source>
        <translation>Wymuś otwarcie bieżącego katalogu w lewym panelu</translation>
    </message>
    <message>
        <source>Force open current directory in right panel</source>
        <translation>Wymuś otwarcie bieżącego katalogu w prawympanelu</translation>
    </message>
    <message>
        <source>Weigh current directory</source>
        <translation>Zważ bieżący katalog</translation>
    </message>
    <message>
        <source>Show settings of application</source>
        <translation>Pokaż ustawienia aplikacji</translation>
    </message>
    <message>
        <source>Show/hide menu bar</source>
        <translation>Pokaż/schowaj pasek menu</translation>
    </message>
    <message>
        <source>Show/hide tool bar</source>
        <translation>Pokaż/schowaj pasek narzędziowy</translation>
    </message>
    <message>
        <source>Show/hide buttons bar</source>
        <translation>Pokaż/schowaj pasek z guzikami</translation>
    </message>
    <message>
        <source>Show filters menu</source>
        <translation>Pokaż menu filtrów</translation>
    </message>
    <message>
        <source>Show FTP manager</source>
        <translation>Pokaż menadżer połączeń FTP</translation>
    </message>
    <message>
        <source>Show filters manager</source>
        <translation>Pokaż menadżer filtrów</translation>
    </message>
    <message>
        <source>Show file properties dialog</source>
        <translation>Pokaż okienko właściwości</translation>
    </message>
    <message>
        <source>QtCommander a old school file manager. An application is similar to Total Commander, but with many extras.</source>
        <translation>QtCommander jest meadżerem plików wywodzącym się ze starej szkoły. Aplikacja jest podobna do Total Commander, ale ma kilka dodatów.</translation>
    </message>
    <message>
        <source>QtCmd have a FTP client (no recursive operations), simple archive support (listing only),</source>
        <translation>QtCmd ma klienta FTP (be obsługiwania operacji rekurencyjnych), prostą obsługę archiwów (tylko listowanie),</translation>
    </message>
    <message>
        <source>full local files system support and mounted file system.</source>
        <translation>pełne wsparcie dla lokalnego systemu plików oraz system montowania systemu plików.</translation>
    </message>
    <message>
        <source>Possess advanced and very configurable file viewer and editor.</source>
        <translation>Posiada zaawansowany a bardzo konfigurowalny podgląd i edytor.</translation>
    </message>
    <message>
        <source>Go to home directory</source>
        <translation>Przejdź do katalogu domowego</translation>
    </message>
    <message>
        <source>Distributed under the terms of the GNU General Public License v2</source>
        <translation>Rozpowszechniane zgodnie z licencją GNU General Public License wersja 2</translation>
    </message>
</context>
<context>
    <name>SoLF</name>
    <message>
        <source>Breaked copying</source>
        <translation>Przerwano kopiowanie</translation>
    </message>
    <message>
        <source>bytes have been copied.</source>
        <translation>bajtów skopiowano.</translation>
    </message>
    <message>
        <source>Remove this file</source>
        <translation>Usuń ten plik</translation>
    </message>
</context>
<context>
    <name>Tar</name>
    <message>
        <source>Could not start</source>
        <translation>Nie można uruchomić</translation>
    </message>
</context>
<context>
    <name>UrlsListManager</name>
    <message>
        <source>URLs List Manager</source>
        <translation>Menadżer listy URLi</translation>
    </message>
    <message>
        <source>&amp;New</source>
        <translation>&amp;Nowy</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation>Z&amp;mień nazwę</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>&amp;Usuń</translation>
    </message>
    <message>
        <source>&amp;Go to URL</source>
        <translation>Przejdź do &amp;URLa</translation>
    </message>
    <message>
        <source>Alt+G</source>
        <translation>Alt+G</translation>
    </message>
    <message>
        <source>Do&amp;wn</source>
        <translation>W &amp;dół</translation>
    </message>
    <message>
        <source>&amp;Up</source>
        <translation>W &amp;górę</translation>
    </message>
    <message>
        <source>P&amp;assword</source>
        <translation>&amp;Hasło</translation>
    </message>
    <message>
        <source>&amp;Port</source>
        <translation>P&amp;ort</translation>
    </message>
    <message>
        <source>&amp;Host</source>
        <translation>&amp;Serwer</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Zapisz</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <source>Favorites Manager</source>
        <translation>Menadżer ulubionych</translation>
    </message>
    <message>
        <source>History Manager</source>
        <translation>Menadżer historii</translation>
    </message>
    <message>
        <source>FTP Connections Manager</source>
        <translation>Menadżer połączeń FTP</translation>
    </message>
    <message>
        <source>New FTP session</source>
        <translation>Nowa sesja FTP</translation>
    </message>
    <message>
        <source>Please to give a name:</source>
        <translation>Proszę podać nazwę:</translation>
    </message>
    <message>
        <source>new name</source>
        <translation>nowa nazwa</translation>
    </message>
    <message>
        <source>Item</source>
        <translation>element</translation>
    </message>
    <message>
        <source>Rename FTP session</source>
        <translation>Zmień nazwę sesji FTP</translation>
    </message>
    <message>
        <source>Direct&amp;ory</source>
        <translation>&amp;Katalog</translation>
    </message>
    <message>
        <source>Us&amp;er</source>
        <translation>&amp;Użytkownik</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <source>Filters Manager</source>
        <translation>Menadżer filtrów</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>Za&amp;stosuj</translation>
    </message>
    <message>
        <source>&amp;Pattern :</source>
        <translation>&amp;Wzorzec :</translation>
    </message>
    <message>
        <source>Connec&amp;t</source>
        <translation>&amp;Połącz</translation>
    </message>
    <message>
        <source>A new filter</source>
        <translation>Nowy filtr</translation>
    </message>
    <message>
        <source>A new favorite</source>
        <translation>Nowy ulubiony</translation>
    </message>
    <message>
        <source>Rename a filter</source>
        <translation>Zmień nazwę filtrowi</translation>
    </message>
    <message>
        <source>Rename a favorite</source>
        <translation>Zmień nazwę ulubionemu</translation>
    </message>
</context>
<context>
    <name>VirtualFS</name>
    <message>
        <source>directory</source>
        <translation>katalog</translation>
    </message>
    <message>
        <source>file</source>
        <translation>plik</translation>
    </message>
    <message>
        <source>name:</source>
        <translation>nazwa:</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <source>This</source>
        <translation></translation>
    </message>
    <message>
        <source>already exists</source>
        <translation>już istnieje</translation>
    </message>
    <message>
        <source>Open it</source>
        <translation>Otwórz go</translation>
    </message>
    <message>
        <source>files selected</source>
        <translation>plików zaznaczonych</translation>
    </message>
    <message>
        <source>this files</source>
        <translation>te pliki</translation>
    </message>
    <message>
        <source>Remove files</source>
        <translation>Usuń pliki</translation>
    </message>
    <message>
        <source>Do you really want to delete</source>
        <translation>Czy na prawdę chcesz skasować</translation>
    </message>
    <message>
        <source>Make links</source>
        <translation>Tworzenie linków</translation>
    </message>
    <message>
        <source>Do you really want to make links for selected file(s)</source>
        <translation>Czy na prawdę chcesz utworzyć linki dla zaznaczonych plików</translation>
    </message>
    <message>
        <source>move</source>
        <translation>przenieś</translation>
    </message>
    <message>
        <source>copy</source>
        <translation>kopiuj</translation>
    </message>
    <message>
        <source>Source file :</source>
        <translation>Plik źródłowy :</translation>
    </message>
    <message>
        <source>Target file :</source>
        <translation>Plik docelowy :</translation>
    </message>
    <message>
        <source>Cannot</source>
        <translation>Nie można</translation>
    </message>
    <message>
        <source>to this same file</source>
        <translation>do tego samego pliku</translation>
    </message>
    <message>
        <source>copying</source>
        <translation>kopiowanie</translation>
    </message>
    <message>
        <source>removing</source>
        <translation>usuwanie</translation>
    </message>
    <message>
        <source>setting attributs</source>
        <translation>ustawianie atrybutów</translation>
    </message>
    <message>
        <source>Break</source>
        <translation>Przerwa</translation>
    </message>
    <message>
        <source>Do you really want to break an operation</source>
        <translation>Czy na prawdę chcesz przerwać operację</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Tak</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;Nie</translation>
    </message>
    <message>
        <source>&amp;All</source>
        <translation>&amp;Wszystkie</translation>
    </message>
    <message>
        <source>N&amp;one</source>
        <translation>&amp;Nic nie robić</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
    <message>
        <source>Delete directory</source>
        <translation>Usuwanie katalogu</translation>
    </message>
    <message>
        <source>Directory not empty</source>
        <translation>Katalog nie jest pusty</translation>
    </message>
    <message>
        <source>Delete it recursively ?</source>
        <translation>Usunąć go rekursywnie ?</translation>
    </message>
    <message>
        <source>new</source>
        <translation>nowy</translation>
    </message>
    <message>
        <source>Please to give a</source>
        <translation>Proszę podać</translation>
    </message>
</context>
</TS>
