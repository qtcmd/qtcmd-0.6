 /***************************************************************************
                          ftpfs.h  -  description
                             -------------------
    begin                : sat nov 30 2002
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

$Id$
 ***************************************************************************/

#ifndef _FTPFS_H_
#define _FTPFS_H_

#include "enums.h"
#include "virtualfs.h"

#include <qftp.h>
#include <qptrstack.h>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa klienta FTP.
 Obs�ugiwane s� tu wszystkie podstawowe operacje na plikach, takie jak:
 kopiowanie/przenoszenie usuwanie, tworzenie nowych pustych, tworzenie nowych
 katalog�w, zmiana atrybut�w. Podczas listowania rozr�niane s� linki do plik�w
 i katalog�w. Nie s� wspierane rekurencyjne operacje na plikach.
 */
class FtpFS : public VirtualFS
{
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param inputURL - nazwa bie��cej �cie�ki,
	 @param listViewExt - wska�nik na obiekt widoku listy plik�w.\n
	 Inicjowane s� tu sk�adowe klasy, tworzony obiekt klasy QFtp oraz ��czone
	 sygna�y ze slotami.
	 */
	FtpFS( const QString & inputURL, ListViewExt *listViewExt );

	/** Destruktor.\n
	 Usuwany jest tu obiekt uzywany w klasie (klasy QFtp).
	 */
	~FtpFS();

	/** Zwraca nazw� g��wnego katalogu.\n
	 @return nazwa g��wnego katalogu - 'ftp://nazwa_hosta/'.
	 */
	QString host() const { return QString("ftp://"+mHostName+"/"); }

	/** Zwraca bie��c� �cie�k� absolutn�.\n
	 @return bie��ca �cie�ka absolutna (bez nazwy g��wnego katalogu).
	 */
	QString path() const { return mRemoteDir; }

	/** Zwraca numer portu na kt�rym obs�ugiwany jest klient FTP.\n
	 @return numer portu.
	 */
	uint portNum() const { return mPortNum; }

	/** Zwraca nazw� (ze �cie�k� absolut�) aktualnie przetwarzanego pliku.
	 @return nazwa pliku ze �cie�� absolutn�.
	 */
	QString nameOfProcessedFile() const { return mNameOfProcessedFile; }

	/** Zwraca kod b��du dla ostatniej operacji plikowej.
	 @return kod b��du, patrz: @see Error.
	 */
	Error errorCode() const { return mErrorCode; }

	/** Uruchamia funkcj� pobieraj�c� wyniki operacji wa�enia, tj. waga ca�o�ci,
	 ilo�� plik�w i katalog�w.\n
	 @param weight - zmienna, w kt�rej zapisana b�dzie waga,
	 @param files - zmienna do zapisania ilo�ci plik�w,
	 @param dirs - zmienna do zapisania ilo�ci katalog�w.\n
	 Uwaga! Metoda nie ma definicji.
	 */
	void getWeighingStat( long long & weigh, uint & files, uint & dirs );

	/** Pobiera informacj� o pliku wcze�niej zainicjowan� w sk�adowej
	 @em mNewUrlInfo .\n
	 @param newUrlInfo - zmienna, w kt�rej nale�y umie�ci� informacj� o pliku.\n
	 Metoda jest wykorzystywana przez @em FilesPanel::slotResultOperation() ,
	 w celu zapewnienia odpowiedniej obs�ugi widoku listy.
	 */
	void getNewUrlInfo( UrlInfoExt & newUrlInfo ) { newUrlInfo = mNewUrlInfo; }

	/** Metoda zwraca pocz�tkow� pozycj� nazwy w listowanym elemencie.
	 @return indeks nazwy.
	 */
	int nameIndex( const QString & name );

	/** Funkcja zwraca status zachowania fragmentu kopiowanego lub przenoszonego pliku.
	@return TRUE jezeli zachowano czesc pliku, w przeciwnym razie FALSE.
	 */
	bool afterBreakKeepFragment() { return m_bAfterBreakKeepFragment; }

private:
	QFtp *mFtp;

	QString mHostName, mRemoteDir, mUserName, mPassword;
	QString mOldUserName, mOldPassword;
	QFtp::Command mLastFinishedCmd;
	Error mErrorCode;
	uint mPortNum, mOldPortNum;

	QString mNameOfProcessedFile, mTargetPath;
	QStringList mCopiedFilesList;

	uint mOpFileCounter, mTotalFiles;
	bool mMoveFiles;

	bool mPassiveModeOn;
	bool mUploading;
	bool mGettingURL;
	UrlInfoExt mNewUrlInfo;
	int mStat;

	bool mReadFile, mFileIsReadable, mSkipNextErrorOccures;
	QPtrList<QFile> mOpenedFilesPtrList;

	QStringList mMainFilesList, mTargetFileForLinkLst;
	uint mLinkCheckingCounter;
	bool mLinkChecking;

	bool m_bAfterBreakKeepFragment;

	uint mRealWritedBytes;

	QTimer *m_pTimer;

protected:
	//bool getFtpAttributs( const QString & inputURL );

	/** Metoda uruchamia procedur� po��czenia z podanym URLem.\n
	 @param inputURL - URL na kt�ry nale�y si� po��czy�,
	 @param error - kod b��du (wcze�niejszej operacji).
	 */
	void connectToHost( const QString & inputURL, QFtp::Error error=QFtp::NoError );

	/** Powoduje zmian� bie��cego katalogu na podany.\n
	 @param dirName - nowa (wzgl�dna) �cie�ka do katalogu.
	 */
	void changeDirOn( const QString & dirName );


	/** Zamyka bie��ce po��czenie je�li ono istnieje, lub jest wymuszane.\n
	 @param force TRUE, wymusza wykonanie roz��czenia z hostem, je�li FALSE
	 wtedy nast�pi roz��czenie tylko wtedy je�li po��czenie takie istnieje.
	 */
	void closeCurrentConnection( bool force=FALSE );

	/** Funkcja uruchamia procedur� przerwania bie��cej operacji.\n
	 UWAGA! Narazie wywo�ywany jest tu @see slotDone oraz ukrywany jest dialog
	 post�pu wykonywania operacji.
	 */
	void breakCurrentOperation();


	/** Sprawdza obecno�� pliku na serwerze wykonuj� operacj� STAT na jego rzecz.\n
	 @param fileName - nazwa pliku do otwarcia.\n
	 Uwaga: metod� nale�y zawsze wywo�ywa� przed otwarciem pliku (do podgl�du).
	 */
	void open( const QString & fileName );


	/** Je�li u�ytkownik jest zalogowany, wtedy podejmowana jest pr�ba zmiany
	 katalogu, w przeciwnym razie pokazywane jest okienko z pytaniem o ponowne
	 po��czenie.\n
	 @param dirName - nowy katalog do odczytania.\n
	 Odpowied� 'yes' w oknie dialogowym po��czenia uruchomi po��czenie, a je�li
	 ono zostanie zako�czone sukcesem wtedy nast�pi otwarcie wybranego katalogu.
	*/
	void readDir( const QString & dirName );

	/** Utworzenie nowego katalogu.\n
	 @param dirName - nazwa nowego katalogu.
	 */
	void mkdir( const QString & dirName );

	/** Metoda uruchamia procedur� utworzenia nowego pliku lub katalogu.\n
	 @param fullFileName - nazwa pliku lub katalogu do utworzenia.\n
	 Je�li nazwa ko�czy sie znakiem '/', wtedy wywo�ywane jest polecenie
	 tworzenia nowego katalogu, w przeciwnym razie tworzony jest pusty plik.
	*/
	void createAnEmptyFile( const QString & fileName );

	/** Uruchamiane jest tutaj kopiowanie lub przenoszenie podanych plik�w do
	 podanego katalogu (z FTP na LOCALfs i odwrotnie).\n
	 @param targetPath - miejsce docelowe kopiowania/przenoszenia,
	 @param selectedFilesList - lista plik�w do skopiowania/przeniesienia,
	 @param savePerm - TRUE, wymusza zachowanie atrybut�w kopiowanych plik�w
	 @param alwaysOvr - TRUE m�wi, �e pliki docelowe b�d� nadpisywane
	 @param followByLink - TRUE, wymusza kopiowanie dowi�za� link�w, FALSE
	  spowoduje kopiowanie link�w jako takich.
	 @param move - TRUE, oznacza �e pliki b�d� przenoszone, w przeciwnym razie
	  zostan� skopiowane.\n
	 Przy czym obs�ugiwane jest tylko kopiowanie plik�w (katalogi kopiowane s�
	 tylko jako puste).
	 */
	void copyFiles( QString & targetPath, const QStringList & selectedFilesList, bool savePerm, bool alwaysOvr, bool followByLink, bool move );

	/** Metoda powoduje podj�cie pr�by usuni�cia podanych plik�w.\n
	 @param filesList - lista plik�w do usuni�cia.
	 */
	void deleteFiles( const QStringList & filesList );


	/** Metoda uruchamia "surowe polecenie" ustawiania atrybut�w dla ka�dego pliku
	 z listy.\n
	 @param filesList - lista plik�w, kt�rym nale�y ustawi� atrybuty,
	 @param newUrlInfo - informacja o atrybutach dla plik�w,
	 @param recursive - r�wne TRUE, spowoduje �e atrybuty b�d� zmieniane dla ca�ej
	  zawarto�ci katalog�w z listy,
	 @param changesFor - rodzaj zmiany atrybut�w, dozwolone s�:
	  @li 0 - zmiana dla wszystkich,
	  @li 1 - zmiana tylko dla plik�w,
	  @li 2 - zmiana tylko dla katalog�w.\n
	 Wynik dzia�ania polecenia nale�y sprawdza� w @see slotRawCommandReply.\n
	 UWAGA! Zmiana atrybut�w jest narazie wykonywana tylko dla pierwszego pliku
	 z listy.
	 */
	void setAttributs( const QStringList & filesList, const UrlInfoExt & newUrlInfo, bool recursive, int changesFor );
//	void rename( const QString & oldName, const QString & newName ) { mFtp->rename( oldName, newName ); }

	/** Metoda uruchamia wa�enie podanych na li�cie plik�w.\n
	 @param filesList - lista plik�w do zwa�enia
	 @param realWeight - r�wne TRUE spowoduje, �e funkcja wa��ca uwzgl�dni
	  rzeczywist� wag� pliku w systemie plik�w.\n
	 UWAGA! Brak definicji.
	 */
	void sizeFiles( const QStringList & filesList, bool realWeigh );

	/** Funkcja wy�uskuje z podanego ci�gu poszczag�lne sk�adowe adresu.\n
	 @param path - nowa �cie�ka.\n
	 Nast�puje tu wyci�gni�cie s� nast�puj�ce sk�adowe adresu: nazwa hosta,
	 zdalny katalog, numer portu, u�ytkownik oraz has�o, po czym zainicjowanie
	 sk�adowych klasy.\n \n Przyk�ady mo�liwych �cie�ek wej�ciowych:\n
	  @li "ftp://user:password@hostname.com:21/subdir/"
	  @li "ftp://hostname.com/subdir/"
	*/
	void initSessionParameters( const QString & path );

	/** Zwraca informacj� o tym czy u�ytkownik jest aktualnie zalogowany.\n
	 @return TRUE oznacza, �e u�ytkownik jest zalogowany, FALSE - nie jest
	 zalogowany.
	 */
	bool isLoggedIn() { return (mFtp->state() == QFtp::LoggedIn); }


	/** Metoda wykonuje translacj� metody typu QFtpCommand na Operation.
	 @return polecenie typu @em Operation .\n
	 Przy czym polecenie QFtpCommand zawiera sk�adowa @em mLastFinishedCmd .
	 */
	Operation currentOperation() const;

	/** Powoduje pokazanie okna dialogowego z pytaniem o ponowne po��czenie
	 z hostem.\n
	 @return TRUE w przypadku udanego po��czenia, w przeciwnym razie FALSE.\n
	 Je�li wybrane zostanie 'Yes', wtedy nast�puje uruchomienie po��czenia.
	 */
	bool connectAgain();


	/** Zwraca nazw� u�ytkownika, kt�ry jest aktualnie zalogowany.\n
	 @return nazwa u�ytkownika
	 */
	QString loggedOwner() { return mUserName; }


	/** Uruchamia polecenie usuni�cia podanego pliku z lokalnego systemu plik�w.\n
	 @param fileName - nazwa pliku do usuni�cia.
	 @return TRUE, je�li operacja si� powiedzie, w przeciwnym razie FALSE.\n
	 Je�li pliku nie uda si� usun�� pokazywane jest okno informacyjne.
	 */
	bool removeFileFromLocalFS( const QString & fileName );

	/** Metoda uruchamia "surowe polecenie" sprawdzenia, ka�dego pliku docelowego
	 dla link�w z listy zebranej podczas listowania plik�w.
	 ( @em mTargetFileForLinkLst ).\n
	 Wynik dzia�ania polecenia nale�y sprawdza� w @see slotRawCommandReply.
	 */
	void checkAllLinks();

public slots:
	/** Slot powoduje zainicjowanie podanego obiektu z informacj� o pliku
	 atrybutami podanego.\n
	 @param fullFileName - nazwa pliku, kt�remu nale�y pobra� atrybuty,
	 @param urlInfoExt - adres obiektu, kt�ry nale�y zainicjowa� atrybutami.\n
	 Przy czym pobierane s� tylko nast�puj�ce atrybuty: nazwa, rozmiar oraz czas
	 ostatniej modyfikacji podanego pliku.
	 */
	void slotGetUrlInfo( const QString & fullFileName, UrlInfoExt & urlInfoExt );

	/** Slot powoduje wczytywanie pliku wybranego do podgl�du.\n
	 @param fileName - nazwa pliku do wczytania,
	 @param buffer - adres bufora, w kt�rym nale�y umieszcza� wczytywane dane,
	 @param bytesRead - ilo�� wczytywanych na raz bajt�w, a po odczycie zawiera
	  ilo�� faktycznie wczytanych bajt�w (0 oznacza koniec pliku)
	 @param fileLoadMethod - metoda wczytywania pliku, patrz @see View::UpdateMode
	 */
	void slotReadFileToView( const QString & fileName, QByteArray & buffer, int & bytesRead, int fileLoadMethod );

private slots:
	/** Pokazywane sa tutaj bledy dla ostatnio wykonywanej czynnosci.
	 @param finishedWithError - TRUE, oznacza �e operacja zako�czy�a si� b��dem
	  (do odczytania przez @em errorCode() ), FALSE mowi �e wszystko posz�o OK.
	*/
	void slotDone( bool finishedWithError );

	/** Slot powoduje wy�wietlenie informacje o stanie bie��cej operacji na pasku
	 statusu.\n
	 @param state - status operacji (typu QFtp::State).\n
	 Wysy�any jest tutaj tylko sygna� @em signalSetOperation() .
	 */
	void slotStateChanged( int state );

	/** Slot wywo�ywany w momencie rozpocz�cia wykonywania polecenia.\n
	 @param cmd - kod polecenia (typu QFtp::Command).\n
	 Wykonywane s� tu czynno�ci przygotowuj�ce, niezb�dne dla danego polecenia.
	 */
	void slotCommandStarted( int command );

	/** Slot wywo�ywany w chwili zako�czenia wykonywania podanego polecenia.\n
	 @param cmd - kod polecenia ( typu @em Operation ),
	 @param error - r�wne TRUE oznacza, �e pojawi� si� b��d w wykonanym poleceniu.\n
	 Je�li pr�ba wykonania polecenia zako�czy�a si� b��dem pokazywany jest
	 komunikat z informacj� o b��dzie i nazwy pliku lub katalogu, kt�rego b��d
	 dotyczny.
	 */
	void slotCommandFinished( int command, bool error );

	/** W slocie parsowane s� polecenia uruchaniane "r�cznie" na ho�cie FTP.\n
	 @param code - kod polecenia
	 @param text - wynik wykonania polecenia
	 */
	void slotRawCommandReply( int code, const QString & text );

	/**
	W slocie wysy�ane s� sygna�y z informacjami post�pie danej operacji.\n
	S� to:\n
	 @see signalFileCounterProgress()\n
	 @see signalTotalProgress()\n
	 @see signalDataTransferProgress().
	*/
	void slotSetProgress( int done, int total );


	/** Slot wywo�ywany przez obiekt klasy FileView, po wykonaniu zapisu do pliku.\n
	 @param fileName - nazwa zapisywanego pliku.
	 */
	void slotUpdateItemOnLV( const QString & fileName );

	void slotQFtpListInfo( const QUrlInfo & ui );

	/** Slot wywo�ywany po up�yni�ciu okre�lonego czasu.
	 * U�ywany do dzia�ania trybu podtrzymywania po��czenia.
	*/
	void slotTimeOut();

signals:
	/** Sygna� wymusza pokazanie dialogu nadpisywania pliku.\n
	 @param sourceFileName - nazwa �r�d�owego pliku,
	 @param targetFileName - nazwa docelowego pliku,
	 @param result - zmienna zawiera kod odpowiedzi u�ytkownika.
	 */
	void signalFileOverwrite( const QString & sourceFileName, QString & targetFileName, int & result );

	/** Sygna� zawiera informacj� o aktualnie listowanym elemencie.
	 @param urlInfoExt - informacja o pliku.\n
	 Wysy�any jest podczas listowania katalogu.
	 */
	void listInfo( const UrlInfoExt & urlInfoExt );

};

#endif
