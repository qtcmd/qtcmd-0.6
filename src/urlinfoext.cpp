/***************************************************************************
                          urlinfoext.cpp  -  description
                             -------------------
    begin                : tue oct 14 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "urlinfoext.h"

#include <qfileinfo.h>
#include <sys/stat.h> // for S_ISUID, S_ISGID, S_ISVTX


UrlInfoExt::UrlInfoExt( const QString & name, long long size, int permissions, const QString & owner, const QString & group, const QDateTime & lastModified, const QDateTime & lastRead, bool dir, bool file, bool symLink, bool readable, bool executable, bool empty, const QString & linkTarget )
 : QUrlInfo(name, permissions, owner, group, (uint)size, lastModified, lastRead, dir, file, symLink, FALSE, readable, executable),
   mSize(size), mEmptyFile(empty), mLastRead(lastRead), mLinkTargetFileStr(linkTarget)
{  }


QString UrlInfoExt::permissionsStr() const
{
	int perm = permissions();
	QString permStr = "---------";

	int permTab[9] = {
		QFileInfo::ReadUser,  QFileInfo::WriteUser,  QFileInfo::ExeUser,
		QFileInfo::ReadGroup, QFileInfo::WriteGroup, QFileInfo::ExeGroup,
		QFileInfo::ReadOther, QFileInfo::WriteOther, QFileInfo::ExeOther
	};
	char permChrTab[3] = { 'r','w','x' };

	for (int i=0; i<9; i++) {
		if ( perm & permTab[i] )
			permStr[i] = permChrTab[i%3];
	}

	if ( (perm & S_ISUID) )  permStr[2] = (perm & QFileInfo::ExeUser)  ? 's' : 'S';
	if ( (perm & S_ISGID) )  permStr[5] = (perm & QFileInfo::ExeGroup) ? 's' : 'S';
	if ( (perm & S_ISVTX) )  permStr[8] = (perm & QFileInfo::ExeOther) ? 't' : 'T';

	return permStr;
}


void UrlInfoExt::setPermissionsStr( const QString & permStr )
{
	int perm = 0;

	if ( permStr.at(0) == 'r' )    perm += QFileInfo::ReadUser;
	if ( permStr.at(3) == 'r' )    perm += QFileInfo::ReadGroup;
	if ( permStr.at(6) == 'r' )    perm += QFileInfo::ReadOther;
	if ( permStr.at(1) == 'w' )    perm += QFileInfo::WriteUser;
	if ( permStr.at(4) == 'w' )    perm += QFileInfo::WriteGroup;
	if ( permStr.at(7) == 'w' )    perm += QFileInfo::WriteOther;
	if ( permStr.at(2) == 'x' || permStr.at(2) == 's' )  perm += QFileInfo::ExeUser;
	if ( permStr.at(5) == 'x' || permStr.at(5) == 's' )  perm += QFileInfo::ExeGroup;
	if ( permStr.at(8) == 'x' || permStr.at(8) == 't' )  perm += QFileInfo::ExeOther;

	if ( permStr.at(2).lower() == 's' )  perm += S_ISUID;
	if ( permStr.at(5).lower() == 's' )  perm += S_ISGID;
	if ( permStr.at(8).lower() == 't' )  perm += S_ISVTX;

	QUrlInfo::setPermissions( perm );
}


void UrlInfoExt::clear()
{
	setName("");
	setSize(-1);
	setOwner("");
	setGroup("");
	QDateTime nullDT;
	setLastModified(nullDT);
	setLastRead(nullDT); // lack in QUrlInfo
	setPermissions(-1);

	setDir(FALSE);
	setFile(FALSE);
	setSymLink(FALSE);
	setReadable(FALSE);
	setWritable(FALSE);
	mEmptyFile = FALSE;
}
