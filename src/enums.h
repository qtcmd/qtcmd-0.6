
#ifndef ENUMS_H
#define ENUMS_H

#include <qdir.h>

enum KindOfPanel       { FILESpanel, FILEVIEWpanel, UNKNOWNpanel };
enum KindOfListView    { LISTpview,  TREEpview, UNKNOWNpview };
enum KindOfFilesSystem { LOCALfs, ARCHIVEfs, FTPfs, SAMBAfs, NETWORKfs, UNKNOWNfs };

enum DirWritable { ENABLEdw, DISABLEdw, UNKNOWNdw };

enum KindOfFormating { NONEformat=0, THREEDIGITSformat, BKBMBGBformat };

enum KindOfArchive { ARJarch=0, RARarch, ZIParch, LZHarch, ZOOarch,  TARarch, TBZIP2arch, TGZIParch, TZarch, TLZMAarch, CPIOarch, CGZIParch, CZarch,  UNKNOWNarch,  COMPRESScompr, BZIP2compr, GZIPcompr, LZMAcompr,  UNKNOWNcompr };
enum KindOfPackage { DEBpkg, RPMpkg, SLACKWAREpkg, UNKNOWNpkg };

enum KindOfFile    { SOURCEfile, RENDERfile, IMAGEfile, SOUNDfile, ARCHIVEfile, VIDEOfile, UNKNOWNfile };
enum KindOfViewer  { INTERNALviewer=0, EXTERNALviewer, UNKNOWNviewer };

enum Operation     { NoneOp, HostLookup, Connect, List, Open, Cd, Rename, Remove, Touch, MakeDir, MakeLink, Get, Put, Copy, Move, Weigh, SetAttributs, Find };
enum Error         { NoError, UnKnown, NotExists, AlreadyExists, ReadError, CannotRead, CannotWrite, CannotRemove, CannotSetAttributs, CannotCreateLink, IllegalName, NoFreeSpace, BreakOperation, OpNotPermitted, CannotRunProc };
enum KindOfError   { Exists, Readable, Writable };

#endif
