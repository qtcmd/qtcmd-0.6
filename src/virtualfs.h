/***************************************************************************
                          virtualfs.h  -  description
                             -------------------
    begin                : mon oct 28 2002
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _VIRTUALFS_H_
#define _VIRTUALFS_H_

//#include "filesassociation.h" // included in "findfiledialog.h"
#include "propertiesdialog.h"
#include "progressdialog.h"
#include "findfiledialog.h"
#include "findcriterion.h"
#include "listviewext.h"
#include "urlinfoext.h"
#include "enums.h"

#include <qurloperator.h>
#include <qvaluelist.h>
#include <qprocess.h>
#include <qwidget.h>

static bool sCloseProgressDlgAfterFinished;

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/**
 @short Klasa bazowa systemu plik�w, po kt�rej dziedzicz� wszystkie FileSystemy.
 Spe�nia ona wiele zada� po�rednich (poprzez metody wirtualne) przy uruchamianiu
 operacji plikowych, jest najwy�sz� warstw� pomi�dzy UI a systemem plik�w.
 Wykonywane s� tutaj wst�pne czynno�ci przed operacjami plikowymi, np. zbieranie
 nazw plik�w na list�, tworzenie, pokazywanie i chowanie dialog�w oraz czynno�ci
 inicjalizacyjne dla widoku listy (konieczne do wykonanie niekt�rych operacji).
 Wykonywane jest tutaj te� wst�pne sprawdzanie b��d�w. Znajduje si� tutaj r�wnie�
 obs�uga dekodowania po tzw. szybkiej zmianie niekt�rych atrybut�w pliku, np.
 praw dost�pu, czy czasu modyfikacji, podawanej wprost przez u�ytkownika. Obiekt
 tej klasy przechowuje �cie�k� do aktualnie otwartego katalogu w danym systemie
 plik�w. Zawiera r�wnie� metody wykrywaj�ce rodzaj systemu plik�w na podstawie
 podanej �cie�ki.
*/
class VirtualFS : public QWidget
{
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param inputURL - nazwa katalogu kt�ry b�dzie otwarty w bie��cym systemie
	 plik�w. Tutaj jest wykorzystywany do wykrywania bie��cego systemu plik�w,
	 @param listViewExt - wska�nik na widok listy plik�w.\n
	 Inicjowane s� tu sk�adowe klasy i wczytywane ustawienia systemu plik�w z pliku
	 konfiguracyjnego.
	 */
	VirtualFS( const QString & inputURL, ListViewExt * listViewExt );

	/** Destruktor.\n
	 Usuwane s� tutaj nast�puj�ce obiekty: dialog post�pu operacji, dialog
	 wyszukiwania plik�w i dialog w�a�ciwo�ci pliku.
	 */
	virtual ~VirtualFS();

	/** Zwraca nazw� (ze �cie�k� absolut�) aktualnie przetwarzanego pliku.
	 @return nazwa aktualnie przetwarzanego pliku.
	 */
	virtual QString nameOfProcessedFile() const { return QString::null; }

	/** Zwraca kod b��du dla ostatniej operacji plikowej zgodny z @em Error .
	 @return kod b��du.
	 */
	virtual Error errorCode()  const { return NoError; }

	/** Zwraca nazw� g��wnego katalogu.\n
	 @return nazwa g��wnego katalogu.
	 */
	virtual QString host()     const { return QString::null; }

	/** Zwraca bie��c� �cie�k� absolutn�.\n
	 @return bie��ca �cie�ka absolutna.
	 */
	virtual QString path()     const { return QString::null; }

	/** Zwraca numer portu na kt�rym obs�ugiwany jest systemu plik�w.\n
	 @return numer portu.\n
	 U�ywane tylko w systemie FtpFS.
	 */
	virtual    uint port()     const { return 0; }


	/** Metoda uruchamia procedur� tworzenia nowego katalogu lub struktury
	 katalog�w o podanej nazwie.
	 */
	virtual void mkdir( const QString & )  {}

	/** Wywo�anie funkcji pobieraj�cej informacje o systemie plik�w dla podanego
	 katalogu.
	 */
	virtual bool getStatFS( const QString & , long long & , long long & ) { return FALSE; }


	/** Metoda uruchamia wa�enie podanych na li�cie plik�w.
	 */
	virtual void sizeFiles( const QStringList &, bool ) {}

	/** Wywo�anie funkcji pobieraj�cej wyniki operacji wa�enia, tj. waga ca�o�ci,
	 ilo�� plik�w i katalog�w.
	 */
	virtual void getWeighingStat( long long & , uint & , uint & ) {}

	/** Zwraca informacj� o tym czy bie��cy katalog ma prawa zapisu dla aktualnie
	 zalogowanego u�ytkownika.
	 */
	virtual DirWritable currentDirWritable() { return UNKNOWNdw; }


	/** Pobiera informacj� o pliku wcze�niej zainicjowan� w sk�adowej klasy
	 zawieraj�cej informacje o pliku.
	 */
	virtual void getNewUrlInfo( UrlInfoExt & ) {}

	/** Uruchamia ponowne odczytanie bie��cego katalogu.\n
	 Wywo�ywana jest tu metoda z klasy widoku listy plik�w.
	 */
	void rereadCurrentDir();

	/** Metoda uruchamia procedur� tworzenia pustego pliku lub katalogu.\n
	 @param createDir - warto�� TRUE wymusza utworzenie katalogu, w przeciwnym
	 razie tworzony jest pusty plik (warto�� domy�lna to FALSE).\n
	 Pokazywany jest tutaj te� dialog wst�pny dla bie��cej operacji.
	 */
	void createNewEmptyFile( bool createDir=FALSE );

	/** Inicjuje otwarcie zaznaczonych plik�w.\n
	 @param fileName - nazwa pliku do otwarcia,
	 @param editMode - warto�� TRUE wymusza otwarcie bie��cego pliku w trybie
	  edycji.\n
	 Nazwy plik�w do otwarcia zbierane s� na list�, je�li dany plik nie mo�na,
	 odczyta� wtedy pokazywany jest komunikat b��du. Zebrane pliki otwierane s� w
	 wewn�trznej lub zewn�trznej przegl�darce w zale�no�ci od ustawienia w bazie
	 skoja�e� danego pliku.
	 */
	void openFile( const QString & fileName, bool editMode=FALSE );

	/** Metoda uruchamia procedur� kopiowania lub przenoszenia plik�w.\n
	 @param targetPath - �cie�ka docelowa kopiowania lub przenoszenia
	 @param filesList - lista nazw plik�w do skopiowania lub przenoszenia
	 @param removeSrc - r�wne TRUE, wymusza usuni�cie plik�w �r�d�owych po ich
	 skopiowaniu (operacja przenoszenia), w przeciwnym razie pliki s� tylko
	 kopiowane.\n
	 Pokazywany jest r�wnie� wst�pny dialog dla tej operacji, a je�li kopiowany
	 lub przenoszony jest jeden plik to nast�puje sprawdzenie jest czy ma tak�
	 sam� nazw� jak docelowy, je�li tak to pokazywany jest komunikt ostrzegawczy
	 i nast�puje powr�t do dialogu wst�pnego operacji.
	 */
	bool copyFilesTo( const QString & targetPath, const QStringList & filesList, bool remove=FALSE );

	/** Metoda uruchamia procedur� usuwania plik�w z aktualnego systemu plik�w.\n
	 Zbierane s� tutaj na list� nazwy plik�w do usuni�cia, a przed uruchomieniem
	 operacji pokazywany jest dialog z pytaniem o potwierdzenia usuwania.
	 */
	void removeFiles();

	/** Metoda uruchamia procedur� tworzenia lub edycji linku.\n
	 @param targetPath - katalog docelowy dla linku
	 @param editOnly - r�wne TRUE, oznacza �e b�dzie to operacja edycji linku,
	  w przeciwnym wypadku wykonywane jest tworzenie.\n
	 Zbierane s� na list� nazwy plik�w, dla kt�rych nale�y wykona� linki.
	 Pokazywany jest r�wnie� wst�pny dialog dla tej operacji.
	 */
	void makeLink( const QString & targetPath, bool editOnly );

	/** Metoda uruchamia procedur� zmiany atrybutu (w podanej kolumnie) dla
	 bie��cego pliku.\n
	 @param column - kolumna w kt�rej nale�y zmieni� atrybut pliku,
	 @param newAttribStr - ci�g opisuj�cy now� warto�� atrybutu.
	 */
	void changeAttribut( ListViewExt::ColumnName column, const QString & newAttribTxt );

	/** Zwraca docelow� nazw� pliku, kt�remu zmieniono nazw�,
	 */
	QString newAttributName() { return mTargetURL; }

	/** Funkcja powoduje pokazanie dialogu w�a�ciwo�ci dla bie��cego pliku.
	 */
	void showPropertiesDialog();

	/** Funkcja powoduje pokazanie dialogu wyszukiwania pliku.
	 */
	void showFindFileDialog();


	/** Funkcja zajmuje si� zainicjowaniem i pokazaniem dialogu post�pu dla podanej
	 operacji.\n
	 @param operation - aktualnie wykonywana operacja
	 @param move - je�li r�wne TRUE, a operacj� jest kopiowanie, wtedy ustawia
	 @param targetPath - �cie�ka docelowa dla bie��cej operacji (niezb�dne dla
	  kopiowania lub przenoszenia).
	 */
	void showProgressDialog( Operation operation, bool move=FALSE, const QString & targetPath=QString::null );

	/** Funkcja powoduje usuni�cie dialogu post�pu.\n
	 @param checkWhetherCanToClose - r�wne TRUE, wymusza sprawdzenie informacji o
	  tym czy u�ytkownik wybra� zamkni�cie na dialogu post�pu.
	 */
	void removeProgressDlg( bool checkWhetherCanToClose=FALSE );


	/** Na podstawie podanej nazwy wykrywa rodzaj docelowego systemu plik�w.\n
	 @param inputURL - nazwa pliku lub katalogu.\n
	 Je�li jest on r��ny od bie��cego, wtedy zwraca URL, w przeciwnym razie jest
	 to warto�� pusta.\nDodatkowo, je�eli bie��cy i docelowy system plik�w jest
	 taki sam, wtedy por�wnywana jest �cie�ka. Je�li jest r��na od bie��cej,
	 wtedy nast�puje pr�ba otwarcia katalogu (lub archiwum).
	 */
	bool needToChangeFS( QString & inputURL );

	/** Zwraca nazw� (pliku lub katalogu) z absolutn� �cie�k� dost�pu bie��cego
	 FS-u dla podanego pliku lub katalogu.\n
	 @param fileName - nazwa pliku lub katalogu,
	 @return nazwa z absolutn� �cie�k� dla bie��cego systemu plik�w.
	*/
	QString absoluteFileName( const QString & fileName ) const;

	/** Zwraca ci�g b�d�cy nazw� katalogu root aktualnego systemu plik�w.\n
	 @return katalog korzenia dla bie��cego systemu plik�w.\n
	 Przyk�adowo dla lokalnego systemu plik�w zwracane jest: "/", natomiast dla
	 FTP - "ftp://", itp. Rodzaj FileSystemu zawarty jest w sk�adowej
	 @em mCurrentFilesSystem .
	 */
	QString rootName() const;

	/** Funkcja inicjuje sk�adow� @em mCurrentURL podan� warto�ci� parametru.\n
	 @param fullPathName - nazwa, kt�r� nale�y zainicjowa� bie��c� �cie�k�.
	 */
	void setCurrentURL( const QString & fullPathName );

	/** Zwraca nazw� poprzedniego katalogu widocznego na li�cie.
	 */
	QString previousDirName() const { return mPreviousDirName; }

	/** Zwraca absolutn� �cie�k� do bie��cego katalogu.
	 */
	QString currentURL() const { return mCurrentURL; }

	/** Zwraca absolutn� nazw� docelowego pliku w operacji zmiany nazwy.
	 */
	QString targetURL() const { return mTargetURL; }

	/** Zwraca absolutn� bie��c� �cie�k� dla aktualnego systemu plik�w.\n
	 @return absolutna �cie�ka.\n
	 Przy czym dla archiwum zwracana jest tu tylko jego nazwa.
	 */
	QString absHostURL() const;


	/** Metoda zwraca informacj�, czy lista plik�w zawiera focus.\n
	 @return TRUE je�li lista zawiera focus, FALSE je�li go nie zawiera.
	 */
	bool hasFocus() const { return mListViewExt->hasFocus(); }

	/** Funkcja zwraca rodzaj systemu plik�w, na podstawie podanej nazwy.\n
	 @param fullFileName - �cie�ka do katalogu,
	 @return rodzaj systemu plik�w, patrz: @see KindOfFilesSystem.
	 */
	static KindOfFilesSystem kindOfFilesSystem( const QString & fullFileName );

	/** Metoda zwraca rodzaj bie��cego systemu plik�w.\n
	 @return rodzaj systemu plik�w, patrz: @see KindOfFilesSystem.
	 */
	KindOfFilesSystem kindOfCurrentFS() const { return mCurrentFilesSystem; }

	/** Metoda zwraca rodzaj widoku listy w panelu.
	 @return rodzaj widoku listy, patrz: @see KindOfListView.
	 */
	KindOfListView kindOfViewFilesPanel() const { return mListViewExt->kindOfView(); }


	/** Funkcja powoduje przygotowanie obiektu klasy ListViewExt w celu pokazania
	 na nim nowej listy element�w.
	 */
	void initializeShowsList();


	/** Metoda wykonuje ewentualn� korekcj� podanej �cie�ki.\n
	 @param path - �cie�ka do poprawienia,
	 @param alwaysAddSlash - r�wne TRUE, wymusza dodanie znaku slash ('/') na
	 ko�cu �cie�ki.\n
	 Zamieniane s� tutaj na w�a�ciw� �cie�k� ci�gi znak�w '../' i './' oraz znaki:
	 '~' (katalog domowy u�ytkownika), '.' (katalog bie��cy). Ci�gi slashy ('//')
	 zamieniane s� na pojedyncze znaki '/'. Je�li podana �cie�ka nie jest
	 absolutna, wtedy jest uzupe�niana bie��c�.
	 */
	void parsePath( QString & path, bool alwaysAddSlash=TRUE );


	/** Funkcja zwraca nazw� podanego kodu operacji na plikach.\n
	 @param operation - kod operacji.
	 @return nazwa operacji.
	 */
	static QString cmdString( Operation operation ); // TYLKO DLA TESTOW


	/** Metoda powoduje ustawienie wagi pliku(�w) na dialogu w�a�ciwo�ci.\n
	 Waga jest pobierana z bie��cego systemu plik�w za pomoc� wirtualnej funkcji.
	 */
	bool setWeighInPropetiesDlg();

	/** Metoda ustawia status zako�czenia operacji.\n
	 @param operationHasFinished - TRUE, oznacza �e operacja zosta�a zako�czona,
	 FALSE m�wi �e operacja nie jest zako�czona.\n
	 Metoda jest wywo�ywana w slocie 'slotDone' klasy aktualnego systemu plik�w.
	 */
	void setOperationHasFinished( bool operationHasFinished ) { mOperationHasFinished = operationHasFinished; }

	/** Metoda wykonuje inicjalizacj� na dialogu w�a�ciwo�ci pliku podan�
	 informacj� o pliku.\n
	 @param urlInfo - adres obiektu z informacj� o pliku,
	 @param loggedUserName - nazwa aktualnie zalogowanego u�ytkownika.
	 */
	void initPropetiesDlgByUrl( const UrlInfoExt & , const QString & loggedUserName );


	/** Metoda powoduje usuni�cie (aktualnie przetwarzanego pliku/katalogu)
	 z listy zaznaczonych.
	 */
	void removeItemFromSIL();


	/** Metoda tworzy obiekt podgl�du (klasy FileView) oraz ��czy sygna�y ze
	 slotami.
	 */
	void createFileView();

	/** Ustawia wska�nik do obiektu skoja�e� plik�w.\n
	 @param fa - wska�nik do obiektu skoja�e� plik�w.
	 */
	void setFilesAssociation( FilesAssociation *fa ) { mFilesAssociation = fa; }



	/** W��cza b�d� wy��cza bezpieczne usuwnie plik�w.
	 */
	virtual void setRemoveToTrash( bool ) {}

	/** W��cza albo wy��cza czynno�� wa�enia plik�w i katalog�w przed operacj�
	 plikow� (np. kopiowania).
	 */
	virtual void setWeighBeforeOperation( bool ) {}

	/** W��cza b�d� wy��cza bezpieczne usuwnie plik�w.
	 */
	virtual void setTrashPath( const QString & ) {}

	/** Ustawia �cie�k� do katalogu roboczego.\n
	 U�ywane tylko w systemie ArchiveFS.
	 */
	virtual void setWorkDirectoryPath( const QString & ) {}

	/** W��cza albo wy��cza utrzymywanie po��czenia ze zdalnym hostem.\n
	 U�ywane tylko w systemie FtpFS.
	 */
	virtual void setStandByConnection( bool ) {}

	/** Ustawia ilo�� powtarzania wykonania operacji po��czanie, gdy zdalny host
	 nie odpowiada.\n
	 U�ywane tylko w systemach FtpFS i SambaFS.
	 */
	virtual void setNumOfTimeToRetryIfServerBusy( uint ) {}


	/** Funkcja zwraca status zachowania fragmentu kopiowanego lub przenoszonego pliku.
	@return TRUE jezli zachowano czesc pliku, w przeciwnym razie FALSE.
	 */
	virtual bool afterBreakKeepFragment() { return FALSE; }


	/** Ustawia status nadpisywania plik�w w przypadku operacji kopiowania
	 i przenoszenia.\n
	 @param alwaysOverwite - r�wne TRUE zaznacza na dialogu, opcje nadpisywania,
	 dla FALSE opcja ta nie jest zaznaczana.\n
	 Ustawienie to jest u�ywane przez dialog kopiowania.
	 */
	void setAlwaysOverwrite( bool alwaysOverwite ) { mAlwaysOverwrite = alwaysOverwite; }

	/** Ustawia status zachowywania praw dost�pu w przypadku operacji kopiowania
	 i przenoszenia.\n
	 @param savePerm - r�wne TRUE zaznacza na dialogu, opcje zachowania praw,
	 dla FALSE opcja ta nie jest zaznaczana.\n
	 Ustawienie to jest u�ywane przez dialog kopiowania. Domy�lnie w�a�ciwo�� ta
	 jest w��czona.
	 */
	void setAlwaysSavePermission( bool savePerm ) { mSavePermision = savePerm; }

	/** W��cza lub wy��cza pokazywanie okienka ostrzegawczego przed operacj�
	 usuwania plik�w.\n
	 @param askBeforeDelete - r�wne TRUE, wymusza pokazanie dialogu, dla FALSE
	  okienko nie zostanie pokazane.\n
	 Domy�lnie w�a�ciwo�� ta jest w��czona.
	 */
	void setAlwaysAskBeforeDeleting( bool askBeforeDelete ) { mAskBeforeDelete = askBeforeDelete; }

	/** W��cza lub wy��cza operacj� wej�cia do katalogu po jego utworzeniu.\n
	 @param getInToDir - r�wne TRUE, wymusza wej�cie do katalogu, dla FALSE
	 ono nie nast�pi.\n
	 Domy�lnie w�a�ciwo�� ta jest wy��czona.
	 */
	void setAlwaysGetInToDirectory( bool getInToDir ) { mGetInToDir = getInToDir; }

	/** W��cza/wy��cza chowanie dialogu post�pu po zako�czeniu operacji.\n
	 @param closeAfterFinished - r�wne TRUE, oznacza zamkni�cie dialogu,
	 w przypadku FALSE nie jest on zamykany.\n
	 Domy�lnie w�a�ciwo�� ta jest wy��czona.
	 */
	void setCloseProgressDlgAfterFinished( bool closeAfterFinished ) { sCloseProgressDlgAfterFinished = closeAfterFinished; }

private:
	ListViewExt *mListViewExt;
	ProgressDialog *mProgressDlg;
	PropertiesDialog *mPropertiesDialog;
	FindFileDialog *mFindFileDialog;
	FilesAssociation *mFilesAssociation;

	QString mTargetURL;
	QString mCurrentURL, mNewURL;
	QString mPreviousDirName;

	KindOfFilesSystem mCurrentFilesSystem;

	QStringList mSelectedFilesList;
	bool mSavePermision, mAlwaysOverwrite, mHardLink, mAskBeforeDelete, mGetInToDir;
	bool mOperationHasFinished;

	int mItemIdForOpenedFile;
	bool mOpenedFileEditMode;

	QProcess mProcess;


	/** Metoda powoduje uruchomienie zewn�trznych program�w dla podgl�du ka�dego
	z podanej listy plik�w.
	@em filesToView - lista nazw plik�w do podgl�du.\n
	Nazwy program�w - przegl�darek pobierane s� z bazy skojarze� plik�w.
	*/
	void runExternalViewer( const QStringList & filesToView );

protected:
	/** Metoda s�u��ca do otwierania podanego pliku do podgl�du.
	 */
	virtual void open( const QString & ) {}

	/** Metoda sprawdza czy podany plik istnieje i czy mo�na go odczytywa�.
	 */
	virtual bool fileIsReadable( const QString & , bool ) { return TRUE; }

	/** Metoda uruchamia listowanie plik�w z podanego katalogu.
	 */
	virtual void readDir( const QString & )  {}

	/** Zwraca liczb� wszystkich plik�w i katalog�w w bie��cym katalogu.\n
	 @return suma plik�w i katalog�w z bie��cego.
	 */
	virtual uint numOfAllFiles()  { return 0; }

	/** Metoda uruchamia procedur� utworzenia nowego pliku lub katalogu.
	 */
	virtual void createAnEmptyFile( const QString & ) {}

	/** Uruchamia kopiowanie lub przenoszenie plik�w z podanej listy do podanego
	 katalogu.
	 */
	virtual void copyFiles( QString & , const QStringList & , bool , bool , bool , bool ) {}

	/** Metoda uruchamia usuwanie plik�w z podanej listy.
	 */
	virtual void deleteFiles( const QStringList & ) {}

	/** Metoda uruchamia tworzenie linku lub grupy link�w, albo edycj�
	 pojedynczego.
	 */
	virtual void makeLink( const QStringList & , const QStringList & , bool , bool , bool ) {}

	/** Metoda uruchamia ustawianie atrybut�w dla podanych na li�cie plik�w.
	 */
	virtual void setAttributs( const QStringList & , const UrlInfoExt & , bool , int ) {}

	/** Metoda wywo�ywana po uruchomieniu wyszukiwania na dialogu szukania pliku.
	 */
	virtual void startFindFile( const FindCriterion & ) {}

	/** Zwraca kod bie��cej operacji, patrz: @see Operation.\n
	 @return kod bie��cej operacji.
	 */
	virtual Operation currentOperation() const { return NoneOp; }

	/** Funkcja uruchamie procedur� przerwania bie��cej operacji.
	 */
	virtual void breakCurrentOperation() {}


	/** Zwraca informacje o tym czy podany plik jest linkiem symbolicznym.\n
	 @return TRUE podany plik jest linkiem, FALSE - plik nie jest linkiem.
	 */
	virtual bool isLink( const QString & ) { return FALSE; }

	/** Zwraca �cie�k� docelow� dla podanego linku.\n
	 @return �cie�ka docelowa linku.
	 */
	virtual QString readLink( const QString & ) { return QString::null; }


	/** Zwraca nazw� aktualnie zalogowanego u�ytkownika.\n
	 @return nazwa u�ytkownika.
	 */
	virtual QString loggedOwner() { return QString::null; }

	/** Zwraca nazw� grupy dla aktualnie zalogowanego u�ytkownika.\n
	 @return nazwa grupy.
	 */
	virtual QString loggedGroup() { return QString::null; }


	/** Funkcja powoduje wystartowanie lub zatrzymanie stopera na dialogu post�pu.\n
	 @param start - r�wne TRUE nakazuje uruchomienie stopera, FALSE powoduje jego
	 zatrzymanie.
	 */
	void startProgressDlgTimer( bool start );

	/** Zwraca nazw� aktualnie wybranego pliku wraz ze �cie�k� absolutn�.\n
	 @return nazwa bie��cego pliku.
	 */
	QString currentFileName() { return mCurrentURL+mListViewExt->currentItemText(); }


	/** Zamienia prawa dost�pu podane w postaci ci�gu 'rwx' lub warto�ci
	 �semkowej, na warto�� typu 'int'.\n
	 @param permissions - adres zmiennej, w kt�rej nale�y umie�ci� wynik,
	 @param oldPermStr - pierwotny ci�g opisuj�cy prawa dost�pu,
	 @param newPermStr - nowy ci�g opisuj�cy prawa dost�pu.
	 */
	void decodePermissions( int & permissions, const QString & oldPermStr, const QString & newPermStr );

	/** Funkcja powoduje zdekodowanie ci�gu tekstowego 'data czas' podanego przez
	 u�ytkownika podczas tzw. szybkiej zmiany atrybutu.\n
	 @param dateTime - adres obiektu z informacj� o czasie i dacie, w kt�rym jest
	 zapisywana zdekodowana nowa warto�� daty i czasu,
	 @param oldTime - stara warto�� daty i czasu,
	 @param timeStr - nowa warto�� daty i czasu podana przez u�ytkownika.\n
	 Ci�g tekstowy podany jako @em timeStr powinien mie� nast�puj�cy format:
	 yyyy-mm-dd hh-mm-ss, przy czym nie wszystkie pola musz� tu wystapi� -
	 - konieczne jest co najmniej jedno z daty lub czasu.
	 */
	void decodeTime( QDateTime & time, const QDateTime & oldTime, const QString & newTimeStr );

public slots:
	/** Slot powoduje zmian� bie��cego katalogu o jeden poziom wy�ej.
	 */
	void slotCloseCurrentDir();

	/** Slot uruchamia procedure otwarcia katalogu, pliku zwyk�ego, lub archiwum.\n
	 @param url - nazwa pliku lub katalogu.\n
	 Je�li nast�pi pr�ba wyj�cia z najni�szego katalogu w systemie plik�w innym
	 ni� lokalny, wtedy wymuszane jest przej�cie do poprzedniego katalogu z
	 lokalnego systemu plik�w.\n \n Uwaga:\n
	 Je�li ostatni znak w podanej nazwie jest znakiem slash ('/'), wtedy nast�puje
	 pr�ba otwarcia katalogu o takiej nazwie. Jako nazw� mo�na r�wnie� poda�
	 nast�puj�ce skr�ty: '..', '~'.
	 */
	void slotOpen( const QString & url );


	/** Slot - pomocnik (po�rednik) listowania plik�w z systemu plik�w.\n
	 @param urlInfo - adres obiektu z informacj� o pliku.\n
	 Jest on wywo�ywany przez dziedzicz�ce po tej klasie, obiekty klas systemu
	 plik�w (LocalFS, FtpFS, etc.), i powoduje wstawianie elementu na widok listy.
	 */
	void slotListInfo( const UrlInfoExt & url );

	/** W slocie ustawiana jest tylko sk�adowa zawieraj�ca bie��c� �cie�k�
	 systemu plik�w.\n
	 @param newPath - nowa �cie�ka, kt�r� nale�y ustawi� jako bie��c�.\n
	 Jest on wykorzystywany w widoku drzewa przez obiekt klasy widoku
	 listy (przy poruszaniu kursorem mo�e si� zmieni� bie��cy katalog).
	 */
	void slotSetCurrentURL( const QString & newPath );

	/** Slot powoduje pobranie informacji o pliku wprost z widoku listy.\n
	 @param fileName - nazwa pliku, kt�remu nale�y pobra� reszte informacji,
	 @param urlInfo - adres obiektu, w kt�rym umieszczana jest pobrana informacja.
	 */
	void slotGetUrlInfoFromList( const QString & fileName, UrlInfoExt & );


	/** Slot powoduje pokazanie dialogu z pytaniem o nadpisywanie docelowego
	 pliku.\n
	 @param sourceFileName - nazwa �r�d�owego plik,
	 @param targetFileName - adres obiektu zawieraj�cego nazw� docelowego pliku,
	 przy czym slot mo�e tu wpisa� now� nazw� (je�li u�ytkownik wybierze jej
	 zmian�),
	 @param result - adres zmiennej, kt�ra b�dzie zawiera� kod wci�ni�tego guzika.\n
	 Dost�pne s� nast�puj�ce odpowiedzi: 'Yes', 'No', 'If different size', 'Rename',
	 'All', 'Update all', 'None', 'Cancel'. Przed pokazaniem dialogu wstrzymywany
	 jest stoper na dialogu post�pu, a po wybraniu opcji stoper jest uruchamiany
	 ponownie.
	 */
	void slotShowFileOverwriteDlg( const QString & sourceFileName, QString & targetFileName, int & result );

	/** Slot powoduje pokazanie dialogu z pytaniem czy podany katalog usun��
	 rekurencyjnie, mo�liwymi odpowiedziami s�: 'Yes', 'No', 'All', 'None',
	 'Cancel'.\n
	 @param dirName - nazwa usuwanego katalogu,
	 @param result - adres zmiennej, kt�ra b�dzie zawiera� kod wci�ni�tego guzika.\n
	 Przed pokazaniem dialogu wstrzymywany jest stoper na dialogu post�pu, a po
	 wybraniu opcji stoper jest uruchamiany ponownie.
	 */
	void slotShowDirNotEmptyDlg( const QString & dirName, int & result );


	/** W tym slocie nast�puje uaktualnienie informacji o wyszukiwaniu pliku na
	 dialogu wyszukiwania.\n
	 @param matches - ilo�� pasuj�cych element�w,
	 @param files - ilo�� wszystkich przej�anych plik�w,
	 @param directories - ilo�� wszystkich przej�anych katalog�w.
	 */
	void slotUpdateFindStatus( uint matches, uint files, uint directories );

	/** Slot uruchamia procedur� wyszukiwania pliku.\n
	 @param findCriterion - adres obiektu zawieraj�cego kryterium wyszukiwania.\n
	 Je�li kryterium jest puste, wtedy dialog wyszukiwania jest przestawiany w
	 stan 'zako�czono wyszukiwanie'.
	 */
	void slotStartFind( const FindCriterion & findCriterion );

	/** Slot powoduje wstawienie na list� dialogu wyszukiwania pliku lub wzorca,
	 podanego elemenu.\n
	 @param urlInfo - adres obiektu zawieraj�cego informacj� o pliku.
	 */
	void slotInsertMatchedItem( const UrlInfoExt & url );

	/** Slot wywo�uje wczytanie konfiguracji skoja�e� plik�w.
	 */
	void slotRereadFilesAssociation();

	/** Slot powoduje wczytywanie pliku wybranego do podgl�du.
	 */
	virtual void slotReadFileToView( const QString & , QByteArray & , int &, int ) {}

	/** Slot powoduje zainicjowanie podanego obiektu z informacj� o pliku
	 atrybutami podanego.
	 */
	virtual void slotGetUrlInfo( const QString & , UrlInfoExt & ) {}

	/** Slot uruchamia procedur� wstrzymania lub wznowienia bie��cej operacji.
	 */
	virtual void slotPause( bool ) {}

private slots:
	/** Slot urchamia procedur� zmiany atrubut�w dla zaznaczonych plik�w.\n
	@param newUrlInfo - adres obiektu zawieraj�cego now� informacj� o pliku,
	@param allSelected - r�wne TRUE, wymsza zmian� atrybut�w dla wszystkich
	zaznaczonych plik�w, w przeciwnym razie zmiana jest wykonywana tylko dla
	bie��cego.\n
	Slot jest wywo�ywany (przy pomocy sygna�u) przez dialog w�a�ciwo�ci pliku.
	Patrz te�: @see setAttributs().
	*/
	void slotChangePropertiesOfFile( const UrlInfoExt & url, bool allSelected );

	/** Slot wywo�ywany w momencie zaznaczenia checkBox-u opisanego jako
	 "Close after finished", na dialogu post�pu danej operacji.\n
	 @param closeState - r�wne TRUE oznacza zamkni�cie dialogu post�pu.
	 */
	void slotCloseAfterFinished( bool closeState );

	/** Slot uruchamia procedur� wa�enia katalogu.\n
	 @param itemNum - indeks elementu na li�cie nazw plik�w.
	 Slot jest wywo�ywany (przy pomocy sygna�u) przez dialog w�a�ciwo�ci pliku.
	 Wa�enie plik�w jest uruchamiane w tzw. trybie 'realWeigh', po wi�cej
	 informacji patrz: @see sizeFiles().
	 */
	void slotCountingDirectory( int itemNum );

	/** Slot powoduje usuni�cie dialogu w�a�ciwo�ci pliku oraz przerwanie bie��cej
	 operacji.
	 */
	void slotClosePropertiesDlg();

	/** Slot powoduje pokazanie dialog z pytaniem o potwierdzenie przerwania
	 bie��cej operacji.\n
	 Po potwierdzeniu przerwania wywo�ywana jest funkcja zatrzymuj�ca operacj�.
	 Slot jest wywo�ywany w momencie przerwania jakiejkolwiek operacji.
	 */
	void slotCancelOperation();


	/** Slot powoduje usuni�cie dialogu wyszukiwania pliku oraz przerwanie
	 bie��cej operacji.
	 */
	void slotCloseFindFileDlg();

	/** W slocie uruchamiana jest procedura usuni�cia podanego pliku.\n
	 @param fileName - nazwa pliku, kt�ry nale�y usun��.\n
	 Slot u�ywany przez obiekt klasy FindFileDialog.
	 */
	void slotDeleteFile( const QString & fileName );

	/** Slot powoduje ewentualn� zmian� bie��cego katalogu oraz skok kursora
	 na li�cie do podanego pliku.\n
	 @param fileName - nazwa pliku, kt�ry nale�y otworzy�.\n
	 Jest on u�ywany przez obiekt klasy FindFileDialog.
	 */
	void slotJumpToTheFile( const QString & fileName );

	/** Slot powoduje otwarcie podanego pliku w podanym trybie.\n
	 @param fileName - nazwa pliku do otwarcia,
	 @param editMode - tryb otwarcia (TRUE - edycja, FALSE - podgl�d).\n
	 Slot u�ywany jest przez obiekt klasy FindFileDialog.
	 */
	void slotViewFile( const QString & fileName, bool editMode );

signals:
	/** Sygna� powoduje pokazanie na statusie panela podanej informacji.\n
	 @param info - tekst do wy�wietlenia.\n
	 U�ywany jest do pokazywania zmiany statusu w bie��cej operacji,
	 np. po��czenia ze zdalnym hostem.
	 */
	void signalSetInfo( const QString & info );

	/** Sygna� oznacza zako�czenie operacji.
	 @param operation - zako�czona operacja,
	 @param noError - r�wne TRUE oznacza, �e operacja zako�czy�a si� bezb��dnie,
	  FALSE m�wi o tym, �e wyst�pi� b��d.\n
	 Jest wysy�any po zako�czenia operacji plikowej.
	 */
	void signalResultOperation( Operation operation, bool noError );


	/** Sygna� u�ywany do pokazywania post�pu dla operacji na pliku,
	 np. kopiowania.\n
	 @param done - ca�kowita liczba przetransportowanych bajt�w z danego pliku,
	 @param total - liczba wszystkich bajt�w danego pliku,
	 @param bytesTransfered - mie�ci liczb� bajt�w aktualnie przetransportowanych.
	 */
	void signalDataTransferProgress( long long done, long long total, uint bytesTransfered );

	/** Sygna� u�ywany do zliczania plik�w podczas wykonywania bie��cej operacji.\n
	 @param value - liczba plik�w,
	 @param fileCounting - warto�� TRUE oznacza, �e zliczane s� pliki,
	 w przeciwnym razie s� to katalogi,
	 @param valueOfAll - waro�� TRUE oznacza, �e zliczana jest ilo�� wszystkich
	 (u�ywane przy operacji wa�enia), dla FALSE - ilo�� przetworzonych.
	 */
	void signalFileCounterProgress( int value, bool fileCounting, bool setTotalValue );

	/** Sygna� u�ywany do wysy�ania nazwy aktualnie przetwarzanego pliku.\n
	@param fileName - nazwa przetwarzanego pliku,
	@param trgFileName - nazwa �cie�ki docelowej.
	 */
	void signalNameOfProcessedFile( const QString & srcFileName, const QString & trgFileName=QString::null );

	/** Sygna� u�ywany do ustawiania nazwy operacji na dialogu post�pu.\n
	 @param op - kod operacji (patrz @see Operation).
	 */
	void signalSetOperation( Operation op );

	/** Sygna� wysy�a warto�� procentow� informuj�c� o post�pie ca�ej operacji.\n
	 @param done - warto�� w procentach oznaczaj�ca ile dot�d zrobiono,
	 @param totalWeight - waga wszystkich przetwarznych plik�w.
	 */
	void signalTotalProgress( int done, long long totalWeight ); // done in percent


	/** Sygna� nakazuje otwiercie podanego archiwum.\n
	 @param fileName - nazwa archiwum.
	 */
	void signalOpenArchive( const QString & fileName );


	/** Sygna� uruchamia wczytywanie pliku do podgl�du.
	 */
	void signalReadyRead();

};

#endif
