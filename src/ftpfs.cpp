/***************************************************************************
                          ftpfs.cpp  -  description
                             -------------------
    begin                : sat nov 30 2002
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

$Id$
 ***************************************************************************/

#include "ftpfs.h"
#include "messagebox.h"
#include "fileinfoext.h"
#include "ftpconnectdialog.h"
#include "fileoverwritedialog.h"
#include "plugins/view/text/view.h"

#include <qapplication.h>
#include <qcombobox.h>
#include <qlineedit.h>
#include <qfileinfo.h>
#include <qsettings.h>
#include <qspinbox.h>
#include <qdir.h>

#include <netdb.h> // for gethostbyname()
extern int h_errno;

static const int FTP_TIMEOUT = 4*60*1000; // 4 min.


FtpFS::FtpFS( const QString & inputURL, ListViewExt * listViewExt )
	: VirtualFS( inputURL, listViewExt )
{
//	debug("FtpFS::FtpFS");
	mMoveFiles = FALSE;
	mGettingURL = FALSE;
	mPassiveModeOn = FALSE;
	m_bAfterBreakKeepFragment = FALSE;

	mOldUserName = "anonymous";
	mOldPassword = "";
	mOldPortNum  = 21;

	initSessionParameters( inputURL ); // need to inits member 'mHostName' which is used by parent class
	mOpenedFilesPtrList.setAutoDelete( TRUE );

	mFtp = new QFtp( this );

	connect( mFtp, SIGNAL(done(bool)),                 SLOT(slotDone(bool)) );
	connect( mFtp, SIGNAL(stateChanged(int)),          SLOT(slotStateChanged(int)) );
	connect( mFtp, SIGNAL(commandStarted(int)),        SLOT(slotCommandStarted(int)) );
	connect( mFtp, SIGNAL(commandFinished(int,bool)),  SLOT(slotCommandFinished(int,bool)) );
	connect( this, SIGNAL(listInfo(const UrlInfoExt &)), SLOT(slotListInfo(const UrlInfoExt &)) );
	connect( mFtp, SIGNAL(rawCommandReply(int, const QString &)), SLOT(slotRawCommandReply(int, const QString &)) );
	connect( mFtp, SIGNAL(dataTransferProgress(int,int)), SLOT(slotSetProgress(int,int)) );
	connect( mFtp, SIGNAL(readyRead()), this, SIGNAL(signalReadyRead()) );
	connect( mFtp, SIGNAL(listInfo(const QUrlInfo &)), this, SLOT(slotQFtpListInfo(const QUrlInfo &)) );

	connect( this, SIGNAL(signalFileOverwrite(const QString &, QString &, int &)),
	 this, SLOT(slotShowFileOverwriteDlg(const QString &, QString &, int &)) );

	m_pTimer = new QTimer;
	connect(m_pTimer, SIGNAL(timeout()), this, SLOT(slotTimeOut()));
}


FtpFS::~FtpFS()
{
	delete mFtp;

	m_pTimer->stop();
	delete m_pTimer;

	QApplication::restoreOverrideCursor();
}


void FtpFS::connectToHost( const QString & inputURL, QFtp::Error error )
{
	debug("FtpFS::connectToHost(), inputURL=%s, mFtp->state()=%d", inputURL.latin1(), mFtp->state() );

	mOldPortNum  = mPortNum;
	mOldUserName = mUserName;
	mOldPassword = mPassword;
	QString oldHostName  = mHostName;
	QString oldRemoteDir = mRemoteDir;
	bool connection = FALSE;
	// --- inits follows members: mHostName, mRemoteDir, mPortNum, mUserName, mPassword
	initSessionParameters( inputURL );

	if ( mFtp->state() == QFtp::Unconnected || error == QFtp::HostNotFound )
		connection = TRUE;
	else
	if ( isLoggedIn() ) {
		if ( mRemoteDir != oldRemoteDir ) {
			if ( error != QFtp::NoError ) {
				mFtp->cd( mRemoteDir ); // po pierwszym wyw. od razu leci do slotCommandFinished (Cd), a error==FALSE
				mFtp->cd( mRemoteDir ); // dlatego trzeba to polecenie wyw. jeszcze raz
			}
			else // no error
				changeDirOn( mRemoteDir );
		}
		else
		if ( mOldUserName != mUserName ) {
			debug("\tis logged in as %s, try login as %s", mOldUserName.latin1(), mUserName.latin1());
			mFtp->login();
		}
		else
			connection = TRUE;
	}
	else
	if ( mFtp->state() == QFtp::Connected && error != QFtp::NoError ) {
		mFtp->login( mUserName, mPassword ); // po pierwszym wyw. od razu leci do slotCommandFinished (Close), a error==FALSE
		mFtp->login( mUserName, mPassword ); // dlatego trzeba to polecenie wyw. jeszcze raz
	}
	else
	if ( mFtp->state() == QFtp::HostLookup ) {
		debug("\tuser break HostLookup");
		emit signalResultOperation( HostLookup, FALSE );
		return;
	}


	if ( connection ) {
		closeCurrentConnection(); // only if was connection

		debug("\ttry connect, host=%s, remoteDir=%s, port=%d, user=%s, pass=%s",
			mHostName.latin1(), mRemoteDir.latin1(), mPortNum, mUserName.latin1(), mPassword.latin1());

		struct hostent *hostEntries = ::gethostbyname( mHostName );
		if ( hostEntries )
			mFtp->connectToHost( hostEntries->h_name, mPortNum );
		else {
			mLastFinishedCmd = QFtp::ConnectToHost;
			slotDone( TRUE ); // error occurred
		}
	}
}


void FtpFS::readDir( const QString & dirName )
{
	mOldUserName = mUserName;
	mOldPassword = mPassword;
	initSessionParameters( dirName );
	debug("FtpFS::readDir, dirName=%s, mRemoteDir=%s, mUserName=%s", dirName.latin1(), mRemoteDir.latin1(), mUserName.latin1() );

	if ( isLoggedIn() ) {
		changeDirOn( mRemoteDir );
		return;
	}
	// --- not loggedIn

	if ( mFtp->state() == QFtp::Unconnected && VirtualFS::kindOfFilesSystem(currentURL()) != FTPfs ) {
		connectToHost( dirName );
		return;
	}

	if ( connectAgain() )
		changeDirOn( mRemoteDir );
}


void FtpFS::checkAllLinks() // NNN
{
	mGettingURL = TRUE;
	mLinkCheckingCounter = 0;
	QStringList itemList;

	for ( uint i=0; i<mTargetFileForLinkLst.count(); i++ ) {
		itemList = QStringList::split( '\n', mTargetFileForLinkLst[i] );
		debug("FtpFS::checkAllLinks(), send STAT");
		mFtp->rawCommand( "STAT "+(mNameOfProcessedFile=itemList[1]) ); // need to check all target files
	}
}


void FtpFS::changeDirOn( const QString & newPath )
{
	UrlInfoExt uie;
	QString newURL = newPath;
	slotGetUrlInfoFromList( newPath, uie );
	if ( uie.isDir() && uie.isSymLink() ) // need to resolve a link
		newURL = uie.linkTarget()+"/";

	debug("FtpFS::changeDirOn, newURL=%s, send commands: CD and PWD to ftp serwer", newURL.latin1() );
	mFtp->cd( newURL );
	mFtp->rawCommand( "PWD" );
}


void FtpFS::open( const QString & fileName )
{
	if ( ! isLoggedIn() ) {
		if ( ! connectAgain() )
			return;
	}
	debug("FtpFS::open(), fileName=%s", fileName.latin1() );
	mErrorCode = NoError;
	mOpFileCounter = 0;
	mReadFile = TRUE;
	mGettingURL = FALSE;
	mFileIsReadable = FALSE;
	mNameOfProcessedFile = fileName;
	debug("FtpFS::open(), send STAT");
	mStat = mFtp->rawCommand( "STAT "+FileInfoExt::fileName(mNameOfProcessedFile) ); // get stat of dir
}


void FtpFS::initSessionParameters( const QString & inputURL )
{
	if ( inputURL.find("ftp://") == -1 )
		return;
	// default settings
	mUserName = mOldUserName;
	mPassword = mOldPassword;
	mPortNum  = mOldPortNum;
	// -- parse URL
	mRemoteDir = inputURL;
	int nMonkeyPos = mRemoteDir.findRev('@'), nColonPos;
	if ( nMonkeyPos != -1 ) {
		mRemoteDir = mRemoteDir.right(mRemoteDir.length()-nMonkeyPos-1);
		mHostName  = mRemoteDir.left(mRemoteDir.find('/'));
		mRemoteDir = mRemoteDir.remove(mHostName);
		mUserName  = inputURL.left(inputURL.findRev('@'));
		mUserName  = mUserName.remove( "ftp://" );
		nColonPos  = mUserName.findRev(':');
		if ( nColonPos != -1 ) {
			mPassword = mUserName.right(mUserName.length()-mUserName.findRev(':')-1);
			mUserName = mUserName.left(mUserName.findRev(':'));
		}
	}
	else { // URL without user
		mHostName  = mRemoteDir.remove( "ftp://" );
		mHostName  = mHostName.left( mHostName.find('/') );
		mRemoteDir = mRemoteDir.remove(mHostName);
	}
	nColonPos = mHostName.findRev(':'); // find port
	if ( nColonPos != -1 ) {
		mPortNum = mHostName.right(mHostName.length()-nColonPos-1).toInt();
		mHostName = mHostName.left( nColonPos ); // rest (without 'nPort:') save into sHost
	}

// 	debug(">> FtpFS::initSessionParameters, mHostName=%s, mRemoteDir=%s", mHostName.latin1(), mRemoteDir.latin1() );
}


void FtpFS::closeCurrentConnection( bool force )
{
	if ( force )
		mFtp->close();
	else
	if ( mFtp->state() != QFtp::Unconnected ) {
		debug("FtpFS::closeCurrentConnection(), mFtp->close()");
		mFtp->close();
	}
}


void FtpFS::getWeighingStat( long long & weigh, uint & files, uint & dirs )
{
	weigh = 48; files = 0; dirs = 1;
//	mFtpExt->getWeighingStat( weigh, files, dirs );
}


void FtpFS::sizeFiles( const QStringList & /*filesList*/, bool /*realWeigh*/ )
{
	/*mSkipNextErrorOccures = FALSE;
	mSoLF->setRealWeighInFS( realWeigh );

	for (uint i=0; i<filesList.count(); i++)
		mSoLF->size( VirtualFS::absoluteFileName(filesList[i]) );*/
}


void FtpFS::createAnEmptyFile( const QString & fileName )
{
	if ( ! isLoggedIn() ) {
		if ( ! connectAgain() )
			return;
	}

	if ( fileName.at(fileName.length()-1) == '/' )
		mFtp->mkdir( FileInfoExt::fileName(mNameOfProcessedFile=fileName) );
}


void FtpFS::mkdir( const QString & dirName )
{
	mFtp->mkdir( FileInfoExt::fileName(mNameOfProcessedFile=dirName) );
}


void FtpFS::setAttributs( const QStringList & filesList, const UrlInfoExt & newUrlInfo, bool /*recursive*/, int /*changesFor*/ )
{
	if ( ! isLoggedIn() ) {
		if ( ! connectAgain() )
			return;
	}
	mSkipNextErrorOccures = FALSE;

	if ( ! newUrlInfo.name().isEmpty() )
		mFtp->rename( FileInfoExt::fileName(filesList[0]), FileInfoExt::fileName(newUrlInfo.name()) );

	if ( newUrlInfo.permissions() != -1 ) {
		QString strOctal = QString::number(newUrlInfo.permissions(), 8);
		debug("FtpFS::setAttributs, send SITE CHMOD");
		mStat = mFtp->rawCommand( "SITE CHMOD "+strOctal+" "+FileInfoExt::fileName(filesList[0]) );
		// po bledzie zwr.ciag:  550 nazwa_pliku: Permission denied
	}
}


void FtpFS::copyFiles( QString & targetPath, const QStringList & filesToCopyList, bool /*savePerm*/, bool /*alwaysOvr*/, bool /*followByLink*/, bool move )
{
	debug("FtpFS::copyFiles()");
	if ( ! isLoggedIn() ) {
		if ( ! connectAgain() )
			return;
	}
	if ( kindOfFilesSystem(targetPath) == LOCALfs )
		if ( ! QFileInfo(targetPath).isWritable() ) {
			MessageBox::critical( this, tr("FTP error")+" - QtCommander",
			 targetPath+"\n\n"+tr("Cannot write to this directory")+"." );
			return;
		}

	mRealWritedBytes = 0;
	mCopiedFilesList = filesToCopyList;
	mOpFileCounter = 0;
	mTotalFiles = filesToCopyList.count(); // total visible files
	QString fName;
	QFile *file;
	bool mUploading = (targetPath.find("ftp://") == 0); // jesli w jednym z paneli jest LocalFS
	bool isFile;
	int flag;
	mTargetPath = targetPath;
	mMoveFiles = move;
	mOpenedFilesPtrList.clear();
	showProgressDialog( (move ? Move : Copy), move, targetPath );
	mSkipNextErrorOccures = FALSE;

	bool isDir;
	bool noneOverwriting = FALSE;
	bool alwaysOverwrite = FALSE, updatingFiles = FALSE, updateIfDiffSize = FALSE;
	UrlInfoExt urlInfoExt;

	for( uint i=0; i<mTotalFiles; i++) {
		isDir = ( filesToCopyList[i].at(filesToCopyList[i].length()-1) == '/' );
		if ( mUploading )
			fName = filesToCopyList[i];
		else {
			fName = targetPath+"/"+FileInfoExt::fileName(filesToCopyList[i]);
			if ( isDir )
				fName += "/"; // becouse FileInfoExt::fileName() cutting it
		}
		if ( ! isDir ) // if is'nt directory
			file = new QFile( fName );

		if ( mUploading )
			if ( isDir ) {
				fName.remove( fName.length()-1, 1 ); // remove last slash
				mFtp->mkdir( FileInfoExt::fileName(fName) );
				continue;
			}
			else
				flag = IO_ReadOnly;
		else { // init dowload file
			if ( isDir ) {
				QDir().mkdir(fName);
				if ( i+1 == mTotalFiles )
					startProgressDlgTimer( FALSE ); // stop
				mNameOfProcessedFile = filesToCopyList[i];
				emit signalNameOfProcessedFile( mNameOfProcessedFile );
				emit signalResultOperation( Copy, TRUE );
				continue;
			}
			else
			if ( file->exists() ) {
				debug("target file alredy exists = '%s'", fName.latin1() );
				int result = -1;
				if ( noneOverwriting ) // never overwrite
					continue;

				QString targetFileName = targetPath+"/"+FileInfoExt::fileName(filesToCopyList[i]);

				if ( alwaysOverwrite ) {
					if ( ! removeFileFromLocalFS(targetFileName) )
						continue;
				}
				else if ( updateIfDiffSize ) {
					slotGetUrlInfoFromList( fName, urlInfoExt );
					if ( urlInfoExt.size() == QFileInfo(targetFileName).size() )
						continue;
					else
					if ( ! removeFileFromLocalFS(targetFileName) )
						continue;
				}
				else if ( updatingFiles ) {
					slotGetUrlInfoFromList( fName, urlInfoExt );
					if ( urlInfoExt.lastModified() == QFileInfo(targetFileName).lastModified() )
						continue;
					else
					if ( ! removeFileFromLocalFS(targetFileName) )
						continue;
				}
				startProgressDlgTimer( FALSE ); // stop
				emit signalFileOverwrite( targetFileName, fName, result ); // show overwrite dialog
				startProgressDlgTimer( TRUE );
				if ( result == FileOverwriteDialog::Yes ) {
					if ( ! removeFileFromLocalFS(targetFileName) )
						continue;
				}
				else if ( result == FileOverwriteDialog::No ) {
					if ( i+1 == mTotalFiles )
						startProgressDlgTimer( FALSE ); // stop
					continue;
				}
				else if ( result == FileOverwriteDialog::DiffSize ) {
					updateIfDiffSize = TRUE;
					slotGetUrlInfoFromList( fName, urlInfoExt );
					if ( urlInfoExt.size() == QFileInfo(targetFileName).size() ) {
						if ( i+1 == mTotalFiles )
							startProgressDlgTimer( FALSE ); // stop
						continue;
					}
					else
					if ( ! removeFileFromLocalFS(targetFileName) )
						continue;
				}
				else if ( result == FileOverwriteDialog::All ) {
					alwaysOverwrite = TRUE;
					if ( ! removeFileFromLocalFS(targetFileName) )
						continue;
				}
				else if ( result == FileOverwriteDialog::Update ) {
					updatingFiles = TRUE;
					slotGetUrlInfoFromList( fName, urlInfoExt );
					if ( urlInfoExt.lastModified() == QFileInfo(targetFileName).lastModified() ) {
						if ( i+1 == mTotalFiles )
							startProgressDlgTimer( FALSE ); // stop
						continue;
					}
					else
					if ( ! removeFileFromLocalFS(targetFileName) )
						continue;
				}
				else if ( result == FileOverwriteDialog::None ) {
					noneOverwriting = TRUE;
					continue;
				}
				else if ( result == FileOverwriteDialog::Cancel ) {
					slotDone( FALSE );
					return;
				}
			}
			flag = (IO_WriteOnly | IO_Append);
		}
		if ( ! file->open(flag) ) {
			debug("\tcannot open file = %s", fName.latin1() );
			continue;
		}

		if ( mUploading ) {
			isFile = QFileInfo(filesToCopyList[i]).isFile();
			mFtp->put( file, FileInfoExt::fileName(filesToCopyList[i]) );
		}
		else {
			isFile = (filesToCopyList[i].at(filesToCopyList[i].length()-1) != '/');
			mFtp->get( FileInfoExt::fileName(filesToCopyList[i]), file );
			mOpenedFilesPtrList.append(file);
		}
		emit signalFileCounterProgress( i+1, isFile, TRUE );
	}
}  // int put ( const QByteArray & data, const QString & file )


void FtpFS::deleteFiles( const QStringList & filesToRemoveList )
{
	if ( ! isLoggedIn() ) {
		if ( ! connectAgain() )
			return;
	}

	bool isFile;
	QString fName;
	mOpFileCounter = 0;
	mCopiedFilesList = filesToRemoveList;
	mTotalFiles = filesToRemoveList.count(); // total visible files
	mSkipNextErrorOccures = FALSE;
	showProgressDialog( Remove );

	for (uint i=0; i<mTotalFiles; i++) {
		fName = filesToRemoveList[i];
		isFile = (fName.at(fName.length()-1) != '/');
		if ( isFile )
			mFtp->remove( FileInfoExt::fileName(fName) );
		else
			mFtp->rmdir( FileInfoExt::fileName(fName) ); // tylko dla pustych

		emit signalFileCounterProgress( mTotalFiles, isFile, TRUE ); // set total files
	}
}


void FtpFS::breakCurrentOperation()
{
	debug("FtpFS::breakCurrentOperation()");
	slotDone( FALSE );
	removeProgressDlg( FALSE );
}


Operation FtpFS::currentOperation() const
{
	Operation operation = NoneOp;

	     if ( mLastFinishedCmd == QFtp::Cd )      operation = Cd;
	else if ( mLastFinishedCmd == QFtp::List )   operation = List;
	else if ( mLastFinishedCmd == QFtp::Remove ) operation = Remove;
	else if ( mLastFinishedCmd == QFtp::Rename ) operation = Rename;
	else if ( mLastFinishedCmd == QFtp::Mkdir )  operation = MakeDir;
	else if ( mLastFinishedCmd == QFtp::ConnectToHost ) operation = Connect;
	else
	if ( mLastFinishedCmd == QFtp::Put || mLastFinishedCmd == QFtp::Get )
		operation = (mReadFile) ? Open : Copy;

	return operation;
}


bool FtpFS::connectAgain()
{
	int result = MessageBox::yesNo( this,
	 tr("FTP connection")+" - QtCommander",
	 tr("You are unconnected to the server.")+"\n"+tr("Could you connect again ?"),
	 MessageBox::Yes, TRUE // default is the 'Yes' button, TRUE - show Cancel button
	);
	if ( result == MessageBox::Yes ) {
		connectToHost( mHostName+mRemoteDir );
		if ( isLoggedIn() )
			return TRUE;
	}
// 	else // No
		//pokazac dialog polaczenia
	else
	if ( result == MessageBox::All ) // 'Cancel' button pressed
		closeCurrentConnection( TRUE ); // TRUE - force unconnect

	return FALSE;
}


bool FtpFS::removeFileFromLocalFS( const QString & fileName )
{
	if ( ! QFile(fileName).remove() ) {
		MessageBox::critical( this, tr("FTP error")+" - QtCommander",
		 fileName+"\n\n"+tr("Cannot remove this file.")+"." );
		return FALSE;
	}

	return TRUE;
}


int FtpFS::nameIndex( const QString & name )
{
	uint i = 1, sepCount = 0;

	while ( sepCount < 8 && name[i] != ' ' && i<name.length() ) {
		i++;
		if ( name[i]==' ' ) {
			sepCount++;
			while (name[i]==' ') i++;
		}
	}

	return i;
}

			// -------- SLOTs ---------

void FtpFS::slotCommandStarted( int command )
{
	debug("FtpFS::slotCommandStarted(), currentCommand=%d, call command=%d", mFtp->currentCommand(), command );
	//QFtp::Command currentCmd = mFtp->currentCommand();
	QFtp::Command currentCmd = (command != QFtp::List) ? mFtp->currentCommand() : QFtp::List;
	mLastFinishedCmd = QFtp::None;
	mErrorCode = NoError;

	switch ( currentCmd ) {
		case QFtp::ConnectToHost:
			emit signalResultOperation( Connect, TRUE ); // zeby pokazac hosta w widoku sciezki (min. dla state=hostLookUp)
			break;

		case QFtp::Login:
			emit signalSetInfo( tr("Connected. Try logged in as")+" "+mUserName );
			break;

		case QFtp::List:
			if ( mGettingURL ) // 'list' for file
				return;
			emit signalSetInfo( tr("Getting the list of files...") );
			QApplication::setOverrideCursor( waitCursor );
			mNameOfProcessedFile = rootName()+mHostName+mRemoteDir;
			if ( mRemoteDir.at(mRemoteDir.length()-1) != '/' )
				mNameOfProcessedFile += "/";
			setCurrentURL( mNameOfProcessedFile ); // fun.from VirtualFS class
			initializeShowsList(); // sets variable a root's of tree and clears panel
			break;

		case QFtp::Get:
		/*	if ( mReadFile ) {
				//emit signalResultOperation( Open, TRUE ); // create the view obj.
				//mOpFileCounter++;
				//emit signalReadyRead(); // this signal is emiting after this slot leaving
				break;
			}*/
		case QFtp::Put:
		case QFtp::Mkdir:
		case QFtp::Rmdir:
		case QFtp::Remove: {
			int id = (currentCmd == QFtp::Remove && mMoveFiles && mOpFileCounter) ? mOpFileCounter-1 : mOpFileCounter;
			if ( currentCmd != QFtp::Mkdir )
				mNameOfProcessedFile = mCopiedFilesList[id]; // need to correct remove/insert
			emit signalNameOfProcessedFile( mNameOfProcessedFile );
			if ( currentCmd != QFtp::Mkdir )
				mOpFileCounter++;
			break;
		}
		default: break;
	} // switch
}


void FtpFS::slotCommandFinished( int command, bool error )
{
	QFtp::Command currentCmd = mFtp->currentCommand();
	mLastFinishedCmd = (command != QFtp::List) ? currentCmd : QFtp::List;
	Operation operation = currentOperation(); // translate from QFtp::operation (value of 'mLastFinishedCmd')
	QApplication::restoreOverrideCursor();
	debug("FtpFS::slotCommandFinished(), command=%d, FTPcurrentCommand=%d, operation=%s, error=%d, state=%d", command, currentCmd, cmdString((Operation)operation).latin1(), error, mFtp->state() );

	// run post operation command
	if ( ! error ) {
		switch ( currentCmd ) {
			case QFtp::ConnectToHost:
				mFtp->login( mUserName, mPassword );
				break;
			case QFtp::Login: // mFtp->state() == QFtp::LoggedIn, bo error=FALSE dla wczesniejszego Login
				changeDirOn( mRemoteDir );
				break;
			case QFtp::Cd: {
				mGettingURL = FALSE;
				mLinkChecking = FALSE;
				mTargetFileForLinkLst.clear();
				//debug("FtpFS::slotCommandFinished() for CD, send: TYPE A, PASV, STAT");
				//mFtp->rawCommand( "TYPE A" );
				//mFtp->rawCommand( "PASV" );
				//mFtp->rawCommand( "LS" );
				//mFtp->rawCommand( "STAT "+mRemoteDir );
				slotCommandStarted( QFtp::List );
				mFtp->list(); // QFtp bug: not taked into consideration trailing spaces in the files name
				break;
			}
			case QFtp::Get:
				if ( mReadFile ) { // file has been read
					mReadFile = FALSE;
					mGettingURL = FALSE;
					operation = NoneOp;
					emit signalReadyRead(); // need to updateStatus
					break;
				}
				else {
					mOpenedFilesPtrList.first()->close(); // close file that has been copied
					mOpenedFilesPtrList.remove();
					// mozna tu ustawiac date modyfikacji pliku (taka jak ma plik zrodlowy)
				}
			case QFtp::Put: {
				if ( mMoveFiles ) { // remove copied files
					if ( currentCmd == QFtp::Get ) {
						mFtp->remove( FileInfoExt::fileName(mNameOfProcessedFile) );
						if ( mOpFileCounter == mTotalFiles ) { // need to clear a file counter before start removing
							mOpFileCounter = 0;
							//emit signalSetOperation( Remove );
						}
					}
					else { // remove file from LOCALfs
						operation = Move;
						bool fileRemoved = QFile( mNameOfProcessedFile ).remove();
						if ( ! fileRemoved && ! mSkipNextErrorOccures ) {
							int result = MessageBox::yesNo( this,
							 tr("Error")+" - QtCommander",
							 mNameOfProcessedFile+"\n\n"+
							 tr("Cannot remove this file.")+"\n\n\t"+tr("Skip all next errors")+" ?",
							 MessageBox::No
							);
							if ( result == MessageBox::Yes )
								mSkipNextErrorOccures = TRUE;
						}
					}
				}
			case QFtp::Remove:
				slotSetProgress( mOpFileCounter, mTotalFiles );
				if ( mOpFileCounter != mTotalFiles )
					return;
				if ( mMoveFiles && currentCmd == QFtp::Remove )
					operation = Move;
				break;
			}
			case QFtp::Rmdir:
				operation = Remove;
				slotSetProgress( mOpFileCounter, mTotalFiles );
				break;
			case QFtp::Mkdir:
				mStat = mFtp->rawCommand( "STAT "+FileInfoExt::fileName(mNameOfProcessedFile) ); // get stat of dir
				return;
				break;
			case QFtp::RawCommand:
				if ( command == mStat ) { // mStat == kod STAT po utworzeniu kat.
					if ( mReadFile ) {
						if ( mFileIsReadable ) {
							//emit signalResultOperation( Open, TRUE ); // create the view obj.
							operation = Open;
							mFtp->get( FileInfoExt::fileName(mNameOfProcessedFile) );
						}
					}
					else
						operation = MakeDir;
				}
				/*else
				if ( command == GettingFileExistsInfo ) { // plik istnieje bo tu polecenie wykonane bez bledu
					//int result;
					//emit signalFileOverwrite( mTargetPath+"/"+FileInfoExt::fileName(mCopiedFilesList[i]), fName, result )
					// jesli brak zapisu to pokaz info
				}*/
				break;
			default: break;
		}
	}
	else { // error
		if ( ! mReadFile )
			mNameOfProcessedFile = rootName()+mHostName+mRemoteDir;
//		QFtp::Error errNum = mFtp->error();  QString errStr = mFtp->errorString();
		switch ( currentCmd ) {
			case QFtp::ConnectToHost:
				//debug("\t(Connected) delete mFtp->currentDevice();");
				//mErrorCode = CannotConnect;
				delete mFtp->currentDevice();
				return;
				break;
			case QFtp::Cd:
				mErrorCode = NotExists; // TODO ponizsze sprawdzic dla wchodzenia bezposredniego - przez Enter na liscie
				return;
				break;
			case QFtp::Login: // mErrorCode = CannotLogin;
				debug("\tlogin finished with error");
				emit signalResultOperation( Connect, TRUE ); // uaktualnia sciezke
				return;
				break;
			case QFtp::Put:
				debug("\tuploading error, file=%s", mCopiedFilesList[mOpFileCounter-1].latin1() );
				//mFtp->list( mCopiedFilesList[mOpFileCounter] ); // jesli uda sie pobrac url to plik istnieje
				return;
				break;
			default: break;
		}
	} // error has been ocurred

	if ( operation != NoneOp )
		emit signalResultOperation( operation, !error );
}


void FtpFS::slotDone( bool finishedWithError )
{
	if ( mGettingURL )
		return;

	QFtp::Command currentCmd = mFtp->currentCommand();
	QApplication::restoreOverrideCursor();
	QApplication::restoreOverrideCursor(); // potrzebne dla syt. nieudanej zmiany katalogu po pierwszym zalogowaniu
	debug("FtpFS::slotDone, mRemoteDir=%s, finishedWithError=%d, errorNum=%d, currentCmd=%d, mLastFinishedCmd=%d, state=%d", mRemoteDir.latin1(), finishedWithError, mFtp->error(), currentCmd, mLastFinishedCmd, mFtp->state() );
	mMoveFiles = FALSE;

	startProgressDlgTimer( FALSE ); // stop
	setOperationHasFinished( TRUE );

	if ( mLastFinishedCmd == QFtp::Close ) { // NNN wykorzystywane przy opuszczaniu dialogu polaczenia (wyw.z wnetrza biezacej kl.)
		debug("\tlogin dialog canceled");
		emit signalResultOperation( Connect, FALSE );
		return;
	}
	else {
		Operation op = currentOperation(); // translate mLastFinishedCmd (QFtp::Command) to Operation
		if ( op == Copy || op == Remove )
			removeProgressDlg( TRUE );
	}

	if ( mLastFinishedCmd == QFtp::List && ! finishedWithError ) {
		QSettings *pSettings = new QSettings;
		bool bStandByConnect = pSettings->readBoolEntry( "/qtcmd/FTP/StandByConnect", FALSE );
		delete pSettings;
		if (bStandByConnect) {
			m_pTimer->start(FTP_TIMEOUT);
			debug("FtpFS::slotDone: Start StandByConnect timer (%d min.)", FTP_TIMEOUT/1000/60);
		}
	}

	if ( ! finishedWithError && ! mReadFile )
		return;
	else if ( mReadFile && mFileIsReadable ) {
		mReadFile = FALSE;
		return;
	}

	mNewUrlInfo.clear();
	debug("\terrorString=%s", mFtp->errorString().latin1() );
	QFtp::Error error = mFtp->error();
	bool hostNotFoundTryAgain = FALSE;
	if ( mFtp->state() == QFtp::Unconnected && error != QFtp::ConnectionRefused && error != QFtp::NotConnected )
		if ( h_errno == HOST_NOT_FOUND || h_errno == TRY_AGAIN )
			error = QFtp::HostNotFound;

	QString msg =
	 tr("Host")+": "+mHostName+"\n"+tr("Directory")+": "+mRemoteDir+"\n"+tr("User")+": "+mUserName+"\n\n";

	if ( error == QFtp::HostNotFound ) {
		msg += tr("Host not found");
		if ( h_errno == TRY_AGAIN ) {
			msg += ".\n"+tr("Try again");
			hostNotFoundTryAgain = TRUE;
		}
	}
	if ( mLastFinishedCmd == QFtp::Cd )
		msg += tr("Changing directory failed");
	else
	if ( error == QFtp::ConnectionRefused )
		msg += tr("The server refused the connection");
	else
	if ( error == QFtp::NotConnected )
		msg += tr("There is no connection to a server");
	else
	if ( h_errno == NO_ADDRESS || h_errno == NO_DATA )
		msg += tr("The requested name is valid but does not have an IP address");
	if ( h_errno == NO_RECOVERY )
		msg += tr("A non-recoverable name server error occurred");
	else
	if ( error == QFtp::UnknownError && mLastFinishedCmd != QFtp::Cd ) {
		if ( mLastFinishedCmd == QFtp::Login )
			msg += tr("Login failed")+".\n"+tr("User or password incorrect");
		else
		if ( mLastFinishedCmd == QFtp::Put )
			msg += tr("Cannot write to this directory");
		else
		if ( mLastFinishedCmd == QFtp::Remove )
			msg += FileInfoExt::fileName(mCopiedFilesList[mOpFileCounter-1])+"\n\n"+tr("Cannot remove this file");
		else
		if ( mLastFinishedCmd == QFtp::Get )
			msg += FileInfoExt::fileName(mCopiedFilesList[mOpFileCounter-1])+"\n\n"+tr("Cannot read this file");
		else
			msg += tr("Unknown error");
	}
	else
	if ( error == QFtp::NoError ) {
		if ( mLastFinishedCmd == QFtp::RawCommand && mReadFile ) {
			mReadFile = FALSE;
			msg += FileInfoExt::fileName(mNameOfProcessedFile)+"\n\n"+tr("Cannot read this file");
			if ( mErrorCode == NotExists ) {
				msg += ".\n"+tr("File not exists");
				emit signalResultOperation( Open, FALSE ); // for remove this item from the list
			}
		}
	}


	if ( ! msg.isEmpty() )
		MessageBox::critical( this, tr("FTP error")+" - QtCommander", msg+"." );

	// --- do action when CD operacja has been finished an error announcement
	bool connected = (mFtp->state() == QFtp::Connected);
	bool loggedIn  = (mFtp->state() == QFtp::LoggedIn);
	bool needToReconnect = (mLastFinishedCmd == QFtp::Cd || mLastFinishedCmd == QFtp::ConnectToHost || mLastFinishedCmd == QFtp::Login);

	if ( (connected || loggedIn || hostNotFoundTryAgain) && needToReconnect ) {
		if ( loggedIn )
			mRemoteDir = FileInfoExt::filePath( mRemoteDir ); // cuts a path
		else
		if ( connected )
			emit signalSetInfo( tr("Connected") );

		// --- show connect dialog
		QString inputURL = "ftp://"+mUserName+":"+mPassword+"@"+mHostName+":"+QString::number(mPortNum)+mRemoteDir;
		if ( ! FtpConnectDialog::showDialog(inputURL, this) ) // canceled connection
			closeCurrentConnection( hostNotFoundTryAgain ? TRUE : FALSE ); // condition check whether to do force close connection
		else
			connectToHost( inputURL, error );

		return;
	}

	if ( mLastFinishedCmd == QFtp::ConnectToHost && ! finishedWithError )
		emit signalResultOperation( Connect, ! finishedWithError );
}


void FtpFS::slotStateChanged( int state )
{
	switch ( (QFtp::State)state ) {
		case QFtp::Unconnected:
			emit signalSetInfo( tr("Unconnected") );
			break;
		case QFtp::HostLookup:
			emit signalSetInfo( tr("Host lookup") );
			break;
		case QFtp::Connecting:
			emit signalSetInfo( tr("Connecting") );
			break;
		case QFtp::Connected:
			emit signalSetInfo( tr("Connected") );
			break;
		case QFtp::LoggedIn:
			emit signalSetInfo( tr("Logged in as")+" "+mUserName );
			break;
		case QFtp::Closing:
			emit signalSetInfo( tr("Closing") );
			break;
	}
}


void FtpFS::slotRawCommandReply( int code, const QString & text )
{
	bool runListingFilesAgain = FALSE;
	debug("___FtpFS::slotRawCommandReply, code=%d, command=%d, text=%s", code, mFtp->currentCommand(), text.latin1());

	if ( code == 257 )
		mRemoteDir = text.section( '"', 1, 1 );
	else
	// STAT for listing and check created new dir
	if ( code == 211 || code == 213 ) { // parse output for STAT command (for new dir), it's need to make new UrlInfoExt obj.
		QStringList filesList = QStringList::split( "\r\n", text );
		if ( ! mGettingURL && ! text.isEmpty() ) { // if listing
			mMainFilesList = filesList;
			mMainFilesList[mMainFilesList.count()-1] = "."; // overwrite line "End Of Status"
		}
		const int count = (mGettingURL) ? filesList.size() : mMainFilesList.size();
		if ( count < 3 ) { // file not exists
			mErrorCode = (Error)NotExists;
			if ( mLinkChecking ) {
				// not existing target file/dir is always a file
				mTargetFileForLinkLst[mLinkCheckingCounter] = mTargetFileForLinkLst[mLinkCheckingCounter] + "\nfile";
				if ( mLinkCheckingCounter == mTargetFileForLinkLst.count() ) {
					mGettingURL = FALSE;
					mLinkChecking = FALSE;
					runListingFilesAgain = TRUE;
				}
				else
					mLinkCheckingCounter++;
			}
			slotCommandFinished( QFtp::List, TRUE );
			return;
		}
		QString name, targetLink;
		QStringList itemList;
		// --- collect all links
		if ( mTargetFileForLinkLst.count() == 0 ) {
			for ( uint i=1; i<mMainFilesList.count(); i++ ) {
				if ( mMainFilesList[i].at(1) == 'l' ) {
					name = mMainFilesList[i].mid( 56, mMainFilesList[i].length()-56 ); // 56 it's begin position of name (server: proftp)
					itemList = QStringList::split( " -> ", name );
					mTargetFileForLinkLst.append( itemList[0]+"\n"+itemList[1] ); // name + target file
				}
			}
			// and init checking all links
			if ( mTargetFileForLinkLst.count() > 0 ) {
				mLinkChecking = TRUE;
				checkAllLinks(); // that contains in mTargetFileForLinkLst
				return;
			}
		}

		UrlInfoExt uie;
		bool isDir, isReadable, isExecutable;
		long long size;
		QTime time;
		QDate date;
		QDateTime dt;
		uint nameId;
		const int futureTolerance = 600; // QFtp part
		const int counter = (mGettingURL) ? 3 : mMainFilesList.count();

		for ( int i=1; i<counter; i++ ) {
			// get name and target file for link
			name = (mGettingURL) ? filesList[i] : mMainFilesList[i];
			nameId = nameIndex( name );
			name = name.mid( nameId, name.length()-nameId );
			itemList = QStringList::split( " -> ", name ); // jesli separatora nie bedzie w ciagu to w wyniku znajdzie sie ciag
			targetLink = itemList[1];
			name = itemList[0];

			if ( name == "." || name.isEmpty() || (! mGettingURL && name == "..") )
				continue;

			itemList = QStringList::split( " ", (mGettingURL) ? filesList[i] : mMainFilesList[i] );

			uie.setPermissionsStr( itemList[0].right(9) ); // conversion to int (result in the 'uie' obj.)

			isDir = (itemList[0].at(0) == 'd');
			// --- check whether file is readable and executable
			if ( itemList[2] == mUserName ) {
				isReadable = (itemList[0].at(1) == 'r'); // user can to read
				isExecutable = (itemList[0].at(3) == 'x'); // user can to execute
			}
			else {
				isReadable = (itemList[0].at(7) == 'r'); // other can to read
				isExecutable = (itemList[0].at(9) == 'x'); // other can to execute
			}
			// --- get size
			size = itemList[4].toLongLong();

		// --- this part is come from the QFtp class (Qt-3.2.3) (little modified by author)
			// --- set date and time
			QString dateStr = "Sun "+itemList[ 5 ]+" "+itemList[ 6 ]+" ";

			if ( itemList[ 7 ].contains( ":" ) ) {
				time = QTime( itemList[ 7 ].left( 2 ).toInt(), itemList[ 7 ].right( 2 ).toInt() );
				dateStr += QString::number( QDate::currentDate().year() );
			} else {
				dateStr += itemList[ 7 ];
			}

			date = QDate::fromString( dateStr );
			uie.setLastModified( QDateTime( date, time ) );

			if ( itemList[ 7 ].contains( ":" ) ) {
				if( uie.lastModified().secsTo( QDateTime::currentDateTime() ) < -futureTolerance ) {
					QDateTime dt = uie.lastModified();
					QDate d = dt.date();
					d.setYMD(d.year()-1, d.month(), d.day());
					dt.setDate(d);
					uie.setLastModified(dt);
				}
			}
		// ---
			if ( mGettingURL ) // get file name from first line
				name = filesList[0].mid( 10, filesList[0].length()-(1+10) ); // 14 - begin pos.of name in first line
			else // is listing, check current link
			if ( mTargetFileForLinkLst.count() > 0 && itemList[0].at(0) == 'l' ) {
				QStringList itemLst;
				for ( uint i=0; i<mTargetFileForLinkLst.count(); i++ ) {
					itemLst = QStringList::split( '\n', mTargetFileForLinkLst[i] );
					if ( name == itemLst[0] ) {
						isDir = (itemLst[2] == "dir" );
						break;
					}
				}
			}
			//debug("name=%s", name.latin1());
			mNewUrlInfo = UrlInfoExt(
			 name, size, uie.permissions(),
			 itemList[2], itemList[3], uie.lastModified(), uie.lastModified(),
			 isDir, !isDir, (itemList[0].at(0) == 'l'),
			 isReadable, isExecutable, (size == 48), targetLink
			);

			if ( mGettingURL || mReadFile) {
				if ( mReadFile )
					mFileIsReadable = isReadable;
				else
				if ( mLinkChecking ) {
					QString linkItem = mTargetFileForLinkLst[mLinkCheckingCounter];
					linkItem += (isDir) ? "\ndir" : "\nfile";
					mTargetFileForLinkLst[mLinkCheckingCounter] = linkItem;
					mLinkCheckingCounter++;
					if ( mLinkCheckingCounter == mTargetFileForLinkLst.count() ) {
						mGettingURL = FALSE;
						mLinkChecking = FALSE;
						runListingFilesAgain = TRUE;
					}
				}
				break;
			}
			else
				emit listInfo( mNewUrlInfo );
		} // for
	} // code == 211
	// code == 213 - SIZE command
	if ( runListingFilesAgain ) {
		slotRawCommandReply( 211, QString::null );
		return;
	}
	if ( ! mGettingURL && ! mLinkChecking )
		slotCommandFinished( QFtp::List, FALSE );
}


void FtpFS::slotSetProgress( int done, int total )
{
	if ( ! total )
		return;
	if ( ! done && total )
		return;

	int td = (done) ? (100 * done) / total : 100;
	debug("FtpFS::slotSetProgress, done=%d b, total=%d, done=%d %%, mOpFileCounter=%d", done, total, td, mOpFileCounter );
	QString fName = mCopiedFilesList[(mOpFileCounter) ? mOpFileCounter-1 : mOpFileCounter]; // dla Put jest tu plik z lokalnego, dla Remove plik z serw.
	bool isFile = (fName.at(fName.length()-1) != '/');
	emit signalFileCounterProgress( mOpFileCounter, isFile, FALSE );
	if ( mTotalFiles )
		emit signalTotalProgress( (100 * mOpFileCounter) / mTotalFiles, 0 ); // 0 - weight of all (is unknown)

	if ( mFtp->currentCommand() != QFtp::Remove ) {
		emit signalDataTransferProgress( done, total, done-mRealWritedBytes );
		mRealWritedBytes = done;
	}
}


void FtpFS::slotReadFileToView( const QString & /*fileName*/, QByteArray & buffer, int & bytesRead, int fileLoadMethod )
{
	int bytesAvailable = mFtp->bytesAvailable();
	debug("FtpFS::slotReadFileToView, fileLoadMethod=%d, bytesAvailable=%d, bytesRead=%d", fileLoadMethod, bytesAvailable, bytesRead );
	if ( bytesAvailable == 0 ) {
		bytesRead = 0;
		return;
	}
	else
	if ( fileLoadMethod == View::ALL || bytesRead < 0 ) {
		buffer    = mFtp->readAll();
		bytesRead = buffer.size()+1;
		buffer.resize( bytesRead );
	}
	else
	if ( fileLoadMethod == View::APPEND ) {
		buffer.resize( bytesAvailable+1 ); // +1 for null character (need to text files)
		int bRead = mFtp->readBlock( buffer.data(), bytesAvailable );

		if ( bRead < 0 ) // an error occures
			slotDone( TRUE );

		bytesRead = bRead;
	}
}


void FtpFS::slotGetUrlInfo( const QString & fullFileName, UrlInfoExt & urlInfoExt )
{
	urlInfoExt.setName( fullFileName );

	// tymczasowe rozw., bo ponizsze info trzeba pobierac wprost z FSu
	UrlInfoExt urlInfo;
	slotGetUrlInfoFromList( fullFileName, urlInfo );
	urlInfoExt.setSize( urlInfo.size() );
	urlInfoExt.setLastModified( urlInfo.lastModified() );
}


void FtpFS::slotUpdateItemOnLV( const QString & /*fileName*/ )
{
//	slotCommandFinished( Put, touch(fileName) );
}

/*
	void uploadFile( const QString & );
	void downloadFile( const QString & );
void FtpFS::uploadFile( const QString &fileName )  {}
void FtpFS::downloadFile( const QString &fileName ) {}
*/

void FtpFS::slotQFtpListInfo(const QUrlInfo & ui)
{
	QString sName = ui.name();
	//debug("FtpFS::slotQFtpListInfo, name=%s", sName.latin1());
	if (sName != ".." && sName != ".")
		emit listInfo(
			UrlInfoExt(
				sName, ui.size(), ui.permissions(), ui.owner(), ui.group(), ui.lastModified(), ui.lastRead(),
				ui.isDir(), ui.isFile(), ui.isSymLink(), ui.isReadable(), ui.isExecutable(), FALSE, ""
			)
		);
}

void FtpFS::slotTimeOut()
{
	if (isLoggedIn()) {
		debug("FtpFS::slotTimeOut: Send PWD command and run timer (%d min.)", FTP_TIMEOUT/1000/60);
		mFtp->rawCommand( "PWD" );

		m_pTimer->start(FTP_TIMEOUT);
	}
}

