// ------ This file contains only functions for supports a mouse

#include "listviewext.h"

#include <qcursor.h>
#include <qtimer.h>


void ListViewExt::contentsMousePressEvent( QMouseEvent *e )
{
	mListViewFindItem->hide();
	// editBox obj. is hiden in the ListViewEditBox::focusOut function

	QListViewItem *previousItem = currentItem();
	QPoint vp = contentsToViewport( e->pos() );

	if ( ! itemAt( vp ) || numOfAllItems() == 0 )
		return;

	setCurrentItem( itemAt( vp ) );
	QListViewItem *currItem = currentItem();

	int y = itemPos( currItem ) - contentsY();
	int yMax = visibleHeight() - currItem->height();
	if ( y<0 || y>yMax  )
		ensureItemVisible( currItem );

	setFocus();

	if ( e->button() == LeftButton )  {
		if ( mKindOfView == TREEpview )
			emit signalPathChanged( absolutePathForItem(currItem) );

		// check is the user clicked on the root decoration of an item
		int x1 = (treeStepSize() * (currItem->depth()-1))+5;
		if ( currentItem()->isExpandable() && vp.x() > x1 && vp.x() < x1+11 )  {
			emit returnPressed( currentItem() );
			return;
		}

		mDragging = TRUE;

		if ( e->state() & ShiftButton )
			selectRange( previousItem, currItem );
		else
		if ( e->state() & (ShiftButton | ControlButton) )
			selectRange( previousItem, currItem, TRUE );
		else
		if ( e->state() & ControlButton )
			slotSelectCurrentItem();
	}
	else
	if ( e->button() == RightButton )
		showPopupMenu();
}


void ListViewExt::contentsMouseReleaseEvent( QMouseEvent * )
{
	if ( ! mDragging )
		return;

	mDragging = FALSE;

	if ( mScrollTimer )  {
		disconnect( mScrollTimer, SIGNAL(timeout()), this, SLOT(doAutoScroll()) );
		mScrollTimer->stop();
		delete mScrollTimer;
		mScrollTimer = 0;
	}
}


void ListViewExt::contentsMouseMoveEvent( QMouseEvent *e )
{
	if ( ! e || ! mDragging )
		return;

	bool needAutoScroll = FALSE;
	QPoint vp = contentsToViewport( QPoint(e->x(), e->y()) );

	// check, if we need to scroll
	if ( vp.y() > visibleHeight() || vp.y() < 0 )
		needAutoScroll = TRUE;
	else
		setCurrentItem( itemAt( vp ) );
/*
	mShiftButtonPressed = FALSE;
	if ( e->state() & ShiftButton )  {
		mShiftButtonPressed = TRUE;
		if ( currentItem() != mCurrentItem )
			slotSelectCurrentItem();
	}
*/
	// if we need to scroll and no autoscroll timer is started, connect the timer

	if ( needAutoScroll && ! mScrollTimer ) {
		mScrollTimer = new QTimer( this );
		connect( mScrollTimer, SIGNAL(timeout()), this, SLOT(slotDoAutoScroll()) );
		mScrollTimer->start( 100, FALSE );
		// call it once manually
		slotDoAutoScroll();
	}
	if ( ! needAutoScroll )
		slotDoAutoScroll();

	mCurrentItem = currentItem();
}

void ListViewExt::contentsMouseDoubleClickEvent( QMouseEvent *e )
{
	QPoint vp = contentsToViewport( e->pos() );

	if ( ! itemAt( vp ) || numOfAllItems(FALSE) == 0 )
		return;

	emit returnPressed( currentItem() );
}


void ListViewExt::slotDoAutoScroll()
{
	QPoint pos = QCursor::pos();
	pos = viewport()->mapFromGlobal( pos );
// 	pos = mapFromGlobal( pos );
	QListViewItem *current_item = currentItem();

	if ( pos.y() > visibleHeight() )  {  // if cursor out of up visible range
		if ( currentItem()->itemBelow() )
			current_item = currentItem()->itemBelow();
	}
	else
	if ( pos.y() < 0 )  {  // if cursor out of bottom visible range
		if ( currentItem()->itemAbove() )
			current_item = currentItem()->itemAbove();
	}

	ensureItemVisible( current_item );
	setCurrentItem( current_item );
/*
	if ( mShiftButtonPressed )  {
		if ( currentItem() != mCurrentItem )
			slotSelectCurrentItem();
	}
*/
}
