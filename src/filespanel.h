/***************************************************************************
                          filespanel.h  -  description
                             -------------------
    begin                : sun oct 27 2002
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/


#ifndef _FILESPANEL_H_
#define _FILESPANEL_H_

#include "keyshortcuts.h"
#include "listviewext.h"
#include "devicesmenu.h"
#include "urlinfoext.h"
#include "virtualfs.h"
#include "statusbar.h"
#include "pathview.h"
#include "enums.h"

#include "plugins/fp/settings/listviewsettings.h"
#include "plugins/fp/settings/otherappsettings.h"
#include "plugins/fp/settings/filesystemsettings.h"

#include <qtimer.h>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/**
 @short Klasa odpowiedzialna za obs�ug� panela plik�w.
 Tworzone s� tutaj obiekty sk�adowe panela, tj. widok �cie�ki, widok listy
 plik�w oraz pasek statusu. Inicjowany jest tutaj wygl�d panela. Zawiera
 obs�ug� klawiatury. Wyst�puj� tutaj funkcje pokazuj�ce r�ne dodatkowe
 informacje, tj. list� ulubionych, list� nap�d�w, statystyk� dla bie��cego
 dysku. Poza tym ma sloty, kt�re powoduj� zastosowanie ustawie� dla listy,
 system�w plik�w i innych ustawie� aplikacji. Mie�ci si� tutaj tak�e slot
 uruchamiany po ka�dym zako�czeniu operacji (rekurencyjnej i zwyk�ej), jego
 g��wnym zadaniem jest uaktualnianie widoku listy.
 */
class FilesPanel : public QWidget
{
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param initURL - �cie�ka lub nazwa pliku, kt�ry ma by� za�adowany do panelu,
	 @param parent - wska�nik na rodzica dla obiektu,
	 @param name - nazwa dla obiektu.\n
	 Inicjowane s� tu niezb�dne sk�adowe klasy, tworzone obiekty: widoku listy,
	 widoku �cie�ki oraz paska statusu, a tak�e inicjowany (asynchronicznie) widok
	 panela.
	 */
	FilesPanel( const QString & initURL, QWidget *parent, const char *name );

	/** Destruktor klasy.\n
	 Wywo�ywane s� tutaj funkcje powoduj�ce zapisanie ustawie� listy i panela do
	 pliku konfiguracyjnego oraz usuni�cie wszystkich utworzonych wcze�niej
	 obiekt�w.
	 */
	~FilesPanel();

	/** Zwraca informacj� o rodzaju systemu plik�w w panelu.\n
	 @return rodzaj systemu plik�w, patrz: @see KindOfFilesSystem.
	 */
	KindOfFilesSystem kindOfCurrentFS() const { return mVirtualFS ? mVirtualFS->kindOfCurrentFS() : UNKNOWNfs; }

	/** Zwraca rodzaj widoku w panelu.
	 @return rodzaj widoku, patrz: @see KindOfListView.
	*/
	int kindOfView() const { return mKindOfListView; }

	/** Zwraca informacj� o tym czy panel posiada focus.\n
	 @return TRUE oznacza posiadanie focus-a, FALSE - brak.
	 */
	bool hasFocus() const { return mVirtualFS ? mVirtualFS->hasFocus() : FALSE; }

	/** Ustawia focus dla panela (w�a�wicie dla widoku listy).
	 */
	void setFocus() { if ( mListViewExt )  mListViewExt->setFocus(); }


	/** Zwraca informacj� o �cie�ce do otwartego w panelu katalogu.\n
	 @return bie��ca �cie�ka.
	 */
	QString currentURL() const { return mVirtualFS ? mVirtualFS->currentURL() : QString("");  }

	/** Metoda powoduje zmian� rozmiaru podanej kolumny widoku listy plik�w.\n
	 @param section - numer kolumny, kt�rej jest zmieniany rozmiar,
	 @param oldSize - poprzedni rozmiar bie��cej kolumny,
	 @param newSize - nowy rozmiar bie��cej kolumny.\n
	 Fukncja wykorzystywana przy synchronizacji kolumn w obu widokach list.
	 */
	void resizeSection( uint section, uint oldSize, uint newSize ) { mListViewExt->slotHeaderSizeChange( section, oldSize, newSize ); }

	/** Metoda ustawia sk�adow� zawieraj�c� �cie�k� do panela obok.\n
	 @param url - nowa �cie�ka.
	 */
	void setPanelBesideURL( const QString & url ) { mPanelBesideURL = url; }

	/** Metoda przywracaj�ca geometri� przy ponownym pokazaniu widoku panela.\n
	 Konieczne jest jej wywo�anie po zmianie widoku z osadzonego podgl�du pliku
	 na widok panela.
	 */
	void updateView();

	/** Funkcja uruchamia procedur� kopiowania lub przenoszenia plik�w.\n
	 @param filesList - lista plik�w do kopiowania b�d� przenoszenia,
	 @param move - ustawiona na TRUE wymusza przenoszenie zamiast kopiowania.\n
	 Wywo�ywana jest tutaj tylko metoda @em mVirtualFS::copyFilesTo(), kt�rej
	 parametrami s�: bie��cy URL, lista plik�w oraz status przenoszenia.\n \n
	 */
	void copyFiles( const QStringList & filesList, bool move );

	/** Metoda powoduje uaktualnienie listy.\n
	 @param updateListOp - rodzaj uaktualnienia, patrz: @see UpdateListOperation,
	 @param list - wska�nik na list� element�w, kt�rymi nale�y uaktualni� bie��c�,
	 @param newUrlInfo - informacja o pliku (u�ywana je�li uaktualniany jest jeden
	 element).\n
	 Jest ona u�ywana do uakatualniania informacji w panelu obok.
	 */
	void updateList( int updateListOp, ListViewExt::SelectedItemsList * selectedItemsList, const UrlInfoExt & newUrlInfo ) {
		mListViewExt->updateList( updateListOp, selectedItemsList, newUrlInfo );
	}

	/** Zwraca wska�nik na list� zaznaczonych element�w.\n
	 @return wska�nik do listy zaznaczonych typu @em SelectedItemsList
	 */
	ListViewExt::SelectedItemsList * selectedItemsList() {
		return mListViewExt->selectedItemsList();
	}

	/** Ustawia wska�nik do obiektu skoja�e� plik�w.\n
	 @param fa - wska�nik do obiektu skoja�e� plik�w.
	 */
	void setFilesAssociation( FilesAssociation *fa ) {
		if ( mVirtualFS ) mVirtualFS->setFilesAssociation( (mFilesAssociation=fa) );
	}

	/** Ustawia wska�nik do obiektu skr�t�w klawiszowych panela plik�w.\n
	 @param keyShortcuts - wska�nika na obiekt skr�t�w klawiszowych.
	 */
	void setKeyShortcuts( KeyShortcuts * keyShortcuts ) {
		mKeyShortcuts = keyShortcuts;
	}

	/** Metoda uaktualnia menu zawieraj�ce list� filtr�w.\n
	 @param filters - wska�nik na obiekt filtr�w.
	 */
	void updateFiltersMenu( Filters * filters );

	/** Otwiera bie��cy plik (element na kt�rym stoi kursor) w podanym trybie.\n
	 @param editMode - tryb otwarcia pliku.
	 */
	void openCurrentFile( bool editMode ) {
		if ( mVirtualFS && mListViewExt ) mVirtualFS->openFile( mListViewExt->pathForCurrentItem(FALSE), editMode );
	}

	void createNewEmptyFile( bool createDir ) {
		if ( mVirtualFS ) mVirtualFS->createNewEmptyFile( createDir );
	}

private:
	PathView    *mPathView;
	StatusBar   *mStatusBar;
	ListViewExt *mListViewExt;
	FilesAssociation *mFilesAssociation;

	VirtualFS    *mVirtualFS;
	DevicesMenu  *mDevicesMenu;
	KeyShortcuts *mKeyShortcuts;

	QToolButton *mFilterButton;
	QPopupMenu  *mFiltersMenu;
	Filters *mFilters;

	uint mWidth;
	int  mKindOfListView;
	bool mShiftButtonPressed;
	QListViewItem *mCurrentItem;

	QString mInitUrl;
	QString mPanelBesideURL;
	QString mFinalPathOfTree;

	int  mFilterId;
	bool mFilterForFiltered;

	/** Metoda usuwa obiekt wirtualnego systemu plik�w ( @em mVirtualFS ).
	*/
	void removeFS();

	/** Funkcja inicjuje ustawienia panela.\n
	 Na pocz�tku inicjowana jest, warto�ci� z pliku konfiguracyjnego, sk�adowa
	 okre�laj�ca filtr dla listy plik�w w panelu. Ustawiany jest rodzaj sortowania
	 listy (w klasie listy) - domy�lnie po nazwie rosn�co. Okre�lane jest (w klasie
	 listy) czy focus ma by� stale widoczny (domy�lne ustawienie).
	 */
	void initPanel();

	/** Inicjowany jest tutaj (warto�ciami z pliku konfiguracyjnego) wygl�d listy.\n
	 Wczytywane s� nast�puj�ce ustawienia: informacje o kolumnach (kt�re b�d�
	 pokazywane i jaki rozmiar ma ka�da), font dla listy, informacja o pokazywaniu
	 ikon na li�cie, o nie chowaj�cym si� kursorze listy, o formacie rozmiar�w
	 plik�w, nazwa schematu kolor�w i warto�ci ich. W dalszej kolejno�ci
	 nast�puje ustawienia tych wszystkich w�asno�ci na li�cie.
	 */
	void initLookOfList();

	/** Inicjowana jest tutaj lista dla menu guzika z filtrami listy plik�w.
	 */
	void initFiltersButton();

	/** W metodzie tworzony jest nowy obiekt systemu plik�w.\n
	 @param url - �cie�ka do katalogu.\n
	 Na pocz�tku usuwana jest stara lista, po czym ustawiany (dla klasy listy)
	 filtr plik�w oraz rodzaj listy (zwyk�a/drzewo). Nast�pnie sprawdzany jest
	 docelowy rodzaj systemu plik�w, i podstawie tej informacji tworzony obiekt
	 klasy odpowiedniego systemu plik�w (LOCALfs, ARCHIVEfs, FTPfs, itp.).
	 Po bezproblemowym utworzeniu tego obiektu ustanawiane s� po��czenia sygna��w
	 ze slotami i wywo�ywana funkcja uaktualniaj�ca statusu panela.
	 */
	void createFS( const QString & url );

protected:
	/** Zapisywane s� tutaj niekt�re ustawienia panela.\n
	 Mianowicie informacje o: kolumnach, nieukrywaniu kursora podczas przesuwania
	 widoku listy, pokazywaniu ikon oraz rodzaju widoku.
	 */
	void saveSettings();

	/** Obs�uga zdarzenia zmiany rozmiaru panela.\n
	 Dostosowywane s� tutaj rozmiary element�w bie��cego widgetu, tj. widoku
	 �cie�ek, panela z list�/drzewem oraz widgetu z info o li�cie ('mStatusBar').
	 Uruchamiana jest te� procedura uaktualniaj�ca status listy plik�w.
	 */
	void resizeEvent( QResizeEvent * );

	/** Metoda zawiera obs�ug� klawiatury dla panelu.\n
	 */
	void keyPressEvent( QKeyEvent *k );

public slots:
	/** Slot powoduje zmian� bie��cego widoku na podany.\n
	 @param kindOfListView - rodzaj widoku, patrz: @see KindOfListView.
	*/
	void slotChangeKindOfView( uint kindOfListView );

	/** Slot powoduje pokazanie dialogu menad�era ulubionych.
	 */
	void slotShowFavoriteManager();

	/** Slot powoduje pokazanie dialogu menad�era filtr�w dla plik�w.
	 */
	void slotShowFiltersManager();

	/** Powoduje dodanie bie��cego katalogu do ulubionych.
	 */
	void slotAddCurrentToFavorites();

	/** Slot powoduje pokazanie, menu z prost� statystyk� dotycz�c� bie��cego
	 dysku.\n Wyst�puj� tam nast�puj�ce warto�ci:
	  @li ca�kowita ilo�c wolnych bajt�w
	  @li ca�kowita ilo�� dost�pnych bajt�w.\n
	 Przy czym warto�ci te s� zaokr�glane do liczby, kt�ra ma wag� (KB/MB/GB) i
	 dw�ch miejsc po przecinku. Ostatnia opcja pojazuje statystyk� dla wszystkich
	 dost�pnych dysk�w. Menu pokazywane jest w lewym g�rnym rogu panelu.
	 */
	void slotShowSimpleDiscStat();

	/** Slot wykonuje tzw. szybkie wyj�cie z systemu plik�w r�nego ni� lokalny.\n
	 Odczytywana jest w tym celu poprzednia (w histori) �cie�ka z lokalnego
	 systemu plik�w.
	 */
	void slotQuickGetOutFromFS();

	/** Slot powoduje pokazanie dialogu menad�era po��cze� FTP.\n
	 Po wci�ni�ciu "Connect", wykonywana jest pr�ba po��czenia.
	 */
	void slotShowFtpManagerDlg();

	/** Slot powoduje ponowne odczytanie bie��cego katalogu.
	 */
	void slotRereadCurrentDir();

	/** Slot powoduje pokazanie menu z list� dost�pnych urz�dze� pami�ci masowej
	 (tj. nap�dy CDROM, HDD, itp.).\n
	 Menu jest pokazywane w lewym g�rnym rogu panelu.
	 */
	void slotShowDevicesMenu();

	/** Slot powoduje pokazanie menu zawieraj�ce list� ulubionych katalog�w i opcj�
	 do jej konfiguracji.\n
	 Menu jest pokazywane w lewym g�rnym rogu bie��cego panelu.
	 */
	void slotShowFavorites();

	/** Slot powoduje wywo�anie dialogu wyszukiwania pliku.
	 */
	void slotFindFile();


	/** Slot powoduje zmian� bie��cego katalogu na podany.\n
	 @param inputURL - nowa �cie�ka do katalogu.\n
	 Przy czym, je�li �cie�ka wskazuje na inny ni� bie��cy system plik�w, wtedy
	 nast�puje jego utworzenie. Slot obs�uguje sygna� z klasy PathView.
	*/
	void slotPathChanged( const QString & inputURL );


	/** Slot uruchamia procedur� usuwania zaznaczonych plik�w.\n
	 Wywo�ywana jest tutaj tylko metoda: @see VirtualFS::removeFiles().
	 */
	void slotDelete();

	/** Slot uruchamia procedure tworzenia linku dla bie��cego pliku.\n
	 Wywo�ywana jest tutaj tylko metoda: @em VirtualFS::makeLink() z parametrem,
	 kt�ry jest �cie�k� do katalogu w s�siednim panelu.
	 */
	void slotMakeLink();

	/** Slot uruchamia procedure edycji linku dla bie��cego pliku.\n
	 Wywo�ywana jest tutaj tylko metoda: @em VirtualFS::makeLink() z parametrem,
	 kt�ry jest �cie�k� do katalogu w s�siednim panelu.
	 */
	void slotEditLink();

	/** Slot powoduje pokazanie dialogu w�a�ciwo�ci dla wybranych plik�w.\n
	 W tym celu wywo�ywana jest metoda: @em mVirtualFS::showPropertiesDialog().
	 */
	void slotShowProperties();

	/** Slot uruchamia procedur� otwarcia bie��cego pliku lub katalogu.\n
	 Wywo�ywany jest w momencie wci�ni�cia klawiszy Enter/Return, podw�jnym
	 klikni�ciu na bie��cy element listy lub klikni�ciu w plusik wy�wietlany
	 na widoku drzewa przy nazwie katalogu. Uruchamia procedur� otwarcia tego
	 elementu listy. Absolutn� �cie�k� inicjowana jest sk�adowa
	 @em mFinalPathOfTree oraz wywo�ywana @em metoda mListViewExt::open(), kt�rej
	 parametrem jest powy�sza �cie�ka.
	 */
	void slotReturnPressed( QListViewItem * );

	/** Funkcja inicjuje i ewentualnie uruchamia operacje kopiowania.\n
	 @param shiftPressed - r�wny TRUE oznacza, ze przy wywo�aniu slotu trzymany
	 by� klawisz Shift. Wymusza on kopiowanie do tego samego katalogu,
	 @param move - ustawiony na TRUE wymusza przenoszenie zamiast kopiowania.\n
	 Uruchamiana jest tutaj procedura kopiowania do bie��cego b�d� s�siedniego
	 panela (jest to zale�ne od systemu plik�w w bie��cym panelu).
	 */
	void slotCopy( bool shiftPressed, bool move );

	/** Slot powoduje otwarcie bie��cego pliku lub katalogu w s�siednim panelu.\n
	 @param dirForce - r�wny TRUE, wymusza otwarcie katalogu, w przeciwnym razie
	 nast�puje pr�ba otwarcia pliku.
	 */
	void slotOpenIntoPanelBeside( bool dirForce );


	/** Slot uruchamia procedur� otwarcia archiwum.\n
	 @param fileName - nazwa archiwum do otwarcia.\n
	 Przed otwraciem sprawdzane jest czy podany plik to archiwum.
	 */
	void slotOpenArchive( const QString & fileName );


	/** Slot wykonuje zastosowanie ustawie� dla widoku listy.\n
	 @param lvs - adres na obiekt z ustawieniami widoku listy.
	 */
	void slotApplyListViewSettings( const ListViewSettings & );

	/** Slot wykonuje zastosowanie ustawie� dla tzw. pozosta�ych ustawie�.\n
	 @param oas - adres na obiekt z tzw. pozosta�ymi ustawieniami.
	 */
	void slotApplyOtherAppSettings( const OtherAppSettings & oas );

	/** Slot wykonuje zastosowanie ustawie� dla systemu plik�w.\n
	 @param fss - adres na obiekt z ustawieniami systemu plik�w.
	 */
	void slotApplyFileSystemSettings( const FileSystemSettings & fss );

	/** Slot uruchamia procedur� wczytywania pliku do widoku.\n
	 @param fileName - nazwa pliku do wczytania,
	 @param buffer - adres bufora, w kt�rym nale�y umieszcza� wczytywane dane,
	 @param bytesRead - ilo�� wczytywanych na raz bajt�w, a po odczycie zawiera
	 ilo�� faktycznie wczytanych bajt�w (0 oznacza koniec pliku)
	 @param fileLoadMethod - metoda wczytywania pliku, patrz @see View::UpdateMode.\n
	 U�ywany do wczytywania pliku otwartego w panelu obok.
	 */
	void slotReadFileToView( const QString & fileName, QByteArray & buffer, int & readBlockSize, int fileLoadMethod ) {
		if ( mVirtualFS ) mVirtualFS->slotReadFileToView( fileName, buffer, readBlockSize, fileLoadMethod );
	} // need for load file from panel beside

private slots:
	/** Slot s�u�y do inicjowania widoku listy.\n
	 Je�li widokiem ma by� FTP, wtedy pokazywany jest dialog po��czenia, a po jego
	 zatwierdzeniu nast�puja pr�ba po��czenia. Je�li widokniem nie b�dzie FTP, wtedy
	 otwierany jest katalog, kt�rego nazwa zawarta jest w sk�adowej @em mInitUrl.
	 Slot wywo�ywany asynchronicznie z konstruktora.
	 */
	void slotInitPanelList();

	/** Slot uruchamia procedure tzw. szybkiej zmiany atrybut�w dla bie��cego
	 pliku.\n
	 @param column - kolumna w kt�rej nale�y zmieni� warto�� atrybutu,
	 @param oldText - aktualny tekst z podanej kolumny,
	 @param newText - nowy tekst dla podanej kolumny.\n
	 Wywo�ywana jest tutaj metoda @em mVirtualFS::changeAttribut(), parametrami
	 podanymi do tego slotu.
	 */
	void slotRenameCurrentItem( int column, const QString & oldText, const QString & newText );

	/** Slot obs�uguj�cy zako�czenie operacji na plikach.\n
	 @param operation - zako�czona operacja,
	 @param noError - status nie wyst�pienia b��du w zako�czonej operacji.\n
	 Uruchamiane s� czynno�ci, kt�re nale�y wykona� po zako�czeniu,
	 operacji, jest to min. uaktualnianie listy w obydwu panelach.
	 */
	void slotResultOperation( Operation, bool );

	/** Slot inicjuj� parametr informacj� o tym czy w bie��cym katalog aktualnie
	 zalogowany u�ytkownik mo�e zapisywa�.\n
	 @param dw - zmienna, w kt�rej zostanie zapisana informacja.\n
	 Ze slotu tego korzysta obiekt klasa PathView w celu wybrania koloru t�a dla
	 siebie.
	 */
	void slotPossibleDirWritable( DirWritable & );

	/** Slot powoduje ustawienie podanej �cie�ki w widoku �cie�ki.\n
	 @param path - �cie�ka do ustawienia.\n
	 Inicjowana jest r�wnie� sk�adowa klasy VirtualFS, okre�laj�ca bie��c� �cie�k�.
	 Slot u�ywany jest przez klas� ListViewExt, kiedy tam przy nawigacji po widoku
	 drzewa konieczna jest zmiana bie��cej �cie�ki.
	 */
	void slotPathChangedForTheViewPath( const QString & path );

	/** Slot powoduje zastosowanie filtru o podany id dla listy plik�w.\n
	 @param filterId - id filtra, kt�ry nale�y ustawi�.
	 */
	void slotChangeFilter( int filterId );

signals:
	/** Sygna� wywo�ywany w momencie zmiany rozmiaru kolumny widoku listy.\n
	 @param section - numer kolumny, kt�rej rozmiar jest zmieniany,
	 @param oldSize - poprzedni rozmiar kolumny,
	 @param newSize - nowy rozmiar kolumny.
	 */
	void signalHeaderSizeChange( int section, int oldSize, int newSize );

	/** Sygna� po zako�czeniu odczytywania listy plik�w z nowego katalogu.\n
	 @param url - nowa �cie�ka dla panela.\n
	 Powoduje on uaktualnienie informacji w panelu obok o bie��cej �cie�ce.
	 */
	void signalSetPanelBesideURL( const QString & url );

	/** Sygna� uruchamia procedur� otwarcia pliku lub katalogu w panlu obok.\n
	 @param fileName - nazwa pliku lub katalogu do otwarcia.
	 */
	void signalOpenIntoPanelBeside( const QString & fileName );

	/** Sygna� powoduje pobranie informacji o systemie plik�w w panelu obok.\n
	 @param kofs - zmienna, kt�ra zostanie zainicjowana rodzajem systemu plik�w.
	 */
	void signalGetKindOfFSInPanelBeside( KindOfFilesSystem & kofs );

	/** Sygna� uruchamia procedur� kopiowania lub przenoszenia inicjowan�
	 z drugiego panela.\n
	 @param filesList - lista plik�w do skopiowania/przeniesienia,
	 @param move - r�wne TRUE wymusza operacj� przenoszenia, w przeciwnym razie
	 wykonane zostanie kopiownie.\n
	 Operacja taka jest wymagana dla system�w plik�w r�nych od lokalnego.
	 */
	void signalCopyFiles( const QStringList & filesList, bool move );

	/** Sygna� powoduje uaktualnienie listy w panelu obok.\n
	 @param updateListOp - rodzaj operacji uaktualniania, patrz:
	  @see ListViewExt::UpdateListOperation,
	 @param itemsList - wska�nik do wska�nika na list� z elementami do
	 uaktualnienia,
	 @param newUrlInfo - informacje o pliku (u�ywane je�li jest jeden pliku
	 uaktualniany).
	 */
	void signalUpdateSecondPanel( int updateListOp, QValueList<QListViewItem *> * selectedItemsList, const UrlInfoExt & );

	/** Sygna� powoduje zapisanie na podany wska�nik do listy, wska�nika do listy
	 zaznaczonych w panelu obok.\n
	 @param selectedItemsList - wska�nik do adresu listy.\n
	 Jest on wykorzystywany przez systemy plik�w r�zne od lokalnego.
	 */
	void signalGetSILfromPanelBeside( QValueList<QListViewItem *> *& selectedItemsList );

	/** Sygna� powoduje zastosowanie zmienionych skr�t�w klawiszowych w oknie
	 g��wnym aplikacji.\n
	 @param keyShortcuts - wska�nik na obiekt skr�t�w klawiszowych.
	 */
	void signalApplyKeyShortcut( KeyShortcuts * keyShortcuts );

};

#endif
