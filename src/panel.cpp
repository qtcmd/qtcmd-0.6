/***************************************************************************
                          panel.cpp  -  description
                             -------------------
    begin                : fri jun 27 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "panel.h"

#include "plugins/fp/settings/listviewsettings.h"
#include "plugins/fp/settings/otherappsettings.h"
#include "plugins/fp/settings/filesystemsettings.h"

#include <qapplication.h> // for 'restoreOverrideCursor()'
#include <qsettings.h>
#include <unistd.h>


Panel::Panel( uint w, uint h, QWidget *parent, QWidget *preParent, const char *name )
	: QWidget( parent, name ),
	  mFilesPanel(NULL), mFileView(NULL), myParent(preParent), mFilesAssociation(NULL),
	  mKindOfPanel(UNKNOWNpanel), mWidth(w)
{
	setName(name);
	resize( w, h );
}


Panel::~Panel()
{
	debug("Panel, destruktor");
	QSettings *settings = new QSettings();
	QString side = QString(name()).left( QString(name()).find('P') );
	int width = 0;

	if ( mFileView ) {
		width = mFileView->width();
		settings->writeEntry( "/qtcmd/"+side+"FileView/URL", mFileView->fileName() );
	}
	else
		width = mFilesPanel->width();

	settings->writeEntry( "/qtcmd/"+side+"Panel/Width", width );
	settings->writeEntry( "/qtcmd/"+QString(name())+"/KindOfPanel", mKindOfPanel );

	delete settings;

	removePanel();
}


int Panel::kindOfView() const
{
	if ( mFilesPanel )
		return mFilesPanel->kindOfView();
	else
	if ( mFileView )
		return mFileView->kindOfView();

	return -1;
}


bool Panel::hasFocus() const
{
	if ( mFilesPanel )
		return mFilesPanel->hasFocus();
	else
	if ( mFileView )
		return mFileView->hasFocus();

	return FALSE;
}


void Panel::removePanel()
{
	if ( mFilesPanel != NULL ) {
		delete mFilesPanel;
		mFilesPanel = 0;
	}
	else
	if ( mFileView != NULL ) {
		delete mFileView;
		mFileView = 0;
	}
}


void Panel::createPanel( KindOfPanel _kindOfPanel, const QString & fileName )
{
	removePanel();
	QApplication::restoreOverrideCursor();

	QSettings *settings = new QSettings();
	QString side = QString(name()).left( QString(name()).find('P') );

	if ( _kindOfPanel == UNKNOWNpanel ) {
	// ----- read kind of panel
		mKindOfPanel = (KindOfPanel)settings->readNumEntry( "/qtcmd/"+side+"Panel/KindOfPanel", UNKNOWNpanel );
		if ( mKindOfPanel != FILESpanel && mKindOfPanel != FILEVIEWpanel )
			mKindOfPanel = FILESpanel; // defaultKindOfPanel;
	}
	else
		mKindOfPanel = _kindOfPanel;

	if ( mKindOfPanel == FILESpanel )  {
		delete settings;
		// !!! --- zamiast QString(name())+"_FP" podawac side+"_FP", wtedy trzeba inaczej wyluskac nazwe strony w FilesPanel
		QString sFileName = fileName;
		if (! sFileName.isEmpty()) {
			if (sFileName.at(sFileName.length()-1) != '/')
				sFileName += "/";
		}
		mFilesPanel = new FilesPanel( sFileName, this, QString(name())+"_FP" );

		connect( myParent, SIGNAL(signalSetNewKindOfView( uint )), mFilesPanel, SLOT(slotChangeKindOfView( uint )) );
		connect( myParent, SIGNAL(signalShowFtpManagerDlg()), mFilesPanel, SLOT(slotShowFtpManagerDlg()) );
		connect( myParent, SIGNAL(signalRereadCurrentDir()),  mFilesPanel, SLOT(slotRereadCurrentDir()) );
		connect( myParent, SIGNAL(signalShowFreeSpace()),     mFilesPanel, SLOT(slotShowSimpleDiscStat()) );
		connect( myParent, SIGNAL(signalDevicesList()),       mFilesPanel, SLOT(slotShowDevicesMenu()) );
		connect( myParent, SIGNAL(signalShowFavorites()),     mFilesPanel, SLOT(slotShowFavorites()) );
		connect( myParent, SIGNAL(signalQuickGetOutFromFS()), mFilesPanel, SLOT(slotQuickGetOutFromFS()) );
		connect( myParent, SIGNAL(signalFindFile()),          mFilesPanel, SLOT(slotFindFile()) );

		connect( myParent, SIGNAL(signalApplyListViewSettings(const ListViewSettings &)),
		 mFilesPanel, SLOT(slotApplyListViewSettings(const ListViewSettings &)) );
		connect( myParent, SIGNAL(signalApplyFileSystemSettings(const FileSystemSettings &)),
		 mFilesPanel, SLOT(slotApplyFileSystemSettings(const FileSystemSettings &)) );
		connect( myParent, SIGNAL(signalApplyOtherAppSettings(const OtherAppSettings &)),
		 mFilesPanel, SLOT(slotApplyOtherAppSettings(const OtherAppSettings &)) );

		connect( mFilesPanel, SIGNAL(signalHeaderSizeChange(int, int, int)), myParent, SLOT(slotHeaderSizeChange(int, int, int)) );
		connect( mFilesPanel, SIGNAL(signalSetPanelBesideURL(const QString &)), myParent, SLOT(slotSetPanelBesideURL(const QString &)) );
		connect( mFilesPanel, SIGNAL(signalOpenIntoPanelBeside( const QString & )), myParent, SLOT(slotOpenIntoPanelBeside( const QString & )) );

		connect( mFilesPanel, SIGNAL(signalCopyFiles(const QStringList &, bool)), myParent, SLOT(slotCopyFiles(const QStringList &, bool)) );
		connect( mFilesPanel, SIGNAL(signalGetKindOfFSInPanelBeside( KindOfFilesSystem & )), myParent, SLOT(slotGetKindOfFSInPanelBeside( KindOfFilesSystem & )) );
		connect( mFilesPanel, SIGNAL(signalUpdateSecondPanel(int, QValueList<QListViewItem *> *, const UrlInfoExt &)),
		 myParent, SLOT(slotUpdateSecondPanel(int, QValueList<QListViewItem *> *, const UrlInfoExt &)) );
		connect( mFilesPanel, SIGNAL(signalGetSILfromPanelBeside( QValueList<QListViewItem *> *& )),
		 myParent, SLOT(slotGetSILfromPanelBeside( QValueList<QListViewItem *> *& )) );
		connect( mFilesPanel, SIGNAL(signalApplyKeyShortcut(KeyShortcuts *)),
		 myParent, SLOT(slotApplyKeyShortcuts(KeyShortcuts *)) );

		mFilesPanel->resize( width(), height() );
		mFilesPanel->updateView();
		mFilesPanel->setFocus();
	}
	else
	if ( mKindOfPanel == FILEVIEWpanel )  {
		QString fName = fileName;
		if ( fileName.isEmpty() )
			fName = settings->readEntry( "/qtcmd/"+side+"FileView/URL" );
		delete settings;

		mFileView = new FileView( fName, 0, FALSE, this, QString(name())+"_VP" ); // FALSE == view mode
		connect( mFileView, SIGNAL(signalUpdateItemOnLV( const QString & )), myParent, SLOT(slotUpdateItemOnLV( const QString & )) );
		connect( mFileView, SIGNAL(signalClose()), myParent, SLOT(slotCloseView()) );
	}
}


void Panel::initLoadFileToView( FilesAssociation *fa, FilesPanel *fp )
{
	if ( ! mFileView || ! fp )
		return;

	mFileView->setFilesAssociation( fa );

	connect( mFileView, SIGNAL(signalReadFile(const QString &, QByteArray &, int &, int)),
	 fp, SLOT(slotReadFileToView(const QString &, QByteArray &, int &, int)) );

	mFileView->slotReadyRead();
}


void Panel::resizeEvent( QResizeEvent * )
{
	if ( mFilesPanel )
		mFilesPanel->resize( width(), height() );
	else
	if ( mFileView )
		mFileView->resize( width(), height() );

	setMinimumWidth( (mWidth < 200) ? mWidth : mWidth/2 );
}
