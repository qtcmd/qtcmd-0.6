/***************************************************************************
                          panel.h  -  description
                             -------------------
    begin                : sat oct 26 2002
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _PANEL_H_
#define _PANEL_H_

#include "filespanel.h"
#include "fileview.h"
#include "enums.h"

#include <qwidget.h>
#include <qstringlist.h>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa bazowa dla obiektu panela.
 Tworzony jest tutaj i usuwany podany panel (listy plik�w lub podgl�d pliku).
 Klasa zawiera wiele metod po�rednicz�cych pomi�dzy obiektem klasy nadrz�dnej
 tj. rodzica dla bie��cej (np. QtCmd) oraz podrz�dnej - potomka (np. FilesPanel)
 Metody wspomagaj� komunikacj� pomi�dzy panelami tych samych typ�w.
*/
class Panel : public QWidget
{
public:
	/** Konstruktor klasy.\n
	 @param w - szeroko�� panelu,
	 @param h - wysoko�� panelu,
	 @param parent - wka�nik na rodzica panelu (obiekt klasy QSplitter),
	 @param preParent - wka�nik na rodzica dla rodzica panelu (obiekt klasy QtCmd)
	 @param name - nazwa dla obiektu.\n
	 Inicjalizowane s� tutaj sk�adowe, oraz ustawiany rozmiar panela.
	 */
	Panel( uint w, uint h, QWidget *parent, QWidget *preParent=0, const char * name=0 );

	/** Destruktor klasy.\n
	 Zapisywane s� tutaj do pliku konfiguracyjnego ustawienia takie jak:
	 je�li bie��cym panelem jest podgl�d pliku - to jego absolutna nazwa,
	 je�li bie��cym panelem jest lista plik�w - to jego szeroko�� oraz rodzaj
	 widoku (drzewo/lista). Usuwany jest tak�e obiekt panela.
	 */
	~Panel();

	/** Podaje typ bie��cego panela.\n
	 @return typ panela, patrz @see KindOfPanel.
	 */
	KindOfPanel kindOfPanel() const { return mKindOfPanel; }

	/** Zwraca rodzaj widoku w bie��cym panelu.\n
	 @return rodzaj widoku, patrz: @see KindOfListView i @see View::KindOfView.
	 */
	int kindOfView() const;

	/** Zwraca informacj� o tym czy panel posiada focus.\n
	 @return TRUE oznacza posiadanie focusa, FALSE jego brak.
	 */
	bool hasFocus() const;

	/** Metoda powoduje utworzenie obiektu panela z list� plik�w lub z podgl�dem
	 pliku.\n
	 @param _kindOfPanel - typ panela, patrz: @see KindOfPanel,
	 @param fileName - nazwa pliku lub katalogu jakim nale�y zainicjowa� panel.\n
	 Rodzaj danego panela odczytywany jest z pliku konfiguracyjnego, po utworzeniu
	 panela ��czone s� sygna�y ze slotami.
	 */
	void createPanel( KindOfPanel kindOfPanel=UNKNOWNpanel, const QString & fileName=QString::null );

	/** Uruchamia kopiowanie lub przenoszenie plik�w.\n
	 @param filesList - lista nazw plik�w do skopiowania lub przeniesienia,
	 @param move - r�wne TRUE wymusza przenoszenie, w przeciwnym razie wykonywane
	 jest kopiowanie.
	 */
	void copyFiles( const QStringList & filesList, bool move ) {
		if ( mFilesPanel ) mFilesPanel->copyFiles( filesList, move );
	}

	/** Pobiera rodzaj systemu plik�w w bie��cym panelu.\n
	 @param kindOfFS - zmienna, w kt�rej umieszczany jest rodzaj FS-u.
	 */
	void getKindOfFS( KindOfFilesSystem & kindOfFS ) {
		if ( mFilesPanel ) kindOfFS = mFilesPanel->kindOfCurrentFS();
	}

	/** Powoduje zmian� rozmiaru podanej kolumny w bie��cym panelu.\n
	 @param section - numer kolumny, kt�re nale�y zmieni� rozmiar,
	 @param oldSize - poprzedni rozmiar podanej kolumny,
	 @param newSize - nowy rozmiar dla podanej kolumny.
	 */
	void resizeSection( int section, int oldSize, int newSize ) {
		if ( mFilesPanel ) mFilesPanel->resizeSection( section, oldSize, newSize );
	}

	/** Uruchamia odczytanie podanego katalogu.\n
	 @param pathName - nazwa katalogu od odczytania.
	 */
	void changeDirectory( const QString & pathName ) {
		if ( mFilesPanel ) mFilesPanel->slotPathChanged( pathName );
	}

	/** Ustawia sk�adow� klasy zawieraj�c� nazw� otwartego katalogu w panelu obok.\n
	 @param url - nazwa katalogu z panelu obok.
	 */
	void setPanelBesideURL( const QString & url ) {
		if ( mFilesPanel ) mFilesPanel->setPanelBesideURL( url );
	}

	/** Metoda uruchamia uaktualnianie listy plik�w.\n
	 @param updateListOp - rodzaj uaktualnienia, patrz:
	  @see ListViewExt::UpdateListOperation,
	 @param selectedItemsList - wska�nik na list� element�w, kt�rymi nale�y
	 uaktualni� bie��c�,
	 @param newUrlInfo - informacja o pliku (u�ywana je�li uaktualniany jest jeden
	 element).
	 */
	void updateList( int updateListOp, QValueList<QListViewItem *> *selectedItemsList, const UrlInfoExt & newUrlInfo ) {
		if ( mFilesPanel ) mFilesPanel->updateList( updateListOp, selectedItemsList, newUrlInfo );
	}

	/** Metoda pobiera wska�nik do listy zaznaczonych element�w w bie��cym panelu.\n
	 @param selectedItemsList - wska�nik, pod kt�ry nale�y zapisa� list� element�w
	 */
	void getSelectedItemsList( QValueList<QListViewItem *> *& selectedItemsList ) {
		if ( mFilesPanel ) selectedItemsList = mFilesPanel->selectedItemsList();
	}

	/** Inicjuje baz� skojarze� plik�w w bie��cym panelu podanym wska�nikiem do
	 obiektu skojarze�.
	 @param fa - wska�nik na obiekt skojarze� plik�w.
	 */
	void setFilesAssociation( FilesAssociation *fa ) {
		mFilesAssociation = fa;
		if ( mFilesPanel ) mFilesPanel->setFilesAssociation( fa );
		else
		if ( mFileView )   mFileView->setFilesAssociation( fa );
	}

	/** Metoda inicjuje skr�ty klawiszowe w panelu.\n
	 @param keyShortcuts - wska�nik na obiekt skr�t�w klawiszowych.
	 */
	void setKeyShortcuts( KeyShortcuts * keyShortcuts ) {
		if ( mFilesPanel ) mFilesPanel->setKeyShortcuts( keyShortcuts );
		else
		if ( mFileView )   mFileView->setKeyShortcuts( keyShortcuts );
	}

	/** Metoda uaktualnia menu zawieraj�ce list� filtr�w w panelu plik�w.\n
	 @param filters - wska�nik na obiekt filtr�w.
	 */
	void setFilters( Filters * filters ) {
		if ( mFilesPanel ) mFilesPanel->updateFiltersMenu( filters );
	}

	/** Podaje �cie�k� lub nazw� pliku w bie��cym panelu.\n
	 @return �cie�ka lub nazwa pliku.
	 */
	QString currentURL() const {
		return (mFilesPanel) ? mFilesPanel->currentURL() : mFileView->currentURL();
	}

	/** Podaje wska�nik do panela plik�w.\n
	 @return wska�nik do panela plik�w.
	 */
	FilesPanel *filesPanel() const { return mFilesPanel; }

	/** Metoda uruchamia �adowanie pliku do podgl�du osadzonego jako panel.\n
	 @param fa - wska�nik na obiekt bazy skojarze� plik�w,
	 @param fp - wska�nik na obiekt panela plik�w.\n
	 Poniewa� panel nie zawiera ob�ugi �adowania, wykorzystywany jest do tego celu
	 obiekt FilesPanel-a, do kt�rego jest tu przekazywany wska�nik. Inicjowane s�
	 tutaj te� skojarzenia plik�w na obiektcie panela podgl�du.
	 */
	void initLoadFileToView( FilesAssociation *fa, FilesPanel *fp );


	/** Otwiera bie��cy plik (lub aktualnie zaznaczone) w trybie podgl�du lub
	 edycji.\n
	 @param editMode - warto�� TRUE, wymusza tryb edycji, w przeciwnym razie
	  trybem b�dzie podgl�d.\n
	 Metoda u�ywana przez klas� rodzica.
	 */
	void viewFile( bool editMode ) {
		if ( mFilesPanel ) mFilesPanel->openCurrentFile( editMode );
	}

	/** Kopiuje lub przenosi bie��cy plik (lub aktualnie zaznaczone).\n
	 @param move - warto�� TRUE wymusza przenoszenie pliku, w przeciwnym razie
	  wykonywane jest kopiowanie.\n
	 Metoda u�ywana przez klas� rodzica.
	 */
	void copyFiles( bool move ) {
		if ( mFilesPanel ) mFilesPanel->slotCopy( FALSE, move );
	}

	/** Tworzy nowy katalog.\n
	 Metoda u�ywana przez klas� rodzica.
	 */
	void makeDir() {
		if ( mFilesPanel ) mFilesPanel->createNewEmptyFile( TRUE );
	}

	/** Usuwa bie��cy plik (lub aktualnie zaznaczone).\n
	 Metoda u�ywana przez klas� rodzica.
	 */
	void deleteFile() {
		if ( mFilesPanel ) mFilesPanel->slotDelete();
	}

	/** Tworzy link(i) do wybranych element�w.
	 */
	void makeLink() {
		if ( mFilesPanel ) mFilesPanel->slotMakeLink();
	}

private:
	FilesPanel *mFilesPanel;
	FileView   *mFileView;
	QWidget    *myParent;

	FilesAssociation *mFilesAssociation;

	KindOfPanel mKindOfPanel;
	/** Kind of current view for files panel and view panel */
	int mKindOfView;
	uint mWidth;


	/** Usuwany jest tutaj obiekt panela listy plik�w lub panela podgl�du pliku.
	 */
	void removePanel();

protected:
	/** Obs�uga zdarzenia zmiany rozmiaru dla obiektu panela.
	 */
	void resizeEvent( QResizeEvent * );

};

#endif
