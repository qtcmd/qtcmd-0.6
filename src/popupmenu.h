/***************************************************************************
                          popupmenu.h  -  description
                             -------------------
    begin                : Tue Aug 21 2001
    copyright            : (C) 2002, 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef POPUPMENU_H
#define POPUPMENU_H

#include "enums.h"

#include <qwidget.h>
#include <qlistview.h>
#include <qpopupmenu.h>


#define CMD_Separator          1
#define CMD_SubMenu            1000

// menu for file list/tree
#define CMD_Open               101
#define CMD_OpenInPanelBeside  102
#define CMD_Rename             103
#define CMD_Delete             104
#define CMD_Copy               105
#define CMD_MakeLink           106
#define CMD_Move               107
#define CMD_Weigh              108
#define CMD_Properties         109

#define CMD_SelectAll          110
#define CMD_UnSelectAll        111
#define CMD_SelectSameExt      112
#define CMD_UnSelectSameExt    113
#define CMD_SelectGroup        114
#define CMD_UnSelectGroup      115

#define CMD_CreateNewArchive   114
#define CMD_AddToArchive       115
#define CMD_ExtractArchive     116
#define CMD_ExtractTo          117
#define CMD_TestArchive        118


/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa obsluguje menu podr�czne panela plik�w.
 Inicjowane jest tutaj menu podr�czne standardowymi opcjami, po wybraniu opcji
 wysy�any jest odpowiedni sygna� dla niej.
*/
class PopupMenu : public QPopupMenu  {
   Q_OBJECT
public:
	/** Rodzaj menu.
	 */
	enum KindOfPopupMenu { FilesPanelMENU, UserFilesPanelMENU };

	/** Konstruktor klasy.\n
	 @param parent - wska�nik na rodzica,
	 @param kindOfPopupMenu - rodzaj menu, patrz: @see KindOfPopupMenu,
	 @param name - nazwa dla obiektu.\n
	 Inicjowane s� tu sk�adowe, inicjowane s� opcje dla menu oraz ��czone sygna�y
	 ze slotami.
	 */
	PopupMenu( QWidget *parent=0, KindOfPopupMenu kindOfPopupMenu=FilesPanelMENU, const char *name=0 );

	/** Destruktor.\n
	 Brak definicji.
	 */
	~PopupMenu();

	/** Pokazuje dialog menu na podanej pozycji.\n
	 @param globalPos - pozycja dla menu.
	 */
	bool showMenu( const QPoint & globalPos );

	/** Funkcja zawiera obs�ug� klawiatury dla menu.\n
	 @param e - zdarzenie klawiatury.
	 Zawiera podstawow� obs�g� klawiatury, tj. poruszanie kursorem
	 (g�ra/d�), litera skr�tu. Nie jest obs�ugiwane podmenu.
	 */
	void keyPressed( QKeyEvent * e );

private:
	QString mCurrentItemName;
	int mHighlightedItemNum;
	bool mMoveFile;

	/** Inicjowane s� tutaj opcje dla standardowego menu panela plik�w.
	 */
	void initFilesPanelMenu();

	/** Inicjowane s� tutaj opcje dla menu panela plik�w - zdefiniowane przez
	 u�ytkownika.
	 */
	void initUserMenu();

	/** Uruchamia akcje okre�lon� w menu.\n
	 @param cmd - kod polecenia z menu.\n
	 Wysy�ane s� tutaj odpowiadaj�ce opcj� sygna�y.
	 */
	void execCmd( int cmd );

private slots:
	/** Slot wywo�ywany po pod�wietleniu opcji w menu.\n
	 @param itemId - kod polecenia z menu.\n
	 Inicjowana jest tutaj tylko sk�adowa zawieraj�ca kod opcji.
	 */
	void slotItemHighlighted( int itemId );

//protected:
//	void keyPressed( QKeyEvent * );

signals:
	/** Sygna� wysy�any po wybraniu opcji 'Open'.\n
	 @param item - wska�nik na element do otwarcia (0 oznacza bie��cy).\n
	 Uruchamia procedur� otwarcia pliku lub katalogu.
	 */
	void signalOpen( QListViewItem * item );

	/** Sygna� wysy�any po wybraniu opcji 'OpenInPanelBeside'.\n
	 @param dirForce - TRUE oznacza otwarcie katalogu (je�li kursor nie stoi
	 na elemencie b�d�cym katalogiem, wtedy otwierany jest bie��cy), FALSE
	 powoduje otwarcie pliku w panelu obok.\n
	 Uruchamia procedur� otwarcia pliku lub katalogu w panelu obok.
	 */
	void signalOpenInPanelBeside( bool dirForce );

	/** Sygna� wysy�any po wybraniu opcji 'Rename'.
	 @param column - numer kolumny w kt�rej nale�y zmieni� warto�� (tutaj
	 zawsze podawane jest 0, oznaczaj�ce kolumn� z nazw�).\n
	 Uruchamia procedur� zmiany nazwy pliku.
	 */
	void signalRename( int column );

	/** Sygna� wysy�any po wybraniu opcji 'Delete'.\n
	 Uruchamia procedur� usuni�cia bie��cego elementu.
	 */
	void signalDelete();

	/** Sygna� wysy�any po wybraniu opcji 'MakeLink'.\n
	 Uruchamia procedur� utworzenia linku dla bie��cego elementu.
	 */
	void signalMakeLink();

	/** Sygna� wysy�any po wybraniu opcji 'Copy'.\n
	 @param shiftPressed - TRUE oznacza, �e wci�ni�ty jest klawisz Shift.
	 @param move - TRUE wymusza przenoszenie, w przeciwnym razie jest to
	 kopiowanie.\n
	 Uruchamia procedur� kopiowania lub przenoszenia zaznaczonych lub
	 bie��cego elementu.
	 */
	void signalCopy( bool shiftPressed, bool move );

	/** Sygna� wysy�any po wybraniu opcji 'SelectAll' lub 'UnSelectAll'.\n
	 @param select - TRUE oznacza, �e wykonywane b�dzie zaznaczenie, w przeciwnym
	 razie elementy zostan� odznaczone.\n
	 Uruchamia procedur� zaznaczenia lub odznaczenia wszystkich element�w.
	 */
	void signalSelectAll( bool select );

	/** Sygna� wysy�any po wybraniu opcji 'SelectSameExt'.\n
	 @param select - TRUE oznacza, �e wykonywane b�dzie zaznaczenie, w przeciwnym
	 razie elementy zostan� odznaczone.\n
	 Uruchamia procedur� zaznaczenia lub odznaczenia element�w z tym samym
	 rozszerzeniem, jakie ma bie��cy plik.
	 */
	void signalSelectSameExt( bool select );

	/** Sygna� wysy�any po wybraniu opcji 'SelectGroup' lub 'UnSelectGroup'.\n
	 @param select - TRUE oznacza, �e wykonywane b�dzie zaznaczenie, w przeciwnym
	 razie elementy zostan� odznaczone.\n
	 Uruchamia procedur� powoduj�c� pokazanie dialogu zaznacze� grupowych
	 (podawany jest tu wzorzec, kt�ry b�dzie dopasowywany do plik�w).
	 */
	void signalSelectGroup( bool select );

	/** Sygna� wysy�any po wybraniu opcji 'Properties'.\n
	 Uruchamia procedur� powoduj�c� pokazanie dialogu w�a�ciwo�ci dla zaznaczonych
	 lub bie��cego pliku.
	 */
	void signalShowProperties();

//	void signalCreateNewArchive();
//	void signalAddToArchive();
//	void signalExtractArchive();
//	void signalExtractTo();
//	void signalTestArchive();

};

#endif
