# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/view/sound
# Cel to biblioteka soundview

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
MOC_DIR = ../../../../.moc 
OBJECTS_DIR = ../../../../.obj 
TARGET = soundview 
DESTDIR = ../../../../.libs/plugins 
CONFIG += warn_on \
          qt \
          thread \
          plugin 
TEMPLATE = lib 
HEADERS += soundview.h \
           soundviewplugin.h \
           view.h 
SOURCES += soundview.cpp \
           soundviewplugin.cpp 
PRECOMPILED_HEADER = ../../../pch.h
