/***************************************************************************
                          soundview.h  -  description
                             -------------------
    begin                : mon nov 3 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _SOUNDVIEW_H_
#define _SOUNDVIEW_H_

#include "view.h"


class SoundView : public View {
// 	Q_OBJECT
public:
	SoundView( QWidget *parent, const char * name=0 );
	~SoundView() { delete mWidget; }

	View::ModeOfView modeOfView() { return View::RENDERmode; }

private:
	QWidget *mWidget;

protected:
	void setGeometry( int x, int y, int w, int h )  { mWidget->setGeometry( x,y, w,h ); }

};

#endif
