/***************************************************************************
                          soundview.cpp  -  description
                             -------------------
    begin                : mon nov 3 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "soundview.h"


SoundView::SoundView( QWidget *parent, const char * name )
	: View(parent,name)
{
	mWidget = new QWidget( parent );
}
