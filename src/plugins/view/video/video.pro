# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/view/video
# Cel to biblioteka videoview

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
MOC_DIR = ../../../../.moc 
OBJECTS_DIR = ../../../../.obj 
TARGET = videoview 
DESTDIR = ../../../../.libs/plugins 
CONFIG += warn_on \
          qt \
          thread \
          plugin 
TEMPLATE = lib 
HEADERS += videoview.h \
           videoviewplugin.h \
           view.h 
SOURCES += videoview.cpp \
           videoviewplugin.cpp 
PRECOMPILED_HEADER = ../../../pch.h
