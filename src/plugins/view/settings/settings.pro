# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/view/settings
# Cel to biblioteka settingsview

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
FORMS += ui/fontviewpage.ui \
         ui/keyshortcutsviewpage.ui \
         ui/othertextviewpage.ui \
         ui/syntaxhighlightingpage.ui \
         ui/filesassociationpage.ui \
         ui/toolbarviewpage.ui \
         ui/settingsviewdialog.ui 
TRANSLATIONS += ../../../../translations/pl/qtcmd-view-setts.ts 
HEADERS += settingsviewdialogplugin.cpp \
           ui/fontviewpage.ui.h \
           ui/keyshortcutsviewpage.ui.h \
           ui/othertextviewpage.ui.h \
           ui/syntaxhighlightingpage.ui.h \
           ui/filesassociationpage.ui.h \
           ui/toolbarviewpage.ui.h \
           ui/settingsviewdialog.ui.h \
           textviewsettings.h 
SOURCES += settingsviewdialogplugin.cpp 
DEPENDPATH = ui
TARGETDEPS += ../../../../.libs/libqtcmduiext.so \
../../../../.libs/libqtcmdutils.so
LIBS += -lqtcmduiext \
-lqtcmdutils
INCLUDEPATH += ../../../../src \
../../../../src/libs/qtcmdutils \
../../../../src/libs/qtcmduiext \
ui
MOC_DIR = ../../../../.moc
UI_DIR = ui
OBJECTS_DIR = ../../../../.obj
QMAKE_LIBDIR = ../../../../.libs \
../../../../.libs
TARGET = settingsview
DESTDIR = ../../../../.libs/plugins
CONFIG += warn_on \
qt \
thread \
plugin
TEMPLATE = lib
PRECOMPILED_HEADER = ../../../pch.h
