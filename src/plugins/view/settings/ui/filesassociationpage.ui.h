/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qsettings.h>
#include <qlistview.h>
#include <qfileinfo.h>
#include <qfiledialog.h>
#include <qinputdialog.h>

#include "extentions.cpp"
#include "messagebox.h"


void FilesAssociationPage::init()
{
	bool extViewer, nonSkip;
	QSettings settings;
	QStringList kindOfFiles;
	QString apps, desc, exts, ext, e;
	enum KindOfFiles { TEXT=0, DOCUMENTS, IMAGE, SOUND, VIDEO };
	kindOfFiles << tr("text") << tr("documents") << tr("image") << tr("sound") <<  tr("video");

	initDefaultDescriptionMap(); // inits mDefaultDescriptionMap

	QListViewItem *root = new QListViewItem( mKindOfFilesListView, tr("files") );
	root->setOpen( TRUE );
	root->setSelected( TRUE );
	root->setEnabled( FALSE );

	int j;
	QListViewItem *kofItem, *item;
	for ( uint i=0; i<kindOfFiles.count(); i++ ) {
		kofItem = new QListViewItem( root, kindOfFiles[i] );
		kofItem->setExpandable( TRUE );
		j = -1;
		if ( i == TEXT ) {
			QListViewItem *srcItem = new QListViewItem( kofItem, tr("sources") );
			srcItem->setExpandable( TRUE );
			while( (ext=sourceFileExt[++j]) != NULL ) {
				nonSkip = TRUE;
				if ( ext == "cpp" || ext == "cxx" || ext == "cc" || ext == "c" || ext == "C" )
					if ( ext == "cpp" )
						ext = "c_c++ src";
					else
						nonSkip = FALSE;
				if ( ext == "h" || ext == "hh" )
					if ( ext == "h" )
						ext = "c_c++ hdr";
					else
						nonSkip = FALSE;
				if ( ext == "adb" || ext == "ads" )
					if ( ext == "adb" )
						ext = "ada";
					else
						nonSkip = FALSE;
				if ( ext == "dif" || ext == "patch" || ext == "php3" || ext == "php4" || ext == "pl" || ext == "p" || ext == "pp" || ext == "dpr" || ext == "rb" || ext == "csh" || ext == "tk" )
						nonSkip = FALSE;
				if ( ext == "S" )
					ext = "asm";

				if ( nonSkip ) { // reads info from config file
					apps = settings.readEntry( "qtcmd_mime/"+ext+"/Application", "" );
					desc = settings.readEntry( "qtcmd_mime/"+ext+"/Description", "" );
					e = (ext == "c_c++ src" || ext == "c_c++ hdr" || ext == "diff" || ext == "php" || ext == "perl" || ext == "pas" || ext == "ada" || ext == "asm" || ext == "ruby" || ext == "sh" || ext == "tcl" ) ? QString("") : e = "*."+ext;
					exts = settings.readEntry( "qtcmd_mime/"+ext+"/FileTemplates", e );
					extViewer = settings.readBoolEntry( "qtcmd_mime/"+ext+"/ExternalViewer", FALSE );

					if ( exts.isEmpty() ) { // sets default extensions
						if ( ext == "c_c++ hdr" )
							exts = "*.h, *.hh";
						else
						if ( ext == "c_c++ src" )
							exts = "*.c, *.C, *.cpp, *.cxx, *.cc";
						else
						if ( ext == "diff" )
							exts = "*.dif, *.diff, *.patch";
						else
						if ( ext == "perl" )
							exts = "*.perl, *.pl";
						else
						if ( ext == "php" )
							exts = "*.php, *.php3, *.php4";
						else
						if ( ext == "pas" )
							exts = "*.p, *.pas, *.pp, *.dpr";
						else
						if ( ext == "ada" )
							exts = "*.adb, *.ads";
						else
						if ( ext == "ruby" )
							exts = "*.rb, .ruby";
						else
						if ( ext == "asm" )
							exts = "*.S";
						else
						if ( ext == "sh" )
							exts = "*.sh, *.csh";
						else
						if ( ext == "tcl" )
							exts = "*.tcl, *.tk";
					}
					if ( desc.isEmpty() )
						desc = mDefaultDescriptionMap[ ext ];
					item = new QListViewItem( srcItem, ext );
					mViewerInfoMap.insert( ext, exts+"\n"+desc+"\n"+apps+"\n"+(extViewer ? "ext" : "int")+"\nSource" );
				}
			}
			QListViewItem *docsItem = new QListViewItem( kofItem, tr("documents") );
			docsItem->setExpandable( TRUE );
			j = -1;
			while( (ext=renderFileExt[++j]) != NULL ) {
				nonSkip = TRUE;
				if ( ext.at(0).isDigit() )
					if ( ext == "1" )
						ext = "man";
					else
						nonSkip = FALSE;
				if ( ext == "htm" || ext == "shtml" || ext == "texi" )
					nonSkip = FALSE;

				if ( nonSkip ) { // reads info from config file
					apps = settings.readEntry( "qtcmd_mime/"+ext+"/Application", "" );
					desc = settings.readEntry( "qtcmd_mime/"+ext+"/Description", "" );
					e = (ext == "man" || ext == "html" || ext == "tex") ? QString("") : e = "*."+ext;
					exts = settings.readEntry( "qtcmd_mime/"+ext+"/Extentions", e );
					extViewer = settings.readBoolEntry( "qtcmd_mime/"+ext+"/ExternalViewer", FALSE );

					if ( exts.isEmpty() ) {
						if ( ext == "man" )
							exts = "*.1, *.2, *.3, *.4, *.5, *.6, *.7, *.8, *.9";
						else
						if ( ext == "html" )
							exts = "*.html, *.htm, *.shtml";
						else
						if ( ext == "tex" )
							exts = "*.tex, *.texi";
					}
					if ( desc.isEmpty() )
						desc = mDefaultDescriptionMap[ ext ];
					item = new QListViewItem( docsItem, ext );
					mViewerInfoMap.insert( ext, exts+"\n"+desc+"\n"+apps+"\n"+(extViewer ? "ext" : "int")+"\nRenderableText" );
				}
			}
		}
		else
		if ( i == DOCUMENTS ) {
			while( (ext=bindocFileExt[++j]) != NULL ) {
				item = new QListViewItem( kofItem, ext );
				// --- reads info from config file
				apps = settings.readEntry( "qtcmd_mime/"+ext+"/Application", "" );
				desc = settings.readEntry( "qtcmd_mime/"+ext+"/Description", "" );
				exts = settings.readEntry( "qtcmd_mime/"+ext+"/Extentions", "*."+ext );
				extViewer = settings.readBoolEntry( "qtcmd_mime/"+ext+"/ExternalViewer", FALSE );
				if ( desc.isEmpty() )
					desc = mDefaultDescriptionMap[ ext ];
				mViewerInfoMap.insert( ext, exts+"\n"+desc+"\n"+apps+"\n"+(extViewer ? "ext" : "int")+"\nBinaryDocument" );
			}
		}
		else
		if ( i == IMAGE ) {
			while( (ext=imageFileExt[++j]) != NULL ) {
				nonSkip = !( ext == "jpg" || ext == "djv" );

				if ( nonSkip ) { // reads info from config file
					apps = settings.readEntry( "qtcmd_mime/"+ext+"/Application", "" );
					desc = settings.readEntry( "qtcmd_mime/"+ext+"/Description", "" );
					e = (ext == "jpeg" || ext == "djvu") ? QString("") : e = "*."+ext;
					exts = settings.readEntry( "qtcmd_mime/"+ext+"/Extentions", e );
					extViewer = settings.readBoolEntry( "qtcmd_mime/"+ext+"/ExternalViewer", FALSE );

					if ( exts.isEmpty() ) { // sets default extensions
						if ( ext == "jpeg" )
							exts = "*.jpg, *.jpeg";
						else
						if ( ext == "djvu" )
							exts = "*.djv, *.djvu";
					}
					if ( desc.isEmpty() )
						desc = mDefaultDescriptionMap[ext];
					item = new QListViewItem( kofItem, ext );
					mViewerInfoMap.insert( ext, exts+"\n"+desc+"\n"+apps+"\n"+(extViewer ? "ext" : "int")+"\nImage" );
				}
			}
		}
		else
		if ( i == SOUND ) {
			while( (ext=soundFileExt[++j]) != NULL ) {
				nonSkip = TRUE;
				if ( ext == "mid" )
					nonSkip = FALSE;
				if ( ext == "mod" || ext == "s3m" || ext == "stm" || ext == "ult" || ext == "uni" || ext == "xm" || ext == "m15" || ext == "mtm" || ext == "669" || ext == "it" )
					if ( ext != "mod" )
						nonSkip = FALSE;
				if ( nonSkip ) { //  reads info from config file
					apps = settings.readEntry( "qtcmd_mime/"+ext+"/Application", "" );
					desc = settings.readEntry( "qtcmd_mime/"+ext+"/Description", "" );
					e = (ext == "midi" || ext == "mod") ? QString("") : e = "*."+ext;
					exts = settings.readEntry( "qtcmd_mime/"+ext+"/Extentions", e );
					extViewer = settings.readBoolEntry( "qtcmd_mime/"+ext+"/ExternalViewer", FALSE );

					if ( exts.isEmpty() ) { // sets default extensions
						if ( ext == "midi" )
							exts = "*.midi, *.mid";
						if ( ext == "mod" )
							exts = "*.mod, *.s3m, *.stm, *.ult, *.uni, *.xm, *.m15, *.mtm, *.669, *.it";
					}
					if ( desc.isEmpty() )
						desc = mDefaultDescriptionMap[ext];
					item = new QListViewItem( kofItem, ext );
					mViewerInfoMap.insert( ext, exts+"\n"+desc+"\n"+apps+"\n"+(extViewer ? "ext" : "int")+"\nSound" );
				}
			}
		}
		else
		if ( i == VIDEO ) {
			while( (ext=videoFileExt[++j]) != NULL ) {
				nonSkip = !( ext == "wmv" || ext == "mpg" || ext == "asx" || ext == "mov" || ext == "moov" || ext == "qtvr" || ext == "anim3" || ext == "anim5" || ext == "anim7" || ext == "flc" );

				if ( nonSkip ) { //  reads info from config file
					apps = settings.readEntry( "qtcmd_mime/"+ext+"/Application", "" );
					desc = settings.readEntry( "qtcmd_mime/"+ext+"/Description", "" );
					e = (ext == "avi" || ext == "mpeg" || ext == "asf" || ext == "qt" || ext == "iff" || ext == "fli") ? QString("") : e = "*."+ext;
					exts = settings.readEntry( "qtcmd_mime/"+ext+"/Extentions", e );
					extViewer = settings.readBoolEntry( "qtcmd_mime/"+ext+"/ExternalViewer", FALSE );
					if ( exts.isEmpty() ) { // sets default extensions
						if ( ext == "mpeg" )
							exts = "*.mpeg, *.mpg";
						else
						if ( ext == "asf" )
							exts = "*.asf, *.asx";
						else
						if ( ext == "qt" )
							exts = "*.qt, *.mov, *.moov, *.qtvr";
						else
						if ( ext == "iff" )
							exts = "*.iff, *.anim3, *.anim5, *.anim7";
						else
						if ( ext == "fli" )
							exts = "*.fli, *.flc";
						else
						if ( ext == "avi" )
							exts = "*.avi, *.wmv";
					}
					if ( desc.isEmpty() )
						desc = mDefaultDescriptionMap[ext];
					item = new QListViewItem( kofItem, ext );
					mViewerInfoMap.insert( ext, exts+"\n"+desc+"\n"+apps+"\n"+(extViewer ? "ext" : "int")+"\nVideo" );
				}
			}
		}
	}

	slotSelectionChanged( root );
	mAddNewKindpushBtn->hide(); // NNN (all support not yet implemented)
}


void FilesAssociationPage::slotGetApplication()
{
	QString appName = QFileDialog::getOpenFileName(
	 "/", tr("All files")+"(*)", this, 0,
	 tr("Select viewer")+" - QtCommander"
	);
	if ( appName.isEmpty() )
		return;

	if ( ! QFileInfo(appName).isExecutable() )
		MessageBox::critical( this, tr("This file is'nt executable !") );
	else
		mApplicationLineEdit->setText( appName );
}


void FilesAssociationPage::slotSelectionChanged( QListViewItem *item )
{
	bool enabled = (item->depth() > 1 && item->childCount() == 0);
	mViewerInfoGroupBox->setEnabled( enabled );
	mIntOrExtViewerBtnGroup->setEnabled( enabled );

	if ( ! enabled ) {
		mInternalViewerRadioBtn->setChecked( FALSE );
		mExternalViewerRadioBtn->setChecked( FALSE );
		mFileTemplatesLineEdit->clear();
		mDescriptionLineEdit->clear();
		mApplicationLineEdit->clear();
		return;
	}

	// --- get info about item and inits all edits
	QStringList infoList = QStringList::split( '\n', mViewerInfoMap[item->text(0)], TRUE );

	mInternalViewerRadioBtn->setChecked( (infoList[EXTVIEWER] == "int") );
	mExternalViewerRadioBtn->setChecked( (infoList[EXTVIEWER] == "ext") );

	mFileTemplatesLineEdit->setText( infoList[FILETEMPL] );
	mDescriptionLineEdit->setText( infoList[DESCRIPTION] );
	mApplicationLineEdit->setText( infoList[APPStoVIEW] );

	mFileTemplatesLineEdit->setCursorPosition( 0 );
	mDescriptionLineEdit->setCursorPosition( 0 );
	mApplicationLineEdit->setCursorPosition( 0 );

	slotEnableViewerInfo( (infoList[EXTVIEWER] == "ext") );
}

void FilesAssociationPage::slotEnableViewerInfo( int enabled )
{
	mDescriptionLineEdit->setReadOnly( ! enabled );
	mFileTemplatesLineEdit->setReadOnly( ! enabled );
	mApplicationLineEdit->setEnabled( enabled );
	mSelectAppToolBtn->setEnabled( enabled );

	changeMapValue( EXTVIEWER, (enabled ? "ext" : "int") );
}

void FilesAssociationPage::slotSave()
{
	QSettings settings;
	QStringList infoList;

	for ( ViewerInfoMap::Iterator it = mViewerInfoMap.begin(); it != mViewerInfoMap.end(); ++it ) {
		infoList = QStringList::split( '\n', it.data(), TRUE );

		settings.writeEntry( "/qtcmd_mime/"+it.key()+"/Application",    infoList[APPStoVIEW].isEmpty() ? "qtcmd" : infoList[APPStoVIEW] );
		settings.writeEntry( "/qtcmd_mime/"+it.key()+"/Description",    infoList[DESCRIPTION] );
		settings.writeEntry( "/qtcmd_mime/"+it.key()+"/FileTemplates",  infoList[FILETEMPL] );
		settings.writeEntry( "/qtcmd_mime/"+it.key()+"/ExternalViewer", (infoList[EXTVIEWER] == "ext") );
		settings.writeEntry( "/qtcmd_mime/"+it.key()+"/KindOfFile",     infoList[KINDofFILE] );
	}
}


void FilesAssociationPage::slotAddNewKind()
{
	QListViewItem *item = mKindOfFilesListView->currentItem();
	if ( item->childCount() != 0 )
		return;

	bool ok;
	QString kindName = QInputDialog::getText(
	 tr("Add a new kind of file")+" - QtCommander", tr("Please to give a name:"),
	 QLineEdit::Normal, tr("new kind"), &ok, this
	);
	if ( ! ok )
		return;

	(void) new QListViewItem( item->parent(), kindName );
//	mDefaultDescriptionMap.insert( kindName, desc );
//	mViewerInfoMap.insert( ext, exts+"\n"+desc+"\n"+apps+"\n"+(extViewer ? "ext" : "int")+"kindOfFile" );
}

void FilesAssociationPage::slotTextChanged( const QString & text )
{
	QString senderName = sender()->name();

	if ( senderName == "mDescriptionLineEdit" )
		changeMapValue( DESCRIPTION, text );
	else
	if ( senderName == "mFileTemplatesLineEdit" )
		changeMapValue( FILETEMPL, text );
	else
	if ( senderName == "mApplicationLineEdit" )
		changeMapValue( APPStoVIEW, text );
}

void FilesAssociationPage::initDefaultDescriptionMap()
{
	// --- source files
	mDefaultDescriptionMap.insert( "c_c++ src", tr("C or C++ source file") );
	mDefaultDescriptionMap.insert( "c_c++ hdr", tr("C or C++ header file") );
	mDefaultDescriptionMap.insert( "perl", tr("Script in Perl") );
	mDefaultDescriptionMap.insert( "sh", tr("Shell script") );
	mDefaultDescriptionMap.insert( "py", tr("Python script ") );
	mDefaultDescriptionMap.insert( "java", tr("Java source file") );
	mDefaultDescriptionMap.insert( "php", tr("PHP script") );
	mDefaultDescriptionMap.insert( "css", tr("Cascade Style Sheet file") );
	mDefaultDescriptionMap.insert( "xml", tr("XML script") );
	mDefaultDescriptionMap.insert( "xslt", tr("XSLT Style Sheet file") );
	mDefaultDescriptionMap.insert( "sql", tr("SQL script") );
	mDefaultDescriptionMap.insert( "asm", tr("Assembler source file") );
	mDefaultDescriptionMap.insert( "wml", tr("WML script") );
	mDefaultDescriptionMap.insert( "awk", tr("AWK script") );
	mDefaultDescriptionMap.insert( "pas", tr("Pascal source file") );
	mDefaultDescriptionMap.insert( "moc", tr("Qt source MOC file") );
	mDefaultDescriptionMap.insert( "tcl", tr("TCL script") );
	mDefaultDescriptionMap.insert( "tk", tr("TK script") );
	mDefaultDescriptionMap.insert( "ada", tr("ADA source file") );
	mDefaultDescriptionMap.insert( "sgml", tr("SGML script") );
	mDefaultDescriptionMap.insert( "ruby", tr("RUBY script") );
	mDefaultDescriptionMap.insert( "um", tr("Cascade Style Sheet") );
	mDefaultDescriptionMap.insert( "diff", tr("Differents between two files") );
	mDefaultDescriptionMap.insert( "ts", tr("Source translations for Qt's program") );

	// --- renderable files
	mDefaultDescriptionMap.insert( "html", tr("HTML document") );
	mDefaultDescriptionMap.insert( "rtf", tr("RTF document") );
	mDefaultDescriptionMap.insert( "latex", tr("LaTeX document") );
	mDefaultDescriptionMap.insert( "tex", tr("TeX document") );
	mDefaultDescriptionMap.insert( "docbook", tr("DocBook document") );
	mDefaultDescriptionMap.insert( "dvi", tr("DVI TeX file") );
	mDefaultDescriptionMap.insert( "ui", tr("Source of User Interface for Qt's programm") );
	mDefaultDescriptionMap.insert( "xmi", tr("UML Umbrello modeler file") );
	mDefaultDescriptionMap.insert( "man", tr("Manual textbook document") );

	// --- binary documents
	mDefaultDescriptionMap.insert( "pdf", tr("PDF document") );
	mDefaultDescriptionMap.insert( "ps", tr("PostScript document") );
	mDefaultDescriptionMap.insert( "doc", tr("Microsoft Word document") );
	mDefaultDescriptionMap.insert( "sxw", tr("OpenOffice.org text document") );
	mDefaultDescriptionMap.insert( "kwd", tr("KWord text document") );
	mDefaultDescriptionMap.insert( "xls", tr("Microsoft Excel document") );
	mDefaultDescriptionMap.insert( "sxc", tr("OpenOffice.org sheet") );
	mDefaultDescriptionMap.insert( "ksp", tr("KWord sheet") );
	mDefaultDescriptionMap.insert( "po", tr("Binary translations for Qt's program") );

	// --- image files
	mDefaultDescriptionMap.insert( "bmp", tr("BMP image") );
	mDefaultDescriptionMap.insert( "gif", tr("GIF image") );
	mDefaultDescriptionMap.insert( "jpeg", tr("JPEG image") );
	mDefaultDescriptionMap.insert( "png", tr("PNG image") );
	mDefaultDescriptionMap.insert( "jp2", tr("JPEG 2000 image") );
	mDefaultDescriptionMap.insert( "xpm", tr("X PixMap image") );
	mDefaultDescriptionMap.insert( "svg", tr("Scalable Vector Graphics") );
	mDefaultDescriptionMap.insert( "pcx", tr("PCX image") );
	mDefaultDescriptionMap.insert( "djvu", tr("D'Javu image") );
	mDefaultDescriptionMap.insert( "tiff", tr("TIFF image") );
	mDefaultDescriptionMap.insert( "tga", tr("Truevision Targa image") );
	mDefaultDescriptionMap.insert( "xbm", tr("X BitMap image") );
	mDefaultDescriptionMap.insert( "cgm", tr("Computer Graphics Metafile") );
	mDefaultDescriptionMap.insert( "eps", tr("EPS image") );
	mDefaultDescriptionMap.insert( "ico", tr("Microsoft Windows icons") );
	mDefaultDescriptionMap.insert( "pbm", tr("Portable BitMap image") );
	mDefaultDescriptionMap.insert( "pgm", tr("Portable GrayMap image") );
	mDefaultDescriptionMap.insert( "ppm", tr("Portable PixMap image") );
	mDefaultDescriptionMap.insert( "wmf", tr("Microsoft Windows Metafile") );
	mDefaultDescriptionMap.insert( "swf", tr("Shockwave Flash Media") );
	mDefaultDescriptionMap.insert( "spl", tr("Netscape Shockwave Flash") );
	mDefaultDescriptionMap.insert( "mng", tr("MNG image") );

	// -- sound files
	mDefaultDescriptionMap.insert( "wav", tr("WAV file") );
	mDefaultDescriptionMap.insert( "mp3", tr("MPEG Layer3 sound") );
	mDefaultDescriptionMap.insert( "ogg", tr("OGG file") );
	mDefaultDescriptionMap.insert( "cda", tr("Audio track of Compact Disk") );
	mDefaultDescriptionMap.insert( "midi", tr("MIDI file") );
	mDefaultDescriptionMap.insert( "mod", tr("Amiga sound file") );
	mDefaultDescriptionMap.insert( "au", tr("ULAW (Sun) audio file") );

	// -- video files
	mDefaultDescriptionMap.insert( "asf", tr("Microsoft Media Format") );
	mDefaultDescriptionMap.insert( "avi", tr("Microsoft AVI Video") );
	mDefaultDescriptionMap.insert( "divx", tr("DIVX video format") );
	mDefaultDescriptionMap.insert( "mpeg", tr("MPEG video format") );
	mDefaultDescriptionMap.insert( "qt", tr("Quicktime video format") );
	mDefaultDescriptionMap.insert( "iff", tr("IFF (Animation) video format") );
	mDefaultDescriptionMap.insert( "fli", tr("Autodesk FLIC file") );
}


void FilesAssociationPage::changeMapValue( KindOfFileInfo kofi, const QString & value )
{
	QString fileType = mKindOfFilesListView->currentItem()->text(0);
	QStringList infoList = QStringList::split( '\n', mViewerInfoMap[fileType].data(), TRUE );
	infoList[kofi] = value;

	mViewerInfoMap[fileType] =
	 infoList[FILETEMPL]+"\n"+
	 infoList[DESCRIPTION]+"\n"+
	 infoList[APPStoVIEW]+"\n"+
	 infoList[EXTVIEWER]+"\n"+
	 infoList[KINDofFILE]+"\n";
}
