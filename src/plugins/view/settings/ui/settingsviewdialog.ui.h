/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qcombobox.h>
#include "ui/settingsviewdialog.h"
#include "../text/syntaxhighlighter.h"

void SettingsViewDialog::init()
{
	setCaption( tr("View settings")+" - QtCommander" );

	// --- TextViewListBox pages
	mFontViewPage = new FontViewPage( mWidgetStack );
	mOtherTextViewPage = new OtherTextViewPage( mWidgetStack );
	mSyntaxhighlightingPage = new SyntaxHighlightingPage( mWidgetStack );

	mWidgetStack->removeWidget( mWidgetStack->widget(0) );
	mWidgetStack->addWidget( mFontViewPage, 0 );
	mWidgetStack->addWidget( mSyntaxhighlightingPage, 1 );
	mWidgetStack->addWidget( mOtherTextViewPage, 2 );

	// --- ImageViewListBox pages
	// --- VideoViewListBox pages
	// --- SoundViewListBox pages
	// --- BinaryViewListBox pages

	// --- OtherViewPage
	mKeyshortcutsViewPage = new KeyShortcutsViewPage( mWidgetStack );
	mFilesAssociationPage = new FilesAssociationPage( mWidgetStack );
	mToolBarViewPage = new ToolBarViewPage( mWidgetStack );

	mWidgetStack->addWidget( mFilesAssociationPage, 3 );
	mWidgetStack->addWidget( mKeyshortcutsViewPage, 4 );
	mWidgetStack->addWidget( mToolBarViewPage, 5 );

	mToolBox->setCurrentIndex( 0 );
	mTextViewListBox->setCurrentItem( 0 );
	resize( QSize(600, 454).expandedTo(minimumSizeHint()) );

	static QValueList <int>sSW; // width the children of spliter obj.
	sSW << (width()*25)/100 << 0;
	sSW[1] = width()-sSW[0];
	mSplitter->setSizes( sSW );
}


void SettingsViewDialog::slotSetKeyShortcuts( KeyShortcuts *keyShortcuts )
{
	if ( mKeyshortcutsViewPage )
		mKeyshortcutsViewPage->setKeyShortcuts( keyShortcuts );
}


void SettingsViewDialog::slotOkButtonPressed()
{
	slotApplyButtonPressed();
	slotSaveButtonPressed();
	close();
}

void SettingsViewDialog::slotApplyButtonPressed()
{
	if ( mCurrentKindOfView == View::TEXTview ) {
		TextViewSettings textViewSettings;
		textViewSettings.fontFamily = mFontViewPage->fontFamily();
		textViewSettings.fontSize   = mFontViewPage->fontSize();
		textViewSettings.fontBold   = mFontViewPage->fontBold();
		textViewSettings.fontItalic = mFontViewPage->fontItalic();
		textViewSettings.tabWidth   = mOtherTextViewPage->tabWidth();
		textViewSettings.externalEditor = mOtherTextViewPage->externalEditorPath();

		if ( mSyntaxhighlightingPage->highlightingChanged() ) {
			SHattribut newSHattribut;
			for ( int i=0; i<11; i++ ) {
				mSyntaxhighlightingPage->shAttribut( i, newSHattribut );
				textViewSettings.shAttrib[i].color  = newSHattribut.color;
				textViewSettings.shAttrib[i].bold   = newSHattribut.bold;
				textViewSettings.shAttrib[i].italic = newSHattribut.italic;
			}
		}
		SyntaxHighlighter::SyntaxHighlighting sh;
		int currentSH = mSyntaxhighlightingPage->currentHighlighting();
		if ( currentSH == 0 )
			sh = SyntaxHighlighter::CCpp;
		else
		if ( currentSH == 1 )
			sh = SyntaxHighlighter::HTML;
		else
			sh = SyntaxHighlighter::NONE;
		textViewSettings.currentSyntaxHighlighting = sh;

		emit signalApply( textViewSettings );
	}
/*
	else
	if ( mCurrentKindOfView == View::IMAGEview ) {
	}
	else
	if ( mCurrentKindOfView == View::SOUNDview ) {
	}
	else
	if ( mCurrentKindOfView == View::VIDEOview ) {
	}
	else
	if ( mCurrentKindOfView == View::BINARYview ) {
	}
*/
	// --- other page
	emit signalApplyKeyShortcuts( mKeyshortcutsViewPage->keyShortcuts() );
}


void SettingsViewDialog::slotSaveButtonPressed()
{
	mKeyshortcutsViewPage->slotSave();
	mFilesAssociationPage->slotSave();
	emit signalSaveSettings();
}

void SettingsViewDialog::slotSetCurrentKindOfView( View::KindOfView currentKindOfView )
{
	mCurrentKindOfView = currentKindOfView;
}

void SettingsViewDialog::slotRaiseWidget( int id )
{
	int offsetId = -1;
	QString title;
	QString senderName = sender()->name();

	if ( senderName == "mTextViewListBox" ) {
		title = mTextViewListBox->text(id);
		offsetId = 0;
	}
	else
	if ( senderName == "mImageViewListBox" ) {
		title = mImageViewListBox->text(id);
		offsetId = 3; // pages into previous pages
	}
	else
	if ( senderName == "mVideoViewListBox" ) {
		title = mVideoViewListBox->text(id);
		offsetId = 3; // pages into previous pages
	}
	else
	if ( senderName == "mSoundViewListBox" ) {
		title = mSoundViewListBox->text(id);
		offsetId = 3; // pages into previous pages
	}
	else
	if ( senderName == "mBinaryViewListBox" ) {
		title = mBinaryViewListBox->text(id);
		offsetId = 3; // pages into previous pages
	}
	else
	if ( senderName == "mOtherViewListBox" ) {
		title = mOtherViewListBox->text(id);
		offsetId = 3; // pages into previous pages
	}

	mWidgetStack->raiseWidget( id+offsetId );
	mTitleLab->setText( title );
}


void SettingsViewDialog::slotCurrentItemChanged( int itemId )
{
	int offsetId, id;
	QString title;

	if ( itemId == 0 ) { // "TextView"
		id = mTextViewListBox->currentItem();
		if ( id < 0 )
			mTextViewListBox->setCurrentItem( id=0 );
		title = mTextViewListBox->text( id );
		offsetId = 0;
	}
	else
	if ( itemId == 1 ) { // "ImageView"
// 		id = mImageViewListBox->currentItem();
// 		if ( id < 0 )
// 			mImageViewListBox->setCurrentItem( id=0 );
// 		title = mImageViewListBox->text( id );
// 		offsetId = 3; // pages into previous pages
	}
	else
	if ( itemId == 2 ) { // "VideoView"
// 		id = mVideoViewListBox->currentItem();
// 		if ( id < 0 )
// 			mVideoViewListBox->setCurrentItem( id=0 );
// 		title = mVideoViewListBox->text( id );
// 		offsetId = 3; // pages into previous pages
	}
	else
	if ( itemId == 3 ) { // "SoundView"
// 		id = mSoundViewListBox->currentItem();
// 		if ( id < 0 )
// 			mSoundViewListBox->setCurrentItem( id=0 );
// 		title = mSoundViewListBox->text( id );
// 		offsetId = 3; // pages into previous pages
	}
	else
	if ( itemId == 4 ) { // "BinaryView"
// 		id = mBinaryViewListBox->currentItem();
// 		if ( id < 0 )
// 			mBinaryViewListBox->setCurrentItem( id=0 );
// 		title = mBinaryViewListBox->text( id );
// 		offsetId = 3; // pages into previous pages
	}
	else
	if ( itemId == 5 ) { // "OtherView"
		id = mOtherViewListBox->currentItem();
		if ( id < 0 )
			mOtherViewListBox->setCurrentItem( id=0 );
		title = mOtherViewListBox->text( id );
		offsetId = 3; // pages into previous pages
	}

	mWidgetStack->raiseWidget( offsetId+id );
	mTitleLab->setText( title );
}
