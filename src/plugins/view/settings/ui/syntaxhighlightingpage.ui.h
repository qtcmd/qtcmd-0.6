/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include "functions_col.h"
#include "listviewwidgets.h"

#include <qsettings.h>

void SyntaxHighlightingPage::init()
{
	QSettings settings;

	QStringList highlightingList;// = settings.readListEntry( "/qtcmd/SyntaxHighlighting/HighlightingList" );
	highlightingList.append("Source/C,C++");
	highlightingList.append("Markup/HTML");

	if ( highlightingList.count() )
		mHighlightingComboBox->insertStringList( highlightingList );

	for ( int col=BoldCOL; col<UseDefaultCOL+1; col++ )
		mListViewWidgets->setColumnAlignment( col, Qt::AlignHCenter );

	mCCppMap.clear();
	mHtmlMap.clear();

	// --- init default highlightings
	// C/C++
	mCCppMap.append( "Base N Integer;(default)FF00FF,,;" );
	mCCppMap.append( "Character;(default)008080,,;" );
	mCCppMap.append( "Comment;(default)808080,,Italic;" );
	mCCppMap.append( "Data type;(default)800000,,;" );
	mCCppMap.append( "Decimal or Value;(default)0000FF,,;" );
	mCCppMap.append( "Extentions;(default)0095FF,Bold,;" );
	mCCppMap.append( "Floating point;(default)800080,,;" );
	mCCppMap.append( "Key word;(default)000000,Bold,;" );
	mCCppMap.append( "Normal;(default)000000,,;" );
	mCCppMap.append( "Preprocesor;(default)008000,,;" );
	mCCppMap.append( "String;(default)FF0000,,;" );

	// HTML (an empty items for void SettingsViewDialog::slotApplyButtonPressed())
	mHtmlMap.append( ";,,;" );
	mHtmlMap.append( ";,,;" );
	mHtmlMap.append( "Comment;(default)808080,,Italic;" );
	mHtmlMap.append( "Data type;(default)800000,,;" );
	mHtmlMap.append( "Decimal or Value;(default)0000FF,,;" );
	mHtmlMap.append( ";,,;" );
	mHtmlMap.append( ";,,;" );
	mHtmlMap.append( "Key word;(default)000000,Bold,;" );
	mHtmlMap.append( "Normal;(default)000000,,;" );
	mHtmlMap.append( ";,,;" );
	mHtmlMap.append( "String;(default)FF0000,,;" );

	// --- read from config file
	QString newSH;
	for ( uint i=0; i<mCCppMap.count(); ++i ) {
		newSH = settings.readEntry( "/qtcmd/SyntaxHighlighting/CC++/"+mCCppMap[i].section(';', 0,0) );
		if ( ! newSH.isEmpty() )
			mCCppMap += newSH;
	}
	for ( uint i=0; i<mHtmlMap.count(); ++i ) {
		newSH = settings.readEntry( "/qtcmd/SyntaxHighlighting/HTML/"+mHtmlMap[i].section(';', 0,0) );
		if ( ! newSH.isEmpty() )
			mHtmlMap += newSH;
	}
	// --- set default highlighting
	mInsertDefault = FALSE;
	slotSelectHighlighting( CCpp );

	connect( mListViewWidgets, SIGNAL(buttonClicked( int )),
	 this, SLOT(slotButtonClicked(int)) );
	connect( mListViewWidgets, SIGNAL(colorChanged( int )),
	 this, SLOT(slotColorChanged(int)) );
}

void SyntaxHighlightingPage::slotSelectHighlighting( int currentHighlighting )
{
	QStringList shMap;
	mListViewWidgets->clear();

	if ( currentHighlighting == CCpp )
		shMap = mCCppMap;
	else
	if ( currentHighlighting == Html )
		shMap = mHtmlMap;

	ListViewWidgetsItem *item;
	QString data, defineSH, defaultSH;

	for ( uint i=0; i<shMap.count(); ++i ) {
		if ( shMap[i].section(';', 0,0).isEmpty() )
			continue;
		item = new ListViewWidgetsItem( mListViewWidgets, TextLabel, CheckBox, CheckBox, ColorFrame, CheckBox );
		item->setTextValue( ContextCOL, shMap[i].section(';', 0,0) );
		defaultSH = shMap[i].section( ';', 1,1 );
		defineSH  = shMap[i].section( ';', 2,2 );

		if ( mInsertDefault )
			data = defaultSH;
		else
			data = (defineSH.isEmpty()) ? defaultSH : defineSH;

		if ( data.section( ',', 1,1 ).lower() == "bold" )
			item->setBoolValue( BoldCOL, TRUE );
		if ( data.section( ',', 2,2 ).lower() == "italic" )
			item->setBoolValue( ItalicCOL, TRUE );

		data = data.section( ',', 0,0 );
		if ( data.find("(default)") == 0 ) {
			item->setBoolValue( UseDefaultCOL, TRUE );
			data.remove( 0, 9 ); // remove string '(default)'
		}

		item->setColorValue( ColorCOL, color(data) );
		item->setTextAttributs( ContextCOL, item->colorValue(ColorCOL), item->boolValue(BoldCOL), item->boolValue(ItalicCOL) );
	}

	mInsertDefault = FALSE;
}


void SyntaxHighlightingPage::slotSetDefaultHighlighting()
{
	mInsertDefault = TRUE;
	slotSelectHighlighting( mHighlightingComboBox->currentItem() );

	// --- update main 'shMap'
	QStringList shMap;
	int currentHighlighting = mHighlightingComboBox->currentItem();

	if ( currentHighlighting == CCpp )
		shMap = mCCppMap;
	else
	if ( currentHighlighting == Html )
		shMap = mHtmlMap;

	QString data;
	for ( uint i=0; i<shMap.count(); i++ ) {
		data = shMap[i];
		if ( ! data.section( ';', 2,2 ).isEmpty() ) // if defineSH exists then
			shMap[i] = data.left( data.findRev(';')+1 ); // remove it
	}
	// --- update main '*Map'
	if ( currentHighlighting == CCpp )
		mCCppMap = shMap;
	else
	if ( currentHighlighting == Html )
		mHtmlMap = shMap;
}

void SyntaxHighlightingPage::slotButtonClicked( int column )
{
	QString data;
	QStringList shMap;
	int currentHighlighting = mHighlightingComboBox->currentItem();
	ListViewWidgetsItem *item = (ListViewWidgetsItem *)mListViewWidgets->currentItem();

	if ( currentHighlighting == CCpp )
		shMap = mCCppMap;
	else
	if ( currentHighlighting == Html )
		shMap = mHtmlMap;

	// --- search id of clicked item
	uint id;
	bool found = FALSE;
	for (id=0; id<shMap.count(); id++ )
		if ( shMap[id].section( ';', 0,0 ) == item->text(ContextCOL) ) {
			found = TRUE;
			break;
		}
	if ( found == FALSE )
		return;

	// --- set default value
	if ( column == UseDefaultCOL ) {
		if ( item->boolValue(UseDefaultCOL) ) {
			data = shMap[id].section(';', 1,1);

			if ( data.section( ',', 1,1 ).lower() == "bold" )
				item->setBoolValue( BoldCOL, TRUE );
			if ( data.section( ',', 2,2 ).lower() == "italic" )
				item->setBoolValue( ItalicCOL, TRUE );

			data = data.section( ',', 0,0 );
			if ( data.find("(default)") == 0 ) {
				item->setBoolValue( UseDefaultCOL, TRUE );
				data.remove( 0, 9 ); // remove string '(default)'
			}
			item->setColorValue( ColorCOL, color(data) );
			data = shMap[id].left( shMap[id].findRev( ';' )+1 ); // init by default value
		}
	}
	else {
		item->setBoolValue( UseDefaultCOL, FALSE );
		int defineSHid = shMap[id].findRev( ';' )+1;
		data = shMap[id].left( defineSHid );
		data.remove( defineSHid, shMap[id].length()-defineSHid );
		data += colorToString( item->colorValue(ColorCOL) )+",";
		if ( item->boolValue(BoldCOL) )
			data += "Bold";
		data += ",";
		if ( item->boolValue(ItalicCOL) )
			data += "Italic";
	}
	// --- update main '*Map'
	if ( currentHighlighting == CCpp )
		mCCppMap[id] = data;
	else
	if ( currentHighlighting == Html )
		mHtmlMap[id] = data;

	// the *Map formats:
	//   ContextName;(default)color,bold,itali;
	//   ContextName;(default)color,bold,itali;color,bold,italic

	item->setTextAttributs( ContextCOL, item->colorValue(ColorCOL), item->boolValue(BoldCOL), item->boolValue(ItalicCOL) );
	mInsertDefault = FALSE;
}

void SyntaxHighlightingPage::slotColorChanged( int column )
{
	slotButtonClicked( column );
}

void SyntaxHighlightingPage::shAttribut( int id, SHattribut & shAttiribut )
{
	if ( id > 10 )
		return;

	QStringList shMap;
	int currentHighlighting = mHighlightingComboBox->currentItem();

	if ( currentHighlighting == CCpp )
		shMap = mCCppMap;
	else
	if ( currentHighlighting == Html )
		shMap = mHtmlMap;

	QString sh = shMap[id];
	QString data;
	if ( sh.at(sh.length()-1) == ';' ) // is default only
		data = sh.section(';', 1,1);
	else { // defined
		data = sh;
		data = data.remove(0, data.findRev( ';' )+1 ); // remove default
	}

	shAttiribut.bold   = (data.section( ',', 1,1 ).lower() == "bold");
	shAttiribut.italic = (data.section( ',', 2,2 ).lower() == "italic");

	data = data.section( ',', 0,0 );
	if ( data.find("(default)") == 0 )
		data.remove( 0, 9 ); // remove string '(default)'

	shAttiribut.color = color( data );
}

bool SyntaxHighlightingPage::highlightingChanged()
{
	QListViewItemIterator it( mListViewWidgets );
	while ( it.current() )
		if ( ((ListViewWidgetsItem *)(*it))->boolValue(UseDefaultCOL) )
			return TRUE;

	return FALSE;
}

int SyntaxHighlightingPage::currentHighlighting()
{
	return mHighlightingComboBox->currentItem();
}
