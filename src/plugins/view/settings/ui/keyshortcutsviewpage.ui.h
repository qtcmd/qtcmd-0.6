/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include "messagebox.h"
#include "functions_col.h" // for color() (convert hex string to QColor)
#include "listviewwidgets.h"

#include <qheader.h>
#include <qsettings.h>

void KeyShortcutsViewPage::init()
{
	mKeyShortcuts = NULL;

	QSettings settings;

	QString scheme = settings.readEntry( "/qtcmd/FilesPanel/ColorsScheme", "DefaultColors" );
	QString colorStr = settings.readEntry( "/qtcmd/"+scheme+"/SecondBackgroundColor", "FFEBDC" );

	mListViewWidgets->setColumnAlignment( OwnCOL, Qt::AlignHCenter );
	mListViewWidgets->setColumnAlignment( DefaultCOL, Qt::AlignHCenter );
	mListViewWidgets->setColumnAlignment( NoneCOL, Qt::AlignHCenter );
	mListViewWidgets->setSecondColorOfBg( color(colorStr, "FFEBDC") );
	mListViewWidgets->setEditBoxGetKeyShortcutMode( TRUE );
	mListViewWidgets->setEnableTwoColorsOfBg( TRUE );
	mListViewWidgets->header()->resizeSection( 0, (width()-20)/2 );
	//mListViewWidgets->header()->resizeSection( 1, 140 );

	connect( mListViewWidgets, SIGNAL(buttonClicked(int)),
	 this, SLOT(slotButtonClicked(int)) );
	connect( mListViewWidgets, SIGNAL(editBoxTextApplied(int, const QString &, const QString &)),
	 this, SLOT(slotEditBoxTextApplied(int, const QString &, const QString &)) );
}


KeyShortcuts * KeyShortcutsViewPage::keyShortcuts()
{
	return mKeyShortcuts;
}


void KeyShortcutsViewPage::setKeyShortcuts( KeyShortcuts *ks )
{
	if ( ! ks )
		return;

	QString keyStr, defaultKeyStr;
	ListViewWidgetsItem *item;
	mKeyShortcuts = ks;

	for ( uint i=0; i<ks->count(); i++ ) {
		defaultKeyStr = mKeyShortcuts->keyDefaultStr(i);
		keyStr = (ks->keyStr(i) == "none" ) ? defaultKeyStr : ks->keyStr(i);
		item = new ListViewWidgetsItem( mListViewWidgets, TextLabel, EditBox, RadioButton, RadioButton, RadioButton, TextLabel );
		item->setExclusiveButtons( TRUE, RadioButton );
		item->setTextValue( ActionCOL, ks->desc(i) );

		if ( keyStr == "none" )
			item->setBoolValue( NoneCOL, TRUE );
		else {
			item->setBoolValue( OwnCOL, TRUE );
			item->setTextValue( ShortcutCOL, keyStr );
		}
		item->setTextValue( DefaultShortcutCOL, defaultKeyStr );
		if ( keyStr == defaultKeyStr )
			item->setBoolValue( DefaultCOL, TRUE );
	}
	if ( mListViewWidgets->childCount() )
		mListViewWidgets->setCurrentItem( mListViewWidgets->firstChild() );
}


void KeyShortcutsViewPage::slotButtonClicked( int column )
{
	ListViewWidgetsItem *item = (ListViewWidgetsItem *)mListViewWidgets->currentItem();
	if ( ! item )
		return;

	if ( column == OwnCOL )
		mListViewWidgets->showWidget( ShortcutCOL ); // show the ListViewEditBox obj.for current item
	else
	if ( column == NoneCOL )
		item->setText( ShortcutCOL, "" );
	else
	if ( column == DefaultCOL )
		item->setText( ShortcutCOL, item->text(DefaultShortcutCOL) );
}

void KeyShortcutsViewPage::slotEditBoxTextApplied( int column, const QString &, const QString & newShortcut )
{
	ListViewWidgetsItem *item = (ListViewWidgetsItem *)mListViewWidgets->currentItem();
	if ( ! item )
		return;

	// --- check a shortcut whether it's use yet
	int result = -1;
	QListViewItemIterator it( mListViewWidgets );
	while ( it.current() ) {
		if ( (*it)->text(column) == newShortcut && (*it) != item ) {
			result = MessageBox::yesNo( this,
			 tr("Key shortcut conflict")+" - QtCommander",
			 QString( tr("The '%1' key combination has already been allocated to the action") ).arg(newShortcut)
			 +" :\n\n\t'"+(*it)->text(ActionCOL)+"'\n\n"+tr("Do you want to reassign it from that action to the current one ?"),
			 MessageBox::No
			);
			break;
		}
		it++;
	}

	if ( result == MessageBox::Yes ) {
		// init current item
		item->setText( column, newShortcut );
		item->setBoolValue( ((newShortcut == item->text(DefaultShortcutCOL)) ? DefaultCOL : OwnCOL), TRUE );
		// reset founded shortcut
		item = (ListViewWidgetsItem *)(*it);
		item->setText( ShortcutCOL, "" );
		item->setBoolValue( NoneCOL, TRUE );

		result = MessageBox::yesNo( this,
		 tr("Set a new key shortcut")+" - QtCommander",
		 tr("The action '%1' has not been allocated to the key shortcut").arg(item->text(ActionCOL))
		 +".\n\n"+tr("Do you want to allocate it now ?"),
		 MessageBox::Yes
		);
		if ( result == MessageBox::Yes ) {
			item->setBoolValue( NoneCOL, TRUE );
			mListViewWidgets->showWidget( column, item ); // shows the ListViewEditBox obj.
			uint col = NoneCOL;
			if ( item->text(ShortcutCOL) == item->text(DefaultShortcutCOL) )
				col = DefaultCOL;
			else
			if ( ! item->text(ShortcutCOL).isEmpty() )
				col = OwnCOL;
			item->setBoolValue( col, TRUE );
		}
	}
	else
	if ( result != MessageBox::No && result != 0 ) { // a new shortcut no causing a conflict
		item->setText( column, newShortcut );
		item->setBoolValue( ((newShortcut == item->text(DefaultShortcutCOL)) ? DefaultCOL : OwnCOL), TRUE );
	}
	// set a new key shortcut for the KeyShortcuts obj.
	mKeyShortcuts->setKey( item->text(ActionCOL), QKeySequence(item->text(ShortcutCOL)) );
}


void KeyShortcutsViewPage::slotSetDefaults()
{
	QListViewItemIterator it( mListViewWidgets );

	while ( it.current() ) {
		(*it)->setText( ShortcutCOL, (*it)->text(DefaultShortcutCOL) );
		((ListViewWidgetsItem *)(*it))->setBoolValue( DefaultCOL, TRUE );
		it++;
	}
}


void KeyShortcutsViewPage::slotEditCurrentShortcut()
{
	mListViewWidgets->showWidget( ShortcutCOL ); // show the ListViewEditBox obj.for current item
}


void KeyShortcutsViewPage::slotSave()
{
	QSettings settings;
	QString action, shortcut;
	QListViewItemIterator it( mListViewWidgets );

	while ( it.current() ) {
		action = (*it)->text(ActionCOL);
		shortcut = (*it)->text(ShortcutCOL);

		if ( shortcut.isEmpty() )
			shortcut = "none";

		it++;
		settings.writeEntry( "/qtcmd/FileViewShortcuts/"+action, shortcut );
	}
}
