/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qimage.h>
#include <qsettings.h>

#include "../../../../../icons/settings.xpm"
#include "../../../../../icons/view_icons.h"
#include "../../../../../icons/textview_icons.h"
#include "../../../../../icons/mainview_icons.h"
#include "../../../../../icons/toolbarsett_icons.h"

void ToolBarViewPage::init()
{
	mInputActionsListBox->insertItem( QPixmap((const char**)filenew), tr("Open in new window"), 0 );
	mInputActionsListBox->insertItem( QPixmap((const char**)fileopen), tr("Open"), 1 );
	mInputActionsListBox->insertItem( QPixmap((const char**)filesave), tr("Save"), 2 );
	mInputActionsListBox->insertItem( QPixmap((const char**)print), tr("Print"), 3 );
	mInputActionsListBox->insertItem( QPixmap((const char**)filereload), tr("Reload"), 4 );
	mInputActionsListBox->insertItem( QPixmap((const char**)undo), tr("Undo"), 5 );
	mInputActionsListBox->insertItem( QPixmap((const char**)redo), tr("Redo"), 6 );
	mInputActionsListBox->insertItem( QPixmap((const char**)cut), tr("Cut"), 7 );
	mInputActionsListBox->insertItem( QPixmap((const char**)copy), tr("Copy"), 8 );
	mInputActionsListBox->insertItem( QPixmap((const char**)paste), tr("Paste"), 9 );
	mInputActionsListBox->insertItem( QPixmap((const char**)findfirst), tr("Find"), 10 );
	mInputActionsListBox->insertItem( QPixmap((const char**)findnext), tr("Find next"), 11 );
	mInputActionsListBox->insertItem( QPixmap((const char**)replace), tr("Replace"), 12 );
	mInputActionsListBox->insertItem( QPixmap((const char**)gotoline), tr("Go to line"), 13 );

	QPixmap settingsScaled;
	QImage img((const char**)settings); // need to rescaled, becouse a pixmap is to big
	settingsScaled.convertFromImage(img.smoothScale( 20, 20 ),  Qt::AutoColor | Qt::DiffuseDither);

	mInputActionsListBox->insertItem( QPixmap((const char**)viewmode), tr("Change kind of view to")+"...", 14 );
	mInputActionsListBox->insertItem( QPixmap((const char**)zoomin), tr("Zoom in"), 15 );
	mInputActionsListBox->insertItem( QPixmap((const char**)zoomout), tr("Zoom out"), 16 );
	mInputActionsListBox->insertItem( settingsScaled, tr("Configure view"), 17 );
	mInputActionsListBox->insertItem( QPixmap((const char**)sidelist), tr("Show/Hide side list"), 18 );

	mDeletePushBtn->setAccel( Key_Delete );
	mUpToolBtn->setIconSet( QPixmap((const char**)up) );
	mDownToolBtn->setIconSet( QPixmap((const char**)down) );
	mInsertToolBtn->setIconSet( QPixmap((const char**)insert) );

// 	QSettings settings;
	// --- reads bars icons list from config file

	// insert bars name
	QStringList barsList;
	barsList.append( tr("global icons") );
	barsList.append( tr("text view") );
	barsList.append( tr("image view") );
	barsList.append( tr("sound view") );
	barsList.append( tr("video view") );
	mBarsComboBox->insertStringList( barsList );

	slotChangeCurrentBar( 0 );
}

void ToolBarViewPage::slotInsert()
{
	if ( ! mInputActionsListBox->hasFocus() )
		return;

	int id = mInputActionsListBox->currentItem();

	if ( mOutputActionsListBox->findItem(mInputActionsListBox->text(id)) == NULL ) {
		mOutputActionsListBox->insertItem( *mInputActionsListBox->pixmap(id), mInputActionsListBox->text(id) );
		updateControlsButtons();
	}
}

void ToolBarViewPage::slotUp()
{
	moveCurrentItem( UP );
}

void ToolBarViewPage::slotDown()
{
	moveCurrentItem( DOWN );
}

void ToolBarViewPage::slotDelete()
{
	if ( mOutputActionsListBox->count() == 0 || ! mOutputActionsListBox->hasFocus() )
		return;

	mOutputActionsListBox->takeItem( mOutputActionsListBox->selectedItem() );
	mOutputActionsListBox->setSelected( mOutputActionsListBox->item(mOutputActionsListBox->currentItem()), TRUE );
	updateControlsButtons();
}

void ToolBarViewPage::slotChangeCurrentBar( int /*currentBarId*/ )
{
	//mOutputActionsListBox->clear();
	//inserts icons list about id == currentBarId (0 - global icons, 1 - TextView icons)
	updateControlsButtons();
}


void ToolBarViewPage::moveCurrentItem( Direct direct )
{
	if ( mOutputActionsListBox->count() < 2 || ! mOutputActionsListBox->hasFocus() )
		return;

	QListBoxItem *replacedItem;
	QListBoxItem *currItem = mOutputActionsListBox->selectedItem();
        if (currItem == NULL)
                return;

	int itHeight = mOutputActionsListBox->itemRect( currItem ).height();
	int currItemYpos = mOutputActionsListBox->itemRect( currItem ).y()+5;

	if ( direct == UP )
		replacedItem = mOutputActionsListBox->itemAt( QPoint(5,currItemYpos-itHeight) );
	else
	if ( direct == DOWN)
		replacedItem = mOutputActionsListBox->itemAt( QPoint(5,currItemYpos+itHeight) );

        if (replacedItem == NULL || replacedItem == currItem)
		return;

	int currentItemId = mOutputActionsListBox->index( currItem );
	int replacedItemId = mOutputActionsListBox->index( replacedItem );

	QPixmap currentItemPixmap = *currItem->pixmap();
	QString currentItemText   = currItem->text();

	mOutputActionsListBox->changeItem( *replacedItem->pixmap(), replacedItem->text(), currentItemId );
	mOutputActionsListBox->changeItem( currentItemPixmap , currentItemText, replacedItemId );
}

void ToolBarViewPage::updateControlsButtons()
{
	mUpToolBtn->setEnabled( (mOutputActionsListBox->count() > 1) );
	mDownToolBtn->setEnabled( (mOutputActionsListBox->count() > 1) );
	mDeletePushBtn->setEnabled( (mOutputActionsListBox->count() > 0) );
}
