/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qsettings.h>
#include <qfiledialog.h>

#include "locationchooser.h"


void OtherTextViewPage::init()
{
	QSettings settings;

	int tabWidth = settings.readNumEntry( "/qtcmd/TextFileView/TabWidth", 2 );
	int useExtEditor = settings.readNumEntry( "/qtcmd/TextFileView/UseExternalEditor", -1 );
	bool useExternalEditor = (useExtEditor < 0) ? FALSE : (bool)useExtEditor;
	mExternalEditorChkBox->setChecked( useExternalEditor );
	QString externalEditorPath = settings.readEntry( "/qtcmd/TextFileView/ExternalEditor" );

	mTabSizeSpinBox->setValue( tabWidth );
	mLocationChooser->setText( externalEditorPath );
	mLocationChooser->setEnabled( mExternalEditorChkBox->isChecked() );
}


uint OtherTextViewPage::tabWidth()
{
	return mTabSizeSpinBox->value();
}

QString OtherTextViewPage::externalEditorPath()
{
	return (mExternalEditorChkBox->isChecked()) ? mLocationChooser->text() : QString("");
}
