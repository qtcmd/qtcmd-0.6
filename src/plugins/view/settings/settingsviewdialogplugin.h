/***************************************************************************
                          settingsviewdialogplugin.h  -  description
                             -------------------
    begin                : wed nov 5 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _SETTINGSVIEWDIALOGPLUGIN_H_
#define _SETTINGSVIEWDIALOGPLUGIN_H_

#include <qwidgetplugin.h>

class SettingsViewDialogPlugin : public QWidgetPlugin
{
public:
	SettingsViewDialogPlugin() {}

	// below is needed to macro 'Q_EXPORT_PLUGIN'
	QWidget * create( const QString & , QWidget * , const char * ) { return 0; }
	bool isContainer( const QString & ) const { return FALSE; }
	QStringList keys() const { return QString::null; }

};

#endif
