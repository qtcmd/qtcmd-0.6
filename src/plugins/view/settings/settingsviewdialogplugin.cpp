/***************************************************************************
                          settingsviewdialogplugin.cpp  -  description
                             -------------------
    begin                : wed nov 5 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "settingsviewdialogplugin.h"
#include "ui/settingsviewdialog.h"

// export symbol for resolve it by 'QLibrary::resolve( const char * )'
extern "C" {
	QWidget * create_widget( QWidget * parent=0, const char * name=0 )
	{
		return new SettingsViewDialog( parent, name, TRUE );
	}
}

Q_EXPORT_PLUGIN( SettingsViewDialogPlugin )
