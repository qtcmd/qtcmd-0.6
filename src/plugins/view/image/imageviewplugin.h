/***************************************************************************
                          imageviewplugin.h  -  description
                             -------------------
    begin                : tue nov 4 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _IMAGEVIEWPLUGIN_H_
#define _IMAGEVIEWPLUGIN_H_

#include "view.h"


class ImageViewPlugin : public QWidgetPlugin
{
public:
	ImageViewPlugin() {}

	// below is needed to macro 'Q_EXPORT_PLUGIN'
	QStringList keys() const { return QString::null; }
	bool isContainer( const QString & ) const { return FALSE; }
	View * create( const QString & , QWidget * , const char * ) { return 0; }

};

#endif
