/***************************************************************************
                          imageview.cpp  -  description
                             -------------------
    begin                : sun apr 27 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
 ***************************************************************************/

#include "messagebox.h"
#include "imageview.h"

#include <qimage.h>
#include <qapplication.h>

static int sPixmapWidth;
static QByteArray sMainBuffer;

ImageView::ImageView( QWidget *parent, const char *name )
	: View(parent,name), mPixmapLabel(0), mParent(parent)
{
	debug("ImageView::ImageView");

	mScrollView = new QScrollView( parent, "imageview", Qt::WStaticContents );
	mScrollView->showNormal();

	mIsModified = FALSE; // tymczasowe
	sMainBuffer.resize( 0 );
	sPixmapWidth = 0;

	mPixmapLabel = new QLabel( mScrollView );
	mPixmapLabel->resize( 0, 0 );
}

ImageView::~ImageView()
{
	delete mPixmapLabel;
	delete mScrollView;
}

void ImageView::updateContents( UpdateMode updateMode, ModeOfView, const QString &, QByteArray & binBuffer )
{
	if ( updateMode == View::NONE ) {
		sMainBuffer.resize( 0 ); // reset for new image
		return;
	}
	mScrollView->removeChild( mPixmapLabel );
	mPixmap.resize( 0,0 );

	bool ok = FALSE;
	int pixmapWidth = 0, pixmapHeight = 0;

	if ( updateMode == ALL ) {
		ok = mPixmap.loadFromData( binBuffer );
		pixmapWidth = mPixmap.width(), pixmapHeight = mPixmap.height();
	}
	else
	if ( updateMode == APPEND ) { // (very slow)
		uint oldSize  = sMainBuffer.size();
		uint newBytes = binBuffer.size()-1; // becouse buffer is increased (need for txt files)
		sMainBuffer.resize( oldSize+newBytes );
		for ( uint i=0; i<newBytes; i++ )
			sMainBuffer[oldSize+i] = binBuffer[i];
		ok = mPixmap.loadFromData( sMainBuffer );
		pixmapWidth = mPixmap.width(), pixmapHeight = mPixmap.height();
	}

	if ( ! ok ) {
		emit signalUpdateStatus( tr("Cannot load a picture !") );
		return;
	}

	if ( mPixmapLabel->width() < pixmapWidth )
		mPixmapLabel->resize( pixmapWidth, pixmapHeight );

	mPixmapLabel->setPixmap( mPixmap );

	if ( mPixmapLabel ) {
		mScrollView->resizeContents( pixmapWidth, pixmapHeight );
		mScrollView->addChild( mPixmapLabel );
		if ( ! mPixmapLabel->isVisible() )
			mPixmapLabel->show();
	}

	updateStatus();
}

bool ImageView::saveToFile( const QString & fileName )
{
	if ( mPixmap.save(fileName, QPixmap::imageFormat(fileName)) )
		return TRUE;

	MessageBox::critical( this,
	 "\""+fileName+"\""+"\n\n"+ tr("Cannot write to this file")
	);
	return FALSE;
}

/*
	void getData( QByteArray & binBuffer );
void ImageView::getData( QByteArray & binBuffer )
{
	//binBuffer = mPixmap.data();
}
*/

void ImageView::updateStatus()
{
	QString message, moremsg;
	// mPixmap.depth() -> pokazuje glebie po skonwertowaniu i zawsze == 24
	// obrazek trzeba ladowac do obiektu QImage i z niega pobierac glebie
	message.sprintf("%dx%d, [%dx%d]",/*,  "+tr("bits")+": %d", */
		mPixmap.width(), mPixmap.height(), mParent->width(), mParent->height()/*, mPixmap.depth()*/ );

	emit signalUpdateStatus( message );
}

void ImageView::zoom( bool in, int range )
{
/*
	if ( mView->currentSize().height() > mView->minimumHeight() )
		mZoomInBtn->setEnabled( TRUE );
	else
		mZoomOutBtn->setEnabled( FALSE );

	if ( mView->currentSize().height() < mView->maximumHeight() )
		mZoomOutBtn->setEnabled( TRUE );
	else
		mZoomInBtn->setEnabled( FALSE );
*/
	QString message;
	message.sprintf( tr("Current picture size")+" : %dx%d", currentSize().width(), currentSize().height() );

	emit signalUpdateStatus( message, 3000, FALSE );
}

void ImageView::slotPrint()
{
	debug("ImageView::slotPrint() - is not yet implemented !");
}

//	void setSmoothInfo( bool );
//	void slotFitToTheWindow();
//	void slotSetOryginalSize();
//	void slotSmoothScaling();
//	void slotShowInFullScreen();
