# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/view/image
# Cel to biblioteka imageview

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
INCLUDEPATH += ../../../../src \
               ../../../libs/qtcmduiext/ 
MOC_DIR = ../../../../.moc 
OBJECTS_DIR = ../../../../.obj 
TARGET = imageview 
DESTDIR = ../../../../.libs/plugins 
CONFIG += warn_on \
          qt \
          thread \
          plugin 
TEMPLATE = lib 
HEADERS += imageview.h \
           imageviewplugin.h \
           view.h 
SOURCES += imageview.cpp \
           imageviewplugin.cpp 
PRECOMPILED_HEADER = ../../../pch.h
