/***************************************************************************
                          imageview.h  -  description
                             -------------------
    begin                : sun apr 27 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _IMAGEVIEW_H_
#define _IMAGEVIEW_H_

#include "view.h"

#include <qlabel.h>
#include <qpixmap.h>
#include <qpainter.h>
#include <qscrollview.h>


class ImageView : public View  {
	Q_OBJECT
public:
	ImageView( QWidget *parent, const char *name=0 );
	~ImageView();

	void updateContents( UpdateMode updateMode, ModeOfView, const QString &, QByteArray & );
	void updateStatus();

	bool loadFile( const QString & );
	bool saveToFile( const QString & );
	bool isModified() const { return mIsModified; }

//	void setSmoothInfo( bool );
//	void setUseColorContextInfo( bool );

	ModeOfView modeOfView() { return RENDERmode; }

	QSize currentSize() const { return QSize(mPixmap.width(), mPixmap.height()); }
	int minimumHeight() const { return 10; }
	int maximumHeight() const { return 10000; }
	void zoom( bool in, int range=1 );

private:
	QScrollView *mScrollView;
	QString mFileName;

	QPixmap mPixmap;
	QLabel *mPixmapLabel;

	QWidget *mParent;
	bool mIsModified;

protected:
	void setGeometry( int x, int y, int w, int h )  { mScrollView->setGeometry( x,y, w,h ); }

private slots:
	void slotPrint();
	void slotZoomIn()  { zoom( TRUE );  }
	void slotZoomOut() { zoom( FALSE ); }

};

#endif
