/***************************************************************************
                          binaryview  -  description
                             -------------------
    begin                : mon sep 1 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "binaryview.h"

#include <qpainter.h> // for slotPrint()
#include <qpaintdevicemetrics.h> // for slotPrint()


BinaryView::BinaryView( QWidget * parent, const char *name )
	: View(parent,name),
	  mModeOfView(HEXmode)
{
	//debug("BinaryView::BinaryView");
	mAppendOffset = 0;
	mIsModified = FALSE;
	mFontFamily = "Fixed"; // Courier New, Courier [Adobe]
	mFontSize = 12;
	mFileSize = 0;

	mPrinter = new QPrinter;

	mTextEdit = new QTextEdit( parent );
//	mTextEdit->viewport()->installEventFilter( this ); // for grab some mouse events from the QTextEdit obj.
//	mTextEdit->installEventFilter( this ); // for grab some KeyEvent from the View obj.
	mTextEdit->setModified( FALSE );
	mTextEdit->setReadOnly( TRUE );
	mTextEdit->setFocus();
	mTextEdit->setFamily( mFontFamily ); // Courier [Adobe]
	mTextEdit->setPointSize( mFontSize );
	mTextEdit->setBold( FALSE );
	mTextEdit->setItalic( FALSE );
	mTextEdit->setFont( mTextEdit->currentFont() );
	mTextEdit->setPaletteBackgroundColor( Qt::white );
	mTextEdit->showNormal();

	mSyntaxHighlighter = new SyntaxHighlighterBin( mTextEdit );
}

BinaryView::~BinaryView()
{
	delete mSyntaxHighlighter;
	delete mTextEdit;
	delete mPrinter;
}

void BinaryView::updateContents( UpdateMode updateMode, ModeOfView modeOfView, const QString & , QByteArray & binBuffer )
{
	debug("BinaryView::updateContents(), modeOfView=%d, updateMode=%d", modeOfView, updateMode );
	mModeOfView = modeOfView;
	QString txtBuffer = "";
	if ( updateMode == NONE ) { // file has been loaded, need to move contents to top of view
		mTextEdit->setContentsPos( 0,0 );
		mAppendOffset = 0;
		return;
	}

	if ( updateMode == ALL )
		mSyntaxHighlighter->setHexHighlighting( (modeOfView != RAWmode) );

	if ( modeOfView == HEXmode ) {
		convertToHex( binBuffer, txtBuffer, updateMode );
		mAppendOffset = binBuffer.size();
	}
	else { // rewrite binBuffer to txtBuffer and replace not visible characters by '.'
		uint i = 0, b;
		uint bufSize = binBuffer.size()-1;
		while( i < bufSize ) {
			b = binBuffer[i];
			txtBuffer[i] = ((b < 32 || ( b < 161 && b > 126 )) ? '.' : (char)b );
			i++;
		}
	}
	bool viewIsEmpty = ! mTextEdit->length();
	mTextEdit->setUndoRedoEnabled( FALSE );
	if ( viewIsEmpty && updateMode == APPEND ) {
		mSyntaxHighlighter->setHexHighlighting( (modeOfView != RAWmode) );
		mFileSize = 0;
	}
	if ( viewIsEmpty ||  updateMode != APPEND ) {
		mTextEdit->clear();
		mTextEdit->setTextFormat( Qt::PlainText );
	}
	if ( updateMode == APPEND ) {
		mTextEdit->append( QString::fromLocal8Bit(txtBuffer) );
		mFileSize += binBuffer.size()-1;
	}
	else {
		mTextEdit->setText( QString::fromLocal8Bit(txtBuffer) );
		mFileSize = binBuffer.size()-1;
	}
	mTextEdit->setUndoRedoEnabled( TRUE );
	updateStatus();
}

void BinaryView::updateStatus()
{
//	emit signalUpdateStatus( QString::number(mFileSize)+" "+tr("bytes") );
	emit signalUpdateStatus( QString::number((mModeOfView == RAWmode) ? mTextEdit->length()-1 : mFileSize)+" "+tr("bytes") );
}

bool BinaryView::saveToFile( const QString & )
{
	return FALSE;
}

void BinaryView::convertToHex( const QByteArray & binBuffer, QString & txtBuffer, UpdateMode updateMode )
{
	debug("BinaryView::convertToHex, binBuffer.size()=%d", binBuffer.size() );

	txtBuffer = "";
	QChar chr;
	QString row, hexNumber;

	uint charWidth = QFontMetrics(QFont(mFontFamily, mFontSize)).width("a");
	uint maxChars = (((mTextEdit->width() / charWidth)-10)/4) - 1;
	uint bufSize = binBuffer.size(), i, max, offsetDiff;
	uint offset = 0;

	if ( maxChars == 0 )
		return;

	while( bufSize ) {
		max = (bufSize<maxChars) ? bufSize : maxChars;

		// ---- writes an offset
		hexNumber = QString::number( (updateMode == ALL) ? offset : mAppendOffset, 16 ).upper();
		offsetDiff = 8 - hexNumber.length();
		if ( hexNumber.length() < 8 )
			while(offsetDiff--)  hexNumber.insert(0, '0');
		row.append( hexNumber+" " );

		// ---- writes string of hex's numbers
		for( i=0; i<max; i++) {
			hexNumber = QString::number( (Q_UINT8)(binBuffer[offset+i]), 16).upper();
			if ( hexNumber.length() < 2 )  hexNumber.insert(0, '0');
			row.append( hexNumber+" " );
		}
		// fills empty space on the end of row by string of space's characters
		if ( bufSize < maxChars )
			for(uint a=0; a<(maxChars-max); a++)
				row.append("   ");
		// adds separator
		row.append("|");

		// ---- writes ascii string
		for( i=0; i<max; i++) {
			chr = binBuffer[offset+i];
			if ( (uint)chr < 32 || ((uint)chr < 161 && (uint)chr > 126) )  chr = '.';
			row.append( chr );
		}
		// for breaks line into textView
		row.append("\n");

		txtBuffer.append( row );
		bufSize-= i; offset+= i;
		if ( updateMode == APPEND )
			mAppendOffset+= i;
		row = "";
	}
}

void BinaryView::getData( QByteArray & binBuffer )
{
	if ( mModeOfView != HEXmode ) {
		binBuffer = mTextEdit->text().local8Bit();
		return;
	}
	// ponizsze jest b.wolne (juz dla 5kb znakow) !
	uint viewOffset = 8, bytesInLine, separatorPos = 0, bufferOffset = 0;
	QString hexLine, asciiLine;
	while ( viewOffset < mFileSize ) {
		separatorPos = mTextEdit->text().find( '|', viewOffset );
		hexLine = mTextEdit->text().mid( viewOffset, separatorPos-(viewOffset+1) );
		hexLine = hexLine.stripWhiteSpace();
		bytesInLine = hexLine.contains( ' ' )+1;
		asciiLine = mTextEdit->text().mid( separatorPos+1, bytesInLine );
		viewOffset = separatorPos+1+bytesInLine+1+9;

		asciiLine = asciiLine.local8Bit();
		binBuffer.resize( bufferOffset+bytesInLine );
		for ( uint i=0; i<bytesInLine; i++ )
			binBuffer[bufferOffset+i] = QChar(asciiLine[i]);
		bufferOffset += bytesInLine;
	}
}

void BinaryView::slotPrint()
{ // this code is come from qmakeapp.cpp (template of qt-application from KDevelop) whith small modification
	const int Margin = 10;
	int pageNo = 1;

	if ( mPrinter->setup(this) ) { // printer dialog
		emit signalUpdateStatus( tr("Printing...") );
		QPainter p;
		if( !p.begin( mPrinter ) ) // paint on printer
			return;

		p.setFont( QFont(mFontFamily, mFontSize) );
		int yPos	= 0; // y-position for each line
		QFontMetrics fm = p.fontMetrics();
		QPaintDeviceMetrics metrics( mPrinter ); // need width/height
		// of printer surface

		for( int i = 0 ; i < mTextEdit->lines() ; i++ ) {
			if ( Margin + yPos > metrics.height() - Margin ) {
				QString msg = "Printing (page ";
				msg += QString::number( ++pageNo ) + ")...";
				emit signalUpdateStatus( msg );
				mPrinter->newPage();  // no more room on this page
				yPos = 0;  // back to top of page
			}
			p.drawText( Margin, Margin + yPos,
			  metrics.width(), fm.lineSpacing(),
			  ExpandTabs | DontClip,
			  mTextEdit->text( i )
			);
			yPos = yPos + fm.lineSpacing();
		}
		p.end(); // send job to printer
		emit signalUpdateStatus( tr("Printing completed") ); //, 2000 );
	}
	else
		emit signalUpdateStatus( tr("Printing aborted") ); //, 2000 );
}

/*
void BinaryView::mouseMoveEvent( QMouseEvent *e )
{
	if ( mModeOfView == HEXmode ) {
		uint charWidth = QFontMetrics(mTextEdit->currentFont()).width("a");
		int row = e->pos().y() / (QFontMetrics(mTextEdit->currentFont()).height()+2);
		uint col = e->pos().x() / charWidth;
		uint maxChars = (((width() / charWidth)-10)/4) - 1;

		if ( row > lines() ) row = mTextEdit->lines();
		if ( row == 1 ) row=1;  if ( col == 0 ) col=1;
		if ( col > 8 && col < (maxChars*3)+7 ) {
			setSelection( row, col, row, col+2, 1 );
// 			debug("c_2=%d c_3=%d, c_3=%d", col%2,col%3,col%4 );
// 			setSelection( row, (col+(maxChars*3))+1, row, col+(maxChars*3)+1, 2 );
// 			setSelectionAttributes( 2, QColor(black), TRUE ); // dwa czarne zaznaczenia
		}
// 		if ( col > (maxChars*3)+9 ) { // znaki ascii
// 		}
	}
}

bool BinaryView::eventFilter( QObject *o, QEvent *e )
{
	if ( o == mTextEdit ) {
		if ( e->type() == QEvent::KeyPress )
			keyPressEvent( (QKeyEvent *)e );
	}
	else
	if ( o == mTextEdit->viewport() ) {
		if ( e->type() == QEvent::MouseMove )
			mouseMoveEvent( (QMouseEvent *)e );
	}

	return QWidget::eventFilter( o, e );    // standard event processing
}
*/
