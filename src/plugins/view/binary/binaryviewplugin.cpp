/***************************************************************************
                          binaryviewplugin.cpp  -  description
                             -------------------
    begin                : tue nov 4 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "binaryviewplugin.h"
#include "binaryview.h"

// export symbol 'create_view' for resolves it by 'QLibrary::resolve( const char * )'
extern "C" {
	QWidget * create_view( QWidget * parent, const char * name )
	{
		return new BinaryView( parent, name );
	}
}

Q_EXPORT_PLUGIN( BinaryViewPlugin )
