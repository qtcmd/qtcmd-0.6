/***************************************************************************
                          binaryview  -  description
                             -------------------
    begin                : mon sep 1 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _BINARYVIEW_H_
#define _BINARYVIEW_H_

#include "view.h"
#include "syntaxhighlighterbin.h"

//#include <qframe.h>
#include <qtextedit.h>
#include <qprinter.h>

class BinaryView : public View
{
//	Q_OBJECT
public:
	BinaryView( QWidget * parent, const char *name );
	~BinaryView();

	void updateContents( UpdateMode updateMode, ModeOfView, const QString &, QByteArray & );
	void updateStatus();
	void clear() { mTextEdit->clear(); }

	bool saveToFile( const QString & );
	bool isModified() const { return mIsModified; }

	ModeOfView modeOfView() { return mModeOfView; }

	void getData( QByteArray & binBuffer );

private:
	ModeOfView mModeOfView;

	QPrinter  *mPrinter;

	SyntaxHighlighterBin *mSyntaxHighlighter;
	QTextEdit *mTextEdit; // temporary solution (QFrame will be used)
	bool mIsModified;
	QString mFontFamily;
	uint mFontSize;
	uint mFileSize;
	uint mAppendOffset;

protected:
	void setGeometry( int x, int y, int w, int h )  { mTextEdit->setGeometry( x,y, w,h ); }
// 	bool eventFilter( QObject *, QEvent * );
// 	void keyPressEvent( QKeyEvent * );
// 	void mouseMoveEvent( QMouseEvent * );

	void convertToHex( const QByteArray & binBuffer, QString & txtBuffer, UpdateMode updateMode );

public slots:
	void slotPrint();

};

#endif
