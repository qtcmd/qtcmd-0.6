# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/view/binary
# Cel to biblioteka binaryview

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
MOC_DIR = ../../../../.moc 
OBJECTS_DIR = ../../../../.obj 
TARGET = binaryview 
DESTDIR = ../../../../.libs/plugins 
CONFIG += warn_on \
          qt \
          thread \
          plugin 
TEMPLATE = lib 
HEADERS += binaryview.h \
           binaryviewplugin.h \
           syntaxhighlighterbin.h \
           view.h 
SOURCES += binaryview.cpp \
           binaryviewplugin.cpp \
           syntaxhighlighterbin.cpp 
PRECOMPILED_HEADER = ../../../pch.h
