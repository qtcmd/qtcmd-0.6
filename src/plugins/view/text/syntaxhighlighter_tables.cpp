
#ifndef SHTABLES_H
#define SHTABLES_H

// -------------- C and CPP -------------

static const char *cpp_keywords[] = {
	"break", "case", "continue", "default", "do", "else", "enum", "extern",
	"for", "goto", "if", "return", "sizeof", "struct",
	"switch", "typedef", "union", "while",
	"asm", "catch", "class", "const_cast", "delete", "dynamic_cast",
	"explicit", "export", "false", "friend", "inline", "namespace", "new",
	"operator", "private", "protected", "public", "reinterpret_cast",
	"static_cast", "template", "this", "throw", "true", "try", "typeid",
	"typename", "using", "virtual",
	"and_eq", "and", "bitand", "bitor", "compl", "not_eq", "not", "or_eq", "or",
	"xor_eq", "xor",
	0
};

static const char *cpp_types[] = {
	"auto", "char", "uchar", "const", "double", "float", "int", "uint", "long", "register",
	"short", "signed", "static", "unsigned", "void", "volatile",
	"bool", "wchar_t", "mutable",
	0
};

static const char *qt_extension[] = { // and kde extension
	"slots", "signals", "connect", "disconnect", "tr", "trUtf8", "i18n",
	"Q_OBJECT", "K_DCOP", "Q_PROPERTY", "Q_ENUMS", "Q_EXPORT", "SLOT", "SIGNAL", "FALSE", "TRUE",
	0
};

// -------------- HTML -------------
/*
static const char *html_tags[] = {
	"a", "abbr", "acronym", "addres", "applet", "area"
	"b", "base", "basefont", "bdo", "big", "blockquote", "body", "br", "button",
	"center", "caption", "cite", "code", "col", "colgroup",
	"dd", "del", "dfn", "dir", "div", "dl", "dt",  "em",
	"fieldset", "font", "form", "frame", "frameset",
	"h1", "h2", "h3", "h4", "h5", "h6", "head", "hr", "html",
	"i",  "iframe", "img", "input", "ins", "isindex",
	"kbd", "label", "legend", "li", "link",
	"map", "menu", "meta",  "noframes", "noscript",
	"object", "ol", "optgroup", "option",  "p", "parm", "pre",
	"s", "samp", "script", "select", "small", "span", "strike", "strong", "style", "sub", "sup",
	"table", "tbody", "td", "textarea", "tfoot", "th", "thead", "title", "tr", "tt",
	"u", "ul",  "var",
	0
};

static const char *html_subTags[] = {
	"bbr","accept-charset","accept","accesskey","action","align","alink","alt","archive","axis","background",
	"bgcolor","border","cellpadding","cellspacing","char","charoff","charset","checked","cite","class","classid",
	"clear","code","codebase","codetype","color","cols","colspan","compact","content","coords","data","datetime",
	"declare","defer","dir","disabled","enctype","face","for","frame","frameborder","headers","height","href",
	"hreflang","hspace","http-equiv","id","ismap","label","lang","language","link","longdesc","marginheight",
	"marginwidth","maxlength","media","method","multiple","name","nohref","noresize","noshade","nowrap","object",
	"onblur","onchange","onclick","ondblclick","onfocus","onkeydown","onkeypress","onkeyup","onload","onmousedown",
	"onmousemove","onmouseout","onmouseover","onmouseup","onreset","onselect","onsubmit","onunload","profile",
	"prompt","readonly","rel","rev","rows","rowspan","rules","scheme","scope","scrolling","selected","shape","size",
	"span","src","standby","start","style","summary","tabindex","target","text","title","type","usemap","valign",
	"value","valuetype","vlink","vspace","width",
	0
};

// -------------- CSS -------------

static const char *css_tags[] = {
	"a:link", a:visited", "a:active", "a:hover",
	"background-attachment", "background-color", "background-image", "background-position", "background-repeat",
	"border", "border-style", "border-color",
	"border-top", "border-bottom", "border-left", "border-right",
	"border-top-width", "border-bottom-width", "border-left-width", "border-right-width",
	"border-top-color", "border-bottom-color", "border-left-color", "border-right-color",
	"border-top-style", "border-bottom-style", "border-left-style", "border-right-style",
	"bottom", "color", "cursor",
	"clear", "direction", "display", "clip", "caption-side", "float", "font",
	"font-family", "font-size", "font-size-adjust", "font-stretch", "font-style", "font-weight", "font-variant"
	"height", "horizontal-align", "left", "letter-spacing", "line-height",
	"list-style", "list-style-image", "list-style-type", "list-style-position",
	"margin", "margin-bottom", "margin-left", "margin-right", "margin-top",
	"min-width",  "min-height", "max-width", "max-height",
	"mso-fareast-font-family", "mso-ansi-language", "mso-fareast-language", "mso-bidi-language", "overflow",
	"padding", "padding-left", "padding-right", "padding-top", "padding-bottom", "position", "right",
	"scrollbar-face-color", "scrollbar-shadow-color", "scrollbar-darkshadow-color", "scrollbar-track-color",
	"scrollbar-highlight-color", "scrollbar-3dlight-color", "scrollbar-arrow-color", "scrollbar-base-color",
	"text-align", "text-decoration", "text-indent", "text-transform", "text-shadow", "top", "table-layout",
	"unicode-bidi", "visibility", "vertical-align", "width", "white-space", "word-spacing", "z-index",
	0
};

static const char *css_subTags[] = {
	0
}
*/

#endif
