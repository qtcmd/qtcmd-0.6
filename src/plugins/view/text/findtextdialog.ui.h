/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/


bool FindTextDialog::showDialog( QWidget * parent, const QStringList & initFindStrLst, QString & findedStr, const QStringList & initNewStrLst, QString & newStr, bool & cs, bool & who, bool & fromCursor, bool & findForward, bool & por, bool replace )
{
	FindTextDialog *dlg = new FindTextDialog( parent );
	CHECK_PTR( dlg );

	if ( replace ) {
		dlg->setCaption( tr("Replace text")+" - QtCommender" );
		dlg->mFRButton->setText( tr("&Replace") );
		dlg->mNewTextComboBox->insertStringList( initNewStrLst );
		dlg->mPromptChkBox->setChecked( por );
	}
	else {
		dlg->setCaption( tr("Find text")+" - QtCommender" );
		dlg->mNewTextLabel->hide();
		dlg->mNewTextComboBox->hide();
		dlg->mPromptChkBox->hide();
		dlg->setFixedHeight( dlg->height() - dlg->mNewTextComboBox->height() );
	}

	dlg->mFindTextComboBox->insertStringList( initFindStrLst );
	dlg->mFindTextComboBox->setCurrentText( findedStr );
	dlg->mCaseSensitiveChkBox->setChecked( cs );
	dlg->mWholeWordsOnlyChkBox->setChecked( who );
	dlg->mFromCursorChkBox->setChecked( fromCursor );
	dlg->mForwardRB->setChecked( findForward );
	dlg->mBackwardRB->setChecked( !findForward );


	bool find = FALSE;
	int result = dlg->exec();

	if ( result == QDialog::Accepted ) {
		find = TRUE;
		cs  = dlg->mCaseSensitiveChkBox->isChecked();
		who = dlg->mWholeWordsOnlyChkBox->isChecked();
		fromCursor  = dlg->mFromCursorChkBox->isChecked();
		findForward = dlg->mForwardRB->isChecked();
		findedStr = dlg->mFindTextComboBox->currentText();
		newStr = dlg->mNewTextComboBox->currentText();
		por = dlg->mPromptChkBox->isChecked();
	}

	delete dlg;
	dlg = 0;

	return find;
}
