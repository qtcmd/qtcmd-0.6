# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/view/text
# Cel to biblioteka textview

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
FORMS += findtextdialog.ui 
TRANSLATIONS += ../../../../translations/pl/qtcmd-view-text.ts 
HEADERS += syntaxhighlighter.h \
           textview.h \
           textviewplugin.h \
           view.h \
           findtextdialog.ui.h 
SOURCES += syntaxhighlighter.cpp \
           syntaxhighlighter_tables.cpp \
           textview.cpp \
           textviewplugin.cpp 
LIBS += -lqtcmduiext
INCLUDEPATH += ../../../../src \
../../../../src/libs/qtcmduiext \
../../../ui
MOC_DIR = ../../../../.moc
OBJECTS_DIR = ../../../../.obj
QMAKE_LIBDIR = ../../../../.libs
TARGET = textview
DESTDIR = ../../../../.libs/plugins
CONFIG += warn_on \
qt \
thread \
plugin
TEMPLATE = lib
PRECOMPILED_HEADER = ../../../pch.h
