/***************************************************************************
                          textview.h  -  description
                             -------------------
    begin                : tue apr 15 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
***************************************************************************/

#ifndef _TEXTVIEW_H_
#define _TEXTVIEW_H_

#include "view.h"
#include "syntaxhighlighter.h"

#include <qstringlist.h>
#include <qtextedit.h>
#include <qprinter.h>
#include <qlistbox.h>
#include <qaction.h>


/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa obs�uguje podgl�d tekstowy.
 Klasa ta ma za zadanie pe�n� obs�ug� widoku tekstowego w trzech dost�pnych
 trybach (RAWmode, RENDERmode, EDITmode). Dla ka�dego z nich udost�pnia
 niezb�dne narz�dzia. Na przyk�ad dla trybu edycji dost�pne s�: wyszukiwanie,
 podmiana oraz skok do linii; dla pozosta�ych tryb�w dost�pne jest tylko
 wyszukiwanie. Ponad to klasa wspiera kolorowanie sk�adni. Domy�lnie jest ono
 wykrywane na podstawie rozszerzenia pliku, mo�na je tak�e ustawi� bezpo�rednio
 z menu g��wnego. Kolorowanie sk�adni jest wykonywane w trybach (RAWmode i
 EDITmode). Tryb RENDERmode jest wykorzystywany do pokazywania plik�w z kodem
 HTML. Dost�pna jest tak�e mo�liwo�� zmiany rozmiaru fontu bezpo�rednio z menu
 g��wnego (wszystkie tryby). Mo�na r�wnie� wydrukowa� zawarto�� widoku (tryby
 RAWmode i EDITmode). Poza tym klasa umo�liwia tak�e: bezpo�redni� zmian�
 rozmiaru i rodziny bie��cej czcionki, zmian� znaku ko�ca linii oraz rozmiaru
 tabulatora.
 */
class TextView : public View
{
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param parent - wska�nika na rodzica,
	 @param name - nazwa dla obiektu.\n
	 Inicjowane s� tutaj sk�adowe klasy, tworzone obiekty drukowania,
	 podgl�du tekstowego i kolorowania sk�adni, inicjowane opcje dla menu i dla
	 paska narz�dziowego, a tak�e inicjowane kodowanie fontu.
	 */
	TextView( QWidget *parent, const char *name=0 );

	/** Destruktor klasy.\n
	 Powoduje usuni�cie obiekt�w wykorzystywanych przez bie��cy obiekt.
	 */
	~TextView();

	/** Metoda zwraca rodzaj bie��cego widoku.\n
	 @return rodzaj widoku.
	 */
	ModeOfView modeOfView() { return mModeOfView; }

	/** Resetowany jest tutaj widok oraz czyszczone bufory.\n
	 */
	void clear() {
		mTextEdit->clear();
		mTxtBuffer = ""; mMainBuffer = "";
	}

	/** Metoda przekazuje wska�nika na obiekt skr�t�w klawiszowych dla bie��cego
	 podgl�du oraz ustawia akcje dla menu i paska narz�dziowego.\n
	 @param keyShortcuts - wska�nik na obiekt skr�t�w klawiszowych.
	 */
	void setKeyShortcuts( KeyShortcuts * keyShortcuts );

	/** Powoduje uaktualnienie bie��cego widoku podanymi danymi.\n
	 @param updateMode - tryb uaktualnienia (dost�pne s�: ALL, APPEND, ON_DEMAND,
	 NONE),
	 @param modeOfView - tryb podgl�du (dost�pne s�: RAWmode, RENDERmode, HEXmode,
	 EDITmode, UNKNOWNmode),
	 @param fileName - nazwa podgl�danego pliku,
	 @param binBuffer - bufor z danymi, kt�rymi nale�y uaktualni� widok.\n
	 Nazwa pliku jest niezb�dna dla uzyskania rozszerzenia pliku, kt�re
	 z kolei konieczne jest dla metody ustawiaj�cej kolorowanie sk�adni i formatu
	 tekstu (dla podgl�du tekstowego).
	 */
	void updateContents( UpdateMode , ModeOfView, const QString &, QByteArray & binBuffer );

	/** Pobiera zawarto�� bie��cego widoku.\n
	 @param binBuffer - bufor w kt�rym nale�y umie�ci� dane.
	 */
	void getData( QByteArray & binBuffer );

	/** Uaktualnia informacj� na pasku statusu.
	 */
	void updateStatus();

	/** Funkcja powoduje zapisanie zawarto�ci okna podgl�du do pliku tekstowego.\n
	 @param fileName - nazwa pliku do zapisu,
	 @return TRUE poprawnie zapisano plik, FALSE - nie mo�na by�o zapisa� pliku.
	 */
	bool saveToFile( const QString & fileName );

	/** Zwraca informacje o statusie modyfikowania zawarto�ci widoku.\n
	 @return TRUE je�li zmodyfikowano widok
	*/
	bool isModified() const { return mTextEdit->isModified(); }

	/** Zwraca wska�nik do grupy opcji menu 'Edit' dla widoku.\n
	 @return wska�nik do grupy opcji menu.
	 */
	QActionGroup *editMenuBarActions() const { return mEditMenuActions; }

	/** Zwraca wska�nik do grupy opcji menu 'View' dla widoku.\n
	 @return wska�nik do grupy opcji menu.
	 */
	QActionGroup *viewMenuBarActions() const { return mViewMenuActions; }

	/** Zwraca wska�nik do grupy opcji paska narz�dziowego 'Edit' dla widoku.\n
	 @return wska�nik do grupy opcji paska narz�dziowego.
	 */
	QActionGroup *editToolBarActions() const { return mEditToolBarActions; }

	/** Zwraca wska�nik do grupy opcji paska narz�dziowego 'View' dla widoku.\n
	 @return wska�nik do grupy opcji paska narz�dziowego.
	 */
	QActionGroup *viewToolBarActions() const { return mViewToolBarActions; }

private:
	QWidget   *mParent;
	QPrinter  *mPrinter;
	QTextEdit *mTextEdit;

	QString mTxtBuffer;
	ModeOfView mModeOfView;

	QString mFontFamily;
	int mFontSize, mTabWidth;
	int mFontBold, mFontItalic;

	QString mInsStatStr, mCurrentLink;
	uint mRowNum, mColNum;
	bool mOvrMode;

	SyntaxHighlighter *mSyntaxHighlighter;
	SyntaxHighlighter::SyntaxHighlighting mForceKindOfSH;
	bool mAutoSH;

	enum KindOfEOL { UNKNOWNeol=0, UNIXeol, WIN_DOSeol };
	KindOfEOL mKindOfEOL;

	// find and replace dialog
	QString mFindedText, mReplacedText;
	bool mCaseSensitive, mWholeWordsOnly, mStartFindFromCursorPos, mFindForward;
	bool mPromptOnReplace;
	bool mFindOnce, mFromBegin, mBtnContinueHasPressed;
	QStringList mFindedTextList;
	QStringList mReplacedTextList;
	// 'Goto line' dialog
	QStringList mLineNumbersList;

	QString mPartTagBuf, mMainBuffer;
	Qt::TextFormat mTextFormat;

	QString mWordPrefix;
	QListBox *mCompletionListBox;

	QActionGroup *mEditActions;
	QActionGroup *mViewMenuActions, *mEditMenuActions;
	QActionGroup *mEditToolBarActions, *mViewToolBarActions;
	QActionGroup *mEolActions, *mCodingFontsActions, *mManuallyCodingActions;
	QActionGroup *mSelectHighlightingActions, *mSourceHighlightActions, *mMarkupHighlightActions;//, *mScriptsHighlightActions;
	QAction *mFindA, *mFindNextA, *mReplaceA, *mGotoLineA, *mUndoA, *mRedoA, *mCutA, *mCopyA, *mPasteA, *mPrintA;
	QAction *mZoomInA, *mZoomOutA;
	QAction *mNoneA, *mCCppA, *mHtmlA;
	QAction *mUnixEolA, *mWinDosEolA;
	QAction *mHiddenEncA, *mHiddenEncInParentA;
	QAction *mHiddenHighlSrcA, *mHiddenHighlMarA, *mHiddenHighlInParentA;
	KeyShortcuts *mKeyShortcuts;

	/** Identyfikatory akcji skr�t�w klawiszowych dla podgl�du tekstowego.
	*/
	enum ActionsOfTextView {
		Find_TEXTVIEW=1000,
		FindNext_TEXTVIEW,
		Replace_TEXTVIEW,
		GoToLine_TEXTVIEW,
		Undo_TEXTVIEW,
		Redo_TEXTVIEW,
		Cut_TEXTVIEW,
		Copy_TEXTVIEW,
		Paste_TEXTVIEW,
		SelectAll_TEXTVIEW,
		Print_TEXTVIEW,
		ZoomIn_TEXTVIEW,
		ZoomOut_TEXTVIEW,
		TextCompletion_TEXTVIEW
	};

	/** Funkcja inicjuj�ca bie��cy widok.\n
	 Wczytywane s� tutaj z pliku konfiguracyjnego i ustawiane na widoku
	 nast�puj�ce w�asno�ci: font, szeroko�� tabulatora.
	 */
	void initView();

	/** Inicjowane s� tutaj opcje przeznaczone dla menu g�ownego oraz paska
	 narz�dziowego okna g�ownego widoku.\n
	 Wska�niki to tych opcji (typu QActionGroup), s� zwracane przez nast�puj�ce
	 metody: @em editMenuBarActions() , @em viewMenuBarActions() ,
	 @em editToolBarActions() , @em viewToolBarActions() .
	 */
	void initMenuAndToolBarActions();

	/** Metoda powoduje zastosowanie ustawie� skr�t�w klawiszowych dla bie��cego
	 podgl�du.\n
	 @param keyShortcuts - wska�nik na obiekt skr�t�w klawiszowych.
	 */
	void applyKeyShortcuts( KeyShortcuts * keyShortcuts );

	/** Wykonuje ustawienie rozmiaru tabulatora.\n
	 @param amountOfChar - ilo�� spacji dla tabulatora
	 */
	void setTabWidth( uint amountOfChars );

	/** Powoduje ustawienie znaku ko�ca linii na podany w bie��cym dokumencie.\n
	 @param kindOfEOL - rodzaj znaku ko�ca linii (musi by� zgodny z KindOfEOL,
	 dost�pne s� : UNIXeol, WIN_DOSeol, UNKNOWNeol)
	 */
	void setKindOfEoL( KindOfEOL kindOfEOL );

	/** Metoda powoduje ustawienie podanego kodowania dla bie��cego fontu.\n
	 @param encoding - nazwa dla kodowania.
	 UWAGA: metoda nie jest jeszcze zaimplementowana.
	 */
	void setEncoding( const QString & encoding=QString::null );

	/** Wykonuje ustawienie fontu dla widoku tekstowego.\n
	 @param family - rodzina fontu
	 @param size - rozmiar fontu
	 @param bold - czy font ma by� pogrubiony
	 @param italic - czy font ma by� pochylony
	 */
	void setFont( const QString & family, uint size, bool bold, bool italic );

	/** Metoda powoduje uaktualnienie zaznaczenia w menu dla aktualnego
	 kolorowania.\n
	 Bie��ce kolorowania pobierane jest z obiektu klasy 'SyntaxHighlighter', kt�ry
	 jest sk�adow� tej klasy.
	 */
	void updateHighlightingAction();

	/** Zapisuje aktualnie zaznaczony (nie d�u�szy ni� 16 znak�w) ci�g do podanej
	 zmiennej.\n
	 @param selectionsStr - tu zapisywany jest aktualnie zaznaczony tekst.
	 */
	void getCurrentSelection( QString & selectionsStr );

	/** Metoda wykorzystywana do zwi�kszania lub zmniejszania fontu na widoku.\n
	 @param in - warto�� TRUE nakazuje zwi�kszenie, FALSE zmniejszenie fontu,
	 @param range - m�wi o ile razy zmieni� rozmiar fontu (domy�lnie 1).
	 */
	void zoom( bool in, int range=1 );

	/** Powoduje dodanie aktualnie znalezionego ci�gu, je�li ju� nie jest
	 zapisany, do sk�adowej klasy @em mFindedTextList typu QStringList.
	 */
	void updateFindedTextList();

	/** Funkcja g��wna wykonuj�ca kolejne wyszukanie.\n
	 Wywo�ywana jest przez slot @em slotFindNext().
	 */
	void findNext();

	/** Zwraca rozmiar dokumentu w bajtach z uwzgl�dnieniem znak�w ko�ca linii
	 dla system�w UNIX i DOS/Windows.
	 */
	uint weight();


	/** Tworzony jest tu obiekt listy kompletowania tekstu.\n
	 Poza tym nast�puje tutaj te� po��czenie niezb�dnych sygna��w i slot�w.
	 */
	void createListBox();

	/** Dopasowuje rozmiar listy kompletowania do ilo�ci element�w na niej.\n
	 @param maxHeight - maksymalna wysoko��,
	 @param maxWidth - maksymalna szeroko��.
	 */
	void adjustListBoxSize( int maxHeight=32767, int maxWidth=32767 );

	/** Pobierana jest tutaj pozycja kursora wzgl�dem widoku.\n
	 @return globalna pozycja kursora na widoku.
	 */
	QPoint textCursorPoint() const;

protected:
	/** Funkcja ustawia geometri� dla obiektu widoku tekstowego.\n
	 @param x - nowa pozycja X,
	 @param y - nowa pozycja Y,
	 @param w - nowa szeroko��,
	 @param h - nowa wysoko��.
	*/
	void setGeometry( int x, int y, int w, int h ) { mTextEdit->setGeometry( x,y, w,h); }

	/** Funkcja przechwytuj�ca wszelkie zdarzenia dotycz�ce bie��cego obiektu.\n
	 @param o - obiekt kt�rego dotyczy zdarzenie,
	 @param e - zdarzenie,
	 @return czy przekaza� sterowanie dalej (TRUE), czy tu zatrzyma�. \n
	 Jest ona pomocna przy przechwytywaniu zdarzenia wci�ni�cia klawisza,
	 klikni�cia oraz poruszania mysz� nad obiektem widoku tekstowego.
	 */
	bool eventFilter( QObject * o, QEvent * e );

	/** Obs�uga wci�niecia niekt�rych klawiszy dla bie��cego widoku.\n
	 @param e - zdarzenie wci�ni�cia klawisza,
	 @return czy przekaza� sterowanie dalej (TRUE), czy tu zatrzyma�.\n
	 Kombinacja 'Ctrl+Insert' - powoduje skopiowanie do schowka, natomiast
	 Ctrl+A - zaznaczea ca�y tekst.
	 */
	bool keyPressed( QKeyEvent * e );

	/** Funkcja obs�uguje poruszanie kursorem myszy nad widokiem tekstowym.\n
	 @param e - zdarzenia dotycz�ce myszy.\n
	 Wykorzystywana jest w trybie 'RENDERmode' do pokazywania adres�w docelowych
	 wskazywanych przez linki.
	*/
	void mouseMoveEvent( QMouseEvent * e );

	/** Funkcja obs�uguje klikni�cie jednym przycisk�w myszy.\n
	 @param e - zdarzenia dotycz�ce myszy.\n
	 Wykorzystywana jest do pokazywania menu podr�cznego po klikni�ciu prawym
	 klawiszem myszy.
	*/
	void mousePressEvent( QMouseEvent * e );

private slots:
	/** Slot wywo�ywany po zmianie pozycji kursora.\n
	 @param par - paragraf, w kt�rym znajduje si� kursor,
	 @param col - kolumna, w kt�rej znajduje si� kursor.
	 */
	void slotCursorPositionChanged( int, int );

	/** Kopiuje do schowka lokacje linka, nad kt�rym aktualnie stoi kursor.\n
	 Ca�y kod tej funkcji zostal wzi�ty ze �r�de� Qt-3.1.1 ( QLineEdit::copy() )
	 */
	void slotCopyLinkToTheClipboard();

	/** Pusty slot wymagany dla dzia�ania slotu @em slotCopyLinkToTheClipboard() .
	 */
	void clipboardChanged() {}

	/** Slot powoduje zapisanie do pliku konfiguracyjnego ustawie� fontu oraz
	 wielko�ci tabulatora (w spacjach).
	 */
	void slotSaveSettings();


	/** Slot pokazuje dialog "Szukania ci�gu tekstowego". \n
	 Po wci�nieciu OK wykonuje wyszukanie. Je�li ci�g zostanie znaleziony,
	 wtedy jest pod�wietlany.
	 */
	void slotFind();

	/** Slot podejmuje pr�b� kolejnego wyszukania, ostatnio szukanego ci�gu.\n
	 Je�li ci�g zostanie znaleziony, wtedy jest pod�wietlany.
	 */
	void slotFindNext();

	/** Pokazuje dialog s�u��cy do zamiany ci�g�w.\n
	 Po wci�ni�ciu OK, je�li podany ci�g zostanie znaleziony, wtedy jest
	 podmieniany na podany (w dialogu).
	 */
	void slotReplace();

	/** Slot powoduje pokazanie dialogu drukowania bie��cego dokumentu.\n
	 Po wci�nieciu OK podejmowana jest pr�ba wydrukowania tekstu.
	 */
	void slotPrint();

	/** Slot pokazuje dialog "Skoku do wybranego numeru linii".\n
	 Po wci�nieciu OK, wykonuje skok do podanej linii (dialog przechowuje r�wnie�
	 histori� numer�w linii).
	 */
	void slotGoToLine();


	/** Slot uruchamia funkcj� powi�kszania fontu.\n
	 */
	void slotZoomIn()  { zoom( TRUE );  }

	/** Slot uruchamia funkcj� zmniejszania fontu.\n
	 */
	void slotZoomOut() { zoom( FALSE ); }


	/** Slot powoduje uruchomienie funkcji zmieniaj�cej znak ko�ca linii.\n
	 @param action - wybrana w menu opcja.\n
	 Slot przeznaczony jest do wykorzystania przez 'connect' dla obiektu 'QAction'
	 */
	void slotChangeEOL( QAction * action );

	/** Slot powoduje uruchomienie funkcji zmieniaj�cej kodowanie bie��cego fontu.\n
	 @param action - wybrana w menu opcja.\n
	 Ponadto wy��czane jest tu zaznaczenie poprzednio aktywnej opcji w menu.
	 Slot przeznaczony jest do wykorzystania przez 'connect' dla obiektu 'QAction'.
	 */
	void slotChangeEncoding( QAction * action );

	/** Slot powoduje uruchomienie funkcji zmieniaj�cej pod�wietlanie sk��dni.\n
	 @param action - wybrana w menu opcja.\n
	 Ponadto wy��czane jest tu zaznaczenie poprzednio aktywnej opcji w menu.
	 Slot przeznaczony jest do wykorzystania przez 'connect' dla obiektu 'QAction'.
	*/
	void slotChangeHighlighting( QAction * action );


	/** Metoda powoduje pokazanie list� z pasuj�cymi ci�gami do wpisanego
	 fragmentu.\n
	 Zbierane s� tu na liste pasuj�ce ci�gi tekstowe z ca�ego tekstu, je�li
	 znaleziono pasuj�ce ci�gu wtedy wy�wietlana jest ich lista.
	 */
	void slotCompleteText();

	/** Slot wywo�ywany po wybraniu pasuj�cego ci�gu z listy kompletowania.\n
	 @param item - wska�nik na wybrany element listy.
	 */
	void slotItemChosen( QListBoxItem *item=0 );

public slots:
	/** Slot powoduje zastosownie ustawie� z przekazanych z dialogu ustawie�.\n
	 @param tvs - struktura opisuj�ca wszystkie w�a�ciwo�ci dla widoku tekstowego
	 na dialogu ustawie�.
	 */
	void slotApplyTextViewSettings( const TextViewSettings & tvs );

};

#endif
