/***************************************************************************
                          textview.cpp  -  description
                             -------------------
    begin                : tue apr 15 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
***************************************************************************/

#include "textview.h"
#include "messagebox.h"
#include "findtextdialog.h"
#include "../../../../icons/view_icons.h"
#include "../../../../icons/textview_icons.h"

#include <qfile.h>
#include <qaccel.h>
#include <qregexp.h>
#include <qcursor.h>
#include <qpainter.h>
#include <qsettings.h>
#include <qpopupmenu.h>
#include <qclipboard.h>
#include <qapplication.h>
#include <qinputdialog.h>
#include <qpaintdevicemetrics.h>
#include <qkeysequence.h>

TextView::TextView( QWidget *parent, const char *name )
	: View(parent,name), mParent(parent), mAutoSH(TRUE)
{
	mEditToolBarActions = NULL;
	mViewToolBarActions = NULL;
	mViewMenuActions    = NULL;
	mEditMenuActions    = NULL;
	mCompletionListBox  = NULL;
	mKeyShortcuts = NULL;

	mFindedText = "";
	mInsStatStr = "INS"; // default for the QTextEdit obj.
	mKindOfEOL = UNKNOWNeol;
	mStartFindFromCursorPos = TRUE;
	mPromptOnReplace = FALSE;
	mWholeWordsOnly = FALSE;
	mCaseSensitive = TRUE;
	mFindForward = TRUE;
	mFromBegin = TRUE;
	mFindOnce = FALSE;
	mOvrMode = FALSE;

	mPrinter = new QPrinter;

	mTextEdit = new QTextEdit( parent );
	mTextEdit->viewport()->installEventFilter( this ); // for grab some mouse events from the QTextEdit obj.
	mTextEdit->installEventFilter( this ); // for grab some KeyEvent from the View obj.
	mTextEdit->setReadOnly( TRUE );
	mTextEdit->setUndoDepth( 40 );
	mTextEdit->setFocus();
	mTextEdit->showNormal();

	connect( mTextEdit, SIGNAL(cursorPositionChanged( int, int )), this, SLOT(slotCursorPositionChanged( int, int )) );

	initView(); // read info from the config file
	mSyntaxHighlighter = new SyntaxHighlighter( mTextEdit );

	initMenuAndToolBarActions(); // read info (about toolBars) from the config file
	setEncoding(); // init encoding (default is auto)
}


TextView::~TextView()
{
	delete mSyntaxHighlighter;
	delete mCompletionListBox;
	delete mTextEdit;
	delete mPrinter;

	delete mViewMenuActions;  delete mEditMenuActions;
	delete mEditToolBarActions;  delete mViewToolBarActions;
}


void TextView::initView()
{
	QSettings *settings = new QSettings;

	QString font = settings->readEntry( "/qtcmd/TextFileView/Font" );
	if ( font.isEmpty() )
		font = "Helvetica,10"; // default
	// - parse 'font'
	mFontFamily = font.left( font.find(',') );
	QString fontSizeStr = font.section( ',', 1,1 ).stripWhiteSpace();
	QString boldStr     = font.section( ',', 2,2 ).stripWhiteSpace().lower();
	QString italicStr   = font.section( ',', 3,3 ).stripWhiteSpace().lower();

	bool ok;
	mFontSize = fontSizeStr.toInt( &ok );
	if ( ! ok )
		mFontSize = 10; // default size

	int tabWidth = settings->readNumEntry( "/qtcmd/TextFileView/TabWidth", -1 );
	if ( tabWidth <= 0 )
		tabWidth = 2; // default width
	if ( tabWidth > 8 )
		tabWidth = 8;
	mTabWidth = tabWidth;

	delete settings;

	setTabWidth( mTabWidth );
	mFontBold = (boldStr == "bold");
	mFontItalic = (italicStr == "italic");
	setFont( mFontFamily, mFontSize, mFontBold, mFontItalic );
}


void TextView::setKeyShortcuts( KeyShortcuts * keyShortcuts )
{
	if ( ! keyShortcuts )
		return;

	mKeyShortcuts = keyShortcuts;

	mKeyShortcuts->append( Find_TEXTVIEW, tr("Find"), Qt::CTRL+Qt::Key_F );
	mKeyShortcuts->append( FindNext_TEXTVIEW, tr("Find next"), Qt::Key_F3 );
	mKeyShortcuts->append( Replace_TEXTVIEW, tr("Replace"), Qt::CTRL+Qt::Key_R );
	mKeyShortcuts->append( GoToLine_TEXTVIEW, tr("Go to line"), Qt::CTRL+Qt::Key_G );
	mKeyShortcuts->append( Undo_TEXTVIEW, tr("Undo"), Qt::CTRL+Qt::Key_Z );
	mKeyShortcuts->append( Redo_TEXTVIEW, tr("Redo"), Qt::CTRL+Qt::Key_Y );
	mKeyShortcuts->append( Cut_TEXTVIEW, tr("Cut"), Qt::CTRL+Qt::Key_X );
	mKeyShortcuts->append( Copy_TEXTVIEW, tr("Copy"), Qt::CTRL+Qt::Key_C );
	mKeyShortcuts->append( Paste_TEXTVIEW, tr("Paste"), Qt::CTRL+Qt::Key_V );
	mKeyShortcuts->append( SelectAll_TEXTVIEW, tr("Select all"), Qt::CTRL+Qt::Key_A );
	mKeyShortcuts->append( Print_TEXTVIEW, tr("Print"), Qt::CTRL+Qt::Key_P );
	mKeyShortcuts->append( ZoomIn_TEXTVIEW,  tr("Zoom in"), Qt::CTRL+Qt::Key_Plus );
	mKeyShortcuts->append( ZoomOut_TEXTVIEW, tr("Zoom out"), Qt::CTRL+Qt::Key_Minus );
	mKeyShortcuts->append( TextCompletion_TEXTVIEW, tr("Text completion"), Qt::CTRL+Qt::Key_Space );

	mKeyShortcuts->updateEntryList( "/qtcmd/FileViewShortcuts/" ); // update from config file
	applyKeyShortcuts( mKeyShortcuts ); // for actions in current window
}


void TextView::initMenuAndToolBarActions()
{
	mViewMenuActions = new QActionGroup( this );
	mEditMenuActions = new QActionGroup( this );

	// --- actions for EDIT menu
	mUndoA     = new QAction( QPixmap((const char**)undo),  tr("&Undo"), CTRL+Key_Z, mTextEdit, "undo" );
	mRedoA     = new QAction( QPixmap((const char**)redo),  tr("&Redo"), CTRL+Key_Y, mTextEdit, "redo" );
	mCutA      = new QAction( QPixmap((const char**)cut),   tr("C&ut"),  CTRL+Key_X, mTextEdit, "cut" );
	mCopyA     = new QAction( QPixmap((const char**)copy),  tr("&Copy"), CTRL+Key_C, mTextEdit, "copy" );
	mPasteA    = new QAction( QPixmap((const char**)paste), tr("&Pase"), CTRL+Key_V, mTextEdit, "paste" );
	mFindA     = new QAction( QPixmap((const char**)findfirst), tr("&Find"), CTRL+Key_F, mTextEdit, "find" );
	mFindNextA = new QAction( QPixmap((const char**)findnext),  tr("Find &next"), Key_F3, mTextEdit, "find next" );
	mReplaceA  = new QAction( QPixmap((const char**)replace),   tr("&Replace"), CTRL+Key_R, mTextEdit, "replace" );
	mGotoLineA = new QAction( QPixmap((const char**)gotoline),  tr("&Go to line"), CTRL+Key_G, mTextEdit, "go to line" );


	connect( mUndoA,  SIGNAL(activated()),  mTextEdit, SLOT(undo()) );
	connect( mRedoA,  SIGNAL(activated()),  mTextEdit, SLOT(redo()) );
	connect( mCutA,   SIGNAL(activated()),  mTextEdit, SLOT(cut()) );
	connect( mCopyA,  SIGNAL(activated()),  mTextEdit, SLOT(copy()) );
	connect( mPasteA, SIGNAL(activated()),  mTextEdit, SLOT(paste()) );
	connect( mFindA,     SIGNAL(activated()),  this, SLOT(slotFind()) );
	connect( mFindNextA, SIGNAL(activated()),  this, SLOT(slotFindNext()) );
	connect( mReplaceA,  SIGNAL(activated()),  this, SLOT(slotReplace()) );
	connect( mGotoLineA, SIGNAL(activated()),  this, SLOT(slotGoToLine()) );

	mEditActions = new QActionGroup( this );
	mEditActions->add( mUndoA );
	mEditActions->add( mRedoA );
	mEditActions->add( mCutA );
	mEditActions->add( mCopyA );
	mEditActions->add( mPasteA );
	mEditActions->add( mReplaceA );
	mEditActions->add( mGotoLineA );

	mEditMenuActions->add( mFindA );
	mEditMenuActions->add( mFindNextA );
	mEditMenuActions->add( mEditActions );

	// --- actions for VIEW menu

	// - init helpful actions
	mHiddenEncA = new QAction( this, "hidden" );
	mHiddenEncA->setToggleAction( TRUE );
	mHiddenEncA->setVisible( FALSE );
	mHiddenEncInParentA = new QAction( this, "hidden" );
	mHiddenEncInParentA->setToggleAction( TRUE );
	mHiddenEncInParentA->setVisible( FALSE );
	mHiddenHighlSrcA = new QAction( this, "hidden" );
	mHiddenHighlSrcA->setToggleAction( TRUE );
	mHiddenHighlSrcA->setVisible( FALSE );
	mHiddenHighlMarA = new QAction( this, "hidden" );
	mHiddenHighlMarA->setToggleAction( TRUE );
	mHiddenHighlMarA->setVisible( FALSE );
	mHiddenHighlInParentA= new QAction( this, "hidden" );
	mHiddenHighlInParentA->setToggleAction( TRUE );
	mHiddenHighlInParentA->setVisible( FALSE );

	// - view actions
	mPrintA = new QAction( QPixmap((const char**)print),   tr("&Print"), CTRL+Key_P, mTextEdit );
	mZoomInA  = new QAction( QPixmap((const char**)zoomin),  tr("Zoom &in"), CTRL+Key_Plus, mTextEdit );
	mZoomOutA = new QAction( QPixmap((const char**)zoomout), tr("Zoom &out"), CTRL+Key_Minus, mTextEdit );
	connect( mPrintA,   SIGNAL(activated()),  this, SLOT(slotPrint()) );
	connect( mZoomInA,  SIGNAL(activated()),  this, SLOT(slotZoomIn()) );
	connect( mZoomOutA, SIGNAL(activated()),  this, SLOT(slotZoomOut()) );
	// - EOL actions
	mUnixEolA   = new QAction( tr("&Unix"), 0, this, "unixEol" );
	mWinDosEolA = new QAction( tr("&Windows and Dos"), 0, this, "winDosEol" );
	mWinDosEolA->setToggleAction( TRUE );
	mUnixEolA->setToggleAction( TRUE );
	// - highlighting actions
	mNoneA = new QAction( tr("None"), 0, this, "none" );
	mCCppA = new QAction( tr("C/C++"), 0, this, "cCpp" );
	mHtmlA = new QAction( tr("HTML/XML"), 0, this, "html" );
	mNoneA->setToggleAction( TRUE );
	mCCppA->setToggleAction( TRUE );
	mHtmlA->setToggleAction( TRUE );
	// - encoding actions  (encodings will be get from a list) TODO napisac klase 'Charsets' i oprzec ja na KCharsets
	QAction *autoEncA, *ceIso2EncA, *ceCpEncA;
	autoEncA   = new QAction( tr("Automatic"), 0, this, "autoEnc" );
	ceIso2EncA = new QAction( tr("Central European")+" (ISO-8859-2)", 0, this, "ceISO-2" );
	ceCpEncA   = new QAction( tr("Central European")+" (CP-1250)", 0, this, "ceCP-1250" );
	autoEncA->setToggleAction( TRUE );
	ceIso2EncA->setToggleAction( TRUE );
	ceCpEncA->setToggleAction( TRUE );
	autoEncA->setOn( TRUE ); // default encoding

	// - create the group of actions object
	mEolActions = new QActionGroup( this, "eol", TRUE );
	mEolActions->setMenuText( tr("End of line") );
	mEolActions->setUsesDropDown( TRUE );

	mSelectHighlightingActions = new QActionGroup( this, "highlighting", TRUE );
	mSelectHighlightingActions->setMenuText( tr("Highlighting &mode") );
	mSelectHighlightingActions->setUsesDropDown( TRUE );

	mSourceHighlightActions = new QActionGroup( this, "source", TRUE );
	mSourceHighlightActions->setMenuText( tr("Source") );
	mSourceHighlightActions->setUsesDropDown( TRUE );

	mMarkupHighlightActions = new QActionGroup( this, "markup", TRUE );
	mMarkupHighlightActions->setMenuText( tr("Markup") );
	mMarkupHighlightActions->setUsesDropDown( TRUE );

	mCodingFontsActions = new QActionGroup( this, "encoding", TRUE );
	mCodingFontsActions->setMenuText( tr("Set encoding") );
	mCodingFontsActions->setUsesDropDown( TRUE );
	mCodingFontsActions->setEnabled( FALSE ); // NOT IMPLEMENTED TODO napisac klase 'Encodings'

	mManuallyCodingActions = new QActionGroup( this, "manualyEncoding", TRUE );
	mManuallyCodingActions->setMenuText( tr("Manually") );
	mManuallyCodingActions->setUsesDropDown( TRUE );

	connect( mEolActions,  SIGNAL(selected(QAction *)),  this, SLOT(slotChangeEOL(QAction *)) );
	connect( mSelectHighlightingActions, SIGNAL(selected(QAction *)),  this, SLOT(slotChangeHighlighting(QAction *)) );
	connect( mSourceHighlightActions,    SIGNAL(selected(QAction *)),  this, SLOT(slotChangeHighlighting(QAction *)) );
	connect( mMarkupHighlightActions,    SIGNAL(selected(QAction *)),  this, SLOT(slotChangeHighlighting(QAction *)) );
	connect( mCodingFontsActions,    SIGNAL(selected(QAction *)),  this, SLOT(slotChangeEncoding(QAction *)) );
	connect( mManuallyCodingActions, SIGNAL(selected(QAction *)),  this, SLOT(slotChangeEncoding(QAction *)) );

	// - update actions
	mEolActions->add( mUnixEolA );
	mEolActions->add( mWinDosEolA );
	mSourceHighlightActions->add( mCCppA );
	mSourceHighlightActions->add( mHiddenHighlSrcA );
	mMarkupHighlightActions->add( mHtmlA );
	mMarkupHighlightActions->add( mHiddenHighlMarA );
	mSelectHighlightingActions->add( mNoneA );
	mSelectHighlightingActions->add( mSourceHighlightActions );
	mSelectHighlightingActions->add( mMarkupHighlightActions );
	mSelectHighlightingActions->add( mHiddenHighlInParentA );
	mManuallyCodingActions->add( ceCpEncA );
	mManuallyCodingActions->add( ceIso2EncA );
	mManuallyCodingActions->add( mHiddenEncA );
	mCodingFontsActions->add( autoEncA );
	mCodingFontsActions->add( mManuallyCodingActions );
	mCodingFontsActions->add( mHiddenEncInParentA );

	mViewMenuActions->addSeparator();
	mViewMenuActions->add( mEolActions );
	mViewMenuActions->add( mCodingFontsActions );
	mViewMenuActions->add( mSelectHighlightingActions );
	mViewMenuActions->addSeparator();
	mViewMenuActions->add( mPrintA );
	mViewMenuActions->add( mZoomInA );
	mViewMenuActions->add( mZoomOutA );

	// --- update toolBars
	// read info from config file
	mEditToolBarActions = new QActionGroup( this );
	mEditToolBarActions->addSeparator();
	mEditToolBarActions->add( mFindA );
	mEditToolBarActions->add( mFindNextA );
	mEditToolBarActions->add( mEditActions );

	mViewToolBarActions = new QActionGroup( this );
	mViewToolBarActions->addSeparator();
	mViewToolBarActions->add( mPrintA );
	mViewToolBarActions->add( mZoomInA );
	mViewToolBarActions->add( mZoomOutA );
}

/* --- z kate ---
  // encoding menu, start with auto checked !
  m_setEncoding = new KSelectAction(i18n("Set &Encoding"), 0, ac, "set_encoding");
  list = KGlobal::charsets()->descriptiveEncodingNames();
  list.prepend( i18n( "Auto" ) );
  m_setEncoding->setItems(list);
  m_setEncoding->setCurrentItem (0);
  connect(m_setEncoding, SIGNAL(activated(const QString&)), this, SLOT(slotSetEncoding(const QString&)));
*/

void TextView::setKindOfEoL( KindOfEOL kindOfEOL )
{
	if ( kindOfEOL == mKindOfEOL || kindOfEOL == UNKNOWNeol || kindOfEOL < UNKNOWNeol || kindOfEOL > WIN_DOSeol )
		return;

	bool unixEol = (kindOfEOL == UNIXeol);
	mWinDosEolA->setOn( ! unixEol );
	mUnixEolA->setOn( unixEol );
	mKindOfEOL = kindOfEOL;

	if ( mTextEdit->length() ) { // replace current EOL
		mMainBuffer = mTextEdit->text();

		if ( kindOfEOL == UNIXeol )
			mMainBuffer.replace( "\x0d\x0a", "\x0a" );
		else // WIN_DOSeol
			mMainBuffer.replace( "\x0a", "\x0d\x0a" );

		mTextEdit->setText( mMainBuffer );
		updateStatus();
	}
}


void TextView::setFont( const QString & family, uint size, bool bold, bool italic )
{
	mTextEdit->setFont( QFont(family, size, (bold ? QFont::Bold : QFont::Normal), italic) );
}


void TextView::setTabWidth( uint amountOfChars )
{
	mTextEdit->setTabStopWidth( QFontMetrics(mTextEdit->currentFont()).width("a")*amountOfChars );
}


void TextView::updateContents( UpdateMode updateMode, ModeOfView modeOfView, const QString & fileName, QByteArray & binBuffer )
{
	debug("TextView::updateContents(), modeOfView=%d", modeOfView );
	QString ext;
	bool textViewIsEmpty = ! mTextEdit->length();

	if ( updateMode == ALL || (updateMode == APPEND && textViewIsEmpty) && mMainBuffer.length() <= mTxtBuffer.length() ) {
		//setModeOfView( modeOfView ); // shows/hides actions
		mEditActions->setVisible( ((mModeOfView=modeOfView) == View::EDITmode) );
		ext = QFileInfo( fileName ).extension( FALSE ).lower();
		if ( modeOfView != RENDERmode ) {
			mSyntaxHighlighter->setSyntaxHighlighting( (mAutoSH) ? SyntaxHighlighter::AUTO : mForceKindOfSH, ext );
			updateHighlightingAction();
		}
		else // a mode 'RENDERmode' not need to the highlighting menu
			mSelectHighlightingActions->setVisible( FALSE );
		// --- set the EOL info
		KindOfEOL kindOfEOL = UNIXeol;
		int p = binBuffer.find( QChar(0x0D) );
		if ( p != -1 )
			if ( binBuffer.find(QChar( 0x0A ), p) != -1 )
				kindOfEOL = WIN_DOSeol;
		setKindOfEoL( kindOfEOL );
	}

	if ( ! textViewIsEmpty && updateMode != APPEND )
		if ( mMainBuffer.length() <= mTxtBuffer.length() )
			mTxtBuffer = mTextEdit->text();
	if ( mTxtBuffer.isEmpty() && updateMode == ALL ) // only for first call this function
		mTxtBuffer = QString::fromLocal8Bit( binBuffer.data() );
	else
	if ( updateMode == APPEND ) {
		mTxtBuffer = QString::fromLocal8Bit( binBuffer.data() );
		if ( ! mPartTagBuf.isEmpty() ) {
			mTxtBuffer.insert(0, mPartTagBuf); mPartTagBuf = ""; // szybciej by dzialalo dolaczanie, czyli ponizsze polenie wczesniej, a tu dolaczanie do mMainBuffer
		}
	}

	if ( updateMode == ALL || (updateMode == APPEND && textViewIsEmpty) ) {
		mTextFormat = Qt::PlainText;
		if ( modeOfView == RENDERmode ) {
			if ( ext == "htm" || ext == "html" || ext == "shtml" || ext == "php" || ext == "php3" )
				mTextFormat = Qt::RichText;
		}
		else
		if ( modeOfView == RAWmode || modeOfView == EDITmode ) {
			mTextEdit->setPaper( QBrush(Qt::white) ); // background always must have white color
			setTabWidth( mTabWidth );
			mRowNum = 0;  mColNum = 0;
		}
	}

	if ( updateMode == ALL || (updateMode == APPEND && textViewIsEmpty) ) {
		mTextEdit->setReadOnly( (modeOfView != EDITmode) );
		mTextEdit->setUndoRedoEnabled( FALSE );
		if ( mMainBuffer.length() <= mTxtBuffer.length() )
			mTextEdit->clear();
		if ( mTextFormat == Qt::RichText && updateMode == APPEND ) {
			mPartTagBuf = mTxtBuffer.right( mTxtBuffer.length()-mTxtBuffer.findRev('>')-1 );
			mTxtBuffer.remove( mTxtBuffer.length()-mPartTagBuf.length(), mPartTagBuf.length() );
		}
		if ( mTextEdit->length() == 0 )
			mMainBuffer = mTxtBuffer;
		if ( mTextFormat == Qt::RichText ) {
			mTextEdit->setTextFormat( mTextFormat );
			if ( mMainBuffer.length() <= mTxtBuffer.length() )
				mTextEdit->setText( mTxtBuffer );
			else
				mTextEdit->setText( mMainBuffer ); // need to update all html, code becouse QTextEdit fixed incomplete code made it goes bad when it is append new code
			//mTextEdit->setText( mTxtBuffer );
		}
		else {
			mTextEdit->setTextFormat( mTextFormat );
			if ( mMainBuffer.length() <= mTxtBuffer.length() )
				mTextEdit->setText( mTxtBuffer );
			else {
				mTextEdit->clear();
				mTextEdit->setText( mMainBuffer );
			}
		}
		mTextEdit->setUndoRedoEnabled( TRUE );
	}
	else
	if ( updateMode == APPEND ) {
		mTextEdit->setUndoRedoEnabled( FALSE );
		if ( mTextFormat == Qt::RichText ) {
			mPartTagBuf = mTxtBuffer.right( mTxtBuffer.length()-mTxtBuffer.findRev('>')-1 );
			mTxtBuffer.remove( mTxtBuffer.length()-mPartTagBuf.length(), mPartTagBuf.length() );
		}
		mMainBuffer += mTxtBuffer;
		mTextEdit->append( mTxtBuffer );
		mTextEdit->setModified( FALSE );
		mTextEdit->setUndoRedoEnabled( TRUE );
	}

	updateStatus();
}


void TextView::getData( QByteArray & binBuffer )
{
	binBuffer = mTextEdit->text().local8Bit();
}


void TextView::zoom( bool in, int range )
{
	uint newFontSize = mTextEdit->pointSize() + ((in) ? range : -range);
	mTextEdit->zoomTo( newFontSize );
	mTextEdit->setPointSize( newFontSize );
	setTabWidth( mTabWidth );

	uint currentSize = mTextEdit->pointSize();

	if ( in ) {
		if ( currentSize < 24 )
			mZoomOutA->setEnabled( TRUE );
		else
			mZoomInA->setEnabled( FALSE );
	}
	else {
		if ( currentSize > 8 )
			mZoomInA->setEnabled( TRUE );
		else
			mZoomOutA->setEnabled( FALSE );
	}

	QString message;
	message.sprintf( tr("Current font size")+" : %d", currentSize );
	emit signalUpdateStatus( message, 3000, FALSE ); // (dla RAW) niestety po tym nie jest zwracana poprzednia wart. (ta sprzed zmiany rozmiaru)
}

// --- FIXME uniezaleznic zapisywanie do pliku od systemu plikow
bool TextView::saveToFile( const QString & fileName )
{
	QFile file( fileName );
	if ( ! file.open(IO_WriteOnly | IO_Truncate) ) {
		MessageBox::critical( this,
		 "\""+fileName+"\""+"\n\n"+ tr("Cannot write to this file")
		);
		return FALSE;
	}
	mTextEdit->setModified( FALSE );

	QTextStream stream( &file );
	stream << mTextEdit->text();
	file.close();

	return TRUE;
}


uint TextView::weight()
{
	uint allEOLs = (mKindOfEOL == UNIXeol) ? 0 : mTextEdit->paragraphs()-1;
	return mTextEdit->length() + allEOLs;
}


void TextView::updateStatus()
{
	QString message = "", moremsg;

// 	if ( mModeOfView != HEXmode && mModeOfView != RENDERmode )
	if ( mModeOfView == RAWmode )
		message.sprintf( "%d "+tr("bytes"), weight() );
// 		message.sprintf( tr("row")+"=%d/%d   "+tr("column")+"=%d/%d",
// 			mRowNum, mTextEdit->paragraphs(), mColNum, mTextEdit->paragraphLength(mRowNum-1)+1 );

	if ( mModeOfView == EDITmode ) {
		message.sprintf( tr("row")+": %d/%d   "+tr("column")+": %d/%d",
			mRowNum, mTextEdit->paragraphs(), mColNum,mTextEdit->paragraphLength(mRowNum-1)+1 );

		uint byteNum = 0, row = mRowNum;
		while( row-- )
			byteNum += mTextEdit->paragraphLength(row-1);
		byteNum += mColNum+mRowNum-1;
		message += moremsg.sprintf("   "+tr("byte")+": %d/%d  %s", byteNum, weight(), mInsStatStr.latin1() );
	}

	emit signalUpdateStatus( message );
}


void TextView::updateFindedTextList()
{
	bool notFound = TRUE;
	for( uint i=0; i<mFindedTextList.count(); i++ )
		if ( mFindedTextList[i] == mFindedText ) {
			notFound = FALSE;
			break;
		}
	if ( notFound )
		mFindedTextList.append( mFindedText );
}


void TextView::getCurrentSelection( QString & selectionsStr )
{
	if ( ! mTextEdit->hasSelectedText() )
		return;

	int id = 0;
	selectionsStr = mTextEdit->selectedText();
	if ( mModeOfView == RENDERmode )
		selectionsStr.remove( 0, 20 ); // remove string <!--StartFragment-->

	if ( selectionsStr.length() > 16 )
		selectionsStr = selectionsStr.left( 16 );
	if ( (id=selectionsStr.find('\n')) != -1 )
		selectionsStr = selectionsStr.left( id-1 );
}


void TextView::setEncoding( const QString & encoding )
{
	QString enc = encoding.section( '(', 1,1 );
	if ( enc.isEmpty() )
		enc = "Automatic";
	else
		enc.remove( enc.length()-1, 1 ); // remove ')'

	debug("TextView::setEncoding, to '%s' (not yet implemented)", enc.latin1() );
}


void TextView::updateHighlightingAction()
{
	SyntaxHighlighter::SyntaxHighlighting sh = mSyntaxHighlighter->currentHighlighting();

	if ( sh != SyntaxHighlighter::NONE ) {
		mHiddenHighlInParentA->setOn( TRUE );
	}

	if ( sh == SyntaxHighlighter::NONE ) {
		if ( ! mNoneA->isOn() )  mNoneA->setOn( TRUE );
		mHiddenHighlSrcA->setOn( TRUE );
		mHiddenHighlMarA->setOn( TRUE );
	}
	else if ( sh == SyntaxHighlighter::CCpp ) {
		if ( ! mCCppA->isOn() )  mCCppA->setOn( TRUE );
		mHiddenHighlMarA->setOn( TRUE );
		//mHiddenHighlScrA->setOn( TRUE ); // scripts
	}
	else if ( sh == SyntaxHighlighter::HTML ) {
		if ( ! mHtmlA->isOn() )  mHtmlA->setOn( TRUE );
		mHiddenHighlSrcA->setOn( TRUE );
		//mHiddenHighlScrA->setOn( TRUE ); // scripts
	}
/*	else if ( sh == SyntaxHighlighter::BASH ) {}
		if ( ! mBashA->isOn() )  mBashA->setOn( TRUE );
// 		mHiddenHighlMarA->setOn( TRUE );
// 		mHiddenHighlSrcA->setOn( TRUE );
	}
	else if ( sh == SyntaxHighlighter::PERL ) {
		if ( ! mPerlA->isOn() )  mPerlA->setOn( TRUE );
// 		mHiddenHighlMarA->setOn( TRUE );
// 		mHiddenHighlSrcA->setOn( TRUE );
	}*/
	mHiddenHighlSrcA->setVisible( FALSE );
	mHiddenHighlInParentA->setVisible( FALSE );
}


void TextView::findNext()
{
	if ( mFindedText.isEmpty() ) {
		slotFind();
		return;
	}

	int p = 0, i = 0;
	if ( mStartFindFromCursorPos )
		mTextEdit->getCursorPosition( &p, &i );

	if ( ! mFromBegin )
		mFromBegin = !mTextEdit->find( mFindedText, mCaseSensitive, mWholeWordsOnly, mFindForward );
	else
		mFromBegin = !mTextEdit->find( mFindedText, mCaseSensitive, mWholeWordsOnly, mFindForward, &p, &i );

	mFindOnce = FALSE;
	if ( ! mFromBegin ) {
		mFindOnce = TRUE;
		return;
	}

	if ( ! mStartFindFromCursorPos || mBtnContinueHasPressed ) {
		MessageBox::information( this, tr("Find text")+" - QtCommander", mFindedText+"\n\n"+tr("Text not found") );
		return;
	}
	else {
		QString s0 = (mFindForward) ? tr("end")   : tr("start");
		QString s1 = (mFindForward) ? tr("begin") : tr("end");

		QStringList btnsNames;
		btnsNames << tr("Continue") << tr("Stop");
		int result = MessageBox::common( this,
		 tr("Find text")+" - QtCommander",
		 tr("Search reached")+" "+s0+" "+tr("of the document")+"\n\n"+tr("Continue from")+" "+s1+" ?",
		 btnsNames, MessageBox::Yes, QMessageBox::Information // dla 'Question' nie jest wysw. ikona ! (chyba blad w Qt)
		);
		if ( result == MessageBox::Yes ) { // Continue
			int paragraphs = 0, index = 0;
			mBtnContinueHasPressed = TRUE;
			if ( ! mFindForward ) {
				paragraphs = mTextEdit->paragraphs();
				index = mTextEdit->paragraphLength( paragraphs );
			}
			mTextEdit->setCursorPosition( paragraphs, index );
			findNext();
		}
	}
}


void TextView::createListBox()
{
	mCompletionListBox = new QListBox( mTextEdit, "listBox", WType_Popup );

	QAccel *accel = new QAccel( mCompletionListBox );
	accel->connectItem( accel->insertItem(Key_Escape), this, SLOT(slotItemChosen()) );

	connect( mCompletionListBox, SIGNAL(clicked(QListBoxItem *)),
	 this, SLOT(slotItemChosen(QListBoxItem *)));
	connect( mCompletionListBox, SIGNAL(returnPressed(QListBoxItem *)),
	 this, SLOT(slotItemChosen(QListBoxItem *)));
}


void TextView::adjustListBoxSize( int maxHeight, int maxWidth )
{
	if ( ! mCompletionListBox->count() )
		return;

	int totalHeight = mCompletionListBox->itemHeight( 0 ) * mCompletionListBox->count();
	if (mCompletionListBox->variableHeight()) {
		totalHeight = 0;
		for (uint i = 0; i < mCompletionListBox->count(); ++i)
			totalHeight += mCompletionListBox->itemHeight(i);
	}

	mCompletionListBox->setFixedHeight(QMIN(totalHeight, maxHeight));
	mCompletionListBox->setFixedWidth(QMIN(mCompletionListBox->maxItemWidth(), maxWidth));
}


QPoint TextView::textCursorPoint() const
{
	int cursorPara, cursorPos;

	mTextEdit->getCursorPosition( &cursorPara, &cursorPos );
	QRect rect = mTextEdit->paragraphRect( cursorPara );
	QPoint point( rect.left(), rect.bottom() );

	while ( mTextEdit->charAt(point, 0) < cursorPos )
		point.rx() += 10;

	return mTextEdit->mapToGlobal( mTextEdit->contentsToViewport(point) );
}


void TextView::applyKeyShortcuts( KeyShortcuts * keyShortcuts )
{
	if ( ! keyShortcuts )
		return;

	mKeyShortcuts = keyShortcuts;

	mFindA->setAccel( keyShortcuts->key(Find_TEXTVIEW) );
	mFindNextA->setAccel( keyShortcuts->key(FindNext_TEXTVIEW) );
	mReplaceA->setAccel( keyShortcuts->key(Replace_TEXTVIEW) );
	mGotoLineA->setAccel( keyShortcuts->key(GoToLine_TEXTVIEW) );
	mUndoA->setAccel( keyShortcuts->key(Undo_TEXTVIEW) );
	mRedoA->setAccel( keyShortcuts->key(Redo_TEXTVIEW) );
	mCutA->setAccel( keyShortcuts->key(Cut_TEXTVIEW) );
	mCopyA->setAccel( keyShortcuts->key(Copy_TEXTVIEW) );
	mPasteA->setAccel( keyShortcuts->key(Paste_TEXTVIEW) );
	mPrintA->setAccel( keyShortcuts->key(Print_TEXTVIEW) );
	mZoomInA->setAccel( keyShortcuts->key(ZoomIn_TEXTVIEW) );
	mZoomOutA->setAccel( keyShortcuts->key(ZoomOut_TEXTVIEW) );
}

		// ----------- SLOTs -----------

void TextView::slotChangeEOL( QAction * action )
{
	setKindOfEoL( (qstrcmp(action->name(), "unixEol") == 0) ? UNIXeol : WIN_DOSeol );
}


void TextView::slotChangeEncoding( QAction * action )
{
	QString actionName = action->name();
	if ( actionName == "hidden" )
		return;

	if ( actionName != "autoEnc" ) {
		mHiddenEncA->setVisible( FALSE );
		mHiddenEncInParentA->setOn( TRUE ); // unset an auto encoding
		mHiddenEncInParentA->setVisible( FALSE );
	}
	else {
		mHiddenEncA->setOn( TRUE ); // unset an specific encoding
		mHiddenEncA->setVisible( FALSE );
		mHiddenEncInParentA->setVisible( FALSE );
	}

	setEncoding( action->text() );
}


void TextView::slotChangeHighlighting( QAction * action )
{
	QString highlightName = action->name();
// TODO typ skladowej 'mForceKindOfSH' moznaby zmienic na QString,
// wykonac odpowiednie modyfikacje w mSyntaxHighlighter::setSyntaxHighlighting(),
// tak zeby mozna bylo przekazywac jej nazwe kolorowania (jako QString)

	if ( highlightName == "none" )
		mForceKindOfSH = SyntaxHighlighter::NONE;
	else
	if ( highlightName == "cCpp" )
		mForceKindOfSH = SyntaxHighlighter::CCpp;
	else
	if ( highlightName == "html" )
		mForceKindOfSH = SyntaxHighlighter::HTML;
	else
	if ( highlightName == "bash" )
		mForceKindOfSH = SyntaxHighlighter::BASH;
	else
	if ( highlightName == "perl" )
		mForceKindOfSH = SyntaxHighlighter::PERL;

	mSyntaxHighlighter->setSyntaxHighlighting( mForceKindOfSH );
	mSyntaxHighlighter->rehighlight();
	updateHighlightingAction();
}


void TextView::slotApplyTextViewSettings( const TextViewSettings & tvs )
{
	mFontFamily = tvs.fontFamily;
	mFontSize   = tvs.fontSize;
	mFontBold   = tvs.fontBold;
	mFontItalic = tvs.fontItalic;
	mTabWidth   = tvs.tabWidth;

	setTabWidth( mTabWidth );
	setFont( mFontFamily, mFontSize, mFontBold, mFontItalic );

	// do'nt change highlighting for different than current kind of highlighting
	if ( tvs.currentSyntaxHighlighting != mSyntaxHighlighter->currentHighlighting() )
		return;

	for ( int i=0; i<11; i++ )
		mSyntaxHighlighter->initAttributsTable( i, tvs.shAttrib[i].color, tvs.shAttrib[i].bold, tvs.shAttrib[i].italic );

	mSyntaxHighlighter->rehighlight();
}


void TextView::slotSaveSettings()
{
	QSettings *settings = new QSettings;
	QString font = mTextEdit->family()+","+QString::number(mTextEdit->pointSize());

	settings->writeEntry( "/qtcmd/TextFileView/Font", font );
	settings->writeEntry( "/qtcmd/TextFileView/TabWidth", mTabWidth );

	delete settings;
}


void TextView::slotGoToLine()
{
	bool ok;
	QString newLineNumStr = QInputDialog::getItem(
		tr("Go to line")+" - QtCommander", tr("Enter new line number:"),
		mLineNumbersList, 0, TRUE, &ok, this );

	if ( ! ok )
		return;

	uint newLine = newLineNumStr.toUInt( &ok );
	const uint maxLines = mTextEdit->paragraphs();
	if ( ok ) {
		bool notFound = TRUE;
		if ( newLine == 0 )
			newLine = 1;
		else
		if ( newLine > maxLines )
			newLine = maxLines;
		newLineNumStr = QString::number(newLine); // need to add (below) correct str.

		for( uint i=0; i<mLineNumbersList.count(); i++ ) {
			if ( mLineNumbersList[i] == newLineNumStr ) {
				notFound = FALSE;
				break;
			}
		}
		if ( notFound )
			mLineNumbersList.append( newLineNumStr );
		mTextEdit->setCursorPosition( newLine-1, 0 );
	}
}


void TextView::slotFind()
{
	getCurrentSelection( mFindedText );

	bool doFind = FindTextDialog::showDialog( this,
		mFindedTextList, mFindedText, mReplacedTextList, mReplacedText,
		mCaseSensitive, mWholeWordsOnly, mStartFindFromCursorPos, mFindForward, mPromptOnReplace );

	if ( ! doFind )
		return;

	updateFindedTextList();

	if ( ! mStartFindFromCursorPos )
		mTextEdit->setCursorPosition( 0,0 );

	mBtnContinueHasPressed = FALSE;
	findNext();

	updateFindedTextList();
}


void TextView::slotFindNext()
{
	mBtnContinueHasPressed = FALSE;
	findNext();
}


void TextView::slotReplace()
{
	// if something has selected then var.'mFindedText' will contains this string
	getCurrentSelection( mFindedText );

	// --- show replacement dialog
	bool doFind = FindTextDialog::showDialog( this,
		mFindedTextList, mFindedText, mReplacedTextList, mReplacedText,
		mCaseSensitive, mWholeWordsOnly, mStartFindFromCursorPos, mFindForward, mPromptOnReplace, TRUE );

	if ( ! doFind )
		return;

	updateFindedTextList();
	// --- update replaced text list
	bool notFound = TRUE;
	for( uint i=0; i<mReplacedTextList.count(); i++ )
		if ( mReplacedTextList[i] == mReplacedText ) {
			notFound = FALSE;
			break;
		}
	if ( notFound )
		mReplacedTextList.append( mReplacedText );

	// --- do replcement
	mBtnContinueHasPressed = FALSE;
	enum { BREAK=0, YES, NO, ALL, CLOSE };
	uint replacedCount = 0, result = YES;
	while ( 1 ) {
		findNext();
		if ( mFindOnce ) {
			if ( mPromptOnReplace && result != ALL ) {
				QStringList btnsNames;
				btnsNames << tr("&Yes") << tr("&No") << tr("&All") << tr("&Cancel");
				result = MessageBox::common( this, tr("Replace text")+" - QtCommander",
					tr("Replace this occurence ?"), btnsNames );
			}
			else
				result = ALL;

			if ( result == CLOSE || result == BREAK ) // BREAK - key Esc pressed
				break;
			else
			if ( result == YES || result == ALL ) {
				mTextEdit->insert( mReplacedText, (uint)QTextEdit::RemoveSelected );
				replacedCount++;
			}
		}
		else {
			MessageBox::information( this, tr("Replace text")+" - QtCommander",
				QString("%1 ").arg(replacedCount)+tr("replaced made") );
			break;
		}
	}
}


void TextView::slotPrint()
{ // this code is come from qmakeapp.cpp (template of qt-application from KDevelop) whith small modification
	const int Margin = 10;
	int pageNo = 1;

	if ( mPrinter->setup(this) ) { // printer dialog
		emit signalUpdateStatus( tr("Printing...") );
		QPainter p;
		if( !p.begin( mPrinter ) ) // paint on printer
			return;

		p.setFont( QFont(mFontFamily, mFontSize, (mFontBold ? QFont::Bold : QFont::Normal), mFontItalic) );
		int yPos	= 0; // y-position for each line
		QFontMetrics fm = p.fontMetrics();
		QPaintDeviceMetrics metrics( mPrinter ); // need width/height
		// of printer surface

		for( int i = 0 ; i < mTextEdit->paragraphs(); i++ ) {
			if ( Margin + yPos > metrics.height() - Margin ) {
				QString msg = "Printing (page ";
				msg += QString::number( ++pageNo ) + ")...";
				emit signalUpdateStatus( msg );
				mPrinter->newPage();  // no more room on this page
				yPos = 0;  // back to top of page
			}
			p.drawText( Margin, Margin + yPos,
			  metrics.width(), fm.lineSpacing(),
			  ExpandTabs | DontClip,
			  mTextEdit->text( i )
			);
			yPos = yPos + fm.lineSpacing();
		}
		p.end(); // send job to printer
		emit signalUpdateStatus( tr("Printing completed"), 2000 );
	}
	else
		emit signalUpdateStatus( tr("Printing aborted"), 2000 );
}


void TextView::slotCopyLinkToTheClipboard()
{ // All code of this function is come from Qt-3.1.1 source ( QLineEdit::copy() )
	disconnect( QApplication::clipboard(), SIGNAL(selectionChanged()), this, 0);
	QApplication::clipboard()->setText( mCurrentLink, QClipboard::Clipboard );
	connect( QApplication::clipboard(), SIGNAL(selectionChanged()), this, SLOT(clipboardChanged()) );
}


void TextView::slotCursorPositionChanged( int par, int col )
{
	if ( mModeOfView == EDITmode ) {
		mRowNum = par+1;  mColNum = col+1;
		updateStatus();
	}
}


void TextView::slotCompleteText()
{
	int cursorPara, cursorPos;
	mTextEdit->getCursorPosition( &cursorPara, &cursorPos );

	QString para = mTextEdit->text( cursorPara );
	int wordStart = cursorPos;
	while ( wordStart > 0 && (para[wordStart - 1].isLetterOrNumber() || para[wordStart - 1] == '_' ) )
		--wordStart;
	mWordPrefix = para.mid( wordStart, cursorPos - wordStart );
	if ( mWordPrefix.isEmpty() )
		return;

	QStringList list = QStringList::split( QRegExp("\\W+"), mTextEdit->text() );
	QMap<QString, QString> map;
	QStringList::Iterator it = list.begin();
	while ( it != list.end() ) {
		if ( (*it).startsWith(mWordPrefix) && (*it).length() > mWordPrefix.length() )
			map[(*it).lower()] = *it;
		++it;
	}

	if ( map.count() == 1 ) {
		mTextEdit->insert( (*map.begin()).mid(mWordPrefix.length()) );
	}
	else
	if ( map.count() > 1 ) {
		if ( !mCompletionListBox )
			createListBox();
		mCompletionListBox->clear();
		mCompletionListBox->insertStringList( map.values() );

		QPoint point = textCursorPoint();
		adjustListBoxSize( qApp->desktop()->height() - point.y(), width() / 2 );
		mCompletionListBox->setSelected( mCompletionListBox->firstItem(), TRUE );
		mCompletionListBox->move( point );
		mCompletionListBox->show();
		mCompletionListBox->raise();
		mCompletionListBox->setActiveWindow();
	}
}


void TextView::slotItemChosen( QListBoxItem *item )
{
	if ( item )
		mTextEdit->insert( item->text().mid(mWordPrefix.length()) );
	else
		mTextEdit->setFocus();

	mCompletionListBox->close();
	mParent->raise();
	mParent->setActiveWindow();
	mTextEdit->raise();
	mTextEdit->setActiveWindow();
}


		// ----------- EVENTs -----------

bool TextView::keyPressed( QKeyEvent *e )
{
	if ( ! mKeyShortcuts )
		return FALSE;

	int key   = e->key();
	int state = e->state();
	int ks    = mKeyShortcuts->ke2ks( e );

	if ( key == Key_Insert ) {
		if ( state == ControlButton ) {
			mTextEdit->copy();
			return FALSE; // let's eat event
		}
		else
		if ( state == AltButton )
			return FALSE; // let's eat event
		else
		if ( state == ShiftButton )
			return TRUE;

		mTextEdit->setOverwriteMode( mOvrMode );
		mOvrMode = ! mOvrMode;
		mInsStatStr = (mOvrMode) ? "OVR" : "INS";
		updateStatus();
	}
	else
	if ( ks == mKeyShortcuts->key(SelectAll_TEXTVIEW) ) {
		mTextEdit->selectAll();
		return FALSE; // let's eat event
	}
	else
	if ( ks == mKeyShortcuts->key(TextCompletion_TEXTVIEW) ) {
		slotCompleteText();
		return FALSE; // let's eat event
	}

	return TRUE;
}


void TextView::mousePressEvent( QMouseEvent *e )
{
	if ( e->button() == RightButton )  {
		QApplication::restoreOverrideCursor();

		int selectAllKCode  = ( mKeyShortcuts ) ? mKeyShortcuts->key(SelectAll_TEXTVIEW)  : CTRL+Key_A;
		QString findKCodeStr    = ( mKeyShortcuts ) ? mKeyShortcuts->keyStr(Find_TEXTVIEW)      : "CTRL+F";
		QString replaceKCodeStr = ( mKeyShortcuts ) ? mKeyShortcuts->keyStr(Replace_TEXTVIEW)   : "CTRL+R";

		QPopupMenu *popupMenu = new QPopupMenu( this );
		bool hasSelectedText = mTextEdit->hasSelectedText();
		popupMenu->insertItem( tr("Select &All"), mTextEdit, SLOT(selectAll()), selectAllKCode, 1 );
		popupMenu->insertItem( tr("&Copy"), mTextEdit, SLOT(copy()), CTRL+Key_Insert, 2 );
		popupMenu->setItemEnabled( 2, hasSelectedText );

		if ( ! mCurrentLink.isEmpty() ) {
			popupMenu->insertSeparator();
			popupMenu->insertItem( tr("Copy &link location"), this, SLOT(slotCopyLinkToTheClipboard()), 0, 3 );
		}

		QString selections;
		if ( hasSelectedText ) {
			getCurrentSelection( selections );

			popupMenu->insertSeparator();
			popupMenu->insertItem( tr("&Find")+": "+selections+"\t"+findKCodeStr, 4 );

			if ( mModeOfView == EDITmode )
				popupMenu->insertItem( tr("&Replace")+": "+selections+"\t"+replaceKCodeStr, 5 );
		}

		int result = popupMenu->exec( mTextEdit->mapToGlobal(QPoint(e->x(), e->y()+2)) );
		delete popupMenu;

		if ( result > 3 ) {
			mFindedText = selections;
			updateFindedTextList();
		}
		if ( result == 4 ) slotFind();
		else
		if ( result == 5 ) slotReplace();
	}
}


void TextView::mouseMoveEvent( QMouseEvent *e )
{
	if ( mModeOfView == RENDERmode ) {
		int xPos = e->pos().x();
		int yPos = e->pos().y();
		int width  = mTextEdit->width() - (mTextEdit->verticalScrollBar()->isVisible() ? mTextEdit->verticalScrollBar()->width()+3 : 0);
		int height = mTextEdit->height() - (mTextEdit->horizontalScrollBar()->isVisible() ? mTextEdit->horizontalScrollBar()->height()+3 : 2);

		if ( yPos < 0 || xPos < 0 || xPos > width || yPos > height ) {
			QApplication::restoreOverrideCursor();
			return;
		}

		uint xMousePos = e->pos().x() + mTextEdit->contentsX();
		uint yMousePos = e->pos().y() + mTextEdit->contentsY();
		mCurrentLink = mTextEdit->anchorAt( QPoint( xMousePos, yMousePos ) );

		QApplication::restoreOverrideCursor();
		if ( ! mCurrentLink.isEmpty() )
			QApplication::setOverrideCursor( PointingHandCursor );

		emit signalUpdateStatus( mCurrentLink );
	}
/*
	else
	if ( mModeOfView == RAWmode ) {
	// ponizsze dzialaloby poprawnie tylko wtedy, gdy czcionka bylaby rownej szerokosci, np. Courier/Fixed
		int row = e->pos().y() / (QFontMetrics(mTextEdit->currentFont()).height()+2);
		int col = e->pos().x() / QFontMetrics(mTextEdit->currentFont()).width("b");
		row++;  if ( row > mTextEdit->lines() ) row = lines();
		col++;  if ( col > mTextEdit->paragraphLength(row-1) )  col = mTextEdit->paragraphLength(row-1);
		mRowNum = row;
		mColNum = col;
		updateStatus();
	}
*/
}


bool TextView::eventFilter( QObject *o, QEvent *e )
{
	if ( o == mTextEdit ) {
		if ( e->type() == QEvent::KeyPress )
			if ( ! keyPressed( (QKeyEvent *)e ) )
				return TRUE; // eat current event
		else
		if ( e->type() == QEvent::FocusOut )
			QApplication::restoreOverrideCursor();
	}
	else
	if ( o == mTextEdit->viewport() ) {
		if ( e->type() == QEvent::MouseButtonPress )
			mousePressEvent( (QMouseEvent *)e );
		else
		if ( e->type() == QEvent::MouseMove )
			mouseMoveEvent( (QMouseEvent *)e );
	}

	return QWidget::eventFilter( o, e );    // standard event processing
}

