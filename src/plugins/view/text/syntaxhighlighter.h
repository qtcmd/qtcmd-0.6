/***************************************************************************
                          syntaxhighlighter.h  -  description
                             -------------------
    begin                : thu apr 17 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
***************************************************************************/

#ifndef _SYNTAXHIGHLIGHTER_H_
#define _SYNTAXHIGHLIGHTER_H_

#include <qsyntaxhighlighter.h>
#include <qtextedit.h>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa zarz�dza kolorowaniem sk�adni widoku tekstowego.
 Klaas ma za zadanie wykonanie kolorowania sk�adni dla podanego jego
 rodzaju pliku. Aby m�c wykorzysta� klase nale�y j� powi�za� z obiektem klasy
 QTextEdit, do k�rego nale�y poda� wska�nik w konstruktorze. \n
 Regu�y kolorowania s� wczytywane z pliku konfiguracyjnego
 (je�li nie zostan� tam znalezione, wtedy brane s� domy�lne).\n
 Ob�ugiwane jest kolorowanie sk�adni dla j�zyk�w: C/C++ oraz HTML/XML.
*/
class SyntaxHighlighter : public QSyntaxHighlighter
{
public:
	/** Lista rodzaj�w kolorowania.
	*/
	enum SyntaxHighlighting {
		NONE=0,
		HTML=100, CSS, LaTeX, XML, SGML, VRML, // markup
		BASH=200, PERL, PHP, PYTHON, AWK, SQL, RUBY, // scripts
		CCpp=300, JAVA, ASM, PASCAL, EIFFEL, PROLOG, HASKELL, ADA, // source
		HEX=400,  CHANGELOG, DIFF, MAKEFILE, RPMspec, // other
		AUTO=1000
	};

	/** Konstruktor klasy, zajmuj�cej si� "pod�wietlaniem" sk�adni.\n
	 @param textEdit - wska�nik na obiekt podgl�du tekstowego.\n
	 Inicjowane s� tutaj: domy�lna czcionka oraz reg�y kolorowania sk�adni.
	*/
	SyntaxHighlighter( QTextEdit * textEdit );

	/** Inicjuje kolorowanie sk�adni.\n
	 @param sh - rodzaj kolorowania sk�adni, patrz @see SyntaxHighlighting,
	 @param ext - rozszerzenie pliku, dla kt�rego nast�puje kolorowanie sk�adni.\n
	 Ustawiana jest tutaj sk�adowa okre�laj�ca rodzaj kolorowania sk�adni
	 oraz wczytywane z pliku konfiguracyjnego regu�y kolorowania sk�adni dla
	 wybranego wcze�niej jego rodzaju.
	 */
	void setSyntaxHighlighting( SyntaxHighlighting sh, const QString & ext=QString::null );

	/** Inicjuje podanymi warto�ciami bie��cy kontekst kolorowania.\n
	 @param context - kontekst kolorowania, patrz: @see ContextOfSH,
	 @param color - kolor w danym kontek�cie,
	 @param bold - pogrubienie tekstu (TRUE) w danym kontek�cie,
	 @param italic - pochylenie tekstu (TRUE) w danym kontek�cie.\n
	 Jest tutaj inicjowana tablica atrybut�w, z kt�rej s� pobierane informacje
	 o sposobie kolorowania w funkcji ustawiaj�cej atrybuty dla kontekstu.
	 */
	void initAttributsTable( int context, const QColor & color, bool bold, bool italic );

	/** Zwraca bie��cy rodzaj kolorowania sk�adni.\n
	 @em return - rodzaj kolorowania sk�adni
	 */
	SyntaxHighlighting currentHighlighting() const { return mSyntaxHighlighting; }

private:
	SyntaxHighlighting mSyntaxHighlighting;
	QColor mColor;
	QFont mFont;

	QStringList mCcppRullesList, mHtmlRullesList;

	struct Attribut {
		QColor color;
		bool bold;
		bool italic;
	};
	Attribut AttrTab[11];

	enum ContextOfSH {
		BASE_N_INTEGER,
		CHARACTER,
		COMMENT,
		DATA,
		DECIMALorVALUE,
		EXTENSION,
		FLOATING_POINT,
		KEYWORD,
		NORMAL,
		OTHERS,
		STRING
	};

	typedef QMap<QString, ContextOfSH> ContextMap;
	ContextMap mContextMap;

protected:
	/** Funkcja wywo�ywana automatycznie przy ka�dej zmianie zawarto�ci obiektu
	 rodzica (QTextEdit).\n
	 @param text - aktualnie zmodyfikowana linia (ci�g zako�czony znakiem '\n').
	 @param endStateOfLastPara - status zako�czenia kolorowania dla bie��cego
	 paragrafu.
	 @return status zako�czenia kolorowania bie��cego paragrafu
	 */
	int highlightParagraph( const QString & text, int endStateOfLastPara );


	/** Funkcja parsuj�ca jedn� lini� tekstu dla �r�d�a C/CPP oraz ustawiaj�ca
	 w niej pod�wietlanie sk�adni.\n
	 @param text - linia tekstu do przeparsowania (ci�g zako�czony znakiem '\n'),
	 @param endStateOfLastPara - informuje o wieloliniowym kolorowaniem
	 (wi�cej informacji szukaj w dokumentacji Qt - klasa QSyntaxHighlighting)
	 @return status zako�czenia kolorowania bie��cego paragrafu
	 */
	int setSyntaxHighlightingForCCpp( const QString & text, int endStateOfLastPara );

	/** Funkcja parsuj�ca jedn� lini� tekstu dla �r�d�a HTML oraz ustawiaj�ca
	 w niej pod�wietlanie sk�adni.\n
	 @param text - linia tekstu do przeparsowania (ci�g zako�czony znakiem '\n'),
	 @param endStateOfLastPara - informuje o wieloliniowym kolorowaniem
	 (wi�cej informacji szukaj w dokumentacji Qt - klasa QSyntaxHighlighting)
	 @return status zako�czenia kolorowania bie��cego paragrafu
	 */
	int setSyntaxHighlightingForHtml( const QString & text, int endStateOfLastPara );

	/** Funkcja ustawiaj�ca w kolorowanie sk�adni w jednej linii dla widoku HEX.\n
	 @param text - linia tekstu do przeparsowania (ci�g zako�czony znakiem '\n'),
	 @param endStateOfLastPara - informuje o wieloliniowym kolorowaniem
	 (wi�cej informacji szukaj w dokumentacji Qt - klasa QSyntaxHighlighting)
	 @return status zako�czenia kolorowania bie��cego paragrafu
	*/
	int setSyntaxHighlightingForHEX( const QString & text, int endStateOfLastPara );


	/** Funkcja ustawia atrybuty: color, bold, italic (b�d�ce sk�adowymi klasy)
	 dla podanego kontekstu.\n
	 @param context - kontekst kolorowania.
	 */
	void setAttributsFor( ContextOfSH );


	/** Funkcja wykonuje sprawdzenie czy podany znak ma specjalne znaczenie
	 w po��czeniu z '\', np. '\n'. Pomocna przy parsowaniu C/C++.\n
	 @param chr - znak do sprawdzenia,
	 @return TRUE, je�li powy�szy warunek zosta� spe�niony, w przeciwnym razie
	 FALSE.\n
	 Jest u�ywana przez funkcje kolorowania sk�adni dla C/C++.
	 */
	bool specChar( const QChar & );

	/** Funkcja sprawdza czy podany znak :\n
		@li nie jest du�� lub ma�� liter�,
		@li jest r�ny od '$',
		@li jest r�wny '\n' lub '\t'.
	 @param chr - znak do sprawdzenia,
	 @return TRUE, je�li powy�szy warunek zosta� spe�niony, w przeciwnym razie
	 FALSE.\n
	 Jest u�ywana przez funkcje kolorowania sk�adni dla C/C++.
	 */
	bool charOkForDigit( const QChar & chr);

	/** Funkcja sprawdzaj�ca czy podany zosta� znak, kt�ry mo�e wyst�pi� za s�owem
	 kluczowym j�zyka C/C++.\n
	 @param chr - znak do sprawdzenia,
	 @return TRUE, je�li powy�szy warunek zosta� spe�niony, w przeciwnym razie
	 FALSE.\n
	 Jest u�ywana przez funkcje kolorowania sk�adni dla C/C++.
	 */
	bool charOkBehindKeyword( const QChar & chr );

};

#endif
