# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/plugins/fp/settings
# Cel to biblioteka settingsapp

INSTALLS += target 
target.path = $$PREFIX/lib/qtcmd/plugins 
FORMS += ui/colorspage.ui \
         ui/columnspage.ui \
         ui/fontapppage.ui \
         ui/ftppage.ui \
         ui/keyshortcutsapppage.ui \
         ui/lfspage.ui \
         ui/archivespage.ui \
         ui/otherfspage.ui \
         ui/otherlistsettingspage.ui \
         ui/toolbarapppage.ui \
         ui/settingsdialog.ui \
         ui/filecolorspage.ui 
TRANSLATIONS += ../../../../translations/pl/qtcmd-setts.ts 
HEADERS += settingsdialogplugin.h \
           ui/colorspage.ui.h \
           ui/columnspage.ui.h \
           ui/fontapppage.ui.h \
           ui/ftppage.ui.h \
           ui/keyshortcutsapppage.ui.h \
           ui/lfspage.ui.h \
           ui/archivespage.ui.h \
           ui/otherfspage.ui.h \
           ui/otherlistsettingspage.ui.h \
           ui/settingsdialog.ui.h \
           ui/toolbarapppage.ui.h \
           ui/filecolorspage.ui.h \
           listviewsettings.h \
           filesystemsettings.h \
           otherappsettings.h 
SOURCES += settingsdialogplugin.cpp 
DEPENDPATH = ui
TARGETDEPS += ../../../../.libs/libqtcmduiext.so \
../../../../.libs/libqtcmdutils.so
LIBS += -lqtcmduiext \
-lqtcmdutils
INCLUDEPATH += ../../../../src \
../../../../src/libs/qtcmdutils \
../../../../src/libs/qtcmduiext
MOC_DIR = ../../../../.moc
UI_DIR = ui
OBJECTS_DIR = ../../../../.obj
QMAKE_LIBDIR = ../../../../.libs \
../../../../.libs
TARGET = settingsapp
DESTDIR = ../../../../.libs/plugins
CONFIG += warn_on \
qt \
thread \
plugin
TEMPLATE = lib
PRECOMPILED_HEADER = ../../../pch.h
