/***************************************************************************
                          settingsdialogplugin.cpp  -  description
                             -------------------
    begin                : wed nov 5 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "settingsdialogplugin.h"
#include "ui/settingsdialog.h"

// export symbol for resolve it by 'QLibrary::resolve( const char * )'
extern "C" {
	QWidget * create_widget( QWidget * parent=0, const char * name=0 )
	{
		return new SettingsDialog( parent, name, TRUE );
	}
}

Q_EXPORT_PLUGIN( SettingsDialogPlugin )
