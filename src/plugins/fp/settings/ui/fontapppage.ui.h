/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qsettings.h>

void FontAppPage::init()
{
// --- read current font from config file
	QSettings settings;
	mOriginalFont = settings.readEntry( "/qtcmd/FilesPanel/Font" );
	if ( mOriginalFont.isEmpty() )
		mOriginalFont = "Helvetica,10"; // default

	QToolTip::add( mOriginalFontBtn, mOriginalFont );
	updateFontFamilies();
	slotSetOriginalFont();
}


void FontAppPage::updateFontFamilies()
{
// --- get font family list
	mFamilyNameList = mFontDataBase.families();

	QString s;
	QStringList newList;
	QStringList::Iterator it = mFamilyNameList.begin();
	for( ; it != mFamilyNameList.end() ; it++ )
	{
		s = *it;
		if ( s.contains('-') )  {
			int i = s.find('-');
			s = s.right( s.length() - i - 1 ) + " [" + s.left( i ) + "]";
		}
		s[ 0 ] = s[ 0 ].upper();
#if 0
		if ( mFontDataBase.isSmoothlyScalable( *it ) )
			newList.append( s + "(TT)" );
		else if ( mFontDataBase.isBitmapScalable( *it ) )
			newList.append( s + "(BT)" );
		else
#endif
		newList.append( s );
	}

	mFontlistBox->insertStringList( newList );
}


void FontAppPage::updateFontSizes()
{
	mFontSizeComboBox->clear();
	QValueList<int> sizes = mFontDataBase.pointSizes( mFontlistBox->currentText(), mStyle, mCharSet );

	if ( sizes.isEmpty() )  {
		debug( "FontAppPage::updateFontSizes(): Internal error, "
		"no pointsizes for family \"%s\" with script \"%s\"\n and style \"%s\"",
		(const char *)mFontlistBox->currentText(), (const char *)mCharSet, (const char *)mStyle );
		return;
	}

	QString sizeStr;
	for(uint i=0; i<sizes.count(); i++)  {
		sizeStr.sprintf( "%i", sizes[ i ] );
		mFontSizeComboBox->insertItem( sizeStr );
	}
}



void FontAppPage::slotFontFamilyHighlighted( int id )
{
	QString family = mFamilyNameList[ id ];

//	charSetNames = mFontDataBase.charSets( family ); // Qt-ver. < 3.x
	QStringList charSetNames = mFontDataBase.families();

	if ( charSetNames.isEmpty() )  {
		debug( "SettOther::familyHighlighted(): Internal error, no character sets for family \"%s\"", (const char *)family );
		return;
	}

	if ( ! charSetNames.isEmpty() )
		mCharSet = charSetNames[ 0 ];

	QStringList styles = mFontDataBase.styles( family, mCharSet );
	mStyle = styles[ 0 ]; // select always style "normal"

	uint currSizePoint = mFontSizeComboBox->currentItem();
	int currentFontSize = ( mFontSizeComboBox->count() > 0 ) ? mFontSizeComboBox->text( currSizePoint ).toInt() : 0;
	updateFontSizes();

	// checks is current familly has on own list size previous font familly too
	QValueList<int> sizes = mFontDataBase.pointSizes( family, mStyle, mCharSet );
	currSizePoint = 0;
	for ( uint sizePoint=0; sizePoint < sizes.count(); sizePoint++ )
		if ( sizes[ sizePoint ] == currentFontSize )  {
			currSizePoint = sizePoint;
			break;
		}

	slotFontSizeHighlighted( currSizePoint );
	mFontSizeComboBox->setCurrentItem( currSizePoint );
}


void FontAppPage::slotFontSizeHighlighted( int id )
{
	if ( id < 0 )
		return;

	int fontSize = mFontSizeComboBox->text( id ).toInt();
	QString currentFontFamily = mFontlistBox->currentText();

	int fontWeight = (mBoldChkBox->isChecked()) ? QFont::Bold : QFont::Normal;
	mFontTestLineEdit->setFont(  QFont(currentFontFamily, fontSize, fontWeight, mItalicChkBox->isChecked())  );
}

void FontAppPage::slotSetDefaultFont()
{
	mItalicChkBox->setChecked( FALSE );
	mBoldChkBox->setChecked( FALSE );
	setPreviewFont( "Helvetica", 10 );
}


void FontAppPage::slotSetOriginalFont()
{
	// parse 'mOriginalFont'
	QString boldStr     = mOriginalFont.section( ',', 2,2 ).stripWhiteSpace().lower();
	QString italicStr   = mOriginalFont.section( ',', 3,3 ).stripWhiteSpace().lower();
	QString fontSizeStr = mOriginalFont.section( ',', 1,1 ).stripWhiteSpace();
	bool ok;
	uint fontSize = fontSizeStr.toInt( &ok );
	if ( ! ok )
		fontSize = 10; // default size

	mBoldChkBox->setChecked( (boldStr == "bold") );
	mItalicChkBox->setChecked( (italicStr == "italic") );
	setPreviewFont( mOriginalFont.section( ',', 0,0 ), fontSize );
}


void FontAppPage::setPreviewFont( const QString & family, int size )
{
	int idFontFamily = -1;
	idFontFamily = mFontlistBox->index( mFontlistBox->findItem(family, Qt::CaseSensitive) );
	if ( idFontFamily < 0 )
		if ( family != "Helvetica" ) { // family different than default font family
			debug("FontAppPage::setPreviewFont, fontFamily='%s' not found", family.latin1() );
			slotSetDefaultFont();
			return;
		}
		else // not found default font family, get first avilable
			idFontFamily = 0;

	mFontlistBox->setCurrentItem( idFontFamily );


	int idFontSize = -1;
	int sizeToFind = size;
	while ( idFontSize == -1 ) {
		for (int i=0; i<mFontSizeComboBox->count(); i++)
			if ( mFontSizeComboBox->text(i).toInt() == sizeToFind ) {
				idFontSize = i;
				break; // 'for' loop
			}
		if ( idFontSize == -1 )
			debug("FontAppPage::setPreviewFont, not found font size=%d", sizeToFind );
		sizeToFind++; // not found passed font size
	}

	mFontSizeComboBox->setCurrentItem( idFontSize );
}

void FontAppPage::slotSetFontAttribut()
{
	slotFontSizeHighlighted( mFontSizeComboBox->currentItem() );
}


QString FontAppPage::fontFamily()
{
	return mFontlistBox->currentText();
}

uint FontAppPage::fontSize()
{
	return mFontSizeComboBox->text( mFontSizeComboBox->currentItem() ).toInt();
}

bool FontAppPage::fontItalic()
{
	return mItalicChkBox->isChecked();
}


bool FontAppPage::fontBold()
{
	return mBoldChkBox->isChecked();
}


void FontAppPage::slotEncodingHighlighted( int id )
{

}

void FontAppPage::slotSave()
{
	QSettings settings;

	QString fontStr = fontFamily()+","+QString::number(fontSize());

	if ( mBoldChkBox->isChecked() )
		fontStr += ",Bold";
	if ( mItalicChkBox->isChecked() )
		fontStr += ",Italic";

	settings.writeEntry( "/qtcmd/FilesPanel/Font", fontStr );
}
