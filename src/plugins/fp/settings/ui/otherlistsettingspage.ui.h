/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include "enums.h"

#include <qsettings.h>
#include <qinputdialog.h>


void OtherListSettingsPage::init()
{
	QSettings settings;

	bool showIcons = settings.readBoolEntry( "/qtcmd/FilesPanel/ShowIcons", TRUE );
	bool showHidden = settings.readBoolEntry( "/qtcmd/FilesPanel/ShowHidden", TRUE );
	bool selectFilesOnly = settings.readBoolEntry( "/qtcmd/FilesPanel/SelectFilesOnly", FALSE );
	bool notHiddenCursor = settings.readBoolEntry( "/qtcmd/FilesPanel/NotHiddenCursor", TRUE );
	bool filterForFiltered = settings.readBoolEntry( "/qtcmd/FilesPanel/FilterForFiltered", FALSE );

	mShowIconsChkBox->setChecked( showIcons );
	mShowHiddenChkBox->setChecked( showHidden );
	mSelectFilesOnlyChkBox->setChecked( selectFilesOnly );
	mCursorAlwaysVisibleChkBox->setChecked( notHiddenCursor );
	mFilterForFilteredChkBox->setChecked( filterForFiltered );

	int fileSizeFormat = settings.readNumEntry( "/qtcmd/FilesPanel/FileSizeFormat", BKBMBGBformat );
	if ( fileSizeFormat < NONEformat || fileSizeFormat > BKBMBGBformat )
		fileSizeFormat = BKBMBGBformat;
	((QRadioButton *)mFileSizeFormatBtnGroup->find(fileSizeFormat))->setChecked(TRUE);

	mFilters = NULL;
}


int OtherListSettingsPage::defaultFilter()
{
	return mFiltersComboBox->currentItem();
}

int OtherListSettingsPage::fileSizeFormat()
{
	return mFileSizeFormatBtnGroup->selectedId();
}

/** Uaktualniana jest tu lista filtr�w.\n
 @param fa - wska�nik na skojarzenia plik�w.\n
 Metoda wywo�ywana podczas uruchamiania dialogu ustawie�.
 */
void OtherListSettingsPage::updateFilters( FilesAssociation * fa )
{
	if ( mFilters )
		delete mFilters;

	// --- init the filters
	mFilters = new Filters( fa );
	for ( uint i=0; i<mFilters->count(); i++ )
		mFiltersComboBox->insertItem( mFilters->name(i) );

	slotFilterHighlighted( mFilters->defaultId() );
}

/** Ustawiane s� tutaj domy�lne warto�ci dla dialogu.
 */
void OtherListSettingsPage::slotSetDefaults()
{
	mShowIconsChkBox->setChecked( TRUE );
	mShowHiddenChkBox->setChecked( TRUE );
	mSelectFilesOnlyChkBox->setChecked( FALSE );
	mCursorAlwaysVisibleChkBox->setChecked( TRUE );
	mFiltersComboBox->setCurrentItem( 0 ); // files and directories
	mFilterForFilteredChkBox->setChecked( FALSE );

	mWeighFormatRadioBtn->setChecked( TRUE );
}


void OtherListSettingsPage::slotFilterHighlighted( int filterId )
{
	mExtentionsLineEdit->setText( mFilters->patterns(filterId) );
	mExtentionsLineEdit->setCursorPosition( 0 );
}


void OtherListSettingsPage::slotSave()
{
	QSettings settings;

	settings.writeEntry( "/qtcmd/FilesPanel/ShowIcons", mShowIconsChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesPanel/ShowHidden", mShowHiddenChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesPanel/SelectFilesOnly", mSelectFilesOnlyChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesPanel/NotHiddenCursor", mCursorAlwaysVisibleChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesPanel/DefaultFilesFilter", mFiltersComboBox->currentItem() );
	settings.writeEntry( "/qtcmd/FilesPanel/FileSizeFormat", mFileSizeFormatBtnGroup->selectedId() );
	settings.writeEntry( "/qtcmd/FilesPanel/FilterForFiltered", mFilterForFilteredChkBox->isChecked() );
}
