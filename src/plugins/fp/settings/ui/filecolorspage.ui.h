/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/

#include "functions_col.h" // for color() and colorToString()
#include "buttonColor.xpm"

#include <qinputdialog.h>
#include <qcolordialog.h>
#include <qsettings.h>
#include <qimage.h>

#include <qpainter.h>
#include <qpixmap.h>


void FileColorsPage::init()
{
	QSettings settings;

	QStringList schemesNameList = settings.readListEntry( "/qtcmd/FilesPanel/FileColorsSchemeList" );

	if ( ! schemesNameList.count() )
		schemesNameList.append( "DefaultColors" );
	if ( schemesNameList.findIndex( "DefaultColors" ) == -1 )
		schemesNameList.insert( 0, "DefaultColors" );

	QString scheme = settings.readEntry( "/qtcmd/FilesPanel/FileColorsScheme", "DefaultColors" );
// 	scheme.insert(0, "File");
// 	scheme[4] = scheme[4].upper();
	mSchemesList.clear();
	// TODO domyslny kolor to itemColor z biezacego schematu kolorow
	FileColorsScheme fileColorsScheme;
	QString sColor;

	for ( uint id=0; id<schemesNameList.count(); id++ ) {
		scheme = schemesNameList[id];

		sColor = settings.readEntry( "/qtcmd/"+scheme+"/CommonFileColor", "000000" );
		fileColorsScheme.commonFileColor = color(sColor, "000000");

		sColor = settings.readEntry( "/qtcmd/"+scheme+"/ExeFileColor", "000000" );
		fileColorsScheme.exeFileColor = color(sColor, "000000");

		sColor = settings.readEntry( "/qtcmd/"+scheme+"/SymlinkColor", "000000" );
		fileColorsScheme.symlinkColor = color(sColor, "000000");

		sColor = settings.readEntry( "/qtcmd/"+scheme+"/BrokenSymlinkColor", "000000" );
		fileColorsScheme.brokenSymlinkColor = color(sColor, "000000");

		sColor = settings.readEntry( "/qtcmd/"+scheme+"/SocketColor", "000000" );
		fileColorsScheme.socketColor = color(sColor, "000000");

		sColor = settings.readEntry( "/qtcmd/"+scheme+"/CharacterDevColor", "000000" );
		fileColorsScheme.characterDevColor = color(sColor, "000000");

		sColor = settings.readEntry( "/qtcmd/"+scheme+"/BlockDevColor", "000000" );
		fileColorsScheme.blockDevColor = color(sColor, "000000");

		sColor = settings.readEntry( "/qtcmd/"+scheme+"/FifoColor", "000000" );
		fileColorsScheme.fifoColor = color(sColor, "000000");

		mSchemesList.append( fileColorsScheme );
	}

	mSchemeComboBox->insertStringList( schemesNameList );
	mSchemeComboBox->setCurrentText( scheme ); // co jesli bedzie zla wart., spr. to !
	slotSetNewScheme( mSchemeComboBox->currentItem() );
}

void FileColorsPage::drawPreview()
{
	int px = 5, py=0;
	const int fontH = 12;
	const int topMargin = 10;
	const int cursH = fontH + 3;
	const int pw = mPreviewLabel->width()-2, ph = mPreviewLabel->height()-2;
	const int maxLine = (ph-topMargin)/fontH; // == 7

	enum Colors {
		CommonFileColor=0, ExeFileColor, SymlinkColor, BrokenSymlinkColor,
		SocketColor, CharacterDevColor, BlockDevColor, FifoColor
	};


	QPixmap screen;
	screen.resize( pw, ph );
	screen.fill( Qt::white );

	QPainter *p = new QPainter;
	p->begin( &screen );

	// draw background (always white)
	p->fillRect( 0, topMargin, pw, cursH, Qt::white );

	// draw the first and next line
	py += fontH + topMargin;
	p->setPen( getColor(CommonFileColor) );
	p->drawText( px, py, tr("Common file") );

	py += fontH + topMargin;
	p->setPen( getColor(ExeFileColor) );
	p->drawText( px, py, tr("Executable file") );

	py += fontH + topMargin;
	p->setPen( getColor(SymlinkColor) );
	p->drawText( px, py, tr("SymLink") );

	py += fontH + topMargin;
	p->setPen( getColor(BrokenSymlinkColor) );
	p->drawText( px, py, tr("Broken symlink") );

	py += fontH + topMargin;
	p->setPen( getColor(SocketColor) );
	p->drawText( px, py, tr("Socket") );

	py += fontH + topMargin;
	p->setPen( getColor(CharacterDevColor) );
	p->drawText( px, py, tr("Character device") );

	py += fontH + topMargin;
	p->setPen( getColor(BlockDevColor) );
	p->drawText( px, py, tr("Block device") );

	py += fontH + topMargin;
	p->setPen( getColor(FifoColor) );
	p->drawText( px, py, tr("Fifo") );

	p->end();

	mPreviewLabel->setPixmap( screen );
}

QColor FileColorsPage::getColor( int nColorId )
{
	QToolButton *btn = (QToolButton *)mColorsBtnGroup->find( nColorId );
	QPixmap btnPixmap = btn->iconSet().pixmap();
	if ( btnPixmap.isNull() )
		return QColor(0,0,0);

	return btnPixmap.convertToImage().pixel(1,1);
}

void FileColorsPage::slotSetNewScheme( int nSchemeId )
{
	QPixmap btnPixmap( (const char**)buttonColor );
	mSchemeComboBox->setCurrentItem( nSchemeId );

	// --- sets colors on the buttons
	btnPixmap.fill( mSchemesList[nSchemeId].commonFileColor );
	mCommonFileColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[nSchemeId].exeFileColor );
	mExeColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[nSchemeId].symlinkColor );
	mSymlinkColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[nSchemeId].brokenSymlinkColor );
	mBrokenSymlinkColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[nSchemeId].socketColor );
	mSocketColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[nSchemeId].characterDevColor );
	mCharacterDevColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[nSchemeId].blockDevColor );
	mBlockDevColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[nSchemeId].fifoColor );
	mFifoColBtn->setIconSet( btnPixmap );

	// disable delete and save buttons only for default scheme
	mDeleteBtn->setEnabled( nSchemeId );

	drawPreview();
}

void FileColorsPage::slotSetColor( int nColorId )
{
	// --- gets color from the button
	QToolButton *btn = (QToolButton *)mColorsBtnGroup->find( nColorId );
	QPixmap btnPixmap = btn->iconSet().pixmap();
	QColor initColor = btnPixmap.convertToImage().pixel(1,1);
	QColor newColor  = QColorDialog::getColor( initColor );

	if ( ! newColor.isValid() )
		return;
	// --- sets color on the button
	btnPixmap.fill( newColor );
	btn->setIconSet( btnPixmap );

	enum Colors {
		CommonFileColor=0, ExeFileColor, SymlinkColor, BrokenSymlinkColor,
		SocketColor, CharacterDevColor, BlockDevColor, FifoColor
	};

	int schemeId = mSchemeComboBox->currentItem();
	if (schemeId < 1) {
		drawPreview();
		return;
	}

	// --- sets colors for scheme
	if ( nColorId == CommonFileColor )
		mSchemesList[schemeId].commonFileColor = newColor;
	else
	if ( nColorId == ExeFileColor )
		mSchemesList[schemeId].exeFileColor = newColor;
	else
	if ( nColorId == SymlinkColor )
		mSchemesList[schemeId].symlinkColor = newColor;
	else
	if ( nColorId == BrokenSymlinkColor )
		mSchemesList[schemeId].brokenSymlinkColor = newColor;
	else
	if ( nColorId == SocketColor )
		mSchemesList[schemeId].socketColor = newColor;
	else
	if ( nColorId == CharacterDevColor )
		mSchemesList[schemeId].characterDevColor = newColor;
	else
	if ( nColorId == BlockDevColor )
		mSchemesList[schemeId].blockDevColor = newColor;
	else
	if ( nColorId == FifoColor )
		mSchemesList[schemeId].fifoColor = newColor;

	drawPreview();
}

void FileColorsPage::slotSave()
{
	QStringList schemesNameList;
	QSettings settings;
	QString scheme;

	schemesNameList.append( "DefaultColors" );

	for ( uint id=1; id<mSchemesList.count(); id++ ) { // start from 1, becouse need to skip default scheme
		scheme = mSchemeComboBox->text(id);
// 		scheme.insert(0, "File");
// 		scheme[4] = scheme[4].upper();
		schemesNameList.append( scheme );

		settings.writeEntry( "/qtcmd/"+scheme+"/CommonFileColor", colorToString(mSchemesList[id].commonFileColor) );
		settings.writeEntry( "/qtcmd/"+scheme+"/ExeFileColor", colorToString(mSchemesList[id].exeFileColor) );
		settings.writeEntry( "/qtcmd/"+scheme+"/SymlinkColor", colorToString(mSchemesList[id].symlinkColor) );
		settings.writeEntry( "/qtcmd/"+scheme+"/BrokenSymlinkColor", colorToString(mSchemesList[id].brokenSymlinkColor) );
		settings.writeEntry( "/qtcmd/"+scheme+"/SocketColor", colorToString(mSchemesList[id].socketColor) );
		settings.writeEntry( "/qtcmd/"+scheme+"/CharacterDevColor", colorToString(mSchemesList[id].characterDevColor) );
		settings.writeEntry( "/qtcmd/"+scheme+"/BlockDevColor", colorToString(mSchemesList[id].blockDevColor) );
		settings.writeEntry( "/qtcmd/"+scheme+"/FifoColor", colorToString(mSchemesList[id].fifoColor) );
	}

	settings.writeEntry( "/qtcmd/FilesPanel/FileColorsSchemeList", schemesNameList );
	settings.writeEntry( "/qtcmd/FilesPanel/FileColorsScheme", mSchemeComboBox->currentText() );
}


void FileColorsPage::slotNew()
{
	bool ok = FALSE;
	QString newSchemeName = QInputDialog::getText(
		tr("New scheme name")+" - QtCommander",
		tr( "Please to give a name:" ),
		QLineEdit::Normal, tr("newScheme"), &ok, this
	);
	if ( ! ok || newSchemeName.isEmpty() )
		return;

	newSchemeName.replace( ' ', '_' ); // ' ' zmienic na wyr.reg. - ciag spacji lub jedna

	FileColorsScheme fileColorsScheme;
	// init by default scheme
	fileColorsScheme.commonFileColor = mSchemesList[0].commonFileColor;
	fileColorsScheme.exeFileColor = mSchemesList[0].exeFileColor;
	fileColorsScheme.symlinkColor = mSchemesList[0].symlinkColor;
	fileColorsScheme.brokenSymlinkColor = mSchemesList[0].brokenSymlinkColor;
	fileColorsScheme.socketColor = mSchemesList[0].socketColor;
	fileColorsScheme.characterDevColor = mSchemesList[0].characterDevColor;
	fileColorsScheme.blockDevColor = mSchemesList[0].blockDevColor;
	fileColorsScheme.fifoColor = mSchemesList[0].fifoColor;

	mSchemesList.append( fileColorsScheme );

	mSchemeComboBox->insertItem( newSchemeName );
	mSchemeComboBox->setCurrentText( newSchemeName );
	slotSetNewScheme( mSchemeComboBox->currentItem() );
}


void FileColorsPage::slotDelete()
{
	mSchemeComboBox->removeItem( mSchemeComboBox->currentItem() );
	mSchemesList.remove( mSchemesList.at(mSchemeComboBox->currentItem()) );
}

