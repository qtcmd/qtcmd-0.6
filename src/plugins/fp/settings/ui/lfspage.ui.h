/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qdir.h>
#include <qsettings.h>

#include "locationchooser.h"


void LfsPage::init()
{
	QSettings settings;

	bool weighBeforeOperation = settings.readBoolEntry( "/qtcmd/LFS/AlwaysWeighBeforeOperation", TRUE );
	bool removeToTrash = settings.readBoolEntry( "/qtcmd/LFS/AlwaysRemoveToTrash", FALSE );
	QString trashDir = settings.readEntry( "/qtcmd/LFS/TrashDirectory", QDir::homeDirPath()+"/tmp/qtcmd/trash" );

	mWeighBeforeOperationChkBox->setChecked( weighBeforeOperation );
	mRemoveToTrash->setChecked( removeToTrash );
	mLocationChooser->setText( trashDir );
}

QString LfsPage::trashPath()
{
	return mLocationChooser->text();
}

void LfsPage::slotSetDefaults()
{
	mWeighBeforeOperationChkBox->setChecked( TRUE );
	mRemoveToTrash->setChecked( FALSE );
	mLocationChooser->setText( QDir::homeDirPath()+"/tmp/qtcmd/trash" );
}


void LfsPage::slotSave()
{
	QSettings settings;

	settings.writeEntry( "/qtcmd/LFS/AlwaysRemoveToTrash", mRemoveToTrash->isChecked() );
	settings.writeEntry( "/qtcmd/LFS/AlwaysWeighBeforeOperation", mWeighBeforeOperationChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/LFS/TrashDirectory", mLocationChooser->text() );
}
