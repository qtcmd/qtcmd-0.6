/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qsettings.h>

void OtherFSPage::init()
{
	QSettings settings;

	bool alwaysOverwrite = settings.readBoolEntry( "/qtcmd/FilesSystem/AlwaysOverwrite", FALSE );
	bool savePermission = settings.readBoolEntry( "/qtcmd/FilesSystem/AlwaysSavePermission", TRUE );
	bool askBeforeDelete = settings.readBoolEntry( "/qtcmd/FilesSystem/AskBeforeDelete", TRUE );
	bool getInToDir = settings.readBoolEntry( "/qtcmd/FilesSystem/AlwaysGetInToDir", FALSE );
	bool closeProgressDlgAfterFinished = settings.readBoolEntry( "/qtcmd/FilesSystem/CloseProgressDlgAfterFinished", FALSE );

	mAlwaysOverwriteChkBox->setChecked( alwaysOverwrite );
	mAskBeforeDeleteChkBox->setChecked( askBeforeDelete );
	mAlwaysGetInToDirChkBox->setChecked( getInToDir );
	mAlwaysSavePermissionChkBox->setChecked( savePermission );
	mCloseProgressDlgAfterFinished->setChecked( closeProgressDlgAfterFinished );
}


void OtherFSPage::slotSetDefaults()
{
	mAlwaysOverwriteChkBox->setChecked( FALSE );
	mAlwaysSavePermissionChkBox->setChecked( TRUE );
	mAskBeforeDeleteChkBox->setChecked( TRUE );
	mAlwaysGetInToDirChkBox->setChecked( FALSE );
	mCloseProgressDlgAfterFinished->setChecked( FALSE );
}


void OtherFSPage::slotSave()
{
	QSettings settings;

	settings.writeEntry( "/qtcmd/FilesSystem/AlwaysOverwrite", mAlwaysOverwriteChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesSystem/AlwaysSavePermission", mAlwaysSavePermissionChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesSystem/AskBeforeDelete", mAskBeforeDeleteChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesSystem/AlwaysGetInToDir", mAlwaysGetInToDirChkBox->isChecked() );
	settings.writeEntry( "/qtcmd/FilesSystem/CloseProgressDlgAfterFinished", mCloseProgressDlgAfterFinished->isChecked() );
}
