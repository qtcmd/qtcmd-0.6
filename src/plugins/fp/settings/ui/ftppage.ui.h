/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qsettings.h>

void FtpPage::init()
{
	QSettings settings;

	bool standByConnectChkBtn = settings.readBoolEntry( "/qtcmd/FTP/StandByConnect", TRUE );
	int numOfTimeToRetryIfBusy = settings.readBoolEntry( "/qtcmd/FTP/NumOfTimeToRetryIfBusy", 10 );

	mStandByConnectChkBox->setChecked( standByConnectChkBtn );
	mNumOfTimeToRetryIfFTPbusySpinBox->setValue( numOfTimeToRetryIfBusy );
}

int FtpPage::numOfTimeToRetryIfBusy()
{
	return mNumOfTimeToRetryIfFTPbusySpinBox->value();
}

void FtpPage::slotSetDefaults()
{
	mStandByConnectChkBox->setChecked( TRUE );
	mNumOfTimeToRetryIfFTPbusySpinBox->setValue( 10 );
}

void FtpPage::slotSave()
{
	QSettings settings;

	settings.writeEntry( "/qtcmd/FTP/StandByConnect", mStandByConnectChkBox->isChecked() );
	settings.readBoolEntry( "/qtcmd/FTP/NumOfTimeToRetryIfBusy", mNumOfTimeToRetryIfFTPbusySpinBox->value() );
}
