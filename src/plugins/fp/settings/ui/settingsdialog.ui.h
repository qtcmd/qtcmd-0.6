/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include "settingsdialog.h"

#include <qcheckbox.h>
#include <qradiobutton.h>


void SettingsDialog::init()
{
	setCaption( tr("QtCommander - settings") );

	// --- List pages
	mFontAppPage = new FontAppPage( mWidgetStack );
	mColorsPage = new ColorsPage( mWidgetStack );
	mFileColorsPage = new FileColorsPage( mWidgetStack );
	mColumnsPage = new ColumnsPage( mWidgetStack );
	mOtherListSettingsPage = new OtherListSettingsPage( mWidgetStack );

	mWidgetStack->removeWidget( mWidgetStack->widget(0) );
	mWidgetStack->addWidget( mFontAppPage, 0 );
	mWidgetStack->addWidget( mColorsPage, 1 );
	mWidgetStack->addWidget( mFileColorsPage, 2 );
	mWidgetStack->addWidget( mColumnsPage, 3 );
	mWidgetStack->addWidget( mOtherListSettingsPage, 4 );

	// --- FileSystem pages
	mLfsPage = new LfsPage( mWidgetStack );
	mFtpPage = new FtpPage( mWidgetStack );
	mArchivesPage = new ArchivesPage( mWidgetStack );
	mOtherFSPage = new OtherFSPage( mWidgetStack );

	mWidgetStack->addWidget( mLfsPage, 5 );
	mWidgetStack->addWidget( mFtpPage, 6 );
	mWidgetStack->addWidget( mArchivesPage, 7 );
	mWidgetStack->addWidget( mOtherFSPage, 8 );

	// --- Other pages
	mKeyshortcutsAppPage = new KeyShortcutsAppPage( mWidgetStack );
	mToolBarAppPage = new ToolBarAppPage( mWidgetStack );

	mWidgetStack->addWidget( mKeyshortcutsAppPage, 9 );
	mWidgetStack->addWidget( mToolBarAppPage, 10 );


	// --- init view onto dialog
	mToolBox->setCurrentIndex( 0 );
	mListListBox->setCurrentItem( 0 );
	resize( QSize(635, 515).expandedTo(minimumSizeHint()) );

	static QValueList <int>sSW; // width the children of spliter obj.
	sSW << (width()*30)/100 << 0;
	sSW[1] = width()-sSW[0];
	mSplitter->setSizes( sSW );
}


void SettingsDialog::slotSetKeyShortcuts( KeyShortcuts *keyShortcuts )
{
	if ( mKeyshortcutsAppPage )
		mKeyshortcutsAppPage->setKeyShortcuts( keyShortcuts );
}


void SettingsDialog::slotSetFilesAssociation( FilesAssociation * fa )
{
	if ( mOtherListSettingsPage )
		mOtherListSettingsPage->updateFilters( fa );
}


void SettingsDialog::slotOkButtonPressed()
{
	slotApplyButtonPressed();
	slotSaveButtonPressed();
	close();
}


void SettingsDialog::slotApplyButtonPressed()
{
	enum Colors {
		ItemColor=0, ItemColorUnderCursor,
		SelectedItemColor, SelectedItemColorUnderCursor,
		BgColorOfSelectedItems, BgColorOfSelectedItemsUnderCursor,
		FirstBackgroundColor, SecondBackgroundColor,
		FullCursorColor, InsideFrameCursorColor, OutsideFrameCursorColor,
		GridColor
	};
	enum FileColors {
		CommonFileColor=0, ExeFileColor, SymlinkColor, BrokenSymlinkColor,
		SocketColor, CharacterDevColor, BlockDevColor, FifoColor
	};
	enum ColumnNames {
		NAMEcol=0, SIZEcol, TIMEcol, PERMcol, OWNERcol, GROUPcol
	};

	// --- inits the list settings
	ListViewSettings listViewSettings;
	// - font
	listViewSettings.fontFamily = mFontAppPage->fontFamily();
	listViewSettings.fontSize   = mFontAppPage->fontSize();
	listViewSettings.fontBold   = mFontAppPage->fontBold();
	listViewSettings.fontItalic = mFontAppPage->fontItalic();
	// - colors
	listViewSettings.showSecondBackgroundColor = mColorsPage->mSecBgColChkBox->isChecked();
	listViewSettings.showGrid = mColorsPage->mShowGridChkBox->isChecked();
	listViewSettings.twoColorsCursor = mColorsPage->mTwoColorsCursorRadioBtn->isChecked();
	listViewSettings.itemColor = mColorsPage->getColor(ItemColor);
	listViewSettings.itemColorUnderCursor  = mColorsPage->getColor(ItemColorUnderCursor);
	listViewSettings.selectedItemColor = mColorsPage->getColor(SelectedItemColor);
	listViewSettings.selectedItemColorUnderCursor = mColorsPage->getColor(SelectedItemColorUnderCursor);
	listViewSettings.bgColorOfSelectedItems = mColorsPage->getColor(BgColorOfSelectedItems);
	listViewSettings.bgColorOfSelectedItemsUnderCursor = mColorsPage->getColor(BgColorOfSelectedItemsUnderCursor);
	listViewSettings.firstBackgroundColor = mColorsPage->getColor(FirstBackgroundColor);
	listViewSettings.secondBackgroundColor = mColorsPage->getColor(SecondBackgroundColor);
	listViewSettings.fullCursorColor = mColorsPage->getColor(FullCursorColor);
	listViewSettings.insideFrameCursorColor = mColorsPage->getColor(InsideFrameCursorColor);
	listViewSettings.outsideFrameCursorColor = mColorsPage->getColor(OutsideFrameCursorColor);
	listViewSettings.gridColor = mColorsPage->getColor(GridColor);
	// - file colors
	listViewSettings.commonFileColor = mFileColorsPage->getColor(CommonFileColor);
	listViewSettings.exeFileColor = mFileColorsPage->getColor(ExeFileColor);
	listViewSettings.symlinkColor = mFileColorsPage->getColor(SymlinkColor);
	listViewSettings.brokenSymlinkColor = mFileColorsPage->getColor(BrokenSymlinkColor);
	listViewSettings.socketColor  = mFileColorsPage->getColor(SocketColor);
	listViewSettings.characterDevColor = mFileColorsPage->getColor(CharacterDevColor);
	listViewSettings.blockDevColor = mFileColorsPage->getColor(BlockDevColor);
	listViewSettings.fifoColor = mFileColorsPage->getColor(FifoColor);

	// - columns
	listViewSettings.columnsWidthTab[NAMEcol]  = mColumnsPage->columnWidth( NAMEcol );
	listViewSettings.columnsWidthTab[SIZEcol]  = mColumnsPage->columnWidth( SIZEcol );
	listViewSettings.columnsWidthTab[TIMEcol]  = mColumnsPage->columnWidth( TIMEcol );
	listViewSettings.columnsWidthTab[PERMcol]  = mColumnsPage->columnWidth( PERMcol );
	listViewSettings.columnsWidthTab[OWNERcol] = mColumnsPage->columnWidth( OWNERcol );
	listViewSettings.columnsWidthTab[GROUPcol] = mColumnsPage->columnWidth( GROUPcol );
	listViewSettings.columnsSynchronize = mColumnsPage->mSynchronizeColumnChkBox->isChecked();
	// - other
	listViewSettings.showIcons = mOtherListSettingsPage->mShowIconsChkBox->isChecked();
	listViewSettings.showHiddenFiles = mOtherListSettingsPage->mShowHiddenChkBox->isChecked();
	listViewSettings.cursorAlwaysVisible = mOtherListSettingsPage->mSelectFilesOnlyChkBox->isChecked();
	listViewSettings.selectFilesOnly = mOtherListSettingsPage->mCursorAlwaysVisibleChkBox->isChecked();
	listViewSettings.filterForFiltered = mOtherListSettingsPage->mFilterForFilteredChkBox->isChecked();
	listViewSettings.fileSizeFormat = mOtherListSettingsPage->fileSizeFormat();
	listViewSettings.defaultFilter = mOtherListSettingsPage->defaultFilter();

	emit signalApply( listViewSettings );

	// --- inits the file system settings
	FileSystemSettings fileSystemSettings;
	// archives page
	fileSystemSettings.workDirectoryPath = mArchivesPage->workDirPath();
	// lfs page
	fileSystemSettings.removeToTrash = mLfsPage->mRemoveToTrash->isChecked();
	fileSystemSettings.weighBeforeOperation = mLfsPage->mWeighBeforeOperationChkBox->isChecked();
	fileSystemSettings.trashPath = mLfsPage->trashPath();
	// ftp page
	fileSystemSettings.standByConnection = mFtpPage->mStandByConnectChkBox->isChecked();
	fileSystemSettings.numOfTimeToRetryIfFtpBusy = mFtpPage->numOfTimeToRetryIfBusy();
	// other page
	fileSystemSettings.alwaysOverwrite = mOtherFSPage->mAlwaysOverwriteChkBox->isChecked();
	fileSystemSettings.alwaysSavePermission = mOtherFSPage->mAlwaysSavePermissionChkBox->isChecked();
	fileSystemSettings.alwaysAskBeforeDeleting = mOtherFSPage->mAskBeforeDeleteChkBox->isChecked();
	fileSystemSettings.alwaysGetInToDirectory = mOtherFSPage->mAlwaysGetInToDirChkBox->isChecked();
	fileSystemSettings.closeProgressDlgAfterFinished = mOtherFSPage->mCloseProgressDlgAfterFinished->isChecked();

	emit signalApply( fileSystemSettings );

	// --- inits the other apps. settings
// 	OtherAppSettings otherAppSettings;

	emit signalApplyKeyShortcuts( mKeyshortcutsAppPage->keyShortcuts() );
}


void SettingsDialog::slotSaveButtonPressed()
{
	// --- List pages
	mColorsPage->slotSave();
	mFontAppPage->slotSave();
	mColumnsPage->slotSave();
	mFileColorsPage->slotSave();
	mKeyshortcutsAppPage->slotSave();
	mOtherListSettingsPage->slotSave();

	// --- FileSystem pages
	mLfsPage->slotSave();
	mFtpPage->slotSave();
	mArchivesPage->slotSave();
}


void SettingsDialog::slotRaiseWidget( int id )
{
	int offsetId;
	QString title;
	QString senderName = sender()->name();

	if ( senderName == "mListListBox" ) {
		title = mListListBox->text( id );
		offsetId = 0;
	}
	else
	if ( senderName == "mFileSystemsListBox" ) {
		title = mFileSystemsListBox->text( id );
		offsetId = 5; // pages into previous pages
	}
	else
	if ( senderName == "mOtherPageListBox" ) {
		title = mOtherPageListBox->text( id );
		offsetId = 9; // pages into previous pages
	}

	mWidgetStack->raiseWidget( offsetId+id );
	mTitleLab->setText( title );
}


void SettingsDialog::slotCurrentItemChanged( int itemId )
{
	int offsetId, id;
	QString title;

	if ( itemId == 0 ) { // "List pages"
		id = mListListBox->currentItem();
		if ( id < 0 )
			mListListBox->setCurrentItem( id=0 );
		title = mListListBox->text( id );
		offsetId = 0;
	}
	else
	if ( itemId == 1 ) { // "FileSystems pages"
		id = mFileSystemsListBox->currentItem();
		if ( id < 0 )
			mFileSystemsListBox->setCurrentItem( id=0 );
		title = mFileSystemsListBox->text( id );
		offsetId = 5; // pages into previous pages
	}
	else
	if ( itemId == 2 ) { // "Other pages"
		id = mOtherPageListBox->currentItem();
		if ( id < 0 )
			mOtherPageListBox->setCurrentItem( id=0 );
		title = mOtherPageListBox->text( id );
		offsetId = 9; // pages into previous pages
	}

	mWidgetStack->raiseWidget( offsetId+id );
	mTitleLab->setText( title );
}
