/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qsettings.h>


void ColumnsPage::init()
{
	QSettings settings;
	const QStringList defaultColumnList = QStringList::split( ',', "0-170,1-70,2-107,3-70,4-55,5-55" );

	// --- read info from config file
	bool columnsSynchronization = settings.readBoolEntry( "/qtcmd/FilesPanel/SynchronizeColumns", TRUE );
	QStringList columnsList = settings.readListEntry( "/qtcmd/FilesPanel/Columns" );
	if ( columnsList.isEmpty() )
		columnsList = defaultColumnList;

	int columnWidth;
	QCheckBox *colVisible;
	for (int i=0; i<6; i++) { // 6 - num of all columns
		colVisible = (QCheckBox *)mShowColumnBtnGroup->find(i);
		if ( columnsList[i].section('-', 0, 0 ) == QString("%1").arg(i) ) { // current column is visible
			columnWidth = (columnsList[i].section('-', -1 )).toInt();
			if ( columnWidth > 0 && columnWidth < 10 )
				columnWidth = (defaultColumnList[i].section('-', -1 )).toInt();
		}
		else
			columnWidth = 0;

		mColumnsWidthTab[i] = columnWidth;
		colVisible->setChecked( (columnWidth > 0) );
	}

	mSynchronizeColumnChkBox->setChecked( columnsSynchronization );
}

int ColumnsPage::columnWidth( int id )
{
	if ( id < 0 || id > 6 )
		return 0;

	int width = (((QCheckBox *)mShowColumnBtnGroup->find(id))->isChecked()) ? mColumnsWidthTab[id] : 0;

	return width;
}

void ColumnsPage::slotDefaultSettings()
{
// "0-170,1-70,2-107,3-70,4-55,5-55"
	mNameChkBox->setChecked( TRUE );
	mSizeChkBox->setChecked( TRUE );
	mTimeChkBox->setChecked( TRUE );
	mPermChkBox->setChecked( TRUE );
	mOwnerChkBox->setChecked( TRUE );
	mGroupChkBox->setChecked( TRUE );

	mSynchronizeColumnChkBox->setChecked( TRUE );
//	mSortColBtnGroup->find( 0 )->setChecked( TRUE );
}

void ColumnsPage::slotSave()
{
	QSettings settings;

	settings.writeEntry( "/qtcmd/FilesPanel/SynchronizeColumns", mSynchronizeColumnChkBox->isChecked() );

	int columnWidth;
	QCheckBox *colVisible;
	QStringList columnsList;
	for ( int i=0; i<6; i++ ) { // 6 - number of all columns
		colVisible = (QCheckBox *)mShowColumnBtnGroup->find(i);
		columnWidth = (colVisible->isChecked() ? mColumnsWidthTab[i] : 0);
		columnsList.append( QString("%1-%2").arg(i).arg(columnWidth) );
	}

	settings.writeEntry( "/qtcmd/FilesPanel/Columns", columnsList );
}
