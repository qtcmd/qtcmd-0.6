/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include "functions_col.h" // for color() and colorToString()
#include "buttonColor.xpm"

#include <qinputdialog.h>
#include <qcolordialog.h>
#include <qsettings.h>
#include <qimage.h>

#include <qpainter.h>
#include <qpixmap.h>

void ColorsPage::init()
{
	QSettings settings;

	QStringList schemesNameList = settings.readListEntry( "/qtcmd/FilesPanel/ColorsSchemeList" );

	if ( ! schemesNameList.count() )
		schemesNameList.append( "DefaultColors" );
	if ( schemesNameList.findIndex( "DefaultColors" ) == -1 )
		schemesNameList.insert( 0, "DefaultColors" );

	QString scheme = settings.readEntry( "/qtcmd/FilesPanel/ColorsScheme", "DefaultColors" );
	mSchemesList.clear();

	ColorsScheme colorsScheme;
	QString colorStr;

	for ( uint id=0; id<schemesNameList.count(); id++ ) {
		scheme = schemesNameList[id];

		colorsScheme.showSecondBackgroundColor = settings.readBoolEntry( "/qtcmd/"+scheme+"/ShowSecondBgColor", TRUE );
		colorsScheme.twoColorsCursor = settings.readBoolEntry( "/qtcmd/"+scheme+"/TwoColorsCursor", TRUE );
		colorsScheme.showGrid = settings.readBoolEntry( "/qtcmd/"+scheme+"/ShowGrid", FALSE );

		colorStr = settings.readEntry( "/qtcmd/"+scheme+"/ItemColor", "000000" );
		colorsScheme.itemColor = color(colorStr, "000000");

		colorStr = settings.readEntry( "/qtcmd/"+scheme+"/ItemColorUnderCursor", "0000EA" );
		colorsScheme.itemColorUnderCursor = color(colorStr, "0000EA");

		colorStr = settings.readEntry( "/qtcmd/"+scheme+"/SelectedItemColor", "FF0000" );
		colorsScheme.selectedItemColor = color(colorStr, "FF0000");

		colorStr = settings.readEntry( "/qtcmd/"+scheme+"/SelectedItemColorUnderCursor", "57BA00" );
		colorsScheme.selectedItemColorUnderCursor = color(colorStr, "57BA00");

		colorStr = settings.readEntry( "/qtcmd/"+scheme+"/BgColorOfSelectedItems", "FFFFFF" );
		colorsScheme.bgColorOfSelectedItems = color(colorStr, "FFFFFF");

		colorStr = settings.readEntry( "/qtcmd/"+scheme+"/BgColorOfSelectedItemsUnderCursor", "FFFFFF" );
		colorsScheme.bgColorOfSelectedItemsUnderCursor = color(colorStr, "FFFFFF");

		colorStr = settings.readEntry( "/qtcmd/"+scheme+"/FirstBackgroundColor", "FFFFFF" );
		colorsScheme.firstBackgroundColor = color(colorStr, "FFFFFF");

		colorStr = settings.readEntry( "/qtcmd/"+scheme+"/SecondBackgroundColor", "FFEBDC" );
		colorsScheme.secondBackgroundColor = color(colorStr, "FFEBD5");

		colorStr = settings.readEntry( "/qtcmd/"+scheme+"/FullCursorColor", "00008B" );
		colorsScheme.fullCursorColor = color(colorStr, "00008B");

		colorStr = settings.readEntry( "/qtcmd/"+scheme+"/InsideFrameCursorColor", "000080" );
		colorsScheme.insideFrameCursorColor = color(colorStr, "000080");

		colorStr = settings.readEntry( "/qtcmd/"+scheme+"/OutsideFrameCursorColor", "FF0000" );
		colorsScheme.outsideFrameCursorColor = color(colorStr, "FF0000");

		colorStr = settings.readEntry( "/qtcmd/"+scheme+"/GridColor", "A9A9A9" );
		colorsScheme.gridColor = color(colorStr, "A9A9A9");

		mSchemesList.append( colorsScheme );
	}

	mSchemeComboBox->insertStringList( schemesNameList );
	mSchemeComboBox->setCurrentText( scheme ); // co jesli bedzie zla wart., spr. to !
	slotSetNewScheme( mSchemeComboBox->currentItem() );
}

void ColorsPage::drawPreview()
{
	bool fullCursor  = mFullCursorRadioBtn->isChecked();
	bool secondBgCol = mSecBgColChkBox->isChecked();
	bool showGrid    = mShowGridChkBox->isChecked();

	int px = 5, py=0;
	const int fontH = 12;
	const int topMargin = 10;
	const int cursH = fontH + 3;
	const int pw = mPreviewLabel->width()-2, ph = mPreviewLabel->height()-2;
	const int maxLine = (ph-topMargin)/fontH; // == 7

	enum Colors {
		ItemColor=0, ItemColorUnderCursor,
		SelectedItemColor, SelectedItemColorUnderCursor,
		BgColorOfSelectedItems, BgColorOfSelectedItemsUnderCursor,
		FirstBackgroundColor, SecondBackgroundColor,
		FullCursorColor, InsideFrameCursorColor, OutsideFrameCursorColor,
		GridColor
	};


	QPixmap screen;
	screen.resize( pw, ph );
	screen.fill( getColor(FirstBackgroundColor) );

	QPainter *p = new QPainter;
	p->begin( &screen );

	p->fillRect( 0,0, pw,topMargin, palette().color(QPalette::Inactive, QColorGroup::Button) );

	if ( secondBgCol ) { // draws two-colors background
		QColor color;
		for ( int i=0; i<maxLine; i++) {
			color = i%2 ? getColor(SecondBackgroundColor) : getColor(FirstBackgroundColor);
			p->fillRect( 0, topMargin+(i*cursH), pw, cursH, color );
		}
	}
	if ( showGrid ) { // draws a grid
		p->setPen( getColor(GridColor) );
		for ( int i=0; i<maxLine; i++)
			p->drawLine( 0, topMargin+(i*cursH), pw, topMargin+(i*cursH) );
	}
	// draw the first line
	py += fontH + topMargin;
	p->setPen( getColor(ItemColor) );
	p->drawText( px, py, tr("Text") );


	// draws the second line
	if ( fullCursor )
		p->fillRect( 0, py+2, pw, cursH+2, getColor(FullCursorColor) );
	else
	{ // draws two-colors frame
		p->setPen( getColor(OutsideFrameCursorColor) );
		p->drawRect( 0, py+2, pw, cursH+2 );
		p->setPen( getColor(InsideFrameCursorColor) );
		p->drawRect( 1, py+1+2, pw-2, cursH );
	}
	py += cursH;
	p->setPen( getColor(ItemColorUnderCursor) );
	p->drawText( px, py, tr("TextUnderCursor") );

	// draws the third line
	p->fillRect( 0, py+4, pw, cursH, getColor(BgColorOfSelectedItems) );
	py += cursH;
	p->setPen( getColor(SelectedItemColor) );
	p->drawText( px, py, tr("Selection") );


	// draws the fourth line
	p->fillRect( 0, py+2, pw, cursH+2, getColor(BgColorOfSelectedItemsUnderCursor) );
	if ( fullCursor )
		p->fillRect( 0, py+2, pw, cursH+2, getColor(FullCursorColor) );
	else
	{ // draws two-colors frame
		p->setPen( getColor(OutsideFrameCursorColor) );
		p->drawRect( 0, py+2, pw, cursH+2 );
		p->setPen( getColor(InsideFrameCursorColor) );
		p->drawRect( 1, py+1+2, pw-2, cursH );
	}
	py += cursH;
	p->setPen( getColor(SelectedItemColorUnderCursor) );
	p->drawText( px, py, tr("SelUnderCursor") );

	p->end();

	mPreviewLabel->setPixmap( screen );
}


QColor ColorsPage::getColor( int colorId )
{
	QToolButton *btn = (colorId < 11) ? (QToolButton *)mColorsBtnGroup->find( colorId ) : mGridColBtn;
	QPixmap btnPixmap = btn->iconSet().pixmap();
	if ( btnPixmap.isNull() )
		return QColor(0,0,0);

	return btnPixmap.convertToImage().pixel(1,1);
}

void ColorsPage::slotShowSecondBgColor( bool show )
{
	int currentItem = mSchemeComboBox->currentItem();
	if ( currentItem )
		mSchemesList[currentItem].showSecondBackgroundColor = show;

	mSecondBgColBtn->setEnabled( show );
	drawPreview();
}

void ColorsPage::slotShowGrid( bool show )
{
	int currentItem = mSchemeComboBox->currentItem();
	if ( currentItem )
		mSchemesList[currentItem].showGrid = show;

	mGridColBtn->setEnabled( show );
	drawPreview();
}

void ColorsPage::slotShowFullCursor( bool show )
{
	int currentItem = mSchemeComboBox->currentItem();
	if ( currentItem )
		mSchemesList[currentItem].twoColorsCursor = ! show;

	mFullCursorColBtn->setEnabled( show );
	mOutFrameCursColBtn->setEnabled( ! show );
	mInsideFrameCursColBtn->setEnabled( ! show );

	drawPreview();
}


void ColorsPage::slotNew()
{
	bool ok = FALSE;
	QString newSchemeName = QInputDialog::getText(
		tr("New scheme name")+" - QtCommander",
		tr( "Please to give a name:" ),
		QLineEdit::Normal, tr("newScheme"), &ok, this
	);
	if ( ! ok || newSchemeName.isEmpty() )
		return;

	newSchemeName.replace( ' ', '_' ); // ' ' zmienic na wyr.reg. - ciag spacji lub jedna


	ColorsScheme colorsScheme;
	// init by default scheme
	colorsScheme.showSecondBackgroundColor = mSchemesList[0].showSecondBackgroundColor;
	colorsScheme.twoColorsCursor = mSchemesList[0].twoColorsCursor;
	colorsScheme.showGrid = mSchemesList[0].showGrid;
	colorsScheme.itemColor = mSchemesList[0].itemColor;
	colorsScheme.itemColorUnderCursor = mSchemesList[0].itemColorUnderCursor;
	colorsScheme.selectedItemColor = mSchemesList[0].selectedItemColor;
	colorsScheme.selectedItemColorUnderCursor = mSchemesList[0].selectedItemColorUnderCursor;
	colorsScheme.bgColorOfSelectedItems = mSchemesList[0].bgColorOfSelectedItems;
	colorsScheme.bgColorOfSelectedItemsUnderCursor = mSchemesList[0].bgColorOfSelectedItemsUnderCursor;
	colorsScheme.firstBackgroundColor = mSchemesList[0].firstBackgroundColor;
	colorsScheme.secondBackgroundColor = mSchemesList[0].secondBackgroundColor;
	colorsScheme.fullCursorColor = mSchemesList[0].fullCursorColor;
	colorsScheme.insideFrameCursorColor = mSchemesList[0].insideFrameCursorColor;
	colorsScheme.outsideFrameCursorColor = mSchemesList[0].outsideFrameCursorColor;
	colorsScheme.gridColor = mSchemesList[0].gridColor;

	mSchemesList.append( colorsScheme );

	mSchemeComboBox->insertItem( newSchemeName );
	mSchemeComboBox->setCurrentText( newSchemeName );
	slotSetNewScheme( mSchemeComboBox->currentItem() );
}


void ColorsPage::slotDelete()
{
	mSchemeComboBox->removeItem( mSchemeComboBox->currentItem() );
	mSchemesList.remove( mSchemesList.at(mSchemeComboBox->currentItem()) );
}


void ColorsPage::slotSave()
{
	bool twoColorsCursor, showSecondBackgroundColor, showGrid;
	QStringList schemesNameList;
	QSettings settings;
	QString scheme;

	schemesNameList.append( "DefaultColors" );

	for ( uint id=1; id<mSchemesList.count(); id++ ) { // start from 1, becouse need to skip default scheme
		showSecondBackgroundColor = mSchemesList[id].showSecondBackgroundColor;
		twoColorsCursor = mSchemesList[id].twoColorsCursor;
		showGrid = mSchemesList[id].showGrid;
		scheme = mSchemeComboBox->text(id);
		schemesNameList.append( scheme );

		settings.writeEntry( "/qtcmd/"+scheme+"/ShowSecondBgColor", showSecondBackgroundColor );
		settings.writeEntry( "/qtcmd/"+scheme+"/TwoColorsCursor", twoColorsCursor );
		settings.writeEntry( "/qtcmd/"+scheme+"/ShowGrid", showGrid );

		settings.writeEntry( "/qtcmd/"+scheme+"/ItemColor", colorToString(mSchemesList[id].itemColor) );
		settings.writeEntry( "/qtcmd/"+scheme+"/ItemColorUnderCursor", colorToString(mSchemesList[id].itemColorUnderCursor) );
		settings.writeEntry( "/qtcmd/"+scheme+"/SelectedItemColor", colorToString(mSchemesList[id].selectedItemColor) );
		settings.writeEntry( "/qtcmd/"+scheme+"/SelectedItemColorUnderCursor", colorToString(mSchemesList[id].selectedItemColorUnderCursor) );
		settings.writeEntry( "/qtcmd/"+scheme+"/BgColorOfSelectedItems", colorToString(mSchemesList[id].bgColorOfSelectedItems) );
		settings.writeEntry( "/qtcmd/"+scheme+"/BgColorOfSelectedItemsUnderCursor", colorToString(mSchemesList[id].bgColorOfSelectedItemsUnderCursor) );
		settings.writeEntry( "/qtcmd/"+scheme+"/FirstBackgroundColor", colorToString(mSchemesList[id].firstBackgroundColor) );
		if ( showSecondBackgroundColor )
			settings.writeEntry( "/qtcmd/"+scheme+"/SecondBackgroundColor", colorToString(mSchemesList[id].secondBackgroundColor) );
		if ( twoColorsCursor ) {
			settings.writeEntry( "/qtcmd/"+scheme+"/InsideFrameCursorColor", colorToString(mSchemesList[id].insideFrameCursorColor) );
			settings.writeEntry( "/qtcmd/"+scheme+"/OutsideFrameCursorColor", colorToString(mSchemesList[id].outsideFrameCursorColor) );
		}
		else
			settings.writeEntry( "/qtcmd/"+scheme+"/FullCursorColor", colorToString(mSchemesList[id].fullCursorColor) );
		if ( showGrid )
			settings.writeEntry( "/qtcmd/"+scheme+"/GridColor", colorToString(mSchemesList[id].gridColor) );
	}

	settings.writeEntry( "/qtcmd/FilesPanel/ColorsSchemeList", schemesNameList );
	settings.writeEntry( "/qtcmd/FilesPanel/ColorsScheme", mSchemeComboBox->currentText() );
}


void ColorsPage::slotSetNewScheme( int id )
{
	QPixmap btnPixmap( (const char**)buttonColor );
	mSchemeComboBox->setCurrentItem( id );

	// --- sets colors on the buttons
	btnPixmap.fill( mSchemesList[id].itemColor );
	mItemsColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[id].itemColorUnderCursor );
	mItemsColUnderCursorBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[id].selectedItemColor );
	mSelectItemsBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[id].selectedItemColorUnderCursor );
	mSelectItemsColUnderCursorBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[id].bgColorOfSelectedItems );
	mBgColOfSelectedItemsBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[id].bgColorOfSelectedItemsUnderCursor );
	mBgColorOfSelectedItemsUnderCursorColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[id].firstBackgroundColor );
	mFirstBgColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[id].secondBackgroundColor );
	mSecondBgColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[id].fullCursorColor );
	mFullCursorColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[id].insideFrameCursorColor );
	mInsideFrameCursColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[id].outsideFrameCursorColor );
	mOutFrameCursColBtn->setIconSet( btnPixmap );

	btnPixmap.fill( mSchemesList[id].gridColor );
	mGridColBtn->setIconSet( btnPixmap );

	// --- sets checkBoxes and radioButtons
	mSecBgColChkBox->setChecked( mSchemesList[id].showSecondBackgroundColor );
	mShowGridChkBox->setChecked( mSchemesList[id].showGrid );
	bool twoColorsCursor = mSchemesList[id].twoColorsCursor;
	if ( twoColorsCursor )
		mTwoColorsCursorRadioBtn->setChecked( twoColorsCursor );
	else
		mFullCursorRadioBtn->setChecked( ! twoColorsCursor );

	mSecondBgColBtn->setEnabled( mSchemesList[id].showSecondBackgroundColor );
	mInsideFrameCursColBtn->setEnabled( mSchemesList[id].twoColorsCursor );
	mOutFrameCursColBtn->setEnabled( mSchemesList[id].twoColorsCursor );
	mFullCursorColBtn->setEnabled( ! mSchemesList[id].twoColorsCursor );
	mGridColBtn->setEnabled( mSchemesList[id].showGrid );

	// disable delete and save buttons only for default scheme
	mDeleteBtn->setEnabled( id );

	drawPreview();
}


void ColorsPage::slotSetColor( int colorId )
{
	// --- gets color from the button
	QToolButton *btn = (colorId < 11) ? (QToolButton *)mColorsBtnGroup->find( colorId ) : mGridColBtn;
	QPixmap btnPixmap = btn->iconSet().pixmap();
	QColor initColor = btnPixmap.convertToImage().pixel(1,1);
	QColor newColor  = QColorDialog::getColor( initColor );

	if ( ! newColor.isValid() )
		return;
	// --- sets color on the button
	btnPixmap.fill( newColor );
	btn->setIconSet( btnPixmap );


	enum Colors {
		ItemColor=0, ItemColorUnderCursor,
		SelectedItemColor, SelectedItemColorUnderCursor,
		BgColorOfSelectedItems, BgColorOfSelectedItemsUnderCursor,
		FirstBackgroundColor, SecondBackgroundColor,
		FullCursorColor, InsideFrameCursorColor, OutsideFrameCursorColor,
		GridColor
	};
	int schemeId = mSchemeComboBox->currentItem();

	if ( schemeId ) {
		// --- sets colors in scheme
		if ( colorId == ItemColor )
			mSchemesList[schemeId].itemColor = newColor;
		else
		if ( colorId == ItemColorUnderCursor )
			mSchemesList[schemeId].itemColorUnderCursor = newColor;
		else
		if ( colorId == SelectedItemColor )
			mSchemesList[schemeId].selectedItemColor = newColor;
		else
		if ( colorId == SelectedItemColorUnderCursor )
			mSchemesList[schemeId].selectedItemColorUnderCursor = newColor;
		else
		if ( colorId == BgColorOfSelectedItems )
			mSchemesList[schemeId].bgColorOfSelectedItems = newColor;
		else
		if ( colorId == BgColorOfSelectedItemsUnderCursor )
			mSchemesList[schemeId].bgColorOfSelectedItemsUnderCursor = newColor;
		else
		if ( colorId == FirstBackgroundColor )
			mSchemesList[schemeId].firstBackgroundColor = newColor;
		else
		if ( colorId == SecondBackgroundColor )
			mSchemesList[schemeId].secondBackgroundColor = newColor;
		else
		if ( colorId == FullCursorColor )
			mSchemesList[schemeId].fullCursorColor = newColor;
		else
		if ( colorId == InsideFrameCursorColor )
			mSchemesList[schemeId].insideFrameCursorColor = newColor;
		else
		if ( colorId == OutsideFrameCursorColor )
			mSchemesList[schemeId].outsideFrameCursorColor = newColor;
		else
		if ( colorId == GridColor )
			mSchemesList[schemeId].gridColor = newColor;
	}
	drawPreview();
}


void ColorsPage::slotSetGridColor()
{
	slotSetColor( 11 );
}
