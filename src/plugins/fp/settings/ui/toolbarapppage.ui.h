/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qimage.h>
// #include <qsettings.h>

#include "../../../../../icons/storage.xpm"
#include "../../../../../icons/settings.xpm"
#include "../../../../../icons/favorites.xpm"
#include "../../../../../icons/toolbarapp_icons.h"
#include "../../../../../icons/toolbarsett_icons.h"


void ToolBarAppPage::init()
{
	QPixmap settingsScaled, rereadScaled, favoritesScaled, quickgetoutScaled;
	QImage img1((const char**)settings); // need to rescaled, becouse a pixmap is to big
	QImage img2((const char**)reread);
	QImage img3((const char**)favorites);
	QImage img4((const char**)quickgetout);

	settingsScaled.convertFromImage(img1.smoothScale( 20, 20 ),  Qt::AutoColor | Qt::DiffuseDither);
	rereadScaled.convertFromImage(img2.smoothScale( 20, 20 ),  Qt::AutoColor | Qt::DiffuseDither);
	favoritesScaled.convertFromImage(img3.smoothScale( 20, 20 ),  Qt::AutoColor | Qt::DiffuseDither);
	quickgetoutScaled.convertFromImage(img4.smoothScale( 20, 20 ),  Qt::AutoColor | Qt::DiffuseDither);

	mInputActionsListBox->insertItem( QPixmap((const char**)treeview), tr("Change kind of list view"), 0 );
	mInputActionsListBox->insertItem( QPixmap((const char**)ftptool), tr("FTP connection manager"), 1 );
	mInputActionsListBox->insertItem( rereadScaled, tr("Reread current directory"), 2 );
	mInputActionsListBox->insertItem( QPixmap((const char**)proportions), tr("Proportions of panels"), 3 );
	mInputActionsListBox->insertItem( QPixmap((const char**)storage), tr("Show storage devices list"), 4 );
	mInputActionsListBox->insertItem( QPixmap((const char**)freespace), tr("Show free space for current disk"), 5 );
	mInputActionsListBox->insertItem( QPixmap((const char**)findfile), tr("Find file"), 6 );
	mInputActionsListBox->insertItem( favoritesScaled, tr("Show favorites"), 7 );
	mInputActionsListBox->insertItem( quickgetoutScaled, tr("Quick get out from file system"), 8 );
	mInputActionsListBox->insertItem( settingsScaled, tr("Configure ")+" QtCommander", 9 );


	mDeletePushBtn->setAccel( Key_Delete );
	mUpToolBtn->setIconSet( QPixmap((const char**)up) );
	mDownToolBtn->setIconSet( QPixmap((const char**)down) );
	mInsertToolBtn->setIconSet( QPixmap((const char**)insert) );

// 	QSettings settings;
	// --- reads bars with icons list

	// insert bars name
	QStringList barsList;
	barsList.append( tr("global icons") );
	mBarsComboBox->insertStringList( barsList );

	slotChangeCurrentBar( 0 );
}

void ToolBarAppPage::slotInsert()
{
	if ( ! mInputActionsListBox->hasFocus() )
		mInputActionsListBox->setFocus();

	int id = mInputActionsListBox->currentItem();

	if ( mOutputActionsListBox->findItem(mInputActionsListBox->text(id)) == NULL ) {
		mOutputActionsListBox->insertItem( *mInputActionsListBox->pixmap(id), mInputActionsListBox->text(id) );
		updateControlsButtons();
	}
}


void ToolBarAppPage::slotUp()
{
	moveCurrentItem( UP );
}


void ToolBarAppPage::slotDown()
{
	moveCurrentItem( DOWN );
}


void ToolBarAppPage::slotDelete()
{
	if ( mOutputActionsListBox->count() == 0 )
		return;

	if ( ! mOutputActionsListBox->hasFocus() )
		mOutputActionsListBox->setFocus();

	mOutputActionsListBox->takeItem( mOutputActionsListBox->selectedItem() );
	mOutputActionsListBox->setSelected( mOutputActionsListBox->item(mOutputActionsListBox->currentItem()), TRUE );
	updateControlsButtons();
}


void ToolBarAppPage::moveCurrentItem( Direct direct )
{
	if ( mOutputActionsListBox->count() < 2 )
		return;

	if ( ! mOutputActionsListBox->hasFocus() )
		mOutputActionsListBox->setFocus();

	QListBoxItem *replacedItem;
	QListBoxItem *currItem = mOutputActionsListBox->selectedItem();
        if (currItem == NULL)
                return;

	int itHeight = mOutputActionsListBox->itemRect( currItem ).height();
	int currItemYpos = mOutputActionsListBox->itemRect( currItem ).y()+5;

	if ( direct == UP )
		replacedItem = mOutputActionsListBox->itemAt( QPoint(5,currItemYpos-itHeight) );
	else
	if ( direct == DOWN)
		replacedItem = mOutputActionsListBox->itemAt( QPoint(5,currItemYpos+itHeight) );

        if (replacedItem == NULL || replacedItem == currItem)
		return;

	int currentItemId = mOutputActionsListBox->index( currItem );
	int replacedItemId = mOutputActionsListBox->index( replacedItem );

	QPixmap currentItemPixmap = *currItem->pixmap();
	QString currentItemText   = currItem->text();

	mOutputActionsListBox->changeItem( *replacedItem->pixmap(), replacedItem->text(), currentItemId );
	mOutputActionsListBox->changeItem( currentItemPixmap , currentItemText, replacedItemId );
}


void ToolBarAppPage::slotChangeCurrentBar( int /*currentBarId*/ )
{
	//mOutputActionsListBox->clear();
	//inserts icons list about id == currentBarId (0 - global icons, 1 - TextView icons)
	updateControlsButtons();
}


void ToolBarAppPage::updateControlsButtons()
{
	mUpToolBtn->setEnabled( (mOutputActionsListBox->count() > 1) );
	mDownToolBtn->setEnabled( (mOutputActionsListBox->count() > 1) );
	mDeletePushBtn->setEnabled( (mOutputActionsListBox->count() > 0) );
}
