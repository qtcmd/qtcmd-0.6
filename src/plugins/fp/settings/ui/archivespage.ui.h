/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qdir.h>
#include <qheader.h>
#include <qsettings.h>
#include <qpushbutton.h>
#include <qfiledialog.h>
#include <qcombobox.h>
#include <qfileinfo.h>
#include <qcheckbox.h>
#include <qdialog.h>
#include <qlayout.h>
#include <qlabel.h>

#include "messagebox.h"
#include "functions_col.h" // for color() (convert hex string to QColor)
#include "locationchooser.h"
#include "listviewwidgets.h"


void ArchivesPage::init()
{
	QSettings settings;

	QString workDir = settings.readEntry( "/qtcmd/Archives/WorkingDirectory", QDir::homeDirPath()+"/tmp/qtcmd" );
	mLocationChooser->setText( workDir );

	mArchiverFullNameList.append( settings.readEntry("/qtcmd/Archives/Bzip2FullName", "") );
	mArchiverFullNameList.append( settings.readEntry("/qtcmd/Archives/GzipFullName", "") );
	mArchiverFullNameList.append( settings.readEntry("/qtcmd/Archives/RarFullName", "") );
	mArchiverFullNameList.append( settings.readEntry("/qtcmd/Archives/TarFullName", "") );
	mArchiverFullNameList.append( settings.readEntry("/qtcmd/Archives/ZipFullName", "") );

	QString scheme = settings.readEntry( "/qtcmd/FilesPanel/ColorsScheme", tr("DefaultColors") );
	QString colorStr = settings.readEntry( "/qtcmd/"+scheme+"/SecondBackgroundColor", "FFEBDC" );

	mArchiversListView->setColumnAlignment( AVAILABLEcol, Qt::AlignHCenter );
	mArchiversListView->setColumnAlignment( COMPRESSIONcol, Qt::AlignHCenter );
	mArchiversListView->setSecondColorOfBg( color(colorStr, "FFEBDC") );
	mArchiversListView->setEditBoxGetKeyShortcutMode( FALSE );
	mArchiversListView->setEnableTwoColorsOfBg( TRUE );
	mArchiversListView->setCursorForColumn( NAMEcol );

	ListViewWidgetsItem *item;

	QStringList compressionLevelLst;
	QStringList archLst;
	QString archName, archFullName;
	int c;
	archLst << "bzip2" << "gzip" << "rar" << "tar" << "tar.bz2" << "tar.gz" << "zip" << "arj";

	for ( uint i=0; i<archLst.count(); i++ ) {
		archName = archLst[i];
		compressionLevelLst.clear();
		compressionLevelLst.append( tr("store") );

		if ( archName == "tar" )
			archFullName = mArchiverFullNameList[TARpath];
		else
		if ( archName == "tar.bz2" || archName == "tar.gz" || archName == "bzip2" || archName == "gzip" ) {
			compressionLevelLst.clear(); // non store
			compressionLevelLst << "1 - "+tr("fast") << "2" << "3" << "4" << "5" << "6" << "7" << "8" << "9 - "+tr("best");
			if ( archName == "bzip2" )
				archFullName = mArchiverFullNameList[BZIP2path];
			else
			if ( archName == "gzip" )
				archFullName = mArchiverFullNameList[GZIPpath];
			else
				archFullName = mArchiverFullNameList[TARpath];
		}
		else
		if ( archName == "rar" ) {
			compressionLevelLst.append( tr("the fastest") );
			compressionLevelLst.append( tr("fast") );
			compressionLevelLst.append( tr("default") );
			compressionLevelLst.append( tr("good") );
			compressionLevelLst.append( tr("the best") );
			archFullName = mArchiverFullNameList[RARpath];
		}
		else
		if ( archName == "zip" ) {
			compressionLevelLst << "1 - "+tr("fast") << "2" << "3" << "4" << "5" << "6 - "+tr("default") << "7" << "8" << "9 - "+tr("best");
			archFullName = mArchiverFullNameList[ZIPpath];
		}
		item = new ListViewWidgetsItem( mArchiversListView, CheckBox, TextLabel, ComboBox, EditBox );
		item->setTextValue( NAMEcol, archLst[i] );
		item->insertStringList( COMPRESSIONcol, compressionLevelLst );
		c = (archName == "zip") ? 1 : 0;
		item->setText( COMPRESSIONcol, compressionLevelLst[compressionLevelLst.count()/2+c] ); // default is first
		item->setTextAttributs( NAMEcol, Qt::black, TRUE );
		if ( archFullName.isEmpty() ) {
			item->setEnableCell( NAMEcol, FALSE ); // disable all archivers
			item->setEnableCell( COMPRESSIONcol, FALSE ); // disable all archivers
		}
		else
			item->setText( FULLNAMEcol, archFullName );
	}

	// --- init RAR addons settings
	mRarAddsSettingsTab[CreateSolidArchive] = settings.readBoolEntry( "/qtcmd/Archives/RarSolidArchive", TRUE );
	mRarAddsSettingsTab[RarSfxArchive] = settings.readBoolEntry( "/qtcmd/Archives/RarSfxArchive", FALSE );
	mRarAddsSettingsTab[RarSaveSymbolicLinks] = settings.readBoolEntry( "/qtcmd/Archives/RarSaveLinks", TRUE );
	mRarAddsSettingsTab[SaveFileOwnerAndGroup] = settings.readBoolEntry( "/qtcmd/Archives/RarSaveOwnerAndGroup", TRUE );
	mRarAddsSettingsTab[MultimediaCompression] = settings.readBoolEntry( "/qtcmd/Archives/RarMultimediaCompression", FALSE );
	mRarAddsSettingsTab[OverwriteExistingFiles] = settings.readBoolEntry( "/qtcmd/Archives/RarOvrExistingFiles", FALSE );
	mRarAddsSettingsTab[DictionarySize] = settings.readNumEntry( "/qtcmd/Archives/RarDictionarySize", 512 );
	// --- init ZIP addons settings
	mZipAddsSettingsTab[ZipSfxArchive] = settings.readBoolEntry( "/qtcmd/Archives/ZipSfxArchive", FALSE );
	mZipAddsSettingsTab[ZipSaveSymbolicLinks] = settings.readBoolEntry( "/qtcmd/Archives/ZipSaveSymbolicLinks", TRUE );
	mZipAddsSettingsTab[ZipDoNotSaveExtraAttrib] = settings.readBoolEntry( "/qtcmd/Archives/ZipDoNotSaveExtraAttrib", FALSE );

	connect( mArchiversListView, SIGNAL(buttonClicked(int)),
	 this, SLOT(slotButtonClicked(int)) );
	connect( mArchiversListView, SIGNAL(editBoxTextApplied(int, const QString &, const QString &)),
	 this, SLOT(slotEditBoxTextApplied(int, const QString &, const QString &)) );

	mProcess = new QProcess( this );
	connect( mProcess, SIGNAL(readyReadStdout()), this, SLOT(slotReadDataFromStdOut()) );
}


void ArchivesPage::slotDetectSupportedArchives()
{
	QString archiverName;
	QString cmd = "whereis\n-b";
	QListViewItemIterator it( mArchiversListView );

	while ( it.current() != 0 ) {
		archiverName = it.current()->text( NAMEcol );
		if ( archiverName.find("tar.") != -1 ) {
			it++;
			continue;
		}
		cmd += "\n"+archiverName;
		it++;
	}
	mProcess->setArguments( QStringList::split('\n', cmd) );
	mProcess->start();
}


void ArchivesPage::slotSetDefaults()
{
	mLocationChooser->setText( QDir::homeDirPath()+"/tmp/qtcmd" );

	int c;
	ListViewWidgetsItem *item;
	QListViewItemIterator it( mArchiversListView );
	QString defaultCompressionLvl;

	// --- set default compression level for all archivers
	while ( (item=(ListViewWidgetsItem *)it.current()) != 0 ) {
		if ( item->isEnableCell(NAMEcol) ) {
			c = (item->text(NAMEcol) == "zip") ? 1 : 0;
			defaultCompressionLvl = item->stringList(COMPRESSIONcol)[item->stringList(COMPRESSIONcol).count()/2+c];
			item->setText( COMPRESSIONcol, defaultCompressionLvl );
		}
		it++;
	}

	// --- set default RAR addons settings
	mRarAddsSettingsTab[CreateSolidArchive]     = TRUE;
	mRarAddsSettingsTab[RarSfxArchive]          = FALSE;
	mRarAddsSettingsTab[RarSaveSymbolicLinks]   = TRUE;
	mRarAddsSettingsTab[SaveFileOwnerAndGroup]  = TRUE;
	mRarAddsSettingsTab[MultimediaCompression]  = FALSE;
	mRarAddsSettingsTab[OverwriteExistingFiles] = FALSE;
	mRarAddsSettingsTab[DictionarySize] = 512;

	// --- set default ZIP addons settings
	mZipAddsSettingsTab[ZipSfxArchive]           = FALSE;
	mZipAddsSettingsTab[ZipSaveSymbolicLinks]    = TRUE;
	mZipAddsSettingsTab[ZipDoNotSaveExtraAttrib] = FALSE;
}


void ArchivesPage::slotSave()
{
	QSettings settings;

	settings.writeEntry( "/qtcmd/Archives/WorkingDirectory", mLocationChooser->text() );

	// --- write full name for all archivers
	settings.writeEntry( "/qtcmd/Archives/Bzip2FullName", mArchiverFullNameList[BZIP2path] );
	settings.writeEntry( "/qtcmd/Archives/GzipFullName", mArchiverFullNameList[GZIPpath] );
	settings.writeEntry( "/qtcmd/Archives/RarFullName", mArchiverFullNameList[RARpath] );
	settings.writeEntry( "/qtcmd/Archives/TarFullName", mArchiverFullNameList[TARpath] );
	settings.writeEntry( "/qtcmd/Archives/ZipFullName", mArchiverFullNameList[ZIPpath] );

	// --- write RAR addons settings
	settings.writeEntry( "/qtcmd/Archives/RarSolidArchive", mRarAddsSettingsTab[CreateSolidArchive] );
	settings.writeEntry( "/qtcmd/Archives/RarSfxArchive", mRarAddsSettingsTab[RarSfxArchive] );
	settings.writeEntry( "/qtcmd/Archives/RarSaveLinks", mRarAddsSettingsTab[RarSaveSymbolicLinks] );
	settings.writeEntry( "/qtcmd/Archives/RarSaveOwnerAndGroup", mRarAddsSettingsTab[SaveFileOwnerAndGroup] );
	settings.writeEntry( "/qtcmd/Archives/RarMultimediaCompression", mRarAddsSettingsTab[MultimediaCompression] );
	settings.writeEntry( "/qtcmd/Archives/RarOvrExistingFiles", mRarAddsSettingsTab[OverwriteExistingFiles] );
	settings.writeEntry( "/qtcmd/Archives/RarDictionarySize", mRarAddsSettingsTab[DictionarySize] );
	// --- write ZIP addons settings
	settings.writeEntry( "/qtcmd/Archives/ZipSfxArchive", mZipAddsSettingsTab[ZipSfxArchive] );
	settings.writeEntry( "/qtcmd/Archives/ZipSaveSymbolicLinks", mZipAddsSettingsTab[ZipSaveSymbolicLinks] );
	settings.writeEntry( "/qtcmd/Archives/ZipDoNotSaveExtraAttrib", mZipAddsSettingsTab[ZipDoNotSaveExtraAttrib] );
}


void ArchivesPage::slotShowAdditionalSettings()
{
	if ( ! ((ListViewWidgetsItem *)mArchiversListView->currentItem())->isEnableCell(NAMEcol) )
		return;

	QString currentArch = mArchiversListView->currentItem()->text(NAMEcol);

	if ( currentArch != "rar" && currentArch != "zip" )
		return;

	QStringList settingsLst;
	if ( currentArch == "rar" ) { // do not change order of this list
		settingsLst.append( tr("Create solid archive") );
		settingsLst.append( tr("Create SFX archive") );
		settingsLst.append( tr("Save symbolic links") );
		settingsLst.append( tr("Save file owner and group") );
		settingsLst.append( tr("Multimedia compression") );
		settingsLst.append( tr("Overwrite existing files") );
	}
	else
	if ( currentArch == "zip" ) { // do not change order of this list
		settingsLst.append( tr("Create SFX archive") );
		settingsLst.append( tr("Save symbolic links") );
		settingsLst.append( tr("Do not save file owner and group and file times") );
	}

	// --- create a dialog and the objects to inserting
	QDialog *dlg = new QDialog( this );
	QPushButton *okBtn     = new QPushButton( tr("&OK"), dlg );
	QPushButton *cancelBtn = new QPushButton( tr("&Cancel"), dlg );
	QGridLayout *gridLayout = new QGridLayout( dlg, 1, 1, 11, 6 );
	QSpacerItem *spacer1 = new QSpacerItem( 16, 21, QSizePolicy::Minimum, QSizePolicy::Expanding );
	QSpacerItem *spacer2 = new QSpacerItem( 31, 16, QSizePolicy::Expanding, QSizePolicy::Minimum );

	QLabel *label = new QLabel( dlg, "label" );
	QComboBox *dictionarySizeCombo = new QComboBox( dlg );
	if ( currentArch == "rar" ) {
		dictionarySizeCombo->insertStringList( QStringList::split(',', "64,128,256,512,1024,2048,4096") );
		label->setText( tr("Dictionary size")+" :" );
	}
	else
		dictionarySizeCombo->hide();

	// signals and slots connections
	connect( okBtn, SIGNAL(clicked()), dlg, SLOT(accept()) );
	connect( cancelBtn, SIGNAL(clicked()), dlg, SLOT(reject()) );

	// --- insert the objects into the dialog
	uint i;
	QCheckBox *chkBox;
	QPtrList <QCheckBox> checkBoxList;

	for ( i=0; i<settingsLst.count(); i++ ) {
		chkBox = new QCheckBox(settingsLst[i], dlg);
		gridLayout->addMultiCellWidget( chkBox, i, i, 0, 3 );
		checkBoxList.append( chkBox );
		if ( currentArch == "rar" )
			chkBox->setChecked( mRarAddsSettingsTab[i] );
		else
		if ( currentArch == "zip" )
			chkBox->setChecked( mZipAddsSettingsTab[i] );;
	}

	if ( currentArch == "rar" ) {
		gridLayout->addMultiCellWidget( label, i, i, 0, 1 );
		gridLayout->addMultiCellWidget( dictionarySizeCombo, i, i, 2, 3 );
		dictionarySizeCombo->setCurrentText( QString::number(mRarAddsSettingsTab[DictionarySize]) );
		i++;
	}
	gridLayout->addItem( spacer1, i, 1 );
	i++;
	gridLayout->addItem( spacer2, i, 0 );
	gridLayout->addWidget( okBtn, i, 1 );
	gridLayout->addWidget( cancelBtn, i, 3 );

	dlg->adjustSize();
	dlg->setCaption( currentArch+" "+tr("settings")+" - QtCommander" );

	// --- show dialog
	int result = dlg->exec();

	// --- get settings from dialog
	if ( result > 0 ) { // 1 - OK
		QCheckBox *checkBox;
		uint count=0;
		for (checkBox=checkBoxList.first(); checkBox; checkBox=checkBoxList.next()) {
			if ( currentArch == "rar" )
				mRarAddsSettingsTab[count] = checkBox->isChecked();
			else
			if ( currentArch == "zip" )
				mZipAddsSettingsTab[count] = checkBox->isChecked();
			count++;
		}
		if ( currentArch == "rar" )
			mRarAddsSettingsTab[DictionarySize] = dictionarySizeCombo->currentText().toInt();//KB (*1024);
	}

	delete dlg;
}


void ArchivesPage::slotChangePathForArchiver()
{
	if ( ! mArchiversListView->selectedItem() )
		return;

	QString fileName = QFileDialog::getOpenFileName(
	 "/", tr("All files")+"(*)", this, 0,
	 tr("Select archiver file")+" "+mArchiversListView->currentItem()->text(NAMEcol)+" - QtCommander"
	);

	if ( ! fileName.isEmpty() && QFileInfo(fileName).isExecutable() ) {
		ListViewWidgetsItem *item = (ListViewWidgetsItem *)mArchiversListView->currentItem();
		item->setTextValue( FULLNAMEcol, fileName );
		item->setEnableCell( NAMEcol, TRUE );
		item->setEnableCell( COMPRESSIONcol, TRUE );
		item->setBoolValue( AVAILABLEcol, TRUE );
	}
}


void ArchivesPage::slotReadDataFromStdOut()
{
	QString buffer = mProcess->readStdout();
	if ( buffer.isEmpty() )
		return;

	ListViewWidgetsItem *item;
	QStringList outputLines = QStringList::split( '\n', buffer );
	QString archiverName, fullName;
	QStringList lineLst;

	for ( uint i=0; i<outputLines.count(); i++ ) {
		lineLst = QStringList::split( ':', outputLines[i] );
		archiverName = lineLst[0];
		fullName = lineLst[1].stripWhiteSpace();
		lineLst = QStringList::split( ' ', fullName );

		for ( uint j=0; j<lineLst.count(); j++ ) {
			if ( QFileInfo(lineLst[j]).isDir() )
				continue;
			if ( QFileInfo(lineLst[j]).isExecutable() ) {
				fullName = lineLst[j];
				break;
			}
		}
		//debug("archiverName=%s, fullName=%s", archiverName.latin1(), fullName.latin1() );

		item = (ListViewWidgetsItem *)mArchiversListView->findItem( archiverName, NAMEcol );
		if ( item ) {
			item->setTextValue( FULLNAMEcol, fullName );
			if ( ! fullName.isEmpty() ) {
				item->setBoolValue( AVAILABLEcol, TRUE );
				item->setEnableCell( NAMEcol, TRUE );
				item->setEnableCell( COMPRESSIONcol, TRUE );
			}
			if ( archiverName == "tar" ) { // update tar.bz2 and tar.gz
				item = (ListViewWidgetsItem *)mArchiversListView->findItem( "tar.bz2", NAMEcol );
				if ( item ) {
					item->setTextValue( FULLNAMEcol, fullName );
					if ( ! fullName.isEmpty() ) {
						item->setBoolValue( AVAILABLEcol, TRUE );
						item->setEnableCell( NAMEcol, TRUE );
					}
				}
				item = (ListViewWidgetsItem *)mArchiversListView->findItem( "tar.gz", NAMEcol );
				if ( item ) {
					item->setTextValue( FULLNAMEcol, fullName );
					if ( ! fullName.isEmpty() ) {
						item->setBoolValue( AVAILABLEcol, TRUE );
						item->setEnableCell( NAMEcol, TRUE );
						item->setEnableCell( COMPRESSIONcol, TRUE );
					}
				}
			}
		}
	}

	ListViewWidgetsItem *currItem = (ListViewWidgetsItem *)mArchiversListView->currentItem();
	if ( currItem )
		mAdditionalSettingsBtn->setEnabled( (currItem->text(NAMEcol) == "rar" || currItem->text(NAMEcol) == "zip") && currItem->isEnableCell(NAMEcol) );
}


void ArchivesPage::slotButtonClicked( int column )
{
	ListViewWidgetsItem *item = ((ListViewWidgetsItem *)mArchiversListView->currentItem());

	if ( item->text(FULLNAMEcol).isEmpty() ) {
		item->setBoolValue( column, FALSE );
		return;
	}

	item->setEnableCell( NAMEcol, item->boolValue(column) );
	item->setEnableCell( COMPRESSIONcol, item->boolValue(column) );
}


void ArchivesPage::slotEditBoxTextApplied( int column, const QString &, const QString & newText )
{
	QFileInfo file(newText);
	ListViewWidgetsItem *item = ((ListViewWidgetsItem *)mArchiversListView->currentItem());

	if ( ! file.exists() )
		MessageBox::critical( this, "\""+newText+"\"\n\n"+tr("This file not exists !") );
	else
	if ( ! file.isExecutable() )
		MessageBox::critical( this, "\""+newText+"\"\n\n"+tr("This file isn't executable !") );
	else {
		item->setEnableCell( NAMEcol, TRUE );
		item->setEnableCell( COMPRESSIONcol, TRUE );
		item->setTextValue( column, newText );
		item->setBoolValue( AVAILABLEcol, TRUE );
	}
}


void ArchivesPage::slotSelectionChanged( QListViewItem * item )
{
	QString archName = item->text(NAMEcol);

	mArchiverNameLab->setText( archName );
	mAdditionalSettingsBtn->setEnabled( (archName == "rar" || archName == "zip") && ((ListViewWidgetsItem *)item)->isEnableCell(NAMEcol) );
}


QString ArchivesPage::workDirPath()
{
	return mLocationChooser->text();
}


QString ArchivesPage::compressionLevel( const QString & archiverName )
{
	ListViewWidgetsItem *item = (ListViewWidgetsItem *)mArchiversListView->findItem( archiverName, NAMEcol );

	if ( item->isEnableCell(NAMEcol) )
		return item->textValue( COMPRESSIONcol );

	return QString::null;
}


QString ArchivesPage::archiverFullName( const QString & archiverName )
{
	ListViewWidgetsItem *item = (ListViewWidgetsItem *)mArchiversListView->findItem( archiverName, NAMEcol );

	if ( item->isEnableCell(NAMEcol) )
		return item->textValue( FULLNAMEcol );

	return QString::null;
}
