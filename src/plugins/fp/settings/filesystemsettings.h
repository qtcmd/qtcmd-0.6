/***************************************************************************
                          filesystemsettings.h  -  description
                             -------------------
    begin                : wed apr 14 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/


#ifndef _FILESYSTEMSETTINGS_H_
#define _FILESYSTEMSETTINGS_H_

struct FileSystemSettings {
	// archives page
	QString workDirectoryPath;
	// lfs page
	bool removeToTrash;
	bool weighBeforeOperation;
	QString trashPath;
	// ftp page
	bool standByConnection;
	int numOfTimeToRetryIfFtpBusy;
	// other page
	bool alwaysOverwrite;
	bool alwaysSavePermission;
	bool alwaysAskBeforeDeleting;
	bool alwaysGetInToDirectory;
	bool closeProgressDlgAfterFinished;
};

#endif
