/***************************************************************************
                          listsettings.h  -  description
                             -------------------
    begin                : wed apr 14 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _LISTVIEWSETTINGS_H_
#define _LISTVIEWSETTINGS_H_

#include <qcolor.h>

struct ListViewSettings {
	// font page
	QString fontFamily;
	uint fontSize;
	bool fontBold;
	bool fontItalic;
	// colors page
	bool showSecondBackgroundColor, showGrid, twoColorsCursor;
	QColor itemColor, itemColorUnderCursor;
	QColor selectedItemColor, selectedItemColorUnderCursor;
	QColor bgColorOfSelectedItems, bgColorOfSelectedItemsUnderCursor;
	QColor firstBackgroundColor, secondBackgroundColor;
	QColor fullCursorColor;
	QColor insideFrameCursorColor, outsideFrameCursorColor;
	QColor gridColor;
	// file colors page
	QColor commonFileColor, exeFileColor, symlinkColor, brokenSymlinkColor;
	QColor socketColor, characterDevColor, blockDevColor, fifoColor;
	// columns page
	int columnsWidthTab[6];
	bool columnsSynchronize;
	// other page
	bool showIcons;
	bool showHiddenFiles;
	bool cursorAlwaysVisible;
	bool selectFilesOnly;
	uint defaultFilter;
	bool filterForFiltered;
	int fileSizeFormat;
};

#endif
