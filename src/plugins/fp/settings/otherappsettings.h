/***************************************************************************
                          otherappsettings  -  description
                             -------------------
    begin                : wed apr 14 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _OTHERAPPSETTINGS_H_
#define _OTHERAPPSETTINGS_H_

struct OtherAppSettings {
	// key codes
	int copyFileKeyCode;
	int moveFileKeyCode;
	int makeDirKeyCode;
	//...
	// other
};

#endif
