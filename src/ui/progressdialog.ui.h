/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include "functions.h"
#include "squeezelabel.h"

void ProgressDialog::init( Operation op, bool closeAfterFinished, bool move, const QString & targetPath )
{
	QString titleMsg;

	if ( op == Copy )
		titleMsg = (move) ? tr("Moving") : tr("Copying");
	else
	if ( op == Remove )
		titleMsg = tr("Removing");
	else
	if ( op == Weigh )
		titleMsg = tr("Weighing");
	else
	if ( op == SetAttributs )
		titleMsg = tr("Setting the attributs");

	slotSetOperation( op );

	mCancelBtn->setAccel( Key_Escape );

	timeLabel->hide(); mTimeLab->hide();
	transferLabel->hide(); mTransferLab->hide();
	estimatedTimeLabel->hide(); mEstimatedTimeLab->hide();

	// remove not useable objects
	if ( op == Remove || op == Weigh || op == SetAttributs ) {
		delete trgLabel; delete mTargetLab;
		delete mFileProgress; mFileProgress = 0;
		delete transferLabel; delete mTransferLab; mTransferLab = 0;
		delete estimatedTimeLabel; delete mEstimatedTimeLab; mEstimatedTimeLab = 0;

		if ( op == Weigh )
			delete mTotalProgress;

		//adjustSize();
		//setFixedSize(QSize(width()+((width()*15)/100), sizeHint().height()));
		//resize(QSize(width()+((width()*15)/100), sizeHint().height()));
		//resize( (width()*2)+2-((width()*2)*10)/100, sizeHint().height() );
	}
	adjustSize();
	//setFixedSize(QSize(width()+((width()*15)/100), sizeHint().height()));
	//resize( (width()*2)+2-((width()*2)*15)/100, sizeHint().height() );

	setCaption( titleMsg+" "+tr("files")+" - QtCommander" );

	if ( op == Copy || op == Move )
		mTargetLab->setText( targetPath );

	mCloseAfterFinishedChkBox->setChecked( closeAfterFinished );

	connect( mCloseAfterFinishedChkBox, SIGNAL(toggled( bool )),
		this, SIGNAL(signalCloseAfterFinished( bool )) );

	mTimer = new QTimer( this );
	connect( mTimer, SIGNAL(timeout()), this, SLOT(slotSetTime()) );

	if ( op == Copy || op == Move ) {
		mFileProgress->setTotalSteps( 100 );
		mTransferLab->setText( "? "+tr("bytes per second") );
	}
	if ( op == Copy || op == Move || op == Remove )
		mTotalProgress->setTotalSteps( 100 );

	mBytesTransfered = 0;
	mTotalFilesStr = "0";
	mTotalDirsStr = "0";
	mNumFilesStr = "0";
	mNumDirsStr = "0";
	mTotalWeight = 0;

	mTime.setHMS( 0, 0, 0 );
	mTime.start();
	slotSetTime();
	timerStart();
}


void ProgressDialog::slotBackground()
{
	mInBrgdBtn->setEnabled( FALSE );
	// trick: swich modal to modeless
	hide();
	setModal( FALSE );
	show();
}

void ProgressDialog::slotSetSourceInfo( const QString & sSourcePath, const QString & /*sTargetPath*/ )
{
	mSourceLab->setText( sSourcePath );
}


void ProgressDialog::slotDataTransferProgress( long long done, long long total, uint bytesTransfered )
{
	if ( mFileProgress && total ) {
		if ( done == total )
			mFileProgress->setProgress( 100 );
		else
			mFileProgress->setProgress( (done*100)/total );

		mBytesTransfered += bytesTransfered;
	}
}


void ProgressDialog::slotSetTotalProgress( int done, long long totalWeight )
{
	if ( done )
		mTotalProgress->setProgress( done );
	mTotalWeight = totalWeight;
}


void ProgressDialog::slotSetTime() // and transfer
{
	if ( mDateTime.time().second() == 1 ) {
		timeLabel->show(); mTimeLab->show();
		if ( mTransferLab ) {
			transferLabel->show(); mTransferLab->show();
		}
		if ( mEstimatedTimeLab ) {
			estimatedTimeLabel->show(); mEstimatedTimeLab->show();
		}
	}
	mDateTime.setTime_t( mTime.elapsed()/1000, Qt::UTC );
	mTimeLab->setText( (mDateTime.time()).toString() );
	// TODO sprobowac zmienic mDateTime na QTime (zrobic tak jak nizej)
	if ( mBytesTransfered ) {
		mTransferLab->setText( formatNumber(mBytesTransfered,BKBMBGBformat)+" "+tr("per second") );
		// --- set an estimated time
		if ( mTotalWeight ) {
			QTime estimatedTime(0,0);
			estimatedTime = estimatedTime.addSecs( mTotalWeight/mBytesTransfered );
			mEstimatedTimeLab->setText( estimatedTime.toString() );
		}
		mBytesTransfered = 0;
	}
}

void ProgressDialog::slotSetProcessedNumber( int value, bool fileCounting, bool totalValue )
{
	if ( totalValue ) {
		if ( fileCounting )
			mTotalFilesStr = QString::number( value );
		else
			mTotalDirsStr  = QString::number( value );

		mProcessedLab->setText(
		 tr("files")+": "+mTotalFilesStr+", "+tr("directories")+": "+mTotalDirsStr
		);
	}
	else {
		if ( fileCounting )
			mNumFilesStr = QString::number( value );
		else
			mNumDirsStr  = QString::number( value );

		mProcessedLab->setText(
		 tr("files")+": "+mNumFilesStr+"\\"+mTotalFilesStr+", "+
		 tr("directories")+": "+mNumDirsStr +"\\"+mTotalDirsStr
		);
	}
// 	debug("%s %s", mProcessedLabel->text().latin1(), mProcessedLab->text().latin1() );
}


void ProgressDialog::slotSetOperation( Operation op )
{
	QString msg;

	if ( op == Copy || op == Move )   msg = tr("Copied");
//	else
//	if ( op == Move )   msg = tr("Moved");
	else
	if ( op == Remove ) msg = tr("Removed");
	else
	if ( op == Weigh )  msg = tr("Weighed");
	else
	if ( op == SetAttributs ) msg = tr("Set the attributs");
	else // unknown operation
		return;

	mProcessedLabel->setText( msg+" :" );
}

void ProgressDialog::timerStart()
{
	mTimer->start( 1000 ); // emits signal every 1 seconds
}

void ProgressDialog::timerStop( bool removeIt )
{
	mTimer->stop();
	mInBrgdBtn->setEnabled( FALSE );

	if ( removeIt )
		delete mTimer;
}

void ProgressDialog::closeEvent( QCloseEvent * e )
{
	mTimer->stop();
	emit signalCancel();
	e->ignore();
}
