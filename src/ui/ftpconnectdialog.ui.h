/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include "messagebox.h"

#include <qsettings.h>

/**
 Statyczna funkcja pokazuj�ca g��wne okno po��czenia FTP.
*/
bool FtpConnectDialog::showDialog( QString & inputURL, QWidget * )
{
	FtpConnectDialog *dlg = new FtpConnectDialog( 0 );
	CHECK_PTR( dlg );

	dlg->setCaption( tr("Connect to")+" ... - QtCommander" );
	dlg->initSession( inputURL );

	bool doConnection = FALSE;
	uint b = 0;

	while( dlg->exec() == QDialog::Accepted ) {
		doConnection = TRUE;  b = 0;
		if ( dlg->mUserCombo->currentText().isEmpty() )
			MessageBox::critical( 0, tr("Do not passed an user name !") );
		else
			b++;

		if ( dlg->mPasswordEdit->text().isEmpty() )
			MessageBox::critical( 0, tr("Do not passed a password !") );
		else
			b++;

		if ( b == 2 )
			break;
	}

	if ( doConnection ) {
		dlg->makeNewURL( inputURL ); // the 'inputURL' will be include a new URL

		QSettings settings;
		if ( dlg->mSaveSessionChkBox->isChecked() ) {
			// --- save an URL to the end of session list
			int n = -1;
			bool addSession = TRUE;
			QString session = "x";
			while( ! session.isEmpty() ) { // find number of last session
				n++;
				session = settings.readEntry( QString("/qtcmd/FTP/S%1").arg(n) );
				if ( session.section( "\\", 1,1 ) == inputURL ) // disable add this same session
					addSession = FALSE;
			}
			if ( addSession )
				settings.writeEntry( QString("/qtcmd/FTP/S%1").arg(n),
					tr("new session")+"\\"+inputURL );
		}
		// --- add new user name (only if not exists it on the list) and save new list
		QString currentUserName = dlg->mUserCombo->currentText();
		bool addUser = TRUE;
		for ( uint id=0; id < dlg->mUsersList.count(); id++ )
			if ( dlg->mUsersList[id] == currentUserName ) {
				addUser = FALSE;
				break;
			}
		if ( addUser ) { // new user, add it's name to the config file
			dlg->mUsersList.append( currentUserName );
			settings.writeEntry( "/qtcmd/FTP/UsersName", dlg->mUsersList, '\\' );
		}
	}

	delete dlg;
	dlg = 0;

	return doConnection;
}

/**
 Metoda inicjuj�ca dialog sk�adowymi nowego URLa ('inputURL'). Nazwy
 urzytkownik�w, s� odczytywane z pliku konfiguracyjnego.
*/
void FtpConnectDialog::initSession( const QString & inputURL )
{
	// --- read user's list from config file
	QSettings settings;

	mUsersList = settings.readListEntry( "/qtcmd/FTP/UsersName", '\\' );
	if ( mUsersList.isEmpty() ) {
		mUsersList.append( "anonymous" );
		settings.writeEntry( "/qtcmd/FTP/UsersName", "anonymous", '\\' );
	}
	mUserCombo->insertStringList( mUsersList );

	// --- parse inputURL
	QString sHost, sUser = "", sPasswd = "";
	QString sDir = inputURL;
	int nPort = 21;

	int nMonkeyPos = sDir.findRev('@'), nColonPos;
	if ( nMonkeyPos != -1 ) {
		sDir  = sDir.right(sDir.length()-nMonkeyPos-1);
		sHost = sDir.left(sDir.find('/'));
		sDir  = sDir.remove(sHost);
		sUser = inputURL.left(inputURL.findRev('@'));
		sUser = sUser.remove( "ftp://" );
		nColonPos = sUser.findRev(':');
		if ( nColonPos != -1 ) {
			sPasswd = sUser.right(sUser.length()-sUser.findRev(':')-1);
			sUser = sUser.left(sUser.findRev(':'));
		}
	}
	else { // URL without user
		sHost = sDir.remove( "ftp://" );
		sHost = sHost.left( sHost.find('/') );
		sDir  = sDir.remove(sHost);
	}
	nColonPos = sHost.findRev(':'); // find port
	if ( nColonPos != -1 ) {
		nPort = sHost.right(sHost.length()-nColonPos-1).toInt();
		sHost = sHost.left( nColonPos ); // rest (without 'nPort:') save into sHost
	}

	mDirEdit->setText( sDir );
	mHostEdit->setText( sHost );
	mPortSpin->setValue( nPort );
	mPasswordEdit->setText( sPasswd );

	if ( sUser.isEmpty() ) // into an URL is not include sUser
		return;

	bool insertUser = TRUE;
	for ( uint id=0; id < mUsersList.count(); id++ )
		if ( mUsersList[id] == sUser ) {
			mUserCombo->setCurrentItem( id );
			insertUser = FALSE;
			break;
		}

	if ( insertUser ) {
		mUserCombo->insertItem( sUser );
		mUserCombo->setCurrentItem( 0 );
	}
	mUserCombo->setCurrentText( sUser );
	// user always must be current
}

/**
 Funkcja tworzy nowy URL na podstawie warto�ci p�l dialogu.
 W parametrze 'newURL' b�dzie zapisany nowy URL, utworzony wed�ug wzorca:
 "ftp://user:pass@hostdir".
*/
void FtpConnectDialog::makeNewURL( QString & newURL )
{
	QString host = mHostEdit->text();
	QString dir  = mDirEdit->text();
	QString user = mUserCombo->currentText();
	QString pass = mPasswordEdit->text();
	QString port = QString::number( mPortSpin->value() );

	if ( dir.at(0) != '/' )
		dir.insert( 0, '/' );
	if ( dir.at(dir.length()-1) != '/' )
		dir += "/";

	if ( host.find("ftp://") == 0 )
		host.remove( 0, 6 );

	if ( host.isEmpty() )
		host = "localhost";
	if ( user.isEmpty() )
		user = "anonymous";
	if ( port.isEmpty() )
		port = "21";

	uint hostLen = host.length()-1;
	if ( host.at(hostLen) == '/' ) // host name cannot be ended by the slash
		host.truncate( hostLen );

	newURL = "ftp://"+user+":"+pass+"@"+host+":"+port+dir;
}
