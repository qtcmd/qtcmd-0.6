/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qmap.h>
//#include <qsettings.h>
#include <qfiledialog.h>

#include "squeezelabel.h"
#include "locationchooser.h"

/** @short Dialog wspiera operacje: MakeLink, Copy, Move, Rename.
*/
// TODO
// Dodac fun. getText bez param.: 'bool & p1', 'bool & alwaysOvr', 'bool p3', 'bool dirsOnly'
// wart.param p1, alwaysOvr, p3, dirsOnly  wczytywac z pliku konf. (trzeba by wtedy przekazywac 'klucze')
// dla MakeLink od 'p1': mHardLink, mAlwaysOverwrite, editOnly, FALSE, domyslna_wart=(0)
// dla Copy/Move od 'p1': mSavePermision, mAlwaysOverwrite, domyslna_wart=(FALSE), domyslna_wart=(TRUE), domyslna_wart=(0)

/** Funkcja pokazuje dialog pobieranie nazwy docelowej dla podanej operacji.\n
 @param infoFileText - nazwa pliku, kt�rego dotyczy operacja,
 @param initEditText - domy�lna nazwa docelowa dla bie��cej operacji,
 @param op - wykonywana operacja,
 @param okPressed - zawiera informacje o wci�ni�ciu guzika OK na bie��cym
  dialogu
 @param keyListReading - nazwa klucza w pliku konfiguracyjnym dla listy, kt�r�
  nale�y wstawi� w ComboBox,
 */
QString PreOperationDialog::getText( const QString & infoFileText, const QString & initEditText, Operation op, const QString & keyListReading, bool & okPressed, bool & p1, bool & alwaysOvr, bool p3, bool dirsOnly, QWidget *parent )
{
	if ( op != MakeLink && op != Copy && op != Move && op != Rename && op != MakeDir && op != Touch )
		return QString::null;

	PreOperationDialog *dlg = new PreOperationDialog( parent );
	CHECK_PTR( dlg );

	QString initTxt = initEditText;
	dlg->mLocationChooser->setOriginalPath( initEditText );
	dlg->mLocationChooser->setMenuItemVisible( LocationChooser::CURRENT_DIR, FALSE );

	if ( op == MakeLink ) {
		delete dlg->mBackgroundCpyBtn;
		dlg->mLocationChooser->setGetMode( LocationChooser::GetFile );

		if ( p3 ) { // hides checkBoxes, becouse it's edit link operation
			dlg->mCheckBox1->hide();
			dlg->mCheckBox2->hide();
		}
		else
			dlg->mCheckBox1->setText( tr("Hard link") );
	}
	else
	if ( op == Copy || op == Move ) {
		dlg->mBackgroundCpyBtn->setEnabled( FALSE ); // 'in background' not supported yet
	}
	else { // other operation (Rename/MakeDir/Touch)
		dlg->mCheckBox1->hide();
		dlg->mCheckBox2->hide();
		delete dlg->mBackgroundCpyBtn;
		dlg->mLocationChooser->setButtonSide( LocationChooser::NoneButton );
		if ( op == Touch )
			dlg->mLocationChooser->setGetMode( LocationChooser::GetFile );
	}

	dlg->mCheckBox1->setChecked( p1 ); // savePerm or hard link
	dlg->mCheckBox2->setChecked( alwaysOvr );


	QMap <Operation, QString> actionMap, capionMap;
	actionMap.insert( Copy, tr("copy to") );
	actionMap.insert( Move, tr("move to") );
	actionMap.insert( Rename, tr("new name for current file") );
// 	actionMap.insert( MakeDir, "" );
// 	actionMap.insert( Touch, "" );
	if ( p3 ) // editOnly for MakeLink operation is TRUE
		actionMap.insert( MakeLink, tr("new path for current link") );
	else
		actionMap.insert( MakeLink, tr("new link name") );

	capionMap.insert( Copy, tr("Copying files to")+" ..." );
	capionMap.insert( Move, tr("Moving files to")+" ..." );
	capionMap.insert( Rename, tr("Rename copied file") );
	capionMap.insert( MakeDir, tr("Making directory") );
	capionMap.insert( Touch, tr("Making empty file") );
	if ( p3 ) // editOnly for MakeLink operation is TRUE
		capionMap.insert( MakeLink, tr("Editing link") );
	else
		capionMap.insert( MakeLink, tr("Making link") );


	if ( op != Rename ) // init list only for MakeLink, Copy and Move operation
		dlg->mLocationChooser->setKeyListReading( keyListReading, TRUE ); // TRUE - read and init settings

	dlg->setCaption( capionMap[op]+" - QtCommander" );
	if ( op != MakeDir && op != Touch ) {
		dlg->mActionLabel->setText( actionMap[op]+" :" );
		dlg->mLocationChooser->setText( initEditText );
	}
	else
	if ( dlg->mLocationChooser->text().isEmpty() ) // list (dir.names) not read
		dlg->mLocationChooser->setText( initEditText );

	dlg->mFileInfoLabel->setText( infoFileText );
	dlg->mLocationChooser->selectAll();
	dlg->mLocationChooser->setFocus();

	dlg->mShowDirsOnly = dirsOnly;
	dlg->mTargetPath = (dirsOnly) ? initEditText : initEditText.left( initEditText.findRev('/') );

	QString targetFile = initEditText;

	okPressed = (dlg->exec() == QDialog::Accepted);

	if ( okPressed ) {
		targetFile = dlg->mLocationChooser->text();
		p1         = dlg->mCheckBox1->isChecked(); // savePerm or hard link
		alwaysOvr  = dlg->mCheckBox2->isChecked();
		dlg->mLocationChooser->insertItem( targetFile );
		dlg->mLocationChooser->saveSettings(); // save list entries
	}

	delete dlg;

	return targetFile;
}
