/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include "localfs.h"   // for getOwners()
#include "functions.h" // for formatNumber()
#include "messagebox.h"
#include "fileinfoext.h"

#include <qwidget.h>
#include <qsettings.h>
#include <qfiledialog.h>


void FindFileDialog::init( const QString & currentPath, FilesAssociation * )
{
	setCaption( tr("Find file(s)")+" - QtCommander" );
	QSettings settings;

	// --- init first Tab
	// 	int caseSensitiveName = readNumEntry( "/qtcmd/FilesPanel/FindCaseSensitiveName", -1 );
	// 	mNameCaseSensitiveChkBox->setChecked( (caseSensitiveName<0) ? FALSE : (bool)caseSensitiveName );
	setTabOrder( mNameChooser, mLocationChooser );
	mLocationChooser->setCurrentPath( currentPath );
	mLocationChooser->setKeyListReading( "/qtcmd/FilesSystem/FindLocations", TRUE );
	mLocationChooser->setOriginalPath( mLocationChooser->text() );

	mNameChooser->setKeyListReading( "/qtcmd/FilesSystem/FindFileNames", TRUE );
	mNameChooser->setFocus();

	// --- second Tab ( NOT YET SUPPORTED )
	mContainsTextEdit->setEnabled( FALSE );

	// --- init third Tab
	mTimeGroupBox->setEnabled( FALSE );
	mFirstDateTimeEdit->setDateTime( QDateTime::currentDateTime() );
	mSecondDateTimeEdit->setDateTime( QDateTime::currentDateTime() );
	mOwnerComboBox->setEnabled( FALSE );
	mGroupComboBox->setEnabled( FALSE );
	slotFileSize( FALSE ); // disables few obj.

	QStringList list;
	LocalFS::getOwners( TRUE, list, TRUE );  // get all users
	mOwnerComboBox->insertStringList( list );
	LocalFS::getOwners( FALSE, list, TRUE ); // get all groups
	mGroupComboBox->insertStringList( list );

	// --- init forth Tab
	mFindRecursiveChkBox->setChecked( TRUE );
	// init the filters
	mFilters = new Filters( /*fa*/ );
	for ( uint i=0; i<mFilters->baseFiltersNum(); i++ )
		mFiltersComboBox->insertItem( mFilters->name(i) );

	mSearchInSelectedChkBox->setEnabled( FALSE ); // NOT YET SUPPORTED

	findFinished();
	mCloseBtn->setAccel( Key_Escape );
}


void FindFileDialog::findFinished()
{
	mFindBtn->setEnabled( TRUE );
	mStopBtn->setEnabled( FALSE );
	mPauseBtn->setEnabled( FALSE );
	// 	mTabWidget->setEnabled( TRUE );
	for (int i=0; i<4; i++)
		mTabWidget->page(i)->setEnabled( TRUE );
	mClearListBtn->setEnabled( TRUE );
	mStatusInfoLab->setText( tr("Ready.") );

	QListViewItem *item = mFindedFilesListView->firstChild();
	if ( item )
		mFindedFilesListView->setCurrentItem( item );
}


void FindFileDialog::updateFindStatus( uint matches, uint files, uint directories )
{
	QString msg =
	 tr("Matched:")+" "+QString::number(matches)+" "+tr("from")+" "+
	 QString::number(files)+" "+tr("files and")+" "+
	 QString::number(directories)+" "+tr("directories");

	mStatusDetailsLab->setText( msg );
}


void FindFileDialog::insertMatchedItem( const UrlInfoExt & ui )
{
	QString name = FileInfoExt::fileName( ui.name() );
	QString location = FileInfoExt::filePath( ui.name() );

	(void) new QListViewItem(
	 mFindedFilesListView,
	 name, ui.sizeStr(BKBMBGBformat),
	 ui.lastModifiedStr(), ui.permissionsStr(), ui.owner(), ui.group(), location
	);
}


void FindFileDialog::slotFindStart()
{
	QString currentComboText;

	// --- check and update the comboBoxes
	currentComboText = mNameChooser->text();
	mFindCriterion.name = currentComboText;
	if ( currentComboText.isEmpty() ) {
		mTabWidget->setCurrentPage( 0 );
		MessageBox::critical( this, tr("Don't gived a name to find")+" !" );
		mNameChooser->setFocus();
		return;
	}
	else
		mNameChooser->insertItem( currentComboText );

	currentComboText = mLocationChooser->text();
	mFindCriterion.location = currentComboText;
	if ( currentComboText.isEmpty() ) {
		mTabWidget->setCurrentPage( 0 );
		MessageBox::critical( this, tr("Don't gived a location for searching !") );
		mLocationChooser->setFocus();
		return;
	}
	else
		mLocationChooser->insertItem( currentComboText );

	// --- enables and disables obj.
	mStopBtn->setEnabled( TRUE );
	mFindBtn->setEnabled( FALSE );
	mPauseBtn->setEnabled( TRUE );
	//	mTabWidget->setEnabled( FALSE );
	for (int i=0; i<4; i++)
		mTabWidget->page(i)->setEnabled( FALSE );
	mClearListBtn->setEnabled( FALSE );

	// ---initialize find criterion obj.
	mFindCriterion.caseSensitiveName = mNameCaseSensitiveChkBox->isChecked();

	mFindCriterion.containText = mContainsTextEdit->text();
	mFindCriterion.caseSensitiveText = mTextCaseSensitiveChkBox->isChecked();
	mFindCriterion.includeBinFiles = mIncludeBinaryChkBox->isChecked();
	mFindCriterion.regExpText = mTextRegExpChkBox->isChecked();

	mFindCriterion.checkTime  = mTimeChkBox->isChecked();
	mFindCriterion.firstTime  = mFirstDateTimeEdit->dateTime();
	mFindCriterion.secondTime = mSecondDateTimeEdit->dateTime();

	mFindCriterion.checkFileSize = (mFileSizeChkBox->isChecked()) ? (FindCriterion::CheckFileSize)mFileSizeQualifiersComboBox->currentItem() : FindCriterion::NONE;
	long long int fileSize = (mFindCriterion.checkFileSize!=FindCriterion::NONE) ? mFileSizeSpinBox->value() : -1;
	if ( fileSize >= 0 ) {
		uint suffixesId = mFileSizeSuffixesComboBox->currentItem();
		if ( suffixesId == 1 )
			fileSize *= 1024; // for KB
		if ( suffixesId == 2 )
			fileSize *= 1073741824; // for MB
		if ( suffixesId == 3 )
			fileSize *= (1073741824*1024); // for GB

		mFindCriterion.fileSize = fileSize;
	}

	QStringList list;
	list = mOwnerComboBox->currentText();
	int uid = LocalFS::getOwners( TRUE, list, FALSE, -1, TRUE );  // get Id for current (in combo) user name
	list = mGroupComboBox->currentText();
	int gid = LocalFS::getOwners( FALSE, list, FALSE, -1, TRUE ); // get Id for current (in combo) group name

	mFindCriterion.checkFileOwner = mFileOwnerChkBox->isChecked();
	mFindCriterion.checkFileGroup = mFileGroupChkBox->isChecked();
	mFindCriterion.ownerId = (mFindCriterion.checkFileOwner) ? uid : -1;
	mFindCriterion.groupId = (mFindCriterion.checkFileGroup) ? gid : -1;

	mFindCriterion.kindOfFiles = (FindCriterion::KindOfFiles)mFiltersComboBox->currentItem();
	mFindCriterion.findRecursive = mFindRecursiveChkBox->isChecked();
	mFindCriterion.searchIntoSelected = mSearchInSelectedChkBox->isChecked();
	mFindCriterion.stopIfXMatched = mStopMatchesChkBox->isChecked();
	mFindCriterion.stopAfterXMaches = (mFindCriterion.stopIfXMatched) ? mMatchesNumSpinBox->value() : -1;

	// --- make an destination action
	mStatusInfoLab->setText( tr("Searching")+"..." );

	emit signalFindStart( mFindCriterion );
	mFindedFilesListView->setFocus();
}

void FindFileDialog::slotFindStop()
{
	emit signalFindStart( FindCriterion() );
	findFinished();
}


void FindFileDialog::slotPause()
{
	emit signalPause( TRUE );

	findFinished();
	mStatusInfoLab->setText( tr("Pause.") );
}


void FindFileDialog::slotFileSize( bool enable )
{
	mFileSizeSpinBox->setEnabled( enable );
	mFileSizeSuffixesComboBox->setEnabled( enable );
	mFileSizeQualifiersComboBox->setEnabled( enable );
}


void FindFileDialog::slotDoubleClickItem( QListViewItem *item )
{
	if ( mFindBtn->isEnabled() ) {
		emit signalJumpToTheFile( item->text(6)+item->text(0) ); // then file location+name
		close();
	}
}


void FindFileDialog::slotClearList()
{
	mFindedFilesListView->clear();
	mStatusDetailsLab->clear();
}

/** Obs�uga zdarzenia zamkni�cia bie��cego okna.\n
 @param ce - zdarzenie zamkni�cia okna.\n
 Uruchamiana jest tu procedura zatrzymania operacji wyszukiwania po czym
 zapisywane s� niekt�re ustawienia (listy: szukanych nazw i lokacji).
 */
void FindFileDialog::closeEvent( QCloseEvent * ce )
{
	slotFindStop();

	// --- save names and locations to the config file
	QSettings settings;

	mNameChooser->saveSettings();
	mLocationChooser->saveSettings(); // save list entries

	// --- close current dialog
	emit signalClose();
	ce->ignore();
}

/** Obsluga klawiatury dla widoku listy znalezionych plik�w.
*/
void FindFileDialog::keyPressEvent( QKeyEvent *e )
{
	if ( ! mFindedFilesListView->hasFocus() ) {
		if ( e->key() == Key_Enter || e->key() == Key_Return )
			slotFindStart();
		return;
	}

	bool findIsEnabled = mFindBtn->isEnabled();
	QListViewItem *item = mFindedFilesListView->currentItem();
	if ( item == NULL )
		return;

	QString fullFileName = item->text(6)+item->text(0);

	switch( e->key() )
	{
	case Key_Enter:
	case Key_Return:
		slotDoubleClickItem( item );
		break;

	case Key_F3:
		if ( findIsEnabled )
			emit signalViewFile( fullFileName, FALSE );
		break;

	case Key_F4:
		if ( findIsEnabled )
			emit signalViewFile( fullFileName, TRUE );
		break;

	case Key_F8:
	case Key_Delete:
		if ( findIsEnabled )
			emit signalDeleteFile( fullFileName );
		break;

	default:
		break;
	}
}


void FindFileDialog::slotRightButtonClickedOnList( QListViewItem * item, const QPoint & pos, int )
{
	if ( ! item || ! mFindBtn->isEnabled() )
		return;

	enum { GOTO=1, VIEW, EDIT, DELETE };

	QPopupMenu menu;
	menu.insertItem( tr("&Go to file")+"\tReturn/Enter", GOTO );
	menu.insertItem( tr("&View file")+"\tF3", VIEW );
	menu.insertItem( tr("&Edit file")+"\tF4", EDIT );
	menu.insertItem( tr("&Delete file")+"\tDelete/F8", DELETE );

	int id = menu.exec( pos );

	QString fullFileName = item->text(6)+item->text(0);

	if ( id == GOTO )
		slotDoubleClickItem( item );
	else if ( id == VIEW )
		emit signalViewFile( fullFileName, FALSE );
	else if ( id == EDIT )
		emit signalViewFile( fullFileName, TRUE );
	else if ( id == DELETE )
		emit signalDeleteFile( fullFileName );
}
