/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include <qdir.h>
#include <qsettings.h>
#include <qfiledialog.h>
#include <qinputdialog.h>


/** Statyczna funkcja pokazuj�ca g��wne okno menad�era URLi i filtr�w.
 @param newURL - nowy URL/filtr wybrany przez u�ytkownika,
 @param kindOfDialog - rodzaj dialogu, dost�pne warto�ci: FTP, HISTORY,
  FAVORITES, FILTERS,
 @param currentPath - bie��ca �cie�ka w panelu (inicjowany jest ni� obiekt
  LocationChooser-a)
 @param parent - wska�nik na rodzica.\n
 Elementy wczytywane s� z pliku konfiguracyjnego po czym wstawiane na list�.
*/
bool UrlsListManager::showDialog( QString & newURL, KindOfDialog kindOfDialog, const QString & currentPath, QWidget *parent )
{
	UrlsListManager *dlg = new UrlsListManager( parent );
	CHECK_PTR( dlg );

	dlg->mPanelSideName = "";
	if ( parent ) {
		dlg->mPanelSideName = parent->name();
		dlg->mPanelSideName = dlg->mPanelSideName.left( dlg->mPanelSideName.find('P') );
	}

	dlg->mCurrentPath = currentPath;

	QString caption;
	if ( kindOfDialog == FAVORITES )
		caption = tr("Favorites Manager");
	else
	if ( kindOfDialog == HISTORY )
		caption = tr("History Manager");
	else
	if ( kindOfDialog == FTP )
		caption = tr("FTP Connections Manager");
	else
	if ( kindOfDialog == FILTERS )
		caption = tr("Filters Manager");
	dlg->setCaption( caption + " - QtCommander" );

	dlg->mOldItemId = -1;
	dlg->mLockStore = TRUE;
	dlg->mLockIfHighlighted = FALSE;
	dlg->mKindOfDialog = kindOfDialog;
	dlg->initURLs(); // reads URLs data from the config file and inserts them on the list

	QString originalPath;
	if ( dlg->mURLsList->count() ) {
		if ( kindOfDialog == HISTORY )
			originalPath = dlg->mURLsStringList[dlg->mURLsList->currentItem()];
		else {
			QChar separator = (dlg->mKindOfDialog == FILTERS) ? '=' : '\\';
			originalPath = dlg->mURLsStringList[dlg->mURLsList->currentItem()].section( separator, 1,1 );
		}
	}
	dlg->mLocationChooser->setOriginalPath( originalPath );
	dlg->mLocationChooser->setCurrentPath( currentPath );

	bool gotoURL = FALSE;
	int  result  = dlg->exec();

	if ( result == QDialog::Accepted )
		dlg->slotSave();
	else
	if ( result == 1111 ) {
		gotoURL = TRUE;
		newURL  = dlg->mNewURL;
	}

	delete dlg;
	dlg = 0;

	return gotoURL;
}

/** Wczytywane s� tutaj, z pliku konfiguracyjnego i wstawiane na list�,
 wszystkie elementy listy danego typu dialogu.
*/
void UrlsListManager::initURLs()
{
	QSettings settings;

	if ( mKindOfDialog == FILTERS )
		mURLsStringList = settings.readListEntry( "/qtcmd/FilesPanel/UserFilters" );
	else
	if ( mKindOfDialog != HISTORY ) {
	// url == "urlName\user:password@host:portdir" // FTP
	// url == "/some/path" // LOCALfs
		uint num    = 0;
		bool empty  = FALSE;
		QString url = "";
		QString kindOfURLstr = (mKindOfDialog==FTP) ? "FTP/S" : "Favorites/Url";

		while( ! empty ) {
			url = settings.readEntry( QString("/qtcmd/"+kindOfURLstr+"%1").arg(num) );
			if ( ! (empty=url.isEmpty()) )
				mURLsStringList.append( url );
			num++;
		}
	}
	else // HISTORY
		mURLsStringList = settings.readListEntry( "/qtcmd/"+mPanelSideName+"FilesPanel/URLs" );

	mAllReadURLs = mURLsStringList.count(); // need to slotSave()

	if ( mKindOfDialog == FILTERS ) {
		mGotoUrlBtn->setText( tr("&Apply") );
		mDirLab->setText( tr("&Pattern :") );
		mDirLab->setBuddy( mLocationChooser );
		mHostLab->hide();   mHostEdit->hide();
		mPortLab->hide();   mPortSpin->hide();
		mUserLab->hide();   mUserEdit->hide();
		mPassLab->hide();   mPasswordEdit->hide();
		mLocationChooser->setButtonSide( LocationChooser::NoneButton );
		mLocationChooser->setGettingWidget( LocationChooser::LineEdit );
	}
	if ( mKindOfDialog == FTP )
		mGotoUrlBtn->setText( tr("Connec&t") );
	if ( mURLsStringList.count() == 0 ) {
		// disable all buttons (except 'New')
		mGotoUrlBtn->setEnabled( FALSE );
		mDeleteBtn->setEnabled( FALSE );
		mRenameBtn->setEnabled( FALSE );
		mDownBtn->setEnabled( FALSE );
		mSaveBtn->setEnabled( FALSE );
		mUpBtn->setEnabled( FALSE );
		// disable all line's edit
		mLocationChooser->setEnabled( FALSE );
		mHostEdit->setEnabled( FALSE );
		mUserEdit->setEnabled( FALSE );
		mPortSpin->setEnabled( FALSE );
		mPasswordEdit->setEnabled( FALSE );
		return;
	}

	QChar separator = (mKindOfDialog == FILTERS) ? '=' : '\\';
	if ( mKindOfDialog != HISTORY ) {
		for( uint i=0; i<mURLsStringList.count(); i++ )
			mURLsList->insertItem( mURLsStringList[i].section( separator, 0,0 ), i );
	}
	else // HISTORY
		mURLsList->insertStringList( mURLsStringList );

	mURLsList->setSelected( 0, TRUE );
}

/** Funkcja z mienia elementy miejscami na liscie oraz w buforze URLi.
*/
void UrlsListManager::changePlacesOfItems( int idItem1, int idItem2 )
{
	int maxIdItem = mURLsList->count()-1;
	bool outOfRange1 = ( idItem1 < 0 || idItem2 > maxIdItem );
	bool outOfRange2 = ( idItem1 > maxIdItem || idItem2 < 0 );

	if ( outOfRange1 || outOfRange2 )
		return;

	saveCurrentUrlDataToList( idItem1 ); // remember current item settings

	QString movableName  = mURLsList->item(idItem1)->text();
	QString replacesName = mURLsList->item(idItem2)->text();

	mURLsList->changeItem( movableName,  idItem2 );
	mURLsList->changeItem( replacesName, idItem1 );

	mURLsList->setSelected( idItem2, TRUE );

	if ( mURLsStringList.isEmpty() )
		return;

	clearAllCells();

// --- change places item1 with item2 into table of URLs data
	movableName  = mURLsStringList[ idItem1 ];
	replacesName = mURLsStringList[ idItem2 ];
	mURLsStringList[ idItem1 ] = replacesName;
	mURLsStringList[ idItem2 ] = movableName;

	slotSelectItem( mURLsList->currentItem() );
}

/** Metoda powoduje zapis danych znajduj�cych si� w polach edycyjnych dialogu
 do bufora (listy) na podanej pozycji.\n
 @param index - pozycja na kt�rej nale�y zapisa� dane.\n
 Je�li pojawi� si� puste pola, wtedy s� one wype�niane domy�lnymi warto�ciami
 (host='localhost', dir='/', user='anonymous',port='21').
*/
void UrlsListManager::saveCurrentUrlDataToList( int index )
{
	QString item, sFTPname;
	QString host = mHostEdit->text();
	QString user = mUserEdit->text();
	QString pass = mPasswordEdit->text();
	QString dir  = mLocationChooser->text();
	QString port = QString::number( mPortSpin->value() );
	QString sn   = mURLsList->text( index ); // section name

	if ( mKindOfDialog == FTP || (mKindOfDialog == FAVORITES && ! dir.isEmpty()) ) {
		if ( dir.at(0) != '/' )
			dir.insert( 0, '/' );
		if ( dir.at(dir.length()-1) != '/' )
			dir += "/";
	}
	if (host.startsWith("ftp://") || host.startsWith("sftp://")) {
		sFTPname = host.left(host.find("//")+2);
		host.remove(0, sFTPname.length()); // remove protocol name
	}

	if ( mKindOfDialog == FTP ) {
		if ( host.isEmpty() )
			host = "localhost";
		if ( user.isEmpty() )
			user = "anonymous";
		if ( port.isEmpty() )
			port = "21";

		uint hostLen = host.length()-1;
		if ( host.at(hostLen) == '/' ) // host name shouldn't have a slash character at the end
			host.truncate( hostLen );

		item = sn+"\\"+"ftp://"+user+":"+pass+"@"+host+":"+port+dir;
	}
	else
	if ( mKindOfDialog == FILTERS )
		item = sn+'='+dir;
	else // FAVORITES (LOCALfs) and HISTORY
	if ( ! dir.isEmpty() ) {
		if ( dir == "~" )
			dir = QDir::homeDirPath()+"/";
		else
		if ( dir.at(0) == '.' && dir.at(1) == '/' ) {
			dir.remove( 0, 2 );
			dir.insert( 0, mCurrentPath );
		}
		else
		if ( dir.at(0) != '/' )
			dir.insert( 0, "/" );
		if ( dir.at(dir.length()-1) != '/' )
			dir += "/";

		if ( mKindOfDialog == HISTORY )
			item = dir;
		else
			item = sn+"\\"+dir;
	}

	mURLsStringList[ index ] = item;
	if ( mOldItemId > -1 ) {
		mURLsStringList[ mOldItemId ] = item;
		mOldItemId = -1;
	}
}

/** Funkcja czy�ci pola edycyjne dialogu, przy czym port dostaje warto�� '21'.
*/
void UrlsListManager::clearAllCells()
{
	mLockIfHighlighted = TRUE;
	mHostEdit->clear();
	mLocationChooser->setText("");
	mUserEdit->clear();
	mPasswordEdit->clear();
	mPortSpin->setValue( 21 );
	mLockIfHighlighted = FALSE;
}

/** Zapisywana jest tutaj, do pliku konfiguracyjnego, bie��cy URL, a nast�pnie
 je�li nie jest to dialog filtr�w to wywo�ywana zostaje funkcja 'done( 1111 )'.
 */
void UrlsListManager::slotGotoUrl()
{
	if ( ! mURLsList->count() || mLocationChooser->text().isEmpty() )
		return;

	int currentItemId = mURLsList->currentItem();
	saveCurrentUrlDataToList( currentItemId );

	if ( mKindOfDialog == HISTORY )
		mNewURL = mURLsStringList[ currentItemId ];
	else {
		QChar separator = (mKindOfDialog == FILTERS) ? '=' : '\\';
		mNewURL = mURLsStringList[ currentItemId ].section( separator, 1,1 );
	}
	if ( mKindOfDialog == FAVORITES || mKindOfDialog == HISTORY || mKindOfDialog == FILTERS )
		if ( mLocationChooser->text().isEmpty() )
			return;

	// save current URL data
	QSettings settings;
	if ( mKindOfDialog != HISTORY ) {
		QString kindOfURLstr = (mKindOfDialog==FTP) ? "FTP/S" : "Favorites/Url";
		settings.writeEntry( QString("/qtcmd/"+kindOfURLstr+"%1").arg(currentItemId),
		 mURLsStringList[currentItemId]
		);
	}
	else
	if ( mKindOfDialog != FILTERS )
		settings.writeEntry( "/qtcmd/"+mPanelSideName+"FilesPanel/URLs", mURLsStringList );

	if ( mKindOfDialog != FILTERS )
		done( 1111 );
}

/** Slot powoduj�cy dodanie na ko�cu listy nowego elementu.\n
*/
void UrlsListManager::slotNew()
{
	mOldItemId  = mURLsList->currentItem();
	int allURLs = mURLsList->count();
	if ( allURLs )
		saveCurrentUrlDataToList( mOldItemId );

	QString urlName, kindOfURLstr;
	if ( mKindOfDialog != HISTORY ) {
		if ( mKindOfDialog == FTP )
			kindOfURLstr = tr("New FTP session");
		else
		if ( mKindOfDialog == FILTERS )
			kindOfURLstr = tr("A new filter");
		else
		if ( mKindOfDialog == FAVORITES )
			kindOfURLstr = tr("A new favorite");

		bool ok = FALSE;
		urlName = QInputDialog::getText(
		kindOfURLstr+" - QtCommander", tr( "Please to give a name:" ),
		QLineEdit::Normal, tr("new name"), &ok, this
		);
		if ( ok == FALSE )
			return;
	}
	else
		urlName = tr("Item")+QString(" %1").arg(mURLsList->count());

	mURLsStringList.append("x");

	clearAllCells();

	mURLsList->insertItem( urlName, allURLs );
	mURLsList->setSelected( allURLs, TRUE );

	if ( mURLsList->count() == 1 ) {
		mGotoUrlBtn->setEnabled( TRUE );
		mDeleteBtn->setEnabled( TRUE );
		mRenameBtn->setEnabled( TRUE );
		mSaveBtn->setEnabled( TRUE );
		// enable line-edits (all for FTP)
		mLocationChooser->setEnabled( TRUE );
		if ( mKindOfDialog == FTP ) {
			mHostEdit->setEnabled( TRUE );
			mUserEdit->setEnabled( TRUE );
			mPortSpin->setEnabled( TRUE );
			mPasswordEdit->setEnabled( TRUE );
		}
	}

	if ( mHostEdit->isEnabled() )
		mHostEdit->setFocus();
	else
		mLocationChooser->setFocus();
}

/** W funkcji usuwany jest bie��cy element listy oraz kasowany w buforze.
*/
void UrlsListManager::slotDelete()
{
	if ( ! mURLsList->selectedItem() || ! mDeleteBtn->isEnabled() )
		return;

	mOldItemId = -1;
	const int currentItemId = mURLsList->currentItem();
	mURLsStringList.remove( mURLsStringList.at(currentItemId) );
	mURLsList->removeItem( currentItemId );

	if ( mURLsList->count() )
		mURLsList->setSelected( mURLsList->currentItem(), TRUE );
	else { // The ListBox obj. is empty
		clearAllCells();
		// disable all buttons (except 'New')
		mGotoUrlBtn->setEnabled( FALSE );
		mDeleteBtn->setEnabled( FALSE );
		mRenameBtn->setEnabled( FALSE );
		mDownBtn->setEnabled( FALSE );
		mSaveBtn->setEnabled( FALSE );
		mUpBtn->setEnabled( FALSE );
		// disable all line-edits
		mLocationChooser->setEnabled( FALSE );
		mHostEdit->setEnabled( FALSE );
		mUserEdit->setEnabled( FALSE );
		mPortSpin->setEnabled( FALSE );
		mPasswordEdit->setEnabled( FALSE );
	}

	slotSelectItem( mURLsList->currentItem() );
}

/** Slot powoduje zmian� nazwy (na li�cie oraz w buforze) bie��cego elementu.
*/
void UrlsListManager::slotRename()
{
	if (! mRenameBtn->isEnabled())
		return;

	QString kindOfURLstr;
	if ( mKindOfDialog == FTP )
		kindOfURLstr = tr("Rename FTP session");
	else
	if ( mKindOfDialog == FILTERS )
		kindOfURLstr = tr("Rename a filter");
	else
	if ( mKindOfDialog == FAVORITES )
		kindOfURLstr = tr("Rename a favorite");

	bool ok = FALSE;
	QString urlName = QInputDialog::getText(
	 kindOfURLstr+" - QtCommander", tr("Please to give a name:"),
	 QLineEdit::Normal, mURLsList->selectedItem()->text(), &ok, this
	);
	if ( ! ok )
		return;

	int currentItemId = mURLsList->currentItem();
	mURLsList->changeItem( urlName, currentItemId );
	debug("UrlsListManager::slotRename(), call saveCurrentUrlDataToList()");
	saveCurrentUrlDataToList( currentItemId );
}

/** Metoda powoduje przeniesienie bie��cego elementu (na li�cie oraz w buforze)
 o jedn� pozycj� do g�ry.
 */
void UrlsListManager::slotMoveUp()
{
	changePlacesOfItems( mURLsList->currentItem(), mURLsList->currentItem()-1 );
}

/** Metoda powoduje przeniesienie bie��cego elementu (na li�cie oraz w buforze)
 o jedn� pozycj� w d�.
 */
void UrlsListManager::slotMoveDown()
{
	changePlacesOfItems( mURLsList->currentItem(), mURLsList->currentItem()+1 );
}

/** Slot wywo�ywany w momencie pod�wietlenia elementu listy.\n
 @param indexItem - index elementu do po�wietlenia.\n
 Ustawiana jest tutaj aktywno�� guzik�w g�ra/d�, a tak�e wype�niane s� pola
 edycyjne dla danych wybranej sesji. Dane te brane s� z bufora o indeksie
 @em indexItem.
 */
void UrlsListManager::slotSelectItem( int indexItem )
{
	if ( indexItem < 0 )
		return;

	// --- enable/disable buttons used to an item moving
	bool upEnabled = TRUE, downEnabled = TRUE;
	if ( indexItem == 0 ) {
		upEnabled   = FALSE;
		downEnabled = TRUE;
	}
	else
	if ( (uint)indexItem == mURLsList->count()-1 ) {
		upEnabled   = TRUE;
		downEnabled = FALSE;
	}
	if ( mURLsList->count() == 1 ) {
		upEnabled = FALSE,  downEnabled = FALSE;
	}
	mUpBtn->setEnabled( upEnabled );
	mDownBtn->setEnabled( downEnabled );

	if ( mURLsStringList.count() == 0 )
		return;

	if ( mOldItemId > -1 && ! mLockStore ) // auto-save
		saveCurrentUrlDataToList( mOldItemId );

	clearAllCells();

	QString url = mURLsStringList[ indexItem ];
	if ( url.isEmpty() || url == "x" )
		return;

	QString dir;

	if ( mKindOfDialog == FTP ) {
		QString sHost, sUser, sPass;
		uint nPort = 21;
		// --- parse url
		if ( url.find("\\")+1 )
			url = url.section( "\\", 1,1 ); // get URL (part with right side of separator)
		if (url.find("ftp://") >= 0)
			url.remove( 0, url.find("/")+2);

		//debug("url='%s'", url.latin1());
		int nColonPos = url.find(':');
		if (nColonPos == 0) // user name not found
			sUser = "anonymous";
		sUser = url.left(nColonPos);
		int nMonkeyPos = url.find('@', nColonPos);
		if (nMonkeyPos > 0) // password found
			sPass = url.mid(nColonPos+1, nMonkeyPos-(nColonPos+1));
		//debug("pass=%s, nColonPos=%d, nMonkeyPos=%d", sPass.latin1(), nColonPos, nMonkeyPos);
		nColonPos = url.find(':', nMonkeyPos);
		if (nColonPos > 0) { // host found
			sHost = url.mid(nMonkeyPos+1, nColonPos-(nMonkeyPos+1));
			//debug("host=%s, nColonPos=%d, nMonkeyPos=%d", sHost.latin1(), nColonPos, nMonkeyPos);
			int nSlashPos = url.find('/', nColonPos);
			if (nSlashPos > 0) { // found port
				nPort = url.mid(nColonPos+1, nSlashPos-(nColonPos+1)).toInt();
				if (nPort == 0)
					nPort = 21;
				dir = url.right(url.length()-nSlashPos);
				//debug("dir=%s", dir.latin1());
			}
		}

		// init cells
		mLockIfHighlighted = TRUE;
		mHostEdit->setText( sHost );
		mUserEdit->setText( sUser );
		mPasswordEdit->setText( sPass );
		mPortSpin->setValue( nPort );
		mLockIfHighlighted = FALSE;
	}
	else
	if ( mKindOfDialog == FAVORITES || mKindOfDialog == FILTERS ) {
		QChar separator = (mKindOfDialog == FILTERS) ? '=' : '\\';
		dir = url.section( separator, 1,1 ); // get URL (part with right side of separator)
	}
	else // HISTORY
		dir = url;

	mLockIfHighlighted = TRUE;
	mLocationChooser->setText( dir );
	mLockIfHighlighted = FALSE;

	if ( mHostEdit->text().isEmpty() ) { // LOCALfs
		mHostEdit->setEnabled( FALSE );
		mUserEdit->setEnabled( FALSE );
		mPortSpin->setEnabled( FALSE );
		mPasswordEdit->setEnabled( FALSE );
	}
	if ( mKindOfDialog == HISTORY )
		mRenameBtn->hide();
}


void UrlsListManager::slotTextChanged( const QString & )
{
	// enable automate save current changed text if highlighting changed
	mOldItemId = (! mLockIfHighlighted) ? mURLsList->currentItem() : -1;
	mLockStore = (mOldItemId == -1);
}


/** W funkcji wykonywany jest zapis do pliku konfiguracyjnego, wszystkich
 element�w.
 */
void UrlsListManager::slotSave()
{
	if ( ! mURLsList->count() )
		return;

	saveCurrentUrlDataToList( mOldItemId=mURLsList->currentItem() );

	QSettings settings;
	QString kindOfURLstr = (mKindOfDialog==FTP) ? "FTP/S" : "Favorites/Url";

	// --- remove all reads (in init fun. of current dialog) URLs
	if ( mKindOfDialog != HISTORY && mKindOfDialog != FILTERS ) {
		for( uint i=0; i<mAllReadURLs; i++ )
			settings.removeEntry( QString("/qtcmd/"+kindOfURLstr+"%1").arg(i) );
	}

	// --- save all URLs
	if ( mKindOfDialog == FILTERS )
		settings.writeEntry( "/qtcmd/FilesPanel/UserFilters", mURLsStringList );
	else
	if ( mKindOfDialog != HISTORY ) {
		for( uint i=0; i<mURLsStringList.count(); i++ )
			settings.writeEntry( QString("/qtcmd/"+kindOfURLstr+"%1").arg(i), mURLsStringList[i] );
	}
	else
		settings.writeEntry( "/qtcmd/"+mPanelSideName+"FilesPanel/URLs", mURLsStringList );
}

/** Przechwytywane s� tutaj wszelkie wci�ni�te klawisze, gdy aktywna jest lista.\n
 @param e - zdarzenie wci�ni�tego klawisza.\n
 Wci�ni�cie Insert powoduje uruchomienie obs�ugi wstawienia nowego elementu.
 Klawisz Delete usuwa bie��cy element.
 */
void UrlsListManager::keyPressEvent( QKeyEvent *e )
{
	if ( ! mURLsList->hasFocus() ) {
		QDialog::keyPressEvent( e );
		return;
	}

	switch ( e->key() ) {
		case Key_Insert:
			slotNew();
			break;

		case Key_Delete:
			slotDelete();
			break;

		case Qt::Key_F2:
			slotRename();
			break;

		default:
			QDialog::keyPressEvent( e );
			break;
	}
}
