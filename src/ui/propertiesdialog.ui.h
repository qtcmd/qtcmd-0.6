/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include "filters.h"
#include "localfs.h" // for getOwners()
#include "functions.h"
#include "messagebox.h"
#include "fileinfoext.h"
#include "squeezelabel.h"

#include <sys/stat.h> // for constans: S_ISUID, S_ISGID, S_ISVTX

// TODO
// 1. Kiedy zaznaczonych jest kilka plikow umozliwic zmiane atrybutow dla kazego z osobna.

void PropertiesDialog::init( const QStringList & fileNameList, FilesAssociation *fa, const QString & currentLoggedUserName )
{
	setCaption( tr("The file(s) properties")+" - QtCommander" );
	mLoggedUserName = currentLoggedUserName;
	mFilesAssociation = NULL;

	mNameComboBox->clear();
	if ( fileNameList.count() > 1 )
		mNameComboBox->insertItem( tr("ALL SELECTED") );
	mNameComboBox->insertStringList( fileNameList );
	initFiltersAndAssociation( fa );
	slotInitAllTabs( mNameComboBox->text(0) );

	mStopBtn->setEnabled( FALSE );
	mCancelBtn->setAccel( Key_Escape );
	if ( mNameComboBox->lineEdit() )
		connect( mNameComboBox->lineEdit(), SIGNAL(returnPressed()), this, SLOT(slotOkPressed()) );

	emit signalRefresh(mNameComboBox->currentItem()-1);
}

void PropertiesDialog::init( const UrlInfoExt & urlInfo, FilesAssociation *fa, const QString & currentLoggedUserName )
{
	initFiltersAndAssociation( fa );
	mNameComboBox->setCurrentText( urlInfo.name() );
	mLoggedUserName = currentLoggedUserName;
}

void PropertiesDialog::initFiltersAndAssociation( FilesAssociation *fa )
{
	mFilesAssociation = fa;
	Filters *filters = new Filters( /*fa*/ );
	for ( uint i=0; i<filters->baseFiltersNum(); i++ )
		mFiltersComboBox->insertItem( filters->name(i) );
	delete filters;
}

void PropertiesDialog::setSize( long long weight, int dirs, int files, bool bigInfo )
{
	QString sizeInfo = formatNumber(weight,BKBMBGBformat)+" ("+formatNumber(weight,THREEDIGITSformat)+")";
	if (weight == 0)
		sizeInfo = "? B (? B)";

	if ( bigInfo )
		sizeInfo += "\n"+QString::number(dirs) +" "+tr("directories and")+" "+
				QString::number(files)+" "+tr("files");

	mSizeLab->setText( sizeInfo );
}


int PropertiesDialog::permissions()
{
	int perm = 0;

	if ( mReadUserChkBox->isChecked() )   perm += QFileInfo::ReadUser;
	if ( mReadGroupChkBox->isChecked() )  perm += QFileInfo::ReadGroup;
	if ( mReadOtherChkBox->isChecked() )  perm += QFileInfo::ReadOther;
	if ( mWriteUserChkBox->isChecked() )  perm += QFileInfo::WriteUser;
	if ( mWriteGroupChkBox->isChecked() ) perm += QFileInfo::WriteGroup;
	if ( mWriteOtherChkBox->isChecked() ) perm += QFileInfo::WriteOther;
	if ( mExeUserChkBox->isChecked() )    perm += QFileInfo::ExeUser;
	if ( mExeGroupChkBox->isChecked() )   perm += QFileInfo::ExeGroup;
	if ( mExeOtherChkBox->isChecked() )   perm += QFileInfo::ExeOther;

	if ( mSuidChkBox->isChecked() ) perm += S_ISUID;
	if ( mSgidChkBox->isChecked() ) perm += S_ISGID;
	if ( mSvtxChkBox->isChecked() ) perm += S_ISVTX;

	return perm;
}

bool PropertiesDialog::isRecursive()
{
	return mChangeRecursiveChkBox->isChecked();
}

int PropertiesDialog::currentItem()
{
	return mNameComboBox->currentItem();
}


void PropertiesDialog::slotInitAllTabs( const QString & fileName )
{
	QString typeInfo, location;
	bool allSelected = FALSE;
	uint files = 0, dirs = 0;
	long long weight = 0;
	UrlInfoExt urlInfo;

	/*if ( mNameComboBox->count() > 1 )
		if ( mNameComboBox->currentItem() == 0 ) // allSelected
			allSelected = TRUE;*/
	if ( fileName == tr("ALL SELECTED") ) {
		allSelected = TRUE;
		for( int i=1; i<mNameComboBox->count(); i++ ) {
			if ( urlInfo.isSymLink() )
				files++;
			else
				( urlInfo.isDir() ) ? dirs++ : files++;
			emit signalGetUrlForFile( mNameComboBox->text(i), urlInfo );
			if ( ! urlInfo.isDir() )
				weight += urlInfo.size();
		}

		typeInfo = tr("files");
		location = FileInfoExt::filePath( mNameComboBox->text(1) );
		emit signalGetUrlForFile( mNameComboBox->text(1), urlInfo ); // for access and modified time
		mNameComboBox->setEditable( FALSE );
		//slotRefresh();
	}
	else { // the single file or directory
		emit signalGetUrlForFile( fileName, urlInfo );
		location = FileInfoExt::filePath( fileName );
		weight = urlInfo.size();

		if ( urlInfo.isDir() && ! urlInfo.isSymLink() ) {
			typeInfo = tr("directory");
			dirs = 1;
			//slotRefresh(); // starts weighing
		}
		else {
			if ( mFilesAssociation )
				typeInfo = mFilesAssociation->fileDescription( fileName );
			if ( typeInfo.isEmpty() || ! mFilesAssociation )
				typeInfo = (urlInfo.isSymLink()) ? tr("symbolic/hard link") : tr("file");
			weight = urlInfo.linkTarget().length();
			files = 1;
		}
		mNameComboBox->setEditable( TRUE );
	}

	(typeInfo == tr("directory")) ? mRefreshBtn->show() : mRefreshBtn->hide();
	mNameComboBox->setFocus();

	// --- init first tab
	mLocationLab->setText( location );
	mTypeLab->setText( typeInfo );
	setSize( weight, dirs, files, TRUE );
	//setSize( weight, dirs, files, (allSelected || (urlInfo.isDir() && ! urlInfo.isSymLink())) );

	mLastAccessTimeEdit->setDateTime( urlInfo.lastRead() );
	mLastModifiedTimeEdit->setDateTime( urlInfo.lastModified() );

	if ( ! allSelected ) {
		mNameComboBox->setCurrentText( FileInfoExt::fileName(urlInfo.name()) );
	}
	// --- init second tab

		// - clear all permissions
	mClassUserChkBox->setChecked( FALSE );
	mClassGroupChkBox->setChecked( FALSE );
	mClassOtherChkBox->setChecked( FALSE );
	slotInitPermissonForClass( 0 );
	slotInitPermissonForClass( 1 );
	slotInitPermissonForClass( 2 );
	mSuidChkBox->setChecked( FALSE );
	mSgidChkBox->setChecked( FALSE );
	mSvtxChkBox->setChecked( FALSE );

		// - init permissions
	if ( allSelected ) {
		mReadBtnsGroup->setButton( 0 );
		mReadBtnsGroup->setButton( 1 );
		mReadBtnsGroup->setButton( 2 );  // read for all
		mWriteBtnsGroup->setButton( 0 ); // write for user
	}
	else {
		int perm = urlInfo.permissions();
		if ( perm & QFileInfo::ReadUser )   mReadBtnsGroup->setButton( 0 );
		if ( perm & QFileInfo::WriteUser )  mWriteBtnsGroup->setButton( 0 );
		if ( perm & QFileInfo::ExeUser )    mExeBtnsGroup->setButton( 0 );
		if ( perm & QFileInfo::ReadGroup )  mReadBtnsGroup->setButton( 1 );
		if ( perm & QFileInfo::WriteGroup ) mWriteBtnsGroup->setButton( 1 );
		if ( perm & QFileInfo::ExeGroup )   mExeBtnsGroup->setButton( 1 );
		if ( perm & QFileInfo::ReadOther )  mReadBtnsGroup->setButton( 2 );
		if ( perm & QFileInfo::WriteOther ) mWriteBtnsGroup->setButton( 2 );
		if ( perm & QFileInfo::ExeOther )   mExeBtnsGroup->setButton( 2 );

		if ( perm & S_ISUID )  mSpecialBtnsGroup->setButton( 0 );
		if ( perm & S_ISGID )  mSpecialBtnsGroup->setButton( 1 );
		if ( perm & S_ISVTX )  mSpecialBtnsGroup->setButton( 2 );
	}

	// --- init user and group combo
	QStringList list;

	LocalFS::getOwners( TRUE, list, TRUE );  // get all users
	mOwnerComboBox->clear();
	mOwnerComboBox->insertStringList( list );

	LocalFS::getOwners( FALSE, list, TRUE ); // get all groups
	mGroupComboBox->clear();
	mGroupComboBox->insertStringList( list );

	QString owner, group;
	if ( allSelected ) {
		LocalFS::getOwners( TRUE, list, FALSE );  // get current logged user name
		owner = list[0];
		LocalFS::getOwners( FALSE, list, FALSE );  // get current logged user's group name
		group = list[0];
	}
	else {
		owner = urlInfo.owner();
		group = urlInfo.group();
	}
//	bool enableOwnerChanges = (owner == mLoggedUserName || mLoggedUserName == "root");

	bool found = FALSE;
	for( int i=0; i<mGroupComboBox->count(); i++)
		if ( mGroupComboBox->text(i) == group ) {
			found = TRUE;
			break;
		}
	if ( ! found ) // not found group
		mGroupComboBox->insertItem( group );

	mOwnerComboBox->setCurrentText( owner );
	mGroupComboBox->setCurrentText( group );

	// --- set third Tab

	mChangeRecursiveChkBox->setEnabled( dirs );
	// TODO mChangeRecursiveChkBox domyslnie ustawic na nieaktywne, i uaktywaniac gdy ktorys
	// z chkBoxow (mChangePermissionChkBox, mChangeOwnerChkBoxm, mChangeTimeChkBox) jest wlaczany
	//mChangeRecursiveChkBox->setEnabled( FALSE );
	mTabWidget->setTabEnabled( mTabWidget->page(2), (urlInfo.isDir() || allSelected) );

	// --- other settings
/* // nie mozna tutaj pobrac wlasciciela, ani innych atrybutow dla katalogu rodzica
	bool changeAttributsAllowed = TRUE; // except group, it always allowed

	if ( ! allSelected ) {
		emit signalGetUrlForFile( FileInfoExt::filePath(fileName), urlInfo );

		if ( urlInfo.owner() != mLoggedUserName )
			changeAttributsAllowed = FALSE;
	}

	mChangeTimeChkBox->setEnabled( changeAttributsAllowed );
	mChangePermissionChkBox->setEnabled( changeAttributsAllowed );
*/
}

/**
*/
void PropertiesDialog::slotInitPermissonForClass( int permClass )
{
	bool isChecked = ((QCheckBox *)mClassBtnsGroup->find( permClass ))->isChecked();

	((QCheckBox *)mReadBtnsGroup->find( permClass ))->setChecked( isChecked );
	((QCheckBox *)mWriteBtnsGroup->find( permClass ))->setChecked( isChecked );
	((QCheckBox *)mExeBtnsGroup->find( permClass ))->setChecked( isChecked );
}

/**
*/
void PropertiesDialog::slotRefresh()
{
	mSizeLab->setText( tr("Weighing")+"...\n\n" );
	emit signalRefresh(mNameComboBox->currentItem()-1);
}

void PropertiesDialog::slotStop()
{
//	mSizeLab->setText( tr("Stop weighing")+".\n\n" );
//	emit signalStop();
}

/** Funkcja wywo�ywana po wci�ni�ciu guzika OK.
*/
void PropertiesDialog::slotOkPressed()
{
	//QString currName  = FileInfoExt::filePath(mNameComboBox->text(mNameComboBox->currentItem()))+mNameComboBox->currentText();

	//QString loggedUserName = LocalFS::getUser();  // get current logged user name
	//QString loggedGroupName = LocalFS::getUser( FALSE );  // get current logged user's group name
	QString currName  = FileInfoExt::fileName(mNameComboBox->currentText());
	QString currOwner = mOwnerComboBox->text(mOwnerComboBox->currentItem());
	QString currGroup = mGroupComboBox->text(mGroupComboBox->currentItem());
	QDateTime currModifiedTime = mLastModifiedTimeEdit->dateTime();
	QDateTime currAccessTime   = mLastAccessTimeEdit->dateTime();
	int currPermission = permissions();

	bool allSelected = FALSE;
	if ( mNameComboBox->count() > 1 )
		if ( mNameComboBox->currentItem() == 0 ) // allSelected
			allSelected = TRUE;
	//bool allSelected = (mNameComboBox->currentText() == tr("ALL SELECTED")) ? TRUE : FALSE;

	// --- check whether an attribut has been modified
	QDateTime nullDateTime;

	if ( ! allSelected ) {
		UrlInfoExt urlInfo;
		emit signalGetUrlForFile( mNameComboBox->text(mNameComboBox->currentItem()), urlInfo );
		QString newName = FileInfoExt::fileName(urlInfo.name());

		if ( currName == newName )
			currName = "";
		if ( currPermission == urlInfo.permissions() )
			currPermission = -1;
		if ( currOwner == urlInfo.owner() )
			currOwner = "";
		if ( currGroup == urlInfo.group() )
			currGroup = "";
		if ( currAccessTime == urlInfo.lastRead() )
			currAccessTime = nullDateTime;
		if ( currModifiedTime == urlInfo.lastModified() )
			currModifiedTime = nullDateTime;
	}
	else // allSelected is true, name must be empty
		currName = "";

	if ( ! mChangeOwnerChkBox->isChecked() )
		currOwner = currGroup = "";
	if ( ! mChangeTimeChkBox->isChecked() )
		currAccessTime = currModifiedTime = nullDateTime;
	if ( ! mChangePermissionChkBox->isChecked() )
		currPermission = -1;


	// --- make temporary the 'UrlInfoExt' object
	UrlInfoExt URLinfo(
	 currName, 0, currPermission, currOwner, currGroup, // size (0) is dummy
	 currModifiedTime, currAccessTime,
	 FALSE, FALSE, FALSE, // there following attributs: isDir, isFile, isSymLink are dummy
	 FALSE, FALSE // there following attributs: isReadable, isExecutable are dummy
	);

	emit signalOkPressed( URLinfo, allSelected );
}


void PropertiesDialog::closeEvent( QCloseEvent * e )
{
	emit signalClose();
	e->ignore();
}


bool PropertiesDialog::changePermission()
{
	return (mChangePermissionChkBox->isEnabled()) ? mChangePermissionChkBox->isChecked() : FALSE;
}


bool PropertiesDialog::changeOwnerAndGroup()
{
	return (mChangeOwnerChkBox->isEnabled()) ? mChangeOwnerChkBox->isChecked() : FALSE;
}


bool PropertiesDialog::changeTime()
{
	return (mChangeTimeChkBox->isEnabled()) ? mChangeTimeChkBox->isChecked() : FALSE;
}


int PropertiesDialog::changesFor()
{
	return mFiltersComboBox->currentItem();
	// changes for All
	// ... for files only
	// ... for dirs only
	// SPRAWDZIC JAK W KLASIE OBSLUGUJACE WYSZUKIWANIE
	// szukac wywolania changesFor()
}


void PropertiesDialog::slotActivatedName( const QString & fullName )
{
	emit signalRefresh(mNameComboBox->currentItem()-1);
	/*
	if ( mNameComboBox->count() > 1 )
		if ( mNameComboBox->currentItem() > 0 ) // not allSelected
			mNameComboBox->setCurrentText( FileInfoExt::fileName(fullName) );
	*/
}


void PropertiesDialog::setEnableAllChanges( bool enable )
{
	mChangeTimeChkBox->setEnabled( enable );
	mChangePermissionChkBox->setEnabled( enable );
	//mChangeOwnerChkBox->setEnabled( enable );
}
