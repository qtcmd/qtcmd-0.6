/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include "squeezelabel.h"

#include <qtooltip.h>


int FileOverwriteDialog::show( QWidget * parent, const UrlInfoExt & targetFileURL, const UrlInfoExt & sourceFileURL )
{
	FileOverwriteDialog *dlg = new FileOverwriteDialog( parent );
	CHECK_PTR( dlg );

	dlg->setCaption( tr("File overwriting")+" - QtCommander" );

	dlg->mTargetFileNameLab->setText( "\""+targetFileURL.name()+"\"" );
	dlg->mSourceDateLab->setText( sourceFileURL.lastModifiedStr() );
	dlg->mTargetDateLab->setText( targetFileURL.lastModifiedStr() );
	dlg->mSourceSizeLab->setText( sourceFileURL.sizeStr(BKBMBGBformat) );
	dlg->mTargetSizeLab->setText( targetFileURL.sizeStr(BKBMBGBformat) );

	QToolTip::add( dlg->mSourceSizeLab, sourceFileURL.sizeStr(THREEDIGITSformat) );
	QToolTip::add( dlg->mTargetSizeLab, targetFileURL.sizeStr(THREEDIGITSformat) );

	// --- show dialog
	int result = dlg->exec();

	delete dlg;
	dlg = 0;

	return result;
}
