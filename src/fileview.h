/***************************************************************************
                          fileview.h  -  description
                             -------------------
    begin                : sun oct 27 2002
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
***************************************************************************/

#ifndef _FILEVIEW_H_
#define _FILEVIEW_H_

#include <qtimer.h>
#include <qaccel.h>
#include <qaction.h>
#include <qlibrary.h>
#include <qmenubar.h>
#include <qtoolbar.h>
#include <qsplitter.h>
#include <qlistview.h>
#include <qtoolbutton.h>
#include <qstringlist.h>

#include "plugins/view/text/view.h"
#include "keyshortcuts.h"
#include "fileinfoext.h"
#include "statusbar.h"
#include "virtualfs.h" // for #include "filesassociation.h"

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Bazowa klasa obs�uguj�ca podgl�d pliku.
 Jej obiekt tworzy okno g��wne podgl�du, paskiem menu, narz�dziowym, pasek
 statusu a tak�e list� boczn� z list� plik�w wybranych przez u�ytkownika do
 podgl�du. Specyficzne dla danego podgl�du funkcje przedstawione w menu
 i na pasku narz�dziowym, s� pobierane z aktualnego obiektu podgl�gu pliku.
 Jej g��wnym zadaniem jest �adowanie pluginu wybranego i inicjowanie jego
 obiektu. Po�redniczy r�wnie� w �adowaniu pliku do pogl�du, wykorzystuj�c
 do tego celu sygna��w (powinny by� one po��czone z obiektem systemu plik�w
 z panelu, z kt�rego uruchamianyjest podgl�d). Klasa zawiera obs�ug�
 podstawowych funkcji z menu, g��wnego, np. zmian� rodzaju widoku - uruchamiaj�c
 odpowiedni� procedur� w obiekcie konkretnego podgl�du. Zarz�dza r�wnie�
 boczn� (chowan�) list� plik�w, �aduj�c do niej nazwy i uruchamiaj�c podgl�d
 po wybraniu jednego z plik�w.
*/
class FileView : public QWidget
{
	Q_OBJECT
public:
	/** Konstruktor klasy podgl�du pliku.\n
	 @param filesList - lista plik�w (ich nazwy s� umieszczne na tzw.bocznej
	 li�cie),
	 @param currentItem - numer bie��cej nazwy pliku na li�cie @em filesList ,
	 @param editMode - czy podgl�d w trybie edycji,
	 @param parent - rodzic obiektu,
	 @param name - nazwa obieku.\n
	 Inicjowane s� tutaj niezb�dne sk�adowe klasy, inicjowany og�lny widok, tworzone
	 obiekty wykorzystywane przez klas�, inicjowane paski: menu i narz�dziowy.
	 */
	FileView( const QStringList & filesList, uint currentItem, bool editMode=FALSE, QWidget *parent=NULL, const char *name=0 );

	/** Destruktor klasy.\n
	 Wy�adowywany jest tutaj plugin ustawie� podgl�du oraz plugin bie��cego
	 podgl�du, a tak�e usuwane s� wszystkie obiekty u�ywane wewn�trz.
	 */
	~FileView();

	/** Metoda ustawia wska�nik obiektu skoja�e� plik�w na podany.\n
	 @param fa - wska�nik obiektu skoja�e� plik�w.
	 */
	void setFilesAssociation( FilesAssociation *fa ) { mFilesAssociation = fa; }

	/** Metoda ustawia wska�nik obiektu skr�t�w klawiszowych na podany.\n
	 @param keyShortcuts - wska�nik obiektu skr�t�w klawiszowych.
	 */
	void setKeyShortcuts( KeyShortcuts * keyShortcuts ) { mKeyShortcuts = keyShortcuts; }

	/** Zwraca �cie�k� do aktualnie otwartego pliku.
	 @return absolutna �cie�ka do aktualnie otwartego pliku.
	 */
	QString currentURL() const { return FileInfoExt::filePath(mCurrentFileName); }

	/**
	 Zwraca nazw� aktualnie otwartego pliku.\n
	 @return nazwa z absolutn� �cie�k� do aktualnie otwartego pliku.
	 */
	QString fileName() const  { return mCurrentFileName; }

	/** Zwraca rodzaj bie��cego widoku.\n
	 @return rodzaj widoku, patrz te�: @see KindOfView
	 */
	int kindOfView() const { return (int)mKindOfView; }

	/** Metoda s�u�y do ustawienia rodzaju widoku dla bie��cego pliku na podany.\n
	 @param kindOfView - rodzaj widoku, patrz te�: @see KindOfView.\n
	 Wywo�ywana jest tu tylko metoda @em createView().
	 */
	void setNewKindOfView( View::KindOfView kindOfView );

	/** Metoda powoduje wstawienie na tzw.boczn� liste widoku podanej listy nazw.\n
	 @param filesList - lista nazwa plik�w do wstawienia.\n
	 Metoda ta jest wywo�ywana z konstruktora.
	 */
	void insertFilesList( const QStringList & filesList );

private:
	View      *mView;
	QSplitter *mSplitter;
	QWidget   *mViewParent;
	StatusBar *mStatus;
	QListView *mSideListView;
	QLibrary  *mSettingsLib, *mViewLib;
	QWidget   *mSettingsWidget;

	FilesAssociation *mFilesAssociation;

	KindOfFile mKindOfFile;
	View::KindOfView mKindOfView;
	View::ModeOfView mModeOfView;

	QString mCurrentFileName, mInitFileName, mCurrentDirPath;

	bool m_bViewInWindow, m_bInitEditMode;
	QMenuBar    *mMenuBar;
	QPopupMenu  *mFileMenu, *mViewMenu, *mEditMenu, *mOptionsMenu;
	QPopupMenu  *mModeOfViewMenu, *mRecentFilesMenu;
	QAction     *mSaveA, *mSideListA;
	QToolBar    *mToolBar, *mAddonToolBar;

	QByteArray mBinBuffer;

	QTimer *mTimer;
	KeyShortcuts *mKeyShortcuts;
	QAction *mNewA, *mOpenA, *mReloadA, *mMenuBarA, *mToolBarA, *mConfigureA;
	QToolButton *mOpenBtn;

	uint mMB_Height, mTB_Height, mSB_Height;
	bool m_bShowMenuBar, m_bShowToolBar, m_bShowTheFilesList;
	uint mInitWidth, mInitHeigth, mSideListWidth;
	bool m_bItIsTextFile;

	QString mPluginsPath;
	QStringList mPluginsNameList;

	int mBytesRead;
	int mFileLoadMethod;
	bool m_bDetectIsBinOrTextFile;

	/** Identyfikatory akcji skr�t�w klawiszowych dla okna podgl�du.
	*/
	enum ActionsOfView {
		Help_VIEW=0,
		Open_VIEW,
		Save_VIEW,
		SaveAs_VIEW,
		Reload_VIEW,
		OpenInNewWindow_VIEW,
		ShowHideMenuBar_VIEW,
		ShowHideToolBar_VIEW,
		ShowHideSideList_VIEW,
		ToggleToRawMode_VIEW,
		ToggleToEditMode_VIEW,
		ToggleToRenderMode_VIEW,
		ToggleToHexMode_VIEW,
		ShowSettings_VIEW,
		Quit_VIEW
	};


	/** Funkcja powoduje uaktualnienie paska menu oraz narz�dziowego w zale�no�ci
	 od bie��cego rodzaju widoku oraz trybu podgl�du.
	 */
	void updateMenuBar();

	/** Inicjowane jest tutaj menu oraz pasek narz�dziowy.\n
	 @param parent - rodzic podgl�du.\n
	 Pokazywany lub chowany jest tak�e pasek menu oraz narz�dziowy (w zale�no�ci
	 od informacji odczytanych w funkcji @em initView() ).
	*/
	void initMenuBarAndToolBar( QWidget * parent );

	/** Metoda powoduje utworzenie obiektu skr�t�w klawiszowych oraz
	 zainicjowanie jego listy domy�lnymi warto�ciami.
	 */
	void initKeyShortcuts();

	/** W funkcji inicjowany jest og�lny widok podgl�du.\n
	 Wczytywane i ustawiane s� nast�puj�ce w�a�ciwo�ci: rozmiar okna (je�li to
	 widok w oknie), informacje o pokazywniu paska menu oraz narz�dziowego.
	 Nast�puje tu tak�� wykrycie �cie�ki do plugin�w (obs�uga konkretnych
	 podgl�d�w).
	*/
	void initView();

	/** Metoda powoduje usuni�cie obiektu plugina-podgl�du oraz wy�adowanie
	 plugina.
	 */
	void removeView();

protected:
	/** Metoda tworzy pusty widok podanego rodzaju, j�li rodzaj zostanie
	 pomini�ty, wtedy jest on odgadywany na podstawie rozszerzenia podanego pliku.\n
	 @param fileName - nazwa pliku wykorzystywana przy wykrywaniu rodzaju widoku;
	 @param kindOfView - rodzaj widoku do utworzenia (podanie UNKNOWNview, wymusza
	 jego detekcj� na podstawie rozszerzenia podanego pliku);
	 @return - TRUE, je�li widok zosta� pomy�lnie utworzony, w przeciwnym razie
	 FALSE.\n
	 Aby utworzy� nowy widok �adowany i uruchamiany jest okre�lony plugin widoku.
	 Inicjowana jest tutaj tak�e, sk�adowa @em mKindOfFile.
	 */
	bool createView( const QString & fileName, View::KindOfView kindOfView=View::UNKNOWNview );

	/** Metoda powoduje uruchomienie tzw. �adowania pliku z op�nieniem.\n
	 @param fileName - nazwa pliku do za�adowania.\n
	 W metodzie inicjowana jest sk�adowa @em mInitFileName oraz wywo�ywany slot:
	 @em slotReadyRead() .
	 */
	void loadingLate( const QString & fileName );


	/** Przechwytywane s� tutaj wszelkie wci�ni�cia klawiszy.\n
	 Funkcja zawiera tylko obs�ug� klawisza Escape, kt�ry s�u�y do zamkni�cia
	 okna.
	 */
	void keyPressEvent( QKeyEvent * );

	/** Funkcja wywo�ywana w momencie zmiany rozmiaru podgl�du.\n
	 Uaktualniane s� tutaj pozycje i rozmiary obiekt�w okna podgl�du.
	*/
	void resizeEvent( QResizeEvent * );

	/** Funkcja wywo�ywana w momencie zamkni�cia podgl�du.\n
	 Wykorzystywana jest do sprawdzania czy zawarto�� widoku zosta�a zmieniona,
	 je�li tak, wtedy nast�puje zapytanie urzytkownika o zapis do pliku.
	 */
	void closeEvent( QCloseEvent * );

	QString kindOfViewStr( View::KindOfView ); // FUN.TYMCZASOWA - TYLKO DO TESTOW

public slots:
	/** W slocie inicjowane jest wczytywanie pliku.\n
	 Nast�puje tu pokazanie okna (je�li to widok w oknie) oraz pokazanie lub
	 schowanie bocznej listy. Je�li wczytywany plik le�y w lokalnym systemie
	 plik�w, wtedy wywo�ywany jest asynchroniczne slot @em slotLoadFile() ,
	 w przeciwnym razie wywo�ywany jest slot @em slotLoadFile() (tutaj
	 wczytywaniem zajmuje si� obiekt klasy FileSystemu).
	 */
	void slotReadyRead();

private slots:
	/** Slot przygotowywuje podgl�d do wczytania pierwszego fragmentu (lub
	 ca�o�ci) pliku.\n
	 Nazwa wczytywanego pliku musi si� znajdowa� w sk�adowej @em mInitFileName ,
	 poniewa� inicjuje ona z koleji sk�adow� @em mCurrentFileName . Ustawiany jest
	 tutaj maksymalny rozmiar wczytywanego na raz bloku. Nast�pnie nast�puje
	 wywo�anie asynchroniczne slotu @em slotReadFile() . Na ko�cu inicjowana
	 jest boczna lista plik�w.
	 */
	void slotLoadFile();

	/** W slocie nast�puje (asynchroniczne) wczytywanie (partiami w ca�o�ci) pliku
	 oraz uaktualnianie bie��cego obiektu podgl�du.\n
	 Po wczytaniu pierwszego fragmentu lub ca�o�ci (w zale�no�ci od ustawionej
	 wcze�niej metody wczytywania pliku) wykrywane jest, czy to plik tekstowy czy
	 binarny, a nast�pnie tworzony w�a�ciwy obiekt podgl�du, po czym uaktualniane
	 menu i pasek narz�dziowy. Na ko�cu uaktualniany jest obiekt podgl�du.\n
	 �adowanie pliku nast�puje w spos�b asynchroniczny, je�li plik le�y w lokalnym
	 systemie plik�w. Wczytywaniem zajmuj� si� sloty w odpowiednich systemach
	 plik�w, komunikacja wykonywana jest za pomoc� sygna�u @em signalReadFile() ,
	 kt�rego parametrami s�: nazwa wczytywanego pliku, adres bufora, rozmiar
	 wczytywanego (na raz) bloku bajt�w oraz metoda �adowania -
	 @em mFileLoadMethod .
	 */
	void slotReadFile();

	/** Slot powoduje ustawienie podanego trybu podgl�du dla bie��cego widoku.\n
	 @param itemId - numer trybu podgl�du (w menu), musi by� ona zgodna
	 z typem @em View::ModeOfView .
	 */
	void slotChangeModeOfView( int itemId );

	/** Slow wywo�ywany w momencie wybrananie przez u�ytkownika pliku z menu
	 ostatnio za�adowanych plik�w.\n
	 @param itemId - numer nazwy pliku w menu.\n
	 Podejmuje pr�b� wczytania pliku o wybranej nazwie.
	 */
	void slotItemOfRecentFilesMenuActivated( int itemId );


	/** Slot powoduje pokazanie dialogu �adowania pliku.\n
	 Wybrany plik jest wczytywany do nowo utworzonego podgl�du.
	 */
	void slotNew();

	/** Slot powoduje pokazanie dialogu �adowania pliku.\n
	 Wybrany plik jest wczytywany do bie��cego podgl�du.
	 */
	void slotOpen();

	/** Slot zapisuje podgl�d do bie��cego pliku, tylko je�li zosta� zmieniony.
	 */
	void slotSave();

	/** Slot pokazuje dialog zapisu pliku, w celu zapisania bie��cego pod podan�
	 tam nazw�.
	 */
	void slotSaveAs();

	/** Slot powoduje ponowne za�adowanie aktualnie wczytanego pliku.
	 */
	void slotReload();


	/** Slot powoduje pokazanie lub schowanie paska menu.\n
	 Przy czym, osadzony w panelu podgl�d ma zawsze ukrywane menu.
	 */
	void slotShowHideMenuBar();

	/** Slot powoduje pokazanie lub schowanie paska narz�dziowego.
	 */
	void slotShowHideToolBar();

	/** Slot powoduje pokazanie ustawie� dla podgl�du.\n
	 �adowany jest w tym celu plugin. Je�li operacja zako�czy si� sukcesem, wtedy
	 ��czone s� niezb�dne sygna�y ze slotami.
	 */
	void slotShowSettings();

	/** W slocie wczytywane s� z pliku konfiguracyjnego nazwy plik�w pokazywane na
	 bocznej li�cie.\n
	 Po wczytaniu nast�puje wstawienie ich na boczn� list�.
	 */
	void slotLoadFilesList();

	/** W slocie zapisywana jest do pliku konfiguracyjnego lista nazw plik�w
	 z bocznej listy.
	 */
	void slotSaveFilesList();

	/** Slot powoduje pokazanie lub ukrycie (w zale�no�ci od warto�ci sk�adowej
	 @em mShowTheFilesList) bocznej listy plik�w.
	 */
	void slotShowTheFilesList();

	/** Slot powoduje za�adowanie klikni�tego na bocznej li�cie pliku.\n
	 @param item - wska�nik elementu bocznej listy.
	 */
	void slotOpenCurrentItem( QListViewItem * item );

	/** Slot zaimplementowany tylko dla wykonania poprawnego ��czenie sygna�u ze
	 slotem.\n Wewn�trz wywo�ywany jest tylko slot @em slotOpenCurrentItem() .
	 */
	void slotOpenCurrentItem( QListViewItem * item, const QPoint & , int );

	/** Slot powoduje zastosowanie ustawie� skr�t�w klawiszowych dla g��wnego
	 okna podgl�du (bie��cej klasy).\n
	 @param keyShortcuts - wska�nik na obiekt skr�t�w klawiszowych.
	 */
	void slotApplyKeyShortcuts( KeyShortcuts * keyShortcuts );

signals:
	/** Sygna� u�ywany do asynchronicznego wczytywania pliku.\n
	 @param fileName - nazwa wczytywanego pliku,
	 @param buffer - adres bufora, w kt�rym nale�y umieszcza� wczytywane dane,
	 @param bytesRead - zmienna, w kt�rej zapisywana b�dzie ilo�� wczytanych
	 bajt�w, zawiera informacj� o maksymalnej ilo�ci bajt�w do wczytania.
	 @param fileLoadMethod - metoda wczytywania pliku, patrz te�:
	  @see View::UpdateMode.\n
	 Sygna� powinien by� po��czony ze slotem w obiekcie klasy okre�lonego systemu
	 plik�w.
	 */
	void signalReadFile( const QString & fileName, QByteArray & buffer, int & bytesRead, int fileLoadMethod );

	/** Sygna� s�u�y do ustawienia informacji o bie��cym widoku w obiekcie
	 ustawie�, (jest on sk�adow� tej klasy). W obiekcie ustawie� informacja ta
	 jest u�ywana przy zastosowywaniu ustawie� dla bie��cego widoku.\n
	 @param currentKindOfView - rodzaj widoku, patrz te�: @see View::KindOfView
	 */
	void signalSetCurrentKindOfView( View::KindOfView currentKindOfView );

	/** Sygna� s�u�y do ustawienia informacji o skr�tach klawiszowych w obiekcie
	 ustawie�, (jest on sk�adow� tej klasy).\n
	 @param keyShortcuts - wska�nik na obiekt skr�t�w klawiszowych.
	 */
	void signalSetKeyShortcuts( KeyShortcuts * keyShortcuts );

	/** Sygna� wysy�any po operacji zapisu pliku i s�u�y do poinformowania panela
	 o potrzebie uaktualniania informacji o tym pliku na li�cie.\n
	 @param fileName - nazwa pliku, kt�ry zosta� zapisany
	 */
	void signalUpdateItemOnLV( const QString & fileName );

	/** Sygna� wysy�any w momencie wci�ni�cia guzika zamkni�cia widoku, kt�ry
	znajduje si� na pasku narz�dziowy.\n
	Guzik zamykania pokazywany jest tylk je�li widok jest osadzony w panelu.
	 */
	void signalClose();

	/** Sygna� wysy�any w momencie zapisu ustawie� ka�dego typu podgl�du.
	 */
	void signalRereadFilesAssociation();

};

#endif
