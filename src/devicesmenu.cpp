	/***************************************************************************
                          devices.cpp  -  description
                             -------------------
    begin                : Fri May 30 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 $Id$
 ***************************************************************************/

#include "../icons/notmounted.xpm"
#include "../icons/mounted.xpm"

#include "devicesmenu.h"
#include "messagebox.h"
#include "solf.h"

#include <qfile.h>
#include <qtimer.h>
#include <qlabel.h>
#include <qregexp.h>
#include <qtooltip.h>
#include <qpushbutton.h>


DevicesMenu::DevicesMenu( QWidget *pParent )
	: QPopupMenu( pParent ), m_pInfoDlg(NULL)
{
	m_pProcess = new QProcess( this );
	connect( m_pProcess, SIGNAL(processExited()), this, SLOT(slotProcessExited()) );
}

DevicesMenu::~DevicesMenu()
{
	delete m_pProcess;
	if (m_pInfoDlg != NULL)
		delete m_pInfoDlg;
}


bool DevicesMenu::init()
{
	QFile file;
	QString sMTabBuf;

	file.setName( "/etc/mtab" );
	if ( file.open( IO_ReadOnly ) ) {
		QTextStream stream( &file );
		sMTabBuf = stream.read(); // read entire stream
		file.close();
	}
	else {
		MessageBox::critical( this, tr("Cannot open file")+": /etc/mtab");
		return FALSE;
	}

	enum { DEV=0, PATH, FS };

	uint i = 1, itHeight = 0, currHeight = 0, id = -1;
	static const char **iconName;
	bool bIsMounted;
	QPopupMenu *pSubMenu = NULL;
	char cCharId = 'a';
	QString sPrefix, sMenuTitle;
	long long freeBytes, allBytes;
	QString freeBytesStr, totalBytesStr;
	QString sLine, sFStabFileBuffer;
	QStringList slFStabLine;

	file.setName( "/etc/fstab" );
	if (! file.open( IO_ReadOnly ) ) {
		MessageBox::critical(this, tr("Cannot open file")+": /etc/fstab");
		return FALSE;
	}
	else {
		QTextStream stream( &file );
		m_vlMountedTab.clear();
		m_vlMountedTab.append(FALSE); // becouse we begin with 1 (i==1)
		while (! stream.eof()) {
			sLine = stream.readLine(); // sLine of text excluding '\n'
			if (sLine.find("swap") < 0) {
				if (sLine.startsWith("UUID=", FALSE) || sLine.startsWith("/dev", FALSE))
					sFStabFileBuffer += sLine + "\n";
			}
		}
		//debug("sFStabFileBuffer=\n%s", sFStabFileBuffer.latin1());
		file.close();
	}

	if (sFStabFileBuffer.length() > 0) {
		// -- read directory /dev/disk/by-uuid
		QDir dir("/dev/disk/by-uuid/");
		dir.setFilter(QDir::Files | QDir::System);

		QFileInfo *pFileInfo;
		QString sFileName;
		QFileInfoList *fiList = (QFileInfoList *)dir.entryInfoList();

		for (uint i = 0; i < fiList->count(); i++) {
			pFileInfo = fiList->at(i);
			pFileInfo->convertToAbs();
			if (pFileInfo->isSymLink()) {
				sFileName = dir.cleanDirPath(dir.absPath()+"/"+pFileInfo->readLink());
				//debug("-- fileName= %s,\treadLink= %s,\tsFileName=%s",  pFileInfo->fileName().latin1(), pFileInfo->readLink().latin1(), sFileName.latin1());
				sFStabFileBuffer.replace(pFileInfo->fileName(), sFileName);
				sFStabFileBuffer.replace("UUID=", "", FALSE);
			}
		}
	}
	QStringList slFStab = QStringList::split("\n", sFStabFileBuffer);
	//debug("sFStabFileBuffer (count=%d)=\n%s", slFStab.count()-1, sFStabFileBuffer.latin1());

	clear(); // menu (current object)

	for (uint it=0; it<slFStab.count()-1; it++) {
		sLine = slFStab[it];
		slFStabLine = QStringList::split(QRegExp("[\\s][\\t]*"), sLine); // split by space and tab
		//debug("slFStabLine[DEV]=%s", slFStabLine[DEV].latin1());
		bIsMounted  = sMTabBuf.find(slFStabLine[DEV])+1;
		iconName = (bIsMounted) ? mounted : notmounted;
		m_vlMountedTab.append(bIsMounted);

		if (cCharId == 'z'+1) // TODO add support to character-id for more than 25 devices (maybe to use digits)
			cCharId = 'a';
		sPrefix = "&"+QString((QChar)cCharId)+"  ";
		cCharId++;
		sMenuTitle = slFStabLine[PATH];

		if ( bIsMounted ) {
			//debug("slFStabLine[PATH]=%s", slFStabLine[PATH].latin1());
			// get device capacity and free space
			SoLF::getStatFS( sMenuTitle, freeBytes, allBytes );
			totalBytesStr = formatNumber( allBytes,  BKBMBGBformat );
			freeBytesStr  = formatNumber( freeBytes, BKBMBGBformat );

			sMenuTitle = sPrefix + slFStabLine[PATH] + "\t( "+totalBytesStr+" / "+freeBytesStr+" )";
			// create menu items
			if (slFStabLine[PATH] != "/" && slFStabLine[PATH] != "/usr" && slFStabLine[PATH] != "/var") {
				id = 1000+i;
				pSubMenu = new QPopupMenu( this );
				pSubMenu->insertItem( tr("Open"), id );
				pSubMenu->insertItem( tr("Unmount"), id+100 );
				insertItem( QIconSet(iconName), sMenuTitle, pSubMenu, i++ );
			}
			else
				insertItem( QIconSet(iconName), sMenuTitle, i++ );
		}
		else
			insertItem( QIconSet(iconName), sMenuTitle, i++ );

		//debug("sizeHint()=%d", sizeHint());
		// --- adds toolTip with more info ( device and file system )
		itHeight = (currHeight == 0) ? 21 : sizeHint().height()/(i-1);
		//debug("------- currHeight=%d, itHeight=%d", currHeight, itHeight);
		// WARNING: tip works correct only when cursor is over a text
		QToolTip::add( this, QRect( 0, currHeight, width(), itHeight ),
					   slFStabLine[DEV]+"  "+slFStabLine[FS]+"  "+tr("(capacity/free space)")
		);
		currHeight = sizeHint().height();
	}

	return TRUE;
}


void DevicesMenu::showMenu()
{
	if (! init())
		return;

	int itemId = exec( parentWidget()->mapToGlobal(QPoint(3,3)) );
	if ( itemId == -1 )
		return;

	bool needUnmount = (itemId > 1100) ? TRUE : FALSE;
	bool bIsMounted = TRUE;

	if ( itemId >= 1000 )
		itemId -= 1000;
	if ( needUnmount )
		itemId -= 100;

	bIsMounted = m_vlMountedTab[itemId];
	QString path = text( itemId );
	//debug("DevicesMenu::showMenu(), itemId= %d, path= %s", itemId, path.latin1());
	path = path.right( path.length()-(path.find(' ')+1)-1 );

	m_sMountPath = path;
	if ( ! bIsMounted ) {
		m_eCurrentOperation = MOUNT;
		action();
	}
	else
	if ( needUnmount ) {
		m_eCurrentOperation = UNMOUNT;
		action();
	}
	else
		emit signalPathChanged( path );
}


void DevicesMenu::runProcess( Operation op, const QString & args )
{
	QString processName = (op==MOUNT) ? "mount" : "umount";

	QString process = processName+" "+args;
	//debug("DevicesMenu::runProcess(), exec=%s", process.latin1() );

	m_pProcess->addArgument( processName );
	m_pProcess->addArgument( args );

	if ( ! m_pProcess->start() )
		MessageBox::critical( this, tr("Could not start")+" :\n"+process );
}


void DevicesMenu::action()
{
	if ( m_eCurrentOperation == MOUNT )
		QTimer::singleShot( 0, this, SLOT(slotMount()) );
	else
	if ( m_eCurrentOperation == UNMOUNT )
		QTimer::singleShot( 0, this, SLOT(slotUnmount()) );

	// --- show info dialog
	QString action = (m_eCurrentOperation==MOUNT) ? tr("Mounting") : tr("Unmounting");
	QString msg = tr("Please wait.")+"\n"+action+" "+tr("still is process")+"...";

	if ( m_pInfoDlg != NULL ) {
		delete m_pInfoDlg;  m_pInfoDlg = NULL;
	}
	m_pInfoDlg = new QDialog( this );
	m_pInfoDlg->setCaption( action+" - QtCommander");
	// dialog must be unresizeable
	m_pInfoDlg->setMinimumSize( 290, 140 );
	m_pInfoDlg->setMaximumSize( 290, 140 );

	QLabel *lab = new QLabel( msg, m_pInfoDlg );
	lab->setGeometry( 20, 30, 250, 40 );
	QPushButton *btn = new QPushButton( tr("&Break"), m_pInfoDlg );
	btn->move( m_pInfoDlg->width()/2-btn->width()/2, m_pInfoDlg->height()-btn->height()-15 );
	connect( btn, SIGNAL(clicked()), this, SLOT(slotBreakProcess()) );

	m_bActionIsFinished = FALSE;
	m_pInfoDlg->exec();
	while ( ! m_bActionIsFinished )
		m_pInfoDlg->exec();
}


void DevicesMenu::slotUnmount()
{
	runProcess( UNMOUNT, m_sMountPath );
}


void DevicesMenu::slotMount()
{
	runProcess( MOUNT, m_sMountPath );
}


void DevicesMenu::slotProcessExited()
{
	m_bActionIsFinished = TRUE;
	if ( m_pInfoDlg != NULL ) {
		delete m_pInfoDlg;  m_pInfoDlg = NULL;
	}

	int exitStat = m_pProcess->exitStatus();
	//debug("DevicesMenu::slotProcessExited(), exitStat=%d, normalExit=%d", exitStat, m_pProcess->normalExit() );

	if ( m_pProcess->normalExit() && exitStat == 0 )
		emit signalPathChanged( m_sMountPath );
	else { // error occures
		QString errorStr;
		if ( exitStat == 32 ) // mount
			errorStr = tr("No medium found.");
		else
		if ( exitStat == 1 || exitStat == 2 ) // umount
			errorStr = tr("Device is busy.");
// 		else
// 		if ( exitStat == 2 ) // umount
// 			errorStr = tr("Device is not mounted (according to mtab)");
		else
		if ( exitStat != 0 )
			errorStr = tr("Error no.")+" "+QString::number(exitStat);

		QString action = (m_eCurrentOperation == MOUNT) ? tr("mount") : tr("umount");
		MessageBox::critical( this, tr("Could not")+" "+action+" "+tr("device")+"\n\n"+errorStr );
	}
}


void DevicesMenu::slotBreakProcess()
{
	if ( ! m_pProcess->normalExit() ) {
		m_pProcess->kill();
		m_bActionIsFinished = TRUE;
		delete m_pInfoDlg; m_pInfoDlg = NULL;
	}
}
