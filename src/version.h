/***************************************************************************
                          version  -  description
                             -------------------
    begin                : nie gru 11 2005
    copyright            : (C) 2005 by Mariusz Borowski
    email                : b0mar@gnes.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef VERSION_H
#define VERSION_H

#define VERSION "0.6.4"
#define AUTHOR "Piotr Mierzwiński"
#define AUTHORMAIL "peterm@go2.pl"

#endif
