/***************************************************************************
                          archivefs.cpp  -  description
                             -------------------
    begin                : tue dec 23 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

//#include "rar.h"
#include "tar.h"
//#include "zip.h"
#include "archivefs.h"
#include "messagebox.h"
#include "fileinfoext.h"

#include <qdir.h>
#include <qsettings.h>
#include <qapplication.h>


ArchiveFS::ArchiveFS( const QString & inputURL, ListViewExt *listViewExt )
	: VirtualFS( inputURL, listViewExt )
{
	KindOfArchive arch = FileInfoExt::kindOfArchive( inputURL );
	debug("ArchiveFS::ArchiveFS, arch=%d", arch );

// 	if ( arch == RARarch )
// 		mVirtualArch = new Rar( this );
// 	else
	if ( arch == TARarch || arch == TGZIParch || arch == TBZIP2arch || arch == GZIPcompr || arch == BZIP2compr )
		mVirtualArch = new Tar( this, arch );
// 	else
// 	if ( kindOfArch == ZIParch )
// 		mVirtualArch = new Zip( this );
	else
		debug("__ archive '%s' not supported !", QFileInfo(inputURL).extension().latin1() );

	if ( mVirtualArch == NULL )
		return;

	mVirtualArch->setArchiveName( inputURL );
	debug("__setArchiveName, archiveName=%s", mVirtualArch->archiveName().latin1() );

	QSettings *settings = new QSettings;
	QString workDirectory = settings->readEntry( "/qtcmd/Archives/WorkingDirectory", QDir::homeDirPath()+"/tmp/qtcmd" );
	if ( workDirectory.at(workDirectory.length()-1) != '/' )
		workDirectory += "/";
	delete settings;

	mVirtualArch->setWorkDirectory( workDirectory );

	connect( mVirtualArch, SIGNAL(done(bool)),                 SLOT(slotDone(bool)) );
	connect( mVirtualArch, SIGNAL(stateChanged(int)),          SLOT(slotStateChanged(int)) );
	connect( mVirtualArch, SIGNAL(commandStarted(int)),        SLOT(slotCommandStarted(int)) );
	connect( mVirtualArch, SIGNAL(commandFinished(int,bool)),  SLOT(slotCommandFinished(int,bool)) );
//	connect( mVirtualArch, SIGNAL(listInfo(const UrlInfoExt &)), SIGNAL(signalListInfo(const UrlInfoExt &)) );
	connect( mVirtualArch, SIGNAL(listInfo(const UrlInfoExt &)), SLOT(slotListInfo(const UrlInfoExt &)) );
}


ArchiveFS::~ArchiveFS()
{
	delete mVirtualArch;
}


bool ArchiveFS::archiveIsSupported( const QString & fileName )
{
	QString fName = FileInfoExt::archiveFullName( fileName );
	if ( fName.isEmpty() )
		return FALSE;

	const uint maxArch = 3;
	bool supported = FALSE;
	KindOfArchive currentArchive = FileInfoExt::kindOfArchive(fName);
	KindOfArchive supportedArchTab[maxArch] = { TARarch, TBZIP2arch, TGZIParch };

	for (uint i=0; i<maxArch; i++)
		if ( supportedArchTab[i] == currentArchive ) {
			supported = TRUE;
			break;
		}

	return supported;
}


DirWritable ArchiveFS::currentDirWritable()
{
	// spr. czy kat. w ktorym lezy archimum jest zapisywalny
	// czy plik archiwum ma ust. "w" dla wlasciciela

	//mVirtualArch->archiveName()
	// ENABLEdw, DISABLEdw, UNKNOWNdw
	return UNKNOWNdw;
}


void ArchiveFS::readDir( const QString & fullPathName )
{
	mVirtualArch->list( fullPathName );
	if ( mVirtualArch->errorCode() == NoError )
		mSkipNextErrorOccures = FALSE;
}

		// ------- SLOTS ---------
/*
void ArchiveFS::slotPause( bool stop )
{
}


void ArchiveFS::slotReadFileToView( const QString & fileName, QByteArray & buffer )
{
}
*/


void ArchiveFS::slotStateChanged( int state )
{
	switch ( (VirtualArch::ArchState)state ) {
		case VirtualArch::Connecting:
			emit signalSetInfo( tr("Reading an archive") );
			break;
		case VirtualArch::Listing:
			emit signalSetInfo( tr("Listing an archive") );
			break;
/*	case QFtp::Unconnected:
			emit signalSetInfo( tr("Unconnected") );
			break;
		case QFtp::HostLookup:
			emit signalSetInfo( tr("Host lookup") );
			break;
		case QFtp::Connected:
			emit signalSetInfo( tr("Connected") );
			break;
		case QFtp::LoggedIn:
			emit signalSetInfo( tr("Logged in as")+" "+mUserName );
			break;
		case QFtp::Closing:
			emit signalSetInfo( tr("Closing") );
			break;*/
			default:
			break;
	}
}


void ArchiveFS::slotCommandStarted( int command )
{
// 	debug("ArchiveFS::slotCommandStarted(), operation=%s, command=%s", cmdString(mSoLF->currentOperation()).latin1(), cmdString((Operation)command).latin1() );
	debug("ArchiveFS::slotCommandStarted, command=%s", cmdString((Operation)command).latin1() );

	if ( command == List ) { // if ( currentOp == List )  {
		//debug("__ start listing dir=%s", (host()+mVirtualArch->nameOfProcessedFile()).latin1() );
		// absHostURL() - zwraca tylko sciezke, po wejsciu do archiwum
		debug("__ start listing absHostURL=%s, nameOfProcessedFile=%s", absHostURL().latin1(), nameOfProcessedFile().latin1() );
		QApplication::setOverrideCursor( waitCursor );
		initializeShowsList(); // min. ustawia zmienna korzenia dla drzewa
	}
}


void ArchiveFS::slotCommandFinished( int command, bool error )
{
	debug("ArchiveFS::slotCommandFinished, command=%s, errorStat=%d", cmdString((Operation)command).latin1(), error );

	if ( error ) {
		if ( command != Connect )
			slotDone( error );
		return;
	}
	// po wejsciu do archiwum (po Connect) nameOfProcessedFile jest rowny arc://nazwa_arch.roz/
	debug("__ absHostURL()=%s, nameOfProcessedFile()=%s", absHostURL().latin1(), mVirtualArch->nameOfProcessedFile().latin1() );
	if ( command == Connect )
		setCurrentURL( mVirtualArch->nameOfProcessedFile()+"/" );
	else
	if ( command == Cd || command == List )
		setCurrentURL( absHostURL() ); // mSkipNextErrorOccures = FALSE;
// 	else
// 	if ( command ==  )
}


void ArchiveFS::slotDone( bool error )
{
	QApplication::restoreOverrideCursor();
	Operation currentOp = mVirtualArch->currentOperation();
	debug("ArchiveFS::slotDone(), errorStat=%d, operation=%s", error, cmdString(currentOp).latin1() );
	// po wejsciu do archiwum (po Connect) nameOfProcessedFile jest rowny arc://nazwa_arch.roz/
	//debug("ArchiveFS::slotDone(), errorStat=%d, errorNo=%d, operation=%s", error, mSoLF->errorNumber(), cmdString(currentOp).latin1() );
	if ( error && currentOp == Connect ) { // cannot open an archive
		MessageBox::critical( this,
		 tr("Can not open an archive")+"\n\n"+mVirtualArch->nameOfProcessedFile()
		);
		return; // no return made CRASH, becouse FS will be removed and later is call to not exists method from Tar class
	}
	startProgressDlgTimer( FALSE ); // stop it
/*
	if ( ! error )
		;
	else
		;
*/
// 		mVirtualArch->setState( VirtualArch::Opened ); // state powinien byc tutaj tylko lapany przez sygnal
	emit signalResultOperation( currentOp, !error );
}
