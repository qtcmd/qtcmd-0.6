/***************************************************************************
                          combopath.cpp  -  description
                             -------------------
    begin                : Tue Aug 14 2001
    copyright            : (C) 2001 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "combopath.h"
#include "listviewext.h"

#include <qlineedit.h>


ComboPath::ComboPath( QListView *ptrOfList, unsigned int maxItems, bool rw, QWidget *parent, const char *name )
	:QComboBox(rw, parent,name), mPtrOfList(ptrOfList)
{
	setInsertionPolicy( QComboBox::AtTop );
	setDuplicatesEnabled( FALSE );
	setTotalItems( maxItems );
	setEditable( TRUE );

	connect( this, SIGNAL(activated(const QString &)),  this, SLOT(slotTextChanged(const QString &)) );
}


void ComboPath::setTotalItems( unsigned int maxItems )
{
	if ( maxItems == 0 ) {
		debug("ComboPath::setTotalItems, 0 is illegal value !");
		return;
	}
	if ( maxItems > 100 )  maxItems = 100;

	setMaxCount( maxItems );
}


void ComboPath::insertPath( const QString & pathName )
{
	if ( pathName.at(pathName.length()-1) != '/' )
		return;

	int id = find( pathName );
	if ( id < 0 )
		insertItem( pathName, nextItemId() );

	setCurrentItem( ( id < 0 ) ? nextItemId() : id );

	if ( count() > maxCount() ) // if too much then
		removeItem( maxCount() ); // cut last one

	mStr = mOldStr = pathName;
}


void ComboPath::removePath( const QString & pathName )
{
	int id = -1;
	while ((id=find( pathName )) > 0) {
		removeItem( id );
	}
}


int ComboPath::find( const QString & pathName ) const
{
	int  id;
	bool found = FALSE;

	for (id=0; id<count(); id++ ) {
		if ( text(id) == pathName ) {
			found = TRUE;
			break;
		}
	}

	return (found) ? id : -1;
}

		// ---------- SLOTs ------------

void ComboPath::slotReturnPressed( const QString & pathName )
{
	mStr = pathName;
	if ( mStr == "ftp:/" || mStr == "smb:/" || mStr == "ftp://" || mStr == "smb://" )
		return;

	if ( mStr != "~" ) {
		if ( mStr.at(mStr.length()-1) != '/' )
			mStr += "/";
	}

	mRestoreOldPath = (mStr.find("ftp://") != 0);

	emit signalPathChanged( mStr );
}


void ComboPath::slotTextChanged( const QString & txt )
{
	if ( txt.find( "\n" ) != -1 || txt.find( "\t" ) != -1 )  {
		setEditText( mOldStr );
		return;
	}

	bool ftpOrSmbStrIsOnBegin = (txt.find("ftp:/") == 0 || txt.find("smb:/") == 0 );
	bool isOnlyFtpOrSmbStr = ( txt == "ftp:///" || txt == "smb:///"); // block return behind insert theird slash

	if ( (txt.find( "//" ) != -1 && !ftpOrSmbStrIsOnBegin ) || isOnlyFtpOrSmbStr
	 || txt.at( 0 ) ==  '.' || txt.at( 1 ) == '.'  || (txt.at( 0 ) == '/' && txt.at( 1 ) == '.')  )  {
		setEditText( txt.left( txt.length() - 1 )  );
		return;
	}

	pathAutoCompletion( txt );

	mStr = currentText();
	if ( mStr.at(mStr.length()-1) == '/' )
		slotReturnPressed( mStr );
}


void ComboPath::pathAutoCompletion( const QString & path )
{
	QString lastName = path;

	if ( path != "/" )
		lastName = path.right( path.length() - path.findRev( '/', path.length()-1 )-1 );

	QString currentName = listContainsItem( lastName );

	if ( ! currentName.isEmpty() )  {
		int highlightLen = currentName.length() - lastName.length();
		lineEdit()->insert( currentName.right(highlightLen) );
		lineEdit()->cursorForward( TRUE, - highlightLen );
	}
}


QString ComboPath::listContainsItem( const QString & itemName )
{
	if ( itemName.isEmpty() )
		return itemName;

	bool found = FALSE;
	bool treeView = ((ListViewExt *)mPtrOfList)->isTreeView();

	QListViewItem *parentOfCurrItem, *currItem;
	QListViewItemIterator it = mPtrOfList;

	if ( treeView ) {
		parentOfCurrItem = mPtrOfList->currentItem()->parent();
		if ( parentOfCurrItem )
			if ( ! parentOfCurrItem->text(0).isEmpty() )
				it = parentOfCurrItem;
		it++;
	}

	while (1) {
		currItem = it.current();
		if ( treeView ) {
			if ( currItem ) {
				if ( currItem->parent() != parentOfCurrItem )
					break;
			}
			else
				break;
		}
		else
			if ( currItem == 0 )
				break;

		if ( ! ((ListViewItemExt *)currItem)->isDir() ) {
			++it;
			continue;
		}

		mStr = currItem->text(0).left( itemName.length() );
		if ( mStr.contains(itemName) ) {
			mStr = currItem->text(0);
			found = TRUE;
			break;
		}
		++it;
	}

	return found ? mStr : QString("");
}


void ComboPath::keyPressEvent( QKeyEvent *e )
{
	if ( e->state() == ControlButton ) {
		QComboBox::keyPressEvent( e );
		return;
	}

	switch ( e->key() )
	{
		case Key_Return:
		case Key_Enter:
		{
			slotReturnPressed( currentText() );
			break;
		}

		case Key_Backspace:
		case Key_Delete:
		case Key_Left:
		case Key_Right:
		case Key_Home:
		case Key_End:
		case Key_Tab:
		{
			QComboBox::keyPressEvent( e );
			break;
		}

		case Key_Up:
		case Key_Down:
		{
			if ( lineEdit()->hasSelectedText() )
				return;

			slotTextChanged( currentText() );
			QComboBox::keyPressEvent( e );
			break;
		}

		case Key_Escape:
		{
			mPtrOfList->setFocus();
			break;
		}

		default:
			if ( ! e->text().isEmpty() )  {
				lineEdit()->insert( e->text() );
				slotTextChanged( currentText()  );
			}
			break;
		}
}


bool ComboPath::eventFilter( QObject *o, QEvent *e )
{
	if ( ! mPtrOfList || ! lineEdit() )
		return QComboBox::eventFilter( o, e );    // standard event processing

	if ( mPtrOfList->childCount() == 0 )
		return QComboBox::eventFilter( o, e );    // standard event processing

	if ( e->type() == QEvent::FocusOut && o == lineEdit() && mRestoreOldPath )
		setEditText( mOldStr );
	else
	if ( e->type() == QEvent::FocusIn && o == lineEdit() )
		setEditText( mStr );

	return QComboBox::eventFilter( o, e );    // standard event processing
}
