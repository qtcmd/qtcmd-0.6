/***************************************************************************
                          pathview.cpp  -  description
                             -------------------
    begin                : wed oct 23 2002
    copyright            : (C) 2002 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include <qdir.h>
#include <qtooltip.h>
#include <qsettings.h>
#include <qlineedit.h>

#include "../icons/home.xpm"
#include "../icons/root.xpm"
#include "../icons/cdup.xpm"
#include "../icons/next.xpm"
#include "../icons/previous.xpm"

#include "urlslistmanager.h"
#include "fileinfoext.h"
#include "messagebox.h"
#include "pathview.h"

/* TODO dodac do menu opcje "Set Default Action" z menu (z checkBox)
 a w nim wszystkie akcje do zmiany biezacego kat. Ustawienie ktorejs opcji
 spowoduje ze pojedyncze klikniecie w guzik bedzie wywolywalo te opcje.
 Domyslna niech bedzie "cd up".
*/

PathView::PathView( QListView *ptrOfList, QWidget *parent, const char *name )
 : QWidget( parent, name )
{
	mQuickCdMenu   = NULL;
	mButtonQuickCD = NULL;
	mComboPathView = NULL;

	setMaximumHeight( 27 );

// 	mCdMenu = new QPopupMenu( mViewMenu );
// 	mCdMenu->setCheckable( TRUE );
// elementy zrobic jako QAction a potem dodac je do grupy, grupe mozna dodac do
// mCdMenu oraz do mQuickCdMenu

// Do menu dodac opcje: 'choose dir'

	mQuickCdMenu = new QPopupMenu();
	mQuickCdMenu->insertTearOffHandle();
	mQuickCdMenu->insertItem( QPixmap((const char**)home), tr("Go to &home directory"),
	 this, SLOT(slotChangeDirToUserHome()) );
	mQuickCdMenu->insertItem( QPixmap((const char**)root), tr("Go to &root directory"),
	 this, SLOT(slotChangeDirToRoot()) );
	mQuickCdMenu->insertItem( QPixmap((const char**)cdup), tr("Go to &up directory"),
	 this, SLOT(slotChangeDirToUp()) );
	mQuickCdMenu->insertSeparator();
	mQuickCdMenu->insertItem( QPixmap((const char**)previous), tr("Go to &previous directory"),
	 this, SLOT(slotChangeDirToPrev()) );
	mQuickCdMenu->insertItem( QPixmap((const char**)next), tr("Go to &next directory"),
	 this, SLOT(slotChangeDirToNext()) );
	mQuickCdMenu->insertSeparator();
	mQuickCdMenu->insertItem( tr("&Edit history"),  this, SLOT(slotEditHistory()) );
	mQuickCdMenu->insertItem( tr("&Clear history"),  this, SLOT(slotClearHistoryPath()) );
	mQuickCdMenu->insertItem( tr("C&lear current"),  this, SLOT(slotClearPath()) );
// 	mQuickCdMenu->insertSeparator();
// 	mQuickCdMenu->insertItem( tr("Set &default action"), mCdMenu );
//	connect( mCdMenu, SIGNAL(activated( int )), this, SLOT(slotChangeDefaultAction( int )) );

	mButtonQuickCD = new QToolButton( this );
	mButtonQuickCD->setFocusPolicy( NoFocus ); // so Tab key cannot move focus here
	mButtonQuickCD->setPopup( mQuickCdMenu );
	mButtonQuickCD->setPopupDelay( 0 ); // for QToolButton
	mButtonQuickCD->setText( ".." );
	//mButtonQuickCD->setPixmap( QPixmap((const char**)cdup) );
	//mButtonQuickCD->setToggleButton( TRUE ); // need to for QPushButton
	mButtonQuickCD->setMaximumSize( 30, 27 );
	connect( mButtonQuickCD, SIGNAL(clicked()), this, SLOT(slotChangeDirToUp()) );


	if ( ptrOfList ) {
		mComboPathView = new ComboPath( ptrOfList, 20, TRUE, this ); // max 20 items, ReadWrite Mode
		mComboPathView->setFocusPolicy( ClickFocus );
		mComboPathView->move( 28,0 );

		connect( mComboPathView, SIGNAL(signalPathChanged(const QString &)),
		 this, SIGNAL(signalPathChanged(const QString &)) );

		setTabOrder( mComboPathView, ptrOfList );
	}

	init();
}


PathView::~PathView()
{
	QSettings *settings = new QSettings();

	// --- save URLs list and URL id
	QStringList urlsList;
	for (int i=0; i<mComboPathView->count(); i++)
		urlsList.append( mComboPathView->text(i) );

	QString n = name();
	QString side = n.left( n.find('P') );

	settings->writeEntry( "/qtcmd/"+side+"FilesPanel/URLs", urlsList );
	settings->writeEntry( "/qtcmd/"+side+"FilesPanel/NumOfCurrentURL", mComboPathView->currentItem()+1 );

	delete settings;

	// --- remove all objects
	delete mQuickCdMenu;
	delete mButtonQuickCD;
	delete mComboPathView;
}


void PathView::init()
{
	QSettings *settings = new QSettings();
	QString side = QString(name()).left( QString(name()).find('P') );

	// --- read list of URLs for the ComboPath obj.
	QStringList listOfURLs = settings->readListEntry( "/qtcmd/"+side+"FilesPanel/URLs" );
	if ( listOfURLs.isEmpty() )
		listOfURLs.append( "/" ); // defaultPanelPath

	// an each URL should have a slash ('/') at last position
	for( uint i=0; i<listOfURLs.count(); i++ )
		if ( listOfURLs[i].at(listOfURLs[i].length()-1) != '/' )
			listOfURLs[i] += '/';
	// init combo path by the list
	mComboPathView->insertStringList( listOfURLs );

	// --- read number of current URL
	int numOfCurrentItem = settings->readNumEntry( "/qtcmd/"+side+"FilesPanel/NumOfCurrentURL" );
	if ( numOfCurrentItem < 1 || numOfCurrentItem > (int)listOfURLs.count() ) {
		numOfCurrentItem = 1;
	}
	numOfCurrentItem--; // becouse saved numer has been increased about 1
	mComboPathView->setCurrentItem( numOfCurrentItem );

	delete settings;
}


QString PathView::prevUrl( bool getFromLocalFS ) const
{
	QString itemStr, urlOut = "/";
	uint allItems = mComboPathView->count();
	uint prevId   = mComboPathView->prevItemId();

	while ( allItems-- ) {
		if ( getFromLocalFS ) { // find path from local file system
			itemStr = mComboPathView->text( prevId );
			if ( itemStr.at(0) == '/' && ! FileInfoExt::isArchive(itemStr) ) {
				urlOut = itemStr;
				break;
			}
			else
				prevId = (prevId>0) ? --prevId : mComboPathView->count()-1;
		}
		else
			return mComboPathView->text( prevId );
	}

	return urlOut;
}


void PathView::showPopupList()
{
	mComboPathView->popup(); // shows history of URLs
}


void PathView::setStatusWritable()
{
	QColor bgColor;
	DirWritable dw;

	emit signalPossibleDirWritable( dw ); // get writable status

	if ( dw == ENABLEdw )
		bgColor = QColor( 228, 240, 223 );
	else
	if ( dw == DISABLEdw )
		bgColor = QColor( 255, 229, 229 );
	else
		bgColor = Qt::white;

	mComboPathView->lineEdit()->setPaletteBackgroundColor( QColor(bgColor) );
	mComboPathView->lineEdit()->setPaletteForegroundColor( Qt::black );
}


void PathView::chagePathInFS( const QString & newPath )
{
	emit signalPathChanged( newPath );
	setStatusWritable();
	updateToolTips();
}


void PathView::showButtonMenu()
{
	mQuickCdMenu->popup( mapToGlobal(QPoint(0, y()+height())) );
}


void PathView::updateToolTips()
{
	int menuHeight  = mQuickCdMenu->sizeHint().height()-5; // 5 - 2*separator
	int actionH     = menuHeight/mQuickCdMenu->count()+4;
	QRect prevActRect = QRect( 0, actionH*4, mQuickCdMenu->width(), actionH );
	QRect nextActRect = QRect( 0, actionH*5, mQuickCdMenu->width(), actionH );

	QToolTip::remove( mQuickCdMenu, prevActRect );
	QToolTip::remove( mQuickCdMenu, nextActRect );

	QToolTip::add( mQuickCdMenu, prevActRect, prevUrl() );
	QToolTip::add( mQuickCdMenu, nextActRect, nextUrl() );
}

//     ----- SLOTS -----

void PathView::slotChangeDirToUp()
{
	chagePathInFS( "../" );
}


void PathView::slotChangeDirToUserHome()
{
	chagePathInFS( "~" );
}


void PathView::slotChangeDirToRoot()
{
	chagePathInFS( "//" );
}


void PathView::slotChangeDirToPrev()
{
	chagePathInFS( prevUrl() );
}


void PathView::slotChangeDirToNext()
{
	chagePathInFS( nextUrl() );
}


void PathView::slotClearHistoryPath()
{
	int result = MessageBox::yesNo( this,
	 tr("Clear history pathes")+" - QtCommander",
	 tr("History contains")+" "+QString::number(mComboPathView->count())+" "+tr("items")+".\n\n"+
	 tr("Do you really want to clear it")+" ?",
	 MessageBox::No // default is the 'No' button
	);
	if ( result == MessageBox::Yes )
		mComboPathView->clear();
}


void PathView::slotClearPath()
{
	mComboPathView->setEditText( "" );
}


void PathView::slotEditHistory()
{
	QString newURL;
	if ( UrlsListManager::showDialog(newURL, UrlsListManager::HISTORY, currentUrl(), this) ) // let's go to URL
		emit signalPathChanged( newURL );

	// a path(es) maybe have be removed, so need to read all again
	QString side = QString(name()).left( QString(name()).find('P') );
	QSettings settings;
	QStringList listOfURLs = settings.readListEntry( "/qtcmd/"+side+"FilesPanel/URLs" );
	mComboPathView->clear();
	mComboPathView->insertStringList( listOfURLs );

	parentWidget()->setFocus();
}


void PathView::slotPathChanged( const QString & newPath )
{
	mComboPathView->insertPath( newPath );
	setStatusWritable();
	updateToolTips();
}


void PathView::slotRemovePath( const QString & pathName )
{
	if ( mComboPathView->count() < 2 )
		return;

	mComboPathView->removePath( pathName );

	int id = mComboPathView->currentItem();
	mComboPathView->setCurrentItem( id += (id > 0) ? -1 : 1 );
}


void PathView::resizeEvent( QResizeEvent * )
{
	mComboPathView->setGeometry( 30,0, width()-30,27 );
}
