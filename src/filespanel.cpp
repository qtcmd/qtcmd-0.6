/***************************************************************************
                          filespanel.cpp  -  description
                             -------------------
    begin                : sun oct 27 2002
    copyright            : (C) 2002, 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright            : See COPYING file that comes with this project

 $Id$
 ***************************************************************************/

#include "ftpconnectdialog.h"
#include "urlslistmanager.h"
#include "findcriterion.h"
#include "functions_col.h"
#include "qtcmdactions.h"
#include "fileinfoext.h"
#include "messagebox.h"
#include "filespanel.h" // includes "qheader.h" too
#include "functions.h" // for few static functions
#include "archivefs.h"
// #include "networkfs.h"
// #include "sambafs.h"
#include "filters.h"
#include "localfs.h"
#include "ftpfs.h"
#include "panel.h"

#include "../icons/filter.xpm"

#include <qsettings.h>
#include <qregexp.h>

static QString sTS;
static bool sSettingsHasRead;
ListViewSettings sListViewSettings;


FilesPanel::FilesPanel( const QString & initURL, QWidget *parent, const char *name )
{
	debug("FilesPanel::FilesPanel (%s), initURL=%s", name, initURL.latin1() );
	mWidth = parent->width();
	mFilesAssociation = NULL;
	mKeyShortcuts = NULL;
	mDevicesMenu = NULL;
	mVirtualFS = NULL;
	mInitUrl  = "";
	mFilterId = 0;
	setName(name);

	mListViewExt  = new ListViewExt( parent, this );
	mPathView     = new PathView( mListViewExt, parent, QString(name)+"_VPW" );
	mStatusBar    = new StatusBar( parent, QString(name)+"_SW" );
	mFilterButton = new QToolButton( parent );
	mDevicesMenu  = new DevicesMenu( mListViewExt );

	mStatusBar->resize( mWidth-3, 21 ); // it's needed by sSettings the infoStrList (in the StatusBar class)

	connect( mPathView, SIGNAL(signalPossibleDirWritable( DirWritable & )),
	 this, SLOT(slotPossibleDirWritable( DirWritable & )) );
	connect( mPathView, SIGNAL(signalPathChanged(const QString &)),
	 this, SLOT(slotPathChanged(const QString &)) );
	connect( mDevicesMenu, SIGNAL(signalPathChanged(const QString &)),
	 this, SLOT(slotPathChanged(const QString &)) );

	// connect ListView's signals
	connect( (QListView *)mListViewExt->header(), SIGNAL(sizeChange(int, int, int)),  this, SIGNAL(signalHeaderSizeChange(int, int, int)) );
	connect( mListViewExt, SIGNAL(signalUpdateOfListStatus(uint, uint, long long)), mStatusBar, SLOT(slotUpdateOfListStatus(uint , uint , long long)) );
	connect( mListViewExt, SIGNAL(signalPathChanged(const QString &)), this, SLOT(slotPathChangedForTheViewPath(const QString &)) );
	connect( mListViewExt, SIGNAL(signalRenameCurrentItem(int, const QString &, const QString &)), this, SLOT(slotRenameCurrentItem(int, const QString &, const QString &)) );
	connect( mListViewExt, SIGNAL(signalSetProgress(int)), mStatusBar, SLOT(slotSetProgress(int)) );
	connect( mListViewExt, SIGNAL(returnPressed(QListViewItem *)), this, SLOT(slotReturnPressed(QListViewItem *)) );

	connect( mListViewExt->popupMenu(), SIGNAL(signalDelete()),            this, SLOT(slotDelete()) );
	connect( mListViewExt->popupMenu(), SIGNAL(signalMakeLink()),          this, SLOT(slotMakeLink()) );
	connect( mListViewExt->popupMenu(), SIGNAL(signalCopy( bool , bool )),  this, SLOT(slotCopy( bool , bool )) );
	connect( mListViewExt->popupMenu(), SIGNAL(signalShowProperties()),    this, SLOT(slotShowProperties()) );
	connect( mListViewExt->popupMenu(), SIGNAL(signalOpenInPanelBeside(bool)), this, SLOT(slotOpenIntoPanelBeside(bool)) );


	initPanel(); // initialize: mKindOfListView
	initLookOfList(); // initialize: columns, FontFamily, FontSize, ShowIcons, colors of the listFiles and cursor, kind of group size of file
	initFiltersButton(); // initialize button contais filters menu


	QString initUrl = ( initURL.isEmpty() ) ? mPathView->currentUrl() : initURL;
	if ( ! initURL.isEmpty() ) {
		if ( initUrl == "./" )
			initUrl = QDir::currentDirPath();
		else
		if ( initUrl.at(0) == '~' )
			initUrl = QDir::homeDirPath();
		else
		if ( initUrl.at(0) != '/' )
			initUrl = QDir::currentDirPath()+"/"+initURL;
		if ( initUrl.at(initUrl.length()-1) != '/' )
			initUrl += "/";
		mPathView->slotPathChanged( initUrl ); // shows a path without waiting for listing directory
	}
	createFS( initUrl );

	QTimer::singleShot( 0, this, SLOT(slotInitPanelList()) );
}


FilesPanel::~FilesPanel() // tu zapisywac do pliku konf. wszystkie ustawienia listy: kolory, itp.
{
	debug("FilesPanel (%s), destructor", name());
	sSettingsHasRead = FALSE; // or saved
	saveSettings();
	removeFS();

	if (mDevicesMenu != NULL) {
		delete mDevicesMenu;  mDevicesMenu = 0;
	}
	delete mListViewExt;  mListViewExt = 0;
	delete mStatusBar;    mStatusBar = 0;
	delete mPathView;     mPathView = 0;
	delete mFilterButton;
	delete mFiltersMenu;
}


void FilesPanel::initPanel()
{
	sTS = name();  // eg. name == "LeftPVM_FP"
	QString panelSide = sTS.left( sTS.find('P') );
	QSettings *settings = new QSettings();

	// --- read kind of view (list/tree)
	mKindOfListView = settings->readNumEntry( "/qtcmd/"+panelSide+"FilesPanel/KindOfView", -1 );
	if ( mKindOfListView < 0 || mKindOfListView == UNKNOWNpview )
		mKindOfListView = LISTpview;

	// read it from config file (need to saved in Destruktor)
	mListViewExt->setSorting( ListViewExt::NAMEcol, TRUE ); // default let's sort increase by first column (name)

	bool showFileLinkSize = settings->readBoolEntry( "/qtcmd/FilesPanel/showFileLinkSize", FALSE );
	mListViewExt->setShowFileLinkSize(showFileLinkSize);

	delete settings;
}


void FilesPanel::initLookOfList()
{
	if ( ! sSettingsHasRead ) { // settings will be reading one time
		sSettingsHasRead = TRUE;
		QSettings *settings = new QSettings();
		const QStringList defaultColumnList = QStringList::split( ',', "0-170,1-70,2-107,3-70,4-55,5-55" );

		mFilterForFiltered = settings->readBoolEntry( "/qtcmd/FilesPanel/FilterForFiltered", FALSE );
		sListViewSettings.filterForFiltered = mFilterForFiltered;
		// --- reads columns info
		bool columnsSynchronize = settings->readBoolEntry( "/qtcmd/FilesPanel/SynchronizeColumns", TRUE );
		QStringList columnsList = settings->readListEntry( "/qtcmd/FilesPanel/Columns" );
		if ( columnsList.isEmpty() ) {
			columnsList = defaultColumnList;
			settings->writeEntry( "/qtcmd/FilesPanel/Columns", columnsList );
		}
		int columnWidth;
		for (int i=0; i<6; i++) { // 6 - number of all columns
			if ( columnsList[i].section('-', 0, 0 ) == QString("%1").arg(i) ) { // current column is visible
				columnWidth = (columnsList[i].section( '-', -1 )).toInt();
				if ( (columnWidth > 0 && columnWidth < 10) || columnWidth < 0 )
					columnWidth = (defaultColumnList[i].section('-', -1 )).toInt();
			}
			else
				columnWidth = 0; // don't show current column

			sListViewSettings.columnsWidthTab[i] = columnWidth;
		}
		sListViewSettings.columnsSynchronize = columnsSynchronize;

		// ----- reads fonts info
		QString font = settings->readEntry( "/qtcmd/FilesPanel/Font" );
		if ( font.isEmpty() )
			font = "Helvetica,10"; // default // defaultFontFamily+","+defaultAppFontSize
		// parse 'font'
		sListViewSettings.fontFamily = font.left( font.find(',') );
		QString fontSizeStr = font.section( ',', 1,1 ).stripWhiteSpace();
		bool ok;
		int fontSize = fontSizeStr.toInt( &ok );
		if ( ! ok || (fontSize < 8 || fontSize > 24) )
			fontSize = 10; // default size
		sListViewSettings.fontSize = fontSize;

		// ----- reads misc info about the list view
		sListViewSettings.showIcons = settings->readBoolEntry( "/qtcmd/FilesPanel/ShowIcons", TRUE );
		sListViewSettings.cursorAlwaysVisible = settings->readBoolEntry( "/qtcmd/FilesPanel/NotHiddenCursor", TRUE );
		int kindOfFileSizeFormat = settings->readNumEntry( "/qtcmd/FilesPanel/FileSizeFormat", BKBMBGBformat );
		if ( kindOfFileSizeFormat < NONEformat || kindOfFileSizeFormat > BKBMBGBformat )
			kindOfFileSizeFormat = BKBMBGBformat;
		sListViewSettings.selectFilesOnly     = settings->readBoolEntry( "/qtcmd/FilesPanel/SelectFilesOnly", FALSE );
		sListViewSettings.showHiddenFiles     = settings->readBoolEntry( "/qtcmd/FilesPanel/ShowHidden", TRUE );
		sListViewSettings.fileSizeFormat      = kindOfFileSizeFormat;

		// --- reads info about the colors
		QString colorsScheme = settings->readEntry( "/qtcmd/FilesPanel/ColorsScheme", "DefaultColors" );

		sListViewSettings.showSecondBackgroundColor = settings->readBoolEntry( "/qtcmd/"+colorsScheme+"/ShowSecondBgColor", TRUE );
		sListViewSettings.twoColorsCursor = settings->readBoolEntry( "/qtcmd/"+colorsScheme+"/TwoColorsCursor", TRUE );
		sListViewSettings.showGrid = settings->readBoolEntry( "/qtcmd/"+colorsScheme+"/ShowGrid", FALSE );

		QString sColor;

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/ItemColor", "000000" );
		sListViewSettings.itemColor = color(sColor, "000000");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/ItemColorUnderCursor", "0000EA" );
		sListViewSettings.itemColorUnderCursor = color(sColor, "0000EA");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/SelectedItemColor", "FF0000" );
		sListViewSettings.selectedItemColor = color(sColor, "FF0000");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/SelectedItemColorUnderCursor", "57BA00" );
		sListViewSettings.selectedItemColorUnderCursor = color(sColor, "57BA00");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/BgColorOfSelectedItems", "FFFFFF" );
		sListViewSettings.bgColorOfSelectedItems = color(sColor, "FFFFFF");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/BgColorOfSelectedItemsUnderCursor", "FFFFFF" );
		sListViewSettings.bgColorOfSelectedItemsUnderCursor = color(sColor, "FFFFFF");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/FirstBackgroundColor", "FFFFFF" );
		sListViewSettings.firstBackgroundColor = color(sColor, "FFFFFF");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/SecondBackgroundColor", "FFEBDC" );
		sListViewSettings.secondBackgroundColor = color(sColor, "FFEBD5");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/FullCursorColor", "00008B" );
		sListViewSettings.fullCursorColor = color(sColor, "00008B");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/InsideFrameCursorColor", "000080" );
		sListViewSettings.insideFrameCursorColor = color(sColor, "000080");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/OutsideFrameCursorColor", "FF0000" );
		sListViewSettings.outsideFrameCursorColor = color(sColor, "FF0000");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/GridColor", "A9A9A9" );
		sListViewSettings.gridColor = color(sColor, "A9A9A9");

		// --- reads info about the file colors
		colorsScheme = settings->readEntry( "/qtcmd/FilesPanel/FileColorsScheme", "DefaultColors" );
// 		colorsScheme.insert(0, "File");
// 		colorsScheme[4] = colorsScheme[4].upper();

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/CommonFileColor", "000000" );
		sListViewSettings.commonFileColor = color(sColor, "000000");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/ExeFileColor", "000000" );
		sListViewSettings.exeFileColor = color(sColor, "000000");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/SymlinkColor", "000000" );
		sListViewSettings.symlinkColor = color(sColor, "000000");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/HangSymlinkColor", "000000" );
		sListViewSettings.brokenSymlinkColor = color(sColor, "000000");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/SocketColor", "000000" );
		sListViewSettings.socketColor = color(sColor, "000000");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/CharacterDevColor", "000000" );
		sListViewSettings.characterDevColor = color(sColor, "000000");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/BlockDevColor", "000000" );
		sListViewSettings.blockDevColor = color(sColor, "000000");

		sColor = settings->readEntry( "/qtcmd/"+colorsScheme+"/FifoColor", "000000" );
		sListViewSettings.fifoColor = color(sColor, "000000");

		delete settings;
	}

	slotApplyListViewSettings( sListViewSettings );
}


void FilesPanel::initFiltersButton()
{
	mFiltersMenu = new QPopupMenu( this );
	connect( mFiltersMenu, SIGNAL(activated(int)), this, SLOT(slotChangeFilter(int)) );

	mFilterButton->setIconSet( QPixmap((const char**)filter) );
	mFilterButton->setFocusPolicy( NoFocus ); // so Tab key cannot move focus here
	mFilterButton->setPopup( mFiltersMenu );
	mFilterButton->setMaximumWidth( 30 );
	mFilterButton->setPopupDelay( 0 );
}


void FilesPanel::updateFiltersMenu( Filters * filters )
{
	if ( mFiltersMenu == NULL || filters == NULL || mKeyShortcuts == NULL )
		return;

	mFilters = filters;
	mFiltersMenu->clear();
		// add items to the filters menu
	mFiltersMenu->insertItem( tr("&Configure filters")+" ("+mKeyShortcuts->keyStr(ShowFiltersManager_APP)+")", this, SLOT(slotShowFiltersManager()), 0, 1000 );
	mFiltersMenu->insertSeparator();
	int firstMenuHeight = mFiltersMenu->sizeHint().height();
	uint i;
	for ( i=0; i<filters->count(); i++ )
		mFiltersMenu->insertItem( filters->name(i), i);

		// add toolTips to an each item of the menu
	int menuHeight = mFiltersMenu->sizeHint().height();
	int actionH    = menuHeight/mFiltersMenu->count()+2;
	for ( uint i=0; i<filters->count(); i++ ) {
		QToolTip::add( mFiltersMenu,
		 QRect( 0, (actionH*i)+firstMenuHeight, mFiltersMenu->width(), actionH ),
		 filters->patterns(mFiltersMenu->text(i))
		);
	}
	sListViewSettings.defaultFilter = filters->defaultId();
	slotChangeFilter( sListViewSettings.defaultFilter );
}


void FilesPanel::saveSettings()
{
	QSettings *settings = new QSettings();

	if ( ! sSettingsHasRead ) { // this setting will be saving one time
		sSettingsHasRead = TRUE;

		bool saveColumns = FALSE;
		int nOldColumnWidth;
		QStringList columnsList;
		for ( int i=0; i<6; i++ ) { // 6 - number of all columns
			nOldColumnWidth = sListViewSettings.columnsWidthTab[i];
			if (! saveColumns && mListViewExt->columnSize(i) != nOldColumnWidth)
				saveColumns = TRUE;
			columnsList.append( QString("%1-%2").arg(i).arg(mListViewExt->columnSize(i)) );
		}
		if ( saveColumns )
			settings->writeEntry("/qtcmd/FilesPanel/Columns", columnsList);
	}
	sTS = name();  // np. name == "LeftPVM_FP"
	QString side = sTS.left( sTS.find('P') );

	settings->writeEntry( "/qtcmd/"+side+"FilesPanel/KindOfView", mListViewExt->kindOfView() );

	delete settings;
}


void FilesPanel::updateView()
{
	mPathView->showNormal();
	mStatusBar->showNormal();
	mListViewExt->showNormal();
}


void FilesPanel::createFS( const QString & url )
{
	QString inputURL = url;
	KindOfFilesSystem kindOfFS = mVirtualFS->kindOfFilesSystem( url );

	if ( FileInfoExt::isArchive(url) ) { // check whether current archive is supported
		if ( ! ArchiveFS::archiveIsSupported(url) ) {
			MessageBox::critical(this,
			 tr("Archives")+" '"+QFileInfo(url).extension(FALSE)+"' "+tr("are not supported !")
			);
			return;
		}
	}

	debug("FilesPanel::createFS(), try with inputURL='%s', mInitUrl='%s'", inputURL.latin1(), mInitUrl.latin1() );
	if ( kindOfFS == FTPfs && ! mInitUrl.isEmpty() ) {
		if ( ! FtpConnectDialog::showDialog(inputURL, this) ) { // user canceled connection
// 			debug("__ connection canceled");
			mPathView->slotPathChanged( mVirtualFS->currentURL() ); // restores previous path
			return;
		}
	}
	/*if ( kindOfFS == UNKNOWNfs )  {
		MessageBox::information( this, tr("Error") + " - QtCommander"+,
			+"\""+currentItemText+"\""+"\n\n"+tr("Unknown file system !") );
		return;
	}*/

	removeFS(); // remove current file system

	if ( kindOfFS == LOCALfs )
		mVirtualFS = new LocalFS( inputURL, mListViewExt );
	else
	if ( kindOfFS == ARCHIVEfs )
		mVirtualFS = new ArchiveFS( inputURL, mListViewExt );
	else
	if ( kindOfFS == FTPfs )
		mVirtualFS = new FtpFS( inputURL, mListViewExt );
// 	else
// 	if ( kindOfFS == SAMBAfs )
// 		mVirtualFS = new SambaFS( inputURL, mListViewExt );
// 	else
// 	if ( kindOfFS == NETWORKfs ) // NFS
// 		mVirtualFS = new NetworkFS( inputURL, mListViewExt );

	if ( ! mVirtualFS )
		return;

	connect( mVirtualFS, SIGNAL(signalResultOperation( Operation, bool )),
		this, SLOT(slotResultOperation( Operation, bool )) );
	connect( mVirtualFS, SIGNAL(signalOpenArchive(const QString & )),
		this, SLOT(slotOpenArchive( const QString & )) );
	connect( mVirtualFS, SIGNAL(signalSetInfo( const QString & )),
		mStatusBar, SLOT(slotSetMessage( const QString & )) );

	connect( mListViewExt, SIGNAL(signalPathChanged(const QString &)),
		mVirtualFS, SLOT(slotSetCurrentURL(const QString &)) );
	connect( mListViewExt, SIGNAL(signalOpen(const QString &)),
		mVirtualFS, SLOT(slotOpen(const QString &)) );
	connect( mListViewExt, SIGNAL(signalClose()),
		mVirtualFS, SLOT(slotCloseCurrentDir()) );

	// --- init some settings of listView
	if ( mVirtualFS->kindOfCurrentFS() != ARCHIVEfs )
		mListViewExt->clear(); // need to go out from non local FS, and shows tree for non local FS
	mListViewExt->setHostName( mVirtualFS->host() );
	if ( mListViewExt->kindOfView() != UNKNOWNpview )
		mKindOfListView = mListViewExt->kindOfView();
	mListViewExt->setKindOfView( (KindOfListView)mKindOfListView );

	mFinalPathOfTree = mVirtualFS->absHostURL(); // dla arch.jest tu tylko nazwa arch.
	QString newURL = (mVirtualFS->kindOfCurrentFS() != ARCHIVEfs) ? mFinalPathOfTree : inputURL;

	if ( mInitUrl.isEmpty() )
		mInitUrl = inputURL;
	else // used when user want go out from FS, or select an url in the path view
		mListViewExt->open( newURL ); // mFinalPathOfTree

// 	if ( mFilters )
// 		slotChangeFilter( mFilterId );
}


void FilesPanel::removeFS()
{
	if ( mVirtualFS == NULL )
		return;

	delete mVirtualFS;  mVirtualFS = 0;
}


void FilesPanel::slotRereadCurrentDir()
{
	if ( ! hasFocus() && ! mPathView->hasFocus() ) // change view only for an active panel
		return;

	if ( mVirtualFS )
		mVirtualFS->rereadCurrentDir();
}


void FilesPanel::slotChangeKindOfView( uint kindOfListView )
{
	if ( ! hasFocus() && ! mPathView->hasFocus() ) // change view only for an active panel
		return;
	if ( kindOfListView == (uint)mListViewExt->kindOfView() )
		return;

	mListViewExt->setKindOfView( (KindOfListView)kindOfListView );
	mFinalPathOfTree = mVirtualFS->currentURL();
	mKindOfListView = kindOfListView;
	mListViewExt->open(); // no param == current path
}


void FilesPanel::keyPressEvent( QKeyEvent *ke )
{
	if ( mKeyShortcuts == NULL ) // no keyShortcuts loaded
		return;

	int key = ke->key();
	int ks  = mKeyShortcuts->ke2ks( ke );

	if ( ks == mKeyShortcuts->key(OpenCurrentFileOrDirectoryInLeftPanel_APP) )
		slotOpenIntoPanelBeside( FALSE );
	else
	if ( ks == mKeyShortcuts->key(OpenCurrentFileOrDirectoryInRightPanel_APP) )
		slotOpenIntoPanelBeside( FALSE );
	else
	if ( ks == mKeyShortcuts->key(ForceOpenCurrentDirectoryInLeftPanel_APP ) )
		slotOpenIntoPanelBeside( TRUE );
	else
	if ( ks == mKeyShortcuts->key(ForceOpenCurrentDirectoryInRightPanel_APP) )
		slotOpenIntoPanelBeside( TRUE );
	else
	if ( ks == mKeyShortcuts->key(GoToPreviousURL_APP) )
		slotPathChanged( mPathView->prevUrl() );
	else
	if ( ks == mKeyShortcuts->key(GoToNextURL_APP) )
		slotPathChanged( mPathView->nextUrl() );
	else
	if ( ks == mKeyShortcuts->key(ShowHistory_APP) )
		mPathView->showPopupList();
	else
	if ( ks == mKeyShortcuts->key(ShowHistoryMenu_APP) )
		mPathView->showButtonMenu();
	else
	if ( ks == mKeyShortcuts->key(ShowFilters_APP) )
		mFiltersMenu->popup( mListViewExt->mapToGlobal(QPoint( mFilterButton->x(), mListViewExt->height()-mFiltersMenu->height() )) );
	else
	if ( ks == mKeyShortcuts->key(ShowFiltersManager_APP) )
		slotShowFiltersManager();
	else
	if ( ks == mKeyShortcuts->key(ChangeViewToList_APP) )
		slotChangeKindOfView( (uint)LISTpview );
	else
	if ( ks == mKeyShortcuts->key(ChangeViewToTree_APP) )
		slotChangeKindOfView( (uint)TREEpview );
	else
	if ( ks == mKeyShortcuts->key(QuickFileSearch_APP) )
		mListViewExt->showFindItemDialog();
	else
	if ( key == Key_Menu )
		mListViewExt->showPopupMenu( FALSE );
	else
	if ( ks == mKeyShortcuts->key(ShowProperties_APP) )
		slotShowProperties();
	else
	if ( ks == mKeyShortcuts->key(MoveCursorToPathView_APP) || key == Key_Backtab )
		mPathView->setFocus();
	else
	if ( ks == mKeyShortcuts->key(ViewFile_APP) )
		slotReturnPressed( 0 );
	else
	if ( ks == mKeyShortcuts->key(EditFile_APP) )
		openCurrentFile( TRUE ); // TRUE - open in edit mode
	else
	if ( ks == mKeyShortcuts->key(MakeAnEmptyFile_APP) )
		mVirtualFS->createNewEmptyFile(); // and open it in edit mode
	else
	if ( ks == mKeyShortcuts->key(CopyFiles_APP) )
		slotCopy( FALSE, FALSE );
	else
	if ( ks == mKeyShortcuts->key(CopyFilesToCurrentDirectory_APP) )
		slotCopy( TRUE, FALSE );
	else
	if ( ks == mKeyShortcuts->key(MoveFiles_APP) )
		slotCopy( FALSE, TRUE );
	else
	if ( ks == mKeyShortcuts->key(QuickFileRename_APP) )
		slotCopy( TRUE, TRUE );
	else
	if ( ks == mKeyShortcuts->key(MakeDirectory_APP) )
		createNewEmptyFile( TRUE );
	else
	if ( ks == mKeyShortcuts->key(DeleteFiles_APP) || key == Key_Delete )
		slotDelete();
	else
	if ( ks == mKeyShortcuts->key(MakeALink_APP) )
		slotMakeLink();
	else
	if ( ks == mKeyShortcuts->key(EditCurrentLink_APP) )
		slotEditLink();
	else
	if ( ks == mKeyShortcuts->key(WeighCurrentDirectory_APP) ) {
		if ( mVirtualFS->kindOfCurrentFS() == LOCALfs ) { // weighing in other than LocalFS is not yet supported
			if ( mListViewExt->currentItemIsDir() )  {
				if (! mListViewExt->currentItem()->isSelected()) {
					mListViewExt->setTextInColumn( ListViewExt::SIZEcol, "0" );
					mVirtualFS->sizeFiles( mListViewExt->currentItemText(), FALSE ); // TODO parameter need to set by option (Real weigh in FS)
				}
				else {
					mListViewExt->setTextInColumn( ListViewExt::SIZEcol, "<DIR>" );
					mListViewExt->countAndWeightSelectedFiles( TRUE );
				}
			}
			mListViewExt->setSelected( mListViewExt->currentItem(), ! mListViewExt->currentItem()->isSelected() );
		}
	}
	else
	if ( ks == mKeyShortcuts->key(GoToUpLevel_APP) )
		mVirtualFS->slotCloseCurrentDir();
	else
	if ( ks == mKeyShortcuts->key(GoToHomeDirectory_APP) )
		slotPathChanged( QDir::homeDirPath()+"/" );
	else
	if ( ks == mKeyShortcuts->key(RefreshDirectory_APP) )
		slotRereadCurrentDir();
	else
	if ( key == Key_Insert ) { // mark current item
		if ( mListViewExt->currentItemIsDir() )
			mListViewExt->setTextInColumn( ListViewExt::SIZEcol, "<DIR>" );
		mListViewExt->slotSelectCurrentItem();
		mCurrentItem = mListViewExt->currentItem();
		mListViewExt->ensureItemVisible( mCurrentItem->itemBelow() );
		if ( mListViewExt->isTreeView() )
			mPathView->slotPathChanged( mListViewExt->pathForCurrentItem(TRUE, TRUE) );
	}
	else
	//if ( ks == mKeyShortcuts->key(InvertSelection_APP) )
	if ( key == Key_Asterisk )
		mListViewExt->slotInvertSelections();
	else
	if ( ks == mKeyShortcuts->key(SelectFilesWithThisSameExtention_APP) )
		mListViewExt->slotSelectWithThisSameExtension();
	else
	if ( ks == mKeyShortcuts->key(DeselectFilesWithThisSameExtention_APP) )
		mListViewExt->slotSelectWithThisSameExtension( TRUE );
	else
	if ( ks == mKeyShortcuts->key(SelectAllFiles_APP) )
		mListViewExt->slotSelectAllItems();
	else
	if ( ks == mKeyShortcuts->key(DeselectAllFiles_APP) )
		mListViewExt->slotSelectAllItems( TRUE );
	else
	if ( ks == mKeyShortcuts->key(SelectGroupOfFiles_APP) )
		mListViewExt->slotSelectGroupOfItems();
	else
	if ( ks == mKeyShortcuts->key(DeselectGroupOfFiles_APP) )
		mListViewExt->slotSelectGroupOfItems( TRUE );
	else
	if ( ks == mKeyShortcuts->key(QuickChangeOfFilePermission_APP) )
		mListViewExt->slotShowEditBox( ListViewExt::PERMcol );
	else
	if ( ks == mKeyShortcuts->key(QuickChangeOfFileModifiedDate_APP) )
		mListViewExt->slotShowEditBox( ListViewExt::TIMEcol );
	else
	if ( ks == mKeyShortcuts->key(QuickChangeOfFileOwner_APP) )
		mListViewExt->slotShowEditBox( ListViewExt::OWNERcol );
	else
	if ( ks == mKeyShortcuts->key(QuickChangeOfFileGroup_APP) )
		mListViewExt->slotShowEditBox( ListViewExt::GROUPcol );
}


void FilesPanel::copyFiles( const QStringList & filesList, bool move )
{
	mVirtualFS->copyFilesTo( mVirtualFS->currentURL(), filesList, move );
}

// ---------- SLOTs ------------

void FilesPanel::slotReturnPressed( QListViewItem * )
{
	mListViewExt->open( (mFinalPathOfTree=mListViewExt->pathForCurrentItem()), TRUE ); // TRUE - closes if open (a branch)
}


void FilesPanel::slotOpenArchive( const QString & fileName )
{
	if ( ! FileInfoExt::isArchive(fileName) )
		return;

	if ( ! ((ListViewItemExt *)mListViewExt->currentItem())->entryInfo().isReadable() ) {
		MessageBox::critical( this,
			fileName+"\n\n"+tr("Cannot open this archive")+".\n"+tr("Permission denied.")
		);
		return;
	}
	createFS(mVirtualFS->currentURL()+FileInfoExt::fileName(fileName));
}


void FilesPanel::slotOpenIntoPanelBeside( bool dirForce )
{
	QString fileName;

	if ( ! dirForce ) {
		fileName = mVirtualFS->currentURL()+mListViewExt->currentItemText();
		if ( mListViewExt->currentItemIsDir() )
			fileName += "/";
		else {
			if ( mFilesAssociation )
				if ( mFilesAssociation->loaded() ) {
					if ( mFilesAssociation->kindOfViewer(fileName) == EXTERNALviewer ) {
						MessageBox::critical( this, tr("Cannot to embedded an external viewer !") );
						return;
					}
				}
		}
	}
	else // open current directory
		fileName = mVirtualFS->currentURL();

	mFinalPathOfTree = fileName;
	emit signalOpenIntoPanelBeside( fileName );
}


void FilesPanel::slotRenameCurrentItem( int column, const QString & oldText, const QString & newText )
{
	if ( ! newText.isEmpty() && oldText != newText )
		mVirtualFS->changeAttribut( (ListViewExt::ColumnName)column, newText );
}


void FilesPanel::slotDelete()
{
	mVirtualFS->removeFiles();
}


void FilesPanel::slotMakeLink()
{
	mVirtualFS->makeLink( mPanelBesideURL, FALSE );
}


void FilesPanel::slotEditLink()
{
	mVirtualFS->makeLink( mPanelBesideURL, TRUE );
}


void FilesPanel::slotCopy( bool shiftPressed, bool move )
{
	if ( shiftPressed && move ) { // show rename edit box on current item position
		mListViewExt->slotShowEditBox( ListViewExt::NAMEcol );
		return;
	}

	KindOfFilesSystem fsInPanelBeside;
	emit signalGetKindOfFSInPanelBeside( fsInPanelBeside );

	QStringList namesList;
	mListViewExt->collectAllSelectedNames( namesList ); // collect abs. files name

	if ( fsInPanelBeside != LOCALfs )
		emit signalCopyFiles( namesList, move ); // calls copyFiles() for the second panel
	else {
		QString tmpPanelBesideURL = mPanelBesideURL;
		if ( shiftPressed && ! move )  // try copy file to current directory
			mPanelBesideURL = mVirtualFS->currentURL();
		if ( ! mVirtualFS->copyFilesTo(mPanelBesideURL, namesList, move) )
			mPanelBesideURL = tmpPanelBesideURL;
	}
	//debug("==back to FilesPanel::slotCopy(), from mVirtualFS->copyFilesTo");
}


void FilesPanel::slotShowProperties()
{
	mVirtualFS->showPropertiesDialog();
}


void FilesPanel::slotPathChanged( const QString & inputURL )
{
	debug("FilesPanel::slotPathChanged, try to '%s'", inputURL.latin1() );

	mFinalPathOfTree = inputURL;
	bool needToChange = mVirtualFS->needToChangeFS( mFinalPathOfTree );

	if ( mFinalPathOfTree.isEmpty() )
		mFinalPathOfTree = mPathView->prevUrl(TRUE); // get previous path from LocalFS

	if ( needToChange )
		createFS( mFinalPathOfTree );
}


void FilesPanel::slotResultOperation( Operation operation, bool noError )
{
	debug("FilesPanel::slotResultOperation(), operation= %s, errorStat=%d, file=%s", VirtualFS::cmdString(operation).latin1(), !noError, mVirtualFS->nameOfProcessedFile().latin1() );
	QString processedFile = mVirtualFS->nameOfProcessedFile();
	QString currentURL    = mVirtualFS->currentURL();
	ListViewExt::SelectedItemsList *selectedItemsList = NULL;
	QString path;
	UrlInfoExt urlInfo;
	Error errorCode = mVirtualFS->errorCode();
	bool moveWithRemoveError = (operation == Move && errorCode == CannotRemove);
	bool copyOrMoveWithSetAttrError = ((operation == Copy || operation == Move) && errorCode == CannotSetAttributs);

	if ( noError || copyOrMoveWithSetAttrError || moveWithRemoveError )  {
		if ( operation == Copy || operation == Move || operation == MakeLink ) { // path = target path
			KindOfFilesSystem FSofProcessedFile = mVirtualFS->kindOfFilesSystem(FileInfoExt::filePath(processedFile));
			if ( mVirtualFS->kindOfCurrentFS() == LOCALfs )
				path = (operation == Move) ? currentURL : ((mPanelBesideURL != currentURL) ? mPanelBesideURL : QString(""));
			else
				path = (FSofProcessedFile != LOCALfs) ? currentURL : mPanelBesideURL;

			if ( path == FileInfoExt::filePath(processedFile) || currentURL != FileInfoExt::filePath(processedFile) ) {
				if ( FSofProcessedFile == LOCALfs && mVirtualFS->kindOfCurrentFS() != LOCALfs ) {
					// copy/move to FTP
					emit signalGetSILfromPanelBeside( selectedItemsList );
					if ( selectedItemsList->count() == 0 )
						return;
					mListViewExt->updateList( ListViewExt::INSERTitems, selectedItemsList );
					if ( operation == Move && noError )
						emit signalUpdateSecondPanel( ListViewExt::REMOVEitems, selectedItemsList, urlInfo );
					else
						emit signalUpdateSecondPanel( ListViewExt::DESELECTitems, selectedItemsList, urlInfo );
				}
				else { // copy/move inside LOCALfs or copy/move to LOCALfs
					selectedItemsList = mListViewExt->selectedItemsList();
					if ( selectedItemsList->count() == 0 ) {
						if ( operation != MakeLink ) // need to update selected list when occures skip the copying files
							mListViewExt->countAndWeightSelectedFiles( TRUE );
						return;
					}
					if ( operation == MakeLink ) {
						if ( selectedItemsList->count() == 1 ) {
							mVirtualFS->slotGetUrlInfoFromList( processedFile, urlInfo );
						}
						urlInfo.setName( mVirtualFS->currentURL()+FileInfoExt::fileName(processedFile) ); // slotGetUrlInfoFromList not get full path
					}
					if ( mVirtualFS->targetURL() == mPanelBesideURL )
						emit signalUpdateSecondPanel( (operation==MakeLink) ? ListViewExt::INSERTitemsAsLinks : ListViewExt::INSERTitems, selectedItemsList, urlInfo );
					if ( operation == Move && noError )
						mListViewExt->updateList( ListViewExt::REMOVEitems );
					else
					//if ( operation != Move )
						mListViewExt->updateList( ListViewExt::DESELECTitems );
				}
				if ( operation != MakeLink ) // need to update selected list when occures skip the copying files
					mListViewExt->countAndWeightSelectedFiles( TRUE );
			}
			else { // Copy/Move/MakeLink to this same directory
				if ( mListViewExt->numOfSelectedItems() == 0 ) {
					mVirtualFS->getNewUrlInfo( urlInfo );
					// need to change user and group for all items on the selectedList, before update it
					if ( operation != Move && mVirtualFS->kindOfFilesSystem(processedFile) == LOCALfs ) { // when copied archive file then kofFS = ARCHIVEfs
						urlInfo.setOwner( LocalFS::getUser() );
						urlInfo.setGroup( LocalFS::getUser( FALSE ) );
					}
					mListViewExt->clearSelectedItemsList();
					if ( ! urlInfo.name().isEmpty() )
						mListViewExt->updateList( ListViewExt::INSERTitems, 0, urlInfo );
				}
			}
		} // Copy/Move/MakeLink
		else
		if ( operation == SetAttributs || operation == Rename ) {
			mVirtualFS->getNewUrlInfo( urlInfo );
			mListViewExt->updateList( ListViewExt::REMOVEitems | ListViewExt::INSERTitems, 0, urlInfo ); // remove and insert item - urlInfo
		}
		else
		if ( operation == Remove ) {
			if ( currentURL == FileInfoExt::filePath(processedFile) )
				mListViewExt->updateList( ListViewExt::REMOVEitems );
		}
		else
		if ( operation == Put || operation == MakeDir || operation == Touch ) {
			mVirtualFS->getNewUrlInfo( urlInfo );
			if ( FileInfoExt::filePath(processedFile) == currentURL )
				mListViewExt->updateList( ListViewExt::INSERTitems, 0, urlInfo );
			else
			if ( FileInfoExt::filePath(processedFile) == mPanelBesideURL )
				emit signalUpdateSecondPanel( ListViewExt::INSERTitems, 0, urlInfo );
		}
		else
		if ( operation == Weigh ) {
			if ( ! mVirtualFS->setWeighInPropetiesDlg() ) {
				long long totalWeigh;
				uint totalFiles, totalDirs;
				mVirtualFS->getWeighingStat( totalWeigh, totalFiles, totalDirs );
		// class QString in Qt-3.1.x not supports long long type !
				//QString sizeStr = QString("%1").arg((double)totalWeigh, 0, 'f'); // for large numbers
				//sizeStr = sizeStr.left( sizeStr.find('.') ); // get everything to a dot
				QString sizeStr = QString::number( totalWeigh );
				debug("__ Weigh=%s (%lld B), files=%d, dirs=%d", sizeStr.latin1(), totalWeigh, totalFiles, totalDirs);
				mListViewExt->setTextInColumn( ListViewExt::SIZEcol, sizeStr, mListViewExt->findName(processedFile) );
				mListViewExt->findName(processedFile)->repaint();
				mListViewExt->countAndWeightSelectedFiles( TRUE );
			}
		}
		else
		if ( operation == List ) {
			processedFile = mVirtualFS->currentURL();
			mStatusBar->clear( TRUE ); // hides progressBar
			mListViewExt->setCurrentURL( processedFile ); // should be set before (in VirtualFS::slotOpen)
			mPathView->slotPathChanged( processedFile );
			mStatusBar->slotUpdateOfListStatus( mListViewExt->numOfSelectedItems(), mListViewExt->numOfAllItems(), mListViewExt->weightOfSelected() );
			emit signalSetPanelBesideURL( processedFile );
// 			debug( "__ sel=%d, all=%d, w-all=%d", mListViewExt->numOfSelectedItems(), mListViewExt->childCount()-1, mListViewExt->weightOfSelected() );

			if ( mListViewExt->isListView() ) {
				if ( ! mVirtualFS->previousDirName().isEmpty() )
					mListViewExt->moveCursorToName( mVirtualFS->previousDirName() );
			}
			else { // TreeView
				QListViewItem *currentItem = mListViewExt->currentItem();
				if ( currentItem )
					if ( currentItem->firstChild() ) {
						mListViewExt->moveCursorToName( currentItem->firstChild()->text(0) );
						// need to open next dir of the tree
						QString nextDirForTree = mFinalPathOfTree;
						if ( nextDirForTree != processedFile ) {
							nextDirForTree = nextDirForTree.left( nextDirForTree.find('/', processedFile.length()+1) );
							mListViewExt->open( nextDirForTree );
						}
					}
			}
		}
		else
		if ( operation == Connect )
			mPathView->slotPathChanged( mVirtualFS->absHostURL() );
		else
		if ( operation == Find )
			mVirtualFS->slotStartFind( FindCriterion() );
		else
		if ( operation == Open )
			mVirtualFS->createFileView();

		// --- synchronization with second panel
		if ( operation == Copy || operation == Move || operation == MakeLink || operation == MakeDir || operation == Put || operation == Touch || operation == Remove ) {
			// 'Copy' and 'MakeLink' only for operation in the current panel
			selectedItemsList = mListViewExt->selectedItemsList();
			if ( operation != Remove )
				mListViewExt->ensureNameVisible( processedFile );
			// whether in the panel beside is opened dir. with path from current panel
			if ( mPanelBesideURL == currentURL ) {
				if ( operation == Remove || operation == Move )
					emit signalUpdateSecondPanel( ListViewExt::REMOVEitems, selectedItemsList, urlInfo );
				else
					emit signalUpdateSecondPanel( ListViewExt::INSERTitems, selectedItemsList, urlInfo );
			}
			if ( operation != Copy && operation != Move ) // NNN
				selectedItemsList->clear();
		}
		else
		if ( operation == SetAttributs || operation == Rename || operation == Open ) {
			selectedItemsList = mListViewExt->selectedItemsList();
			if ( mPanelBesideURL == currentURL && operation != Open )
				emit signalUpdateSecondPanel( ListViewExt::REMOVEitems | ListViewExt::INSERTitems, selectedItemsList, urlInfo );

			if ( operation != Open || (operation == Open && ! mListViewExt->numOfSelectedItems() && selectedItemsList->count() == 1) )
				selectedItemsList->clear();

			if ( operation == Rename )
				mListViewExt->ensureNameVisible( mVirtualFS->newAttributName() );
		}

		return;
	} // noError

	// ----- operation finished with an error
	debug("__ERROR: code=%d, host=%s, path=%s, currentURL()=%s, nameOfProcessedFile=%s",
		errorCode, mVirtualFS->host().latin1(), mVirtualFS->path().latin1(), mVirtualFS->currentURL().latin1(), processedFile.latin1() );

	if ( operation == Open || operation == SetAttributs || operation == Rename ) {
		if ( errorCode == NotExists )
			mListViewExt->removeItem( processedFile );
	}
	else
	if ( operation == Cd || operation == List ) {
		if ( processedFile.isEmpty() )
			return;
		if ( errorCode == NotExists || errorCode == CannotRead ) {
			QString newPath = FileInfoExt::filePath(processedFile);
			debug("__(modify) nameToRemove=%s, pathToRemove=%s, newPath=%s, kindOfCurrentFS=%d", FileInfoExt::fileName(processedFile).latin1(), processedFile.latin1(), newPath.latin1(), mVirtualFS->kindOfCurrentFS() );
			if ( mVirtualFS->kindOfCurrentFS() == LOCALfs ) {
				mPathView->slotRemovePath( processedFile ); // attempts to remove a path from the history list
				mPathView->slotPathChanged( newPath );
				mListViewExt->setCurrentURL( newPath ); // need to signalPathChanged sended from ListViewExt::eventFilter
			}
			if ( errorCode == NotExists ) {
				if ( currentURL == newPath )
					mListViewExt->removeItem( processedFile );
				else
					mListViewExt->open( newPath );
				mListViewExt->countAndWeightSelectedFiles();
				mStatusBar->slotUpdateOfListStatus( mListViewExt->numOfSelectedItems(), mListViewExt->numOfAllItems(), mListViewExt->weightOfSelected() );
			}
		}
	} // Cd
	else
	if ( operation == Copy || operation == Move || operation == Remove ) {
		if ( errorCode == BreakOperation ) {
			//QListViewItem *pItem = NULL;
			if ( operation != Remove ) {
				KindOfFilesSystem FSofProcessedFile = mVirtualFS->kindOfFilesSystem(FileInfoExt::filePath(processedFile)); // of src.file
				if ( mVirtualFS->kindOfCurrentFS() == LOCALfs ) // path = target path
					path = (operation == Move) ? currentURL : ((mPanelBesideURL != currentURL) ? mPanelBesideURL : QString(""));
				else
					path = (FSofProcessedFile != LOCALfs) ? currentURL : mPanelBesideURL;

				if ( path == FileInfoExt::filePath(processedFile) || currentURL != FileInfoExt::filePath(processedFile) ) {
					if ( FSofProcessedFile == LOCALfs && mVirtualFS->kindOfCurrentFS() != LOCALfs ) {
						debug("Not implemented!"); // copy/move to FTP (mayby this same like for LocalFS will be good)
					}
					else { // copy/move inside LOCALfs or copy/move to LOCALfs
						mVirtualFS->getNewUrlInfo( urlInfo );
						if ( mListViewExt->numOfSelectedItems() > 0 ) {
							selectedItemsList = mListViewExt->selectedItemsList();
							mListViewExt->setSelected(mListViewExt->findName(processedFile), FALSE); // file for breaked operation
							emit signalUpdateSecondPanel( ListViewExt::INSERTitems, selectedItemsList, urlInfo );
							mListViewExt->updateList( (operation == Copy) ? ListViewExt::DESELECTitems : ListViewExt::REMOVEitems );
							if ( mVirtualFS->afterBreakKeepFragment() || ((ListViewItemExt *)mListViewExt->findName(processedFile))->isDir() ) {
								selectedItemsList = NULL;
								emit signalUpdateSecondPanel( ListViewExt::INSERTitems, selectedItemsList, urlInfo );
							}
							else
								mListViewExt->setSelected(mListViewExt->findName(processedFile), TRUE); // back selecting for not copied/moved
						}
						else // one non selected item
						if ( mVirtualFS->afterBreakKeepFragment() ) {
							emit signalUpdateSecondPanel( ListViewExt::INSERTitems, selectedItemsList, urlInfo ); // selectedItemsList is null
						}
					}
				}
			}
			if ( operation == Remove ) {
				if ( mListViewExt->numOfSelectedItems() > 0 ) {
					selectedItemsList = mListViewExt->selectedItemsList();
					mListViewExt->setSelected(mListViewExt->findName(processedFile), FALSE); // file for breaked operation
					mListViewExt->updateList( ListViewExt::REMOVEitems );
				}
			}
		}
		else
		if ( errorCode == NotExists ) {
			if ( currentURL == FileInfoExt::filePath(processedFile) )
				mListViewExt->removeItem( processedFile );
		}
		if ( operation != Remove ) // need to update selected list when occures skip the copying files
			mListViewExt->countAndWeightSelectedFiles( TRUE );
	}
	else
	if ( operation == Find )
		mVirtualFS->slotStartFind( FindCriterion() ); // restores dlg.usable
	else
	if ( operation == Connect ) { // failed connections - create list with previous local fs directory
		createFS( mPathView->prevUrl(TRUE) );
		mListViewExt->moveCursorToName( processedFile );
	}
	else
	if ( operation == HostLookup ) // HostLookup have breaked by HostLookup
		createFS( mVirtualFS->absHostURL() );

	if ( operation == Copy || operation == Move || operation == Remove || operation == Open || operation == SetAttributs || operation == Rename )
		if ( mPanelBesideURL == currentURL && errorCode != BreakOperation )
			emit signalUpdateSecondPanel( ListViewExt::REMOVEitems, mListViewExt->selectedItemsList(), urlInfo );

	if ( operation != Copy && operation != Move )
		mListViewExt->clearSelectedItemsList();
}


void FilesPanel::slotInitPanelList()
{
	if ( ! mListViewExt->isVisible() )
		return;

	if ( mVirtualFS ) {
		if ( mVirtualFS->kindOfFilesSystem( mInitUrl ) == FTPfs ) {
			if ( ! FtpConnectDialog::showDialog(mInitUrl, this) ) {
				// User canceled connection
				// mInitUrl need to init, becouse it is checked into createFS()
				//debug("__ connection canceled");
				mInitUrl = mPathView->prevUrl(TRUE);
				createFS( mInitUrl ); // create list with previous localFS path
				return;
			}
		}
		mListViewExt->open( mInitUrl );
		// after start a focus must be in the left panel
		if ( QString(name()).find("Left") != -1 )
			setFocus();
	}
}


void FilesPanel::slotShowFtpManagerDlg()
{
	if ( ! hasFocus() && ! mPathView->hasFocus() ) // signal is sended to both panels
		return;

	QString newURL;
	if ( UrlsListManager::showDialog(newURL, UrlsListManager::FTP, mVirtualFS->path()) ) { // let's do connection
		mInitUrl = ""; // need to skip 'FtpConnectDialog' into 'createFS' fun.
		createFS( newURL );
		if ( mVirtualFS )
			mListViewExt->open( mVirtualFS->absHostURL() );
	}
}


void FilesPanel::slotShowDevicesMenu()
{
	if (! hasFocus() && ! mPathView->hasFocus()) // signal has been sended to both panels
		return;

	mDevicesMenu->showMenu();
}


void FilesPanel::slotShowSimpleDiscStat()
{
	if ( ! hasFocus() && ! mPathView->hasFocus() ) // signal is sended to both panels
		return;

	if ( mVirtualFS->kindOfCurrentFS() != LOCALfs )
		return;

	long long freeBytes, allBytes;
	mVirtualFS->getStatFS( mVirtualFS->currentURL(), freeBytes, allBytes ); // TODO in SystemInfo::getStatFS

	QString freeBytesStr  = formatNumber( freeBytes, BKBMBGBformat );
	QString totalBytesStr = formatNumber( allBytes, BKBMBGBformat );

	QPopupMenu *statDiskMenu = new QPopupMenu();
	statDiskMenu->setFocusPolicy( NoFocus );
//	statDiskMenu->insertTearOffHandle();
	statDiskMenu->insertItem( tr("Free bytes on current disk")+": "+freeBytesStr );
	statDiskMenu->insertItem( tr("Total bytes on current disk")+": "+totalBytesStr );
//	statDiskMenu->insertItem( tr("More information..."), this, SLOT(slotShowSimpleAllDiscsStat()), 0, 3 );
	statDiskMenu->insertItem( tr("More information ..."), 3 );
	statDiskMenu->setItemEnabled( 3, FALSE );
	statDiskMenu->exec( mListViewExt->mapToGlobal(QPoint(2,2)) );

	delete statDiskMenu;
}
/*
	void slotShowSimpleAllDiscsStat();
void FilesPanel::slotShowSimpleAllDiscsStat()
{
	QStringList discsList;
	SystemInfo::getAllowDisc( discsList );
}
*/


void FilesPanel::slotShowFavorites()
{
	if ( ! hasFocus() && ! mPathView->hasFocus() ) // signal is sended to both panels
		return;

	QString url;
	bool empty = FALSE;
	QSettings settings;
	QStringList favoritesList;
	uint i = 0, currHeight = 0, itHeight;

	QPopupMenu *favoritesMenu = new QPopupMenu();

	while( ! empty ) {
		url = settings.readEntry( QString("/qtcmd/Favorites/Url%1").arg(i) );
		if ( (empty=url.isEmpty()) )
			continue;

		favoritesMenu->insertItem( url.section( "\\", 0,0 ), i+101 );
		favoritesList.append( url.section("\\", 1,1) );
		i++;
		// --- adds toolTip with url's absolute path
		itHeight = favoritesMenu->sizeHint().height()/i;
		// WARNING: tip works correct only when cursor is over a text
		QToolTip::add( favoritesMenu,
		 QRect( 0, currHeight, favoritesMenu->width(), itHeight ),
		 favoritesList[i-1]
		);
		currHeight = favoritesMenu->sizeHint().height();
	}
	favoritesMenu->insertSeparator();
	favoritesMenu->insertItem( tr("&Configure favorites"), this, SLOT(slotShowFavoriteManager()) );
	favoritesMenu->insertItem( tr("&Add current URL"), this, SLOT(slotAddCurrentToFavorites()) );
	int urlId = favoritesMenu->exec( mListViewExt->mapToGlobal(QPoint(2,2)) );

	if ( urlId > 100 )
		slotPathChanged( favoritesList[urlId-101] );

	delete favoritesMenu;
}


void FilesPanel::slotShowFavoriteManager()
{
	if ( ! hasFocus() && ! mPathView->hasFocus() ) // signal is sended to both panels
		return;

	QString newURL;
	if ( UrlsListManager::showDialog(newURL, UrlsListManager::FAVORITES, mVirtualFS->path()) ) { // let's go to URL
		mInitUrl = ""; // need to skip 'FtpConnectDialog' into 'createFS' fun.
		createFS( newURL );
		if ( mVirtualFS )
			mListViewExt->open( mVirtualFS->absHostURL() );
	}
}


void FilesPanel::slotAddCurrentToFavorites()
{
	QString path = mVirtualFS->currentURL();
	QString newFavorite = path.section('/', path.contains('/')-1)+"\\"+path;
	QSettings settings;
	QStringList fl = settings.entryList( "/qtcmd/Favorites/" ); // get all favorites
	settings.writeEntry( QString("/qtcmd/Favorites/Url%1").arg(fl.count()), newFavorite );
}


void FilesPanel::slotShowFiltersManager()
{
	QString newPattern;
	if ( UrlsListManager::showDialog(newPattern, UrlsListManager::FILTERS, QString::null) ) // let's apply a new filter
		mListViewExt->applyFilter( newPattern, Filters::AllFiles );

	mFilters->updateFilters();
	updateFiltersMenu( mFilters ); // need to do for both panels
}


void FilesPanel::slotQuickGetOutFromFS()
{
	if ( ! hasFocus() && ! mPathView->hasFocus() ) // signal is sended to both panels
		return;

	if ( kindOfCurrentFS() != LOCALfs )
		mVirtualFS->slotOpen( mVirtualFS->host()+"../" );
}


void FilesPanel::slotFindFile()
{
	if ( ! hasFocus() && ! mPathView->hasFocus() ) // signal is sended to both panels
		return;

	mVirtualFS->showFindFileDialog();
}


void FilesPanel::slotPossibleDirWritable( DirWritable & dw )
{
	if ( mVirtualFS )
		dw = mVirtualFS->currentDirWritable();
}


void FilesPanel::slotPathChangedForTheViewPath( const QString & path )
{
	mVirtualFS->setCurrentURL( path );
	mPathView->slotPathChanged( path );
}


void FilesPanel::slotChangeFilter( int filterId )
{
	if ( ! mFilters || filterId == 1000 )
		return;

	mFilterId = filterId;
	mFilterButton->setTextLabel( mFiltersMenu->text(filterId)+"\n("+mFilters->patterns(filterId)+")" );

	Filters::DirFilter dirFilterTab[5] = {
	 Filters::AllFiles, Filters::DirsOnly, Filters::FilesOnly, Filters::SymLinks, Filters::HangSymLinks
	};
	Filters::DirFilter dirFilter = (filterId > (int)mFilters->baseFiltersNum()-1) ? Filters::AllFiles : dirFilterTab[filterId];

	mListViewExt->applyFilter( mFilters->patterns(filterId), dirFilter, mFilterForFiltered );
	// TODO mListViewExt->applyFilter(), need to synchronization with next panel
}


void FilesPanel::slotApplyListViewSettings( const ListViewSettings & lvs )
{
	// --- sets columns
	for (int i=0; i<6; i++) // 6 - number of all columns
		mListViewExt->header()->removeLabel( 0 );
	for (int i=0; i<6; i++) // 6 - number of all columns
		mListViewExt->setColumn( (ListViewExt::ColumnName)i, lvs.columnsWidthTab[i] );
	mListViewExt->setShowColumns( TRUE );

	// --- sets fonts and other list settings
	mListViewExt->setFontList( lvs.fontFamily, lvs.fontSize );
	mListViewExt->setShowIcons( lvs.showIcons );
	mListViewExt->setKindOfGroupFileSize( (KindOfFormating)lvs.fileSizeFormat );
	mListViewExt->setFocusAlwaysVisible( lvs.cursorAlwaysVisible );
	mListViewExt->setSelectFilesOnly( lvs.selectFilesOnly );
	mListViewExt->setHiddenItems( lvs.showHiddenFiles ); // for inserting items
	mListViewExt->applyFilter( (! lvs.showHiddenFiles) ? "" : "h", Filters::HiddenNot, lvs.filterForFiltered );

	// --- sets colors
	mListViewExt->setDblFrameCursor( lvs.twoColorsCursor );
	mListViewExt->setFullCursor( ! lvs.twoColorsCursor );
	if ( lvs.twoColorsCursor )
		mListViewExt->setDblFrameCursorColors( lvs.insideFrameCursorColor, lvs.outsideFrameCursorColor );
	else
		mListViewExt->setCursorColor( lvs.fullCursorColor );

	mListViewExt->setTextColor( lvs.itemColor );
	mListViewExt->setTextColorUnderCursor( lvs.itemColorUnderCursor ); // QColor(0,0,234)
	mListViewExt->setHighlightedTextColor( lvs.selectedItemColor );
	mListViewExt->setHighlightTextColorUnderCursor( lvs.selectedItemColorUnderCursor ); // QColor(87,186,0)
	mListViewExt->setHighlightBgColor( lvs.bgColorOfSelectedItems );
	mListViewExt->setBackgroundColor( lvs.firstBackgroundColor );

	mListViewExt->setShowGrid( lvs.showGrid );
	mListViewExt->setGridColor( lvs.gridColor );

	mListViewExt->setEnableTwoColorsOfBg( lvs.showSecondBackgroundColor );
	mListViewExt->setSecondColorOfBg( lvs.secondBackgroundColor );// QColor(255,235,220) 255,246,230;  204,255,212 - blady pomaranczowy, (blady zielony)

	// --- sets file colors
	mListViewExt->setCommonFileColor( lvs.commonFileColor );
	mListViewExt->setExecutableFileColor( lvs.exeFileColor );
	mListViewExt->setSymlinkColor( lvs.symlinkColor );
	mListViewExt->setBrokenSymlinkColor( lvs.brokenSymlinkColor );
	mListViewExt->setSocketColor( lvs.socketColor );
	mListViewExt->setCharacterDevColor( lvs.characterDevColor );
	mListViewExt->setBlockDevColor( lvs.blockDevColor );
	mListViewExt->setFifoColor( lvs.fifoColor );
}


void FilesPanel::slotApplyOtherAppSettings( const OtherAppSettings & /*oas*/ )
{
	// sets shortcuts and toolBar
}


void FilesPanel::slotApplyFileSystemSettings( const FileSystemSettings & fss )
{
	KindOfFilesSystem currentFS = mVirtualFS->kindOfCurrentFS();

	if ( currentFS == ARCHIVEfs ) {
		mVirtualFS->setWorkDirectoryPath( fss.workDirectoryPath );
	}
	else
	if ( currentFS == LOCALfs ) {
		mVirtualFS->setRemoveToTrash( fss.removeToTrash );
		mVirtualFS->setWeighBeforeOperation( fss.weighBeforeOperation );
		mVirtualFS->setTrashPath( fss.trashPath );
	}
	else
	if ( currentFS == FTPfs ) {
		mVirtualFS->setStandByConnection( fss.standByConnection );
		mVirtualFS->setNumOfTimeToRetryIfServerBusy( fss.numOfTimeToRetryIfFtpBusy );
	}
	// settings for all
	mVirtualFS->setAlwaysOverwrite( fss.alwaysOverwrite );
	mVirtualFS->setAlwaysSavePermission( fss.alwaysSavePermission );
	mVirtualFS->setAlwaysAskBeforeDeleting( fss.alwaysAskBeforeDeleting );
	mVirtualFS->setAlwaysGetInToDirectory( fss.alwaysGetInToDirectory );
	mVirtualFS->setCloseProgressDlgAfterFinished( fss.closeProgressDlgAfterFinished );
}

// ---------- EVENTs ----------

void FilesPanel::resizeEvent( QResizeEvent * )
{
	mPathView->setGeometry( 0,0, width(), 27 );
// 	mPathView->resize( width(), 25 );
	mListViewExt->setGeometry( 0, 25, width(), height()-(27+1+21) );
	mStatusBar->setGeometry( 0, mListViewExt->height()+26, width()-mFilterButton->width(), 21 );
	mFilterButton->setGeometry( mStatusBar->width(), mListViewExt->height()+26, mFilterButton->width(), 21 );
}
