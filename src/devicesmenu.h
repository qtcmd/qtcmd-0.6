/***************************************************************************
                          devices.h  -  description
                             -------------------
    begin                : Fri May 30 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 $Id$
 ***************************************************************************/

#ifndef DEVICES_H
#define DEVICES_H

#include <qdialog.h>
#include <qwidget.h>
#include <qprocess.h>
#include <qpopupmenu.h>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa obs�uguje menu z list� urz�dze� masowych.
 W klasie inicjowane jest menu z list� urz�dze� oraz stan ich zamontowania.
 Wykorzystywane s� do tego dwa pliki: /etc/fstab, /etc/mtab (stan zamontowania).
 Po wybraniu urz�dzenia przez u�ytkownika uruchamiana jest odpowiednia akcja.
 Mianowicie: je�li urz�dzenie jest zamontowane, wtedy nast�puje jego
 odmontowanie, je�eli nie jest zamontowane, wtedy podejmowana jest pr�ba
 zamontowania i trzecim przypadku, kiedy urz�dzenie jest zamontowane a
 u�ytkownik je wybierze, wtedy wysy�any jest sygna� @em signalPathChanged() ,
 kt�ry wymusza zmian� bie��cej �cie�ki.
*/
class DevicesMenu: public QPopupMenu
{
	Q_OBJECT
public:
	/** Konstruktor klasy.
	 * Utworzenie obiektu QProcess
	@param pParent - wska�nik na rodzica.
	 */
	DevicesMenu( QWidget *pParent );

	/** Destruktor.\n
	 Usuwane s� tu obiekty u�ywane w klasie.
	 */
	~DevicesMenu();

	/** Rodzaj operacji jaki mo�na wykona� na bie��cym elemencie menu.
	 */
	enum Operation { OPENDIR=0, MOUNT, UNMOUNT };

	/** Powoduje pokazanie menu z list� dost�pnych urz�dze�.\n
	 Pokazywane jest ono na pozycji (3,3) wzgl�dem obiektu rodzica. Po wybraniu
	 opcji uruchamiana jest odpowiednia akcja. Je�li urz�dzenie jest zamontowane,
	 wtedy nast�puje jego odmontowanie, je�eli nie jest zamontowane, wtedy
	 podejmowana jest pr�ba zamontowania i trzecim przypadku kiedy urz�dzenie jest
	 zamontowane a u�ytkownik je wybierze wtedy wysy�any jest tylko sygna�
	 @em signalPathChanged(), kt�ry wymusza zmian� bie��cej �cie�ki.
	 */
	void showMenu();

private:
	QValueList<bool> m_vlMountedTab;
	Operation m_eCurrentOperation;

	QProcess *m_pProcess;
	QDialog  *m_pInfoDlg;
	QString   m_sMountPath;

	bool m_bActionIsFinished;

	/**
	Wczytywane s� tutaj dwa pliki: /etc/fstab, /etc/mtab. Nast�pnie inicjowane
	jest menu list� dost�pnych urz�dze� oraz stanem ich zamot
	 @return status wczytania fstab i mtab
	 */
	bool init();

	/** Uruchamia (asynchronicznie) akcj� na, kt�r� wskazuje sk�adowa zawieraj�ca
	 polecenie.\n
	 Nast�pnie pokazywy jest dialog z pro�b� o cierpliwo�� i guzikiem
	 umo�liwiaj�cym przerwanie operacji.
	 */
	void action();

	/** Powoduje uruchamienie podanego polecenia z podanymi argumentami.\n
	 @param op - operacja do wykonania, patrz: @see Operation,
	 @param args - argumenty dla polecenia.
	 */
	void runProcess( Operation op, const QString & args );

private slots:
	/** Zawiera obs�ug� zako�czenia wykonywania polecenia.\n
	 Je�li podczas operacji wyst�pi� b��d, wtedy tutaj pokazywany jest stosowny
	 komunikat.
	 */
	void slotProcessExited();

	/** Powoduje przerwanie bie��cej operacji.
	 */
	void slotBreakProcess();

	/** Uruchamia proces odmontowywania bie��cego urz�dzenia.
	 */
	void slotUnmount();

	/** Uruchamia proces zamontowania bie��cego urz�dzenia.
	 */
	void slotMount();

signals:
	/** Sygna� wymuszaj�cy zmian� �cie�ki w bie��cym panelu na podan�.\n
	 @param path - nowa �cie�ka do katalogu.
	 */
	void signalPathChanged( const QString & path );

};

#endif
