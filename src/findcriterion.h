/***************************************************************************
                          findcriterion.h  -  description
                             -------------------
    begin                : wed oct 15 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _FINDCRITERION_H_
#define _FINDCRITERION_H_

#include <qdatetime.h>

/**
 *
 * Piotr Mierzwiński
 **/
class FindCriterion
{
public:
	enum CheckFileSize { NONE=0, EQUAL, ATLEAST, MAXIMUM };
	enum KindOfFiles { ALL=0, DIRECTORIES, FILES, LINKS, HANG_LINKS, PATTERN };

	QString name, location;
	bool caseSensitiveName;

	QString containText;
	bool caseSensitiveText, regExpText, includeBinFiles;

	bool checkTime;
	QDateTime firstTime, secondTime;

	CheckFileSize checkFileSize;
	long long fileSize;

	bool checkFileOwner;
	int ownerId;
	bool checkFileGroup;
	int groupId;

	KindOfFiles kindOfFiles;
	bool findRecursive, searchIntoSelected, stopIfXMatched;
	int stopAfterXMaches;

	bool isEmpty() const { return name.isEmpty(); }
};

#endif
