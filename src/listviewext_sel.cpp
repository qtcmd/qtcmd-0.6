// ------ This file contains only methods for supports selection of items

#include "listviewext.h"

#include <qregexp.h>
#include <qsettings.h>
#include <qinputdialog.h>


void ListViewExt::setSelected( QListViewItem * pItem, bool select )
{
	if ( pItem == NULL || pItem->text(0) == ".." /*|| pItem->isSelected() == select*/ )
		return;

	long long size = ((ListViewItemExt *)pItem)->entryInfo().size();

	// need to reset list when mNumOfSelectedItems is equal 0 and is added an item to the list (in collectAllSelectedNames())
	if ( ! mNumOfSelectedItems )
		mSelectedItemsList->clear();

	if ( pItem->isSelected() != select ) {
		mNumOfSelectedItems += (select) ? 1    : - 1;
		mWeightOfSelected   += (select) ? size : - size;

		QListView::setSelected( pItem, select );
	}

	if ( pItem->isSelected() )
		mSelectedItemsList->append( pItem );
	else
		mSelectedItemsList->remove( pItem );

	emit signalUpdateOfListStatus( mNumOfSelectedItems, numOfAllItems(), mWeightOfSelected );
}


void ListViewExt::countAndWeightSelectedFiles( bool addToTheSelectedItemsList )
{
	// need to deselect item '..', becouse it is selected in follow slots:
	//  slotSelectByUsePattern(), slotInvertSelections() and slotSelectAllItems()
	QListView::setSelected( firstChild(), FALSE );

	QListViewItem *item;
	QListViewItemIterator it( this );
	mNumOfSelectedItems = 0, mWeightOfSelected = 0;
	mSelectedItemsList->clear();

	while ( (item=it.current()) != 0 ) {
		if ( item->isSelected() ) {
			mNumOfSelectedItems++;
			if ( addToTheSelectedItemsList )
				mSelectedItemsList->append( item );
			mWeightOfSelected += ((ListViewItemExt *)item)->entryInfo().size();
		}
		it++;
	}

	emit signalUpdateOfListStatus( mNumOfSelectedItems, numOfAllItems(), mWeightOfSelected );
}


void ListViewExt::selectRange( QListViewItem *from, QListViewItem *to, bool clear, bool invert )
{ // function based on source of Qt-3.1 - QListView::clearRange()
	if ( ! from || ! to )
		return;

	// Swap
	if ( from->itemPos() > to->itemPos() ) {
		QListViewItem *temp = from;
		from = to;
		to = temp;
	}

	// Clear, select or invert items <from, to>
	for ( QListViewItem *i = from; i; i = i->itemBelow() ) {
		if ( invert )
			setSelected( i, ! i->isSelected() );
		else
			if ( clear ) {
				if ( i->isSelected() )
					setSelected( i, FALSE );
			}
			else
				if ( ! i->isSelected() )
					setSelected( i, TRUE );

		if ( i == to )
			break;
	}
}


void ListViewExt::collectAllSelectedNames( QStringList & selectedNamesList )
{
	selectedNamesList.clear();
// przy usuwaniu kat.w Ftp, do spr.czy usunac kat czy plik, dla rozpoznania jest potrzebny '/' na koncu nazwy kat., ktory trzeba tutaj dodac
// pozniej trzeba tez przejzec czy gdzies nie jest wykonywana op.dodawania '/' do elementu tutaj tworzonej listy
// najpierw szukac wywolania 'collectAllSelectedNames()'
	if ( mNumOfSelectedItems == 0 ) {
		if ( currentItemText() != ".." ) {
			//selectedNamesList += absolutePathForItem(0, TRUE); // 0 - current item
			selectedNamesList += absolutePathForItem(0, TRUE)+(((ListViewItemExt *)currentItem())->isDir() ? "/" : ""); // 0 - current item
			mSelectedItemsList->clear();
			mSelectedItemsList->append( currentItem() );
		}
		return;
	}
	else  { // selected is one or more items, let's store them on the selectedNamesList
		SelectedItemsList::iterator it;
		for (it = mSelectedItemsList->begin(); it != mSelectedItemsList->end(); ++it)
			if ( (*it)->isSelected() )
				selectedNamesList += absolutePathForItem( (*it), TRUE ); // get path+name
	}
}

// ----------- SLOTs ------------

void ListViewExt::slotSelectCurrentItem()
{
	setSelected( currentItem(), (currentItem()->isSelected() ? FALSE : TRUE) );
}


void ListViewExt::slotSelectGroupOfItems( bool clear )
{
	QSettings settings;

	QStringList templateList = settings.readListEntry( "/qtcmd/FilesSystem/TemplateSelection" );
	if ( templateList.isEmpty() )
		settings.writeEntry( "/qtcmd/FilesSystem/TemplateSelection", "*" );


	QString dlgName = clear ? tr("UnSelect") : tr("Select");
	dlgName += " " + tr("group of files");

	bool ok = FALSE;
	QString newTemplate = QInputDialog::getItem(
	 dlgName, tr( "Please to give new template:" ),
	 templateList, 0, TRUE, &ok, this
	);

	if ( ok )  {
		bool templateExists = FALSE;
		QStringList::Iterator it;
		for ( it = templateList.begin(); it != templateList.end(); ++it )
			if ( newTemplate == *it )  {
				templateExists = TRUE;
				break;
			}

		// -- selected or a new template should be always on the top a list
		if ( ! templateExists )
			templateList.insert( templateList.begin(), newTemplate );
		else {
			QString selectedTmpl = *it;
			templateList.remove( it );
			templateList.insert( templateList.begin(), selectedTmpl );
		}
		settings.writeEntry( "/qtcmd/FilesSystem/TemplateSelection", templateList );

		if ( newTemplate.find( '/' ) > 0 )  {  // is more that one templates
			QStringList currentPatternList = QStringList::split( "/", newTemplate );
			for( uint i=0; i<currentPatternList.count(); i++ )
				slotSelectByUsePattern( !clear, currentPatternList[ i ] );
		}
		else  // a new template
			slotSelectByUsePattern( !clear, newTemplate );
	}
}


void ListViewExt::slotSelectAllItems( bool clear )
{
	slotSelectByUsePattern( !clear, "*" );
}


void ListViewExt::slotInvertSelections()
{
	slotSelectByUsePattern( mInvSelection, "*" );
	mInvSelection = !mInvSelection;
}


void ListViewExt::slotSelectByUsePattern( bool select, const QString & pattern )
{
	QListViewItemIterator it( this );
	QRegExp expr( pattern, TRUE, TRUE );

	while ( it.current() != 0 )  {
		if (  expr.exactMatch( it.current()->text(0) ) )
			if ( mSelectFilesOnly ) {
				if ( ! ((ListViewItemExt *)*it)->isDir() )
					setSelected( it.current(), select );
			}
			else
				setSelected( it.current(), select );
		it++;
	}
}


void ListViewExt::slotSelectWithThisSameExtension( bool clear )
{
	QString currentExt, currentName = currentItem()->text(0);
	QString ext = currentName.right( currentName.length() - currentName.findRev('.') - 1 );
	QListViewItemIterator it( this );

	while ( it.current() != 0 )  {
		currentName = it.current()->text(0);
		currentExt = currentName.right( currentName.length() - currentName.findRev('.') - 1 );
		if ( currentExt == ext )
			if ( mSelectFilesOnly ) {
				if ( ! ((ListViewItemExt *)*it)->isDir() )
					setSelected( it.current(), !clear );
			}
			else
				setSelected( it.current(), !clear );
		it++;
	}
}
