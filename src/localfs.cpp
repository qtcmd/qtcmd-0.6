/***************************************************************************
                          localfs.cpp  -  description
                             -------------------
    begin                : mon oct 28 2002
    copyright            : (C) 2002, 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project

$Id$
 ***************************************************************************/

#include <qcheckbox.h>
#include <qsettings.h>
#include <qapplication.h>

#include "plugins/view/text/view.h"

#include "fileinfoext.h"
#include "messagebox.h"
#include "virtualfs.h"
#include "localfs.h"


LocalFS::LocalFS( const QString & inputURL, ListViewExt *listViewExt )
	: VirtualFS( inputURL, listViewExt )
{
	debug("LocalFS::LocalFS(), inputURL=%s", inputURL.latin1());
	mSoLF = new SoLF( this );

	mCurrentPath = inputURL;
	mFileHasBeenOpened = FALSE;

	QSettings *settings = new QSettings;
	mTrashDirectory = settings->readEntry( "/qtcmd/LFS/TrashDirectory", QString::null );
	mRemoveToTrash  = settings->readBoolEntry( "/qtcmd/LFS/AlwaysRemoveToTrash", FALSE );
	mWeighBeforeOperation = settings->readBoolEntry( "/qtcmd/LFS/AlwaysWeighBeforeOperation", TRUE );
	delete settings;

	connect( mSoLF, SIGNAL(done(bool)),                 SLOT(slotDone(bool)) );
	connect( mSoLF, SIGNAL(stateChanged(int)),          SLOT(slotStateChanged(int)) );
	connect( mSoLF, SIGNAL(commandStarted(int)),        SLOT(slotCommandStarted(int)) );
	connect( mSoLF, SIGNAL(commandFinished(int,bool)),  SLOT(slotCommandFinished(int,bool)) );
//	connect( mSoLF, SIGNAL(listInfo(const UrlInfoExt &)), SIGNAL(signalListInfo(const UrlInfoExt &)) );
	connect( mSoLF, SIGNAL(listInfo(const UrlInfoExt &)), SLOT(slotListInfo(const UrlInfoExt &)) );

	connect( mSoLF, SIGNAL(signalNameOfProcessedFile(const QString &, const QString &)),
		this, SIGNAL(signalNameOfProcessedFile(const QString &, const QString &)) );
	connect( mSoLF, SIGNAL(signalFileCounterProgress(int, bool, bool)),
		this, SIGNAL(signalFileCounterProgress(int, bool, bool)) );

	connect( mSoLF, SIGNAL(signalTotalProgress(int, long long)),
		this, SIGNAL(signalTotalProgress(int, long long)) );
	connect( mSoLF, SIGNAL(signalDataTransferProgress(long long, long long, uint)),
		this, SIGNAL(signalDataTransferProgress(long long, long long, uint)) );

	connect( mSoLF, SIGNAL(signalFileOverwrite(const QString &, QString &, int &)),
	 this, SLOT(slotShowFileOverwriteDlg(const QString &, QString &, int &)) );
	connect( mSoLF, SIGNAL(signalDirectoryNotEmpty(const QString &, int &)),
	 this, SLOT(slotShowDirNotEmptyDlg(const QString &, int &)) );

	connect( mSoLF, SIGNAL(signalInsertMatchedItem(const UrlInfoExt &)),
	 this, SLOT(slotInsertMatchedItem(const UrlInfoExt &)) );
	connect( mSoLF, SIGNAL(signalUpdateFindStatus(uint, uint, uint)) ,
	 this, SLOT(slotUpdateFindStatus(uint, uint, uint)) );
}


LocalFS::~LocalFS()
{
	delete mSoLF;  mSoLF = 0;
	removeProgressDlg(); // only if exists
}


void LocalFS::readDir( const QString & fullPathName )
{
	mSoLF->list( fullPathName );
	if ( mSoLF->errorCode() == NoError )
		mSkipNextErrorOccures = FALSE;
}


void LocalFS::open( const QString & fileName )
{
	debug("LocalFS::open, fileName=%s", fileName.latin1() );
	mSkipNextErrorOccures = FALSE;
	mFileHasBeenOpened = FALSE;

	bool noError = mSoLF->open( absoluteFileName(fileName) );
	slotCommandFinished( Open, !noError );
	//emit signalResultOperation( Open, noError );

	if ( noError )
		emit signalReadyRead();
}


bool LocalFS::fileIsReadable( const QString & fileName, bool silent )
{
	// checks whether a file exists and is readable
	bool noError = mSoLF->open( absoluteFileName(fileName) );
	if ( ! noError && ! silent ) // an error occures
		MessageBox::critical( this, tr("Cannot to read this file") );

	return noError;
}


void LocalFS::slotReadFileToView( const QString & fileName, QByteArray & buffer, int & bytesRead, int fileLoadMethod )
{
	if ( ! bytesRead )
		return;

	if ( ! mFileHasBeenOpened ) {
		if ( mFile.name() != fileName )
			mFile.setName( fileName );
		if ( ! mFile.open(IO_ReadOnly) )
			return;
		else
			mFileHasBeenOpened = TRUE;
	}

	if ( fileLoadMethod == View::ALL || bytesRead < 0 ) { // read all bytes of file
		buffer    = mFile.readAll();
		bytesRead = buffer.size()+1;
		buffer.resize( bytesRead );
		mFileHasBeenOpened = FALSE;
		mFile.close();
	}
	else
	if ( fileLoadMethod == View::APPEND ) { // read file by parts
		buffer.resize( bytesRead+1 ); // +1 for null character (need to text files)
		int bRead = mFile.readBlock( buffer.data(), bytesRead );

		if ( bRead == 0 ) {
			mFile.close();
			mFileHasBeenOpened = FALSE;
		}
		else
		if ( bRead < 0 ) { // an error occures
			mFile.close();
			slotDone( TRUE );
			mFileHasBeenOpened = FALSE;
		}

		bytesRead = bRead;
	}
}


void LocalFS::createAnEmptyFile( const QString & fullFileName )
{
	mSkipNextErrorOccures = FALSE;

	if ( fullFileName.at(fullFileName.length()-1) == '/' ) // has given dir name
		mSoLF->makeDir( fullFileName );
	else { // has given file name
		mSoLF->touch( fullFileName );
		if ( mSoLF->errorCode() == NoError )
			openFile( fullFileName, TRUE );
	}
}


void LocalFS::mkdir( const QString & dirName )
{
	mSkipNextErrorOccures = FALSE;
	mSoLF->makeDir( dirName );
}


void LocalFS::copyFiles( QString & targetPath, const QStringList & filesList, bool savePerm, bool alwaysOvr, bool followByLink, bool move )
{
	mOpFileCounter = 0;
	mTotalFiles = filesList.count();
	debug("LocalFS::copyFiles, move=%d, targetPath=%s, toCopied=%d", move, targetPath.latin1(), mTotalFiles );
	mSkipNextErrorOccures = FALSE;

	// --- check whether it's copying to new file (eg.to this same dir.)
	bool copyingToNewFile = FALSE;
	QFileInfo file( targetPath );
	if ( ! file.exists() )
		copyingToNewFile = TRUE;
	else
	if ( file.isDir() ) {
		if ( targetPath != host() )
			targetPath += "/";
	}
	else
		copyingToNewFile = TRUE;

	if ( move ) // (if tree view then everyone file can have a different path)
		for ( uint i=0; i<filesList.count(); i++ )
			if ( targetPath == FileInfoExt::filePath(filesList[i]) ) {
				copyingToNewFile = TRUE;
				break;
			}

	QString targetFileName;
	if ( copyingToNewFile ) {
		if ( mTotalFiles > 1 ) {
			QString actionStr = (move) ? tr("Cannot rename more than one file") : tr("Cannot to copy more than one file to this same directory");
			MessageBox::critical( this,
			 QString::number(mTotalFiles)+" "+tr("files selected")+"\n\n"+
			 actionStr+" !"
			);
			return;
		}
		targetFileName = FileInfoExt::fileName( targetPath );
		if ( targetPath.at(targetPath.length()-1) == '/' )
			targetPath = FileInfoExt::filePath( targetPath );
		else
			targetPath = FileInfoExt::filePath( filesList[0] );

		initNewUrlInfo( filesList[0] ); // inits member: 'mNewUrlInfo'
		mNewUrlInfo.setName(targetFileName);
	}

	// --- if a directory that have'nt write permission (check for one file)
	if ( move )
		if ( mTotalFiles == 1 )
			if ( ! QFileInfo(FileInfoExt::filePath(absoluteFileName(filesList[0]))).isWritable() ) {
				MessageBox::critical( this,
				 "\""+filesList[0]+"\""+"\n\n"+ tr("Cannot remove this file")+
				 " !\n"+tr("Permission denied")+"."
				);
			}
	// --- if copying/moving to this same directory
	if ( targetPath == FileInfoExt::filePath(filesList[0]) )
		if ( mTotalFiles == 1 )
			if ( move ) {
				changeAttribut( ListViewExt::NAMEcol, targetPath+targetFileName );
				return;
			}

	mSoLF->setSavePermission( savePerm );
	mSoLF->setAlwaysOverwrite( alwaysOvr );
	mSoLF->setFollowByLink( followByLink );
	mSoLF->setWeightBefore( TRUE, TRUE ); // for directory recursive weighing

	bool existsFilesToProcessed = FALSE;
	for( uint i=0; i<mTotalFiles; i++) {
		targetFileName = targetPath+((copyingToNewFile) ? targetFileName : FileInfoExt::fileName(filesList[i]));
		if ( filesList[i] != targetPath && filesList[i]+"/" != targetPath ) { // check file and dir
			mSoLF->copy( filesList[i], targetFileName, move );
			existsFilesToProcessed = TRUE;
		}
		else {
		// early, into VirtualFS::copyFilesTo() fun. this file or directory is deselected and removed from the SelectedList
			QString opStr = (move) ? tr("move") : tr("copy");
			MessageBox::critical( this,
			 tr("Source file :")+"\n\t\""+filesList[i]+"\"\n"+
			 tr("Target file :")+"\n\t\""+targetPath+"\"\n\n"+
			 tr("Cannot")+" "+opStr+" "+tr("this same to this same")+" !\n\n"+
			 tr("This file will be passing over.")
			);
		}
	}
	if ( existsFilesToProcessed )
		showProgressDialog( Copy, move, targetPath );
}


void LocalFS::deleteFiles( const QStringList & filesList )
{
	mSkipNextErrorOccures = FALSE;
	mTotalFiles = filesList.count();
	mOpFileCounter = 0;

	mSoLF->setRemoveToTheTrash( mRemoveToTrash );
	mSoLF->setTrashDir( mTrashDirectory );
	mSoLF->setWeightBefore( mWeighBeforeOperation, TRUE ); // for directory recursive weighing

	showProgressDialog( Remove );

	for (uint i=0; i<mTotalFiles; i++)
		mSoLF->remove( filesList[i] );
}


void LocalFS::sizeFiles( const QStringList & filesList, bool realWeight )
{
	mSkipNextErrorOccures = FALSE;
	mSoLF->setRealWeightInFS( realWeight );
	QString sFileName;
	QFileInfo fi;
	if (filesList.count() == 1) {
		// need to remove last slash for QFileInfo
		sFileName = VirtualFS::absoluteFileName(filesList[0]);
		if ( sFileName.at(sFileName.length()-1) == '/') // remove last slash
			sFileName.remove(sFileName.length()-1, 1);
		fi.setFile(sFileName);
		//if (fi.isDir() && ! fi.isSymLink()) // dir
			mSoLF->weigh(VirtualFS::absoluteFileName(filesList[0]));
	}
	else {
		for (uint i=0; i<filesList.count(); i++) {
			sFileName = VirtualFS::absoluteFileName(filesList[i]);
			if (sFileName.at(sFileName.length()-1) == '/') // remove last slash
				sFileName.remove(sFileName.length()-1, 1);
			fi.setFile(sFileName);
			if (fi.isDir() && ! fi.isSymLink()) // dir
				mSoLF->weigh(sFileName);
			//else
			//	mSoLF->size(sFileName);
		}
	}
}


void LocalFS::makeLink( const QStringList & sourceFilesList, const QStringList & targetFilesList, bool editOnly, bool hard, bool alwaysOvr )
{
	mSkipNextErrorOccures = FALSE;
	mTotalFiles = sourceFilesList.count();
	mOpFileCounter = 0;

	mSoLF->setHardLink( hard );
	mSoLF->setAlwaysOverwrite( alwaysOvr );
	mSoLF->setWeightBefore( TRUE, TRUE ); // for directory recursive weighing

	if ( editOnly )
		mSoLF->remove( targetFilesList[0] );

	for (uint i=0; i<mTotalFiles; i++)
		mSoLF->makeLink( sourceFilesList[i], targetFilesList[i], editOnly );
}


void LocalFS::setAttributs( const QStringList & filesList, const UrlInfoExt & newUrlInfo, bool recursive, int changesFor )
{
	mOpFileCounter = 0;
	mTotalFiles = filesList.count();
	mSkipNextErrorOccures = FALSE;
	bool isRename = FALSE;

	mNewUrlInfo = newUrlInfo; // if an error ocurres then member is cleared (in slotDone())
	mNewUrlInfo.setName( FileInfoExt::fileName(newUrlInfo.name()) ); // TODO moznaby to przeniesc do getNewUrlInfo()

	if ( ! newUrlInfo.name().isEmpty() ) {
		// not asynchronously operation (instant effect on the list)
		slotCommandFinished( Rename, mSoLF->rename(filesList[0], newUrlInfo.name()) );
		isRename = TRUE;
	}

	if ( newUrlInfo.permissions() < 0 &&
	 newUrlInfo.owner().isEmpty() && newUrlInfo.group().isEmpty() &&
	 newUrlInfo.lastRead().isNull() && newUrlInfo.lastModified().isNull()
	)
		return;

	mSoLF->setPermissionForAll( newUrlInfo.permissions() );
	if ( ! mSoLF->setOwnerAndGroupForAll(newUrlInfo.owner(), newUrlInfo.group()) ) {
		slotCommandFinished( SetAttributs, TRUE ); // error occures (not found Id), show announcement
		return;
	}
	// FIXME need to check is new id (or group name) exists on the group list of logged user
	// if not, then show error

	mSoLF->setAccessAndModificationTimeForAll( newUrlInfo.lastRead(), newUrlInfo.lastModified() );
	mSoLF->setRecursiveAttributs( recursive );
	mSoLF->setChangesForAll( changesFor );

	if ( filesList.count() == 1 )
		if ( QFileInfo(filesList[0]).isDir() && recursive && ! QFileInfo(filesList[0]).isSymLink() )
			showProgressDialog( SetAttributs );

	mSoLF->setWeightBefore( TRUE, recursive ); // for directory set 'recursive' weighing


	for (uint i=0; i<mTotalFiles; i++)
		if ( isRename )
			mSoLF->setAttributs( newUrlInfo.name() );
		else
			mSoLF->setAttributs( filesList[i] );
}


void LocalFS::breakCurrentOperation()
{
	mSoLF->breakCurrentOperation();
	initNewUrlInfo(mSoLF->nameOfProcessedFile());
}


void LocalFS::startFindFile( const FindCriterion & findCriterion )
{
	if ( ! findCriterion.isEmpty() ) {
		QString location = findCriterion.location;
		QFileInfo f(location);
		if ( ! f.exists() )
			MessageBox::critical( this, location+"\n\n\t"+tr("This directory not exists !") );
		else
			if ( ! f.isDir() || f.isSymLink() )
				MessageBox::critical( this, location+"\n\n\t"+tr("This is not directory !") );
			else {
				// TODO when dir will be weighed before find then can show progress of find (slowly finding)
				mSoLF->setWeightBefore( FALSE /*TRUE*/ ); // for directory recursive weighing
				mSoLF->findFile( findCriterion );
			}
	}
	else
		mSoLF->breakCurrentOperation();
}


int LocalFS::getOwners( bool getUser, QStringList & list, bool getAll, int id, bool returnId )
{
	QString name = list[0], gName;
	bool superUser = (::getuid() == 0) ? TRUE : FALSE;
	int newId = (getUser) ? ::getuid() : ::getgid(), retId = -1;
	enum KindOfReturn { RETURNidForName, RETURNallNames, RETURNnameForId } kindOfReturn;

	// --- preparing
	if ( returnId ) {
		if ( name.isEmpty() ) // return current logged user/group Id
			return newId;
		else { // return Id for passed (by 'name') user/group
			if ( id >= 0 ) // return name for passed 'id'
				newId = id;
		}
		kindOfReturn = RETURNidForName;
	}
	else { // return user/group name
		if ( id >= 0 ) // return current logged name of user/group for passed 'id'
			newId = id;
		kindOfReturn = (getAll) ? RETURNallNames : RETURNnameForId;
	}

	if ( kindOfReturn == RETURNidForName )
		name = ","+list[0]+","; // need to making correct compare

	// --- read file
	list.clear();

	QString fileName = (getUser) ? "/etc/passwd" : "/etc/group";
	QFile file( fileName );
	if ( ! file.exists() )  {
		MessageBox::critical( 0, tr("Error")+" - QtCommander",
		 fileName+"\n\n"+ tr("This file not exists !")
		);
		return -1;
	}

	QString lineBuffer;
	if ( file.open(IO_ReadOnly) ) {
		QTextStream ts( &file );
		while ( ! ts.eof() ) {  // until end of file...
			lineBuffer = ts.readLine();  // line of text excluding '\n'
			if ( lineBuffer.isEmpty() || lineBuffer.at(0) == '#' ) // skip empty and comment line
				continue;
			if ( kindOfReturn == RETURNidForName ) {
				if ( (","+lineBuffer.section(':', 0,0)+",") == name ) {
					retId = (lineBuffer.section(':', 2,2)).toInt();
					break;
				}
			}
			else
			if ( kindOfReturn == RETURNnameForId ) {
				if ( newId == (lineBuffer.section(':', 2,2)).toInt() ) {
					list = lineBuffer.section(':', 0,0);
					break;
				}
			}
			else
			if ( kindOfReturn == RETURNallNames ) {
				if ( superUser )
					list.append( lineBuffer.section(':', 0,0) );
				else {
					if ( getUser ) {
						if ( newId == (lineBuffer.section(':', 2,2)).toInt() ) {
							list = lineBuffer.section(':', 0,0);
							break;
						}
					}
					else { // get group(s)
						gName = lineBuffer.section( ':', 0,0 );
						lineBuffer = ","+lineBuffer.section( ':', 3,3 )+",";
						if ( lineBuffer.find(name) != -1 )
							list.append( gName );
					}
				} // common user
			}
		} // while
		file.close();
	} // if ( file.open() )
	else
		MessageBox::critical( 0, tr("Error")+" - QtCommander",
		 fileName+"\n\n"+ tr("Cannot open this file !")
		);

	return retId;
}


QString LocalFS::getUser( bool user, int id )
{
	QStringList list;

	getOwners( user, list, FALSE, id );

	return list[0];
}


void LocalFS::initNewUrlInfo( const QString & fileName )
{
	QString fFileName = (fileName.isEmpty()) ? mSoLF->nameOfProcessedFile() : fileName;

	QFileInfo f( fFileName );
	//FileInfoExt f( fFileName );
	//bool isEmptyDir = f.isEmpty();

	bool isEmptyDir = FALSE;
	if ( f.isDir() && ! f.isSymLink() )
		if ( f.size() == 48 ) // it's true only for non Win/DOS FileSystems.
			isEmptyDir = TRUE;

	UrlInfoExt URLinfo( FileInfoExt::fileName( fFileName ),
	 f.isDir() ? 0 : (f.isSymLink() ? f.readLink().length() : f.size()),
	 FileInfoExt::permission(fFileName),
	 f.owner(),        f.group(),
	 f.lastModified(), f.lastRead(),
	 f.isDir(),        f.isFile(),  f.isSymLink(),
	 f.isReadable(),   f.isExecutable(), isEmptyDir // f.isEmpty()
	);

	mNewUrlInfo = URLinfo;
	//mNewUrlInfo = f.urlInfoExt(); // wtedy nie byloby potrzebne powyzsze tw.obiektu i spr.czy pusty
}

			// ------------ SLOTs --------------

void LocalFS::slotCommandStarted( int cmd )
{
// 	debug("LocalFS::slotCommandStarted(), operation=%s, command=%s", cmdString(mSoLF->currentOperation()).latin1(), cmdString((Operation)cmd).latin1() );
	if ( cmd == List ) {
		debug("\tstart listing dir, path=%s", mSoLF->nameOfProcessedFile().latin1() );
		QApplication::setOverrideCursor( waitCursor );
		initializeShowsList(); // min. ustawia zmienna korzenia dla drzewa
	}
	else {
		if ( cmd != Weigh ) {
			if ( ! mOpFileCounter )
				setOperationHasFinished( FALSE );
			mOpFileCounter++;
		}
	}
}


void LocalFS::slotCommandFinished( int cmd, bool error )
{
// 	debug("LocalFS::slotCommandFinished(), command=%s, operation= %s, error=%d", cmdString((Operation)cmd).latin1(), cmdString(operation).latin1(), error );
	bool singleFileOperation = (cmd == Open || cmd == Rename || cmd == MakeDir || cmd == Put || cmd == Touch);

	if ( ! error ) { // no error
		if ( cmd == Cd ) { // need to inits member mCurrentURL in VirtualFS
			setCurrentURL( (mCurrentPath=mSoLF->nameOfProcessedFile()) );
			mSkipNextErrorOccures = FALSE;
		}
		Error error = mSoLF->errorCode(); // NNN
		if ( error == BreakOperation ) // for removed skipped file from the selected list
			if ( (cmd == Copy && error != CannotSetAttributs) || cmd == Move || cmd == Remove || cmd == MakeLink )
				removeItemFromSIL();
	}
	else { // an error occured
		Error error = mSoLF->errorCode();
		if ( error == BreakOperation ) // NNN
			if ( (cmd == Copy && error != CannotSetAttributs) || cmd == Move )
				removeItemFromSIL(); // for removed skipped file from the selected list

		startProgressDlgTimer( FALSE ); // stop timer
	// --- show an error announcement
		if ( error != CannotSetAttributs )
			mNewUrlInfo.clear();

		if ( ! mSkipNextErrorOccures ) {
			int errorNum = mSoLF->errorNumber();
			QString fileName = mSoLF->nameOfProcessedFile(); // errorFileName
			bool isFile = (fileName.at(fileName.length()-1) != '/');
			QString kindOfFileStr = (isFile) ? tr("file") : tr("directory"); // all symlinks are treats as files
			QString message = "\""+fileName+"\""+"\n\n\t";

			switch ( error ) {
				case NotExists:
					message += tr("This")+" "+kindOfFileStr+" "+tr("not exists")+" !";
					break;
				case NoFreeSpace:
					message += tr("Not enough free space in target directory")+" !";
					break;
				case ReadError:
					message += tr("Cannot read this")+" "+kindOfFileStr+" !\n"+tr("Read error")+
								     " = "+QString::number(errorNum);
					break;
				case CannotRead:
					message += tr("Cannot read this");
					break;
				case CannotWrite:
					message += (cmd == Remove) ? tr("Cannot to remove this file") : tr("Cannot write to this");
					break;
				case CannotRemove:
					message += tr("Cannot remove this");
					break;
				case CannotSetAttributs:
					message += tr("Cannot set attributs for this");
					break;
				case CannotCreateLink:
					message += tr("Cannot create this link");
					break;
				case AlreadyExists:
					message += tr("This")+" "+kindOfFileStr+" "+tr("already exists")+" !";
					break;
				default:
					message += tr("Error number: ")+QString::number(errorNum);
					break;
			}
			if ( error == CannotRead || error == CannotWrite || error == CannotRemove )
				message += " "+kindOfFileStr+" !\n\t"+tr("Permission denied")+".";
			else
			if ( error == CannotSetAttributs || error == CannotCreateLink )
				message += " "+kindOfFileStr+" !\n\t"+tr("Operation not permitted")+".";

			if ( cmd == Rename && error == AlreadyExists ) {
				int result = MessageBox::yesNo( this,
				 tr("Overwrite file")+" - QtCommander",
				 message+"\n\t"+tr("Overwrite it by")+"\n\n\""+currentFileName()+"\" ?",
				 MessageBox::Yes
				);
				if ( result == MessageBox::Yes ) {
// 					mSoLF->remove( fileName );
// 					mSoLF->rename( currentFileName(), fileName ); // zrobic jako asynchr
					// --- rozw.tymczasowe
					if ( QFile::remove(fileName) ) {
						emit signalResultOperation( Remove, !error ); // removes an item from list
						bool stat = mSoLF->rename( currentFileName(), fileName );
						emit signalResultOperation( Rename, !stat );
					}
					else
						MessageBox::critical( this,
						 FileInfoExt::filePath(fileName)+"\n\n\t"+tr("Cannot write to this directory !")
						);
					return;
				}
			}
			else
			if ( error != BreakOperation ) {
				if ( isFile && singleFileOperation )
					MessageBox::critical( this, message );
				else {
					if (MessageBox::critical( this, message, TRUE, tr("Don't show next error") ) != 1) // == 101
						mSkipNextErrorOccures = TRUE;
					/*int result = MessageBox::yesNo( this,
						tr("Error")+" - QtCommander",
						message+"\n\n\t"+tr("Do you want to see next error announcement")+" ?",
						MessageBox::No
					);
					if ( result == MessageBox::No )
						mSkipNextErrorOccures = TRUE;
					*/
				}
			}
		} // if not skip all errors
		if ( cmd == Find )
			slotStartFind( FindCriterion() );
	} // error occured

	if ( singleFileOperation )
		slotDone( error );
	else
		startProgressDlgTimer( TRUE );
}


void LocalFS::slotDone( bool error )
{
	QApplication::restoreOverrideCursor();
	Operation operation = mSoLF->currentOperation();
	debug("LocalFS::slotDone(), errorStat=%d, errno=%d, operation=%s", error, mSoLF->errorNumber(), cmdString(operation).latin1() );

	startProgressDlgTimer( FALSE ); // stop timer
	setOperationHasFinished( TRUE );

	if ( ! error || (operation == Copy && mSoLF->errorCode() == CannotSetAttributs) ) {
		switch( operation ) {
			case Copy:  // copying (moving)/removing/setting attributs has been finished
			case Move:
			case Weigh:
			case Remove:
			case SetAttributs:
				// --- hide progress dialog only if user selected it (in this dialog)
				removeProgressDlg( TRUE );
				break;

			case Touch:
				//openFile( mSoLF->nameOfProcessedFile(), TRUE ); // open empty new file into view
			case Put: // Put is used during EditLink operation, too
			case MakeDir:
				initNewUrlInfo();
				break;

			default: break;
		}
	}

	emit signalResultOperation( operation, !error );
}


void LocalFS::slotStateChanged( int state )
{
	emit signalSetOperation( (Operation)state ); // set info about an operation on the progress dialog
}


void LocalFS::slotUpdateItemOnLV( const QString & fileName )
{
	mSoLF->touch( fileName );
}


void LocalFS::slotGetUrlInfo( const QString & fullFileName, UrlInfoExt & urlInfoExt )
{
	urlInfoExt.setName( fullFileName );
	urlInfoExt.setSize( QFileInfo(fullFileName).size() );
	urlInfoExt.setLastModified( QFileInfo(fullFileName).lastModified() );
}


void LocalFS::slotPause( bool stop )
{
	mSoLF->pauseCurrentOperation( stop ); // or start
}
