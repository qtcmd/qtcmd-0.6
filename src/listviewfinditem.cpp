/***************************************************************************
                          listviewfinditem.cpp  -  description
                             -------------------
    begin                : Thu Nov 30 2000
    copyright            : (C) 2000 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "listviewfinditem.h"

#include <qlineedit.h>
#include <qlabel.h>


ListViewFindItem::ListViewFindItem( QListView *parent, const char *name, WFlags fl )
 : QFrame( parent, name, fl ), mListView( parent )
{
	setFrameStyle( QFrame::WinPanel | QFrame::Raised );

	QFont fontWeight;
	fontWeight.setWeight( QFont::Bold );

	QLabel *label = new QLabel( tr("Search")+":", this );
	label->setGeometry( 5, 5, 74, 22 );
	label->setFont( fontWeight );

	mLineEdit = new QLineEdit( this );
	mLineEdit->setGeometry( 5+70, 5, 170, 22 );

	connect( this, SIGNAL(signalReturnPressed(QListViewItem *)), parent, SIGNAL(returnPressed(QListViewItem *)) );

	mTimer = new QTimer( this );
	connect( mTimer, SIGNAL(timeout()), SLOT(slotTimeOut()) );
}


ListViewFindItem::~ListViewFindItem()
{
	delete mLineEdit;
	delete mTimer;
}


void ListViewFindItem::show( const QPoint & pos )
{
	setGeometry( pos.x(), pos.y(),  250, 32 );

	mTimer->start( 500, FALSE );
	mLineEditCharsCounter = 0;
	mShowCursor = TRUE;

	mLineEdit->setText("|");
	mLineEdit->setCursorPosition( 0 );

	QWidget::show();
}


void ListViewFindItem::keyPressed( QKeyEvent * e )
{
	unsigned int key = e->key();
	QString text = e->text();

	switch( key )
	{
		case Key_Tab:
		case Key_Escape:
			close();
		break;

		case Key_Up:
			moveCursor( Up );
		break;

		case Key_Down:
			moveCursor( Down );
		break;

		case Key_Home:
			moveCursor( Home );
		break;

		case Key_End:
			moveCursor( End );
		break;

		case Key_Enter:
		case Key_Return:
		{
			close();
			emit signalReturnPressed( mListView->currentItem() );
		}
		break;

		case Key_Backspace:
		{
			mLineEdit->setCursorPosition( mLineEditCharsCounter );
			if ( mLineEditCharsCounter )
					mLineEditCharsCounter--;
			else
				return;

			mLineEdit->backspace();
		}
		break;

		default:
		{
			if ( text.isEmpty() ) // Control/Shift/Alt pressed
				break;

			mLineEdit->insert( text ); // insert char
			mLineEditCharsCounter = mLineEdit->cursorPosition();
			matchItemForCurrentText();
		}
		break;
	}
}


QString ListViewFindItem::text()
{
	mTimer->stop();  // stop blinking cursor

	if ( mShowCursor )  // if cursor is visible then remove it
		mLineEdit->del();

	QString txt = mLineEdit->text(); // get string without cursor

	if ( mShowCursor )
		mShowCursor = FALSE;  // becouse cursor should be shows ( in slotTimeOut() )

	mTimer->start( 500, FALSE );  // start blinking cursor

	return txt;
}


void ListViewFindItem::matchItemForCurrentText()
{
	bool found = FALSE;
	QString currentName, s = text();

	QListViewItemIterator it( mListView );
	while ( it.current() != 0 )  {
		currentName = it.current()->text(0).left( s.length() );

		if ( currentName.contains( s ) )  {
			found = TRUE;
			break;
		}
		it++;
	}
	if ( ! found )  // fix text on mLineEdit
		mLineEdit->backspace();
	else  {  // move cursor to founded item
		mListView->setCurrentItem( it.current() );
		mListView->ensureItemVisible( it.current() );
	}
}


void ListViewFindItem::moveCursor( Direct direct )
{
	QString currentName, s = text();
	if ( s.isEmpty() )  return;

	QListViewItem *item
	 = (direct == Up || direct == Home) ? mListView->currentItem()->itemAbove() : mListView->currentItem()->itemBelow();

	if ( item )  {  // is item above/below exists ?
		QListViewItemIterator it( mListView );
		it = item;
		while ( TRUE )  {
			currentName = it.current()->text(0).left( s.length() );

			if ( currentName.contains(s) )  {
				mListView->setCurrentItem( it.current() );
				mListView->ensureItemVisible( it.current() );
				if ( direct == Up || direct == Down )
					break;
			}

			if ( direct == Up || direct == Home )
				it--;
			else
				it++;

			if ( ! it.current() )
				return;
		} // while
	} // is item
}

//	void signalMoveCursorList( Direct );
// void ListViewExt::slotMoveCursor( Direct )
//	void keyPressed( QKeyEvent * e );

void ListViewFindItem::slotTimeOut()
{
	mShowCursor =! mShowCursor;
	if ( mShowCursor )  {  // show cursor
		mLineEdit->end( FALSE );
		mLineEdit->insert( "|" );
		mLineEdit->cursorBackward( FALSE, 1 );
	}
	else  // remove cursor
		mLineEdit->del();
}

// ----------- EVENTs -----------

void ListViewFindItem::closeEvent( QCloseEvent * e )
{
	mTimer->stop();
	e->accept();
}
