/***************************************************************************
                          helpdialog.h  -  description
                             -------------------
    begin                : sun jan 23 2005
    copyright            : (C) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef HELPDIALOG_H
#define HELPDIALOG_H

#include <qdialog.h>

class QPushButton;
class QTextBrowser;

/** @short Klasa powoduje pomocy dla aplikacji.
*/
/**
@author Piotr Mierzwiński
*/
class HelpDialog : public QDialog
{
	Q_OBJECT
public:
	HelpDialog(QWidget *parent = 0, const char *name = 0, bool modal = FALSE, WFlags f = 0);
	~HelpDialog();

private:
	QPushButton *m_pBtn_left, *m_pBtn_right, *m_pBtn_home;
	QTextBrowser *m_pTB_view;
	QString m_sHelpPath;

	void initGUI();
	bool loadFile();

protected:
	void resizeEvent( QResizeEvent * );

};

#endif
