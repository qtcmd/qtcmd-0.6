/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you wish to add, delete or rename functions or slots use
** Qt Designer which will update this file, preserving your code. Create an
** init() function in place of a constructor, and a destroy() function in
** place of a destructor.
*****************************************************************************/

#include "squeezelabel.h"


int MessageBox::common( QWidget * parent, const QString & caption, const QString & msg, const QStringList & btnsNames, int defaultButton, QMessageBox::Icon icon, const QString & sChkBoxMsg )
{
	MessageBox *dlg = new MessageBox( parent );
	CHECK_PTR( dlg );

	// --- init dialog
	// !!! nie jest wyswietlana ikona 'Question' (testowane na Qt-3.2.3) !!!
	// chyba blad w fun.: QMessageBox::standardIcon()
	dlg->mPixmapLab->setPixmap( QMessageBox::standardIcon(icon) );

	if ( ! caption.isEmpty() )
		dlg->setCaption( caption );

	if ( ! msg.isEmpty() )
		dlg->mMsgLabel->setText( msg );

	// --- set buttons
	const int totalButtons = 5;
	int maxButtons = btnsNames.count();

	if ( maxButtons > totalButtons ) {
		debug("MessageBox::common. Too big number of buttons, max is %d !", totalButtons );
		maxButtons = 5;
	}
	if ( maxButtons < 1 )
		maxButtons = 1;

	QPushButton *buttonsTab[totalButtons] =
		{ dlg->mBtn1, dlg->mBtn2, dlg->mBtn3, dlg->mBtn4, dlg->mBtn5 };

	// set buttons text
	if ( maxButtons > 1 ) {
		if ( ! btnsNames[0].isEmpty() )
			for ( uint i=0; i<btnsNames.count(); i++ )
				buttonsTab[i]->setText( btnsNames[i] );
	}
	else // maxButtons == 1
		if ( btnsNames[0].isEmpty() )
			dlg->mBtn1->setText( tr("&OK") );
		else
			dlg->mBtn1->setText( btnsNames[0] );

	int maxDlgWidth = 0; // it's width of dialog with everyone buttons
	for ( int i=0; i<totalButtons; i++ ) {
		buttonsTab[i]->setFixedWidth(100);
		maxDlgWidth += buttonsTab[i]->width()+10;
	}

	// set default button
	if ( defaultButton > 0 )
		buttonsTab[defaultButton-1]->setDefault( TRUE );
	else // set defaultButton on first button
		buttonsTab[0]->setDefault( TRUE );

	//debug("MessageBox::common, sChkBoxMsg= %s", sChkBoxMsg.latin1());
	// support for check box
	if ( sChkBoxMsg.isEmpty() )
		delete dlg->m_pCheckBox;
	else
		dlg->m_pCheckBox->setText(sChkBoxMsg);

	// remove not used buttons
	int i = maxButtons;
	while ( i<totalButtons ) {
		delete buttonsTab[i];
		i++;
	}
	dlg->mButtonGroup->adjustSize();

	if ( dlg->width() > maxDlgWidth )
		dlg->adjustSize();
	else {
		if ( maxButtons < 3 )
			maxDlgWidth += (maxDlgWidth/2)/2;
		else
		if ( maxButtons > 2 )
			maxDlgWidth += maxDlgWidth/2;

		maxDlgWidth /= 2;
		//dlg->resize( (maxDlgWidth*2)+2-((maxDlgWidth*2)*30)/100, dlg->sizeHint().height() );
		dlg->resize( maxDlgWidth+2, dlg->sizeHint().height() );
	}

	// --- show dialog
	int result = dlg->exec();

	// check checkBox
	if ( ! sChkBoxMsg.isEmpty() ) {
		if ( dlg->m_pCheckBox->isChecked() )
			result += ChkBoxTrue;
	}

	delete dlg;
	dlg = 0;

	return result;
}


int MessageBox::information( QWidget * parent, const QString & caption, const QString & msg, const QString & button0Text )
{
	return common( parent, caption, msg, button0Text, 0, QMessageBox::Information );
}


int MessageBox::warning( QWidget * parent, const QString & caption, const QString & msg, const QString & button0Text )
{
	return common( parent, caption, msg, button0Text, 0, QMessageBox::Warning );
}


int MessageBox::critical( QWidget * parent, const QString & caption, const QString & msg, const QString & sChkBoxMsg, const QString & button0Text )
{
	return common( parent, caption, msg, button0Text, 0, QMessageBox::Critical, sChkBoxMsg );
}

int MessageBox::critical( QWidget * parent, const QString & msg, bool , const QString & sChkBoxMsg, const QString & button0Text )
{
	return common( parent, tr("Error")+" - QtCommander", msg, button0Text, 0, QMessageBox::Critical, sChkBoxMsg );
}

int MessageBox::yesNo( QWidget * parent, const QString & caption, const QString & msg, int defaultButton, bool threeButtons, const QString & buttonText, const QString & sChkBoxMsg )
{
	QStringList btnsNames;
	btnsNames << tr("&Yes") << tr("&No");

	if ( threeButtons ) {
		if ( buttonText.isEmpty() )
			btnsNames.append( tr("&Cancel") );
		else
			btnsNames.append( buttonText );
	}

	return common( parent, caption, msg, btnsNames, defaultButton, QMessageBox::Warning, sChkBoxMsg );
}
