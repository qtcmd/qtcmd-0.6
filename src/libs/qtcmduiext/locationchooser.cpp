/***************************************************************************
                          locationchooser.cpp  -  description
                             -------------------
    begin                : wed mar 17 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "locationchooser.h"

#include <qsettings.h>
#include <qmessagebox.h>
#include <qfiledialog.h>


LocationChooser::LocationChooser( QWidget *parent, const char *name )
	: QWidget( parent, name ),
	 mLayout(NULL), mComboBox(NULL), mLineEdit(NULL), mChooserButton(NULL)
{
	mCurrentPath  = "";
	mOriginalPath = "";
	mKeyListReadingStr = "";

	// --- init properties by default values
	mShowSeparator = TRUE;
	mGetMode = GetDirectory;
	mButtonSide = RightButton;
	mGettingWidget = ComboBox;
	mShowPrevNextActions = FALSE;

/*
	QAction *currA;
	QActionGroup *actions = new QActionGroup();

	QPopupMenu *actionsMenu = new QPopupMenu();

	actions->addTo( mButtonMenu );
	actions->addTo( actionsMenu );
*/

	mButtonMenu = new QPopupMenu();
	mButtonMenu->insertItem( tr("&Current directory"),  CURRENT_DIR );
	mButtonMenu->insertItem( tr("&Original directory"), ORIGINAL_DIR );
	mButtonMenu->insertItem( tr("&Home directory"), HOME_DIR );
	mButtonMenu->insertItem( tr("&Root directory"), ROOT_DIR );
	mButtonMenu->insertSeparator( 1001 );
	mButtonMenu->insertItem( tr("&Previous directory"), PREVIOUS_DIR );
	mButtonMenu->insertItem( tr("&Next directory"), NEXT_DIR );
	mButtonMenu->insertItem( tr("Choose &directory"), SELECT_DIR );
	mButtonMenu->insertSeparator( 1002 );
	mButtonMenu->insertItem( tr("Clear &all URLs"), CLEAR_ALL  );
	mButtonMenu->insertItem( tr("Clear current &URL"), CLEAR_CURRENT );
//	mButtonMenu->insertSeparator();
//	mButtonMenu->insertItem( tr("Set default action"), SET_DEFAULT_ACTION );

	connect( mButtonMenu, SIGNAL(activated( int )), this, SLOT(slotSelectDirMenuItem( int )) );

	createWidget();
	if ( mLayout )
		mLayout->setSpacing( mShowSeparator ? 5 : 0 );
}


LocationChooser::~LocationChooser()
{
	//saveSettings();

	if ( mLayout ) delete mLayout;
	if ( mLineEdit ) delete mLineEdit;
	if ( mComboBox ) delete mComboBox;
	if ( mButtonMenu ) delete mButtonMenu;
	if ( mChooserButton ) delete mChooserButton;
}


void LocationChooser::setKeyListReading( const QString & keyListReadingStr, bool readSettings )
{
	mKeyListReadingStr = keyListReadingStr;

	if ( readSettings )
		init();
}


void LocationChooser::init()
{
	if ( mComboBox && ! mKeyListReadingStr.isEmpty() ) {
		QSettings *settings = new QSettings();
		QStringList URLsList = settings->readListEntry( mKeyListReadingStr );
		int currentURLid = settings->readNumEntry( mKeyListReadingStr+"Id", -1 );

		if ( currentURLid < 0 )
			currentURLid = 0;
		if ( currentURLid > (int)URLsList.count()-1 )
			currentURLid = URLsList.count()-1;

		if ( URLsList.count() ) {
			insertStringList( URLsList );
			mComboBox->setCurrentItem( currentURLid );
		}
		else
			mComboBox->insertItem( mCurrentPath );

		delete settings;
		setMenuItemVisible( CLEAR_ALL, (mComboBox->count() > 0) );
	}
}


void LocationChooser::saveSettings()
{
	if ( mComboBox && ! mKeyListReadingStr.isEmpty() ) {
		QStringList list;
		for(int i=0; i<mComboBox->count(); i++)
			list.append( mComboBox->text(i) );

		QSettings *settings = new QSettings();
		if ( mComboBox->count() )
			settings->writeEntry( mKeyListReadingStr, list );
		else
			settings->removeEntry( mKeyListReadingStr );
		settings->writeEntry( mKeyListReadingStr+"Id", mComboBox->currentItem() );
		delete settings;
	}
}


void LocationChooser::setButtonSide( ButtonSide btnSide )
{
	mButtonSide = btnSide;
	createWidget();
}

void LocationChooser::setShowSeparator( bool show )
{
	mShowSeparator = show;
	mLayout->setSpacing( mShowSeparator ? 5 : 0 );
}

void LocationChooser::setGettingWidget( GettingWidget getWidget )
{
	mGettingWidget = getWidget;
	createWidget();
}


void LocationChooser::createWidget()
{
	// --- remove widget
	if ( mLayout )
		delete mLayout;
	if ( mLineEdit ) {
		delete mLineEdit;
		mLineEdit = 0;
	}
	if ( mComboBox ) {
		delete mComboBox;
		mComboBox = 0;
	}
	if ( mChooserButton ) {
		delete mChooserButton;
		mChooserButton = 0;
	}

	// --- create widget
	mLayout = new QHBoxLayout( this );
	mLayout->setAutoAdd( TRUE );
	mLayout->setMargin( 0 );
	mLayout->setSpacing( mShowSeparator ? 5 : 0 );

	if ( mButtonSide != NoneButton )
		if ( mButtonSide == LeftButton )
			createChooserButton();

	if ( mGettingWidget == LineEdit ) {
		mLineEdit = new QLineEdit( this );
		setFocusProxy( mLineEdit );
		connect( mLineEdit, SIGNAL(textChanged(const QString &)), this, SIGNAL(textChanged(const QString &)) );
		mLineEdit->show(); // need to plugin (for Qt Designer) works correct
	}
	else
	if ( mGettingWidget == ComboBox ) {
		mComboBox = new QComboBox( TRUE, this );
		mComboBox->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)0, 0, 0, mComboBox->sizePolicy().hasHeightForWidth() ) ); // prefered
		mComboBox->setInsertionPolicy( QComboBox::AtTop );
		mComboBox->setDuplicatesEnabled( FALSE );
		setFocusProxy( mComboBox->lineEdit() );
		connect( mComboBox->lineEdit(), SIGNAL(textChanged(const QString &)), this, SIGNAL(textChanged(const QString &)) );
		mComboBox->show(); // need to plugin (for Qt Designer) works correct
	}

	if ( mButtonSide != NoneButton )
		if ( mButtonSide == RightButton )
			createChooserButton();

	setMenuItemVisible( PREVIOUS_DIR, (mGettingWidget == ComboBox && mShowPrevNextActions) );
	setMenuItemVisible( NEXT_DIR, (mGettingWidget == ComboBox && mShowPrevNextActions) );
	setMenuItemVisible( 1001, (mGettingWidget == ComboBox && mShowPrevNextActions) ); // the first separator
	setMenuItemVisible( CLEAR_ALL, (mGettingWidget == ComboBox) );

	setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)5, (QSizePolicy::SizeType)0, 0, 0, sizePolicy().hasHeightForWidth() ) ); // prefered
}

void LocationChooser::createChooserButton()
{
	mChooserButton = new QToolButton( this );
	mChooserButton->setPopup( mButtonMenu );
	mChooserButton->setPopupDelay( 0 );
	mChooserButton->setText( "..." );
	mChooserButton->setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)0, (QSizePolicy::SizeType)1, 0, 0, mChooserButton->sizePolicy().hasHeightForWidth() ) ); // fixed
	mChooserButton->setMaximumHeight( mChooserButton->height()-5 );

	if ( mButtonMenu ) {
		mChooserButton->setPopup( mButtonMenu );
		mChooserButton->setPopupDelay( 0 );
	}
	connect( mChooserButton, SIGNAL(clicked()), this, SLOT(slotGetExistingDirectory()) );

	mChooserButton->show(); // need to plugin (for Qt Designer) works correct
}


QString LocationChooser::text( int id ) const
{
	if ( mLineEdit )
		return mLineEdit->text();

	if ( ! mComboBox )
		return QString("");

	return (id < 0) ? mComboBox->currentText() : mComboBox->text( id );
}


void LocationChooser::setText( const QString & text )
{
	if ( mLineEdit )
		mLineEdit->setText( text );
	else
	if ( mComboBox )
		mComboBox->setEditText( text );
}


void LocationChooser::selectAll()
{
	if ( mLineEdit )
		mLineEdit->selectAll();
	else
	if ( mComboBox )
		mComboBox->lineEdit()->selectAll();
}


void LocationChooser::setMenuItemVisible( int id, bool visible )
{
	mButtonMenu->setItemVisible( id, visible );
}


void LocationChooser::setMenuItemEnabled( int id, bool enable )
{
	mButtonMenu->setItemEnabled( id, enable );
}


void LocationChooser::insertItem( const QString & itemStr, uint itemId )
{
	if ( mLineEdit ) {
		mLineEdit->setText( itemStr );
		return;
	}
	// entries can't duplicates
	for ( int i=0; i<mComboBox->count(); i++ ) {
		if ( itemStr == mComboBox->text(i) )
			return;
	}
	// correct for path to directory
	QString str = itemStr;
	if ( mGetMode == GetDirectory )
		if ( str.at(str.length()-1) != '/' )
			str += "/";

	if ( mComboBox->count() > 49 ) // max items == 50
		mComboBox->removeItem( mComboBox->count()-1 );

	mComboBox->insertItem( str, itemId );
}


void LocationChooser::insertStringList( const QStringList & stringList )
{
	// insert method protecting to duplicates entries
	if ( mComboBox )
		for ( uint i=0; i<stringList.count(); i++ )
			insertItem( stringList[i], i );
}


QString LocationChooser::prevEntry() const
{
	int id = mComboBox->currentItem();

	return mComboBox->text( (id > 0) ? id-1 : mComboBox->count()-1 );
}


QString LocationChooser::nextEntry() const
{
	int id = mComboBox->currentItem();

	return mComboBox->text( (id < mComboBox->count()-1) ? id+1 : 0 );
}

	// ------ SLOTs -------

void LocationChooser::slotSelectDirMenuItem( int itemId )
{
	QString targetDir;

	if ( itemId == CURRENT_DIR )
		targetDir = mCurrentPath;
	else
	if ( itemId == ORIGINAL_DIR )
		targetDir = mOriginalPath;
	else
	if ( itemId == HOME_DIR )
		targetDir = QDir::homeDirPath()+"/";
	else
	if ( itemId == ROOT_DIR )
		targetDir = "/";
	else
	if ( itemId == SELECT_DIR ) {
		slotGetExistingDirectory();
		return;
	}
	else
	if ( itemId == CLEAR_ALL ) {
		if ( ! mComboBox )
			return;
		int result = QMessageBox::question( this,
		 tr("Clear history pathes")+" - QtCommander",
		 tr("History contains")+" "+QString::number(mComboBox->count())+" "+tr("items")+".\n\n"+
		  tr("Do you really want to clear it")+" ?",
		 tr("&Yes"), tr("&No"), QString::null,
		 1, 1 // default, cancel - 'No' button
		);
		if ( result == 0 ) // Yes
			mComboBox->clear();
		return;
	}
	else
	if ( itemId == CLEAR_CURRENT ) {
		mComboBox->lineEdit()->clear();
		return;
	}
	else
	if ( itemId == PREVIOUS_DIR )
		targetDir = prevEntry();
	else
	if ( itemId == NEXT_DIR )
		targetDir = nextEntry();

	if ( mLineEdit )
		mLineEdit->setText( targetDir );
	else
	if ( mComboBox ) {
		mComboBox->insertItem( targetDir, 0 );
		mComboBox->setCurrentItem( 0 );
	}
	emit locationChanged( targetDir );
}


void LocationChooser::slotGetExistingDirectory()
{
	QString targetUrl;

	if ( mGetMode == GetDirectory  )
		targetUrl = QFileDialog::getExistingDirectory(
		 mCurrentPath, this, 0,
		 tr("Select target directory")+" - QtCommander"
		);
	else
		targetUrl = QFileDialog::getOpenFileName(
		 mCurrentPath, tr("All files")+"(*)", this, 0,
		 tr("Select target file")+" - QtCommander"
		);

	if ( ! targetUrl.isEmpty() ) {
		if ( targetUrl.at(targetUrl.length()-1) != '/' )
			targetUrl += "/";

		if ( mLineEdit )
			mLineEdit->setText( targetUrl );
		else
		if ( mComboBox )
			mComboBox->setEditText( targetUrl );

		emit locationChanged( targetUrl );
	}
}
