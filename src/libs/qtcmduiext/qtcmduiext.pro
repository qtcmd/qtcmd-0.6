# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/libs/qtcmduiext
# Cel to biblioteka qtcmduiext

INSTALLS += target 
target.path = $$PREFIX/lib 
FORMS += messagebox.ui 
TRANSLATIONS += ../../../translations/pl/qtcmd-uiext.ts 
HEADERS += squeezelabel.h \
           messagebox.ui.h \
           statusbar.h \
           listviewwidgets.h \
           locationchooser.h \
           listvieweditbox.h 
SOURCES += squeezelabel.cpp \
           statusbar.cpp \
           listviewwidgets.cpp \
           locationchooser.cpp \
           listvieweditbox.cpp 
MOC_DIR = ../../../.moc
OBJECTS_DIR = ../../../.obj
TARGET = qtcmduiext
DESTDIR = ../../../.libs
CONFIG += warn_on \
qt \
thread \
dll
TEMPLATE = lib
PRECOMPILED_HEADER = ../../pch.h
