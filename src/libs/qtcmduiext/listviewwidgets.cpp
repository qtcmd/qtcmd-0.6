/***************************************************************************
                          listviewwidgets.cpp  -  description
                             -------------------
    begin                : fri mar 26 2004
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
***************************************************************************/

#include "listviewwidgets.h"

#include <qheader.h>
#include <qpainter.h>
#include <qcolordialog.h>

// TODO przydalaby sie tu jeszcze jedna klasa - 'WidgetCell' zawierajaca atrybuty pojedynczej komorki,
// np. czcionka (zwykla/pobrugiona, italic, kolor), status widgeta (aktywny/nieaktywny), biezaca wartosc

ListViewWidgetsItem::ListViewWidgetsItem( ListViewWidgets *parent,
	 Widget w1, Widget w2, Widget w3, Widget w4, Widget w5, Widget w6, Widget w7, Widget w8
	) : QListViewItem(parent), mParentList(parent)
{
	mWidgetsTab[0] = w1;
	mWidgetsTab[1] = w2;
	mWidgetsTab[2] = w3;
	mWidgetsTab[3] = w4;
	mWidgetsTab[4] = w5;
	mWidgetsTab[5] = w6;
	mWidgetsTab[6] = w7;
	mWidgetsTab[7] = w8;

	for ( int i=0; i<8; i++ ) {
		mCheckedTab[i] = FALSE;
		mComboBoxLists[i].clear();
		mColorsTab[i] = QColor( 0,0,0 );
	}
	mExclusiveButtons = FALSE;

	mTextColor = QColor( 0,0,0 );
	mItalic = FALSE;
	mBold = FALSE; // only for cursorForColumn() column
}


void ListViewWidgetsItem::paintCell( QPainter *p, const QColorGroup & , int column, int width, int alignment )
{
	int x = 0, y = 2;
	int w = height()-6, h = height()-6;
	int columnAlignment = mParentList->columnAlignment( column );
	Widget widget = mWidgetsTab[column];

	if ( widget != TextLabel ) {
		if ( columnAlignment == Qt::AlignHCenter ) {
			if ( widget == ColorFrame )
				x = (width/2)-w;
			else
				x = (width/2)-(w/2);
		}
		else
		if ( columnAlignment == Qt::AlignLeft )
			x = 1;
		else
		if ( columnAlignment == Qt::AlignRight ) {
			if ( widget == ColorFrame )
				x = width-(w*2)-1;
			else
				x = width-w-1;
		}
	}

	if ( widget == CheckBox || widget == RadioButton || widget == ColorFrame )
		p->fillRect( 0, 0, width, height(), mParentList->backgroundColor() );  // set background color

	if ( widget == CheckBox ) {
		p->drawRect( x,y, w,h );
		p->drawRect( x+1,y+1, w-2,h-2 );
		if ( mCheckedTab[column] == TRUE ) {
			p->drawLine( x+3,y+5, x+5,y+7 );  p->drawLine( x+5,y+7, (x+w)-4,y+3 );
			p->drawLine( x+3,y+6, x+5,y+8 );  p->drawLine( x+6,y+8, (x+w)-4,y+4 );
		}
	}
	else
	if ( widget == RadioButton ) {
		p->drawEllipse( x,y, w,h );
		p->drawEllipse( x+1,y+1, w-2,h-2 );
		if ( mCheckedTab[column] == TRUE ) {
			y = h/2;
			x += (w/2)-2;
			p->drawEllipse( x,y, 4,4 );
			p->drawRect( x+1,y+1, 2,2 );
		}
	}
	else
	if ( widget == ColorFrame ) {
		w *= 2;
		p->drawRect( x,y, w,h );
		p->drawRect( x+1,y+1, w-2,h-2 );
		p->fillRect( x+2, y+2, w-4, h-4, mColorsTab[column] );
	}
	else { // widget == TextLabel || widget == ComboBox || widget == EditBox
		//QListViewItem::paintCell( p, cg, col, width, alignment );
		if ( mParentList->enableTwoColorsOfBg() ) {
			//QColor color = itemPos()%(height()*2) ? QColor(255,235,220) : mParentList->backgroundColor();
			QColor color = itemPos()%(height()*2) ? mParentList->secondColorOfBg() : mParentList->backgroundColor();
			p->fillRect( 0, 0, width, height(), color );  // set background color
			QColor col = mParentList->secondColorOfBg();
		}
		else
			p->fillRect( 0, 0, width, height(), mParentList->backgroundColor() );  // set background color

		int x = mParentList->itemMargin()+1;
		int y = 1;
		int fontWeight;
		p->setPen( mTextColor );
		if ( column == (int)mParentList->cursorForColumn() ) {
			//p->setPen( mTextColor );
			fontWeight = (mBold) ? QFont::Bold : QFont::Light;
			p->setFont( QFont(mParentList->font().family(), mParentList->font().pointSize(), fontWeight, mItalic) );
			if ( mBold ) {
				x -= 1;  y -= 1;
			}
		}
		p->drawText( x, y, width-mParentList->itemMargin(), height(), alignment, trimedString(text(column), column, width, fontWeight) );
	}
}


void ListViewWidgetsItem::paintFocus( QPainter *p, const QColorGroup & , const QRect & r )
{
	QRect rect = r;

	uint column = mParentList->cursorForColumn();
	if ( column ) {
		rect.setX( mParentList->header()->sectionPos(column) );
		rect.setWidth( mParentList->header()->sectionSize(column) );
	}
	p->setPen( QColor(Qt::red) ); // outside frame
	p->drawRect( rect.x(), rect.y(), rect.width(), height() );
	p->setPen( QColor(Qt::darkBlue) ); // inside frame
	p->drawRect( rect.x()+1, rect.y() + 1, rect.width()-2, height()-2 );
}


QString ListViewWidgetsItem::trimedString( const QString & str, const int & /*column*/, const int & columnWidth, const int & weight )
{
	bool truncate = FALSE;
	QString txtStr = str, tmpTxt = "...";

	QFont font = mParentList->font();
	font.setBold( weight==QFont::Bold );
	QFontMetrics fm( font );

	if ( fm.width( txtStr ) > columnWidth )  {
		truncate = TRUE;
		int i = 0;
		while ( fm.width(tmpTxt + txtStr[ i ]) < columnWidth )
			tmpTxt += txtStr[ i++ ];

		tmpTxt.remove( 0, 3 );
		if ( tmpTxt.isEmpty() )
			tmpTxt = txtStr.left( 1 );

		tmpTxt += "...";
	}
	if ( truncate )
		txtStr = tmpTxt;

	return txtStr;
}


QString ListViewWidgetsItem::textValue( uint column ) const
{
	Widget w = mWidgetsTab[ column ];
	if ( w == TextLabel || w == EditBox || w == ComboBox )
		return text( column );

	debug( "Invalid operation ! Current widget's properties is text value" );
	return QString("");
}


void ListViewWidgetsItem::setTextValue( uint column, const QString & str )
{
	Widget w = mWidgetsTab[ column ];
	if ( w == TextLabel || w == EditBox || w == ComboBox )
		setText( column, str );
	else
		debug( "Invalid operation ! Current widget's properties is text value" );
}


bool ListViewWidgetsItem::boolValue( uint column ) const
{
	if ( mWidgetsTab[column] == CheckBox || mWidgetsTab[column] == RadioButton )
		return mCheckedTab[column];

	debug( "Invalid operation ! Current widget's properties is bool value" );
	return FALSE;
}


void ListViewWidgetsItem::setBoolValue( uint column, bool value )
{
	Widget w = mWidgetsTab[ column ];
	if ( w == CheckBox || w == RadioButton ) {
		if ( w != mExlusiveButton )
			mCheckedTab[column] = value;
		else
		if ( mExclusiveButtons ) {
			if ( mCheckedTab[column] == FALSE )
				setExclusivelyButton( column );
			else
				mCheckedTab[column] = TRUE;
		}
		else
			mCheckedTab[column] = value;
		repaint();
	}
	else
		debug( "Invalid operation ! Current widget's properties is bool value" );
}


QColor ListViewWidgetsItem::colorValue( uint column ) const
{
	if ( mWidgetsTab[column] == ColorFrame )
		return mColorsTab[column];

	debug( "Invalid operation ! Current widget's properties is QColor value" );
	return QColor(0,0,0);
}


void ListViewWidgetsItem::setColorValue( uint column, const QColor & color )
{
	if ( mWidgetsTab[column] == ColorFrame ) {
		mColorsTab[column] = color;
		repaint();
	}
	else
		debug( "Invalid operation ! Current widget's properties is QColor value" );
}


void ListViewWidgetsItem::setTextAttributs( uint column, const QColor & color, bool bold, bool italic )
{
	Widget w = mWidgetsTab[ column ];
	if ( w == TextLabel || w == EditBox || w == ComboBox ) {
		mBold = bold;
		mItalic = italic;
		mTextColor = color;
		repaint();
	}
	else
		debug( "Invalid operation ! Current widget's is text label" );
}


void ListViewWidgetsItem::insertStringList( uint column, const QStringList & stringList )
{
	if ( mWidgetsTab[column] == ComboBox )
		mComboBoxLists[column] = stringList;
	else
		debug( "Invalid operation ! Current widget's properties is QComboBox value" );
}


QStringList ListViewWidgetsItem::stringList( uint column ) const
{
	if ( mWidgetsTab[column] == ComboBox )
		return mComboBoxLists[column];
	else {
		debug( "Invalid operation ! Current widget's properties is QComboBox value" );
		return QString::null;
	}
}


void ListViewWidgetsItem::setWidget( uint column, Widget widget )
{
	if ( column > 7 ) {
		debug("Number of column is too big ! Maximum number is 7.");
		return;
	}
	mWidgetsTab[column] = widget;
	repaint();
}


void ListViewWidgetsItem::setExclusiveButtons( bool exclusive, Widget widget )
{
	if ( widget != CheckBox && widget != RadioButton ) {
		debug( "Invalid operation ! There current widgets are'nt exlusively" );
		return;
	}
	mExclusiveButtons = exclusive;
	mExlusiveButton   = widget;
}


void ListViewWidgetsItem::setExclusivelyButton( uint column )
{
	if ( mWidgetsTab[column] != mExlusiveButton )
		return;

	for ( int i=0; i<8; i++ ) {
		if ( mWidgetsTab[i] != mExlusiveButton )
			continue;
		mCheckedTab[i] = FALSE;
	}
	mCheckedTab[column] = TRUE;
}


void ListViewWidgetsItem::setEnableCell( uint column, bool enable )
{
	Widget w = mWidgetsTab[column];

	if ( w == TextLabel || w == EditBox || w == ComboBox ) {
		mColorsTab[column] = (enable) ? Qt::black : Qt::gray;
		setTextAttributs( column, mColorsTab[column], mBold ); // use is only color attrib
	}
}



ListViewWidgets::ListViewWidgets( QWidget *parent, const char *name )
	: QListView( parent, name ),
	  mCurrentColumn(-1), mEditColumn(-1), mEnableTwoColorsOfBg(FALSE),
		mEditBox(NULL), mComboBox(NULL)
{
	mEditBox = new ListViewEditBox( viewport(), TRUE );
	mComboBox = new QComboBox( viewport() );
	mComboBox->hide();

	mCursorForColumn = 0;
	mSecondColorOfBg = Qt::white;
	setBackgroundColor( Qt::white );
	mEditBoxGetKeyShortcut = FALSE;

	connect( header(), SIGNAL(sizeChange( int, int, int )),
	 this, SLOT(slotHeaderSizeChange( int, int, int )) );
	connect( this, SIGNAL(clicked( QListViewItem *, const QPoint &, int )),
	 this, SLOT(slotClicked( QListViewItem *, const QPoint &, int )) );
	connect( mEditBox, SIGNAL(signalRename(int, const QString &, const QString &)),
	 this, SIGNAL(editBoxTextApplied(int, const QString &, const QString &)) );
	connect( mComboBox, SIGNAL(activated(int)),
	 this, SLOT(slotSetCurrentComboItem(int)) );
}


ListViewWidgets::~ListViewWidgets()
{
	delete mEditBox;
	delete mComboBox;
}


void ListViewWidgets::setSecondColorOfBg( const QColor & c )
{
	mSecondColorOfBg = c;
	triggerUpdate();
}


void ListViewWidgets::setEnableTwoColorsOfBg( bool enable )
{
	mEnableTwoColorsOfBg = enable;
	triggerUpdate();
}


void ListViewWidgets::showWidget( int column, ListViewWidgetsItem *itemPtr )
{
	ListViewWidgetsItem *item = (itemPtr == NULL) ? (ListViewWidgetsItem *)currentItem() : itemPtr;
	if ( itemPtr ) {
		setCurrentItem( item );
		ensureItemVisible( item );
	}
	Widget columnWidget = item->widget( column );

	if ( columnWidget == EditBox || columnWidget == ComboBox )
		slotClicked( currentItem(), QPoint(0,0), column );
}


void ListViewWidgets::hideWidgetForCurrentItem( int column )
{
	Widget columnWidget = ((ListViewWidgetsItem *)currentItem())->widget( column );

	if ( columnWidget == EditBox )
		mEditBox->hide();
	else
	if ( columnWidget == ComboBox )
		mComboBox->hide();
}


void ListViewWidgets::slotClicked( QListViewItem * item, const QPoint & vp, int column )
{
	if ( mComboBox->isVisible() )
		mComboBox->hide();
	mEditColumn = -1;

	if ( vp.x() > 0 )
		if ( ! itemAt(viewport()->mapFromGlobal( vp )) || childCount() == 0 )
			return;

	mCurrentColumn = column;
	Widget columnWidget = ((ListViewWidgetsItem *)item)->widget( column );

	if ( columnWidget == EditBox || columnWidget == ComboBox ) {
		mEditColumn = column;

		if ( columnWidget == EditBox ) {
                        QHeader *head = header();
                        int x = head->sectionPos(column) + 1 - head->offset();
                        mEditBox->setGetKeyShortcutMode( mEditBoxGetKeyShortcut );
                        mEditBox->show( column );
                        mEditBox->move( x, mEditBox->y() );
		}
		else
		if ( columnWidget == ComboBox ) {
			if ( ! ((ListViewWidgetsItem *)item)->isEnableCell(mCursorForColumn) )
				return;
// 			if ( ((ListViewWidgetsItem *)item)->stringList(column).count() == 1 )
// 				return;

			QHeader *head = header();
			QRect r = itemRect( currentItem() );
			int x = head->sectionPos(column) + 1 - head->offset();
			int w = head->sectionSize(column);
			int h = r.height();
			int y = r.y();
			mComboBox->clear();
			mComboBox->insertStringList( ((ListViewWidgetsItem *)item)->stringList(column) );
			mComboBox->setCurrentText( currentItem()->text(column) );
			mComboBox->setGeometry( x,y-2, w,h+4 );
			mComboBox->setFocus();
			mComboBox->show();
		}
		viewport()->setFocusProxy( viewport()->parentWidget() );
	}
	else
	if ( columnWidget == ColorFrame ) {
		QColor initColor = ((ListViewWidgetsItem *)item)->colorValue( column );
		QColor newColor  = QColorDialog::getColor( initColor );
		if ( newColor.isValid() ) {
			((ListViewWidgetsItem *)item)->setColorValue( column, newColor );
			emit colorChanged( column );
		}
	}
	else
	if ( columnWidget == CheckBox || columnWidget == RadioButton ) {
		// mozna spr. czy klikniecie zostalo wykonane w obrebie obiektu
		bool inversedValue = ! ((ListViewWidgetsItem *)item)->boolValue( column );
		((ListViewWidgetsItem *)item)->setBoolValue( column, inversedValue );
		emit buttonClicked( column );
	}
}


void ListViewWidgets::slotSetCurrentComboItem( int id )
{
	currentItem()->setText( mEditColumn, mComboBox->text(id) ); // mEditColumn - clicked column
	mComboBox->hide();
	setFocus();
}


void ListViewWidgets::slotHeaderSizeChange( int section, int oldSize, int newSize )
{
	ListViewWidgetsItem *item = (ListViewWidgetsItem *)currentItem();

	if ( section < columns() && mEditColumn > -1 ) {
		if ( mEditColumn > section ) {
			Widget nextColWidget = item->widget( mEditColumn );
			if ( nextColWidget == EditBox || nextColWidget == ComboBox ) {
				int sizesDiff = newSize - oldSize;
				int y = itemRect(currentItem()).y();
				if ( mEditBox->isVisible() )
					mEditBox->move( mEditBox->x()+sizesDiff, y );
				else
				if ( mComboBox->isVisible() )
					mComboBox->move( mComboBox->x()+sizesDiff, y );
				return;
			}
		}
	}

	Widget currColWidget = item->widget( section );
	if ( currColWidget == EditBox || currColWidget == ComboBox ) {
		if ( mEditBox->isVisible() )
			mEditBox->setFixedWidth( newSize );
		else
		if ( mComboBox->isVisible() )
			mComboBox->setFixedWidth( newSize );
	}
}
