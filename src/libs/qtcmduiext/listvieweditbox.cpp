/***************************************************************************
                          listvieweditbox.cpp  -  description
                             -------------------
    begin                : Wed Jun 26 2002
    copyright            : (C) 2002 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
***************************************************************************/

#include "listvieweditbox.h"

#include <qapplication.h> // for 'setPalette()'
#include <qkeysequence.h>
#include <qlistview.h>
#include <qheader.h>


ListViewEditBox::ListViewEditBox( QWidget * parent, bool getKeyShortcut, const char * name )
	: QLineEdit(parent,name),
	  mParent(parent), mGetKeyShortcut(getKeyShortcut)
{
	setFont( mParent->parentWidget()->font() );
	setPalette( QApplication::palette() ); // it's needed becouse a panel has own color, there by QLineEdit are inherits
//	setFrameStyle( QFrame::Panel | QFrame::Plain );
	hideMe();

	if ( getKeyShortcut ) {
		mKeysStateMap[Key_Alt]     = "Alt";
		mKeysStateMap[Key_Control] = "Ctrl";
		mKeysStateMap[Key_Shift]   = "Shift";
		mKeysStateMap[Key_Super_L] = "LeftWin";
		mKeysStateMap[Key_Super_R] = "RightWin";
	}
}

void ListViewEditBox::show( int column, bool treeView )
{
	mStateStr = "";
	mColumn   = column;

	QListView *lv     = (QListView *)mParent->parentWidget();
	QListViewItem *ci = lv->currentItem();
	QHeader *hdr      = lv->header();

	int hdrOffset  = hdr->offset();
	int treeMargin = (treeView) ? (20*ci->depth()) : 0;
	int iconWidth  = (ci->pixmap(column)) ? ci->pixmap(column)->width() : 0;
	int margin     = iconWidth+treeMargin;

	// --- init geometry
	QRect r = lv->itemRect( ci );
	int x = hdr->sectionPos(column) + margin + 1 - hdrOffset - ((treeView && column) ? treeMargin: 0);
	int y = (r.y() != 0) ? r.y() : ci->itemPos()-lv->contentsY();
	int w = hdr->sectionSize(column) - iconWidth - 2 - ((treeView && !column) ? treeMargin : 0);
	int h = lv->currentItem()->height(); // r.height();

	// --- ensure a cell is visible
	bool moveToLeft = FALSE;
	if ( x < hdrOffset ) {
		if ( treeView )
			if ( hdrOffset > treeMargin ) {
				x -= (margin-treeMargin-iconWidth);
				moveToLeft = TRUE;
			}
		if ( ! treeView || moveToLeft ) { // move to left
			lv->scrollBy( x-1, 0 );
			x += hdrOffset - margin; // correct the X pos.for obj.
		}
	}
	else
	if ( x+w > lv->width() ) { // move to right
		lv->scrollBy( (x+w)-lv->viewport()->width()+1, 0 );
		if ( treeView ) // correct the X pos.for obj.
			x -= hdr->offset()-hdrOffset;
		else
			x -= hdr->offset()-margin;
	}

	// --- show and init the editBox
	setGeometry( x,y, w,h );
	QLineEdit::show();

	setText( mOldText = ci->text(column) );
	//if ( ! mGetKeyShortcut )
		selectAll();
	setFocus();
	mParent->setFocusProxy( lv );
}


void ListViewEditBox::hideMe()
{
	hide();
	clear();
	mParent->setFocusProxy( mParent->parentWidget() ); // mParent->parentWidget() - ListViewExt obj.
	mParent->parentWidget()->setFocus();
}

// ----------------- EVENTs ---------------

void ListViewEditBox::keyPressEvent( QKeyEvent * e )
{
	int key = e->key();

	// --- work like 'key shortcut grabber'
	if ( mGetKeyShortcut ) {
		if ( key == Key_Escape || key == Key_unknown ) {
			hideMe();
			parentWidget()->setFocus();
			return;
		}
		ButtonState stateAfter = e->stateAfter();

		KeysMap::Iterator itState = mKeysStateMap.find( key );
		if ( ! itState.data().isEmpty() )
			mStateStr = itState.data()+"+";
		if ( stateAfter == (AltButton | ControlButton) )
			mStateStr = "Alt+Ctrl+";
		else
		if ( stateAfter == (AltButton | ShiftButton) )
			mStateStr = "Alt+Shift+";
		else
		if ( stateAfter == (ControlButton | ShiftButton) )
			mStateStr = "Ctrl+Shift+";
		else
		if ( stateAfter == (AltButton | ControlButton | ShiftButton) )
			mStateStr = "Alt+Ctrl+Shift+";

		QString keyStr = QString(QKeySequence(key));
		QString outStr = mStateStr + keyStr;

		bool done = (e->text().isEmpty() && keyStr.length() == 1);
		if ( done )
			outStr.remove( outStr.length()-1, 1 );
		setText( outStr );

		if ( ! done ) {
			hideMe();
			parentWidget()->setFocus();
			emit signalRename( mColumn, mOldText, outStr );
		}
		return;
	}

	// --- work like 'common line edit'
	switch ( key )
	{
		case Key_Enter:
		case Key_Return: {
			emit signalRename( mColumn, mOldText, text() );
			hideMe();
			break;
		}
		case Key_Up:
		case Key_Down:
		case Key_Escape:
		case Key_unknown: {
			hideMe();
			break;
		}
		case Key_Insert: {
			if ( e->state() == ControlButton ) {
				if ( hasSelectedText() )
					copy();
			}
			else
			if ( e->state() == ShiftButton )
				paste();
			break;
		}
		default: {
			QLineEdit::keyPressEvent( e );
			e->accept();
			break;
		}
	}
}


bool ListViewEditBox::event( QEvent * e )
{
	if ( e->type() == QEvent::KeyPress ) {
		QKeyEvent * k = (QKeyEvent *)e;

		if ( k->key() == Key_Tab )  {
			hideMe();
			return TRUE;
		}
	}
	else
	if ( e->type() == QEvent::FocusOut ) {
		hideMe();
		return TRUE;
	}

	return QWidget::event( e );
}
