/***************************************************************************
                          squeezelabel.h  -  description
                             -------------------
    begin                : sun aug 10 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
***************************************************************************/

#ifndef _SQUEEZELABEL_H
#define _SQUEEZELABEL_H

#include <qlabel.h>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/**
 @short Klasa etykiety tekstowej z mo�liwo�ci� jej �ciskania.
 Klasa spe�nia zadanie takie same jak QLabel. Jedyn� jej przewag� jest
 mo�liwo�� tzw. �ciskania tekstu w szeroko�ci jak� ma aktualnie obiekt tej
 klasy. Je�eli skalowanie zawarto�ci jest w��czone (domy�lnie), wtedy jest
 sprawdzane czy ci�g mie�ci si� w ca�o�ci na etykiecie, je�li nie wtedy
 jest on skracany. Polega to na tym, �e pokazywany jest fragment pocz�tkowy
 i ko�cowy, natomiast �rodek jest zast�powany przez ci�g "...". Mo�liwe jest
 pokazywanie ca�o�ci tekstu po najechaniu nad nim kursorem myszy (w�a�ciwo��
 ta jest domy�lnie w��czona).
*/
class SqueezeLabel : public QLabel
{
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param parent - wska�nik na rodzica dla obiektu,
	 @param name - nazwa dla obiektu,
	 @param f - flaga typu Qt::WFlags (patrz dokumentacja dot. klasy Qt:QLabel).\n
	 W��czane jest tutaj pokazywanie dymka z ca�o�ci� �ci�ni�tego tekstu oraz
	 skalowanie zawarto�ci i inicjowane s� sk�adowe klasy.
	*/
	SqueezeLabel( QWidget * parent, const char * name=0, WFlags f=0 );

	/** Destruktor.\n
	 Brak definicji (jest pusta).
	 */
	virtual ~SqueezeLabel() {}

	/** Metoda powoduje w��czenie lub wy��czenie pokazywania dymku z pe�nym
	 tekstem skr�conego napisu.\n
	 @param show - warto�� TRUE w��cza pokazywanie dymku, FALSE - wy��cza.\n
	 W�a�ciwo�c ta jest domy�lnie w��czona.
	 */
	void setShowToolTip( bool show );

private:
	bool mShowTip;
	QString mFullText;


	/** Funkcja wykorzystywana do ustawiania kolejnych '�ci�ni�tych' linii tekstu
	 oraz dodaj�ca tzw. dymek (je�li w�a�ciwo�� ta zosta�a w��czona) do obiektu.\n
	 @param maxWidth - maksymalna szeroko�� obiektu (w punktach obrazu).
	 */
	void setSqueezingText( uint maxWidth );

	/** W�a�ciwa funkcja skracaj�ca podan� linie tekstu do podanej szeroko�ci.\n
	 @param line - linia tekstu do skr�cenia,
	 @param maxWidth - maksymalna szeroko�� obiektu (w punktach obrazu).
	 */
	QString squeezeLine( const QString &, uint );

public slots:
	/** Slot s�u��cy do ustawiania tekstu na obiekcie.\n
	 @param text - tekst do ustawiania.
	 */
	virtual void setText( const QString & text );

protected:
	/** Obs�uga zdarzenie zmiany rozmiaru.\n
	 Uruchamiana jest tu procedura dopasowania tekstu na obiekcie do jego
	 szeroko�ci, przy ka�dorazowym zmianie rozmiaru obiektu.
	 */
	virtual void resizeEvent( QResizeEvent * );

};

#endif
