/***************************************************************************
                          locationchooser.h  -  description
                             -------------------
    begin                : wed mar 17 2004
    copyright            : (C) 2004 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef LOCATIONCHOOSER_H
#define LOCATIONCHOOSER_H

#include <qlayout.h>
#include <qwidget.h>
#include <qlineedit.h>
#include <qcombobox.h>
#include <qpopupmenu.h>
#include <qtoolbutton.h>


/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa wspomagaj�ca wyb�r pliku lub katalogu.
 Obiekt tej klasy sk�ada si� z widgetu pobieraj�cego (linijka edycyjna lub
 lista typu 'ComboBox') oraz guzika z menu, kt�re zawiera opcje zwi�zane z
 widgetem pobieraj�cym. Ka�dy element w menu mo�na ukry� i pokaza� jak r�wnie�
 uczyni� nieaktywnym lub aktywnym. Po wstawieniu �cie�ki katalogu wysy�any jest
 sygna�: @em locationChanged() . Wygl�d widgetu mo�na modyfikowa� za pomoc�
 dost�pnych metod. Mo�liwa jest zmiana widgetu pobieraj�cego na linijk�
 edycyjn� lub list� ComboBox, guzik z menu mo�e by� umieszczony z lewej, lub
 z prawej strony widgetu pobieraj�cego, mo�liwe jest r�wnie� jego usuni�cie.
 Lista �cie�ek z widetu ComboBox mo�e by� automatycznie odczytywana oraz
 zapisywana do pliku konfiguracyjnyego (poprzez metod�), je�li ustawiony
 zostanie dla niego klucz.
 */
class LocationChooser : public QWidget
{
	Q_OBJECT
public:
	/** Tryb pobierania scie�ki (dla pliku albo dla katalogu).
	 */
	enum GetMode { GetDirectory=0, GetFile };

	/** Identyfikatory akcji w menu guzika.
	 */
	enum MenuItems { CURRENT_DIR=0, ORIGINAL_DIR, HOME_DIR, ROOT_DIR, PREVIOUS_DIR, NEXT_DIR, SELECT_DIR, CLEAR_CURRENT, CLEAR_ALL, SET_DEFAULT_ACTION };
	/** Rodzaj obiektu pokazywanego obok guzika z menu.
	 */
	enum GettingWidget { LineEdit=0, ComboBox };

	/** Rodzaj strony, na kt�rej b�dzie pokazany guzik, lub jego brak.
	 */
	enum ButtonSide { LeftButton=0, RightButton, NoneButton };


	/** Konstruktor klasy.\n
	 @param parent - wska�nik na rodzica,
	 @param name - nazwa dla obiektu.\n
	 Inicjowane s� tutaj sk�adowe oraz tworzone obiekty sk�adaj�ce si� na bie��cy.
	 */
	LocationChooser( QWidget *parent, const char *name=0 );

	/** Destruktor.\n
	 Usuwane s� tu obiekty u�ywane w klasie.
	 */
	~LocationChooser();

	/** Ustawia bie��c� �cie�k� aplikacji.\n
	 @param currentPath - nazwa bie��cej �cie�ki.
	 */
	void setCurrentPath( const QString & currentPath )   { mCurrentPath  = currentPath;  }

	/** Ustawia �cie�k�, kt�ra by�a bie��c�.\n
	 @param originalPath - poprzednia bie��ca �cie�ka.
	 */
	void setOriginalPath( const QString & originalPath ) { mOriginalPath = originalPath; }

	/** Ukrywa b�d� pokazuje element o podanym Id w menu widoku.\n
	 @param id - identyfikator elementu menu (patrz @see MenuItems),
	 @param visible - TRUE, oznacza �e element b�dzie widoczny, FALSE powoduje
	  jego ukrycie.
	 */
	void setMenuItemVisible( int id, bool visible );

	/** Uaktywnia lub dezaktywuje element o podanym Id w menu widoku.\n
	 @param id - identyfikator elementu menu (patrz @see MenuItems),
	 @param enable - TRUE, oznacza �e element b�dzie aktywny, FALSE powoduje
	  jego dezaktywacje.
	 */
	void setMenuItemEnabled( int id, bool enable );

	/** Ustawia focus na widgetcie pobierania �cie�ki (QLineEdit).
	 */
	void setFocus() { mLineEdit ? mLineEdit->setFocus() : mComboBox->setFocus(); }

	/** Metoda ustawia nazw� klucza (dla pliku konfiguracyjnego), z kt�rego
	 nale�y odczyta� list� URLi.\n
	 @param keyReadingStr - pe�na nazwa klucza.
	 */
	void setKeyListReading( const QString & keyListReadingStr, bool readSettings=FALSE );

	/** Powoduje zaznaczenie bie��cej zawarto�ci widgetu pobieraj�cego.
	 */
	void selectAll();

	/** Powoduje wstawienia podanego elementu na podanej pozycji na list�
	 ComboBox.\n
	 @param itemStr - ci�g tekstowy do wstawienia,
	 @param itemId - pozycja, na kt�rej nale�y umie�ci� ci�g.\n
	 Metoda obs�uguje obiekt pobierania ComboBox, je�li aktualnym jest LineEdit,
	 wtedy poday ci�g jest ustawiany na nim. Je�li podany ci�g ju� istnieje na
	 li�cie, wtedy nie jest on wstawiany.
	 */
	void insertItem( const QString & itemStr, uint itemId=0 );

	/** Wstawiana jest tu na list� ComboBox podana lista ci�g�w tekstowych.\n
	 @param stringList - lista do wstawienia.
	 */
	void insertStringList( const QStringList & stringList );

	/** Metoda powoduje wczytanie listy element�w na list�.\n
	 Przy czym jest robione tylko je�li obiektem pobieraj�cym jest ComboBox oraz
	 nazwa klucza dla pliku konfiguracyjnego nie jest pusta.
	 \n \n Patrz te�: @see setKeyListReading.
	 */
	void init();

	/** Zapisuje do pliku konfiguracyjnego list� obiektu pobieraj�cego ComboBox.\n
	 Przy czym korzysta z wcze�niej ustawionego klucza, je�li jest on pusty to
	 nic nie jest robione.
	 \n \n Patrz te�: @see setKeyListReading.
	 */
	void saveSettings();

	/** Zwraca nast�pny wzgl�dem bie��cego element listy.\n
	 @return nast�pny element listy ComboBox.\n
	 Przy czym, je�li bie��cy jest ostatnim, wtedy zwracany jest pierwszy.
	 */
	QString nextEntry() const;

	/** Zwraca poprzedni wzgl�dem bie��cego element listy.\n
	 @return poprzedni element listy ComboBox.
	 Przy czym, je�li bie��cy jest pierwszym, wtedy zwracany jest ostatni.
	 */
	QString prevEntry() const;

	/** Zwraca aktualn� zawarto�� obiektu pobieraj�cego.\n
	 @return ci�g tekstowy
	 */
	QString text( int id=-1 ) const;

	/** Zwraca informacj� o rodzaju pobierania (plik/katalog).\n
	 @return rodzaj pobierania, patrz @see GetMode.\n
	 W�a�ciwo�� domy�lnie ustawiona jest na 'GetDirectory'.
	 */
	GetMode getMode() const { return mGetMode; }

	/** Zwraca informacj� o tym czy pokazywany jest separator pomi�dzy widgetem
	 pobieraj�cym a guzikiem z menu.\n
	 @return TRUE - separator jest pokazywany, FALSE - nie jest.\n
	 Domy�lnie separator jest pokazywany.
	 */
	bool showSeparator() const { return mShowSeparator; }

	/** Zwraca informacj� o tym czy pokazywane b�d� w menu (dla widgetu ComboBox)
	 opcje: "Previous directory" i "Next directory".\n
	 @return TRUE - opcje s� pokazywane, FALSE - s� ukrywane.\n
	 Domy�lnie opcje te s� ukrywane.
	 */
	bool showPrevNextActions() const { return mShowPrevNextActions; }

	/** Zwraca informacj� o tym z kt�rej strony jest pokazywany guzik z menu,
	 albo czy nie jest pokazywany.\n
	 @return strona pokazywania, patrz te�: @see ButtonSide.
	 */
	ButtonSide buttonSide() const { return mButtonSide; }

	/** Zwraca informacj� o rodzaju widgetu pobieraj�cego.\n
	 @return identyfikator widgetu, patrz te�: @see GettingWidget.
	 */
	GettingWidget gettingWidget() const { return mGettingWidget; }

public slots:
	/** Ustawia podany tekst jako bie��cy widgetcie pobieraj�cym.\n
	 @param text - tekst do ustawienia.
	 */
	void setText( const QString & text );

	/** Ustawia tryb pobierania �cie�ki przez dialog pokazywany po wybraniu opcji
	 "Select directory" z menu guzika.\n
	 @param mode - tryb pobierania (patrz: @see GetMode).\n
	 */
	void setGetMode( GetMode mode ) { mGetMode = mode; }

	/** Ustawia stron� pokazywania guzika lub usuwa go.\n
	 @param btnSide - strona dla guzika lub wymuszenie usuni�cia go.\n
	 Po wywo�aniu tej metody widget jest przebudowywany.
	 */
	void setButtonSide( ButtonSide btnSide );

	/** Metoda powoduje pokazanie lub ukrycie separatora pomi�dzy widgetem
	 pobieraj�cym a guzikiem z menu.\n
	 @param show - r�wne TRUE, spowoduje pokazanie separatora, w przeciwnym razie
	 zostanie on ukryty.
	 */
	void setShowSeparator( bool show );

	/** Umo�liwia zmian� widgetu pobieraj�cego na podany.\n
	 @param getWidget - typ nowego widgetu, patrz te�: @see GettingWidget \n
	 Po wywo�aniu tej metody widget jest przebudowywany.
	 */
	void setGettingWidget( GettingWidget getWidget );

	/** Metoda powoduje �e dla widgetu ComboBox zawsze b�d� pokazywane lub
	 ukrywane opcje z menu "Previous directory" i "Next directory".\n
	 @param showPrevNext - r�wne TRUE, oznacza �e opcje b�d� pokazywane, FALSE
	 m�wi �e nale�y je ukrywa�.
	 */
	void setShowPrevNextActions( bool showPrevNext ) { mShowPrevNextActions = showPrevNext; }

signals:
	/** Sygna� informuje o zmianie �cie�ki w widgetcie pobieraj�cym.\n
	 @param filePath - nowa �cie�ka w widoku.\n
	 Sygna� jest wysy�any po wstawieniu �cie�ki do widoku.
	 */
	void locationChanged( const QString & filePath );

	/** Sygna� wysy�any w momencie zmiany zawarto�ci linijki edycyjnej.\n
	 @param text - nowy tekst
	 */
	void textChanged( const QString & text );

private slots:
	/** Slot u�ywany przez menu guzika przy wybieraniu opcji.\n
	 @param itemId - identyfikator opcji, patrz @see MenuItems.
	 */
	void slotSelectDirMenuItem( int itemId );

	/** Slot pokazuje dialog pobierania �cie�ki do pliku lub katalogu.\n
	 To czy b�dzie wybierany plik czy katalog zale�y od ustawienia sk�adowej
	 @em mGetMode . Patrz te�: @see setGetMode().
	 */
	void slotGetExistingDirectory();

private:
	QString mCurrentPath, mOriginalPath;

	GetMode mGetMode;
	ButtonSide mButtonSide;
	GettingWidget mGettingWidget;
	bool mShowSeparator, mShowPrevNextActions;

	QHBoxLayout *mLayout;
	QComboBox   *mComboBox;
	QLineEdit   *mLineEdit;
	QToolButton *mChooserButton;
	QPopupMenu  *mButtonMenu;

	QString mKeyListReadingStr;

	/** Metoda powoduje budowanie widgetu sk�adaj�cego si� z guzika oraz widgetu
	 pobieraj�cego.\n
	 Wykonywane jest to na podstawie sk�adowych klasy okre�laj�cych wygl�d.
	 */
	void createWidget();

	/** Metoda powoduje utworzenie guzika z menu.
	 */
	void createChooserButton();

};

#endif
