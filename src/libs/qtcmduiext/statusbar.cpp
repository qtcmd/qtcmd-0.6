/***************************************************************************
                          statusbar.cpp  -  description
                             -------------------
    begin                : sun oct 27 2002
    copyright            : (C) 2002 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include <qtimer.h>

#include "statusbar.h"


StatusBar::StatusBar( QWidget *pParent, const char *sz_pName )
	: SqueezeLabel( pParent, sz_pName )
{
	setFrameStyle( QFrame::Panel | QFrame::Sunken );
	setAlignment( AlignCenter );
	setScaledContents( FALSE ); // for list status

	m_pProgress = new QProgressBar( 100, this, 0, WType_Popup );
	m_pProgress->setCenterIndicator( TRUE );
	m_pProgress->hide();

	m_nHtmlTagsWidth = fontMetrics().width( "<font color=red3><b></b></font><b></b><font color=red3><b></b></font>" );

	m_nNumOfSelectedItems = 0;
	m_nWeightOfSelected = 0;
	m_nNumOfAllItems = 0;

	m_eSizeOfMessage = BigMSG;
}


StatusBar::~StatusBar()
{
	delete m_pProgress;
}


void StatusBar::clear( bool bProgress )
{
	if ( bProgress )
		m_pProgress->hide();
	else
		setText("");
}


void StatusBar::setStatusListStr()
{
	QString sNoSI = QString::number( m_nNumOfSelectedItems );
	QString sWoSI = QString::number( m_nWeightOfSelected );
	QString sNoAI = QString::number( m_nNumOfAllItems );

	uint nInsertPos = sWoSI.length();
	// make (max) three digits group, eg. input = 12345,  output = 12 345
	if ( m_eSizeOfMessage != SmallMSG )
		while ( nInsertPos > 3 )  {
			nInsertPos -= 3;
			sWoSI.insert( nInsertPos, " " );
		}

	if ( m_eSizeOfMessage == BigMSG )  {
		m_sListStatusInfo = tr("Selected")+" <font color=red3><b>"+sNoSI+"</b></font> "
			+ tr("files, from")+ " <b>"+sNoAI+"</b>, "+tr("about total size")+" <font color=red3><b>"+sWoSI+"</b></font> "
			+tr("bytes");
	}
	else if ( m_eSizeOfMessage == TinyMSG )  {
		m_sListStatusInfo = "<font color=red3><b>"+sNoSI+"</b></font> "+tr("files, from")+" <b>"+sNoAI+"</b>,"
			+ tr("size")+" <font color=red3><b>"+sWoSI+"</b></font>";
	}
	else if ( m_eSizeOfMessage == SmallMSG )  {
		m_sListStatusInfo = "<font color=red3><b>"+sNoSI+"</b></font> "+", <b>"+sNoAI+"</b>, "
			+"<font color=red3><b>"+sWoSI+"</b></font>";
	}

	m_sStatusInfo = m_sListStatusInfo;
}


void StatusBar::squeezeListStatus()
{
	m_stWidthOfListStatusInfo[m_eSizeOfMessage] = fontMetrics().width( m_sListStatusInfo ) - m_nHtmlTagsWidth;
	uint nWidgetWidth = width();

	if ( nWidgetWidth < m_stWidthOfListStatusInfo[m_eSizeOfMessage]+3 ) {
		if ( m_eSizeOfMessage > SmallMSG ) {
			int nSize = m_eSizeOfMessage;
			nSize--;
			m_eSizeOfMessage = (SizeOfMessage)nSize;
			//(int)m_eSizeOfMessage -= 1;
		}
		setStatusListStr();
	}
	else { // widget is wider than string
		int sil = ((int)m_eSizeOfMessage+1 > BigMSG) ? (int)m_eSizeOfMessage : (int)m_eSizeOfMessage+1;
		// czy widget jest szerszy od ciagu na poziomie biezacy+1 (tu Tiny+1 = Big )
		if ( nWidgetWidth > m_stWidthOfListStatusInfo[sil]+3 ) { // yes, neet to widen string
			if ( m_eSizeOfMessage < BigMSG ) {
				int nSize = m_eSizeOfMessage;
				nSize++;
				m_eSizeOfMessage = (SizeOfMessage)nSize;
			}
		}
	}
	setStatusListStr();
}

// -------- SLOTs --------

void StatusBar::slotSetProgress( int nValue )
{
	if ( nValue == 101 ) {
		m_pProgress->hide();
		return;
	}

	if ( ! m_pProgress->isVisible() ) {
		m_pProgress->move( mapToGlobal(QPoint(0,0)) );
		m_pProgress->show();
	}

	m_pProgress->setProgress( nValue );

	if ( nValue == 100 )
		m_pProgress->hide();
}


void StatusBar::slotSetMessage( const QString & sInfo, uint nMS, bool bShowPrevious )
{
	m_sOldStatusInfo = m_sStatusInfo;
	m_bShowPrevMsg   = bShowPrevious;

	m_sShowOfListStatusInfo = (sInfo.contains("</b>")); // or "</font>"
	if ( m_sShowOfListStatusInfo ) {
		setStatusListStr();
		squeezeListStatus();
	}
	else
		m_sStatusInfo = sInfo;

	setText( m_sStatusInfo );

	if ( nMS )
		QTimer::singleShot( nMS, this, SLOT(slotTimerDone()) );
}


void StatusBar::slotUpdateOfListStatus( uint nNumOfSelectedItems, uint nNumOfAllItems, long long nWeightOfSelected )
{
	m_nNumOfSelectedItems = nNumOfSelectedItems;
	m_nNumOfAllItems      = nNumOfAllItems;
	m_nWeightOfSelected   = nWeightOfSelected;

	m_eSizeOfMessage = BigMSG;
	setStatusListStr();
	squeezeListStatus();

	setScaledContents( FALSE );
	setText( m_sStatusInfo );
}


void StatusBar::slotTimerDone()
{
	setText( m_bShowPrevMsg ? m_sOldStatusInfo : QString("") );
}

// -------- EVENTs --------

void StatusBar::resizeEvent( QResizeEvent * )
{
	if ( m_pProgress->isVisible() )
		m_pProgress->resize( width(), height() );

	if ( m_sShowOfListStatusInfo ) {
		squeezeListStatus();
		setText( m_sStatusInfo ); // jesli w setStatusListStr() na koncu znajdzie sie setText, to ten tu jest zbedny
	}
}
