/***************************************************************************
                          listvieweditbox.h  -  description
                             -------------------
    begin                : Wed Jun 26 2002
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef LISTVIEWEDITBOX_H
#define LISTVIEWEDITBOX_H

#include <qlineedit.h>
#include <qwidget.h>
#include <qmap.h>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Obs�uga okienka edycyjnego pokazywanego na li�cie plik�w.
 Obiekt mo�e pracowa� w dw�ch trybach:
  @li tryb podstawowy jako zwyk�a linijka edycyjna - QLineEdit z obs�ug� skr�t�w
   Ctrl+Insert (kopiowanie do schowka) i Shift+Insert (wklejanie ze schowka),
  @li 'pobieracz' skr�t�w klawiszowych.\n
 Po zako�czeniu pobierania lub potwierdzeniu (klawiszem Return/Enter) wysy�any
 jest sygna� @see signalRename(). Nie jest konieczne ustawianie geometrii dla
 obiektu, poniewa� obiekt sam si� tym zajmuje - dostosowuj�c pozycj� i rozmiar
 do kom�rki elementu listy, na pozycji kt�rej ma zosta� pokazany.\n \n
 @b Uwaga. Obiekt jest przystosowany do pracy jako potomek klasy QListView
 (i dziedzicz�cymi po niej) dlatego nie nale�y go u�ywa� z innymi !
*/
class ListViewEditBox : public QLineEdit {
	Q_OBJECT
public:
	/** Konstruktor obiektu.\n
	 @param param - wska�nik na rodzica (wymagany viewport() obiektu QListView),
	 @param getKeyShortcut - r�wne TRUE wymusza prac� obiektu, jako pobieracza
	  skr�t�w klawiszowych,
	 @param name - nazwa dla obiektu.\n
	 Inicjowane s� tutaj niezb�dne sk�adowe. Ustawiany jest font na taki sam jak
	 u rodzica, poza tym ustawiana jest paleta na warto�� QApplication::palette()
	 (jest to niezb�dne, aby zachowa� standardowe kolory obiektu klasy QLineEdit.
	 Na ko�cu obiekt jest ukrywany.
	 */
	ListViewEditBox( QWidget * parent, bool getKeyShortcut=FALSE, const char * name=0 );

	/** Destruktor.\n
	 Brak definicji.
	 */
	virtual ~ListViewEditBox() {}

	/** Metoda powoduje pokazanie obiektu i zainicjowanie go podanymi parametrami.\n
	 @param column - mumer kolumny, w kt�rej nale�y pokaza� okienko edycyjne,
	 @param treeView - r�wne TRUE, m�wi �e widokiem jest drzewo, w przeciwnym
	  razie to lista.\n
	 Obiekt jest inicjowany bie��c� warto�ci� w kolunmie.
	 */
	virtual void show( int column, bool treeView=FALSE );

	/** Zwraca numer kolumny, w kt�rej jest pokazywany obiekt.\n
	 @return numer kolumny.
	 */
	uint column() const { return mColumn; }

	/** W��cza/wy��cza tryb pracy 'pobieracza' skr�t�w klawiszowych.\n
	 @param getKeyShortcut - r�wne TRUE, w��cza tryb pracy 'pobieracza', FALSE
	  oznacza prace jako zwyk�a linijka edycyjna.
	*/
	void setGetKeyShortcutMode( bool getKeyShortcut ) { mGetKeyShortcut = getKeyShortcut; }

private:
	QWidget *mParent;
	bool mGetKeyShortcut;
	QString mOldText, mStateStr;
	uint mColumn;

	typedef QMap<int, QString> KeysMap;
	KeysMap mKeysStateMap;


	/** Metoda czy�ci zawarto�� oraz ukrywa bie��cy obiekt.
	 */
	void hideMe();


protected:
	/** Zawiera obs�ug� niekt�rych klawiszy kontrolnych.\n
	 @param e - wska�nik zdarzenia klawiatury.\n
	 W trybie pracy jako linijka edycyjna przechwytywane s� nast�puj�ce klawisze:
	  @li Key_Enter, Key_Return (wys�anie sygna�u @em signalRename() i ukrycie
	   obiektu);
	  @li Key_Up, Key_Down, Key_Escape (powoduj� ukrycie okienka edycyjnego);
	  @li Ctrl+Insert (wykonuje kopiowanie do schowka zaznaczonego ci�gu);
	  @li Shit+Insert (wklejanie ze schowka).
	 */
	void keyPressEvent( QKeyEvent * e );

	/** Obs�uga zdarze� dla bie��cego obiektu.\n
	 Przchwytywane s� tutaj zdarzenia: 'FocusOut' oraz wci�ni�cia klawisza Tab,
	 kt�re powoduj� ukrycie okienka edycyjnego.
	 */
	bool event( QEvent * e );

signals:
	/** Sygna� wysy�any w momencie zatwierdzenia nowego tekstu.\n
	 @param column - numer kolumny, w kt�rej jest pokazywany obiekt,
	 @param oldText - pierwotna warto�� kom�rki (bie��cy tekst z kom�rki elementu
	  listy),
	 @param newText - nowy warto�� tekstowa dla kom�rki elementu listy
	 */
	void signalRename( int column, const QString & oldText, const QString & newText );

};

#endif
