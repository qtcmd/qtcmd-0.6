/***************************************************************************
                          squeezelabel.cpp  -  description
                             -------------------
    begin                : sun aug 10 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
***************************************************************************/

#include "squeezelabel.h"

#include <qregexp.h>
#include <qtooltip.h>
#include <qstringlist.h>


SqueezeLabel::SqueezeLabel( QWidget * parent, const char * name, WFlags f )
	: QLabel( parent, name, f ), mShowTip( TRUE )
{
	setScaledContents( TRUE );
}


void SqueezeLabel::setShowToolTip( bool show )
{
	mShowTip = show;
}


void SqueezeLabel::setText( const QString & text )
{
	if ( ! hasScaledContents() )
		QLabel::setText( text );
	else {
		mFullText = text;
		setSqueezingText( width() );
	}
}


void SqueezeLabel::setSqueezingText( uint maxWidth )
{
	if ( ! hasScaledContents() )
		return;

	QString fullText = mFullText;

	if ( mFullText.contains('\n') ) {
		fullText = "";
		QStringList textLines = QStringList::split( '\n', mFullText, TRUE );
		for ( uint i=0; i<textLines.count(); i++)
			fullText += squeezeLine( textLines[i], maxWidth );
	}
	else
		fullText = squeezeLine( mFullText, maxWidth );

	if ( fullText.at(fullText.length()-1) == '\n' )
		fullText.remove( fullText.length()-1, 1 ); // remove last "\n"
// 	fullText.replace( QRegExp("\\n\\n"), "\n\n\n\n\n\n" ); // need to fix bug in Qt-3.2.0
	QLabel::setText( fullText );

	int p;
	if ( (p=fullText.find("...")) != -1 && fullText != "..." ) {
		if ( mShowTip ) {
			QToolTip::remove( this ); // remove previous tip
			QToolTip::add( this, mFullText );
		}
	}
	else
		if ( mShowTip )
			QToolTip::remove( this );
}


QString SqueezeLabel::squeezeLine( const QString & sLine, uint nLabelWidth )
{
	if ( sLine.isEmpty() || sLine == "..." )
		return QString("\n");

	QString sNewLine = sLine;
	// replace each occurence of tab character to 8 space char.
	sNewLine.replace( QRegExp("\\t"), "        " );

	// TODO need to add a rich text support !

	QString leftHalfOfText, rightHalfOfText;
	uint nTextWidthInPixels = fontMetrics().width( sNewLine, sNewLine.length() );
	uint nThreeDotsWidth = fontMetrics().width( "...", QString("...").length() );

	if ( nLabelWidth < nTextWidthInPixels ) {
		// text measuring
		QString sChr;
		uint i, nTxtWidth = 0;
		for (i=0; i<sNewLine.length(); i++) {
			sChr = sNewLine[i];
			nTxtWidth += fontMetrics().width(sChr, 1);
			if (nTxtWidth+nThreeDotsWidth > nLabelWidth)
				break;
		}
		leftHalfOfText  = sNewLine.left( i/2 );
		rightHalfOfText = sNewLine.right( i/2 );
		sNewLine = leftHalfOfText + "..." + rightHalfOfText;
	}

	return sNewLine+"\n";
}


void SqueezeLabel::resizeEvent( QResizeEvent * )
{
	setSqueezingText( width() );
}

