/***************************************************************************
                          listviewwidgets.h  -  description
                             -------------------
    begin                : fri mar 26 2004
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
***************************************************************************/

#ifndef LISTVIEWWIDGETS_H
#define LISTVIEWWIDGETS_H

#include <qlistview.h>

enum Widget { CheckBox=0, RadioButton, ColorFrame, ComboBox, EditBox, TextLabel };

class ListViewWidgets;

/** @short Klasa elementu listy widget�w (klasy ListViewWidgets).
 Zajmuje si� ona rysowaniem niekt�rych widget�w. Obs�uguje nast�puj�ce widety:
 CheckBox, RadioButton, ColorFrame, ComboBox, EditBox, TextLabel. Umo�liwia
 ustawianie oraz pobieranie warto�ci poszczeg�lnych widget�w. Widget mo�na
 ustawi� jako nieaktywny (dotyczy to tylko element�w typu TextLabel) lub
 aktywny. Rysowany jest r�wnie� tutaj kursor dla listy w postaci dwukolorowej
 ramki.
 */
class ListViewWidgetsItem : public QListViewItem
{
public:
	/** Konstruktor klasy.\n
	 @param parent - wska�nik na rodzica,
	 @param w1 ... @param w8 - rodzaj widgetu w kolejnych kom�rkach.\n
	 Inicjowana jest tutaj tablica widget�w warto�ciami parametr�w. Czyszczone s�
	 te� tablice w��czonych widget�w o oraz tablica kolor�w (dla widget�w
	 pokazuj�cych kolory).
	 */
	ListViewWidgetsItem( ListViewWidgets *parent,
	 Widget w1=TextLabel, Widget w2=TextLabel, Widget w3=TextLabel, Widget w4=TextLabel, Widget w5=TextLabel, Widget w6=TextLabel, Widget w7=TextLabel, Widget w8=TextLabel
	);

	/** Destruktor.\n
	 Brak definicji.
	 */
	virtual ~ListViewWidgetsItem() {}

	/** Metoda jest odpowiedzialna ze rysowanie kom�rki elementu.\n
	 @param p - wska�nik 'rysownika' zwi�zany z otwartym urz�dzeniem rysuj�cym,
	 @param cg - grupa kolor�w do u�ycia,
	 @param col - bie��ca kolumna, w kt�rej nast�puje rysowanie,
	 @param width - szeroko�� bie��cej kolumny,
	 @param alignment - strona do ustawiania zawarto�ci rysowanego elementu,
	 (lewa,centrum,prawa).\n
	 W kom�rce takiej znale�� si� mo�e CheckBox, RadioButton, ColorFrame.
	 W przypadku pozosta�ych widget�w rysowanie jest przekazywane do klasy
	 macierzystej (QListView).
	*/
	virtual void paintCell( QPainter *p, const QColorGroup & cg, int col, int widht, int alignment );

	/** Metoda odpowiedzialna za rysowanie kursora.\n
	 @param p - wska�nik 'rysownika' zwi�zany z otwartym urz�dzeniem rysuj�cym
	 @param cg - grupa kolor�w do u�ycia,
	 @param r - geometria dla kursora.
	 Kursor jest rysowany na kom�rce kt�rej numer zwraca metoda
	  @see ListViewWidgets::cursorForColumn()
	 */
	virtual void paintFocus( QPainter *p, const QColorGroup & cg, const QRect & r );

	/** Zwraca przyci�ty, podany ci�g tekstowy, kt�ry jest dopasowany do
	 szeroko�ci podanego numeru kolumny listy.\n
	 @param str - ci�g tekstowy do przyci�cia,
	 @param column - numer kolumny, w kt�rej znajduje si� podany ci�g,
	 @param columnWidth - szeroko�� kolumny, w kt�rej znajduje si� podany ci�g,
	 @param fontWeight - TRUE je�li ci�g jest pisany fontem QFont::bold,
	 w przeciwnym razie FALSE.
	 */
	QString trimedString( const QString & str, const int & column, const int & columnWidth, const int & fontWeight );

	/** Pobiera warto�� tekstow� z podanej kolumny.\n
	 @param column - kolumna, z kt�rej nale�y pobra� warto�� tekstow�,
	 @return ci�g tekstowy.\n
	 Przy czym zwracana jest prawdziwa warto��, tylko je�li w podanej kolumnie
	 znajduje si� jeden z nast�puj�cych widget�w: TextLabel, ComboBox, EditBox.
	 */
	QString textValue( uint column ) const;

	/** Ustawia podany tekst w podanej kolumnie.\n
	 @param column - kolumna, w kt�rej nale�y ustawi� podany tekst
	 @param str - tekst do ustawienia.\n
	 Przy czym, tekst jest ustawiany tylko je�li w podanej kolumnie znajduje si�
	 jeden z nast�puj�cych widget�w: TextLabel, EditBox, ComboBox
	 */
	void setTextValue( uint column, const QString & str );

	/** Pobiera logiczny status widgeta z podanej kolumny.\n
	 @param column - kolumna, z kt�rej nale�y pobra� warto�� logiczn�
	 @return warto�� logiczna (TRUE lub FALSE).\n
	 Przy czym zwraca prawdziw� warto��, tylko je�li znajduje si� w niej jeden
	 z nast�puj�cych widget�w: CheckBox, RadioButton
	 */
	bool boolValue( uint column ) const;

	/** Ustawia warto�� logiczn� w podanej kolumnie.\n
	 @param column - kolumna, w kt�rej nale�y ustawi� podan� warto�� logiczn�
	 @param value - warto�� do ustawienia (TRUE lub FALSE).\n
	 Przy czym warto�� jest ustawiana tylko, je�li w podanej kolumnie znajduje si�
	 jeden z nast�puj�cych widget�w: CheckBox, RadioButton.
	 */
	void setBoolValue( uint column, bool value );

	/** Pobiera kolor widgeta z podanej kolumny.
	 @param column - kolumna, z kt�rej nale�y pobra� warto�� QColor
	 @return kolor - zwracany kolor widgetu z podanej kolumny.\n
	 Przy czym zwraca prawdziw� warto��, tylko je�li znajduje si� w niej widget
	 typu ColorFrame.
	 */
	QColor colorValue( uint column ) const;

	/** Ustawia kolor widgeta w podanej kolumnie.\n
	 @param column - kolumna, w kt�rej nale�y ustawi� podan� warto�� typu QColor,
	 @param color - warto�� QColor do ustawienia.\n
	 Przy czym warto�� jest ustawiana tylko, je�li w podanej kolumnie znajduje si�
	 widget typu ColorFrame.
	 */
	void setColorValue( uint column, const QColor & color );

	/** Wstawia w widgetcie dla podanej kolumny podan� list� ci�g�w tekstowych.\n
	 @param column - kolumna, w kt�rej nale�y wstawi� list�,
	 @param stringList - lista ci�g�w tekstowych.\n
	 Przy czym lista jest wstawiana tylko je�li w podanej kolumnie znajduje si�
	 widgetu typu ComboBox.
	 */
	void insertStringList( uint column, const QStringList & stringList );

	/** Zwraca list� ci�g�w tekstowych.\n
	 @param column - kolumna, z kt�rej nale�y pobra� list�,
	 @return lista ci�g�w tekstowych.\n
	 Przy czym zwraca prawdziw� warto��, tylko je�li znajduje si� w niej widget
	 typu ComboBox.
	 */
	QStringList stringList( uint column ) const;

	/** Ustawia atrybuty dla tekstu w podanej kolumnie,
	 @param column - kolumna w kt�rej znajduje si� tekst
	 @param color - kolor tekstu
	 @param bold - okre�la czy tekst ma by� pogrubiony
	 @param italic - okre�la czy tekst ma by� pochylony \n
	 Atrybuty s� ustawiane tylko je�li w podanej kolumnie znajduje si� jeden
	 z nast�puj�cych widget�w: TextLabel, EditBox, ComboBox. \n
	 @b UWAGA. Atrybuty mo�na ustawi� tylko w kolumnie, dla kt�rej jest ustawiony
	 kursor (patrz: @see ListViewWidgets::setCursorForColumn() )
	*/
	void setTextAttributs( uint column, const QColor & color, bool bold=FALSE, bool italic=FALSE );

	/** Zwraca rodzaj widgetu z podanej kolumny.\n
	 @param column - numer kolumny,
	 @return rodzaj widgetu.
	 */
	Widget widget( uint column ) const { return mWidgetsTab[column]; }

	/** Ustawia w podanej kolumnie podany widget.\n
	 @param column - kolumna w kt�rej trzeba ustawi� widget
	 @param widget - widget do ustawienia (CheckBox, ColorFrame, ComboBox, EditBox,
	 RadioButton, TextLabel)
	 */
	void setWidget( uint column, Widget widget );

	/** W��cza lub wy��cza tryb exclusive.\n
	 @param exlusive - TRUE w��cza tryb 'exlusive', FALSE wy��cza ten tryb dla podanych widget�w,\n
	 @param widget   - rodzaj widget�w, kt�re powinny dzia�a� w trybie exlusive
	 (tryb ten jest mo�liwy tylko dla widget�w typu: 'CheckBox' i 'RadioButton')
	 */
	void setExclusiveButtons( bool exclusive, Widget widget );

	/** Zwraca status trybu 'exlcusive' dla widget�w typu CheckBox i RadioButton.\n
	 @return TRUE tryb exclusive jest w��czony, dla FALSE wy��czony.
	 */
	bool exclusiveButtons() const { return mExclusiveButtons; }

	/** Prze��cza widget w podanej kolumnie na aktywny lub nieaktywny.\n
	 @param column - kolumna, w kt�rej znajduje si� widget do zmiany statusu,
	 @param enable - TRUE oznacza tryb aktywny, w przeciwnym razie w��czany jest
	 tryb nieaktywny.\n
	 Przy czym dotyczy to tylko widget�w typu: TextLabel, EditBox, ComboBox.
	 */
	void setEnableCell( uint column, bool enable=TRUE );

	/** Zwraca status aktywno�ci widgeta w podanej kolumnie.\n
	 @param column - kolumna, w kt�rej znajduje si� widget do sprawdzenia,
	 @return TRUE - oznacza, �e widget jest aktywny, dla FALSE jest nieaktywny.\n
	 Przy czym prawdziwa warto�� jest zwracana tylko dla widget�w typu: TextLabel,
	 EditBox, ComboBox.
	 */
	bool isEnableCell( uint column ) const { return (mColorsTab[column] == Qt::black); }

private:
	ListViewWidgets *mParentList;

	Widget mWidgetsTab[8];

	bool mCheckedTab[8];
	QColor mColorsTab[8];
	QStringList mComboBoxLists[8];

	Widget mExlusiveButton;
	bool mExclusiveButtons;

	bool mBold, mItalic;
	QColor mTextColor;//, mTextBgColor;

protected:
	/** Ustawiane s� tutaj warto�ci widgetow w tablicy stanow.\n
	 @param column - kolumna klini�tego wdigeta.\n
	 Je�li widget w podanej kolumnie ma w��czony tryb exclusive, wtedy zostaje
	 mu ustawiona w tablicy warto�� TRUE, natomiast pozosta�ym FALSE. Je�eli
	 nie ma w�aczenego tego trybu, wtedy jest pomijany.\n
	 */
	void setExclusivelyButton( uint column );

};


#include "listvieweditbox.h"

#include <qcombobox.h>


/** @short Klasa obs�uguje list� element�w typu ListViewWidgetsItem.
 W kom�rkach tego elementu mo�na wstawi� nast�puj�ce rodzaje widget�w:
  @li warto�� logiczna (z edycj�): CheckBox, RadioButton (dzia�aj� tak samo,
   a r�ni� si� tylko wygl�dem)
  @li lista ci�g�w tekstowych - ComboBox
  @li ramka z kolorem - ColorFrame
  @li linijka edycyjna - EditBox
  @li tekst - TextLabel.\n
 Element listy mo�e mie� powy�sze widgety maksymalnie w o�miu pierwszych
 kom�rkach. Widgetom CheckBox i RadioButton mo�na ustawi� tryb pracy "exlusive"
 (domy�lnie wy��czony). Klikni�cie w ColorFrame pokazuje dialog wyboru koloru.
 Widget EditBox mo�e pracowa� w dw�ch trybach. Mianowicie jako pobieracz skr�t�w
 klawiszowych oraz jako zwyk�a linijka edycyjna. Po klikni�ciu w ten widget
 pokazywane jest okienko edycyjne pracuj�ce w jednym z powy�szych tryb�w.
 Klikni�cie w widget typu ComboBox powoduje pokazanie obiektu typu QComboBox.
 Widget ten mo�na zainicjowa� list� ci�g�w tekstowych, kt�ra b�dzie pokazywana
 przy jego rozwini�ciu. Dost�pna jest mo�liwo�� ustawienia kursora dla wybranej
 kolumny. Po klikni�ciu w interaktywny widget wysy�any jest odpowiedni sygna�.
 Mianowicie dla ColorFrame - "colorChanged()", dla CheckBox i RadioButton -
 "buttonClicked()", dla EditBox - "editBoxTextApplied()".
*/
class ListViewWidgets : public QListView
{
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param parent - wska�nik na rodzica,
	 @param name - nazwa dla obiektu.\n
	 Tworzone s� tutaj obiekty klas ListViewEditBox oraz QComboBox,
	 a tak�e ��czone niezb�dne sygna�y ze slotami i inicjowane sk�adowe.
	 */
	ListViewWidgets( QWidget *parent = 0, const char *name = 0);

	/** Destruktor.\n
	 Usuwane s� tutaj obiekty klas ListViewEditBox oraz QComboBox.
	 */
	virtual ~ListViewWidgets();

// 	void addColumn( Widget widget );

	/** Zwraca drugi kolor t�a listy.\n
	 @return kolor.
	 */
	QColor secondColorOfBg() const { return mSecondColorOfBg; }

	/** Metoda ustawia drugi kolor t�a (t�o mo�e by� w paski).\n
	 @param c - nowy kolor drugiego paska t�a (pierwszy jest zawsze bia�y).
	 */
	void setSecondColorOfBg( const QColor & c );

	/** Zwraca informacj� o tym czy t�o jest dwukolorowe.\n
	 @return TRUE - oznacza, �e t�o jest dwukolorowe, w przeciwnym razie ma jeden.
	 */
	bool enableTwoColorsOfBg() const { return mEnableTwoColorsOfBg; }

	/** W��cza/wy��cza dwukolorowe t�o.\n
	 @param TRUE - wymusza rysowanie dwukolorowego t�o, w przeciwnym razie b�dzie
	 mie� jeden kolor.\n
	 Domy�lnie w�a�ciwo�� ta jest wy��czona.
	 */
	void setEnableTwoColorsOfBg( bool enable = TRUE );

//		QColor insideFrameCursorColor() const { return mInsideFrameCursorColor; }
// 	void setInsideFrameCursorColor( const QColor & color ) {
// 		mInsideFrameCursorColor = color;  triggerUpdate();
// 	}
//		QColor outsideFrameCursorColor() const { return mOutsideFrameCursorColor; }
// 	void setOutsideFrameCursorColor( const QColor & color ) {
// 		mOutsideFrameCursorColor = color; triggerUpdate();
// 	}

	/** Pokazuje widget w podanej kolumnie na pozycji podanego elementu.\n
	 @param column - kolumna, w kt�rej nale�y pokaza� widget,
	 @param item - wska�nik na element, na kt�rym nale�y pokaza� widget.
	 Metoda obs�uguje widgety: ComboBox i EditBox.
	 */
	void showWidget( int column, ListViewWidgetsItem *itemPtr=0 );

	/** Chowa widget w podanej kolumnie na pozycji bie��cego elementu, tylko je�li
	 znajduje si� w niej ComboBox lub EditBox.\n
	 @param column - kolumna w kt�rej nale�y ukry� widget.
	 */
	void hideWidgetForCurrentItem( int column );

	/** Ustawia numer kolumny, w kt�rej b�dzie pokazywany kursor.\n
	 @param column - numer kolumny.
	*/
	void setCursorForColumn( uint column ) {
		if ( column <= (uint)columns() )  mCursorForColumn = column;
	}

	/** Zwraca numer kolumny, kt�rej jest pokazywany kursor.\n
	 @return numer kolumny.
	*/
	uint cursorForColumn() const { return mCursorForColumn; }

	/** Zwraca status pracy widgetu typu EditBox jako pobieracz kod�w klawiszy.\n
	 @return TRUE widget EditBox pracuje jako pobieracz kod�w klawiszy, FALSE
	  oznacza �e widget pracuje jako zwyk�a linijka edycyjna.
	 */
	bool editBoxGetKeyShortcutMode() const { return mEditBoxGetKeyShortcut; }

	/** Ustawia tryb pracy widgetu EditBox jako jako pobieracz kod�w klawiszy.\n
	 @param editBoxGetKeyShortcutMode - r�wne TRUE oznacza, �e EditBox b�dzie
	 pobiera� kody klawiszy, w przeciwnym razie b�dzie linijk� edycjn�.
	 */
	void setEditBoxGetKeyShortcutMode( bool editBoxGetKeyShortcut ) { mEditBoxGetKeyShortcut = editBoxGetKeyShortcut; }

private slots:
	/** Slot obs�uguje klikni�cie w kom�rk� elementu listy.\n
	 Je�li w podanej kolumnie znajduje si� jeden z podanych widget�w: ComboBox,
	 EditBox, wtedy jest on pokazywany. Je�li w podanej kolumnie znajduje si�
	 jeden z podanych widget�w: CheckBox, RadioButton, wtedy nast�puje zmiana jego
	 stanu. Je�li kolumna zawiera widget ColorFrame, wtedy pokazywany jest dialog
	 wyboru koloru. \n
	 @param item - wska�nik klikni�tego elementu
	 @param vp - globalna pozycja kursora w momencie klikni�cia
	 @param column - kolumna, w kt�rej klikni�to
	 */
	void slotClicked( QListViewItem * item, const QPoint & point, int column );

	/** Slot wywo�ywany w momencie zmiany rozmiaru kolumn.\n
	 Je�li w kolumnie znajduje si� widget (widoczny): ComboBox lub EditBox, wtedy
	 zmieniany jest jego rozmiar (i ewentualnie pozycja).\n
	 @param section - kolumna, kt�ra ma zmieniany rozmiar
	 @param oldSize - szeroko�� kolumny sprzed zmiany rozmiaru
	 @param newSize - szeroko�� kolumny po zmianie rozmiaru
	 */
	void slotHeaderSizeChange( int section, int oldSize, int newSize );

	/** Ustawia jako bie��cy numer elementu dla widgeta typu ComboBox numer
	 podanego elementu.\n
	 @param column - kolumna, w kt�rej znajduje si� widget ComboBox,
	 @param id - numer elementu, kt�ry ma by� aktywny.
	 */
	void slotSetCurrentComboItem( int id );

private:
	bool mEditBoxGetKeyShortcut;
	uint mCursorForColumn;
	int mCurrentColumn;
	int mEditColumn;

	bool mEnableTwoColorsOfBg;
	QColor mSecondColorOfBg;
	//QColor mInsideFrameCursorColor,  mOutsideFrameCursorColor;

	ListViewEditBox *mEditBox;
	QComboBox *mComboBox;

signals:
	/** Sygna� wysy�any gdy zostanie zmieniony kolor w widgetcie ColorFrame
	 w podanej kolumnie.\n
	 @param column - kolumna, w kt�rej znajduje si� wymagany widget.
	 */
	void colorChanged( int column );

	/** Sygna� wysy�any w momencie, gdy zostanie klikni�ty widget typu CheckBox
	 lub RadioButton w podanej kolumnie.\n
	 @param column - kolumna, w kt�rej znajduje si� wymagany widget.
	 */
	void buttonClicked( int column );

	/** Sygna� wysy�any, gdy zostanie zmieniona zawarto�� widgetu EditBox.\n
	 @param column - kolumna, w kt�rej znajduje si� wymagany widget,
	 @param oldText - poprzedni tekst z podanej kolumny,
	 @param newText - nowy tekst dla podanej kolumny.
	 */
	void editBoxTextApplied( int column, const QString & oldText, const QString & newText );

};

#endif
