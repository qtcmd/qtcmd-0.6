/***************************************************************************
                          keyshortcuts.h  -  description
                             -------------------
    begin                : sun aug 1 2004
    copyright            : (C) 2004 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _KEYSHORTCUT_H_
#define _KEYSHORTCUT_H_

#include <qobject.h>
#include <qvaluelist.h>

/** @short Klasa pomocnicza trzymaj�ca opis pojedynczego kodu klawiszowego.
 Na opis skr�tu sk�adaj� si� nast�puj�ce rzeczy: identyfikator akcji, opis
 akcji, domy�lny kod klawisza, aktualny kod klawisza. W celu utworzenia kodu
 nale�y zainicjowa� konstruktor. Klasa udost�pnia niezb�dne metody do
 pobierania i ustawiania warto�ci kodu, przy czym ustawi� mo�na tylko kod
 klawiszowy.
 */
class KeyCode
{
public:
	/** Konstruktor.\n
	 @param act - identyfikator akcji,
	 @param desc - opis akcji,
	 @param dfltCode - domy�lny kod klawisza,
	 @param _code - w�a�ciwy kod klawisza.\n
	 Inicjalizowane s� tutaj sk�adowe klasy.
	 */
	KeyCode( int act, const QString & desc, int dfltCode, int _code=Qt::Key_unknown ) :
		mAction(act), mDescription(desc), mDefaultCode(dfltCode), code(_code)
	{}

	/** Destruktor.\n
	 Nie zawiera definicji.
	 */
	~KeyCode() {}

	/** Zwraca opis akcji.\n
	 @return opis akcji.
	 */
	QString description() const { return mDescription; }

	/** Zwraca domy�lny kod klawisza.\n
	 @return domy�lny kod klawisza.
	 */
	int defaultCode() const { return mDefaultCode; }

	/** Zwraca identyfikator akcji.\n
	 @return identyfikator akcji.
	 */
	int action() const { return mAction; }

private:
	int mAction;
	QString mDescription;
	int mDefaultCode;
	int mCode;

public:
	/** Aktualny kod klawisza.
	 */
	int code;

};

// ooooooooooo-----------         -------------ooooooooooo

/** @short Klasa obs�uguj�ca skr�ty klawiszowe.
 Klasa przeznaczona do edycji skr�t�w klawiszowych u�ywanych w oknie podgl�du
 oraz oknie g��wnym aplikacji. Trzymana jest tutaj lista powy�szych,
 udost�pniane s� metody konieczne do pobierania i ustawiania skr�t�w. W
 momencie inicjacji nast�puje pr�ba odczytania ich z pliku konfiguracyjnego,
 je�li si� to nie uda przyjmowane s� domy�lne warto�ci.
*/
class KeyShortcuts : public QObject
{
	Q_OBJECT
public:
	/** Konstruktor.\n
	 Czyszczona jest tutaj tylko lista skr�t�w.
	 */
	KeyShortcuts();

	/** Destruktor.\n
	 Nie zawiera definicji.
	 */
	virtual ~KeyShortcuts() {}

	/** Powoduje uaktualnienie kod�w skr�t�w klawiszowych na podstawie klucza z
	 pliku konfiguracyjnego.\n
	 @param configFileKey - klucz dla pliku konfiguracyjnego.\n
	 Przy czym przed wywo�aniem tej funkcji nale�y wype�ni� list� domy�lnymi
	 warto�ciami, patrz: @see append.
	 */
	virtual void updateEntryList( const QString & configFileKey );

	/** Zwraca skr�t klawiszowy dla podanego jego identyfikatora akcji.\n
	 @param action - identyfikator akcji dla skr�tu klawiszowego, (patrz te�:
	  @see ActionsOfView, oraz @see ActionsOfApp),
	 @return skr�t klawiszowy.
	 */
	virtual int key( int action );

	/** Metoda powoduje ustawienie podanego skr�tu klawiszowego dla podanego
	 jego opisu.\n
	 @param action - identyfikator akcji dla skr�tu klawiszowego, (patrz te�:
	  @see ActionsOfView, oraz @see ActionsOfApp),
	 @param shortcut - nowy skr�t do ustawienia.
	 */
	virtual void setKey( int action, int shortcut );

	/** Metoda powoduje ustawienie podanego skr�tu klawiszowego dla podanego
	 jego identyfikatora akcji.\n
	 @param description - opis akcji dla skr�tu klawiszowego,
	 @param shortcut - nowy skr�t do ustawienia.
	 */
	virtual void setKey( const QString & description, int shortcut );


	/** Zwraca aktualn� ilo�� skr�t�w klawiszowych.\n
	 @return liczba skr�t�w klawiszowych.
	 */
	uint count() const { return mKeyCodeList.count(); }

	/** Metoda powoduje wyczyszczenie listy skr�t�w klawiszowych.
	 */
	void clear() { mKeyCodeList.clear(); }

	/** Metoda umo�liwia dodanie skr�tu klawiszowego do listy.\n
	 @param action - identyfikator akcji,
	 @param description - opis akcji,
	 @param defaultKeyShortcut - domy�lny skr�t klawiszowy. Przy czym w przypadku
	 kombinacji klawiszowych nale�y u�y� typu 'Qt::Modifier', np. Qt::CTRL.
	 */
	virtual void append( int action, const QString & description, int defaultKeyShortcut=Qt::Key_unknown ) {
		mKeyCodeList.append( new KeyCode(action, description, defaultKeyShortcut) );
	}

	/** Wykonuje konwersje z typu QKeyEvent to QKeySequence i zwraca ten drugi.\n
	 @param ke - wska�nika na zdarzenie KeyEvent,
	 @return zwraca skr�t klawiszowy typu QKeySequence.
	 */
	virtual int ke2ks( QKeyEvent *ke );

	/** Zwraca ci�g tekstowy domy�lnego skr�tu klawiszowego dla podanego Id.\n
	 @return ci�g tekstowy opisuj�cy skr�t klawiszowy.
	 */
	virtual QString keyDefaultStr( uint index ) const;

	/** Zwraca ci�g tekstowy aktualnego skr�tu klawiszowego dla podanego Id.\n
	 @return ci�g tekstowy opisuj�cy skr�t klawiszowy.
	 */
	virtual QString keyStr( uint index ) const;

	/** Zwraca opis akcji dla podanego Id.
	 @return opis akcji.
	 */
	QString desc( uint index ) const {
		return (*mKeyCodeList.at( index ))->description();
	}

private:
	typedef QValueList <KeyCode *> KeyCodeList;
	KeyCodeList mKeyCodeList;

};

#endif
