/***************************************************************************
                          filesassociation.cpp  -  description
                             -------------------
    begin                : wed may 12 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "filesassociation.h"

#include <qfile.h>
#include <qsettings.h>
#include <qvaluelist.h>


FilesAssociation::FilesAssociation()
{
	mFilesAssociationLoaded = FALSE;
	readFilesAssociation();
}


void FilesAssociation::readFilesAssociation()
{
	mFilesAssociationList.clear();

	// --- read all types of file
	QStringList fileTypesList;
	QFile f( QDir::homeDirPath()+"/.qt/qtcmd_mimerc" );
	if ( f.exists() ) {
		if ( f.open( IO_ReadOnly ) )  {
			QString lineBuffer;
			QTextStream t( &f );        // use a text stream
			while ( ! t.eof() )  {        // until end of file...
				lineBuffer = t.readLine();       // line of text excluding '\n'
				if ( ! lineBuffer.isEmpty() )
					if ( lineBuffer[0] == '[' )
						fileTypesList.append( lineBuffer.mid(1, lineBuffer.find(']')-1) );
			}
			f.close();
		}
	}
	else
		return;

	// --- read all FilesAssociation for each type of file
	//     and append it to the FilesAssociationList
	QString fileType, kindOfFileStr;
	QSettings settings;
	FileAssociation fa;
	bool externalViewer;

	for ( uint i=0; i<fileTypesList.count(); i++ ) {
		fileType = fileTypesList[i];

		fa.fileType    = fileType;
		fa.appsForView = settings.readEntry( "qtcmd_mime/"+fileType+"/Application", "" );
		fa.description = settings.readEntry( "qtcmd_mime/"+fileType+"/Description", "" );
		fa.templates   = settings.readEntry( "qtcmd_mime/"+fileType+"/FileTemplates", "" );
		externalViewer = settings.readBoolEntry( "qtcmd_mime/"+fileType+"/ExternalViewer", FALSE );
		kindOfFileStr  = settings.readEntry( "qtcmd_mime/"+fileType+"/KindOfFile", "Unknown" );

		fa.kindOfViewer = (externalViewer) ? EXTERNALviewer : INTERNALviewer;
		fa.kindOfFile   = kindOfFileFromName( kindOfFileStr );

		mFilesAssociationList.append( fa );
		if ( i == 1 ) {
			mFilesAssociationLoaded = TRUE;
			debug("FilesAssociation(), files association has been loaded");
		}
	}
}


KindOfFile FilesAssociation::kindOfFile( const QString & fileName )
{
	if ( fileName.isEmpty() || ! mFilesAssociationLoaded )
		return UNKNOWNfile;

	Iterator it = find( fileName );
	KindOfFile kof = (it != mFilesAssociationList.end()) ? (*it).kindOfFile : UNKNOWNfile;

	return kof;
}


QString FilesAssociation::fileTemplates( const QString & fileType )
{
	if ( fileType.isEmpty() || ! mFilesAssociationLoaded )
		return QString::null;

	QString templates = "";
	bool notFoundFileType = TRUE;
	FilesAssociationList::iterator it;
	for (it = mFilesAssociationList.begin(); it != mFilesAssociationList.end(); ++it) {
		if ( (*it).fileType == fileType ) {
			templates = (*it).templates;
			notFoundFileType = FALSE;
		}
	}
	if ( notFoundFileType )
		debug("'%s' - unknown file type !", fileType.latin1() );

	return templates;
}


KindOfViewer FilesAssociation::kindOfViewer( const QString & fileName )
{
	if ( fileName.isEmpty() || ! mFilesAssociationLoaded )
		return UNKNOWNviewer;

	Iterator it = find( fileName );
	KindOfViewer kov = (it != mFilesAssociationList.end()) ? (*it).kindOfViewer : UNKNOWNviewer;

	return kov;
}


QString FilesAssociation::appsForView( const QString & fileName )
{
	if ( fileName.isEmpty() || ! mFilesAssociationLoaded )
		return QString::null;

	Iterator it = find( fileName );
	QString viewer = (it != mFilesAssociationList.end()) ? (*it).appsForView : QString::null;

	return viewer;
}


QString FilesAssociation::fileDescription( const QString & fileName )
{
	if ( fileName.isEmpty() || ! mFilesAssociationLoaded )
		return QString::null;

	Iterator it = find( fileName );
	QString desc = (it != mFilesAssociationList.end()) ? (*it).description : QString::null;

	return desc;
}


KindOfFile FilesAssociation::kindOfFileFromName( const QString & kindOfFileStr )
{
	if ( kindOfFileStr == "Source" )
		return SOURCEfile;
	else
	if ( kindOfFileStr == "RenderableText" )
		return RENDERfile;
	else
	if ( kindOfFileStr == "Image" )
		return IMAGEfile;
	else
	if ( kindOfFileStr == "Sound" )
		return SOUNDfile;
	else
	if ( kindOfFileStr == "Video" )
		return VIDEOfile;
// 	else
// 	if ( kindOfFileStr == "Unknown" )
// 		return UNKNOWNfile;
	else
		return UNKNOWNfile;
}


FilesAssociation::Iterator FilesAssociation::find( const QString & fileName )
{
	if ( fileName.isEmpty() || ! mFilesAssociationLoaded )
		mFilesAssociationList.end();

	bool foundExt;
	QStringList extsList;
	int findDot = fileName.findRev( '.' );
	QString ext = (findDot > 0) ? fileName.right(fileName.length()-findDot-1).lower() : QString("");

	Iterator it = mFilesAssociationList.end();
	for (it = mFilesAssociationList.begin(); it != mFilesAssociationList.end(); ++it) {
		extsList = QStringList::split( "*.", (*it).templates );
		foundExt = FALSE;
		for ( uint i=0; i<extsList.count(); i++ ) {
			if ( ext+", " == extsList[i] || ext == extsList[i] ) {
				foundExt = TRUE;
				break;
			}
		}
		if ( foundExt )
			break;
	}

	return it;
}

