# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src/libs/qtcmdutils
# Cel to biblioteka qtcmdutils

INSTALLS += target 
target.path = $$PREFIX/lib 
TRANSLATIONS += ../../../translations/pl/qtcmd-utils.ts 
HEADERS += filters.h \
           filesassociation.h \
           keyshortcuts.h 
SOURCES += filters.cpp \
           filesassociation.cpp \
           keyshortcuts.cpp 
MOC_DIR = ../../../.moc
OBJECTS_DIR = ../../../.obj
TARGET = qtcmdutils
DESTDIR = ../../../.libs
CONFIG += warn_on \
qt \
thread \
dll
TEMPLATE = lib
PRECOMPILED_HEADER = ../../pch.h
