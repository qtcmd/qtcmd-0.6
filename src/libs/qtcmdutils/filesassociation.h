/***************************************************************************
                          filesassociation.h  -  description
                             -------------------
    begin                : wed may 12 2004
    copyright            : (C) 2004 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _FILESASSOCIATION_H_
#define _FILESASSOCIATION_H_

#include "../../enums.h"

#include <qvaluelist.h>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/**
 @short Klasa obs�uguje skojarzenia dla plik�w.
 W klasie tworzona jest lista skojarze� plik�w poprzez za�adowanie ustawie�
 z pliku konfiguracyjnego (qtcmd_mimerc). Je�li plik taki nie zostanie
 znaleziony na list� wstawione zostan� domy�lne warto�ci dla skojarze�. Mo�na
 pobra� nast�puj�ce informacje z obiektu tej klasy:
  @li czy skojarzenia zosta�y za�adowane,
  @li nazwa aplikacji dla podanego pliku,
  @li rodzaj pliku dla podanego, patrz: @see KindOfFile,
  @li wzorce dopasowywa� dla podanego typu pliku,
  @li rodzaj podgl�du (wewn�trzny/zewn�trzny), patrz: @see KindOfViewer,
*/
class FilesAssociation
{
public:
	/** Konstruktor klasy.\n
	 * Wywo�anie �adowania pliku skoja�e�.
	 */
	FilesAssociation();

	/** Destruktor.\n
	 Brak definicji (jest pusta).
	 */
	~FilesAssociation() {}

	/** �adowany jest tutaj plik skojarze� i na jego podstawie tworzona lista
	 skojarze�. W przypadku brak pliku, lista jest uzupe�niana domy�lnymi
	 warto�ciami.
	 */
	void readFilesAssociation();

	/** Zwraca informacj� o tym czy skojarzenia zosta�y za�adowane.\n
	 @return TRUE, je�li za�adowano skoja�enia, FALSE - nie zosta�o to zrobione.
	 */
	bool loaded() const { return mFilesAssociationLoaded; }


	/** Zwraca informacj� o nazwie przegl�darki dla podanego pliku.\n
	 @param fileName - nazwa pliku do sprawdzenia,
	 @return nazwa przegl�darki z absolutn� �cie�k�.
	 */
	QString appsForView( const QString & fileName );

	/** Zwraca informacj� o rodzaju pliku dla podanego.\n
	 @param fileName - nazwa pliku do sprawdzenia,
	 @return rodzaj pliku, patrz: @see KindOfFile.
	 */
	KindOfFile kindOfFile( const QString & fileName );

	/** Zwraca wzorce dopasowa� dla podanego typu pliku.\n
	 @param fileType - typ pliku (avi/gif/mp3),
	 @return wzorce dopasowa� (rozszerzenia plik�w, np. *.ogg).
	 */
	QString fileTemplates( const QString & fileType );

	/** Zwraca informacj� o rodzaju podgl�du (zewn�trzny/wewn�trzny) dla podanego.\n
	 @param fileName - nazwa pliku do sprawdzenia,
	 @return rodzaj podgl�du, patrz: @see KindOfViewer.
	 */
	KindOfViewer kindOfViewer( const QString & fileName );

	/** Zwraca opis typu dla podanego pliku.\n
	 @param fileName - nazwa pliku do sprawdzenia,
	 @return opis typu.
	 */
	QString fileDescription( const QString & fileName );

private:
	bool mFilesAssociationLoaded;


	/** Struktura opisuj�ca skoja�enie dla pliku.\n
	 @param fileType - typ pliku (gif/avi itp.);
	 @param appsForView - absolutna nazwa przegl�darki dla bie��cego typu;
	 @param templates - wzorce dopasowywa� (rozszerzenia plik�w, np.: *.avi);
	 @param kindOfViewer - rodzaj podgl�du, patrz: @see KindOfViewer;
	 @param kindOfFile - rodzaj pliku, patrz: @see KindOfFile.
	 */
	struct FileAssociation {
		QString fileType;
		QString appsForView;
		QString description;
		QString templates;
		KindOfViewer kindOfViewer;
		KindOfFile kindOfFile;
	};
	typedef QValueList <FileAssociation> FilesAssociationList;
	typedef FilesAssociationList::iterator Iterator;
	FilesAssociationList mFilesAssociationList;


	/** Zwraca identyfikator rodzaju pliku dla podanej nazwy typu pliku.\n
	 @param kindOfFileStr - nazwa rodzaju pliku, obs�ugiwane s� nast�puj�ce:
	 (Source, RenderableText, Image, Sound, Video),
	 @return rodzaj pliku, patrz: @see KindOfFile.\n
	 Metoda u�ywana podczas inicjowania listy w konstruktorze.
	 */
	KindOfFile kindOfFileFromName( const QString & kindOfFileStr );

	/** Zwraca pozycj� znalezionego dopasowania podanego pliku na li�cie
	 skojarze�.\n
	 @param fileName - nazwa pliku do dopasowania,
	 @return pozycja na li�cie, lub warto�� mFilesAssociationList.end(), je�li
	  nie odnaleziono dopasowania.\n
	 Metoda u�ywana jest do znajdowania przegl�darki dla podanego pliku, rodzaju
	 przegl�darki, rodzaju podanego pliku, w metodach za to odpowiedzialnych.
	 */
	Iterator find( const QString & fileName );

};

#endif
