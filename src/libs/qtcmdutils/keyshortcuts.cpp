/***************************************************************************
                          keyshortcuts.cpp  -  description
                             -------------------
    begin                : sun aug 1 2004
    copyright            : (C) 2004 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "keyshortcuts.h"

#include <qsettings.h>
#include <qkeysequence.h>


KeyShortcuts::KeyShortcuts()
{
	mKeyCodeList.clear();
}


void KeyShortcuts::updateEntryList( const QString & configFileKey )
{
	if ( ! mKeyCodeList.count() )
		return;

	// --- try to read shortcuts from config file
	QSettings *settings = new QSettings();
	QString newKeyStr, dfltKeyStr;
	KeyCodeList::iterator it;
	int k;

	for (it = mKeyCodeList.begin(); it != mKeyCodeList.end(); ++it) {
		dfltKeyStr = QString( QKeySequence( (*it)->defaultCode() ) );
		newKeyStr  = settings->readEntry( configFileKey+(*it)->description(), dfltKeyStr );

		k = (int)QKeySequence(newKeyStr); // below need to check combination too
		if ( k == Qt::SHIFT || k == Qt::ALT || k == Qt::CTRL ) // occured a wrong value
			newKeyStr = dfltKeyStr;

		(*it)->code = QKeySequence(newKeyStr);
	}

	delete settings;
}


int KeyShortcuts::key( int action )
{
	int keyCode = Qt::Key_unknown;

	KeyCodeList::iterator it;
	for (it = mKeyCodeList.begin(); it != mKeyCodeList.end(); ++it) {
		if ( (*it)->action() == action ) {
			keyCode = (*it)->code;
			break;
		}
	}
	if ( it == mKeyCodeList.end() )
		debug("Not found a passed action='%d' !", action );

	return keyCode;
}


QString KeyShortcuts::keyStr( uint index ) const
{
	int kCode = (*mKeyCodeList.at( index ))->code;
	return (kCode == Qt::Key_unknown) ? QString("none") : QString(QKeySequence( kCode ));
}


QString KeyShortcuts::keyDefaultStr( uint index ) const
{
	return QKeySequence((*mKeyCodeList.at( index ))->defaultCode());
}


void KeyShortcuts::setKey( int action, int shortcut )
{
	KeyCodeList::iterator it;
	for (it = mKeyCodeList.begin(); it != mKeyCodeList.end(); ++it) {
		if ( (*it)->action() == action ) {
			(*it)->code = shortcut;
			break;
		}
	}
	if ( it == mKeyCodeList.end() )
		debug("Not found given action='%d' !", action );
}


void KeyShortcuts::setKey( const QString & description, int shortcut )
{
	KeyCodeList::iterator it;
	for (it = mKeyCodeList.begin(); it != mKeyCodeList.end(); ++it) {
		if ( (*it)->description() == description ) {
			(*it)->code = shortcut;
			break;
		}
	}
	if ( it == mKeyCodeList.end() )
		debug("Not found given description='%s' !", description.latin1() );
}


int KeyShortcuts::ke2ks( QKeyEvent * ke )
{
	int key   = ke->key();
	int state = ke->state();
	QString keyTxt = ke->text();
	bool keysException = (key == Key_Space || key == Key_Backspace || key == Key_Delete || key == Key_Enter || key == Key_Return);
	int ksk = (keyTxt.isNull() || keyTxt.isEmpty() || keysException) ? (int)QKeySequence(key) : (int)QKeySequence(keyTxt);
	int keToKs = Qt::UNICODE_ACCEL + key + ((state & 0x0FFF)*0x2000);
	if ( ksk == key ) // QKeySequence returns no UNICODE_ACCEL type
		keToKs -= Qt::UNICODE_ACCEL;

	return keToKs;
}
