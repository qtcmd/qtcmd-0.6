/***************************************************************************
                          filters.cpp  -  description
                             -------------------
    begin                : sun aug 15 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "filters.h"

#include <qsettings.h>


Filters::Filters( FilesAssociation *fa )
{
	updateFilters();

	if ( fa )
		mFilesAssociation = fa;
// 	else
// 		mFilesAssociation = new FilesAssociation();
	updateByFilesAssociation( mFilesAssociation );
}


QString Filters::name( uint id ) const
{
	return (id > count()-1) ? QString::null : mFiltersList[id].section( '=', 0,0 );
}


QString Filters::patterns( uint id ) const
{
	return (id > count()-1) ? QString::null : mFiltersList[id].section( '=', -1 );
}


QString Filters::patterns( const QString & name ) const
{
	int filterId = -1;
	for (uint i=0; i<mFiltersList.count(); i++)
		if ( mFiltersList[i].section('=', 0,0) == name ) {
			filterId = i;
			break;
		}

	if ( filterId < 0 ) {
		debug("not found filter for name=%s", name.latin1() );
		return QString::null;
	}
	return mFiltersList[filterId].section( '=', -1 );
}


void Filters::add( const QString & name, const QString & patterns )
{
	int filterId = -1; // default not found
	for (uint i=0; i<mFiltersList.count(); i++)
		if ( mFiltersList[i].section('=', 0,0) == name ) {
			filterId = i;
			break;
		}

	if ( filterId > 0 ) {
		if ( filterId > (int)mBaseFiltersNum-1 ) { // only for user filters
			mFiltersList[filterId] = name+"="+patterns;
			debug("Filter about name '%s' has been replaced !", name.latin1() );
		}
		else
			debug("Cannot replace a standard filter !");
	}
	else
		mFiltersList.append( name+"="+patterns );
}


void Filters::remove( uint id )
{
	if ( id > mFiltersList.count()-1 ) {
		debug("An filter index is too big number !");
		return;
	}
	if ( id > mBaseFiltersNum-1 )
		mFiltersList.remove( mFiltersList.at(id) );
	else
		debug("Cannot remove a standard filter !");
}


void Filters::updateFilters()
{
	// --- add standard filters
	mFiltersList.clear();
	mFiltersList.append( tr( "files and directories" )+"=*" ); // first from 'enum FilesFilters'
	mFiltersList.append( tr( "directories only" )+"=*" );
	mFiltersList.append( tr( "files only" )+"=*" );
	mFiltersList.append( tr( "links" )+"=*" );
	mFiltersList.append( tr( "hanging links" )+"=*" );
	mBaseFiltersNum = mFiltersList.count();

	QSettings *settings = new QSettings();

	QStringList userFilters = settings->readListEntry( "/qtcmd/FilesPanel/UserFilters" );
	if ( userFilters.count() )
		mFiltersList += userFilters;

	mDefaultFilterId = settings->readNumEntry( "/qtcmd/FilesPanel/DefaultFilesFilter", 0 );
	if ( mDefaultFilterId > mFiltersList.count()-1 )
		mDefaultFilterId = 0;

	delete settings;
}


void Filters::updateByFilesAssociation( FilesAssociation *fa )
{
	if ( ! fa )
		return;

	// read all the files association and adds them to the list
	//mFiltersList.append( desc+"="+patterns )
}
