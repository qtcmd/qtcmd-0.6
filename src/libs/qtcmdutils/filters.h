/***************************************************************************
                          filters.h  -  description
                             -------------------
    begin                : sun aug 15 2004
    copyright            : (C) 2004 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _FILTERS_H_
#define _FILTERS_H_

#include <qdir.h> // for DirFilter
#include <qobject.h>
#include <qstringlist.h>

#include "filesassociation.h"


class Filters : public QObject
{
public:
	Filters( FilesAssociation *fa=0 );
	virtual ~Filters() {}

	uint baseFiltersNum() const { return mBaseFiltersNum; }

	uint defaultId() const { return mDefaultFilterId; }
	uint count() const { return mFiltersList.count(); }

	QString name( uint id ) const;
	QString patterns( uint id ) const;
	QString patterns( const QString & filterName ) const;

	void add( const QString & name, const QString & patterns );
	void remove( uint id );

	void updateByFilesAssociation( FilesAssociation *fa );
	void updateFilters();


	enum DirFilter {
		AllFiles     = QDir::All   | QDir::System | QDir::Hidden,
		DirsOnly     = QDir::Dirs  | QDir::Hidden,
		FilesOnly    = QDir::Files | QDir::Hidden,
		HiddenNot    = QDir::All   | QDir::System,
		SymLinks     = 1015,
		HangSymLinks = 1016
	};

private:
	QStringList mFiltersList;
	FilesAssociation *mFilesAssociation;

	uint mDefaultFilterId, mBaseFiltersNum;

};

#endif
