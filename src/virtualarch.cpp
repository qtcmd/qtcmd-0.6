/***************************************************************************
                          virtualarch.cpp  -  description
                             -------------------
    begin                : tue dec 23 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "virtualarch.h"


VirtualArch::VirtualArch( QWidget *parent, const char *name )
	: QProcess(parent, name)
{
	connect( this, SIGNAL(processExited()),   this, SLOT(slotProcessExited()) );
	connect( this, SIGNAL(readyReadStdout()), this, SLOT(slotReadFromStdout()) );
	connect( this, SIGNAL(readyReadStderr()), this, SLOT(slotReadFromStderr()) );
}


void VirtualArch::listFiles( QStringList & rawFilesList, uint levelInArchive )
{
	debug("VirtualArch::listFiles(), levelInArchive=%d", levelInArchive );
	emit commandStarted( List );
	emit stateChanged( state() );

	QDateTime dt;
	UrlInfoExt uie;
	long long size;
	uint slashesNum, filesNum = rawFilesList.count();
	bool isDir, isSymLink;
	QString name, perm;//, line;
	QStringList lineList;

	QString currentPath = mCurrentPath;
	currentPath.replace( ' ', '|' );
	if ( currentPath.length() > 1 )
		currentPath.remove(0,1);
	enum { PERM=0, SIZE, DATE, TIME, OWNER, GROUP, NAME };
		//line = rawFilesList[i];
	// Optymalizacja szybk. moznaby wyeliminowac zliczanie slash przez wczesniejsze dopisanie w nowej kolumnie
	// podobnie moznaby zrobic ze statusem pliku (f/d/l)

	for (uint i=0; i<filesNum; i++) {
		isDir = (rawFilesList[i].at(0) == 'd');
		slashesNum = rawFilesList[i].contains('/', FALSE);
		if ( isDir ) {
			if ( levelInArchive != slashesNum )
				continue;
		}
		else // files & links
			if ( levelInArchive-1 != slashesNum )
				continue;
		if ( rawFilesList[i].find(currentPath) == -1 && slashesNum )
			continue;

		lineList = QStringList::split(' ', rawFilesList[i]);

		name = lineList[NAME];
		if ( name.find('|') != -1 )
			name.replace('|', ' ');

		perm = lineList[PERM];
		perm.remove( 0, 1 );// remove the first bit (-/d/l) - (file/dir/link)
		uie.setPermissionsStr( perm );
		//bool readable = uie.isReadable();
		//bool executable = uie.isExecutable();
		//int permissions = uie.permissions();
		size = lineList[SIZE].toLongLong();
		dt = QDateTime::fromString(lineList[DATE]+" "+lineList[TIME], Qt::ISODate);
		isSymLink = (isDir) ? FALSE : (lineList[PERM].at(0) == 'l');

		emit listInfo(
		 UrlInfoExt( FileInfoExt::fileName(name),
		  isDir ? 0 : size, uie.permissions(), lineList[OWNER], lineList[GROUP], dt, dt,
		  isDir, ! isDir, isSymLink,
		  TRUE, isDir ? TRUE : (perm.find('x')!=-1), FALSE // FALSE - is not empty
		  //uie.isReadable(), uie.isExecutable(), FALSE // FALSE - is not empty
		 )
		);
	}

	emit commandFinished( List, FALSE );
	emit done( FALSE );
}


Operation VirtualArch::currentOperation() const
{
	ArchOperation archOperation = archiveOperation();

	if ( archOperation == OPEN )
		return Connect;
	else
	if ( archOperation == LIST )
		return List;
	else
	if ( archOperation == CD )
		return Cd;
	else
	if ( archOperation == GET || archOperation == PUT )
		return Copy;
	else
	if ( archOperation == REMOVE )
		return Remove;

	return NoneOp;
}

/*
	mUrlInfo.setName();
	mUrlInfo.setSize();
	mUrlInfo.setPermissionStr();
	mUrlInfo.setLastModified();
	mUrlInfo.setLastRead();
	mUrlInfo.setOwner();
	mUrlInfo.setGroup();
	mUrlInfo.setDir();
	mUrlInfo.setFile();
	mUrlInfo.setSymLink();
*/
