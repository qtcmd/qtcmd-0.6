/***************************************************************************
                          popupmenu.cpp  -  description
                             -------------------
    begin                : Tue Aug 21 2001
    copyright            : (C) 2001 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "popupmenu.h"


PopupMenu::PopupMenu( QWidget * parent, KindOfPopupMenu kindOfPopupMenu, const char * name )
 : QPopupMenu(parent,name)
{
	mMoveFile = FALSE;

	if ( kindOfPopupMenu == FilesPanelMENU )
		initFilesPanelMenu();
	else
	if ( kindOfPopupMenu == UserFilesPanelMENU )
		initUserMenu();

	connect( this, SIGNAL(highlighted(int)), this, SLOT(slotItemHighlighted( int ))  );
	connect( this, SIGNAL(signalOpen(QListViewItem *)), parent, SIGNAL(returnPressed(QListViewItem *)) );
}


PopupMenu::~PopupMenu()
{
// 	debug("PopupMenu::destructor=%s", name() );
}


void PopupMenu::initFilesPanelMenu()
{
/* moznaby przekazywac do tej klasy typ kliknietego pliku, albo jego nazwe
 (a typ sprawdzac tutaj), wtedy moznaby np.:
 jesli bylby to kat. to pokazac dodatkowo opcje "Weigh" i "Make archive",
 jesli archiwum, wtedy pokazac opcje "Extract",
 jesli link to "Edit Link", itp.
*/
	insertItem( tr("&Open"), CMD_Open );
	insertItem( tr("Open in panel &beside"), CMD_OpenInPanelBeside );
	insertItem( tr("Re&name"), CMD_Rename );
	insertItem( tr("&Delete"), CMD_Delete );
	insertItem( tr("&Copy"), CMD_Copy );
	insertItem( tr("&Make a link"), CMD_MakeLink );
//	insertItem( tr("Create &archive"), CMD_CreateNewArchive );
	insertSeparator();
	insertItem( tr("Select &All"), CMD_SelectAll );
	insertItem( tr("&UnSelect All"), CMD_UnSelectAll );
	insertItem( tr("Select with this &same ext"), CMD_SelectSameExt );
	insertItem( tr("UnSelect with this same ext"), CMD_UnSelectSameExt );
	insertSeparator();
//	insertItem( tr("&Weigh"), CMD_Weigh );
	insertItem( tr("&Properties"), CMD_Properties );
}


void PopupMenu::initUserMenu()
{
/* TODO
 Menu uzytkownika - bedzie przygotowywane na podstawie wartosci "CMD_xxxx", ktore zostana zapisane w tablicy.
 Konfiguracje takiego menu bedzie sie odbywac poprzez drzewo. Mozliwosc dodawania i usuwania opcji w menu.
 Wyglad konfiguracji.
 Widok podzielony na dwie cz., w jednej dostepne elementy, w drugiej drzewo z gotowym menu
 Nad widokiem checkBoxy z opcjami - "FilesPanelMenu", "ViewPanelMenu"
 Ponizej checkBox z opcjami - "Uzyj standardowego menu", "Uzyj wlasnego menu"
*/
}


bool PopupMenu::showMenu( const QPoint & globalPos )
{
	mMoveFile = FALSE;
	mHighlightedItemNum = -1;
	execCmd(  exec( globalPos )  );

	return TRUE;
}


void PopupMenu::keyPressed( QKeyEvent * e )
{
	mMoveFile = (e->key() == Key_Shift);
	int popUpCounter = mHighlightedItemNum;
	int maxItems = count() - 1;
	QChar ascii = e->ascii();
	int key = e->key();

	switch( key )
	{
		case Key_Escape:
			close();
			break;

		case Key_Down:
			if ( popUpCounter < maxItems )  {
				popUpCounter++;
				if ( idAt( popUpCounter ) < 0 )
					popUpCounter++; // skip separator
			}
			else
				popUpCounter = 0;
			break;

		case Key_Up:
			if ( popUpCounter < 0 ) {
				popUpCounter = maxItems;
				break;
			}
			if ( popUpCounter != 0 )  {
				popUpCounter--;
				if ( idAt( popUpCounter ) < 0 )
					popUpCounter --;  // skip separator
			}
			else
				popUpCounter = maxItems;
			break;

		case Key_Left:
		case Key_Right:
			break;

		case Key_Enter:
		case Key_Return:
			if ( isItemEnabled(idAt( popUpCounter )) ) {
				hide();
				execCmd( idAt( popUpCounter ) );
			}
			else
				close();
			break;

		default: { // has been pressed any key, check it
			if ( (uint)ascii != 0 )  {
				QChar asciiKey = ascii.lower();
				QString itemText;
				int idChar, separatorCount = 0, firstCMD = 101;  // CMD_Open;

				for(uint i=0; i<count(); i++)  {
					if ( idAt( i ) < 0 )  {  separatorCount++;  continue;  }  // skip separator
					if ( ! isItemEnabled(idAt( i )) )  continue;  // skip disable item

					itemText = text( idAt( i ) );
					idChar = itemText.find( "&" );
					if ( idChar < (int)itemText.length()-1 && idChar != -1 )
						if ( itemText.at( idChar+1 ).lower() == asciiKey )  {
							hide();
							execCmd( firstCMD + i - separatorCount );
							break;
						}
				} // for
			}
			break;
		} // default
	}

	if ( key == Key_Up || key == Key_Down )
		setActiveItem( popUpCounter );
}


void PopupMenu::execCmd( int cmd )
{
	switch( cmd )
	{
	/* TODO:
	 sygnaly, ktore nie maja parametru zamienic na jeden z param. KindOfAction, gdzie
	 KindOfAction byloby wartościa enum okreslajacej jedna z akcji, np. OPENkoa, DELETEkoa, itp.
	*/
		case CMD_Open:
			emit signalOpen( 0 );
			break;
		case CMD_OpenInPanelBeside:
			emit signalOpenInPanelBeside( FALSE ); // get current file for open
			break;
		case CMD_Rename:
			emit signalRename( 0 ); // column 0 (file name - ListViewExt::NAMEcol)
			break;
		case CMD_Delete:
			emit signalDelete();
			break;
		case CMD_MakeLink:
			emit signalMakeLink();
			break;
		case CMD_Copy:
			emit signalCopy( FALSE, mMoveFile );
			break;
// 		case CMD_CreateNewArchive:
// 			emit signalCreateNewArchive();
// 			break;
		case CMD_SelectAll:
			emit signalSelectAll( FALSE );
			break;
		case CMD_UnSelectAll:
			emit signalSelectAll( TRUE );
			break;
		case CMD_SelectSameExt:
			emit signalSelectSameExt( FALSE );
			break;
		case CMD_UnSelectSameExt:
			emit signalSelectSameExt( TRUE );
			break;
		case CMD_SelectGroup:
			emit signalSelectGroup( TRUE );
			break;
		case CMD_UnSelectGroup:
			emit signalSelectGroup( FALSE );
			break;
		case CMD_Properties:
			emit signalShowProperties();
			break;

		default:  break;
	}

	close();
}

// -------- SLOTs --------

void PopupMenu::slotItemHighlighted( int itemId )
{
	mHighlightedItemNum = indexOf( itemId );
}
