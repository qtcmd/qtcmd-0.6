/***************************************************************************
                          qtcmd.h  -  description
                             -------------------
    begin                : wed oct 23 2002
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef QTCMD_H
#define QTCMD_H

#include <qmainwindow.h>
#include <qtoolbutton.h>
#include <qpushbutton.h>
#include <qpopupmenu.h>
#include <qsplitter.h>
#include <qtoolbar.h>
#include <qlibrary.h>
#include <qaccel.h>

#include "enums.h"
#include "panel.h"
#include "filters.h"
#include "keyshortcuts.h"

#include "plugins/fp/settings/listviewsettings.h"
#include "plugins/fp/settings/otherappsettings.h"
#include "plugins/fp/settings/filesystemsettings.h"

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/**
 @short Klasa g��wna projektu - pokazuje okno g��wne aplikacji.
 Inicjowane s� tutaj wszystkie pokazywane w oknie g��wnym obiekty (pasek menu,
 narz�dziowy oraz panele). Znajduj� si� tutaj te� sloty pomocnicze przy
 komunikacji obydwu paneli.
 */
class QtCmd: public QMainWindow
{
	Q_OBJECT
public:
	/** Konstruktor okna g��wnego aplikacji.\n
	 @param leftURL - �cie�ka lub nazwa pliku dla lewego panelu,
	 @param rightURL - �cie�ka lub nazwa pliku dla prawego panelu.\n
	 Inicjuje g�owne ustawienia aplikacji (rozmiar, rodzj panel�w, informacje o
	 pokazaniu paska narz�dziowego), menu g��wne, pasek narz�dziwy oraz zarz�dce
	 widoku paneli.
	 */
	QtCmd( const QString & leftURL=QString::null, const QString & rightURL=QString::null );

	/** Destruktor klasy.\n
	 Usuwane s� tutaj obiekty paneli i inne u�ywane przez klase. Nast�puje tak�e
	 zapisanie ustawie� (szeroko�ci, wysoko�ci, statusu pokazywania paska menu
	 oraz narz�dziowego) do pliku konfiguracyjnego. Poza tym wy�adowywany jest
	 plugin ustawie� aplikacji.
	 */
	~QtCmd();

private:
	uint mWidth, mHeight;
	int  mWidthOfLeftPanel, mWidthOfRightPanel;

	QSplitter *mSplitter;

	QToolBar *mToolBar, *mButtonBar;
	QPopupMenu  *mViewMenu, *mKindOfVFPmenu, *mProportionsOfPanelsMenu, *mFavoritesMenu;
	QToolButton *mChangeKindOfViewToolBtn, *mFTPtoolBtn, *mRereadBtn, *mProportionsBtn, *mFreeSpaceBtn, *mDevicesBtn, *mFavoritesBtn, *mQuickGetOutBtn, *mSettingsBtn;

	Panel *mLeftPanel, *mRightPanel;
	FilesAssociation *mFilesAssociation;
	Filters *mFilters;

	QString   mPluginsPath;
	QLibrary *mSettingsLib;
	QWidget  *mSettingsWidget;
	KeyShortcuts *mKeyShortcuts;
	QAction *mQuitAppsA, *mMenuBarA, *mToolBarA, *mBtnBarA, *mConfigureA, *mFtpMmngA, *mFreeSpaceA, *mStorageA, *mRereadA, *mFavoritesA, *mFindFilesA, *mQuickGetOutA;

	enum CurrentPanel { LEFTpanel=0, RIGHTpanel, NOpanel };

	enum PanelProportion { PP_5050=21, PP_3070, PP_7030, PP_USER };
	bool mMenuBarVisible, mToolBarVisible;
	bool mButtonBarVisible, mFlatButtonBar;
	bool mShowQuitWarn;

	enum ButtonBarKeys {
	 HelpBTN=0, PanelViewBTN, FileViewBTN, FileEditBTN, CopyBTN, MoveBTN, MkDirBTN, DeleteBTN, QuitBTN
	};
	QPushButton *mButtonsTab[10];


	/** Inicjowanie okna g��wnego aplikacji.\n
	 Odczytuje z pliku konfiguracyjnego rozmiar okna g��wnego oraz przypisuje te
	 paremetry aplikacji. Je�li wymagane parametry nie zosta�y znalezione w pliku
	 konfiguracyjnym, zapisywane s� w nim domy�lne, a aplikacja nimi inicjowana.
	 Odczytywane s� r�wnie� szeroko�ci obu paneli.
	 */
	void initMainSettings();

	/** Funkcja inicjuje menu g��wne aplikacji oraz pasek narz�dziowy.
	 */
	void initMenuAndToolBar();

	/** Inicjowane s� tutaj pod-menu dla opcji: "Rodzaj widoku dla bie��cego
	 panela" oraz dla "Proporcje paneli".
	 */
	void initSubMenus();

	/** Metoda tworzy i inicjuje pasek zawieraj�cy guziki powi�zane z
	 podstawowymi akcjami na plikach.\n
	 Pasek jest umieszczany domy�lnie u do�u okna.
	 */
	void initButtonsBar();

	/** Metoda powoduje utworzenie obiektu skr�t�w klawiszowych oraz
	 zainicjowanie jego listy domy�lnymi warto�ciami.
	 */
	void initKeyShortcuts();

	/** Inicjuje dwa panele.\n
	 Tworzone s� tutaj obiekty obydwu paneli oraz obiekt splittera, kt�ry jest
	 rodzicem paneli.\n
	 @param leftURL - �cie�ka lub nazwa pliku dla lewego panelu,
	 @param rightURL - �cie�ka lub nazwa pliku dla prawego panelu.
	 */
	void createPanels( const QString & leftURL, const QString & rightURL );

	/** Funkcja zwraca numer bie��cego panelu.\n
	 @return numer bie��cego panelu (dla lewego jest to 'LEFTpanel', a prawy
	 jest r�wny 'RIGHTpanel').\n
	 U�ywana jest przez sloty u�ywane do komunikacji pomi�dzy panelami.
	 */
	uint currentPanel();

protected:
	/** Obs�uga zdarzenia zmiany rozmiaru dla g��wnego okna aplikacji.
	 */
	void resizeEvent( QResizeEvent * );

private slots:
	/** Pokazuje okienko informacyjne z nazw�, przeznaczeniem programu, autorach,
	 bie��cej wersji oraz prawach autorskich.
	 */
	void about();

	/** Pokazuje informacj� o wersji biblioteki Qt zlinkowanej z programem.
	 */
	void aboutQt();

	/** Slot wykonuje zamkni�cie aplikacji.
	 */
	void slotQuit();

	/** Slot powoduje pokazanie okna pomocy.
	 */
	void slotShowHelp();

	/** Slot uruchamia zmian� widoku.\n
	 U�ywany przez pasek guzik�w.
	 */
	void slotChangeView();

	/** Slot uruchamia podgl�d bie��cego pliku.\n
	 U�ywany przez pasek guzik�w.
	 */
	void slotFileView();

	/** Slot uruchamia edycj� bie��cego pliku.\n
	 U�ywany przez pasek guzik�w.
	 */
	void slotFileEdit();

	/** Slot uruchamia kopiowanie plik�w.\n
	 U�ywany przez pasek guzik�w.
	 */
	void slotCopy();

	/** Slot uruchamia przenoszenie plik�w.\n
	 U�ywany przez pasek guzik�w.
	 */
	void slotMove();

	/** Slot uruchamia tworzenie folderu w bie��cym katalogu.\n
	 U�ywany przez pasek guzik�w.
	 */
	void slotMakeDir();

	/** Slot uruchamia usuwanie plik�w.\n
	 U�ywany przez pasek guzik�w.
	 */
	void slotDelete();


	/** Slot uruchamia utworzenie linku(�) do wybranych plik�w.
	 U�ywany przez pasek guzik�w.
	 */
	void slotMakeLink();


	/** Slot wywo�ywany w momencia podj�cia pr�by zmiany aktualnego widoku panela
	 na list�.\n
	 Wysy�any jest tutaj tylko sygna� uruchamiaj�cy powy�sz� zmian�.
	 */
	void slotChangeToListView();

	/** Slot wywo�ywany w momencia podj�cia pr�by zmiany aktualnego widoku panela
	 na drzewo.\n
	 Wysy�any jest tutaj tylko sygna� uruchamiaj�cy powy�sz� zmian�.
	 */
	void slotChangeToTreeView();

	/** Slot powoduje ukrycie lub pokazanie paska menu.\n
	 Stan pokazywania zale�y od aktualnej warto�ci sk�adowej @em mMenuBarVisible ,
	 kt�ra jest te� tutaj modyfikowana, tak �e nast�pne wywo�anie tego slota
	 b�dzie mia�o skutek przeciwny do poprzedniego.
	 */
	void slotShowHideMenuBar();

	/** Slot powoduje ukrycie lub pokazanie paska narz�dziowego.\n
	 Stan pokazywania zale�y od aktualnej warto�ci sk�adowej @em mToolBarVisible ,
	 kt�ra jest te� tutaj modyfikowana, tak �e nast�pne wywo�anie tego slota
	 b�dzie mia�o skutek przeciwny do poprzedniego.
	 */
	void slotShowHideToolBar();

	/** Slot powoduje ukrycie lub pokazanie dolnego paska z guzikami.\n
	 Stan pokazywania zale�y od aktualnej warto�ci sk�adowej @em mButtonBarVisible ,
	 kt�ra jest te� tutaj modyfikowana, tak �e nast�pne wywo�anie tego slota
	 b�dzie mia�o skutek przeciwny do poprzedniego.
	 */
	void slotShowHideButtonBar();

	/** Slot powoduje zmian� rozmiar�w paneli na t� o podanym Id.\n
	 @param proportionId - id dla nowego rozmiaru. Dost�pne s� nast�puj�ce
	 warto�ci parametru :
	  @li PP_5050 - 50/50 (procentowo),
	  @li PP_3070 - 30/70 (procentowo),
	  @li PP_7030 - 70/30 (procentowo),
	  @li PP_USER - Zdefiniowane przez u�ytkownika, (ustawione przy starcie
	  aplikacji).\n
	  Slot u�ywany przez menu guzika s�u��cego do zmiany rozmiar�w paneli.
	 */
	void slotChangeProportionOfPanels( int proportionId=PP_5050 );


	/** Slot wykonuje pr�b� za�adowania (je�li jest za�adowany to tylko pokazuje)
	 pluginu dialogu ustawie� aplikacji, po czym go pokazuje.\n Je�eli nie uda�o
	 si� znale�� pliku plugina, wtedy pokazywany jest stosowny komunikat.
	 */
	void slotShowSettings();

	/** Slot pokazuje dialog s�u��cy do konfigucji paska narz�dziowego (dodawania,
	 przestawiania lub usuwania ikon na nim).\n
	 UWAGA: Metoda nie ma jeszcze definicji !
	 */
	void slotShowConfigureToolBarDlg();

public slots:
	/** Slot powoduje otwarcie podanego pliku lub katalogu w panelu obok.\n
	 @param fileName - nazwa pliku lub katalogu do otwarcia.\n
	 Przy czym to czy b�dzie otwarty plik czy katalog zale�y od wyst�pienia znaku
	 '/' na ko�cu podanej nazwy. Je�li b�dzie tam znak '/', wtedy nast�pi pr�ba
	 otwarcia katalogu w przeciwnym razie pliku.
	 */
	void slotOpenIntoPanelBeside( const QString & fileName );

	/** Slot wywo�ywany w momencie zmiany rozmiaru kolumn.\n
	 @param section - kolumna, kt�ra ma zmieniany rozmiar,
	 @param oldSize - poprzedni (stary) rozmiar kolumny podanej w @em section ,
	 @param newSize - nowy rozmiar kolumny podanej w @em section .\n
	 U�ywany jest do synchronizowania zmian szeroko�ci kolumn w obu panelach.
	 */
	void slotHeaderSizeChange( int section, int oldSize, int newSize );

	/** Slot powoduje ustawienie podanej �cie�ki bie��cego panela w drugim.\n
	 @param url - �cie�ka do ustawienia.
	 */
	void slotSetPanelBesideURL( const QString & url );

	/** Slot u�ywany do uaktualniania na li�cie w drugim panelu informacji o pliku.\n
	 @param fileName - nazwa pliku do uaktualniania.\n
	 Jest wykorzystywany przy edycji dla podgladu pliku osadzonego w oknie g�ownym
	 aplikacji. \n \n UWAGA: brak definicji !
	 */
	void slotUpdateItemOnLV( const QString & fileName );

	/** Slot zamyka widok w bie��cym panelu i tworzy tam nowy typu FILESpanel.\n
	 Jest on u�ywany do usuwania widoku podgl�du pliku w bie��cym panelu.
	 */
	void slotCloseView();

	/** Slot pobiera w podanym parametrze rodzaj systemu plik�w jaki si� znajduje
	 w drugim panelu.\n
	 @param kindOfFS - zmienna w jakiej zostanie zapisany rodzaj systemu plik�w.
	 */
	void slotGetKindOfFSInPanelBeside( KindOfFilesSystem & kindOfFS );

	/** Slot powoduje wywo�anie metody uruchamiaj�cej kopiowanie w panelu obok.\n
	 @param filesList - lista plik�w do skopiowania lub przeniesienia,
	 @param move - je�li zawiera TRUE, wtedy bie��c� operacj� bedzie przenoszenie,
	 w przeciwnym razie jest to kopiowanie.
	 */
	void slotCopyFiles( const QStringList & filesList, bool move );

	/** Slot wywo�uje funkcje powoduj�c� uaktualnienie listy w panelu obok.\n
	 @param updateListOp - rodzaj operacji uaktualniania. patrz:
	  @see ListViewExt::UpdateListOperation,
	 @param itemsList - wska�nik do wska�nika na list� z elementami do
	 uaktualnienia,
	 @param newUrlInfo - informacje o pliku (u�ywane je�li jest jeden pliku
	 uaktualniany).
	 */
	void slotUpdateSecondPanel( int updateListOp, QValueList<QListViewItem *> *itemsList, const UrlInfoExt & newUrlInfo );

	/** Slot ustawia w podanym parametrze adres wska�nika do listy zaznaczonych
	 element�w w drugim panelu.\n
	 @param selectedItemsList - adres wska�nika do listy zaznaczonych
	 */
	void slotGetSILfromPanelBeside( QValueList<QListViewItem *> *& selectedItemsList );

	/** Ustawia skr�ty klawiszowe dla akcji w bie��cym obiektcie (oknie g��wnym).
	 */
	void slotApplyKeyShortcuts( KeyShortcuts *keyShortcuts );

	/** Slot ustawia filtry dla listy plik�w w obydwu panelach.
	 */
	void slotApplyFilters( Filters * filters );

signals:
	/** Sygna� uruchamia zmian� bie��cego widoku w bie��cym panelu na podany.\n
	 @param kindOfView - rodzaj nowego widoku. Dla widoku listy dozwolne s�:
	 LISTpview oraz TREEpview.\n
	 Sygna� jest u�ywany przez sloty @em slotChangeToListView oraz
	 @em slotChangeToTreeView .
	 */
	void signalSetNewKindOfView( uint kindOfView );

	/** Sygna� uruchamia tzw. szybkie wyj�cie z bie��cego systemu plik�w.\n
	 Przy czym "wyskoczy�" mo�na tylko z nie lokalnego systemu plik�w.
	 */
	void signalQuickGetOutFromFS();

	/** Sygna� powoduje pokazanie dialogu zarz�dzania po��czeniami FTP.
	 */
	void signalShowFtpManagerDlg();

	/** Sygna� uruchamia ponowne odczytanie bie��cego katalogu w aktywnym panelu.
	 */
	void signalRereadCurrentDir();

	/** Sygna� powoduje pokazanie w lewym g�rnym rogu bie��cego panelu - menu
	 z 'ulubionymi katalogami'.
	 */
	void signalShowFavorites();

	/** Sygna� powoduje pokazanie w lewym g�rnym rogu bie��cego panelu - menu
	 z prost� statystyk� opisuj�c� ca�kowite i dost�pne miejsce w bie��cym
	 katalogu.
	 */
	void signalShowFreeSpace();

	/** Sygna� powoduje pokazanie w lewym g�rnym rogu bie��cego panelu - menu
	 z list� dost�pnych urz�dze� masowych (informacja odczywytana jest z pliku
	 '/etc/fstab').
	 */
	void signalDevicesList();

	/** Sygna� powoduje pokazanie dialog s�u��cego do wyszukiwania plik�w.
	 */
	void signalFindFile();


	/** Sygna� uruchamia zastosowanie ustawie� dla widoku listy.
	 @param lvs - struktura zawieraj�ca ustawienia
	 */
	void signalApplyListViewSettings( const ListViewSettings & lvs );

	/** Sygna� uruchamia zastosowanie ustawie� dla pozosta�ych ustawie�.
	 @param oas - struktura zawieraj�ca ustawienia
	 */
	void signalApplyOtherAppSettings( const OtherAppSettings & oas );

	/** Sygna� uruchamia zastosowanie ustawie� dla systemu plik�w.
	 @param fss - struktura zawieraj�ca ustawienia
	 */
	void signalApplyFileSystemSettings( const FileSystemSettings & fss );

	/** Sygna� u�ywany do inicjowania skr�t�w klawiszowych w dialogu
	 konfiguracyjnym.\n
	 @param keyShortcuts - wska�nik na obiekt skr�t�w klawiszowych.
	 */
	void signalSetKeyShortcuts( KeyShortcuts * keyShortcuts );

	/** Sygna� u�ywany do inicjowania skoja�e� dla plik�w w dialogu
	 konfiguracyjnym.\n
	 @param fa - wska�nik na obiekt skoja�e� dla plik�w.
	 */
	void signalSetFilesAssociation( FilesAssociation *fa );

};

#endif
