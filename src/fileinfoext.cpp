/***************************************************************************
                          fileinfoext.cpp  -  description
                             -------------------
    begin                : tue dec 9 2003
    copyright            : (C) 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 $Id$
 ***************************************************************************/

#include "fileinfoext.h"
#include "extentions.cpp" // tables that contains file's extentions

#include <errno.h>


KindOfFile FileInfoExt::kindOfFile( const QString & fileName )
{
	if ( fileName.at(fileName.length()-1) == '/' || fileName.isEmpty() )
		return UNKNOWNfile;

	bool foundExt = FALSE;
	KindOfFile kindOfFile = UNKNOWNfile;
	QString fExt = QFileInfo(fileName).extension(FALSE).lower(), ext;

	uint ti=0, i;
	const char **extentionTab;
	const char **extentionTabPtrs[] = {
		sourceFileExt, renderFileExt, imageFileExt, soundFileExt, archiveFileExt, videoFileExt, bindocFileExt,
		0
	};

	KindOfFile kindOfFileTab[] = {
		SOURCEfile, RENDERfile, IMAGEfile, SOUNDfile, ARCHIVEfile, VIDEOfile,
		UNKNOWNfile
	};


	while( (extentionTab=extentionTabPtrs[ti]) ) {
		i = 0;
		while( (ext=extentionTab[i]) ) {
			if ( ext == fExt ) {
				kindOfFile = kindOfFileTab[ti];
				foundExt = TRUE;
				break;
			}
			i++;
		}
		if ( foundExt )
			break;
		ti++;
	}

	return kindOfFile;
}


bool FileInfoExt::isArchive( const QString & fileName )
{
	return (kindOfFile(archiveFullName(fileName)) == ARCHIVEfile);
}


QString FileInfoExt::archiveFullName( const QString & fileName )
{
	bool archive = FALSE;
	QString fName = fileName;
	uint fNameLength = fName.length()-1;

	if ( fName.at(fNameLength) == '/' ) // it's directory
		fName.remove( fNameLength, '/' );

	while ( ! fName.isEmpty() ) {
		if ( FileInfoExt::kindOfFile(fName) == ARCHIVEfile ) {
			archive = TRUE;
			break;
		}
		fName = fName.left( fName.findRev('/', fName.length()-2) );
		if ( fName.contains('/') < 2 )
			break;
	}

	return fName;
}


KindOfArchive FileInfoExt::kindOfArchive( const QString & fileName )
{
// "rpm", "cpio", "deb", "ace", "tzo", "zoo"
	QString fName     = archiveFullName( fileName );
	QString firstExt  = QFileInfo(fName).extension(FALSE).lower(); // first from right side
	QString secondExt = QFileInfo(fName).extension().lower();
	uint dotNum = secondExt.contains('.')-1;
	secondExt = secondExt.section( '.', dotNum, dotNum );

	if ( firstExt == "lzma" ) {
		if ( secondExt != "cpio" ) {
			if ( secondExt == "tar" )
				return TLZMAarch;
			else
				return LZMAcompr;
		}
	}
	else
	if ( firstExt == "bz2" ) {
		if ( secondExt != "cpio" ) {
			if ( secondExt == "tar" )
				return TBZIP2arch;
			else
				return BZIP2compr;
		}
	}
	else
	if ( firstExt == "gz" ) {
		if ( secondExt != "cpio" ) {
			if ( secondExt == "tar" )
				return TGZIParch;
			else
				return GZIPcompr;
		}
	}
	else
	if ( firstExt == "z" ) {
		if ( secondExt != "cpio" ) {
			if ( secondExt == "tar" )
				return TZarch;
			else
				return COMPRESScompr;
		}
	}
	else
	if ( firstExt == "gz" ) {
		if ( secondExt == "cpio" )
			return CGZIParch;
		else
			return GZIPcompr;
	}
	else
	if ( firstExt == "z" ) {
		if ( secondExt == "cpio" )
			return TZarch;
		else
			return COMPRESScompr;
	}
	else
	if ( firstExt == "tgz" )
		return TGZIParch;
	else
	if ( firstExt == "tar" )
		return TARarch;
	else
	if ( firstExt == "cpio" )
		return CPIOarch;
	else
	if ( firstExt == "rar" || firstExt == "sfx" )
		return RARarch;
	else
	if ( firstExt == "zip" || firstExt == "jar" )
		return ZIParch;
	else
	if ( firstExt == "arj" )
		return ARJarch;
	else
	if ( firstExt == "lzh" )
		return LZHarch;
	else
	if ( firstExt == "zoo" )
		return ZOOarch;

	return UNKNOWNarch;
}


QString FileInfoExt::filePath( const QString & name )
{
	bool absolutePath = (
	 (name.at(0) == '/')        || (name.find("ftp://") == 0) ||
	 (name.find("smb://") == 0) || (name.find("arc://") == 0)
	);
	if ( ! absolutePath )
		return name;

	if ( name.find("//") != -1 )
		if ( name.contains('/') < 4 )
			return name;

	QString path;

	if ( name.at(name.length()-1) == '/' ) // directory
		path = name.left( name.findRev('/', name.length()-2)+1 );
	else
		path = name.left( name.findRev('/')+1 );

	return path;
}


QString FileInfoExt::fileName( const QString & name )
{
	QString fName;

	if ( name.at(name.length()-1) == '/' ) { // directory
		int numOfSlashes = name.contains('/')-1;
		fName = name.section( '/', numOfSlashes, numOfSlashes );
	}
	else
		fName = name.right( name.length() - name.findRev('/') - 1 );

	return fName;
}


QString FileInfoExt::hostName( const QString & url, bool addSlash )
{
	QString host;

	if ( url.at(0) == '/' )
		host = "/";
	else
	if ( url.find("ftp://") == 0 || url.find("smb://") == 0 || url.find("arc://") == 0 )
		host = url.left( url.find('/', url.find("//")+1) );

	if ( addSlash && host != "/" )
		host += "/";

	return host;
}


bool FileInfoExt::specialFile( const QString & fileName, SpecialFileType sft )
{
	return doStat( fileName, (int)sft );
}


bool FileInfoExt::specialPerm( const QString & fileName, SpecialPerm sp )
{
	return doStat( fileName, (int)sp );
}


bool FileInfoExt::doStat( const QString & fileName, int statInfo )
{
	if ( fileName.at(0) != '/' ) {
		debug("Illegal file system of file '%s' !\nAvailable only for local FS.", fileName.latin1() );
		return FALSE;
	}

	QString fName = fileName;

	if ( statInfo == S_IFSOCK || statInfo == S_IFBLK || statInfo == S_IFCHR || statInfo == S_IFIFO ) {
		QFileInfo file( fName );
		if ( file.isSymLink() ) {
			fName = file.readLink();
			//debug("file '%s', link= %s, path= %s", fileName.latin1(), fName.latin1(), FileInfoExt::filePath(fileName).latin1() );
			// fix for not absolute path in link target (readLink not resolves path found in the file path)
			if (fName.at(0) != '/') // is relative path?
				fName = QDir().cleanDirPath(FileInfoExt::filePath(fileName) + file.readLink());
		}
	}

	struct stat statBuffer;
	if ( ::lstat(QFile::encodeName( fName ), & statBuffer) != 0 )  {
		//debug("FileInfoExt::doStat, lstat error=%d, file=%s", errno, fName.latin1() );
		return FALSE;
	}

	bool permission = statBuffer.st_mode & statInfo;

	if ( statInfo == S_IFCHR )  permission = S_ISCHR( statBuffer.st_mode );
	else
	if ( statInfo == S_IFBLK )  permission = S_ISBLK( statBuffer.st_mode );
	else
	if ( statInfo == S_IFIFO )  permission = S_ISFIFO( statBuffer.st_mode );
	else
	if ( statInfo == S_IFSOCK)  permission = S_ISSOCK( statBuffer.st_mode );

	return permission;
}


int FileInfoExt::permission( const QString & fileName )
{
	if ( fileName.at(0) != '/' || fileName.isEmpty() ) {
		debug("FileInfoExt::permission(). Illegal file system '%s' !\nAvailable only for local FS.", fileName.latin1() );
		return -1;
	}

	int perm = 0;
	QFileInfo fileInfo( fileName );

	if ( fileInfo.isSymLink() )
		fileInfo.setFile( fileInfo.readLink() );
	else
	if ( ! fileInfo.exists() )
		return -1;

	if ( fileInfo.permission( QFileInfo::ReadUser   ) )   perm += QFileInfo::ReadUser;
	if ( fileInfo.permission( QFileInfo::WriteUser  ) )   perm += QFileInfo::WriteUser;
	if ( fileInfo.permission( QFileInfo::ExeUser    ) )   perm += QFileInfo::ExeUser;
	if ( fileInfo.permission( QFileInfo::ReadGroup  ) )   perm += QFileInfo::ReadGroup;
	if ( fileInfo.permission( QFileInfo::WriteGroup ) )   perm += QFileInfo::WriteGroup;
	if ( fileInfo.permission( QFileInfo::ExeGroup   ) )   perm += QFileInfo::ExeGroup;
	if ( fileInfo.permission( QFileInfo::ReadOther  ) )   perm += QFileInfo::ReadOther;
	if ( fileInfo.permission( QFileInfo::WriteOther ) )   perm += QFileInfo::WriteOther;
	if ( fileInfo.permission( QFileInfo::ExeOther   ) )   perm += QFileInfo::ExeOther;

	if ( specialPerm( fileName, UID ) )   perm += S_ISUID;
	if ( specialPerm( fileName, GID ) )   perm += S_ISGID;
	if ( specialPerm( fileName, VTX ) )   perm += S_ISVTX;

	return perm;
}


long long FileInfoExt::size( const QString & fileName )
{
	struct stat statBuffer;

	if ( ::lstat(QFile::encodeName( fileName ), & statBuffer) != 0 )  {
		debug("lstat error=%d, file=%s", errno, fileName.latin1() );
		return -1;
	}

	return statBuffer.st_size;
}
