/***************************************************************************
                          helpdialog.xpp  -  description
                             -------------------
    begin                : sun jan 23 2005
    copyright            : (C) 2005 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include <qapplication.h>
#include <qtextbrowser.h>
#include <qpushbutton.h>
#include <qdragobject.h>
#include <qtextcodec.h>
#include <qfileinfo.h>

#include "../icons/previous.xpm"
#include "../icons/next.xpm"
#include "../icons/home.xpm"

#include "helpdialog.h"
#include "version.h"

HelpDialog::HelpDialog( QWidget *parent, const char *name, bool modal, WFlags f )
	: QDialog(parent, name, modal, f),
	m_pBtn_left(NULL), m_pBtn_right(NULL), m_pBtn_home(NULL),
	m_pTB_view(NULL)
{
	initGUI();
	loadFile();
}


HelpDialog::~HelpDialog()
{
	delete m_pBtn_right;
	delete m_pBtn_left;
	delete m_pBtn_home;
	delete m_pTB_view;
}

void HelpDialog::initGUI()
{
	m_pTB_view = new QTextBrowser(this);

	m_pBtn_right = new QPushButton(QPixmap((const char**)previous), "", this);
//	, tr("&Previous")
	m_pBtn_left = new QPushButton(QPixmap((const char**)next), "", this);
//	, tr("&Next")
	m_pBtn_home = new QPushButton(QPixmap((const char**)home), "", this);
//	, tr("&Home")

	m_pBtn_right->setGeometry( 10,10, 32,32 );
	m_pBtn_left->setGeometry( 10+32+10,10, 32,32 );
	m_pBtn_home->setGeometry( 10+32+32+20,10, 32,32 );

	connect(m_pBtn_right, SIGNAL(clicked()), m_pTB_view, SLOT(forward()) );
	connect(m_pBtn_left,  SIGNAL(clicked()), m_pTB_view, SLOT(backward()) );
	connect(m_pBtn_home,  SIGNAL(clicked()), m_pTB_view, SLOT(home()) );
}

bool HelpDialog::loadFile( )
{
/*
	  For languages from Western Europe (English, French, German) -  8859-1.txt ( locale = en, fr, de ? )
	  For languages from Eastern Europe (Polish, Czech, Slovak, Croatian)  - 8859-2.txt. (locale = pl, cz. sl, cr ?)
	  For Esperanto that will be mapping file 8859-3.txt. (locale = es ?)
	  For Russian that will be mapping file 8859-5.txt or koi8-r.txt. ( locale = ru ? )
	  For Chines, Japanise ?
*/
	QString locale = QTextCodec::locale();
	locale = locale.section('_', 0,0); // get only the first two characters
	QString charset = "charset=";

	if ( locale.find("pl") != -1 )  charset +="iso8859-2";
	else
	if ( locale.find("ru") != -1 )  charset +="koi8-r"; // or 8859-5
	else
	if ( locale.find("he") != -1 )  charset +="iso8859-8"; // for Hebrew (?)
	else
		charset += "iso8859-1";

	QString appPath  = qApp->applicationDirPath();
	QString helpPath = appPath.left( appPath.findRev('/')+1 ); // parent dir.for binary file location dir.

// 	if (QFileInfo(helpPath+".libs").exists()) // apps. executed from project
// 		helpPath += "doc/"+locale;
// 	else // apps. is installed
	if (! QFileInfo(helpPath+".libs").exists()) // apps. executed from project
		helpPath += QString("/share/doc/qtcmd-"VERSION"/");
	helpPath += locale+"/";
//	debug("helpPath=%s", helpPath.latin1() );

	if (! m_pTB_view)
		return FALSE;

	m_pTB_view->mimeSourceFactory()->setExtensionType( "htm", "text/html;"+charset ); // only for sets coding page
	m_pTB_view->mimeSourceFactory()->setExtensionType( "html", "text/html;"+charset );
	m_pTB_view->mimeSourceFactory()->setFilePath( helpPath );

	QString fullFileName = helpPath+"index.html";
	QString txtBuf;
	const QMimeSource *m = m_pTB_view->mimeSourceFactory()->data(fullFileName, m_pTB_view->context());

	if (! m) {
		debug("QtCmd: no mimesource for %s", fullFileName.latin1() );
		return FALSE;
	}
	else
	if (! QTextDrag::decode(m, txtBuf)) {
		debug("QtCmd: cannot decode %s", fullFileName.latin1() );
		return FALSE;
	}

	m_pTB_view->setText(txtBuf);

	QApplication::restoreOverrideCursor();  // restore original cursor

	return TRUE;
}

void HelpDialog::resizeEvent( QResizeEvent * )
{
	m_pTB_view->setGeometry( 10, 32+15, width()-20, height()-20-32-10 );
}
