# Plik utworzony przez menad?era projekt�w kdevelopa
# ------------------------------------------- 
# Podkatalog wzgl?dem g?�wnego katalogu projektu: ./src
# Cel to program:  qtcmd

INSTALLS += target 
target.path = $$PREFIX/bin 
FORMS += ui/fileoverwritedialog.ui \
         ui/findfiledialog.ui \
         ui/ftpconnectdialog.ui \
         ui/preoperationdialog.ui \
         ui/progressdialog.ui \
         ui/propertiesdialog.ui \
         ui/urlslistmanager.ui 
TRANSLATIONS += ../translations/pl/qtcmd.ts 
HEADERS += ui/fileoverwritedialog.ui.h \
           ui/findfiledialog.ui.h \
           ui/ftpconnectdialog.ui.h \
           ui/preoperationdialog.ui.h \
           ui/progressdialog.ui.h \
           ui/propertiesdialog.ui.h \
           ui/urlslistmanager.ui.h \
           combopath.h \
           enums.h \
           filespanel.h \
           functions.h \
           listviewext.h \
           localfs.h \
           panel.h \
           popupmenu.h \
           qtcmd.h \
           solf.h \
           urlinfoext.h \
           virtualfs.h \
           findcriterion.h \
           listviewfinditem.h \
           listviewitemext.h \
           ftpfs.h \
           fileinfoext.h \
           devicesmenu.h \
           archivefs.h \
           virtualarch.h \
           tar.h \
           pathview.h \
           fileview.h \
           functions_col.h \
           helpdialog.h \
           version.h 
SOURCES += combopath.cpp \
           devicesmenu.cpp \
           filespanel.cpp \
           listviewext.cpp \
           listviewext_mouse.cpp \
           listviewext_sel.cpp \
           listviewitemext.cpp \
           localfs.cpp \
           main.cpp \
           panel.cpp \
           popupmenu.cpp \
           qtcmd.cpp \
           solf.cpp \
           urlinfoext.cpp \
           virtualfs.cpp \
           listviewfinditem.cpp \
           ftpfs.cpp \
           fileinfoext.cpp \
           archivefs.cpp \
           virtualarch.cpp \
           tar.cpp \
           pathview.cpp \
           fileview.cpp \
           extentions.cpp \
           helpdialog.cpp 
DEPENDPATH = icons
TARGETDEPS += ../.libs/libqtcmduiext.so \
../.libs/libqtcmdutils.so
LIBS += -lqtcmduiext \
-lqtcmdutils
INCLUDEPATH += ../src \
../src/libs/qtcmdutils \
../src/libs/qtcmduiext \
icons \
ui
MOC_DIR = ../.moc
UI_DIR = ui
OBJECTS_DIR = ../.obj
QMAKE_LIBDIR = ../.libs \
../.libs
TARGET = qtcmd
DESTDIR = ../bin
CONFIG += warn_on \
qt \
thread
QMAKE_CLEAN += ../qtcmd.kdevses ../qtcmd.kdevelop.pcs
TEMPLATE = app
PRECOMPILED_HEADER = pch.h
