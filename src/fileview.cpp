/***************************************************************************
                          fileview.cpp  -  description
                             -------------------
    begin                : Sun Oct 27 2002
    copyright            : (C) 2002 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project

 ***************************************************************************/

#include "panel.h"
#include "virtualfs.h"
#include "fileview.h"
#include "statusbar.h"
#include "messagebox.h"

#include "../icons/settings.xpm"
#include "../icons/closeview.xpm"
#include "../icons/mainview_icons.h"

#include "plugins/view/settings/textviewsettings.h" // a structure contains the settings
#include "plugins/view/text/textview.h"

#include <qheader.h>
#include <qfileinfo.h>
#include <qsettings.h>
#include <qvaluelist.h>
#include <qfiledialog.h>
// #include <qdatastream.h>
#include <qapplication.h>

// TODO 1. liste QListView zamienic na ListViewWidgets

static QValueList <int>sSW; // width of 2 spliter obj.
static bool sCanShowView;


FileView::FileView( const QStringList & slFilesList, uint nCurrentItem, bool bEditMode, QWidget *pParent, const char *sz_pName )
	: mView(NULL), mSplitter(NULL), mStatus(NULL), mSideListView(NULL), mSettingsLib(NULL), mViewLib(NULL), mSettingsWidget(NULL)
	, mFilesAssociation(NULL), mKindOfFile(UNKNOWNfile), mKindOfView(View::UNKNOWNview), m_bInitEditMode(bEditMode), mMenuBar(NULL)
	, mToolBar(NULL), mKeyShortcuts(NULL), mSB_Height(24)
{
	if ( slFilesList.isEmpty() || slFilesList[0] == "" ) {
		debug("FileView::FileView (%s) - ERROR - not given file to open!", sz_pName);
		return;
	}
	debug("FileView::FileView (%s), filesToOpen=%d file1=%s", sz_pName, slFilesList.count(), slFilesList[0].latin1());

	mCurrentDirPath = FileInfoExt::filePath( slFilesList[nCurrentItem] );
	m_bViewInWindow = (pParent != NULL) ? FALSE : TRUE;

	setName( sz_pName );

	mViewParent = (m_bViewInWindow) ? this : pParent;

	initView(); // inits: width, height, mInitWidth and whether to show toolBar, by data from the config file, mPluginsPath

	mSettingsLib = new QLibrary( mPluginsPath+"libsettingsview.so" );

	mSplitter = new QSplitter( Qt::Horizontal, mViewParent, "Spliter" );
	mSplitter->resize( mInitWidth, height() );
	mSplitter->setOpaqueResize( TRUE ); // during size change still shows contains of panels
	mSplitter->setHandleWidth( 3 ); // default is 6
	mSplitter->show();

	mSideListView = new QListView( mSplitter );
	mSideListView->addColumn( tr("Name") );
	mSideListView->addColumn( tr("Location") );
	mSideListView->setAllColumnsShowFocus( TRUE );
	connect( mSideListView, SIGNAL(spacePressed( QListViewItem * )),
	 this, SLOT(slotOpenCurrentItem(QListViewItem *)) );
	connect( mSideListView, SIGNAL(returnPressed( QListViewItem * )),
	 this, SLOT(slotOpenCurrentItem(QListViewItem *)) );
	connect( mSideListView, SIGNAL(doubleClicked( QListViewItem *, const QPoint &, int )),
	 this, SLOT(slotOpenCurrentItem( QListViewItem *, const QPoint &, int )) );

	mStatus = new StatusBar( mViewParent, QString(sz_pName)+"_SW" );
	mStatus->resize( mInitWidth-3, 23 );
	mStatus->setSqueezingText( TRUE );

	insertFilesList( slFilesList ); // into the side list
	mSideListView->setSelected( mSideListView->findItem(FileInfoExt::fileName(slFilesList[nCurrentItem]), 0), TRUE );
// 		mSideListView->setSelected( mSideListView->firstChild(), TRUE );
	resize( mInitWidth, mInitHeigth );

	mSideListWidth = width()*24/100; // 24% of total width
	uint initListViewWidth = (slFilesList.count() > 1) ? mSideListWidth : 0;
	sSW.clear();
	sSW << initListViewWidth << width()-initListViewWidth;
	m_bShowTheFilesList = (initListViewWidth) ? TRUE : FALSE;
	mSideListView->header()->resizeSection( 0, mSideListWidth );

	initMenuBarAndToolBar( mViewParent );
	initKeyShortcuts();

	mInitFileName = slFilesList[nCurrentItem];
	sCanShowView = TRUE;
}


FileView::~FileView()
{
	debug("FileView, destruktor");
	// save window settings
	if ( m_bViewInWindow ) {
		mOptionsMenu->setItemChecked( 39, FALSE );

		QSettings settings;
		QString sWindowSize = QString::number(width())+","+QString::number(height());

		settings.writeEntry( "/qtcmd/FileView/Size", sWindowSize );
		settings.writeEntry( "/qtcmd/FileView/ShowMenuBar", ! m_bShowMenuBar );
		settings.writeEntry( "/qtcmd/FileView/ShowToolBar", ! m_bShowToolBar );

		mView->slotSaveSettings();
	}

	if ( mSettingsWidget != NULL )
		delete mSettingsWidget;
	if ( mSettingsLib != NULL ) {
		if ( mSettingsLib->isLoaded() ) {
			mSettingsLib->unload();
			debug("__ unload plugin '%s'", mSettingsLib->library().latin1() );
		}
		delete mSettingsLib;
	}
	removeView(); // remove view and unload plugin

	if ( mSideListView != NULL )
		 delete mSideListView;
	if ( mMenuBar != NULL )
		delete mMenuBar;
	if ( mToolBar != NULL )
		delete mToolBar;  // mAddonToolBar will be deleted too
	if ( mSplitter != NULL )
		delete mSplitter;
	if ( mStatus != NULL )
		delete mStatus;
}


void FileView::initView()
{
	QSettings settings;

	if ( m_bViewInWindow ) { // if ( mMB_Height )
		QString sizeStr = settings.readEntry( "/qtcmd/FileView/Size" );
		mInitWidth  = 600; // default value
		mInitHeigth = 400; // default value
		bool ok;

		if ( ! sizeStr.isEmpty() ) {
			int width = (sizeStr.left( sizeStr.find(',') )).toInt(&ok);
			if ( ok )
				if ( width >= 200 && width <= QApplication::desktop()->width() )
					mInitWidth = width;

			int height = (sizeStr.right( sizeStr.length()-sizeStr.findRev(',')-1 )).toInt(&ok);
			if ( ok )
				if ( height >= 100 && height <= QApplication::desktop()->height() )
					mInitHeigth = height;
		}
	}
	else { // embedded view
		mMB_Height  = 0;
		mTB_Height  = 31;
		mInitWidth  = mViewParent->width();
		mInitHeigth = mViewParent->height();
	}

	m_bShowMenuBar = settings.readBoolEntry( "/qtcmd/FileView/ShowMenuBar", TRUE );
	m_bShowToolBar = settings.readBoolEntry( "/qtcmd/FileView/ShowToolBar", TRUE );

	// --- init plugins settings
	mPluginsNameList.clear();
	mPluginsNameList << "libtextview.so" << "libimageview.so" << "libsoundview.so" << "libvideoview.so" << "libbinaryview.so" << "";

	mPluginsPath = FileInfoExt::filePath( qApp->applicationDirPath() ); // parent dir.for binary file location dir.
	if ( QFileInfo(mPluginsPath+".libs").exists() ) // apps. executed from project
		mPluginsPath += ".libs/plugins/";
	else
		mPluginsPath += "lib/qtcmd/plugins/"; // apps. is installed
}


void FileView::initKeyShortcuts()
{
	mKeyShortcuts = new KeyShortcuts();

	mKeyShortcuts->append( Help_VIEW, tr("Help"), Qt::Key_F1 );
	mKeyShortcuts->append( Open_VIEW, tr("Open"), Qt::CTRL+Qt::Key_O );
	mKeyShortcuts->append( Save_VIEW, tr("Save"), Qt::CTRL+Qt::Key_S );
	mKeyShortcuts->append( Reload_VIEW, tr("Reload"), Qt::Key_F5 );
	mKeyShortcuts->append( SaveAs_VIEW, tr("Save as"), Qt::ALT+Qt::CTRL+Qt::Key_S );
	mKeyShortcuts->append( OpenInNewWindow_VIEW, tr("Open in new window"), Qt::CTRL+Qt::Key_N );
	mKeyShortcuts->append( ShowHideMenuBar_VIEW, tr("Show/hide menu bar"), Qt::CTRL+Qt::Key_M );
	mKeyShortcuts->append( ShowHideToolBar_VIEW, tr("Show/hide tool bar"), Qt::CTRL+Qt::Key_T );
	mKeyShortcuts->append( ShowHideSideList_VIEW, tr("Show/hide side list"), Qt::CTRL+Qt::Key_L );
	mKeyShortcuts->append( ToggleToRawMode_VIEW, tr("Toggle to Raw mode"), Qt::ALT+Qt::CTRL+Qt::Key_W );
	mKeyShortcuts->append( ToggleToEditMode_VIEW, tr("Toggle to Edit mode"), Qt::Key_F4 );
	mKeyShortcuts->append( ToggleToRenderMode_VIEW, tr("Toggle to Render mode"), Qt::ALT+Qt::CTRL+Qt::Key_R );
	mKeyShortcuts->append( ToggleToHexMode_VIEW, tr("Toggle to Hex mode"), Qt::ALT+Qt::CTRL+Qt::Key_H );
	mKeyShortcuts->append( ShowSettings_VIEW, tr("Show settings"), Qt::CTRL+Qt::SHIFT+Qt::Key_S );
	mKeyShortcuts->append( Quit_VIEW, tr("Quit"), Qt::CTRL+Qt::Key_Q );

	mKeyShortcuts->updateEntryList( "/qtcmd/FileViewShortcuts/" ); // update from config file
	slotApplyKeyShortcuts( mKeyShortcuts ); // for actions in current window
}


void FileView::initMenuBarAndToolBar( QWidget * parent )
{
	// actions for FILE menu
	mNewA = new QAction( QPixmap((const char**)filenew), tr("Open in a &new window"), CTRL+Key_N, this );
	mOpenA = new QAction( QPixmap((const char**)fileopen), tr("&Open"), CTRL+Key_O, this );
	mSaveA = new QAction( QPixmap((const char**)filesave), tr("&Save"), CTRL+Key_S, this );
	mReloadA = new QAction( QPixmap((const char**)filereload), tr("&Reload"), Key_F5, this );

	connect( mNewA,    SIGNAL(activated()), this, SLOT(slotNew()) );
	connect( mOpenA,   SIGNAL(activated()), this, SLOT(slotOpen()) );
	connect( mSaveA,  SIGNAL(activated()), this, SLOT(slotSave()) );
	connect( mReloadA, SIGNAL(activated()), this, SLOT(slotReload()) );

	// --- inits main menu
	mMenuBar = new QMenuBar( parent );
	mFileMenu = new QPopupMenu( mMenuBar );
	mEditMenu = new QPopupMenu( mMenuBar );
	mViewMenu = new QPopupMenu( mMenuBar );
	mOptionsMenu = new QPopupMenu( mMenuBar );

	mMenuBar->insertItem( tr("&File"),    mFileMenu, 1 );
	mMenuBar->insertItem( tr("&Edit"),    mEditMenu, 2 );
	mMenuBar->insertItem( tr("&View"),    mViewMenu, 3 );
	mMenuBar->insertItem( tr("&Options"), mOptionsMenu, 4 );

	// - update file menu
	mRecentFilesMenu = new QPopupMenu( mFileMenu );

	QPopupMenu *sideListMenu = new QPopupMenu( mFileMenu );
	sideListMenu->insertItem( tr("&Load old files list"),    this, SLOT(slotLoadFilesList()) );
	sideListMenu->insertItem( tr("Save current files list"), this, SLOT(slotSaveFilesList()) );

	mNewA->addTo( mFileMenu );
	mOpenA->addTo( mFileMenu );
	mFileMenu->insertItem ( tr("Open &recent files"), mRecentFilesMenu );
	mSaveA->addTo( mFileMenu );
	mFileMenu->insertItem( tr("Save &as"), this, SLOT(slotSaveAs()), ALT+CTRL+Key_S, SaveAs_VIEW );
	mFileMenu->insertSeparator();
	mReloadA->addTo( mFileMenu );
	mFileMenu->insertItem( tr("Side files &list"), sideListMenu );
	mFileMenu->insertItem( tr("&Quit"), this, SLOT(close()), CTRL+Key_Q, Quit_VIEW );

	connect( mRecentFilesMenu, SIGNAL(activated( int )), this, SLOT(slotItemOfRecentFilesMenuActivated( int )) );

	// - update edit menu
	// there all options are into the view object. (mView)

	// - update view menu
	mViewMenu->setCheckable( TRUE );

	mSideListA = new QAction( QPixmap((const char**)sidelist), tr("Show/Hide side &list"), CTRL+Key_L, this );
	mMenuBarA  = new QAction( tr("Show/Hide &menu bar"), CTRL+Key_M, this );
	mToolBarA  = new QAction( tr("Show/Hide &tool bar"), CTRL+Key_T, this );

	connect( mSideListA, SIGNAL(activated()), this, SLOT(slotShowTheFilesList()) );
	connect( mMenuBarA,  SIGNAL(activated()), this, SLOT(slotShowHideMenuBar()) );
	connect( mToolBarA,  SIGNAL(activated()), this, SLOT(slotShowHideToolBar()) );

	mModeOfViewMenu = new QPopupMenu( mViewMenu );
	mModeOfViewMenu->setCheckable( TRUE );

	mViewMenu->insertItem( tr("Kind of view"), mModeOfViewMenu );
	mViewMenu->insertSeparator();
	mMenuBarA->addTo( mViewMenu );
	mToolBarA->addTo( mViewMenu );
	mSideListA->addTo( mViewMenu );
	mViewMenu->setItemChecked( 38, m_bShowTheFilesList );

	// update of sub-menus contains into view menu
	mModeOfViewMenu->insertItem( tr("Ra&w"),    View::RAWmode );
	mModeOfViewMenu->insertItem( tr("&Render"), View::RENDERmode );
	mModeOfViewMenu->insertItem( tr("&Hex"),    View::HEXmode );
	mModeOfViewMenu->insertItem( tr("&Edit"),   View::EDITmode );
	mModeOfViewMenu->setAccel( ALT+CTRL+Key_W, View::RAWmode );
	mModeOfViewMenu->setAccel( ALT+CTRL+Key_R, View::RENDERmode );
	mModeOfViewMenu->setAccel( ALT+CTRL+Key_H, View::HEXmode );
	mModeOfViewMenu->setAccel( Key_F4, View::EDITmode );
	connect( mModeOfViewMenu, SIGNAL(activated( int )), this, SLOT(slotChangeModeOfView( int )) );

	// - update option menu
	mConfigureA = new QAction( QPixmap((const char**)settings), tr("&Configure view"), CTRL+SHIFT+Key_S, this );

	connect( mConfigureA, SIGNAL(activated()), this, SLOT(slotShowSettings()) );

	mConfigureA->addTo( mOptionsMenu );

	if ( mMB_Height == 0 )
		mMenuBar->hide();

	// --- init ToolBar
	mToolBar = new QToolBar( "toolBar", 0, parent );
	mNewA->addTo( mToolBar );
	mOpenBtn = new QToolButton( mToolBar );
	mOpenBtn->setIconSet( QPixmap((const char**)fileopen) );
	mOpenBtn->setPopup( mRecentFilesMenu );
	mOpenBtn->setTextLabel( tr("Open")+"\t(Ctrl+O)" );
	mOpenBtn->setPopupDelay( 0 );
	connect( mOpenBtn, SIGNAL(clicked()), this, SLOT(slotOpen()) );

	mSaveA->addTo( mToolBar );
	mReloadA->addTo( mToolBar );

	QToolButton *viewBtn = new QToolButton( mToolBar );
	viewBtn->setPopup( mModeOfViewMenu );
	viewBtn->setPopupDelay( 0 );
	viewBtn->setIconSet( QPixmap((const char**)viewmode) );
	viewBtn->setTextLabel( tr("Change kind of view to")+"..." );

	mConfigureA->addTo( mToolBar );
// 	if ( mSideListView->childCount() > 1 )
	mSideListA->addTo( mToolBar );

	mAddonToolBar = new QToolBar( "AddonToolBar", 0, mToolBar );

	// need to resize a toolBar to width this same like the MainWindow width
	QToolButton *emptyBtn = new QToolButton( mToolBar );
	emptyBtn->setEnabled( FALSE );
	mToolBar->setStretchableWidget( emptyBtn );

	if ( ! m_bViewInWindow ) {
		QToolButton *closeBtn;
		closeBtn = new QToolButton( QPixmap((const char**)closeview), tr("Close view"), "", this, SIGNAL(signalClose()), mToolBar );
		closeBtn->setAccel( Key_Escape );
	}

	slotShowHideMenuBar();
	slotShowHideToolBar();
}


void FileView::updateMenuBar()
{
	// --- menu must be exclusive
	for( uint i=0; i<4; i++ ) // 4 modes of view
		mModeOfViewMenu->setItemChecked( i, FALSE );
	mModeOfViewMenu->setItemChecked( mModeOfView, TRUE );

	mSaveA->setEnabled( (mModeOfView == View::EDITmode && mKindOfView == View::TEXTview) );

	if ( ! m_bItIsTextFile ) { // binary file
		mFileMenu->setItemVisible( 13, FALSE ); // saveAs
		mModeOfViewMenu->setItemVisible( View::EDITmode, FALSE ); // edit mode for binary file is't available, now
	}
	else // it's text file
		mModeOfViewMenu->setItemVisible( View::EDITmode, TRUE );

	mModeOfViewMenu->setItemVisible( View::RENDERmode, TRUE );
	if ( mKindOfView == View::BINARYview && (mKindOfFile == ARCHIVEfile || mKindOfFile == UNKNOWNfile) )
		mModeOfViewMenu->setItemVisible( View::RENDERmode, FALSE );
	if ( mKindOfView == View::TEXTview && (mKindOfFile == SOURCEfile || mKindOfFile == UNKNOWNfile) )
		mModeOfViewMenu->setItemVisible( View::RENDERmode, FALSE );

	mFileMenu->setItemEnabled( 12, mModeOfViewMenu->isItemChecked(View::EDITmode) ); // enable/disable save

	mSideListA->setVisible( (mSideListView->childCount() > 1) );
}


bool FileView::createView( const QString & fileName, View::KindOfView _kindOfView )
{
	// --- detect kind of file and init kind of view
	View::KindOfView kindOfView = _kindOfView;

	if ( _kindOfView == View::UNKNOWNview ) { // then detect it
		if ( mFilesAssociation ) {
			if ( mFilesAssociation->loaded() )
				mKindOfFile = mFilesAssociation->kindOfFile( fileName );
			else
				mKindOfFile = FileInfoExt::kindOfFile( fileName ); // FilesAssociation not loaded
		}
		else
			mKindOfFile = FileInfoExt::kindOfFile( fileName ); // FilesAssociation not loaded

		if ( mKindOfFile == IMAGEfile )  kindOfView = View::IMAGEview;
		else
		if ( mKindOfFile == SOUNDfile )  kindOfView = View::SOUNDview;
		else
		if ( mKindOfFile == VIDEOfile )  kindOfView = View::VIDEOview;
		else
		if ( ! m_bItIsTextFile && (mKindOfFile == UNKNOWNfile || mKindOfFile == ARCHIVEfile) )
			kindOfView = View::BINARYview;
		else
			kindOfView = View::TEXTview;
	}
	if ( kindOfView == mKindOfView ) // attempt to change to this same kind of view
		return TRUE;
	else
		removeView();

	debug("FileView::createView, init kindOfView=%d, detectable kindOfView=%d", _kindOfView, kindOfView );

	// --- loading a plugin
	if ( ! QFileInfo(mPluginsPath+mPluginsNameList[kindOfView]).exists() ) {
		QApplication::restoreOverrideCursor();
		int result = MessageBox::yesNo( this,
		 tr("File view error")+" - QtCommander",
		 tr("Plugin's path")+" : \""+mPluginsPath+"\"\n"+
		 tr("Plugin")+" \""+mPluginsNameList[(int)kindOfView]+"\" "+
		 tr("not exists in foregoing location !")+"\n\n"+
		 tr("Try to open into binary view ?"),
		 MessageBox::Yes, TRUE // TRUE - show the Cancel button, too
		);
		if ( result == MessageBox::Yes ) {
			kindOfView = View::BINARYview;
			if ( ! QFileInfo(mPluginsPath+mPluginsNameList[kindOfView]).exists() ) {
				MessageBox::critical( this,
				 tr("Plugins path")+" : \""+mPluginsPath+"\"\n\n"+
				 tr("Plugin")+" '"+mPluginsNameList[(int)kindOfView]+"' "+
				 tr("not exists in foregoing location !")
				);
				return FALSE;
			}
		}
		else // 'No' or 'Cancel'
			return FALSE;
	}

	mViewLib = new QLibrary( mPluginsPath+mPluginsNameList[(int)kindOfView] );
	mViewLib->load();
	debug( " load plugin='%s', isLoaded=%d", mViewLib->library().latin1(), mViewLib->isLoaded() );
	if ( ! mViewLib->isLoaded() )
		return FALSE;

	// load and init a plugin
	typedef View *(*t_func)(QWidget *, const char *); // make type of init's function
	t_func create = (t_func)mViewLib->resolve( "create_view" ); // resolve symbol and init fun.address

	mSideListView->resize( 0, mSplitter->height() );
	if ( create ) {
		debug(" symbol resolved, let's create view");
		// --- create new view
		mView = create( mSplitter, 0 );
		if ( mView == NULL )
			debug(" Cannot create new view !");
	}
	else
		debug(" symbol NOT resolved !");

	mSplitter->setSizes( sSW );
	mSideListView->resize( sSW[0], mSplitter->height() );
	if ( ! create || mView == NULL ) {
		QApplication::restoreOverrideCursor();
		return FALSE;
	}

	// --- initialize menu bar, status bar and tool bar
	mKindOfView = kindOfView;
	debug(" '%s' has been created", kindOfViewStr(mKindOfView).latin1() );
	mView->clear();

	if ( mStatus ) {
		mStatus->showNormal();
		connect( mView, SIGNAL(signalUpdateStatus(const QString & , uint, bool)),
			mStatus, SLOT(slotSetMessage(const QString & , uint, bool)) );
	}
	// - update the actions
	if ( mKindOfView == View::TEXTview || mKindOfView == View::BINARYview ) {
		if ( mView->editMenuBarActions() ) // actions available
			mView->editMenuBarActions()->addTo( mEditMenu );
		if ( mView->viewMenuBarActions() )
			mView->viewMenuBarActions()->addTo( mViewMenu );
	}
	mAddonToolBar->clear();
	if ( mView->viewToolBarActions() ) // actions available
		mView->viewToolBarActions()->addTo( mAddonToolBar );
	if ( mView->editToolBarActions() )
		mView->editToolBarActions()->addTo( mAddonToolBar );

	mView->setKeyShortcuts( mKeyShortcuts );

	return TRUE;
}


void FileView::removeView()
{
	if ( mView != NULL ) {
		delete mView; mView = 0;
	}
	if ( mViewLib != NULL )  {
		if ( mViewLib->isLoaded() /*&& mAlwaysUnloadPlugin*/ ) {
			debug("  unload plugin='%s'", mViewLib->library().latin1() );
			mViewLib->unload();
			delete mViewLib; mViewLib = 0;
		}
	}
}


void FileView::setNewKindOfView( View::KindOfView kindOfView )
{
	debug("FileView::setNewKindOfView, try to create kind of view=%d", kindOfView );
	createView( mCurrentFileName, kindOfView );
}


void FileView::insertFilesList( const QStringList & filesList )
{
	if ( mSideListView )
		for(uint i=0; i<filesList.count(); i++ )
			(void) new QListViewItem( mSideListView, FileInfoExt::fileName(filesList[i]), FileInfoExt::filePath(filesList[i]) );
}


void FileView::loadingLate( const QString & fileName )
{
	mInitFileName = fileName;
	slotReadyRead(); // asynchronously loading file with name 'mInitFileName'
}


void FileView::slotReadyRead()
{
	debug("FileView::slotReadyRead()" );
	mStatus->slotSetMessage( tr("Loading file in progress ...") );

	bool isLocalFS = (VirtualFS::kindOfFilesSystem(mInitFileName) == LOCALfs);
	if ( sCanShowView ) { // need for FTP, becouse part of file has been read
		sCanShowView = FALSE;
		if ( m_bViewInWindow )
			show();
		if ( ! isLocalFS )
			slotLoadFile(); // preparing view window for show
	}
	if ( isLocalFS )
		QTimer::singleShot( 0, this, SLOT(slotLoadFile()) );
	else
		slotReadFile();
}


void FileView::slotLoadFile()
{
	debug("FileView::slotLoadFile(), fileName=%s", mInitFileName.latin1());
	m_bDetectIsBinOrTextFile = FALSE;
	mCurrentFileName = mInitFileName;
	QApplication::setOverrideCursor( waitCursor );

	bool isLocalFS = (VirtualFS::kindOfFilesSystem(mCurrentFileName) == LOCALfs);
	mFileLoadMethod = View::APPEND;

	if ( FileInfoExt::kindOfFile(mInitFileName) == IMAGEfile && isLocalFS )
		mFileLoadMethod = View::ALL;
	mBytesRead = (mFileLoadMethod == View::ALL) ? -1 : 10240;

	// init read file
	if ( isLocalFS )
		QTimer::singleShot( 0, this, SLOT(slotReadFile()) );

	// --- update window's capion
	if ( m_bViewInWindow )
		setCaption( mCurrentFileName + " - QtCommander" );
	// --- inserts files name to the recent files list
	if ( mRecentFilesMenu ) {
		bool insertName = TRUE;
		for( uint i=0; i<mRecentFilesMenu->count(); i++ )
			if ( mRecentFilesMenu->text(i) == mCurrentFileName ) {
				insertName = FALSE;
				break;
			}
		if ( insertName )
			mRecentFilesMenu->insertItem( mCurrentFileName, mRecentFilesMenu->count() );
	}
}


void FileView::slotReadFile()
{
	emit signalReadFile( mInitFileName, mBinBuffer, mBytesRead, mFileLoadMethod );

	if ( mBytesRead < 1 && mView != NULL ) { // mBytesRead < 0 when an error occures
		QApplication::restoreOverrideCursor();
		if ( mBytesRead == 0 ) // file has been loaded
			mView->updateStatus();
		if ( (mModeOfView == View::HEXmode || (! m_bItIsTextFile && mModeOfView == View::RAWmode)) && mBinBuffer.size() > 0 ) // need to move a contents to top of view
			mView->updateContents( View::NONE, mModeOfView, mCurrentFileName, mBinBuffer );
		updateMenuBar();
		return;
	}
	if ( mBytesRead > 0 )
		mBinBuffer.resize( mBytesRead+1 );
	else
		mBinBuffer.resize( 0 );

	if ( ! m_bDetectIsBinOrTextFile ) { // executed one time
		m_bDetectIsBinOrTextFile = TRUE;
		m_bItIsTextFile = (mBinBuffer.contains(0) < 2); // whether a file has two or more 0's bytes
		debug(" It's text file=%d, try create view", m_bItIsTextFile );
		if ( ! createView(mInitFileName) ) {
			close( TRUE );
			return;
		}
		if ( m_bItIsTextFile ) {
			if ( m_bInitEditMode ) {
				m_bInitEditMode = FALSE;
				mModeOfView = View::EDITmode;
			}
			else
				mModeOfView = (mKindOfFile == RENDERfile) ? View::RENDERmode : View::RAWmode;
		} // if ( m_bItIsTextFile )
		else
			mModeOfView = ((mKindOfFile == IMAGEfile || mKindOfFile == SOUNDfile || mKindOfFile == VIDEOfile) ? View::RENDERmode : View::HEXmode);

		updateMenuBar();
	} // if ( ! m_bDetectIsBinOrTextFile )
	if ( m_bItIsTextFile && mBytesRead > 0 )
		mBinBuffer[ mBytesRead ] = '\0'; // need to cuts some rubbishes during conversion to local8Bit

	mView->updateContents( (View::UpdateMode)mFileLoadMethod, mModeOfView, mCurrentFileName, mBinBuffer );

	//if ( (mFileLoadMethod == View::ALL || mFileLoadMethod == View::ON_DEMAND) && mBinBuffer.size() > 0) {
	if ( mFileLoadMethod == View::ALL || mFileLoadMethod == View::ON_DEMAND) {
		QApplication::restoreOverrideCursor();
		return;
	}
	// only for HTML files (sets all contents a file - need to fix bug in the HTML rendering)
	if ( mFileLoadMethod == View::APPEND && mBytesRead < 10240 && m_bItIsTextFile && mModeOfView == View::RENDERmode )
		mView->updateContents( View::ALL, mModeOfView, mCurrentFileName, mBinBuffer );

	if ( VirtualFS::kindOfFilesSystem(mInitFileName) == LOCALfs)
		QTimer::singleShot( 0, this, SLOT(slotReadFile()) ); // slotReadFile();

	mStatus->slotSetMessage( tr("Loading file in progress ...") );
}


void FileView::slotNew()
{
	QString fileName = QFileDialog::getOpenFileName( mCurrentDirPath, QString::null, this, 0, tr("Open file into new window")+" - QtCommander" );

	if ( fileName.isEmpty() )
		return;

	QStringList filesList;
	filesList.append( fileName );
	FileView *view = new FileView( filesList, 0 );
	view->show();
}


void FileView::slotOpen()
{
	QString fileName = QFileDialog::getOpenFileName( mCurrentDirPath, QString::null, this, 0, tr("Open file")+" ... - QtCommander" );

	if ( ! fileName.isEmpty() ) {
		if ( mView != NULL ) { // remove old view
			delete mView;  mView = 0;
			mKindOfView = View::UNKNOWNview;
		}
		(void) new QListViewItem( mSideListView, FileInfoExt::fileName(fileName), FileInfoExt::filePath(fileName) ); // NEW FEATURE
		mSideListA->setVisible( (mSideListView->childCount() > 1) );
		loadingLate( fileName ); // asynchronously file loading
	}
	else
		mStatus->slotSetMessage( tr("Loading aborted"), 2000 );
}


void FileView::slotSave()
{
	if ( mView == NULL || ! mView->isModified() || ! m_bItIsTextFile )
		return;

	if ( mView->saveToFile(mCurrentFileName) ) {
		mStatus->slotSetMessage( tr("File has been saved."), 2000 );
		emit signalUpdateItemOnLV( mCurrentFileName );
	}
}


void FileView::slotSaveAs()
{
	QString startPath = mCurrentFileName.left( mCurrentFileName.findRev('/')+1 );
	QString fn = QFileDialog::getSaveFileName( startPath, QString::null, this, 0, tr("Save as")+" ... - QtCommander" );

	if ( ! fn.isEmpty() ) {
		if ( m_bItIsTextFile ) {
			mView->saveToFile( fn );
			emit signalUpdateItemOnLV( fn ); // trzeba wstawic elem.
		}
	}
	else
		mStatus->slotSetMessage( tr("Saving aborted"), 2000 );
}


void FileView::slotReload()
{
	if ( mView != NULL ) {
		delete mView;  mView = 0; // remove old view
		mKindOfView = View::UNKNOWNview;
	}
	loadingLate( mCurrentFileName ); // asynchronously file loading
}


void FileView::slotChangeModeOfView( int itemId )
{
	debug("FileView::slotChangeModeOfView, to %d", itemId );
	mModeOfView = (View::ModeOfView)itemId;

	if ( ! m_bItIsTextFile ) { // it's binary file
		//mView->getData( mBinBuffer ); // b.wolno dziala (dla HEXmode), lepiej wczytac jeszcze raz
		View::KindOfView kindOfView = (mModeOfView == View::RAWmode || mModeOfView == View::HEXmode) ? View::BINARYview : View::UNKNOWNview;
		createView( mCurrentFileName, kindOfView );
		bool isLocalFS = (VirtualFS::kindOfFilesSystem(mCurrentFileName) == LOCALfs);
		mFileLoadMethod = View::APPEND;
		if ( mKindOfFile == IMAGEfile && mKindOfView != View::BINARYview && isLocalFS )
			mFileLoadMethod = View::ALL;
		mBytesRead = (mFileLoadMethod == View::ALL) ? -1 : 10240;
		mView->clear();
		if ( isLocalFS )
			QTimer::singleShot( 0, this, SLOT(slotReadFile()) );
	}
	else { // text's file
		if ( itemId == (int)View::HEXmode ) {
			if ( mKindOfView == View::TEXTview )
				mView->getData( mBinBuffer ); // gets (maybe) changed data
			createView( mCurrentFileName, View::BINARYview );
		}
		else
			createView( mCurrentFileName );

		if ( itemId == (int)View::RENDERmode )
			mView->clear(); // need to HTML render turn on
		mView->updateContents( View::ALL, (View::ModeOfView)itemId, mCurrentFileName, mBinBuffer );
	}

	updateMenuBar();
}


void FileView::slotItemOfRecentFilesMenuActivated( int itemId )
{
	QString fileName = mRecentFilesMenu->text( itemId );

	if ( fileName != mCurrentFileName ) {
		mView->clear();
		loadingLate( fileName ); // asynchronously file loading
	}
}


void FileView::slotShowHideMenuBar()
{
	if ( ! m_bViewInWindow ) { // embedded view must be without menu bar
		mMenuBar->hide();
		return;
	}

	mOptionsMenu->setItemChecked( 36, m_bShowMenuBar );

	if ( m_bShowMenuBar ) {
		mMB_Height = mMenuBar->height();
		mMenuBar->show();
	}
	else {
		mMB_Height = 0;
		mMenuBar->hide();
	}
	m_bShowMenuBar = ! m_bShowMenuBar;

	// update of geometry view
	mToolBar->move( 0, mMB_Height );
	mSplitter->setGeometry( 0, mMB_Height+mTB_Height,  width(),height()-(mMB_Height+mTB_Height+mSB_Height) );
}


void FileView::slotShowHideToolBar()
{
	mOptionsMenu->setItemChecked( 37, m_bShowToolBar );

	if ( m_bShowToolBar ) {
		mTB_Height = 31;
		mToolBar->show();
	}
	else {
		mTB_Height = 0;
		mToolBar->hide();
	}
	m_bShowToolBar = ! m_bShowToolBar;

	resizeEvent(NULL);  // update geometry of view
}


void FileView::slotShowTheFilesList()
{
	if ( mSideListView->childCount() < 2 )
		return;

	m_bShowTheFilesList = ! m_bShowTheFilesList;

	mOptionsMenu->setItemChecked( 38, m_bShowTheFilesList );
	sSW[0] = (m_bShowTheFilesList) ? mSideListWidth : 0;
	mSplitter->setSizes( sSW );
}


void FileView::slotLoadFilesList()
{
	QSettings settings;
	QStringList filesList = settings.readListEntry( "/qtcmd/FileView/URLs" );

	if ( filesList.isEmpty() )
		return;

	insertFilesList( filesList );

	// if the files list is invisible (or only part is visible) then show it
	if ( sSW[0] < width()*24/100 ) {
		m_bShowTheFilesList = FALSE;
		slotShowTheFilesList();
	}

	mStatus->slotSetMessage( tr("The files list has been loaded"), 2000 );
}


void FileView::slotSaveFilesList()
{
	QSettings settings;
	QStringList filesList = settings.readListEntry( "/qtcmd/FileView/URLs" );

	QListViewItemIterator it( mSideListView->firstChild() );
	while ( it.current() ) {
		filesList.append( it.current()->text(1)+it.current()->text(0) );
		++it;
	}

	if ( settings.writeEntry("/qtcmd/FileView/URLs", filesList) )
		mStatus->slotSetMessage( tr("The files list has been saved"), 2000 );
}


void FileView::slotOpenCurrentItem( QListViewItem * item )
{
	mView->clear();
	loadingLate( item->text(1)+item->text(0) ); // asynchronously file loading
}


void FileView::slotOpenCurrentItem( QListViewItem * item, const QPoint & , int )
{
	slotOpenCurrentItem( item );
}


void FileView::slotShowSettings()
{
	if ( mSettingsLib->isLoaded() ) // if a plugin is loaded then only show a widget.
		if ( mSettingsWidget ) {
			mSettingsWidget->show();
			return;
		}

	if ( ! QFileInfo(mPluginsPath+"libsettingsview.so").exists() ) {
		MessageBox::critical( this,
		 tr("Plugins path")+" : \""+mPluginsPath+"\"\n\n"+
		 tr("Plugin")+" 'libsettingsview.so' "+
		 tr("not exists in foregoing location !") );
		return;
	}
	// load and init a plugin
	typedef QWidget *(*t_func)(QWidget *, const char *); // make type of init's function
	t_func create = (t_func)mSettingsLib->resolve( "create_widget" ); // resolve symbol and init fun.address

	if ( ! mSettingsLib->isLoaded() ) {
		debug("plugin \""+mPluginsPath+"libsettingsview.so\" has'nt been loaded !");
		return;
	}
	if ( create ) {
		debug("symbol resolved, let's create the settings dialog");
		mSettingsWidget = create( 0,0 );

		if ( ! mSettingsWidget ) {
			debug("Cannot create the settings dialog");
			return;
		}
		connect( mSettingsWidget, SIGNAL(signalApply(const TextViewSettings &)),
		 mView, SLOT(slotApplyTextViewSettings(const TextViewSettings &)) );
/*		connect( mSettingsWidget, SIGNAL(signalApply(const ImageViewSettings &)),
		 mView, SLOT(slotApplyTextViewSettings(const ImageViewSettings &)) );
		connect( mSettingsWidget, SIGNAL(signalApply(const SoundViewSettings &)),
		 mView, SLOT(slotApplyTextViewSettings(const SoundViewSettings &)) );
		connect( mSettingsWidget, SIGNAL(signalApply(const VideoViewSettings &)),
		 mView, SLOT(slotApplyTextViewSettings(const VideoViewSettings &)) );*/
		connect( mSettingsWidget, SIGNAL(signalApplyKeyShortcuts(KeyShortcuts *)),
		 this, SLOT(slotApplyKeyShortcuts(KeyShortcuts *)) );
		connect( mSettingsWidget, SIGNAL(signalSaveSettings()), mView, SLOT(slotSaveSettings()) );
		connect( mSettingsWidget, SIGNAL(signalSaveSettings()), SIGNAL(signalRereadFilesAssociation()) );
		// --- init settings dlg.
		connect( this, SIGNAL(signalSetCurrentKindOfView(View::KindOfView)),
		 mSettingsWidget, SLOT(slotSetCurrentKindOfView(View::KindOfView)) );
		connect( this, SIGNAL(signalSetKeyShortcuts(KeyShortcuts *)),
		 mSettingsWidget, SLOT(slotSetKeyShortcuts(KeyShortcuts *)) );
		emit signalSetCurrentKindOfView( mKindOfView );
		emit signalSetKeyShortcuts( mKeyShortcuts );

		mSettingsWidget->show();
	}
	else
		debug("symbol NOT resolved");
}


void FileView::slotApplyKeyShortcuts( KeyShortcuts * keyShortcuts )
{
	if ( ! keyShortcuts )
		return;

	mNewA->setAccel( keyShortcuts->key(OpenInNewWindow_VIEW) );
	mOpenA->setAccel( keyShortcuts->key(Open_VIEW) );
	mReloadA->setAccel( keyShortcuts->key(Reload_VIEW) );
	mMenuBarA->setAccel( keyShortcuts->key(ShowHideMenuBar_VIEW) );
	mToolBarA->setAccel( keyShortcuts->key(ShowHideToolBar_VIEW) );
	mSideListA->setAccel( keyShortcuts->key(ShowHideSideList_VIEW) );
	mConfigureA->setAccel( keyShortcuts->key(ShowSettings_VIEW) );

	mModeOfViewMenu->setAccel( keyShortcuts->key(ToggleToRawMode_VIEW), View::RAWmode );
	mModeOfViewMenu->setAccel( keyShortcuts->key(ToggleToRenderMode_VIEW), View::RENDERmode );
	mModeOfViewMenu->setAccel( keyShortcuts->key(ToggleToHexMode_VIEW), View::HEXmode );
	mModeOfViewMenu->setAccel( keyShortcuts->key(ToggleToEditMode_VIEW), View::EDITmode );

	mOpenBtn->setTextLabel( tr("Open")+"\t("+mKeyShortcuts->keyStr(Open_VIEW)+")" );

	mFileMenu->setAccel( keyShortcuts->key(SaveAs_VIEW), SaveAs_VIEW );
	mFileMenu->setAccel( keyShortcuts->key(Quit_VIEW), Quit_VIEW );

// 	if ( mView != NULL )
// 		mView->applyKeyShortcuts( keyShortcuts );
}


// FUN.TYMCZASOWA - PRZEZNACZONA TYLKO DO TESTOW
QString FileView::kindOfViewStr( View::KindOfView kindOfView )
{
	if ( kindOfView == View::TEXTview )
		return "TEXTview";
	else
	if ( kindOfView == View::IMAGEview )
		return "IMAGEview";
	else
	if ( kindOfView == View::SOUNDview )
		return "SOUNDview";
	else
	if ( kindOfView == View::VIDEOview )
		return "VIDEOview";
	else
	if ( kindOfView == View::BINARYview )
		return "BINARYview";

	return "Unknown view";
}


void FileView::keyPressEvent( QKeyEvent * e )
{
	if ( e->key() == Key_Escape )
		close( TRUE ); // close and delete this
}

/*
	bool eventFilter( QObject * , QEvent * );
bool FileView::eventFilter( QObject * o, QEvent * e )
{
	if ( e->type() == QEvent::KeyPress ) {
		debug("FileView::eventFilter, KeyPress");
	if ( o == mSideListView )
		if ( e->key() == Key_Delete ) {
			QListViewItem *deletedItem = mSideListView->currentItem();
			bool isItemBelow = (bool)deletedItem->itemBelow();
			mSideListView->setCurrentItem( isItemBelow ? deletedItem->itemBelow() : deletedItem->itemAbove() );
			mSideListView->takeItem( deletedItem );
			return TRUE;
		}
	}
// zmieniac kolor statusu, kiedy okno jest aktywne
// Mozna w klasie SqueezeLabel dac metode (setActive) zmieniajaca kolor tla.

	focusOutEvent( QFocusEvent * );
	focusInEvent( QFocusEvent * );
	QColor mStatusBgColor;
	mStatusBgColor = mStatus->paletteBackgroundColor();
		mStatus->setPalette( mStatusBgColor );
		mStatus->setPalette( QColor( 219, 255, 244 ) ); //  QColor( 221, 255, 227 ); // blado zielony

	return QWidget::eventFilter( o, e );    // standard event processing
}
*/


void FileView::closeEvent( QCloseEvent *ce )
{
	if ( mView != NULL ) {
		if ( ! mView->isModified() ) {
			ce->accept();
			delete this; // force call destruktor (for unload a plugin)
			return;
		}
	}
	else {
		ce->accept();
		delete this; // force call destruktor (for unload a plugin)
		return;
	}

	int result = MessageBox::yesNo( this,
	 tr("Save file")+" - QtCommander",
	 tr("Do you want to save changed document ?"),
	 MessageBox::Yes, TRUE // TRUE - show the Cancel button, too
	);
	switch( result )
	{
		case MessageBox::Yes:
			slotSave();
			ce->accept();
			delete this; // force call destruktor (for unload a plugin)
			break;

		case MessageBox::No:
			ce->accept();
			delete this; // force call destruktor (for unload a plugin)
			break;

		case MessageBox::All: // 'Cancel' button pressed
		default: // just for sanity
			ce->ignore();
			break;
	}
}


void FileView::resizeEvent( QResizeEvent * )
{
	int windowWidth = width();

	if ( mToolBar != NULL )
		mToolBar->setGeometry( 0, mMB_Height-1, windowWidth, mTB_Height );
	if ( mStatus != NULL )
		mStatus->setGeometry( 0, height()-mSB_Height, windowWidth, mSB_Height );
	if ( mSplitter != NULL )
		mSplitter->setGeometry( 0, mMB_Height+mTB_Height,  windowWidth,height()-(mMB_Height+mTB_Height+mSB_Height) );
}
