/***************************************************************************
                          localfs.h  -  description
                             -------------------
    begin                : mon oct 28 2002
    copyright            : (C) 2002,2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
***************************************************************************/

#ifndef _LOCALFS_H_
#define _LOCALFS_H_

#include "virtualfs.h"
#include "enums.h"
#include "solf.h"

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa jest odpowiedzialna za obs�ug� lokalnego systemu plik�w.
 Uruchamiane s� wszelkie operacje plikowe na tym systemie plik�w, takie
 jak: listowanie katalogu, kopiowanie, przenoszenie, usuwanie, zmiana nazwy,
 tworzenie katalogu, tworzenie pustego pliku, wczytywanie pliku do podgl�du,
 wa�enie katalog�w, tworzenie link�w, ustawianie atrybut�w, wyszukiwanie pliku.
 Poza tym zawarta jest tu obs�uga zako�czenia i startowania poszczeg�lnych
 polece� systemu plik�w (min. obs�uga b��d�w) oraz zako�czenia operacji.
 Klasa po�redniczy pomi�dzy obiektem klasy VirtualFS a najni�ej operuj�cy
 obiektem klasy SoLF. Zawiera metody, kt�re zwracaj� podstawowe informacje
 o systemie plik�w i przetwarzanych plikach.
*/
class LocalFS : public VirtualFS
{
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param inputURL - nazwa bie��cej �cie�ki,
	 @param listViewExt - wska�nik na obiekt widoku listy plik�w.\n
	 �cie�ka w parametrze jest wykorzystywana przez klas� bazow� systemu plik�w,
	 tj. klas� VirtualFS.
	 */
	LocalFS( const QString & inputURL, ListViewExt *listViewExt );

	/** Destruktor klasy.\n
	 Usuwany jest tutaj dialog post�pu operacji oraz obiekt klasy SoLF.
	 */
	~LocalFS();

	/** Zwraca kod b��du dla ostatniej operacji plikowej.
	 @return kod b��du, patrz: @see Error.
	 */
	Error errorCode() const { return mSoLF->errorCode(); }

	/** Zwraca nazw� g��wnego katalogu.\n
	 @return nazwa g��wnego katalogu - "/".
	 */
	QString host()    const { return QString("/"); }

	/** Zwraca bie��c� �cie�k� absolutn�.\n
	 @return bie��ca �cie�ka absolutna.
	 */
	QString path()    const { return mCurrentPath; }

	/** Zwraca nazw� (ze �cie�k� absolut�) aktualnie przetwarzanego pliku.
	 @return nazwa pliku ze �cie�� absolutn�.
	 */
	QString nameOfProcessedFile() const {
		return mSoLF->nameOfProcessedFile();
	}

	/** Wywo�anie funkcji pobieraj�cej informacje o systemie plik�w dla podanego
	 katalogu.\n
	 @param path - nazwa katalogu w systemie plik�w,
	 @param freeBytes - zmienna, w kt�rej nale�y umie�ci� ilo�� wolnych bajt�w,
	 @param allBytes - zmienna, w kt�rej nale�y umie�ci� ilo�� wszystkich bajt�w.
	 @return TRUE, je�li uda�o si� pobra� informacje, w przeciwnym razie FALSE.
	 */
	bool getStatFS( const QString & path, long long & freeBytes, long long & allBytes ) {
		return SoLF::getStatFS( path, freeBytes, allBytes );
	}

	/** Uruchamia funkcj� pobieraj�c� wyniki operacji wa�enia, tj. waga ca�o�ci,
	 ilo�� plik�w i katalog�w.\n
	 @param weight - zmienna, w kt�rej zapisana b�dzie waga,
	 @param files - zmienna do zapisania ilo�ci plik�w,
	 @param dirs - zmienna do zapisania ilo�ci katalog�w.
	 */
	void getWeighingStat( long long & weigh, uint & files, uint & dirs ) {
		mSoLF->getWeighingStat( weigh, files, dirs );
	}

	/** Pobiera informacj� o pliku wcze�niej zainicjowan� w sk�adowej
	 @em mNewUrlInfo .\n
	 @param newUrlInfo - zmienna, w kt�rej nale�y umie�ci� informacj� o pliku.\n
	 Metoda jest wykorzystywana przez @em FilesPanel::slotResultOperation() ,
	 w celu zapewnienia odpowiedniej obs�ugi widoku listy.
	 */
	void getNewUrlInfo( UrlInfoExt & newUrlInfo ) {
		newUrlInfo = mNewUrlInfo;
	}

	/** Zwraca informacj� o tym czy bie��cy katalog ma prawa zapisu dla aktualnie
	 zalogowanego u�ytkownika.\n
	 @return informacja o zapisie typu @em DirWritable
	 \n \n Patrz te�: @see DirWritable.
	 */
	DirWritable currentDirWritable() {
		return QFileInfo(currentURL()).isWritable() ? ENABLEdw : DISABLEdw;
	}


	/** Funkcja zwraca nazw� u�ytkownika lub grupy, aktualnie zalogowanego lub
	 o podanym numerze Id.\n
	 @param user - r�wne TRUE (domy�lnie) wymusza zwr�cenie nazwy u�ytkownika,
	  w przeciwnym razie jest to grupa,
	 @param id - mniejsze od zera (domy�lnie) wymusza zwr�cenie nazwy zalogowanego
	  u�ytkownika b�d� grupy, do kt�rej nale�y. Wi�ksze od zera nakazuje zwr�cenie
	  nazwy u�ytkownika lub grupy podanego Id,
	 @return nazwa u�ytkownika lub grupy.
	 */
	static QString getUser( bool user=TRUE, int id=-1 );

	/** Funkcja umo�liwia pobranie listy u�ytkownik�w lub grup, albo numeru Id
	 podanego u�ytkownika lub grupy.
	 @param getUser - r�wne TRUE wymusza pobranie u�ytkownika(�w), w przeciwnym
	  razie pobierane s� grupy,
	 @param list - adres listy na kt�r� wstawiani s� pobrani u�ytkownicy b�d� grupy;
	  podawana jest te� przez ten parametr nazwa u�ytkownika lub grupy, kt�rej Id
	  powinno by� zwr�cone,
	 @param getAll - r�wne TRUE wymusza zapisanie na list� wszystkich dost�pnych
	  u�ytkownik�w lub grup w systemie, w przeciwnym razie na list� wstawiane s�
	  tylko nazwy grup dla aktualnie zalogowanego,
	 @param id - numer Id u�ytkownika lub grupy, kt�rej nazw� nale�y zapisa� na
	  li�cie,
	 @param returnId - r�wne TRUE, nakazuje zwr�cenie numeru Id u�ytkownika lub
	  grupy podanej na li�cie, w przciwnym razie zwracana jest warto�� -1,
	 @return numer Id u�ytkownika lub grupy.
	*/
	static int getOwners( bool getUser, QStringList & list, bool getAll, int id=-1, bool returnId=FALSE );

	/** Funkcja zwraca status zachowania fragmentu kopiowanego lub przenoszonego pliku.
	@return TRUE jezeli zachowano czesc pliku, w przeciwnym razie FALSE.
	*/
	bool afterBreakKeepFragment() { return mSoLF->afterBreakKeepFragment(); }

private:
	SoLF *mSoLF;
	QString mCurrentPath;

	bool mOperationHasFinished, mSkipNextErrorOccures;
	uint mOpFileCounter, mTotalFiles;
	UrlInfoExt mNewUrlInfo;

	QFile mFile;
	bool mFileHasBeenOpened;

	bool mRemoveToTrash, mWeighBeforeOperation;
	QString mTrashDirectory;

protected:
	/** Metoda s�u��ca do otwierania podanego pliku do podgl�du.\n
	 @param fileName - nazwa pliku do otwarcia.\n
	 Plik w rzeczywisto�ci nie jest otwierany, sprawdzane jest tylko, czy on
	 istnieje i czy mo�na go odczytywa�, je�li to prawda, wtedy wywo�ywana jest
	 procedura uruchamiaj�ca podgl�d tego pliku.
	 */
	void open( const QString & fileName );

	/** Metoda sprawdza czy podany plik istnieje i czy mo�na go odczytywa�.\n
	 @param fileName - nazwa pliku do sprawdzenia.
	 @param silent - TRUE spowoduje, �e nie zostanie pokazana informacja o b��dzie
	 @return TRUE je�li sprawdzanie zako�czy�o si� sukcesem, w przeciwnym
	  wypadku FALSE.
	 */
	bool fileIsReadable( const QString & fileName, bool silent=FALSE );

	/** Metoda uruchamia listowanie plik�w z podanego katalogu.\n
	 @param fullPathName - �cie�ka katalogu, kt�ry nale�y wylistowa�.
	 */
	void readDir( const QString & fullPathName );

	/** Metoda uruchamia procedur� tworzenia nowego katalogu lub struktury
	 katalog�w o podanej nazwie.\n
	 @param dirName - nazwa katalogu lub struktury do utworzenia.
	 */
	void mkdir( const QString & dirName );

	/** Metoda uruchamia procedur� utworzenia nowego pliku lub katalogu.\n
	 @param fullFileName - nazwa pliku lub katalogu do utworzenia.\n
	 Je�li nazwa ko�czy sie znakiem '/', wtedy wywo�ywane jest polecenie
	 tworzenia nowego katalogu, w przeciwnym razie tworzony jest pusty plik.
	*/
	void createAnEmptyFile( const QString & fullFileName );

	/** Uruchamia kopiowanie lub przenoszenie plik�w z podanej listy do podanego
	 katalogu.\n
	 @param targetPath - �cie�ka docelowa kopiowania lub przenoszenia,
	 @param filesList - lista plik�w do skopiowania lub przeniesienia,
	 @param savePerm - r�wne TRUE nakazuje zachowywanie atrybut�w kopiowanych plik�w,
	 @param alwaysOvr - je�li ma warto�� TRUE spowoduje, �e pliki zawsze b�d�
	  nadpisywane,
	 @param followByLink - r�wne TRUE spowoduje skopiowanie nie link�w, ale plik�w
	  na kt�ry ka�dy link wskazuje,
	 @param move - r�wne TRUE wymusza przenoszenie plik�w, w przeciwnym razie
	  nast�puje kopiowanie.
	 */
	void copyFiles( QString & targetPath, const QStringList & filesList, bool savePerm, bool alwaysOvr, bool followByLink, bool move );

	/** Metoda uruchamia usuwanie plik�w z podanej listy.\n
	 @param filesList - lista plik�w do usuni�cia,
	 */
	void deleteFiles( const QStringList & filesList );

	/** Metoda uruchamia wa�enie podanych na li�cie plik�w.\n
	 @param filesList - lista plik�w do zwa�enia
	 @param realWeight - r�wne TRUE spowoduje, �e funkcja wa��ca uwzgl�dni
	  rzeczywist� wag� pliku w systemie plik�w.
	 */
	void sizeFiles( const QStringList & filesList, bool realWeight );

	/** Metoda uruchamia tworzenie linku lub grupy link�w, albo edycj�
	 pojedynczego.\n
	 @param sourceFilesList - lista �r�d�owych plik�w,
	 @param targetFilesList - lista nazw docelowych link�w,
	 @param editOnly - r�wny TRUE, m�wi �e nast�pi tylko edycja jednego linku,
	 @param hard - r�wny TRUE, wymusza tworzenie tzw.twartych link�w, w przeciwnym
	  razie tworzone s� tzw. mi�kkie linki,
	 @param alwaysOvr - r�wny TRUE, spowoduje �e linki zawsze b�d� nadpisywane.
	 */
	void makeLink( const QStringList & sourceFilesList, const QStringList & targetFilesList, bool editOnly, bool hard, bool alwaysOvr );

	/** Metoda uruchamia ustawianie atrybut�w dla podanych na li�cie plik�w.\n
	 @param filesList - lista plik�w, kt�rym nale�y ustawi� atrybuty,
	 @param newUrlInfo - informacja o atrybutach dla plik�w,
	 @param recursive - r�wne TRUE, spowoduje �e atrybuty b�d� zmieniane dla ca�ej
	  zawarto�ci katalog�w z listy,
	 @param changesFor - rodzaj zmiany atrybut�w, dozwolone s�:
	  @li 0 - zmiana dla wszystkich,
	  @li 1 - zmiana tylko dla plik�w,
	  @li 2 - zmiana tylko dla katalog�w.
	 */
	void setAttributs( const QStringList & filesList, const UrlInfoExt & newUrlInfo, bool recursive, int changesFor );

	/** Metoda wywo�ywana po uruchomieniu wyszukiwania na dialogu szukania pliku.\n
	 @param findCriterion - adres obiektu zawieraj�cego kryteria wyszukiwania.\n
	 Uruchamiane jest tutaj wyszukiwanie pliku.
	 */
	void startFindFile( const FindCriterion & findCriterion );

	/** Zwraca kod bie��cej operacji, patrz: @see Operation.\n
	 @return kod bie��cej operacji.
	 */
	Operation currentOperation() const { return mSoLF->currentOperation(); }

	/** Funkcja uruchamia procedur� przerwania bie��cej operacji.
	 */
	void breakCurrentOperation();

	/** Zwraca informacje o tym czy podany plik jest linkiem symbolicznym.\n
	 @return TRUE podany plik jest linkiem, FALSE - plik nie jest linkiem.
	 */
	bool isLink( const QString & fileName ) { return QFileInfo( fileName ).isSymLink(); }

	/** Zwraca �cie�k� docelow� dla podanego linku.\n
	 @return �cie�ka docelowa linku.
	 */
	QString readLink( const QString & fileName ) { return QFileInfo( fileName ).readLink(); }

	/** Zwraca nazw� aktualnie zalogowanego u�ytkownika.\n
	 @return nazwa u�ytkownika.
	 */
	QString loggedOwner() { return getUser(); }

	/** Zwraca nazw� grupy dla aktualnie zalogowanego u�ytkownika.\n
	 @return nazwa grupy.
	 */
	QString loggedGroup() { return getUser( FALSE ); }

	/** Zwraca liczb� wszystkich plik�w i katalog�w w bie��cym katalogu.\n
	 @return suma plik�w i katalog�w z bie��cego.
	 */
	uint numOfAllFiles() { return mSoLF->numOfAllFiles(); }

	/** Metoda wykorzystywana do inicjowania skladowej klasy @em mNewUrlInfo .\n
	 @param fileName - nazwa pliku, kt�remu nale�y pobra� atrybuty.\n
	 \n Patrz te�: @em getNewUrlInfo()
	 */
	void initNewUrlInfo( const QString & fileName=QString::null );

	/** W��cza b�d� wy��cza bezpieczne usuwnie plik�w.\n
	 @param toTrash - TRUE w��cza bezpieczne usuwanie (przenoszenie do "kosza"),
	  FALSE w��cza rzeczywiste usuwanie (domy�lne ustawianie).\n
	 Patrz te�: @see setTrashPath().
	 */
	void setRemoveToTrash( bool toTrash ) { mRemoveToTrash = toTrash; }

	/** Ustawia �cie�k� dla katalogu "kosza".\n
	 @param path - �cie�ka do katalogu "kosza".
	 */
	void setTrashPath( const QString & path ) { mTrashDirectory = path; }

	/** W��cza albo wy��cza czynno�� wa�enia plik�w i katalog�w przed operacj�
	 plikow� (np. kopiowania).\n
	 @param weighBefore - warto�� TRUE spowoduje, �e pliki i katalogi b�d� wa�one,
	  w przeciwnym razie czynno�� ta zostanie pomini�ta (domy�lnie jest to
	  w��czone, wy��czenie przy�piesza operacj� plikow�, ale nie jest dost�pna
	  informacja o jej  post�pie).
	 */
	void setWeighBeforeOperation( bool weighBefore ) { mWeighBeforeOperation = weighBefore; }

public slots:
	/** Slot uruchamia procedur� wstrzymania lub wznowienia bie��cej operacji.\n
	 @param stop - r�wne TRUE, wymusza wstrzymanie operacji, FALSE powoduje
	  wznowienie wstrzymanej.
	 */
	void slotPause( bool stop );

	/** Slot powoduje zainicjowanie podanego obiektu z informacj� o pliku
	 atrybutami podanego.\n
	 @param fullFileName - nazwa pliku, kt�remu nale�y pobra� atrybuty,
	 @param urlInfoExt - adres obiektu, kt�ry nale�y zainicjowa� atrybutami.\n
	 Przy czym pobierane s� tylko nast�puj�ce atrybuty: nazwa, rozmiar oraz czas
	 ostatniej modyfikacji podanego pliku.
	 */
	void slotGetUrlInfo( const QString & fullFileName, UrlInfoExt & urlInfoExt );

	/** Slot powoduje wczytywanie pliku wybranego do podgl�du.\n
	 @param fileName - nazwa pliku do wczytania,
	 @param buffer - adres bufora, w kt�rym nale�y umieszcza� wczytywane dane,
	 @param bytesRead - ilo�� wczytywanych na raz bajt�w, a po odczycie zawiera
	  ilo�� faktycznie wczytanych bajt�w (0 oznacza koniec pliku),
	 @param fileLoadMethod - metoda wczytywania pliku, patrz @see View::UpdateMode
	 */
	void slotReadFileToView( const QString & fileName, QByteArray & buffer, int & readBlockSize, int fileLoadMethod );

private slots:
	/** Slot wywo�ywany w momenecie zakonczenia operacji.\n
	 @param error - r�wne TRUE, m�wi �e wyst�pi� b��d.\n
	 Operacja zosta�a zako�czona je�li wykonano ostatnie polecenie z kolejki, lub
	 wyst�pi� b��d "nie do omini�cia". Przyk�adow� operacj� mo�e by� przenoszenie,
	 a ostatnim jego poleceniem jest usuwanie.
	 */
	void slotDone( bool error );

	/** Slot powoduje wy�wietlenie info o bie��cej operacji na pasku statusu.\n
	 @param state - kod operacji (typu Operation).\n
	 Wysy�any jest tutaj tylko sygna� @em signalSetOperation() .
	 */
	void slotStateChanged( int state );

	/** Slot wywo�ywany w momencie rozpocz�cia wykonywania polecenia.\n
	 @param cmd - kod polecenia (typu Operation).
	 */
	void slotCommandStarted( int command );

	/** Slot wywo�ywany w chwili zako�czenia wykonywania podanego polecenia.\n
	 @param cmd - kod polecenia ( typu @em Operation ),
	 @param error - r�wne TRUE oznacza, �e pojawi� si� b��d w wykonanym poleceniu.\n
	 Je�li pr�ba wykonania polecenia zako�czy�a si� b��dem pokazywany jest
	 komunikat z informacj� o b��dzie i nazwy pliku lub katalogu, kt�rego b��d
	 dotyczny.
	 */
	void slotCommandFinished( int command, bool error );


	/** Slot wywo�ywany przez obiekt klasy FileView, po wykonaniu zapisu do nowego
	 lub ju� otwartego pliku.\n
	 @param fileName - nazwa pliku, do kt�rego nast�puje zapis.
	 */
	void slotUpdateItemOnLV( const QString & fileName );

};

#endif
