/***************************************************************************
                          pathview.h  -  description
                             -------------------
    begin                : wed oct 23 2002
    copyright            : (C) 2002 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/


#ifndef _PATHVIEW_H_
#define _PATHVIEW_H_

#include "combopath.h"
#include "enums.h"

#include <qwidget.h>
#include <qlistview.h>
#include <qpopupmenu.h>
#include <qtoolbutton.h>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa pokazuj�ca list� ze �cie�kami oraz guzik z dodatkowymi opcjami.
 W klasie tworzony jest obiekt listy �cie�ek ( @em ComboPath ), ponad nast�puje
 utworzenie guzika z opcjami powoduj�cymi zmian� bie��cej �cie�ki na wybran�
 oraz opcjami edycji historii �cie�ek (czyszczenie i edycja w�a�ciwa).\n
 Poza tym klasa zawiera metody po�rednicz�ce w obs�udze widoku �cie�ek, takie
 jak: wstawianie, usuwanie podanej �cie�ki, ustawianie jako bie��cej �cie�ki tej
 o podanym numerze a tak�e pokazywanie historii �cie�ek.\n
 Dodatkow� cech� obiektu tej klasy jest zmiana t�a widoku w zale�no�ci od
 statusu zapisywalno�ci bie��cego katalogu przez aktualnie zalogowanego
 u�ytkownika. T�o jest blado zielone je�li mo�na zapisywa�, blado czerwone,
 je�eli brak tej mo�liwo�ci oraz bia�e gdy nie jest znany ten status.
*/
class PathView : public QWidget
{
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param ptrOfList - wska�nik na list� plik�w,
	 @param parent - wska�nik na obiekt rodzica,
	 @param name - nazwa dla obiektu.\n
	 Inicjowane s� tutaj niezb�dne sk�adowe oraz tworzone obiekty klasy ComboPath
	 oraz QToolButton (przycisk z menu).
	 */
	PathView( QListView *ptrOfList, QWidget *parent, const char *name=0 );

	/** Destruktor klasy.\n
	 Nast�puje tutaj zapisanie do pliku konfiguracyjnego listy URLi, oraz id dla
	 bie��cego, poza tym usuwane s� obiekty tworzone w klasie.
	 */
	~PathView();

	/** Zwraca bie��c� �cie�k�.\n
	 @return bie��cy ci�g na li�cie.\n
	 */
	QString currentUrl() const { return mComboPathView->currentText(); }

	/** Zwraca indeks bie��cego elementu na li�cie @em ComboPath .
	 @return indeks bie��cego elementu.
	 */
	int currentId() const { return mComboPathView->currentItem(); }

	/** Ustawia focus na widoku �cie�ki ( obiekt @em ComboPath ).
	 */
	void setFocus() { mComboPathView->setFocus(); }

	/** Zwraca informacj� o obecno�ci focusa na widoku �cie�ki.\n
	 @return TRUE, je�li widok �cie�ki zawiera focus, FALSE widok go nie posiada.
	 */
	bool hasFocus() const { return mComboPathView->hasFocus(); }

	/** Rozwija list� �cie�ek z odwiedzonymi katalogami.
	 */
	void showPopupList();

	/** Powoduje pokazanie menu guzika znajduj�cego si� obok widoku listy �cie�ek.
	 */
	void showButtonMenu();

	/** Zwraca poprzedni wzgl�dem bie��cego element listy.\n
	 @param localFS - r�wny TRUE, wtedy szukana jest �cie�ka z lokalnego systemu
	 plik�w, w przeciwnym razie zwr�cona zostanie pierwsza napotkana.\n
	 @return poprzedni URL.\n
	 Przy czym, je�li bie��cy URL jest pierwszym, wtedy zwracany jest ostatni.
	 */
	QString prevUrl( bool localFS=FALSE ) const;

	/** Zwraca nast�pny wzgl�dem bie��cego element listy.\n
	 @return nast�pny URL.\n
	 Przy czym, je�li bie��cy URL jest ostatnim, wtedy zwracany jest pierwszy.
	 */
	QString nextUrl() const {
		return mComboPathView->text( mComboPathView->nextItemId() );
	}

private:
	QToolButton *mButtonQuickCD;
	ComboPath   *mComboPathView;
	QPopupMenu  *mQuickCdMenu;


	/** Ustawia kolor t�a widoku �cie�ki na blado zielony lub blado czerwony.\n
	 Przed ustawieniem koloru nast�puje sprawdzenie statusu bie��cej �cie�ki.
	*/
	void setStatusWritable();

	/** Metoda inicjuj�ca widok �cie�ki.\n
	 Wczytywana jest tutaj i wstawiana do widoku, lista URLi oraz numer bie��cego
	 URLa, kt�ry staje si� bie��cym w widoku.
	 */
	void init();

	/** Wywo�uje sygna� uruchamiaj�cy zmian� bie��cej �cie�ki w systemie plik�w.\n
	 @param newPath - nowa �cie�ka.
	 */
	void chagePathInFS( const QString & newPath );

	/**
	 */
	void updateToolTips();

protected:
	/** Obs�ugiwane jest tutaj zdarzenie zmiany rozmiaru dla bie��cego obiektu.\n
	 Ustawiana jest odpowiednia szeroko�� widoku �cie�ki (pomniejszona o warto��
	 28, kt�ra jest szeroko�ci� guzika z menu).
	 */
	void resizeEvent( QResizeEvent * );

public slots:
	/** Powoduje wstawienie na list� nowego elementu (�cie�ki).\n
	 @param newPath - nazwa elementu do usuni�cia.\n
	 Przy czym element jest wstawiany, tylko je�li previousURL sam nie istnieje ju� na
	 li�cie.
	 */
	void slotPathChanged( const QString & newPath );

	/** Powoduje usuni�cie podanego elementu (�cie�ki) z listy.\n
	 @param pathName - nazwa elementu do usuni�cia.\n
	 Przy czym jako bie��cy ustawiany jest poprzedni, a je�li nie istnieje
	 poprzedni, wtedy brany jest nast�pny. Je�li lista zawiera 1 lub 0 element�w,
	 wtedy nic nie jest robione.
	 */
	void slotRemovePath( const QString & pathName );

private slots:
	/** Uruchamia zmian� katalogu na wy�szy w hierarchi (w bie��cym systemie
	 plik�w).\n
	 Wysy�any jest tutaj tylko sygna� wymuszaj�cy powy�sz� zmian�.
	 */
	void slotChangeDirToUp();

	/** Powoduje zmian� katalogu na korze� (w bie��cym systemie plik�w).
	 */
	void slotChangeDirToRoot();

	/** Powoduje zmian� katalogu na domowy w lokanym systemie plik�w.
	 */
	void slotChangeDirToUserHome();

	/** Powoduje zmian� katalogu na poprzedni wzgl�dem bie��cego (z historii).
	 */
	void slotChangeDirToPrev();

	/** Powoduje zmian� katalogu na nast�pny wzgl�dem bie��cego (z historii).\n
	 Przy czym je�li nie ma poprzedniego to brany jest ostatni.
	 */
	void slotChangeDirToNext();

	/** Usuwa wszystkie wpisy na li�cie historii �cie�ek.
	 */
	void slotClearHistoryPath();

	/** Powoduje wyczyszczenie bie��cej �cie�ki.
	 */
	void slotClearPath();

	/** Pokazuje okienko s�u��ce edycji historii �cie�ek.\n
	 Pozwala ono na: usuni�cie, edycj� oraz zmian� kolejno�ci na li�cie.
	 */
	void slotEditHistory();

signals:
	/** Sygna� powoduje pobranie statusu zapisywalno�ci bie��cego katalogu przez
	 aktualnie zalogowanego u�ytkownika.
	 @param dw - zmienna, kt�rej zapisana zostanie informacja, patrz te�:
	  @see DirWritable.\n
	 */
	void signalPossibleDirWritable( DirWritable & dw );

	/** Sygna� uruchamia zmian� bie��cego katalogu na podany.
	 @param newPath - nowa �cie�ka do odczytania.
	 */
	void signalPathChanged( const QString & newPath );

};

#endif
