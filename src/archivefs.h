/***************************************************************************
                          archivefs.h  -  description
                             -------------------
    begin                : tue dec 23 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _ARCHIVEFS_H_
#define _ARCHIVEFS_H_

#include "virtualarch.h"
#include "virtualfs.h"
#include "enums.h"

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short G��wna klasa zarz�dzaj�ca obiektami klas specyficznych dla archiw�w.
 Tworzone s� tutaj obiekty klas obs�uguj�cych poszczeg�lne archiwizery
 oraz uruchamiane czynno�ci wykonywane na archiwach, np. listowanie.
 Zawarta jest tutaj tak�e obs�uga akcji, kt�re nale�y wykona� przed
 rozpocz�ciem, albo po zako�czeniu danego polecenia dla archiwum.
*/
class ArchiveFS : public VirtualFS
{
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param inputURL - nazwa archiwum na kt�rym nale�y wykona� operacj�,
	 @param listViewExt wska�nik dla widok listy plik�w (konieczny dla rodzica).\n
	 Tworzone s� tutaj obiekty klas archiwizer�w (na podstawie podanej nazwy)
	 oraz ��czone sygna�y klasy VirtualArch ze slotami z bie��cej.
	 */
	ArchiveFS( const QString & currentItem, ListViewExt *listViewExt );

	/** Destruktor klasy.\n
	 Usuwany jest tutaj obiekt klasy VirtualArch.
	 */
	~ArchiveFS();

	/** Zwraca kod b��du dla ostatniej operacji na archiwum.\n
	 @return kod b��du.
	 */
	Error errorCode() const { return mVirtualArch->errorCode(); }

//	QString host() const { return QString("arc://"+mVirtualArch->archiveName()+"/"); }
	/** Zwraca nazw� archiwum z absolutn� �cie�k�.\n
	 @return nazwa archiwum z absolutn� �cie�k�.
	 */
	QString host() const { return currentURL().left(currentURL().find(mVirtualArch->archiveName()))+mVirtualArch->archiveName()+"/"; }

	/** Zwraca bie��c� �cie�k� w archiwum.\n
	 @return bie��ca �cie�ka.
	 */
	QString path() const { return mVirtualArch->currentPath(); }

	/** Zwraca nazw� aktualnie przetwarzanego pliku z absolutn� �cie�k�.\n
	 @return nazwa aktualnie przetwarzanego pliku.
	 */
	QString nameOfProcessedFile() const {
		return mVirtualArch->nameOfProcessedFile();
	}

	/** Pobierane s� tu wyniki operacji wa�enia, tj. waga ca�o�ci, ilo�� plik�w
	 i katalog�w \n
	 @param weight - zmienna, w kt�rej zapisana b�dzie waga,
	 @param files - zmienna do zapisania ilo�ci plik�w,
	 @param dirs - zmienna do zapisania ilo�ci katalog�w.
	 */
	void getWeighingStat( long long & weigh, uint & files, uint & dirs ) {
		mVirtualArch->getWeighingStat( weigh, files, dirs );
	}

	/** Pobiera informacj� o pliku wcze�niej zainicjowan� w sk�adowej
	 @em mNewUrlInfo .\n
	 @param newUrlInfo - zmienna, w kt�rej nale�y umie�ci� informacj� o pliku.\n
	 Metoda jest wykorzystywana przez @em FilesPanel::slotResultOperation() ,
	 w celu zapewnienia odpowiedniej obs�ugi widoku listy.
	 */
	void getNewUrlInfo( UrlInfoExt & newUrlInfo ) {
		newUrlInfo = mNewUrlInfo;
	}

	/** Zwraca informacj� o tym czy w bie��cym katalogu mo�na zapisywa�.\n
	 @return status zapisywalno�ci, mo�e on przyj�� nast�puj�ce warto�ci:
	  @li ENABLEdw - mo�na zapisywa�,
	  @li DISABLEdw - brak praw zapisu,
	  @li UNKNOWNdw - nie wiadomo.\n
	 Aktualnie zawsze zwracana jest warto�� UNKNOWNdw.
	 */
	DirWritable currentDirWritable();

	/** Zwraca informacje czy archiwum w podanym pliku jest obs�ugiwane.\n
	 @param fileName - nazwa pliku archiwum.
	 @return TRUE - archiwum jest obs�ugiwane, przy czym aktualnie wspierane s�
	 nast�puj�ce rodzaje archiw�w: 'tar', 'tar.gz', 'tar.bz2'.
	 */
	static bool archiveIsSupported( const QString & fileName );

	/** Zwraca nazw� katalogu roboczego.\n
	 @return nazwa katalogu roboczego z absolutn� �cie�k�.\n
	 Katalog roboczy jest wykorzystywany min. jako miejsce rozpakowywania plik�w
	 do podgl�du.
	 */
	QString workDirectory() const { return mVirtualArch->workDirectory(); }

private:
	VirtualArch *mVirtualArch;
	UrlInfoExt mNewUrlInfo;

	bool mSkipNextErrorOccures;

protected:
	/** Metoda uruchamia procedur� listowania archiwum dla podanego katalogu.\n
	 @param fullPathName - nazwa katalogu do wylistowania, przy czym podanie '/'
	 spowoduje wylistowanie na najwy�szym poziomie.
	 */
	void readDir( const QString & fullFileName );

	/** Ustawia �cie�k� do katalogu roboczego.\n
	 @param workDirectory - nowa �cie�ka do katalogu roboczego.
	 */
	void setWorkDirectoryPath( const QString & workDirectory ) {
		mVirtualArch->setWorkDirectory( workDirectory );
	}
// public slots:
// 	void slotPause( bool stop );
// 	void slotReadFileToView( const QString & fileName, QByteArray & buffer );

private slots:
	/** Slot wywo�ywany po zako�czeniu wykonywania operacji na archiwum.\n
	 @param error - TRUE oznacza, �e podczas wykonywania operacji wyst�pi� b��d.
	 */
	void slotDone( bool error );

	/** Metoda powoduje wys�anie sygna�u z informacj� o zmianie statusu
	 w przetwarzaniu archiwum.\n
	 @param state - status operacji.\n
	 Wysy�any jest sygna� o nazwie @em signalSetInfo().
	 */
	void slotStateChanged( int state );

	/** Slot wywo�ywany przed rozpocz�ciem wykonywania polecenia na archiwum.\n
	 @param command - polecenie do wykonania na archiwum.
	 */
	void slotCommandStarted( int command );

	/** Slot wywo�ywany po zako�czeniu wykonywania polecenia na archiwum.\n
	 @param command - wykonane polecenie na archiwum,
	 @param error - TRUE oznacza, �e podczas wykonywania polecenia wyst�pi� b��d,
	 przy czym warto�� b��du mo�na odczyta� przy pomocy metody @em errorCode().
	 */
	void slotCommandFinished( int command, bool error );

//	void slotUpdateItemOnLV( const QString & );
};

#endif
