/***************************************************************************
                          combopath.h  -  description
                             -------------------
    begin                : Tue Aug 14 2001
    copyright            : (C) 2001 by Piotr Mierzwi�ski
    email                : piom@obywatel.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef COMBOPATH_H
#define COMBOPATH_H

#include <qwidget.h>
#include <qcombobox.h>
#include <qlistview.h>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa zajmuje si� obs�ug� listy �cie�ek.
 Zawiera mechanizmy kompletowania podanego fragmentu nazwy wpisanego w linii
 edycyjnej. Dzia�a on wtedy, gdy pasuj�cy znajduje si� na li�cie QListView,
 do kt�rej wska�nik jest przekazywany w konstruktorze.
*/
class ComboPath : public QComboBox  {
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param ptrOfList - wska�nik na list� QListView,
	 @param maxItems - maksylna ilo�� element�w na li�cie,
	 @param rw - warto�� TRUE w��cza (domy�lne ustawienie) tryb odczytu i zapisu
	 @param parent - rodzic dla obiektu,
	 @param name - nazwa dla obiektu.
	 Inicjowane s� tutaj sk�adowe klasy oraz bie��cy obiekt (QComboBox).
	 Spe�nia on zadanie listy �cie�ek z linijk� edycyjn� dla bie��cej nazwy, je�li
 	 (parametr @em rm ma warto�� TRUE). Wska�nik na list� QListView jest niezb�dny
	 do dzia�ania kompletowania nazw katalog�w.
	 */
	ComboPath( QListView *ptrOfList, unsigned int maxItems, bool rw, QWidget *parent, const char *name=0 );

	/** Destruktor klasy.\n
	 Brak definicji.
	 */
	~ComboPath() {}

	/** Powoduje wstawienie podanej parametrze �cie�ki na pierwsz� pozycj� na
	li�cie.\n
	 @param pathName - nazwa �cie�ki jaka ma zosta� wstawiona na list�.\n
	 Przy czym, je�li taka sama �cie�ka ju� si� na niej znajduje, wtedy jest ona
	 tylko przestawiana na pierwsz� pozycj�.
	 */
	void insertPath( const QString & pathName );

	/** Metoda usuwa podan� �cie�k� z listy.\n
	 @param pathName - nazwa �cie�ki do usuni�cia.
	 */
	void removePath( const QString & pathName );

	/** Funkcja s�u�y do ustawiania maksymalnej ilo�ci element�w na li�cie.\n
	 @param maxItems - maksylna liczba element�w na li�cie, przy czym nie mo�e by�
	 wi�ksza ni� 100 (je�li tak jest nast�puje korekta na 100).
	 */
	void setTotalItems( unsigned int maxItems );

	/** Metoda wyszukuje na li�cie podan� nazw�.\n
	 @param pathName - nazwa �cie�ki do odszukania.
	 @return -1 je�li nazwa nie zosta�a odnaleziona, Id je�li j� znaleziono.
	 */
	int find( const QString & pathName ) const;

	/** Zwraca Id nast�pne wzgl�dem Id bie��cego elementu.\n
	 @return Id nast�pnego elementu, je�li bie��cym jest ostatni to 0.
	 */
	int nextItemId() const {
		return (currentItem() < count()-1) ? currentItem()+1 : 0;
	}

	/** Zwraca Id poprzednie wzgl�dem Id bie��cego elementu.\n
	 @return Id poprzedniego elementu, je�li bie��cym jest pierwszy to ostatni.
	 */
	int prevItemId() const {
		return (currentItem() > 0) ? currentItem()-1 : count()-1;
	}

private:
	QString mOldStr, mStr;
	QListView *mPtrOfList;
	bool mRestoreOldPath;


	/** Kompletowanie bie��cej �cie�ki.\n
	 @param path - aktualna zawarto�� linijki edycyjnej.\n
	 Metoda pr�buje uzupe�ni� wpisan� w linii edycyjnej nazw�,
	 dopasowuj�c ci�g do nazw na li�cie @em mPtrOfList (z jej kolumny zerowej).
	 Je�li dopasowanie si� powiedzie, wtedy pokazywany jest pod�wietlony
	 nowy pasuj�cy fragment nazwy, a kursor ustawiany na jego pocz�tek.
	*/
	void pathAutoCompletion( const QString & path );

	/** Szukanie podobnego elementu na li�cie.\n
	 @param itemName - szukany fragment nazwy.\n
	 Metoda sprawdza czy na li�cie (jej wska�nik zosta� podany w konstruktorze)
	 wyst�puje element, w kt�rego nazwie zawiera si� podany ci�g, je�li to prawda,
	 wtedy jest zwracana pierwsza taka nazwa.
	 */
	QString listContainsItem( const QString & itemName );

protected:
	/** Obs�uga klawiatury.\n
	 @param e - zdarzenie wci�ni�cia klawisza.\n
	 Przechwytywne jest tutaj wci�ni�cie pewnych klawiszy, np. Enter spowoduje
	 wywo�anie slotu informuj�cego o wci�nieciu klawisza Enter
	 ( @em slotReturnPressed ).
	 */
	void keyPressEvent( QKeyEvent * );

	/** Przechwytywanie wszystkich zdarze� dla obiektu.\n
	 @param o - obiekt, kt�rego dotyczny zdarzenie,
	 @param e - zdarzenie na podanym obiekcie.\n
	 Wewn�trz jest przechwytywane jest zdarzenie opuszczenia bie��cego obiektu,
	 co powoduje ustawienie jako bie��cej �cie�ki tej zapisanej w sk��dowej
	 @em mOldStr (�cie�ka kt�ra wcze�niej by�a bie��ca). Drugie �apane zdarzenie
	 to uaktywnienie bie��cego obiektu - wykonywane jest wtedy ustawienie jako
	 bie��cej �cie�ki zawarto�ci sk�adowej @em mStr.
	 */
	bool eventFilter( QObject *o, QEvent *e );

private slots:
	/** Slot wywo�ywany w momencie wci�ni�cia klawisza Enter.\n
	 @param pathName - nazwa bie��cej �cie�ki.\n
	 Wysy�any jest tutaj sygna� @em signalPathChanged() z bie��c� �cie�k�.
	 */
	void slotReturnPressed( const QString & pathName );

	/** Slot wywo�ywany w momencie zmiany tre�ci w linijce edycyjnej.\n
	 @param txt - aktualna zawarto�� linijki edycyjnej widoku �cie�ki.\n
	 Uruchamiana jest tu procedura kompletowania �cie�ki, a tak�e wywo�ywany
	 slot informuj�cy o wci�ni�ciu klawisza Enter.
	 */
	void slotTextChanged( const QString & txt );

signals:
	/** Sygna� uruchamiaj�cy zmian� bie��cego katalogu na podany.
	 @param newPath - nowa �cie�ka do odczytania.
	 */
	void signalPathChanged( const QString & newPath );

};

#endif
