/***************************************************************************
                          qtcmd.cpp  -  description
                             -------------------
    begin                : wed oct 23 2002
    copyright            : (C) 2002, 2003 by Piotr Mierzwiński
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

// ----------------------------------
#include "version.h"
#include "qtcmd.h"
#include "helpdialog.h"
#include "messagebox.h"
#include "fileinfoext.h"
#include "qtcmdactions.h"

#include "../icons/qtcmd.xpm"
#include "../icons/storage.xpm"
#include "../icons/settings.xpm"
#include "../icons/favorites.xpm"
#include "../icons/toolbarapp_icons.h"

#include <qdir.h>
#include <qpixmap.h>
#include <qaction.h>
#include <qsettings.h>
#include <qmessagebox.h>
#include <qapplication.h>

#include <qfileinfo.h>


QtCmd::QtCmd( const QString & leftURL, const QString & rightURL )
	: QMainWindow( 0, "QtCmd", WDestructiveClose ), mSettingsWidget(NULL)
{
	setIcon( qtcmd );
	initMainSettings(); // reads and initialize: width and height of main window and size of panels
	initSubMenus(); // submenus for tool and menu bar
	initMenuAndToolBar(); // and buttonBar
	initButtonsBar();

	mMenuBarVisible   ? menuBar()->show()  : menuBar()->hide();
	mToolBarVisible   ? mToolBar->show()   : mToolBar->hide();
	mButtonBarVisible ? mButtonBar->show() : mButtonBar->hide();

	mFilters = NULL;
	mKeyShortcuts = NULL;
	mFilesAssociation = NULL;

	debug("QtCmd::QtCmd\n\tparam. leftURL=%s\n\tparam. rightURL=%s", leftURL.latin1(), rightURL.latin1());
	createPanels( leftURL, (! leftURL.isEmpty()) ? rightURL : QString("") );
}


QtCmd::~QtCmd()
{
	debug("QtCmd, destruktor");
	QSettings *settings = new QSettings();

	// przed zapisem mozna ew. spr. czy jakas z zapisywanych wartosci sie zmienila
	settings->writeEntry( "/qtcmd/General/Width", width() );
	settings->writeEntry( "/qtcmd/General/Height", height() );
	settings->writeEntry( "/qtcmd/General/ShowMenuBar", mMenuBarVisible );
	settings->writeEntry( "/qtcmd/General/ShowToolBar", mToolBarVisible );
	settings->writeEntry( "/qtcmd/General/ShowButtonBar", mButtonBarVisible );
	settings->writeEntry( "/qtcmd/General/FlatButtonBar", mFlatButtonBar );
	settings->writeEntry( "/qtcmd/General/ShowQuitWarning", mShowQuitWarn );

	delete settings;

	if ( mSettingsWidget != NULL )
		delete mSettingsWidget;
	if ( mSettingsLib != NULL ) {
		if ( mSettingsLib->isLoaded() ) {
			mSettingsLib->unload();
			debug("__ unload plugin '%s'", mSettingsLib->library().latin1() );
		}
		delete mSettingsLib;
	}
	if ( mToolBar != NULL )
		delete mToolBar;
	if ( mButtonBar != NULL )
		delete mButtonBar;

	if ( mFilesAssociation != NULL )
		delete mFilesAssociation;
	if ( mKeyShortcuts != NULL )
		delete mKeyShortcuts;
	if ( mFilters != NULL )
		delete mFilters;

	delete mLeftPanel;
	delete mRightPanel;
}


void QtCmd::initMainSettings()
{
	const uint defaultAppWinWidth  = 800;
	const uint defaultAppWinHeight = 600;

	QSettings *settings = new QSettings();

	// ----- reads width i height main window
	int width  = settings->readNumEntry( "/qtcmd/General/Width" );
	int height = settings->readNumEntry( "/qtcmd/General/Height" );

	if ( width <= 600 || width >= QApplication::desktop()->width()+1 )
		width = defaultAppWinWidth;
	if ( height <= 400 || height >= QApplication::desktop()->height()+1 )
		height = defaultAppWinHeight;
	resize( width, height );

	// ----- reads info whether to show or not menu and tool bar
	mMenuBarVisible = settings->readBoolEntry( "/qtcmd/General/ShowMenuBar", TRUE );
	mToolBarVisible = settings->readBoolEntry( "/qtcmd/General/ShowToolBar", TRUE );
	mButtonBarVisible = settings->readBoolEntry( "/qtcmd/General/ShowButtonBar", TRUE );
	mFlatButtonBar = settings->readBoolEntry( "/qtcmd/General/FlatButtonBar", FALSE );
	mShowQuitWarn = settings->readBoolEntry( "/qtcmd/General/ShowQuitWarning", TRUE );

	// ----- reads the panels size
	int widthOfLeftPanel  = settings->readNumEntry( "/qtcmd/LeftPanel/Width" );
	int widthOfRightPanel = settings->readNumEntry( "/qtcmd/RightPanel/Width" );
	if ( !widthOfLeftPanel )
		widthOfLeftPanel = width/2;
	if ( !widthOfRightPanel )
		widthOfRightPanel = width/2;

	if ( widthOfLeftPanel <= widthOfRightPanel && widthOfRightPanel < width )  {
		mWidthOfLeftPanel  = widthOfLeftPanel;
		mWidthOfRightPanel = width-widthOfLeftPanel;
	}
	else
	if ( widthOfLeftPanel > widthOfRightPanel && widthOfLeftPanel < width )  {
		mWidthOfRightPanel = widthOfRightPanel;
		mWidthOfLeftPanel  = width-widthOfRightPanel;
	}
	else {
		mWidthOfLeftPanel  = width/2;
		mWidthOfRightPanel = width/2;
	}

	mPluginsPath = FileInfoExt::filePath( qApp->applicationDirPath() ); // parent dir.for binary file location dir.
	if ( QFileInfo(mPluginsPath+".libs").exists() ) // apps. executed from project
		mPluginsPath += ".libs/plugins/";
	else
		mPluginsPath += "lib/qtcmd/plugins/"; // apps. is installed
	mSettingsLib = new QLibrary( mPluginsPath+"libsettingsapp.so" );
}


void QtCmd::initSubMenus()
{
	// --- init popupMenu with kind of view files panel
	mKindOfVFPmenu = new QPopupMenu();
	mKindOfVFPmenu->setCheckable( TRUE );
// 	mKindOfVFPmenu->insertTearOffHandle();
	mKindOfVFPmenu->insertItem( tr("List View"), this, SLOT(slotChangeToListView()) );
	mKindOfVFPmenu->insertItem( tr("Tree View"), this, SLOT(slotChangeToTreeView()) );

	mProportionsOfPanelsMenu = new QPopupMenu();
	mProportionsOfPanelsMenu->setCheckable( TRUE );
	mProportionsOfPanelsMenu->insertItem( "50/50 %", PP_5050 );
	mProportionsOfPanelsMenu->insertItem( "30/70 %", PP_3070 );
	mProportionsOfPanelsMenu->insertItem( "70/30 %", PP_7030 );
	mProportionsOfPanelsMenu->insertItem( tr("User defined"), PP_USER );
	mProportionsOfPanelsMenu->setItemChecked( PP_USER, TRUE ); // default

	int currHeight = mProportionsOfPanelsMenu->sizeHint().height();
	int actionH = currHeight/mProportionsOfPanelsMenu->count();
	QToolTip::add( mProportionsOfPanelsMenu,
	 QRect( 0, actionH*4, mProportionsOfPanelsMenu->width(), actionH ),
	 tr("width of left panel") +" = "+QString::number(mWidthOfLeftPanel)+"\n"+
	 tr("width of right panel")+" = "+QString::number(mWidthOfRightPanel)
	);

	connect( mProportionsOfPanelsMenu, SIGNAL(activated( int )), this, SLOT(slotChangeProportionOfPanels( int )) );
}


void QtCmd::initMenuAndToolBar()
{
	// --- main menu
	QPopupMenu *fileMenu     = new QPopupMenu( this );
	QPopupMenu *settingsMenu = new QPopupMenu( this );
	QPopupMenu *toolsMenu    = new QPopupMenu( this );
	QPopupMenu *helpMenu     = new QPopupMenu( this );

	mViewMenu = new QPopupMenu( this );
	mViewMenu->setCheckable( TRUE );

	menuBar()->insertItem( tr("&File"),     fileMenu );
	menuBar()->insertItem( tr("&View"),     mViewMenu );
	menuBar()->insertItem( tr("&Settings"), settingsMenu );
	menuBar()->insertItem( tr("&Tools"),    toolsMenu );
	menuBar()->insertItem( tr("&Help"),     helpMenu );

// 	mOpenFileA = new QAction( QPixmap((const char**)fileOpen), tr("Open &file"), 0, this );
// 	mOpenDirA = new QAction( QPixmap((const char**)fileOpen), tr("Open &directory"), CTRL+Key_O, this );
	mQuitAppsA = new QAction( tr("Quit an application"), 0, this );
	mMenuBarA = new QAction( tr("Show/Hide &menu bar"), 0, this );
	mToolBarA = new QAction( tr("Show/Hide &tool bar"), 0, this );
	mBtnBarA  = new QAction( tr("Show/Hide &button bar"), 0, this );
	mConfigureA = new QAction( QPixmap((const char**)settings), tr("Configure ")+" QtCommander", 0, this );
	mFtpMmngA = new QAction( QPixmap((const char**)ftptool), tr("FTP connection manager"), 0, this );
	mFreeSpaceA = new QAction( QPixmap((const char**)freespace), tr("Show free space on current disk"), 0, this );
	mStorageA = new QAction( QPixmap((const char**)storage), tr("Show storage devices list"), 0, this );
	mRereadA = new QAction( QPixmap((const char**)reread), tr("Reread current directory"), 0, this );
	mFavoritesA = new QAction( QPixmap((const char**)favorites), tr("Show favorites"), 0, this );
	mFindFilesA = new QAction( QPixmap((const char**)findfile), tr("Fin&d file"), 0, this );
	mQuickGetOutA = new QAction( QPixmap((const char**)quickgetout), tr("Quick get out from file system"), 0, this );
	//mConfViewA = new QAction( QPixmap((const char**)settings), tr("Configure file &view"), 0, this );

	mMenuBarA->setToggleAction( TRUE );
	mToolBarA->setToggleAction( TRUE );
	mBtnBarA->setToggleAction( TRUE );
	mMenuBarA->setOn( mMenuBarVisible );
	mToolBarA->setOn( mToolBarVisible );
	mBtnBarA->setOn( mButtonBarVisible );

// 	connect( mOpenFileA, SIGNAL(activated()), this, SLOT(slotOpenFile()) );
// 	connect( mOpenDirA, SIGNAL(activated()), this, SLOT(slotOpenDirectory()) );
	connect( mQuitAppsA, SIGNAL(activated()), this, SLOT(slotQuit()) );
	connect( mMenuBarA, SIGNAL(activated()), this, SLOT(slotShowHideMenuBar()) );
	connect( mToolBarA, SIGNAL(activated()), this, SLOT(slotShowHideToolBar()) );
	connect( mBtnBarA, SIGNAL(activated()), this, SLOT(slotShowHideButtonBar()) );
	connect( mConfigureA, SIGNAL(activated()), this, SLOT(slotShowSettings()) );
	connect( mFtpMmngA, SIGNAL(activated()), this, SIGNAL(signalShowFtpManagerDlg()) );
	connect( mFreeSpaceA, SIGNAL(activated()), this, SIGNAL(signalShowFreeSpace()) );
	connect( mStorageA, SIGNAL(activated()), this, SIGNAL(signalDevicesList()) );
	connect( mRereadA, SIGNAL(activated()), this, SIGNAL(signalRereadCurrentDir()) );
	connect( mFavoritesA, SIGNAL(activated()), this, SIGNAL(signalShowFavorites()) );
	connect( mFindFilesA, SIGNAL(activated()), this, SIGNAL(signalFindFile()) );
	connect( mQuickGetOutA, SIGNAL(activated()), this, SIGNAL(signalQuickGetOutFromFS()) );

// 	mOpenFileA->addTo( fileMenu );
// 	mOpenDirA->addTo( fileMenu );
	mQuitAppsA->addTo( fileMenu );

	mMenuBarA->addTo( mViewMenu );
	mToolBarA->addTo( mViewMenu );
	mBtnBarA->addTo( mViewMenu );
	mViewMenu->insertSeparator();
	mViewMenu->insertItem( tr("Kind of &view for current panel"), mKindOfVFPmenu );
	mViewMenu->insertItem( tr("&Proportions of panels"), mProportionsOfPanelsMenu );

	mConfigureA->addTo( settingsMenu );

	mFtpMmngA->addTo( toolsMenu );
	mStorageA->addTo( toolsMenu );
	mFreeSpaceA->addTo( toolsMenu );
	mFavoritesA->addTo( toolsMenu );
	mFindFilesA->addTo( toolsMenu );

	helpMenu->insertItem( tr("&About"), this, SLOT(about()) );
	helpMenu->insertItem( tr("About &Qt"), this, SLOT(aboutQt()) );
	helpMenu->insertItem( tr("&Help"), this, SLOT(slotShowHelp()), Key_F1 );

	// --- init toolBar
	mToolBar = new QToolBar( this );

	// --- init buttons
	mChangeKindOfViewToolBtn = new QToolButton( QPixmap((const char**)treeview), tr("Change kind of list view"), 0, this, 0, mToolBar );
	mFtpMmngA->addTo( mToolBar );
	mRereadA->addTo( mToolBar );
	mFindFilesA->addTo( mToolBar );
	mProportionsBtn = new QToolButton( QPixmap((const char**)proportions), tr("Proportions of panels"), 0, this, 0, mToolBar );
	mFreeSpaceA->addTo( mToolBar );
	mStorageA->addTo( mToolBar );
	mFavoritesA->addTo( mToolBar );
	mQuickGetOutA->addTo( mToolBar );
	mConfigureA->addTo( mToolBar );

	mChangeKindOfViewToolBtn->setPopupDelay( 0 ); // popupMenu is initiated into QtCmd::createPanels
	mProportionsBtn->setPopup( mProportionsOfPanelsMenu );
	mProportionsBtn->setPopupDelay( 0 );

	connect( mProportionsBtn, SIGNAL(clicked()), this, SLOT(slotChangeProportionOfPanels()) );

	// --- need to resize tool bar to width of application window
	QToolButton *emptyBtn = new QToolButton( mToolBar );
	emptyBtn->setEnabled( FALSE );
	mToolBar->setStretchableWidget( emptyBtn );
}


void QtCmd::initButtonsBar()
{
	const uint buttonsNum = 10;
	mButtonBar = new QToolBar( this );
	moveDockWindow( mButtonBar, Qt::DockBottom );

	QString buttonsLabel[buttonsNum] = {
	 "F1 "+tr("Help"), "F2 "+tr("Change view"), "F3 "+tr("View"), "F4 "+tr("Edit"), "F5 "+tr("Copy"),
	 "F6 "+tr("Move"), "F7 "+tr("Make dir"), "F8 "+tr("Delete"), "F9 "+tr("Make a link"), "F10 "+tr("Quit")
	};
	const char *buttonSlot[] = {
		SLOT(slotShowHelp()),   // F1
		SLOT(slotChangeView()), // F2
		SLOT(slotFileView()),   // F3
		SLOT(slotFileEdit()),   // F4
		SLOT(slotCopy()),       // F5
		SLOT(slotMove()),       // F6
		SLOT(slotMakeDir()),    // F7
		SLOT(slotDelete()),     // F8
		SLOT(slotMakeLink()),   // F9
		SLOT(slotQuit())        // F10
	};
	QPushButton *btn;
	QFont boldFont;   boldFont.setWeight( QFont::Bold );

	for ( uint i=0; i<buttonsNum; i++ ) {
		btn = new QPushButton( buttonsLabel[i], mButtonBar );
		btn->setFocusPolicy( QWidget::NoFocus );
		btn->setFlat( mFlatButtonBar );
		btn->setFont( boldFont );
		mButtonBar->addSeparator();
		mButtonsTab[i] = btn;
		connect( btn, SIGNAL(clicked()), this, buttonSlot[i] );
		if ( i == buttonsNum-1 ) // F10
			btn->setAccel( Key_F10 );
	}
	// TODO dostosowywac dlugosci etykiet na przyciskach do szerokosci okna aplikacji
	// mozna wyciac znaki '- '
}


void QtCmd::initKeyShortcuts()
{
	mKeyShortcuts = new KeyShortcuts();

	mKeyShortcuts->append( Help_APP, tr("Help"), Qt::Key_F1 );
	mKeyShortcuts->append( ViewFile_APP, tr("View file"), Qt::Key_F3 );
	mKeyShortcuts->append( EditFile_APP, tr("Edit file"), Qt::Key_F4 );
	mKeyShortcuts->append( CopyFiles_APP, tr("Copy files"), Qt::Key_F5 );
	mKeyShortcuts->append( MoveFiles_APP, tr("Move files"), Qt::Key_F6 );
	mKeyShortcuts->append( MakeDirectory_APP, tr("Make directory"), Qt::Key_F7 );
	mKeyShortcuts->append( DeleteFiles_APP, tr("Delete files"), Qt::Key_F8 );
	mKeyShortcuts->append( MakeAnEmptyFile_APP, tr("Make an empty file"), Qt::SHIFT+Qt::Key_F4 );
	mKeyShortcuts->append( CopyFilesToCurrentDirectory_APP, tr("Copy files to current directory"), Qt::SHIFT+Qt::Key_F5 );
	mKeyShortcuts->append( QuickFileRename_APP, tr("Quick file rename"), Qt::SHIFT+Qt::Key_F6 );
	mKeyShortcuts->append( QuickChangeOfFileModifiedDate_APP, tr("Quick change of file modified date"), Qt::SHIFT+Qt::Key_F7 );
	mKeyShortcuts->append( QuickChangeOfFilePermission_APP, tr("Quick change of file permission"), Qt::SHIFT+Qt::Key_F8 );
	mKeyShortcuts->append( QuickChangeOfFileOwner_APP, tr("Quick change of file owner"), Qt::SHIFT+Qt::Key_F9 );
	mKeyShortcuts->append( QuickChangeOfFileGroup_APP, tr("Quick change of file group"), Qt::SHIFT+Qt::Key_F10 );
	mKeyShortcuts->append( GoToUpLevel_APP, tr("Go to up level"),Qt::Key_Backspace );
	mKeyShortcuts->append( RefreshDirectory_APP, tr("Refresh directory"), Qt::CTRL+Qt::Key_R );
	mKeyShortcuts->append( MakeALink_APP, tr("Make a link"), Qt::CTRL+Qt::Key_L );
	mKeyShortcuts->append( EditCurrentLink_APP, tr("Edit current link"), Qt::CTRL+Qt::SHIFT+Qt::Key_L );
//	mKeyShortcuts->append( InvertSelection_APP, tr("Invert current selection"), Qt::Key_Asterisk );
	mKeyShortcuts->append( SelectFilesWithThisSameExtention_APP, tr("Select files with this same extention"), Qt::ALT+Qt::Key_Plus );
	mKeyShortcuts->append( DeselectFilesWithThisSameExtention_APP, tr("Deselect files with this same extention"), Qt::ALT+Qt::Key_Minus );
	mKeyShortcuts->append( SelectAllFiles_APP, tr("Select all files"), Qt::CTRL+Qt::Key_Plus );
	mKeyShortcuts->append( DeselectAllFiles_APP, tr("Deselect all files"), Qt::CTRL+Qt::Key_Minus );
	mKeyShortcuts->append( SelectGroupOfFiles_APP, tr("Select group of files"), Qt::Key_Plus );
	mKeyShortcuts->append( DeselectGroupOfFiles_APP, tr("Deselect group of files"), Qt::Key_Minus );
	mKeyShortcuts->append( ChangeViewToList_APP, tr("Change view to list"),Qt::ALT+Qt::CTRL+Qt::Key_I );
	mKeyShortcuts->append( ChangeViewToTree_APP, tr("Change view to tree"), Qt::ALT+Qt::CTRL+Qt::Key_T );
	mKeyShortcuts->append( QuickFileSearch_APP, tr("Quick file search"), Qt::CTRL+Qt::Key_S );
	mKeyShortcuts->append( ShowHistoryMenu_APP, tr("Show history's menu"), Qt::CTRL+Qt::Key_H );
	mKeyShortcuts->append( ShowHistory_APP, tr("Show history"), Qt::ALT+Qt::Key_Down );
	mKeyShortcuts->append( ShowFilters_APP, tr("Show filters menu"), Qt::ALT+Qt::Key_Up );
	mKeyShortcuts->append( GoToPreviousURL_APP, tr("Go to previous URL"), Qt::CTRL+Qt::Key_Up );
	mKeyShortcuts->append( GoToNextURL_APP, tr("Go to next URL"), Qt::CTRL+Qt::Key_Down );
	mKeyShortcuts->append( GoToHomeDirectory_APP, tr("Go to home directory"), Qt::CTRL+Qt::Key_Home );
	mKeyShortcuts->append( MoveCursorToPathView_APP, tr("Move cursor to path view"), Qt::ALT+Qt::CTRL+Qt::Key_P );
	mKeyShortcuts->append( ShowFtpManager_APP, tr("Show FTP manager"), Qt::ALT+Qt::CTRL+Qt::Key_C );
	mKeyShortcuts->append( ShowFiltersManager_APP, tr("Show filters manager"), Qt::ALT+Qt::CTRL+Qt::Key_F );
	mKeyShortcuts->append( ShowStorageDevicesList_APP, tr("Show storage devices list"), Qt::CTRL+Qt::Key_D );
	mKeyShortcuts->append( ShowFreeSpace_APP, tr("Show free space"), Qt::CTRL+Qt::Key_F );
	mKeyShortcuts->append( ShowFavorites_APP, tr("Show favorites"), Qt::CTRL+Qt::Key_Backslash );
	mKeyShortcuts->append( FindFile_APP, tr("Find file"), Qt::ALT+Qt::Key_F7 );
	mKeyShortcuts->append( QuickGoOutFromCurrentFileSystem_APP, tr("Quick go out from current file system"), Qt::CTRL+Qt::Key_Backspace );
	mKeyShortcuts->append( OpenCurrentFileOrDirectoryInLeftPanel_APP, tr("Open current file or directory in left panel"), Qt::CTRL+Qt::Key_Left );
	mKeyShortcuts->append( OpenCurrentFileOrDirectoryInRightPanel_APP, tr("Open current file or directory in right panel"), Qt::CTRL+Qt::Key_Right );
	mKeyShortcuts->append( ForceOpenCurrentDirectoryInLeftPanel_APP, tr("Force open current directory in left panel"), Qt::ALT+Qt::CTRL+Qt::Key_Left );
	mKeyShortcuts->append( ForceOpenCurrentDirectoryInRightPanel_APP, tr("Force open current directory in right panel"), Qt::ALT+Qt::CTRL+Qt::Key_Right );
	mKeyShortcuts->append( WeighCurrentDirectory_APP, tr("Weigh current directory"), Qt::Key_Space );
	mKeyShortcuts->append( ShowSettingsOfApplication_APP, tr("Show settings of application"), Qt::ALT+Qt::CTRL+Qt::Key_S );
	mKeyShortcuts->append( ShowHideMenuBar_APP, tr("Show/hide menu bar"), Qt::CTRL+Qt::Key_M );
	mKeyShortcuts->append( ShowHideToolBar_APP, tr("Show/hide tool bar"), Qt::CTRL+Qt::Key_T );
	mKeyShortcuts->append( ShowHideButtonBar_APP, tr("Show/hide buttons bar"), Qt::CTRL+Qt::Key_B );
	mKeyShortcuts->append( ShowProperties_APP, tr("Show file properties dialog"), Qt::ALT+Qt::Key_Return );
	mKeyShortcuts->append( QuitApps_APP, tr("Quit an application"), Qt::CTRL+Qt::Key_Q );

	mKeyShortcuts->updateEntryList( "/qtcmd/FilesPanelShortcuts/" ); // update from config file
	slotApplyKeyShortcuts( mKeyShortcuts ); // for actions in current window
}


void QtCmd::createPanels( const QString & leftURL, const QString & rightURL )
{
	mSplitter = new QSplitter( Qt::Horizontal, this, "Spliter" );

	// name of panel is need to correct intialize view in the 'Panel' class ( initPanelView() )
	mLeftPanel  = new Panel( mWidthOfLeftPanel,  height(), mSplitter, this, "LeftPanel" );
	mRightPanel = new Panel( mWidthOfRightPanel, height(), mSplitter, this, "RightPanel" );

	mLeftPanel->createPanel( UNKNOWNpanel, leftURL );
	mRightPanel->createPanel( UNKNOWNpanel, rightURL );

	KindOfPanel eKindOfLeftPanel  = mLeftPanel->kindOfPanel();
	KindOfPanel eKindOfRightPanel = mRightPanel->kindOfPanel();

	if ( eKindOfLeftPanel == FILESpanel )
		mChangeKindOfViewToolBtn->setPopup( mKindOfVFPmenu );
	else
		mChangeKindOfViewToolBtn->setPopup( 0 ); // non popupMenu


	mSplitter->resize( width(), height() );
	mSplitter->setOpaqueResize( TRUE ); // during size changing shows contains of panels
	mSplitter->setHandleWidth( 3 ); // default is 6
	mSplitter->show();
	setCentralWidget( mSplitter );

	initKeyShortcuts(); // create and init obj.
	if ( mKeyShortcuts != NULL ) {
		mLeftPanel->setKeyShortcuts( mKeyShortcuts );
		mRightPanel->setKeyShortcuts( mKeyShortcuts );
	}

	mFilesAssociation = new FilesAssociation(); // load the files association
	if ( mFilesAssociation )
		if ( mFilesAssociation->loaded() ) {
			mLeftPanel->setFilesAssociation( mFilesAssociation );
			mRightPanel->setFilesAssociation( mFilesAssociation );
		}
	mFilters = new Filters( mFilesAssociation );
	slotApplyFilters( mFilters );

	// ---- we are need to open a file
	// need to use a file load mechanizm from the panel beside
	// ! a problem is when panel beside have file system other than current (contains file to open)
	if ( eKindOfLeftPanel == FILESpanel && eKindOfRightPanel == FILEVIEWpanel )
		mRightPanel->initLoadFileToView( mFilesAssociation, mLeftPanel->filesPanel() );
	else
	if ( eKindOfRightPanel == FILESpanel && eKindOfLeftPanel == FILEVIEWpanel )
		mLeftPanel->initLoadFileToView( mFilesAssociation, mRightPanel->filesPanel() );
}


uint QtCmd::currentPanel()
{
	QString senderName = sender()->name();
	CurrentPanel currPanel = NOpanel;

	if ( senderName.find("Left") != -1 )
		currPanel = LEFTpanel;
	else
	if ( senderName.find("Right") != -1 )
		currPanel = RIGHTpanel;

	return (uint)currPanel;
}


void QtCmd::about()
{
	QMessageBox::about( this, "QtCmd",
	 "<H3><CENTER>QtCommander "VERSION" </CENTER></H3><P>"
	 +tr("Twin panels files manager")
	 +"<P>(C) 2002, "AUTHOR", "AUTHORMAIL
	 +"<P><I>"+tr("Distributed under the terms of the GNU General Public License v2")+"</I>"
	);
}


void QtCmd::slotApplyKeyShortcuts( KeyShortcuts *keyShortcuts )
{
	if ( ! keyShortcuts )
		return;

	mKeyShortcuts = keyShortcuts;

	mQuitAppsA->setAccel( keyShortcuts->key(QuitApps_APP) );
	mMenuBarA->setAccel( keyShortcuts->key(ShowHideMenuBar_APP) );
	mToolBarA->setAccel( keyShortcuts->key(ShowHideToolBar_APP) );
	mBtnBarA->setAccel( keyShortcuts->key(ShowHideButtonBar_APP) );
	mConfigureA->setAccel( keyShortcuts->key(ShowSettingsOfApplication_APP) );
	mFtpMmngA->setAccel( keyShortcuts->key(ShowFtpManager_APP) );
	mFreeSpaceA->setAccel( keyShortcuts->key(ShowFreeSpace_APP) );
	mStorageA->setAccel( keyShortcuts->key(ShowStorageDevicesList_APP) );
	mRereadA->setAccel( keyShortcuts->key(RefreshDirectory_APP) );
	mFavoritesA->setAccel( keyShortcuts->key(ShowFavorites_APP) );
	mFindFilesA->setAccel( keyShortcuts->key(FindFile_APP) );
	mQuickGetOutA->setAccel( keyShortcuts->key(QuickGoOutFromCurrentFileSystem_APP) );
}


void QtCmd::slotApplyFilters( Filters * filters )
{
	if ( filters ) {
		mLeftPanel->setFilters( filters );
		mRightPanel->setFilters( filters  );
	}
}

		// ---------- SLOTs ------------

void QtCmd::aboutQt()
{
	QMessageBox::aboutQt( this, "QtCmd" );
}


void QtCmd::slotQuit()
{
	if ( mShowQuitWarn ) {
		int result = MessageBox::yesNo( this,
		 tr("Quit an application")+" - QtCommander",
		 tr("Do you really want to quit")+" ?",
		 MessageBox::No
		);
		if ( result == MessageBox::Yes )
			qApp->closeAllWindows();
	}
	else
		qApp->closeAllWindows();
}


void QtCmd::slotShowHelp()
{
	HelpDialog *dlg = new HelpDialog();

	dlg->setCaption( tr("Help")+" - QtCommander" );
	dlg->resize( 530, 610 );
//	dlg->setMinimumSize( 530, 610 );
//	dlg->setMaximumSize( 530, 610 );
	dlg->exec();

	delete dlg;
	dlg = NULL;
}


void QtCmd::slotChangeView()
{
	KindOfListView kindOfLV = UNKNOWNpview;

	if ( mLeftPanel->hasFocus() ) {
		if ( mLeftPanel->kindOfPanel() != FILESpanel )
			return;
		kindOfLV = (KindOfListView)mLeftPanel->kindOfView();
	}
	else {
	if ( mRightPanel->hasFocus() )
		if ( mRightPanel->kindOfPanel() != FILESpanel )
			return;
		kindOfLV = (KindOfListView)mRightPanel->kindOfView();
	}

	emit signalSetNewKindOfView( (kindOfLV == TREEpview) ? LISTpview : TREEpview );
}


void QtCmd::slotFileView()
{
	if ( mLeftPanel->hasFocus() )
		mLeftPanel->viewFile( FALSE );
	else
	if ( mRightPanel->hasFocus() )
		mRightPanel->viewFile( FALSE );
}

void QtCmd::slotFileEdit()
{
	if ( mLeftPanel->hasFocus() )
		mLeftPanel->viewFile( TRUE );
	else
	if ( mRightPanel->hasFocus() )
		mRightPanel->viewFile( TRUE );
}

void QtCmd::slotCopy()
{
	if ( mLeftPanel->hasFocus() )
		mLeftPanel->copyFiles( FALSE );
	else
	if ( mRightPanel->hasFocus() )
		mRightPanel->copyFiles( FALSE );
}

void QtCmd::slotMove()
{
	if ( mLeftPanel->hasFocus() )
		mLeftPanel->copyFiles( TRUE );
	else
	if ( mRightPanel->hasFocus() )
		mRightPanel->copyFiles( TRUE );
}

void QtCmd::slotMakeDir()
{
	if ( mLeftPanel->hasFocus() )
		mLeftPanel->makeDir();
	else
	if ( mRightPanel->hasFocus() )
		mRightPanel->makeDir();
}

void QtCmd::slotDelete()
{
	if ( mLeftPanel->hasFocus() )
		mLeftPanel->deleteFile();
	else
	if ( mRightPanel->hasFocus() )
		mRightPanel->deleteFile();
}


void QtCmd::slotMakeLink()
{
	if ( mLeftPanel->hasFocus() )
		mLeftPanel->makeLink();
	else
	if ( mRightPanel->hasFocus() )
		mRightPanel->makeLink();
}


void QtCmd::slotHeaderSizeChange( int section, int oldSize, int newSize )
{
	CurrentPanel currPanel = (CurrentPanel)currentPanel();

	if ( currPanel == LEFTpanel )
		mRightPanel->resizeSection( section, oldSize, newSize );
	else
	if ( currPanel == RIGHTpanel )
		mLeftPanel->resizeSection( section, oldSize, newSize );
}


void QtCmd::slotShowHideMenuBar()
{
	mMenuBarVisible = ! mMenuBarVisible;
	mMenuBarVisible ? menuBar()->show() : menuBar()->hide();
}


void QtCmd::slotShowHideToolBar()
{
	mToolBarVisible = ! mToolBarVisible;
	mToolBarVisible ? mToolBar->show() : mToolBar->hide();
}


void QtCmd::slotShowHideButtonBar()
{
	mButtonBarVisible = ! mButtonBarVisible;
	mButtonBarVisible ? mButtonBar->show() : mButtonBar->hide();
}


void QtCmd::slotChangeToListView()
{
	emit signalSetNewKindOfView( LISTpview );
}


void QtCmd::slotChangeToTreeView()
{
	emit signalSetNewKindOfView( TREEpview );
}


void QtCmd::slotChangeProportionOfPanels( int proportionId )
{
	debug("QtCmd::slotChangeProportionOfPanels");
	if ( proportionId < PP_5050 || proportionId > PP_USER )
		return;

	// menu must be exclusive
	for( uint i=PP_5050; i<PP_USER+1; i++ ) // 4 panel's proporions
		mProportionsOfPanelsMenu->setItemChecked( i, FALSE );
	mProportionsOfPanelsMenu->setItemChecked( proportionId, TRUE );

	uint leftPanelWidth, rightPanelWidth;

	switch ( proportionId )
	{
		case PP_5050:
			leftPanelWidth  = width()*50/100;
			rightPanelWidth = width()*50/100;
		break;

		case PP_3070:
			leftPanelWidth  = width()*30/100;
			rightPanelWidth = width()*70/100;
		break;

		case PP_7030:
			leftPanelWidth  = width()*70/100;
			rightPanelWidth = width()*30/100;
		break;

		case PP_USER:
			leftPanelWidth  = mWidthOfLeftPanel;
			rightPanelWidth = mWidthOfRightPanel;
		break;

		default:
		break;
	}

	static QValueList <int>panelsWidth; // width of 2 spliter obj.
	panelsWidth.clear();
	panelsWidth.append( leftPanelWidth );
	panelsWidth.append( rightPanelWidth );
	mSplitter->setSizes( panelsWidth );
}


void QtCmd::slotShowSettings()
{
	if ( mSettingsLib->isLoaded() ) // if a plugin is loaded then only show a widget.
		if ( mSettingsWidget ) {
			mSettingsWidget->show();
			return;
		}

	if ( ! QFileInfo(mPluginsPath+"libsettingsapp.so").exists() ) {
		MessageBox::critical( this,
		 tr("Plugins path")+" : \""+mPluginsPath+"\"\n\n"+
		 tr("Plugin")+" 'libsettingsapp.so' "+
		 tr("not exists in foregoing location !") );
		return;
	}

	// load and init a plugin
	typedef QWidget *(*t_func)(QWidget *, const char *); // make type of init's function
	t_func create = (t_func)mSettingsLib->resolve( "create_widget" ); // resolve symbol and init fun.address

	if ( ! mSettingsLib->isLoaded() ) {
		debug("plugin \""+mPluginsPath+"libsettingsapp.so\" has'nt been loaded !");
		return;
	}
	if ( create ) {
		debug("symbol resolved, let's create the settings dialog");
		mSettingsWidget = create( 0,0 );

		if ( ! mSettingsWidget ) {
			debug("Cannot create the settings dialog");
			return;
		}
		connect( mSettingsWidget, SIGNAL(signalApply(const ListViewSettings &)),
		 this, SIGNAL(signalApplyListViewSettings(const ListViewSettings &)) );
		connect( mSettingsWidget, SIGNAL(signalApply(const FileSystemSettings &)),
		 this, SIGNAL(signalApplyFileSystemSettings(const FileSystemSettings &)) );
		connect( mSettingsWidget, SIGNAL(signalApply(const OtherAppSettings &)),
		 this, SIGNAL(signalApplyOtherAppSettings(const OtherAppSettings &)) );
		connect( mSettingsWidget, SIGNAL(signalApplyKeyShortcuts(KeyShortcuts *)),
		 this, SLOT(slotApplyKeyShortcuts(KeyShortcuts *)) );
		// --- init settings dlg.
		connect( this, SIGNAL(signalSetKeyShortcuts(KeyShortcuts *)),
		 mSettingsWidget, SLOT(slotSetKeyShortcuts(KeyShortcuts *)) );
		emit signalSetKeyShortcuts( mKeyShortcuts );
		connect( this, SIGNAL(signalSetFilesAssociation(FilesAssociation *)),
		 mSettingsWidget, SLOT(slotSetFilesAssociation(FilesAssociation *)) );
		emit signalSetFilesAssociation( mFilesAssociation );

		mSettingsWidget->show();
	}
	else
		debug("symbol NOT resolved");
}


void QtCmd::slotShowConfigureToolBarDlg()
{
}


void QtCmd::slotSetPanelBesideURL( const QString & url )
{
	CurrentPanel currPanel = (CurrentPanel)currentPanel();

	if ( currPanel == LEFTpanel )
		mRightPanel->setPanelBesideURL( url );
	else
	if ( currPanel == RIGHTpanel )
		mLeftPanel->setPanelBesideURL( url );
}


void QtCmd::slotOpenIntoPanelBeside( const QString & fileName )
{
	CurrentPanel currPanel = (CurrentPanel)currentPanel();
	QString fName = fileName;

	if ( fName.at(fName.length()-1) == '/' ) { // let's open directory
		fName.remove( fileName.find(".."), 3 );

		if ( currPanel == LEFTpanel ) {
			if ( mRightPanel->kindOfPanel() == FILESpanel )
				mRightPanel->changeDirectory( fName );
			else
				mRightPanel->createPanel( FILESpanel, fName );
		}
		else
		if ( currPanel == RIGHTpanel ) {
			if ( mLeftPanel->kindOfPanel() == FILESpanel )
				mLeftPanel->changeDirectory( fName );
			else
				mLeftPanel->createPanel( FILESpanel, fName );
		}
		return;
	}
	// ---- we are need to open a file
	// need to use a file load mechanizm from the panel beside
	// ! a problem is when panel beside have file system other than current (contains file to open)
	if ( currPanel == LEFTpanel ) {
		mRightPanel->createPanel( FILEVIEWpanel, fName );
		mRightPanel->initLoadFileToView( mFilesAssociation, mLeftPanel->filesPanel() );
	}
	else
	if ( currPanel == RIGHTpanel ) {
		mLeftPanel->createPanel( FILEVIEWpanel, fName );
		mLeftPanel->initLoadFileToView( mFilesAssociation, mRightPanel->filesPanel() );
	}
}

void QtCmd::slotCloseView()
{
	CurrentPanel currPanel = (CurrentPanel)currentPanel();
	QString panelBesideUrl;

	if ( currPanel == LEFTpanel ) {
		panelBesideUrl = mRightPanel->currentURL();
		mLeftPanel->createPanel( FILESpanel );
		mLeftPanel->setPanelBesideURL( panelBesideUrl );
		mLeftPanel->setFilesAssociation( mFilesAssociation );
		mLeftPanel->setKeyShortcuts( mKeyShortcuts );
		mLeftPanel->setFilters( mFilters );
	}
	else
	if ( currPanel == RIGHTpanel ) {
		panelBesideUrl = mLeftPanel->currentURL();
		mRightPanel->createPanel( FILESpanel );
		mRightPanel->setPanelBesideURL( panelBesideUrl );
		mRightPanel->setFilesAssociation( mFilesAssociation );
		mRightPanel->setKeyShortcuts( mKeyShortcuts );
		mRightPanel->setFilters( mFilters  );
	}
}


void QtCmd::slotUpdateItemOnLV( const QString & fileName )
{
	//slotUpdateSecondList( Open, fileName, "" );
	// FIXME uaktulniac liste przy podgladzie pliku osadzonym w panelu
	// wykorzystywany przez osadzony w panelu podgladu pliku (w trybie edycji, zapis, uaktualnienie el.na liscie w panelu obok)
}

/*
void QtCmd::slotUpdateSecondList( Operation operation, const QString & fileName, const QString & newFileName )
{
	CurrentPanel currPanel = (CurrentPanel)currentPanel();

	if ( currPanel == LEFTpanelIsCURRENT )
		mRightPanel->updateList( operation, fileName, newFileName );
	else
	if ( currPanel == RIGHTpanelIsCURRENT )
		mLeftPanel->updateList( operation, fileName, newFileName );
}
*/

void QtCmd::slotCopyFiles( const QStringList & filesList, bool move )
{
	CurrentPanel currPanel = (CurrentPanel)currentPanel();

	if ( currPanel == LEFTpanel )
		mRightPanel->copyFiles( filesList, move );
	else
	if ( currPanel == RIGHTpanel )
		mLeftPanel->copyFiles( filesList, move );
}


void QtCmd::slotGetKindOfFSInPanelBeside( KindOfFilesSystem & kindOfFS )
{
	CurrentPanel currPanel = (CurrentPanel)currentPanel();

	if ( currPanel == LEFTpanel )
		mRightPanel->getKindOfFS( kindOfFS );
	else
	if ( currPanel == RIGHTpanel )
		mLeftPanel->getKindOfFS( kindOfFS );
}


void QtCmd::slotUpdateSecondPanel( int updateListOp, QValueList<QListViewItem *> *itemsList, const UrlInfoExt & newUrlInfo )
{
	CurrentPanel currPanel = (CurrentPanel)currentPanel();

	if ( currPanel == LEFTpanel )
		mRightPanel->updateList( updateListOp, itemsList, newUrlInfo );
	else
	if ( currPanel == RIGHTpanel )
		mLeftPanel->updateList( updateListOp, itemsList, newUrlInfo );
}


void QtCmd::slotGetSILfromPanelBeside( QValueList<QListViewItem *> *& selectedItemsList )
{
	CurrentPanel currPanel = (CurrentPanel)currentPanel();

	if ( currPanel == LEFTpanel )
		mRightPanel->getSelectedItemsList( selectedItemsList );
	else
	if ( currPanel == RIGHTpanel )
		mLeftPanel->getSelectedItemsList( selectedItemsList );
}


void QtCmd::resizeEvent( QResizeEvent * )
{
	setMinimumSize( 600, 400 );
}
