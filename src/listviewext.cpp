/***************************************************************************
                          listviewext.cpp  -  description
                             -------------------
    begin                : Mon Jul 9 2001
    copyright            : (C) 2001 by Mariusz Borowski (init)
                         : (C) 2002 by Piotr Mierzwiński (develop)
    email                : mcsoft@softhome.net, peterm@go2.pl

    copyright            : See COPYING file that comes with this project

$Id$
 ***************************************************************************/

#include "listviewext.h"
#include "fileinfoext.h" // for static fun.: specialFile()
#include "functions.h" // for static functions
#include "virtualfs.h" // for getUser()

#include "../icons/files_icons.h"
#include "../icons/specf_icons.h"
#include "../icons/cdup.xpm"
#include "../icons/folder.xpm"
#include "../icons/folder_lock.xpm"
#include "../icons/folder_link.xpm"
#include "../icons/broken_link.xpm"
#include "../icons/file_link.xpm"

#include <qtimer.h>
#include <qcursor.h>
#include <qpixmap.h>
#include <qregexp.h>
#include <qpainter.h>
#include <qapplication.h>


static QString sDevPath = "/dev/";


ListViewExt::ListViewExt( QWidget *preParent, QWidget *parent, bool popupMenu, bool toolTip, bool renameBox, bool findDlg, const char *name )
	: QListView( preParent, name ), mParent(parent)
{
	mToolTip = NULL;
	mPopupMenu = NULL;
	mScrollTimer = NULL;
	mListViewEditBox = NULL;
	mListViewFindItem = NULL;

	mNumOfSelectedItems = 0;  mWeightOfSelected = 0;
	mNumOfAllItemsPB = 0; mInsertedItemsCounter = 0;
	mKindOfView = UNKNOWNpview;
	mSelectFilesOnly = TRUE;
	mInvSelection = TRUE;
	mCurrentURL = "";

	setName( QString(parent->name())+"_LV" );
	mGridColor = Qt::gray;
	mFullCursorColor = Qt::darkBlue;
	setBackgroundColor( Qt::white );
	mSecondColorOfBg = QColor( 11, 240, 11 ); // paletteBackgroundColor().dark(110);

	mDragging = FALSE;
	mShowGrid = FALSE;
	mFullCursor = TRUE;
	m_bShowHidden = TRUE;
	mDblFrameCursor = FALSE;
	mEnableTwoColorsOfBg = FALSE;
	mKindOfFormating = NONEformat;
	mColumnsList.clear();

	setFullCursor();
	setShowIcons( FALSE );
	setCursorColor( darkBlue );
	setFontList( "Helvetica", 10 ); // default


// 	setMouseTracking( TRUE );
// 	viewport()->setMouseTracking( TRUE );

	setMultiSelection( TRUE );
	setShowSortIndicator( TRUE );
	setAllColumnsShowFocus( TRUE );

	connect( verticalScrollBar(),   SIGNAL(valueChanged(int)), this, SLOT(slotChangeVslider(int)) );
	connect( horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(slotChangeHSlider(int)) );

	header()->setMaximumHeight( 23 );
	header()->setMovingEnabled( FALSE );
	connect( header(), SIGNAL(clicked(int)), this, SLOT(slotHeaderClicked(int)) );
	connect( header(), SIGNAL(sizeChange(int, int, int)),  this, SLOT(slotHeaderSizeChange(int, int, int)) );

	mSelectedItemsList = new SelectedItemsList;
	mSelectedItemsList->clear();
// 	mSelectedItemsList->setAutoDelete( TRUE );

	if ( toolTip ) {
		mToolTip = new ListViewExtToolTip( viewport(), this );
		mToolTip->setPalette( QColor( 221, 255, 227 ) ); // green background of toolTip
	}

	if ( renameBox ) {
		mListViewEditBox = new ListViewEditBox( viewport() );
		connect( mListViewEditBox, SIGNAL(signalRename(int, const QString &, const QString &)),
		 this, SIGNAL(signalRenameCurrentItem(int, const QString &, const QString &)) );
		setTabOrder( mListViewEditBox, this );
	}

	if ( findDlg )
		mListViewFindItem = new ListViewFindItem( this );

	if ( popupMenu ) {
		mPopupMenu = new PopupMenu( this );
		connect( mPopupMenu, SIGNAL(signalRename(int)),         this, SLOT(slotShowEditBox(int)) );
		connect( mPopupMenu, SIGNAL(signalSelectAll(bool)),     this, SLOT(slotSelectAllItems(bool)) );
		connect( mPopupMenu, SIGNAL(signalSelectSameExt(bool)), this, SLOT(slotSelectWithThisSameExtension(bool)) );
	}
	installEventFilter( this ); // for grab follow events: FocusIn, FocusOut, KeyPress
}


ListViewExt::~ListViewExt()
{
	removeEventFilter(this);
	delete mToolTip;           mToolTip = 0;
	delete mScrollTimer;       mScrollTimer = 0;
	delete mListViewEditBox;   mListViewEditBox = 0;
	delete mListViewFindItem;  mListViewFindItem = 0;
	delete mSelectedItemsList; mSelectedItemsList = 0;
}


void ListViewExt::setColumn( ColumnName columnName, uint columnWidth )
{
	if ( columnName == NAMEcol ) {
		mColumnsList.clear();
		mColumnsList.append( NAMEcol ); // the column NAMEcol must be always visible
	}
	else
	if ( columnWidth )
		mColumnsList.append( columnName );

	if ( columnWidth == 0 && columnName != NAMEcol )
		return;

	uint colWidth = columnWidth;
	const QStringList defaultColumnList = QStringList::split( ',', "0-170,1-70,2-107,3-70,4-55,5-55" );
	uint defaultColumnWidth = (defaultColumnList[columnName].section('-', -1 )).toInt();
	if ( colWidth < defaultColumnWidth && colWidth != 0 )
		colWidth = defaultColumnWidth;

	if ( columnName == NAMEcol )       addColumn( tr("Name"), (colWidth==0) ? (defaultColumnList[0].section('-', -1 )).toInt() : colWidth );
	else if ( columnName == SIZEcol )  addColumn( tr("Size"), colWidth );
	else if ( columnName == TIMEcol )  addColumn( tr("Time"), colWidth );
	else if ( columnName == PERMcol )  addColumn( tr("Permission"), colWidth );
	else if ( columnName == OWNERcol ) addColumn( tr("Owner"), colWidth );
	else if ( columnName == GROUPcol ) addColumn( tr("Group"), colWidth );
}


void ListViewExt::setShowColumns( bool show )
{
	if ( show )
		header()->show();
	else
		header()->hide();
}


void ListViewExt::setFontList( const QString & fontFamily, const uint fontSize )
{
	mFontFamily = fontFamily;  mFontSize = fontSize;
}


void ListViewExt::setShowIcons( bool showIcons )
{
	mShowIcons = showIcons;
	mIconWidth = (showIcons) ? 16 : 0;
}


void ListViewExt::setColor( QColorGroup::ColorRole r, const QColor & c )
{
	QPalette p = palette();
	QColorGroup cg = p.active();
	cg.setColor( r, c );
	p.setActive( cg );
	cg = p.inactive();
	cg.setColor( r, c );
	p.setInactive( cg );
	setPalette( p );
}


void ListViewExt::setHighlightBgColor( const QColor & highlBgColor )
{
	setColor( QColorGroup::Highlight, highlBgColor );
}


void ListViewExt::setHighlightedTextColor( const QColor & highlTextColor )
{
	setColor( QColorGroup::HighlightedText, highlTextColor );
}


void ListViewExt::setTextColor( const QColor & txtColor )
{
	setColor( QColorGroup::Text, txtColor );
}


void ListViewExt::formatNumberStr( QString & numStr )
{
	::formatNumberStr( numStr, mKindOfFormating );
}


QPixmap * ListViewExt::icon( const UrlInfoExt & urlInfo ) const
{
	QString sFileName = urlInfo.name();
	QString sFullFileName;

	if ( sFileName == ".." )
		return (new QPixmap( cdup ));

	static const char **iconName;

	if ( urlInfo.isSymLink() )  {
		iconName = broken_link;
		if ( mCurrentDirIsDevDir ) {
			sFullFileName = mCurrentURL + sFileName;
			//debug("ListViewExt::icon, sFullFileName= %s, mCurrentURL=%s", sFullFileName.latin1(), mCurrentURL.latin1() );
/*			if ( urlInfo.specialFile(S_IFCHR) )  iconName = chrDevice;
			else
			if ( urlInfo.specialFile(S_IFBLK) )  iconName = blkDevice;
			else
			if ( urlInfo.specialFile(S_IFIFO) )  iconName = fifo;
			else
			if ( urlInfo.specialFile(S_IFSOCK) ) iconName = socket;*/
			if ( FileInfoExt::specialFile(sFullFileName, FileInfoExt::CHRDEV) )  iconName = chrDevice;
			else
			if ( FileInfoExt::specialFile(sFullFileName, FileInfoExt::BLKDEV) )  iconName = blkDevice;
			else
			if ( FileInfoExt::specialFile(sFullFileName, FileInfoExt::FIFO) )    iconName = fifo;
			else
			if ( FileInfoExt::specialFile(sFullFileName, FileInfoExt::SOCKET) )  iconName = socket;
		}
		if ( urlInfo.isFile() )
			iconName = file_link;
		else
		if ( urlInfo.isDir() )
			iconName = folder_link;
	}
	else
	if ( urlInfo.isDir() )  {
		iconName = folder;
		if ( ! urlInfo.isReadable() )
			iconName = folder_lock;
	}
	else  { // this is file
		int findDot = sFileName.findRev( '.' );
		QString ext = (findDot > 0) ? sFileName.right(sFileName.length()-findDot-1).lower() : QString("");

			 if ( ext== "bmp" )  iconName= bmp;
		else if ( ext== "gif" )  iconName= gif;
		else if ( ext== "jpg" )  iconName= jpg;
		else if ( ext== "png" )  iconName= png;
		else if ( ext== "xbm" )  iconName= xbm;
		else if ( ext== "xpm" )  iconName= xpm;
//		else if ( ext== "pcx" )  iconName= pcx;
		else if ( ext== "txt" )  iconName= txt;
		else if ( ext== "so" )   iconName= lib;
		else if ( ext== "c" )    iconName= c;
		else if ( ext== "h"    || ext== "hh" )    iconName= h;
		else if ( ext== "cpp"  || ext== "cxx" || ext== "cc"  || ext== "C" )  iconName= cpp;
		else if ( ext== "html" || ext== "htm" || ext== "php" || ext== "php3" || ext== "phtml" || ext== "css" )  iconName= html;
		else if ( ext== "gz"   || ext== "bz2" || ext== "zip" || ext== "rar"  || ext== "sfx"   || ext == "z"  || ext== "tar" ||
				  ext== "tgz"  || ext== "ace" || ext== "arj" || ext== "lzh"  || ext== "rpm"   || ext== "deb" || ext== "lzma" )
					iconName= package;
		else {
			if ( mCurrentDirIsDevDir ) {
				sFullFileName = mCurrentURL + sFileName;
/*				if ( urlInfo.specialFile(S_IFCHR) )  iconName = chrDevice;
				else
				if ( urlInfo.specialFile(S_IFBLK) )  iconName = blkDevice;
				else
				if ( urlInfo.specialFile(S_IFIFO) )  iconName = fifo;
				else
				if ( urlInfo.specialFile(S_IFSOCK))  iconName = socket;*/
				if ( FileInfoExt::specialFile(sFullFileName, FileInfoExt::CHRDEV) )  iconName = chrDevice;
				else
				if ( FileInfoExt::specialFile(sFullFileName, FileInfoExt::BLKDEV) )  iconName = blkDevice;
				else
				if ( FileInfoExt::specialFile(sFullFileName, FileInfoExt::FIFO) )    iconName = fifo;
				else
				if ( FileInfoExt::specialFile(sFullFileName, FileInfoExt::SOCKET) )  iconName = socket;
			}
			else
				iconName= unknown;
		}

		if ( (urlInfo.permissions() & 0111) && iconName != lib ) // urlInfo.isExecutable()
			iconName= exe;
	}

	return (new QPixmap( iconName ));
}


void ListViewExt::moveCursorToName( const QString & name )
{
	QListViewItem *i;
	if ( name.isEmpty() ) {
		i = firstChild();
// 		return;
	}

	QString key;
	if ( currentItem() )
		key = currentItem()->key( mSortColumn, mAscending );
	i = findName( name );

	if ( i == 0 ) {
		QListViewItemIterator it = this;
		// --- NEW for the TREEpview, usuwam ostatni elem. z katalogu
		QListViewItem *parentOfCurrItem;
		if ( mKindOfView == TREEpview ) {
//			parentOfCurrItem = currentItem()->parent();
			parentOfCurrItem = currentItem();
			if ( parentOfCurrItem )
				if ( ! parentOfCurrItem->text(0).isEmpty() ) {
					it = parentOfCurrItem;
//					debug("ListViewExt::moveCursorToName, it.current=%s", (it.current()->text(0)).latin1() );
					ensureItemVisible( parentOfCurrItem );
					return;
				}
//			it++; // go to first item in a current branch
		}

		QListViewItem *item = 0;
//		QListViewItemIterator it( this );
		while ( (item = it.current()) != 0 )  {
			++it;
			QListViewItem *nextItem = it.current();
			if ( ! nextItem )
				break;

			if ( mAscending )  {
				if ( (item->key( mSortColumn, mAscending ) < key) && (nextItem->key( mSortColumn, mAscending ) > key) )  {
					i = nextItem;
					break;
				}
			}
			else
				if ( (item->key( mSortColumn, mAscending ) > key) && (nextItem->key( mSortColumn, mAscending ) < key) )  {
					i = nextItem;
					break;
				}
		} // while

		if ( i == 0 )
			i = firstChild();
	} // if ( i == 0 )

	ensureItemVisible( i );
}


void ListViewExt::removeItem( const QString & name )
{
	if ( name.isEmpty() )
		return;

	QListViewItem *item2remove = findName( name );
	if ( ! item2remove )
		return;

	QListViewItem *parent = item2remove->parent();

	if ( parent )
		parent->takeItem( item2remove );
	else
		QListView::takeItem( item2remove );

	moveCursorToName( currentItemText() ); // sets cursor to next/previous item
}


void ListViewExt::slotInsertItem( const UrlInfoExt & urlInfo )
{
	insertItem( urlInfo, mRootsItemOfCurrentPath, FALSE, TRUE ); // FALSE - don't remove this same, TRUE - set progress
}


void ListViewExt::insertItem( const UrlInfoExt & urlInfo, QListViewItem *pParentItemTree, bool bTryRemoveThisSameItem, bool setProgress )
{
	QListViewItem *pItem = (mKindOfView==TREEpview) ? pParentItemTree : 0;
	ListViewItemExt *pNewItem;

	if ( bTryRemoveThisSameItem )
		removeItem( urlInfo.name() ); // if an item already exists then remove it

	if ( pItem != NULL )
		pNewItem = new ListViewItemExt( (ListViewItemExt *)pItem, urlInfo );
	else
		pNewItem = new ListViewItemExt( this, urlInfo );

	bool bHideItem = false;
	if (! m_bShowHidden)
		if (urlInfo.name() != "..")
			if (urlInfo.name()[0] == '.')
				bHideItem = true;
	( bHideItem ) ? pNewItem->hide() : pNewItem->show();

	if ( setProgress && ! bHideItem )
		if ( mNumOfAllItemsPB ) {
			mInsertedItemsCounter++;
//			emit signalSetProgress( (100 * mInsertedItemsCounter) / mNumOfAllItemsPB );
		}
}


void ListViewExt::applyFilter( const QString & pattern, Filters::DirFilter dirFilter, bool filterForFiltered )
{
	bool hide = FALSE;
	UrlInfoExt urlInfo;
	QStringList patternLst;
	ListViewItemExt *itemExt;
	QListViewItemIterator it( this );
	patternLst = QStringList::split( '/', pattern );
	//debug("ListViewExt::applyFilter");

	while ( (itemExt=(ListViewItemExt *)it.current()) != 0 ) {
		if ( itemExt->text(NAMEcol) == ".." ) {
			it++; continue;
		}
		if ( filterForFiltered && dirFilter != Filters::HiddenNot )
			if ( ! itemExt->isVisible() ) {
				it++; continue;
			}

		urlInfo = itemExt->entryInfo();

		if ( dirFilter == Filters::AllFiles ) {
			hide = FALSE;
			if ( urlInfo.isFile() ) {
				for ( uint i=0; i<patternLst.count(); i++ ) { // check all patterns
					QRegExp expr( patternLst[i], TRUE, TRUE );
					hide = ( ! expr.exactMatch(itemExt->text( NAMEcol )) );
					if ( ! hide ) // file matches
						break;
				}
			}
		}
		else
		if ( dirFilter == Filters::DirsOnly )
			hide = ( ! urlInfo.isDir() );
		else
		if ( dirFilter == Filters::FilesOnly )
			hide = urlInfo.isDir();
		else
		if ( dirFilter == Filters::SymLinks )
			hide = ( ! urlInfo.isSymLink() );
		else
		if ( dirFilter == Filters::HangSymLinks )
			hide = (urlInfo.isSymLink()) ? (! ( ! urlInfo.isDir() && ! urlInfo.isFile() )) : TRUE;
		else
		if ( dirFilter == Filters::HiddenNot )
		{
			hide = (urlInfo.name()[0] == '.' && pattern.isEmpty());
		}

		( hide ) ? itemExt->hide() : itemExt->show();
		//if (hide)
			//debug("ListViewExt::applyFilter, hide=%d, name='%s', height=%d", hide, urlInfo.name().latin1(), itemExt->height());
		it++;
	}
}


void ListViewExt::setTextInColumn( int nColumn, const QString & sTxt, QListViewItem *pItem )
{
	ListViewItemExt *pItemExt = (ListViewItemExt *)((pItem == NULL) ? currentItem() : pItem);
	if ( nColumn == SIZEcol )
		pItemExt->setShowDirSize( (sTxt != "<DIR>") );

	pItemExt->setText( nColumn, sTxt );
}


void ListViewExt::setSorting( int column, bool ascending )
{
	mSortColumn = column;
	mAscending  = ascending;

	QListView::setSorting( column, ascending );
}


QString ListViewExt::pathForCurrentItem( bool absolutePath, bool findParent )
{
	QListViewItem *currItem = (findParent) ? currentItem()->parent() : currentItem();
	bool notRoot = ! currItem->text(0).isEmpty();
	QString path = (notRoot) ? currItem->text(0) : QString("");
	bool isDir   = (notRoot) ? ((ListViewItemExt *)currItem)->isDir() : TRUE; // root dir

	if ( isDir ) {
		if ( notRoot )
			path += "/";
		else // path is empty
			path = mHost;
	}
	if ( absolutePath && mKindOfView == TREEpview ) {
		path = mHost;
		if ( notRoot )
			path += ((ListViewItemExt *)currItem)->fullName();

		if ( ! isDir && path.at(path.length()-1) == '/' )
			path = path.left( path.length()-1 );
	}
//	debug("ListViewExt::pathForCurrentItem, path=%s, pathToParent=%d", path.latin1(), findParent );

	return path;
}


QString ListViewExt::absolutePathForItem( QListViewItem *item, bool addNameWhenDir )
{
	if ( item == NULL )
		item = currentItem();
// 	if ( item == NULL )
// 		return mHost;

	QString path;
	QListViewItem *currItem = (item->parent()) ? item->parent() : item;

	if ( mKindOfView == TREEpview ) {
		if ( item->isOpen() )
			currItem = item;
		path = mHost+((ListViewItemExt *)currItem)->fullName();
		if ( addNameWhenDir )
			if ( ((ListViewItemExt *)currItem)->isDir() ) {
				if ( item->isOpen() ) // to opened dir not to need adds a name
					path.remove( path.length()-1, 1 ); // remove last slash
				else
					path += item->text(0);
			}
	}
	else {
		path = mCurrentURL;
		if ( addNameWhenDir )
// 			if ( ((ListViewItemExt *)currItem)->isDir() || isSymLink )
				path += currItem->text(0);
	}

	return path;
}


uint ListViewExt::numOfAllItems( bool minusOneForList )
{
	uint number = 0;

	if ( mKindOfView == LISTpview ) {
		if ( childCount() )
			number = childCount() - (minusOneForList ? 1 : 0);
	}
	else { // TREEpview
		if ( currentItem() ) {
			if ( currentItem()->parent() )
				number = currentItem()->parent()->childCount();
		}
	}

	return number;
}


void ListViewExt::initFilesList( const QString & path, uint numOfAllItems, const QString & owner, const QString & group, bool forceCleaning )
{
//	setUpdatesEnabled( TRUE ); // always paint items
	mOwner = owner;
	mGroup = (group.isEmpty()) ? owner : group;

	if ( mKindOfView == LISTpview || forceCleaning ) {
		clear();
		mSelectedItemsList->clear();
		mNumOfSelectedItems = 0; mWeightOfSelected = 0;
	}
	mCurrentDirIsDevDir = (path.find("/dev/") == 0);
	mNumOfAllItemsPB = numOfAllItems; // for progress bar obj.
	mInsertedItemsCounter = 0;

	if ( mKindOfView == LISTpview )
		slotInsertItem( UrlInfoExt("..", 0, 0755, "root", "root", QDateTime::currentDateTime(), QDateTime::currentDateTime(), TRUE, FALSE, FALSE, TRUE, TRUE, TRUE) );
}


void ListViewExt::closeBranch( const QString & absPathName )
{
	if ( mKindOfView != TREEpview )
		return;

	QListViewItem *currItem, *newItem, *parent;
	// ---NNN
	if ( absPathName.isEmpty() )
		currItem = currentItem();
	else
	if ( (currItem=findName(absPathName)) == NULL )
		return;

	if ( ! currItem->isOpen() )
		currItem = currItem->parent();
	// ---
	//currItem = (currentItem()->isOpen()) ? currentItem() : currentItem()->parent();

	UrlInfoExt urlInfo = ((ListViewItemExt *)currItem)->entryInfo();
	parent = currItem->parent();
	//debug("ListViewExt::closeBranch, name=%s", urlInfo.name().latin1() );

	if ( parent ) {
		parent->takeItem( currItem );
		newItem = new ListViewItemExt( (ListViewItemExt *)parent, urlInfo );
	}
	else {
		takeItem( currItem );
		newItem = new ListViewItemExt( this, urlInfo );
	}

	ensureItemVisible( newItem );
	//setContentsPos( 0, mRootsItemOfCurrentPath->itemPos() ); // closed item must be on the top of view
}


void ListViewExt::openBranch( const QString & absPathName, bool moveViewAtTop )
{
	if ( mKindOfView != TREEpview )
		return;

	QString path;
	if ( absPathName.isEmpty() || childCount() == 0 ) { // --- create a root for the tree view
		clear();
		path = mHost;
		mRootsItemOfCurrentPath = new ListViewItemExt( this, UrlInfoExt() );
	}
	else // --- try to open a new brach
	if ( (mRootsItemOfCurrentPath = findName(absPathName)) ) {
		if ( mRootsItemOfCurrentPath->isOpen() )
			return;
		//debug("ListViewExt::openBranch, name=%s", mRootsItemOfCurrentPath->text(0).latin1() );
		ensureItemVisible( mRootsItemOfCurrentPath ); // (mRootsItemOfCurrentPath->childCount()) ? mRootsItemOfCurrentPath->itemBelow() : mRootsItemOfCurrentPath
		if ( moveViewAtTop )
			setContentsPos( 0, mRootsItemOfCurrentPath->itemPos() ); // opened item must be on the top of view
		mCurrentDirIsDevDir = (absPathName.find("/dev/") == 0);
		mRootsItemOfCurrentPath->setOpen( TRUE );
		path = absPathName;
	}

	emit signalOpen( path );
}


void ListViewExt::open( const QString & fileName, bool closeIfOpen )
{
	QString fName = (fileName.isEmpty()) ? mCurrentURL : fileName;

	bool isDir = (fName.at(fName.length()-1) == '/');
	ListViewItemExt *item;
	if ( (item=(ListViewItemExt *)findName(fName)) )
		if ( (isDir=item->isDir()) )
			if ( fName.at(fName.length()-1) != '/' )
				fName += "/";

	bool fNameIsArchive = FileInfoExt::isArchive( fName );
	if ( mKindOfView == TREEpview )
		if ( fNameIsArchive )
			if ( item ) {
				isDir = TRUE;
				item->setExpandable( TRUE );
			}

	if ( mKindOfView == TREEpview && isDir ) {
		if ( closeIfOpen )
			if ( currentItem()->isOpen() ) {
				emit signalClose(); // for current item
				return;
			}
		if ( mHost == FileInfoExt::hostName(mCurrentURL) && fNameIsArchive == FileInfoExt::isArchive(mCurrentURL) ) {
			int slashesDifferent = fName.contains('/') - mCurrentURL.contains('/');
			if ( slashesDifferent > 1 && fName.contains(mCurrentURL) )
				fName = fName.left( fName.find('/', mCurrentURL.length()+1)+1 );
			else
			if ( mCurrentURL.contains(fName) && mCurrentURL != fName ) {
				fName = mCurrentURL.left( mCurrentURL.find('/', fName.length()+1)+1 );
				close( fName );
				ensureNameVisible( mCurrentURL );
				mCurrentURL = FileInfoExt::filePath( fName );
				emit signalUpdateOfListStatus( mNumOfSelectedItems, numOfAllItems(), mWeightOfSelected );
				return;
			}
			else
			if ( mCurrentURL != fName && closeIfOpen )
				if ( ! fName.contains(mCurrentURL) ) {
					QString nameToClose = mCurrentURL.left( mCurrentURL.find('/', mHost.length()+1)+1 );
					close( nameToClose );
					fName = fName.left( fName.find('/', mHost.length()+1)+1 );
				}
		}
		openBranch( fName );
	}
	else {
		if ( isDir )
			close();
		emit signalOpen( fName );
	}
}


void ListViewExt::close( const QString & path )
{
	QString newPath = (path.isEmpty()) ? mCurrentURL : path;
	if ( newPath == mHost )
		return;
// FIXME jesli sciezka nie zostanie znaleziona to nic nie robic, aktualnie powoduje to CRASH
	if ( mKindOfView == TREEpview )
		closeBranch( newPath );
	else
		mNumOfSelectedItems = 0; mWeightOfSelected = 0;
}


void ListViewExt::refresh()
{
	close(); // no param. == current path
	open();  // no param. == current path
}


void ListViewExt::setKindOfView( KindOfListView kindOfView )
{
	if ( mKindOfView == kindOfView || kindOfView == UNKNOWNpview )
		return;

	//debug("ListViewExt::setKindOfView, kindOfView=%d", kindOfView );
	clear();
	mKindOfView = kindOfView;
	mNumOfSelectedItems = 0; mWeightOfSelected = 0;
}


void ListViewExt::updateList( int updateListOp, SelectedItemsList *list, const UrlInfoExt & newUrlInfo )
{
	QListViewItem *parentItemTree = (mKindOfView==TREEpview) ? findName(mCurrentURL) : 0;

	if ( list == NULL ) // default is class member
		list = mSelectedItemsList;

	// --- operation MakeDir/Put
	if ( list->isEmpty() && newUrlInfo.isValid() ) {
		if ( updateListOp == INSERTitems )
			insertItem( newUrlInfo, parentItemTree );

		setCurrentItem( findName(newUrlInfo.name()) );
		countAndWeightSelectedFiles( FALSE ); // FALSE - do not adds to the SelectedItemsList
	}

	if ( list == NULL )
		return;
	if ( list->isEmpty() )
		return;

// 	mInsertedItemsCounter = 0; // jesli bedzie pokazywany postep wstawiania
// 	mNumOfAllItemsPB = list.count(); // jw.
	UrlInfoExt urlInfo;
	QListViewItem *item;
	SelectedItemsList::iterator it;
	QString sSrcFile;
	int linkSize = -1;
	// TODO drobna optymalizacja szybkosciowa.
	// Przed petla mozna zrobic: if ( updateListOp == (REMOVEitems | INSERTitems) ) wtedy
	// pobrac do zmiennych wlasciwosci obiektu 'newUrlInfo' (permissions, owner, itd.)

	for (it = list->begin(); it != list->end(); ++it) {
		item = (*it);
		urlInfo = ((ListViewItemExt *)item)->entryInfo();

		if ( updateListOp == INSERTitems || updateListOp == INSERTitemsAsLinks ) {
			if ( urlInfo.owner() != mOwner ) {
				urlInfo.setOwner( mOwner );
				urlInfo.setGroup( mGroup );
			}
			if ( updateListOp == INSERTitemsAsLinks ) {
				urlInfo.setSymLink( TRUE );
				if ( ! mShowFileLinkSize ) { // shows length of target link (name + path)
					sSrcFile = FileInfoExt::filePath(newUrlInfo.name()) + FileInfoExt::fileName(urlInfo.name());
					if ( FileInfoExt(sSrcFile).isSymLink() ) // TODO info about symlink stat. should be send on the SelectedItemsList
						linkSize = FileInfoExt(sSrcFile).readLink().length();
					else
						linkSize = FileInfoExt::fileName(newUrlInfo.name()).length() + FileInfoExt::filePath(newUrlInfo.name()).length();
					urlInfo.setSize( linkSize );
				}
				// else shows target file size
				if ( list->count() == 1 )
					urlInfo.setName( FileInfoExt::fileName(newUrlInfo.name()) ); // sets a new name
			}
			insertItem( urlInfo, parentItemTree ); // TRUE, TRUE ); // bedzie pokazywac postep wst.
		}
		else
		if ( updateListOp == REMOVEitems )
			removeItem( item->text(0) );
		else
		if ( updateListOp == DESELECTitems )
			QListView::setSelected( item, FALSE );
		else // change attributs
		if ( updateListOp == (REMOVEitems | INSERTitems) ) {
			if ( ! newUrlInfo.name().isEmpty() )
				urlInfo.setName( newUrlInfo.name() );
			if ( newUrlInfo.permissions() > -1 )
				urlInfo.setPermissions( newUrlInfo.permissions() );
			if ( ! newUrlInfo.owner().isEmpty() )
				urlInfo.setOwner( newUrlInfo.owner() );
			if ( ! newUrlInfo.group().isEmpty() )
				urlInfo.setGroup( newUrlInfo.group() );
			if ( ! newUrlInfo.lastModified().isNull() )
				urlInfo.setLastModified( newUrlInfo.lastModified() );
			if ( ! newUrlInfo.lastRead().isNull() )
				urlInfo.setLastRead( newUrlInfo.lastModified() );

			QListView::takeItem( findName(item->text(0)) );
			insertItem( urlInfo, parentItemTree );
		}

		if ( updateListOp != INSERTitems )
			if ( mNumOfSelectedItems > 0 ) {
				mNumOfSelectedItems--;
				mWeightOfSelected -= urlInfo.size();
			}
	}

	if ( updateListOp != REMOVEitems && updateListOp != DESELECTitems )
		ensureItemVisible( findName(urlInfo.name()) ); // let's show last inserted item

	emit signalUpdateOfListStatus( mNumOfSelectedItems, numOfAllItems(), mWeightOfSelected );
}


QListViewItem * ListViewExt::findName( const QString & name, bool bFindInSelectedNor )
{
	if ( name.isEmpty() )
		return 0L;

	QString newName;
	if ( mKindOfView == LISTpview ) {
		// if name has a path then get only last name from it
		if ( name.at(name.length()-1) == '/' ) {
			int numOfSlashes = name.contains('/')-1;
			newName = name.section( '/', numOfSlashes, numOfSlashes );
		}
		else
			newName = name.right( name.length()-name.findRev('/')-1 );

		QListViewItem *pFindItem = QListView::findItem( newName, NAMEcol );

		if ( bFindInSelectedNor ) {
			SelectedItemsList::iterator selIt = qFind( mSelectedItemsList->begin(), mSelectedItemsList->end(), pFindItem );
			if ( selIt == mSelectedItemsList->end() ) // item not found
				pFindItem = NULL;
		}
		return pFindItem;
	}

	// --- find item into the tree
	QString itemPath;
	QListViewItem *item = 0;
	QListViewItemIterator it( this );

	newName = name;
	if ( newName.find(mHost) != 0 ) // the tree need to absolute path
		newName.insert( 0, absolutePathForItem() );
	if ( newName.at(newName.length()-1) == '/' ) // remove last slash
		newName.remove( newName.length()-1, 1 );

	while ( it.current() != 0 ) {
		itemPath = mHost+((ListViewItemExt *)*it)->fullName( FALSE ); // FALSE - without adds a slash
		if ( itemPath == newName ) {
			item = *it;
			break;
		}
		it++;
	}

	return item;
}


void ListViewExt::ensureItemVisible( QListViewItem * item )
{
	if ( item == NULL )
		item = currentItem();
	if ( item == NULL )
		return;

	setCurrentItem( item );
	QListView::ensureItemVisible( item );
}


void ListViewExt::ensureNameVisible( const QString & name )
{
	ensureItemVisible( findName(name) );
}


void ListViewExt::showPopupMenu( bool onCursorPos )
{
	QPoint position;

	if ( onCursorPos )
		position = QPoint( QCursor::pos().x()-2, QCursor::pos().y()+2 );
	else // for Key_Menu
		position = mapToGlobal(QPoint( header()->sectionSize( NAMEcol ), itemRect( currentItem() ).y()+currentItem()->height()+header()->height()+5 ));

	int y = position.y(), menuHeight = mPopupMenu->sizeHint().height();
	if ( y+menuHeight > QApplication::desktop()->height() ) {
		position.setY( y-menuHeight );
		if ( ! onCursorPos ) // correction for Key_Menu
			position.setY( y-currentItem()->height()-5 );
	}

	mPopupMenu->showMenu( position );
}


void ListViewExt::showFindItemDialog()
{
	mListViewFindItem->show( mapToGlobal(QPoint( header()->sectionSize(NAMEcol)-20, height()-15 )) );
}


void ListViewExt::removeItemFromSIL( QListViewItem * item )
{
	if ( item )
		if ( mSelectedItemsList->count() ) // if a list isn't empty
			if ( mSelectedItemsList->find(item) != mSelectedItemsList->end() ) // item found
				mSelectedItemsList->remove( mSelectedItemsList->find(item) );
			//setSelected( item, FALSE ); // deselect and remove from the selecting list
}


void ListViewExt::slotHeaderClicked( int section )
{
	QListView::ensureItemVisible( currentItem() );

	uint column = header()->mapToIndex( section ); // jesli kiedys mozna byloby zmieniac kolejnosc kolumn

	if ( column != mSortColumn )
		mAscending = TRUE;
	else
		mAscending = !mAscending;

	mSortColumn = column;
}


void ListViewExt::slotHeaderSizeChange( int section, int oldSize, int newSize )
{
	header()->resizeSection( section, newSize );
	triggerUpdate();

	int editBoxColumn = mListViewEditBox->column();
	if ( editBoxColumn == section ) { // a current colum contains mListViewEditBox obj.
		if ( mListViewEditBox->isVisible() )
			mListViewEditBox->setFixedWidth( newSize - ((section == NAMEcol) ? mIconWidth : 0) - 2 ); // -2 it's correction
	}
	else
	if ( editBoxColumn > section ) { // a next colum contains mListViewEditBox obj.
		if ( mListViewEditBox->isVisible() )
			mListViewEditBox->move( mListViewEditBox->x()+(newSize-oldSize), itemRect(currentItem()).y() );
	}
}


void ListViewExt::slotChangeVslider( int contentsY )
{
	if ( mScrollTimer || ! mNotHiddenFocus ) // mScrollTimer if true only when is working fun. autoScroll()
		return;

	int y = itemPos( currentItem() );
	int itemHeight = currentItem()->height();

// FIXME Przewijanie w gore. Przy duzych ilosciach plikow kursor skacze o coraz wiecej elem.

	// if cursor is behind top margin then sets it on a first visible
	if ( contentsY > y )
		setCurrentItem( itemAt( QPoint( 0, contentsY/itemHeight + 1 ) ) );

	// if cursor is behind bottom margin then sets it on a last visible
	int bm = y - visibleHeight() + itemHeight;
	if ( contentsY < bm ) {
		int bottomLine = (visibleHeight()/itemHeight - 1)*itemHeight;
		setCurrentItem( itemAt( QPoint(0, bottomLine) ) );
	}
}


void ListViewExt::slotChangeHSlider( int )
{
	currentItem()->repaint();
}


void ListViewExt::slotShowEditBox( int column )
{
	if ( currentItem()->text(NAMEcol) != ".." && columnWidth(column) )
		mListViewEditBox->show( column, (mKindOfView == TREEpview) );
}


bool ListViewExt::eventFilter( QObject * o, QEvent * e )
{
	bool popupMenuOrFindItemIsVisible = (mPopupMenu->isVisible() || mListViewFindItem->isVisible());
	QEvent::Type eventType = e->type();

	if ( eventType == QEvent::FocusOut ) {
		if ( popupMenuOrFindItemIsVisible ) {
			setFocus();
			return TRUE;
		}
	}
	else
	if ( eventType == QEvent::FocusIn ) {
		if ( mListViewEditBox->isVisible() )
			return TRUE;

		if ( popupMenuOrFindItemIsVisible )
			return TRUE;

		emit signalPathChanged( mCurrentURL ); // need to update path in pathView
	}
	else
	if ( eventType == QEvent::KeyPress ) {
		if ( ! numOfAllItems(FALSE) )
			return TRUE;

		QKeyEvent *keyEvent = (QKeyEvent *)e;
		int state = keyEvent->state();
		int key   = keyEvent->key();

		if ( mPopupMenu->isVisible() ) {
			mPopupMenu->keyPressed( keyEvent );
			return TRUE;
		}
		else
		if ( mListViewFindItem->isVisible() ) {
			// let's execute action (view/edit/copy/move/delete) on found file
			if ( key == Key_F3 || key == Key_F4 || key == Key_F5 || key == Key_F6 || key == Key_F8 ) {
				mListViewFindItem->hide();
				QApplication::sendEvent( mParent, keyEvent );
			}
			else
				mListViewFindItem->keyPressed( keyEvent );
			return TRUE;
		}

		switch( key )
		{
			case Key_Up:
			case Key_Down:
				if ( state != NoButton ) {
					QApplication::sendEvent( mParent, keyEvent );
					return TRUE;
				}
			case Key_PageUp:
			case Key_PageDown:
			case Key_Home:
			case Key_End:
				if ( state != NoButton && state != Keypad ) {
					QApplication::sendEvent( mParent, keyEvent ); // send event to parent
					return TRUE;
				}
				else
				if ( mKindOfView == TREEpview ) { // path changed in the tree view
					if ( key != Key_Home )
						QListView::keyPressEvent( keyEvent );
					else
						ensureItemVisible( firstChild()->itemBelow() );

					emit signalPathChanged( absolutePathForItem() );
					return TRUE;
				} // if ( mKindOfView == TREEpview )
			break;

			case Key_Left:
			case Key_Right:
				if ( state != NoButton ) {
					QApplication::sendEvent( mParent, keyEvent );
					return TRUE;
				}
				else
				if ( mKindOfView == TREEpview && currentItemIsDir() ) { // archive == dir, too
					if ( key == Key_Left ) {
						// close(); // closes a branch, called in VirtualFS::slotCloseCurrentDir()
						emit signalClose(); // for current item
						return TRUE;
					}
					else if ( key == Key_Right ) {
						open( currentItemText() ); // opens brach
						return TRUE;
					}
				}
			break;

			case Key_Tab:
			break;

			case Key_Enter:
			case Key_Return:
				if ( state != NoButton && state != Keypad ) {
					QApplication::sendEvent( mParent, keyEvent ); // send event to parent
					return TRUE;
				}
				// The obj.of QListView class is emiting the signal 'returnPressed()'
			break;

			default:
				QApplication::sendEvent( mParent, keyEvent ); // send event to parent
				return TRUE; // eat event
			break;
		} // switch()
	} // if ( eventType == QEvent::KeyPress )

	return QListView::eventFilter( o, e );    // standard event processing
}


void ListViewExt::paintEmptyArea( QPainter * p, const QRect & rect )
{
	p->fillRect(rect, backgroundColor());
}

