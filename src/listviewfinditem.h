/***************************************************************************
                          listviewfinditem.h  -  description
                             -------------------
    begin                : Thu Nov 30 2000
    copyright            : (C) 2000 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef LISTVIEWFINDITEM_H
#define LISTVIEWFINDITEM_H

#include <qlineedit.h>
#include <qlistview.h>
#include <qwidget.h>
#include <qframe.h>
#include <qtimer.h>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa okienka dialogowego do szybkiego wyszukiwania elementu listy.
 Obiekt tej klasy powoduje pokazanie, na pozycji dolnego brzegu rodzica (tutaj
 panela), okienka z linijk� edycyjn� s�u��c� do wprowadzania nazw szukanych
 element�w. Obs�uga jego dzia�a na zasadzie przyrostowego wyszukiwania ci�gu,
 tzn. w miar� wpisywania kursor panela jest ustawiany na szukanym elemencie
 listy. Kiedy wpisany jest fragment tekstu, mo�na porusza� si� po podobnych
 element�w za pomoc� klawiszy strza�ek (g�ra/d�), na pierwszy pasuj�cy (Home)
 i ostatni (End). Klawisz Escape ukrywa okienko. Wci�ni�cie "Enter/Return"
 spowoduje wys�anie sygna�u: "signalReturnPressed()" z parametrem b�d�cym
 wska�nikiem do elementu, na kt�rym aktualnie stoi kursor, po czym nast�pi
 zamkni�cie dialogu. Klasa jest przystosowana do pracy z widokiem typu QListView
 i dziedzicz�cych po nim.
*/
class ListViewFindItem : public QFrame {
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param ptrOfList - wska�nik do listy (rodzica dla bie��cego obiektu),
	 @param name - nazwa dla obiektu,
	 @param fl - flaga typu WFlags.
	 */
	ListViewFindItem( QListView * ptrOfList, const char * name=0, WFlags fl=WType_Popup );

	/** Destruktor klasy.\n
	 Usuwane s� tutaj obiekty linijki edycyjnej oraz timera.
	 */
	~ListViewFindItem();

	/** Obs�uga klawiatury dla dialogu szybkiego wyszukiwania.\n
	 @param e - zdarzenie klawiatury, min. wci�ni�cie klawisza.\n
	 Obs�ugiwane klawisze to: Escape, Up, Down, Home, End, Enter/Return,
	 Backspace. Wci�ni�cie klawisza innego ni� powy�sze spowoduje wywo�anie
	 funkcji dopasowuj�cej ci�g do elementu na li�cie, patrz:
	 @see matchItemForCurrentText().
	 */
	void keyPressed( QKeyEvent * e );

	/** Funkcja powoduje pokazanie bie��cego obiektu na podanej pozycji.
	 @param pos - pozycja, na kt�rej powinno si� pokaza� obiekt.\n
	 Uruchamiana jest tutaj r�wnie� obs�uga migaj�cego kursora.
	 */
	void show( const QPoint & pos );

private:
	QListView *mListView;
	QLineEdit *mLineEdit;
	QTimer *mTimer;

	uint mLineEditCharsCounter;
	bool mShowCursor;

	/** Kierunek ruchu kursorem.
	 */
	enum Direct { Up, Down, Home, End };


	/** Dopasowanie wpisanej w okienku edycyjnym nazwy z nazw� na li�cie.\n
	 Wyszukuje element na liscie (jej wska�nik jest inicjowany w konstruktorze),
	 kt�rego nazwa zaczyna si� tak jak w podano w okienku edycyjnym. Je�li element
	 zostanie znaleziony ustawiany jest na niego kursor listy (belka),
	 w przeciwnym razie nic nie jest robione.
	 */
	void matchItemForCurrentText();

	/** Poprzez t� funkcj� jest pobierany tekst z okienka edycyjnego.\n
	 Jest niezb�dna, aby pobra� tekst bez kursora, poniewa� jest on tutaj
	 obs�ugiwany 'r�cznie'.
	 */
	QString text();

	/** Pr�buje przesun�� kursor o jeden element listy w podanym kierunku.\n
	 @param direct - kierunek przesuni�cia kursora.\n
	 Kursor jest przesuwany tylko wtedy, gdy nazwa elementu listy zaczyna tak
	 samo, jak tekst w okienku edycyjnym.
	 */
	void moveCursor( Direct direct );

private slots:
	/** Slot obs�uguj�cy migaj�cy kursor.\n
	 Jest on wywo�ywany po pokazaniu obiektu co 500 ms.
	 */
	void slotTimeOut();

protected:
	/** Funkcja wykorzystywana do zatrzymywania timera, kt�ry jest u�ywany przy
	 obs�udze migaj�cego kursora.
	 @param e - zdarzenie zamkni�cia dialogu.
	 */
	void closeEvent( QCloseEvent * e );

signals:
	/** Sygna� wysy�any gdy wci�ni�ty zostnianie klawisz Return/Enter.
	 @param item - wska�nik elementu listy na kt�rym aktualnie stoi kursor.
	 */
	void signalReturnPressed( QListViewItem * item );

};

#endif
