/***************************************************************************
                          virtualfs.cpp  -  description
                             -------------------
    begin                : sat nov 30 2002
    copyright            : (C) 2002, 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#include "localfs.h" // for getOwners()
#include "fileview.h"
#include "virtualfs.h"
#include "messagebox.h"
#include "fileinfoext.h"
#include "preoperationdialog.h"
#include "fileoverwritedialog.h"

#include <qregexp.h>
#include <qsettings.h>
#include <qinputdialog.h>


VirtualFS::VirtualFS( const QString & inputURL, ListViewExt *listViewExt )
	: mListViewExt(listViewExt),
	  mProgressDlg(NULL), mPropertiesDialog(NULL), mFindFileDialog(NULL), mFilesAssociation(NULL)
{
	// --- init FS info
	mCurrentFilesSystem = kindOfFilesSystem( inputURL );

	QSettings *settings = new QSettings;
	sCloseProgressDlgAfterFinished = settings->readBoolEntry( "/qtcmd/FilesSystem/CloseProgressDlgAfterFinished", FALSE );
	mAlwaysOverwrite = settings->readBoolEntry( "/qtcmd/FilesSystem/AlwaysOverwrite", FALSE );
	mSavePermision = settings->readBoolEntry( "/qtcmd/FilesSystem/AlwaysSavePermission", TRUE );
	mAskBeforeDelete = settings->readBoolEntry( "/qtcmd/FilesSystem/AskBeforeDelete", TRUE );
	mGetInToDir = settings->readBoolEntry( "/qtcmd/FilesSystem/AlwaysGetInToDir", FALSE );
	mHardLink = FALSE;
	delete settings;

	debug("VirtualFS::VirtualFS(), init mCurrentFilesSystem=%d, inputURL=%s", mCurrentFilesSystem, inputURL.latin1() );
}


VirtualFS::~VirtualFS()
{
	delete mProgressDlg;
	delete mFindFileDialog;
	delete mPropertiesDialog;
}


void VirtualFS::rereadCurrentDir()
{
	mPreviousDirName = mListViewExt->pathForCurrentItem();
	if ( mPreviousDirName.find("..") != -1 )
		mPreviousDirName = "";
	mListViewExt->refresh(); // current list or current branch
	mListViewExt->moveCursorToName( mPreviousDirName ); // need to for tree view
}


void VirtualFS::slotCloseCurrentDir()
{
	if ( mCurrentURL == "/" && mListViewExt->isListView() )
		return;

	if ( mListViewExt->isTreeView() && mCurrentURL != host() ) {
		ListViewItemExt *parent = (ListViewItemExt *)mListViewExt->currentItem()->parent();
		if ( parent->text(0).isEmpty() )
			mCurrentURL = mListViewExt->pathForCurrentItem();
		else
			mCurrentURL = host()+parent->fullName();
	}
// 	debug("VirtualFS::slotCloseCurrentDir(), mCurrentURL=%s", mCurrentURL.latin1() );

	if ( mCurrentURL != host() ) {
		if ( mListViewExt->isTreeView() ) // need sets variable mPreviousDirName, it value gets moveCursorToName()
			mPreviousDirName = mCurrentURL;
		else { // 'mPreviousDirName' is contains current dir name (without absolute path)
			int numOfSlashes = mCurrentURL.contains('/')-1;
			mPreviousDirName = mCurrentURL.section( '/', numOfSlashes, numOfSlashes );
			if ( FileInfoExt::isArchive(mPreviousDirName) ) {
				mPreviousDirName = "";
				emit signalResultOperation( Connect, FALSE ); // force create list with previous local fs directory
				return;
			}
		}
	}
	else { // if current FS is network or archive FS, and user want go out from it
		if ( mCurrentFilesSystem != LOCALfs ) {
			mPreviousDirName = "";
			emit signalResultOperation( Connect, FALSE ); // force create list with previous local fs directory
		}
		return;
	}

	// LISTpview
	if ( mListViewExt->isListView() ) {
		slotOpen( "../" );
		return;
	}

	// TREEpview
	mListViewExt->close(); // closes brach for current item
	mNewURL = mListViewExt->pathForCurrentItem(); // get absolute path
	mNewURL = FileInfoExt::filePath( mNewURL ); // gets previous path

	mCurrentURL = mNewURL;
	emit signalResultOperation( List, TRUE );
}


void VirtualFS::slotOpen( const QString & url )
{
	if ( url.isEmpty() )
		return;

	bool isFile    = (url.at(url.length()-1) != '/');
	bool isArchive = FileInfoExt::isArchive( url );
	bool isDir     = (! isFile);

	QString newURL = (isArchive) ? url : absoluteFileName( url );
	debug("VirtualFS::slotOpen, url=%s", url.latin1() );

	if ( isDir ) // in localFS
		if ( newURL.find("/../") != -1 ) {
			if ( newURL == host()+"../" ) { // need to go out from FS
				if ( kindOfFilesSystem(newURL) != LOCALfs )
					emit signalResultOperation( Connect, FALSE ); // force create list with previous local fs directory
				return;
			}
			parsePath( newURL );
			int numOfSlashes = mCurrentURL.contains('/')-1;
			mPreviousDirName = mCurrentURL.section( '/', numOfSlashes, numOfSlashes );
		}

	if ( isArchive ) {
		if ( kindOfCurrentFS() != ARCHIVEfs ) {
			emit signalOpenArchive( newURL );
			return;
		}
	}
	else
	if ( isFile ) {
		openFile( newURL );
		return;
	}
	mListViewExt->setCurrentURL(newURL);
	readDir( mNewURL = newURL );
}


void VirtualFS::openFile( const QString & fileName, bool editMode )
{
	if ( fileName.isEmpty() || fileName.at(fileName.length()-1) == '/' )
		return;
	QString newFileName = fileName;

	if ( mListViewExt->numOfSelectedItems() == 0 )
		mListViewExt->clearSelectedItemsList();

	mListViewExt->collectAllSelectedNames( mSelectedFilesList );

	QListViewItem *item;
	uint currentItemId = 0;
	QStringList filesList; // exists and reable files
	QString fName;
	for( uint i=0; i<mSelectedFilesList.count(); i++ ) {
		fName = mSelectedFilesList[i];
		item  = mListViewExt->findName( fName );
		if ( item )
			if ( ! ((ListViewItemExt *)item)->isDir() )
				if ( fileIsReadable(fName, FALSE) )
					filesList.append( mSelectedFilesList[i] );
	}
	mSelectedFilesList = filesList;

	if ( mFilesAssociation ) {
		if ( mFilesAssociation->loaded() ) {
			mSelectedFilesList.clear();
			QStringList filesListForExtView;
			// collect the files names for external viewer
			for ( uint i=0; i<filesList.count(); i++ ) {
				fName = filesList[ i ];
				if ( mFilesAssociation->kindOfViewer(fName) == EXTERNALviewer )
					filesListForExtView.append( fName );
				else
					mSelectedFilesList.append( fName );
			}
			if ( filesListForExtView.count() > 0 )
				runExternalViewer( filesListForExtView );
		}
	}

	if ( mSelectedFilesList.count() > 0 ) { // for internal view
		mOpenedFileEditMode  = editMode;
		mItemIdForOpenedFile = currentItemId;
		open( fName ); // call virtual fun.
	}
}


void VirtualFS::createFileView()
{
	FileView *fv = new FileView( mSelectedFilesList, mItemIdForOpenedFile, mOpenedFileEditMode, NULL, "Window");
	fv->setFilesAssociation( mFilesAssociation );

	connect( fv, SIGNAL(signalUpdateItemOnLV( const QString & )),
	 this, SLOT(slotUpdateItemOnLV( const QString & )) );
	connect( fv, SIGNAL(signalReadFile(const QString &, QByteArray &, int &, int)),
	 this, SLOT(slotReadFileToView(const QString &, QByteArray &, int &, int)) );
	connect( fv, SIGNAL(signalRereadFilesAssociation()),
	 this, SLOT(slotRereadFilesAssociation()) );

	disconnect( SIGNAL(signalReadyRead()) ); // old connection
	connect( this, SIGNAL(signalReadyRead()), fv, SLOT(slotReadyRead()) );
}


void VirtualFS::createNewEmptyFile( bool createDir )
{
	QString kindOfFileStr = (createDir) ? tr("directory") : tr("file");

	bool okPressed;
	bool p1, p2; // dummy
	QString fileName = PreOperationDialog::getText(
	 tr("Please to give a")+" "+kindOfFileStr+" "+tr("name:"),
	 tr("new")+" "+kindOfFileStr, (createDir) ? MakeDir : Touch,
	 QString("/qtcmd/FilesSystem/Make")+QString((createDir) ? "DirNames" : "FileNames"),
	 okPressed, p1, p2
	);
	if ( ! okPressed || fileName.isEmpty() )
		return;

	// --- simple parser
	if ( fileName.find("./") == 0 )
		fileName.remove( 0, 2 );
	if ( fileName.find("~/") == 0 )
		fileName.replace( "~/", QDir::homeDirPath()+"/" );

	if ( createDir ) {
		if ( fileName.at(fileName.length()-1) != '/' )
			fileName += "/";
	}
	else
		if ( fileName.at(fileName.length()-1) == '/' )
			fileName = fileName.length()-1;
	// ----

	QString fullFileName = absoluteFileName(fileName);

	createAnEmptyFile( fullFileName ); // or create new directory

	if ( errorCode() == AlreadyExists && ! createDir ) {
		int result = MessageBox::yesNo( this,
		 tr("Error")+" - QtCommander",
		 fullFileName+"\n\n"+tr("This")+" "+kindOfFileStr+" "+
		 tr("already exists")+" !\n"+tr("Open it")+" ?",
		 MessageBox::Yes // default is the 'Yes' button
		);
		if ( result == MessageBox::Yes )
			openFile( fullFileName, TRUE ); // call virtual fun.
	}
}


void VirtualFS::removeFiles()
{
	mListViewExt->collectAllSelectedNames( mSelectedFilesList );
	uint nor = mSelectedFilesList.count(); // number of removed
	if ( nor == 0 ) // maybe cursor fits to item ".."
		return;

	int result;
	if ( mAskBeforeDelete ) {
		QString info1, info2;
		if ( nor > 1 ) {
			info1 = QString("%1 "+tr("files selected")).arg(nor);
			info2 = tr("this files");
		}
		else {
			info1 = "\""+mSelectedFilesList[0]+"\"";
			info2 = (info1.at(info1.length()-2) == '/') ? tr("directory") : tr("file");
		}
		result = MessageBox::yesNo( this,
		tr("Remove files")+" - QtCommander",
		info1+"\n\n"+tr("Do you really want to delete")+" "+info2+" ?",
		MessageBox::Yes // default is the 'Yes' button
		);
	}
	else
		result = MessageBox::Yes;

	if ( result == MessageBox::Yes )
		deleteFiles( mSelectedFilesList );
}


void VirtualFS::makeLink( const QString & targetPath, bool editOnly )
{
	QString sourceFileName = mListViewExt->currentItemText();

	uint nocf = 1;
	QString targetFileName;
	QStringList targetFilesList;
	mOperationHasFinished = FALSE;
	sourceFileName.insert( 0, mCurrentURL );
// 	QString msg1 = (editOnly) ? tr("Edit") : tr("Make");

	if ( editOnly ) {
		if ( ! isLink(sourceFileName) )
			return;
		targetFileName = readLink( sourceFileName );
		mSelectedFilesList.append( FileInfoExt::fileName(sourceFileName) );
	}
	else {
		mListViewExt->collectAllSelectedNames( mSelectedFilesList );
		nocf = mSelectedFilesList.count();
		for (uint i=0; i<nocf; i++)
			targetFilesList.append( targetPath+FileInfoExt::fileName(mSelectedFilesList[i]) );
	}

	bool ok = FALSE;
	QString targetURL;
	if ( nocf < 2 || editOnly ) {
		if ( ! editOnly )
			targetFileName = targetFilesList[0];
		targetURL = PreOperationDialog::getText(
		 "\""+mSelectedFilesList[0]+"\"", targetFileName, MakeLink,
		 "/qtcmd/FilesSystem/MakeLinkURLs",
		 ok, mHardLink, mAlwaysOverwrite, editOnly, FALSE
		);
		parsePath( targetURL, FALSE ); // FALSE - none adds last slash (remove it if exists)

		if ( targetURL.isEmpty() )
			targetURL = "/" + mListViewExt->currentItemText();

		targetFilesList[0] = targetURL;
	}
	else {
		int result = MessageBox::yesNo( this,
		 tr("Make links")+" - QtCommander",
		 QString("%1 "+tr("files selected")).arg(nocf)+
		 "\n\n"+tr("Do you really want to make links for selected file(s)")+" ?",
		 MessageBox::Yes // default is the 'Yes' button
		);
		if ( result == MessageBox::Yes )
			ok = TRUE;
	}

	if ( ok ) {
		if ( editOnly ) // target file is source file
			makeLink( (mTargetURL=targetURL), sourceFileName, editOnly, mHardLink, mAlwaysOverwrite );
		else {
			mTargetURL = FileInfoExt::filePath(targetFilesList[0]);
			makeLink( mSelectedFilesList, targetFilesList, editOnly, mHardLink, mAlwaysOverwrite );
		}
	}
}


bool VirtualFS::copyFilesTo( const QString & targetPath, const QStringList & filesList, bool removeSrc )
{
	if ( targetPath.isEmpty() )
		return FALSE;

	uint nocf = filesList.count(); // mumber of copied files
	if ( nocf == 0 ) // maybe cursor fits on item ".."
		return FALSE;

	bool ok;
	QString targetURL, copyStrInfo;
	Operation operation = (removeSrc) ? Move : Copy;
	QString fileName = (nocf == 1) ? "\""+filesList[0]+"\"" : QString::number(nocf)+" "+tr("files selected");
	while( 1 ) {
		targetURL = PreOperationDialog::getText(
		 fileName, targetPath, operation,
		 "/qtcmd/FilesSystem/CopyMoveURLs",
		 ok, mSavePermision, mAlwaysOverwrite
		);
		if ( ! ok || targetURL.isEmpty() ) // user canceled an copying/moving operation
			return FALSE;

		parsePath( targetURL );

		if ( nocf > 1 )
			break;
		else { // one file selected, check whether have it this same name
			if ( filesList[0]+"/" != targetURL )
				break;
		}
		copyStrInfo = (removeSrc) ? tr("move") : tr("copy");
		MessageBox::critical( this,
			tr("Source file :")+" \""+FileInfoExt::fileName(filesList[0])+"\"\n"+
			tr("Target file :")+" \""+FileInfoExt::fileName(targetURL)+"\"\n\n"+
			tr("Cannot")+" "+copyStrInfo+" "+tr("to this same file")
		);
	}

	parsePath( targetURL, FALSE ); // need to remove the last slash

	for ( uint i=0; i<filesList.count(); i++ )
		if ( filesList[i] == targetURL ) {
			mListViewExt->setSelected( mListViewExt->findName(targetURL), FALSE );
			break;
		}

	mTargetURL = targetURL+"/"; // need to synchronize panels in the FilesPanel class
	debug("VirtualFS::copyFilesTo(), %s to targetURL=%s", copyStrInfo.latin1(), targetURL.latin1() );
	copyFiles( targetURL, filesList, mSavePermision, mAlwaysOverwrite, FALSE, removeSrc );

	return TRUE;
}


void VirtualFS::parsePath( QString & path, bool alwaysAddSlash )
{
	if ( path.find( '~' ) != -1 )
		path.replace( QRegExp("~+"), QDir::homeDirPath() );

	if ( alwaysAddSlash ) {
		if ( path.at(path.length()-1) != '/' )
			path += "/";
	}
	else // remove last slash
		if ( path != "/" && path.at(path.length()-1) == '/' )
			path.remove( path.length()-1, 1 );

	path = absoluteFileName( path );

	// --- mini path-filter
	// an expression "\/[^\/]*\/\.\.\/" - have replaced string "/any_string/../" to "/"
	// here, all occures this string have been replaced
	while ( path.contains(QRegExp("\\/[^\\/]*\\/\\.\\.\\/")) )
		path.replace(QRegExp("\\/[^\\/]*\\/\\.\\.\\/"), "/" );

	// for LocalFS result maybe "//", for other - eg. "ftp://" (equale 'rootName')
	if ( path.find("../") != -1 )
		path.replace( "../", "/" );

	if ( path != "/" )
		if ( path == rootName() ) {
			path = "";
			return;
		}

	QString host = path.left(path.find('/')+1);
	path.remove( 0, host.length() );
	// an expression "\\/\\.\\/+" - have been replaced all strings /./ to /
	while ( path.contains( "/./") )
		path.replace( "/./", "/" );
	// an expression "\\/\\/+" - have been replaced all strings // to /
	while ( path.contains( "//") )
		path.replace( "//", "/" );

	path.insert( 0, host );
}


bool VirtualFS::needToChangeFS( QString & inputURL )
{
	bool needToChange = FALSE;
	QString targetURL = inputURL;

	if ( targetURL == "../" ) {
		slotCloseCurrentDir();
		return FALSE;
	}
	else
	if ( targetURL == "//" ) {
		mListViewExt->open( host() );
		return FALSE;
	}

	parsePath( targetURL );
	if ( targetURL.isEmpty() ) { // go to previous path from LocalFS
		inputURL = targetURL;
		return TRUE;
	}

	if ( mCurrentFilesSystem != LOCALfs )
		if ( targetURL == rootName() )
			inputURL = "";


	bool targetURLisAbsolute =
		((targetURL.find("/") == 0) || (targetURL.find("ftp://") == 0) || (targetURL.find("smb://") == 0) || (targetURL.find("arc://") == 0));
	if ( ! targetURLisAbsolute )
		targetURL.insert( 0, mCurrentURL ); // make targetURL as absolute

	debug("VirtualFS::needToChangeFS, (filtered) targetURL='%s', currentFS=%d, targetFS=%d", targetURL.latin1(), mCurrentFilesSystem, kindOfFilesSystem(targetURL) );

	if ( mCurrentFilesSystem == kindOfFilesSystem(targetURL) )
		mListViewExt->open( targetURL, TRUE );
	else {
		if ( ! inputURL.isEmpty() )
			inputURL = targetURL;
		needToChange = TRUE;
	}

	return needToChange;
}


void VirtualFS::initializeShowsList()
{ // --- get info from FS and initialize the List
	mListViewExt->initFilesList( nameOfProcessedFile(), numOfAllFiles(), loggedOwner(), loggedGroup() );
}


KindOfFilesSystem VirtualFS::kindOfFilesSystem( const QString & path )
{
	KindOfFilesSystem kindOfFS = UNKNOWNfs;
	bool isArchive = FileInfoExt::isArchive(path);

	if ( (path.at(0) == '/' || path == "~") && ! isArchive )
		kindOfFS = LOCALfs;
	else
	if ( path.find("ftp://") == 0 )
		kindOfFS = FTPfs;
	else
	if ( path.find("smb://") == 0 )
		kindOfFS = SAMBAfs;
	else
	if ( path.find("arc://") == 0 || isArchive )
		kindOfFS = ARCHIVEfs;

	return kindOfFS;
}


QString VirtualFS::rootName() const
{
	if ( mCurrentFilesSystem == LOCALfs )    return QString("/");
	else
	if ( mCurrentFilesSystem == FTPfs )      return QString("ftp://");
	else
	if ( mCurrentFilesSystem == SAMBAfs )    return QString("smb://");
	else
	if ( mCurrentFilesSystem == ARCHIVEfs )  return QString("arc://");

	return QString("?");
}


void VirtualFS::showProgressDialog( Operation operation, bool move, const QString & targetPath )
{
	mProgressDlg = new ProgressDialog();
	mProgressDlg->init( operation, sCloseProgressDlgAfterFinished, move, targetPath );
	connect( mProgressDlg, SIGNAL(signalCancel()),
		this, SLOT(slotCancelOperation()) );
	connect( mProgressDlg, SIGNAL(signalCloseAfterFinished( bool )),
		this, SLOT(slotCloseAfterFinished( bool )) );
	connect( this, SIGNAL(signalNameOfProcessedFile(const QString &, const QString &)),
		mProgressDlg, SLOT(slotSetSourceInfo(const QString &, const QString &)) );
	connect( this, SIGNAL(signalFileCounterProgress(int, bool, bool)),
		mProgressDlg, SLOT(slotSetProcessedNumber(int, bool, bool)) );
	connect( this, SIGNAL(signalTotalProgress(int, long long)),
		mProgressDlg, SLOT(slotSetTotalProgress(int, long long)) );
	connect( this, SIGNAL(signalDataTransferProgress( long long, long long, uint )),
		mProgressDlg, SLOT(slotDataTransferProgress( long long, long long, uint )) );
	connect( this, SIGNAL(signalSetOperation(Operation)),
		mProgressDlg, SLOT(slotSetOperation(Operation)) );

	mOperationHasFinished = FALSE;
	mProgressDlg->setModal(TRUE);
	mProgressDlg->show();
}


void VirtualFS::removeProgressDlg( bool checkWhetherCanToClose )
{
	if ( checkWhetherCanToClose )
		if ( ! sCloseProgressDlgAfterFinished )
			return;

	if ( mProgressDlg ) {
		delete mProgressDlg;  mProgressDlg = 0;
		mListViewExt->raise();
		mListViewExt->setFocus();
	}
}


void VirtualFS::startProgressDlgTimer( bool start )
{
	if ( mProgressDlg == NULL )
		return;

	(start) ? mProgressDlg->timerStart() : mProgressDlg->timerStop();
}


void VirtualFS::showPropertiesDialog()
{
	if ( mPropertiesDialog ) {
		delete mPropertiesDialog;  mPropertiesDialog = NULL;
	}
	mListViewExt->collectAllSelectedNames( mSelectedFilesList );

	if ( mSelectedFilesList.count() == 0 )
		return;

	mPropertiesDialog = new PropertiesDialog( this );
	connect( mPropertiesDialog, SIGNAL(signalGetUrlForFile(const QString &, UrlInfoExt &)),
	 this, SLOT(slotGetUrlInfoFromList(const QString &, UrlInfoExt &)) );
	connect( mPropertiesDialog, SIGNAL(signalOkPressed(const UrlInfoExt &, bool)),
	 this, SLOT(slotChangePropertiesOfFile(const UrlInfoExt &, bool)) );
	connect( mPropertiesDialog, SIGNAL(signalRefresh( int )),
	 this, SLOT(slotCountingDirectory( int )) );
	connect( mPropertiesDialog, SIGNAL(signalClose()),
	 this, SLOT(slotClosePropertiesDlg()) );

	mPropertiesDialog->init( mSelectedFilesList, mFilesAssociation, LocalFS::getUser() ); // loggedUserName
	mOperationHasFinished = FALSE;
	mPropertiesDialog->show();
}


void VirtualFS::showFindFileDialog()
{
	if ( mFindFileDialog ) {
		delete mFindFileDialog;  mFindFileDialog = NULL;
	}
	mFindFileDialog = new FindFileDialog( this );
	connect( mFindFileDialog, SIGNAL(signalFindStart(const FindCriterion &)),
	 this, SLOT(slotStartFind(const FindCriterion &)) );
	connect( mFindFileDialog, SIGNAL(signalClose()), this, SLOT(slotCloseFindFileDlg()) );
	connect( mFindFileDialog, SIGNAL(signalPause( bool )), this, SLOT(slotPause( bool )) );
	connect( mFindFileDialog, SIGNAL(signalJumpToTheFile(const QString &)),
	 this, SLOT(slotJumpToTheFile(const QString &)) );
	connect( mFindFileDialog, SIGNAL(signalViewFile(const QString &, bool)),
	 this, SLOT(slotViewFile(const QString &, bool)) );
	connect( mFindFileDialog, SIGNAL(signalDeleteFile(const QString &)),
	 this, SLOT(slotDeleteFile(const QString &)) );

	mFindFileDialog->init( mCurrentURL, mFilesAssociation );
	mOperationHasFinished = FALSE;
	mFindFileDialog->show();
}


void VirtualFS::slotCancelOperation()
{
	if ( ! mOperationHasFinished ) {
		slotPause( TRUE );

		QString operationStr;
		Operation operation = currentOperation();
		if ( operation == Copy )
			operationStr = tr("copying"); // it's true for 'moving' too
		else
		if ( operation == Remove )
			operationStr = tr("removing");
		else
		if ( operation == SetAttributs )
			operationStr = tr("setting attributs");

		int result = MessageBox::yesNo( mProgressDlg,
		 tr("Break")+" "+operationStr+" - QtCommander",
		 tr("Do you really want to break an operation")+" ?",
		 MessageBox::Yes // default is the 'Yes' button
		);
		if ( result == MessageBox::Yes ) {
			breakCurrentOperation();
			removeProgressDlg();
			emit signalResultOperation( operation, FALSE ); // break operation is like error
		}
		else { // "No" or Esc
			mProgressDlg->timerStart(); // let's timer continue
			slotPause( FALSE ); // let's operation continue
		}
	}
	else // an operation has been finished
		removeProgressDlg();
}


void VirtualFS::slotCloseAfterFinished( bool closeState )
{
	sCloseProgressDlgAfterFinished = closeState;
}


QString VirtualFS::absoluteFileName( const QString & fileName ) const
{
	QString fullPathName = fileName;

	// mozna to troche przyspieszyc, poprzez spr. wczesniej biezacego FS-u
	// i dopiero potem czy sciezka abs.
	bool absolutePath = (
	 (fileName.at(0) == '/')        || (fileName.find("ftp://") == 0) ||
	 (fileName.find("smb://") == 0) || (fileName.find("arc://") == 0)
	);

	if ( ! absolutePath ) { // is it relative path ?
		if ( FileInfoExt::isArchive(mCurrentURL) ) {
			if ( mCurrentURL.at(mCurrentURL.length()-1) != '/' )
				fullPathName.insert( 0, '/' );
		}
		fullPathName.insert(0, mCurrentURL );
	}

	return fullPathName;
}


void VirtualFS::setCurrentURL( const QString & fullPathName )
{
	mCurrentURL = fullPathName;
}


void VirtualFS::removeItemFromSIL()
{
	mListViewExt->removeItemFromSIL( mListViewExt->findName(nameOfProcessedFile()) );
}


QString VirtualFS::absHostURL() const
{
	if ( mCurrentFilesSystem == LOCALfs && ! FileInfoExt::isArchive(mNewURL) )
		return path();

	return host().left(host().length()-1)+path();
}


bool VirtualFS::setWeighInPropetiesDlg()
{
	if ( ! mPropertiesDialog )
		return FALSE;

	long long totalWeigh;
	uint totalFiles, totalDirs;
	getWeighingStat( totalWeigh, totalFiles, totalDirs );

	mPropertiesDialog->setSize( totalWeigh, totalDirs, totalFiles, TRUE ); // bigInfo=TRUE as default value

	return TRUE;
}


void VirtualFS::initPropetiesDlgByUrl( const UrlInfoExt & urlInfo, const QString & loggedUserName )
{
	if ( ! mPropertiesDialog )
		return;

	mPropertiesDialog->init( urlInfo, mFilesAssociation, loggedUserName );

// 	long long totalWeigh;
// 	uint totalFiles, totalDirs;
// 	getWeighingStat( totalWeigh, totalFiles, totalDirs );

	mPropertiesDialog->setSize( urlInfo.size(), urlInfo.isDir() ? 1 : 0, urlInfo.isFile() ? 1 : 0, FALSE );
}


void VirtualFS::changeAttribut( ListViewExt::ColumnName column, const QString & newAttribStr )
{
	mListViewExt->clearSelectedItemsList();
	QListViewItem *currentItem = mListViewExt->currentItem();
	mListViewExt->selectedItemsList()->append( currentItem );
	mSelectedFilesList.clear(); // strings list
	mSelectedFilesList.append( ((ListViewItemExt *)currentItem)->fullName() );
	mTargetURL = newAttribStr;

	int permissions = -1;
	QDateTime lastModifiedTime;
	QString newName, owner, group;
	UrlInfoExt entryInfo = ((ListViewItemExt *)(mListViewExt->findName( mSelectedFilesList[0] )))->entryInfo();

	if ( column == ListViewExt::NAMEcol )
		newName = absoluteFileName( newAttribStr );
	else
	if ( column == ListViewExt::TIMEcol )
		decodeTime( lastModifiedTime, entryInfo.lastModified(), newAttribStr );
	else
	if ( column == ListViewExt::PERMcol )
		decodePermissions( permissions, entryInfo.permissionsStr(), newAttribStr );
	else
	if ( column == ListViewExt::OWNERcol )
		owner = newAttribStr;
	else
	if ( column == ListViewExt::GROUPcol )
		group = newAttribStr;

	entryInfo.setOwner( owner );
	entryInfo.setGroup( group );
	entryInfo.setName( newName );
	entryInfo.setPermissions( permissions );
	entryInfo.setLastModified( lastModifiedTime );
	entryInfo.setLastRead( lastModifiedTime );

	setAttributs( mSelectedFilesList, entryInfo, FALSE, 0 ); // FALSE - not recursive, 0 - changesForAll
	//mListViewExt->countAndWeightSelectedFiles();
}


void VirtualFS::decodeTime( QDateTime & dateTime, const QDateTime & oldTime, const QString & newTimeStr )
{
	QString newDTstr = newTimeStr;
	// simplify some trailing characters to such one character
	while ( newDTstr.contains(QRegExp("--+")) )
		newDTstr.replace( QRegExp("--+"), "-" );
	while ( newDTstr.contains(QRegExp("\\.+")) )
		newDTstr.replace( QRegExp("\\.\\.+"), "-" );
	while ( newDTstr.contains(QRegExp(" +")) )
		newDTstr.replace( QRegExp("  +"), "-" );
	while ( newDTstr.contains(QRegExp("::+")) )
		newDTstr.replace( QRegExp("::+"), ":" );
	while ( newDTstr.at(newDTstr.length()-1) == '-' || newDTstr.at(newDTstr.length()-1) == ':' )
		newDTstr.remove( newDTstr.length()-1, 1 );
	while ( newDTstr.at(0) == '-' || newDTstr.at(0) == ':' )
		newDTstr.remove( 0, 1 );

	uint timeStrLen = newDTstr.length();
	if ( timeStrLen > 19 )
		newDTstr = newDTstr.left( 19 );


	QDate date = oldTime.date();
	QTime time = oldTime.time();
	int year  = date.year();
	int month = date.month();
	int day   = date.day();
	int hour   = time.hour();
	int minute = time.minute();
	int second = time.second();
	int y,m,d, hh,mm,ss;
	uint offset;
	y = m = d = hh = mm = ss = -1;

	bool changeDate = ( newDTstr.find('-') != -1 || newDTstr.find('.') != -1 || timeStrLen == 4 );
	bool changeTime = ( newDTstr.find(':') != -1 );

	if ( changeDate ) {
		if ( timeStrLen == 4 ) // is year only
			y = 1;
		else
		if ( timeStrLen == 4+3 ) // is year+month
			y = m = 1;
		else
		if ( timeStrLen == 4+3+3 ) // is year+month+day
			y = m = d = 1;
	}
	if ( changeTime ) {
		offset = (changeDate) ? 4+3+3 : 0;
		if ( timeStrLen == offset+3 ) // is [year+month+day]+hour
			hh = 1;
		else
		if ( timeStrLen == offset+3+3 ) // is [year+month+day]+hour+minute
			hh = mm = 1;
		else
		if ( timeStrLen == offset+3+3+3 ) // is [year+month+day]+hour+minute+second
			hh = mm = ss = 1;
	}


	bool isNumber = FALSE;
	if ( y > -1 ) {
		y = newDTstr.section( '-', 0,0 ).toUInt( &isNumber );
		if ( isNumber )
			year = y;
	}
	if ( m > -1 ) {
		m = newDTstr.section( '-', 1,1 ).toUInt( &isNumber );
		if ( isNumber )
			if ( month<12+1 )
				month = m;
	}
	if ( d > -1 ) {
		d = newDTstr.section( '-', 2,2 ).toUInt( &isNumber );
		if ( isNumber )
			if ( day<31+1 )
				day = d;
	}

	if ( changeTime ) {
		offset = (changeDate) ? 3 : 0;
		if ( hh > -1 ) {
			hh = newDTstr.section( '-', offset+0,offset+0 ).toUInt( &isNumber );
			if ( isNumber )
				hour = hh;
		}
		if ( mm > -1 ) {
			mm = newDTstr.section( '-', offset+1,offset+1 ).toUInt( &isNumber );
			if ( isNumber )
				if ( month<12+1 )
					minute = mm;
		}
		if ( ss > -1 ) {
			ss = newDTstr.section( '-', offset+2,offset+2 ).toUInt( &isNumber );
			if ( isNumber )
				if ( day<31+1 )
					second = ss;
		}
	}

	dateTime.setDate( QDate(year, month, day) );
	dateTime.setTime( QTime(hour, minute, second) );
}


void VirtualFS::decodePermissions( int & permissions, const QString & oldPermStr, const QString & newPermStr )
{
	bool isOctalNumber;
	int  permStrLen = newPermStr.length()-1;
	if ( permStrLen > 9 )
		permStrLen = 9;

	// decode from octal
	int perm = newPermStr.toUInt( &isOctalNumber, 8 );
	if ( isOctalNumber ) {
		if ( perm < 36864 ) // 107777 it's max value (in decimal == 36863)
			permissions = perm;

		return;
	}

	// decode from string
	QChar c, pChr;
	QString newPerm = oldPermStr;
	for (int i=0; i<permStrLen; i++) {
		c = newPermStr[i].lower();
		if ( c == 'r' || c == 'w' || c == 'x' )
			pChr = c;
		else
		if ( c == 's' || c == 't' )
			pChr = newPermStr[i];
		else
			pChr = '-';

		newPerm[i] = pChr;
	}

	UrlInfoExt urlInfo;
	urlInfo.setPermissionsStr( newPerm );
	permissions = urlInfo.permissions();
}


void VirtualFS::runExternalViewer( const QStringList & filesToView )
{
	if ( mFilesAssociation == NULL || ! mFilesAssociation->loaded() )
		return;

	QString cmd;
	QStringList argList;
	for ( uint i=0; i<filesToView.count(); i++ ) {
		cmd = mFilesAssociation->appsForView(filesToView[i])+"\n"+filesToView[i];
		argList = QStringList::split( '\n', cmd );
		//if ( QFileInfo(argList[0]).exists() ) {
			mProcess.setArguments( argList );
			mProcess.start();
		//}
	}
}

	// ------- SLOTs --------

void VirtualFS::slotSetCurrentURL( const QString & newPath )
{
	mCurrentURL = newPath;
}


void VirtualFS::slotListInfo( const UrlInfoExt & urlInfo )
{
	mListViewExt->slotInsertItem( urlInfo );
}


void VirtualFS::slotShowFileOverwriteDlg( const QString & sourceFileName, QString & targetFileName, int & result )
{
	UrlInfoExt sourceURL, targetURL;
	slotGetUrlInfo( sourceFileName, sourceURL );
	slotGetUrlInfo( targetFileName, targetURL );
	startProgressDlgTimer( FALSE ); // stop
	if ( result != FileOverwriteDialog::Rename ) // force to show only rename dlg.
		result = FileOverwriteDialog::show( this, targetURL, sourceURL );

	if ( result == FileOverwriteDialog::Rename ) {
		bool okPressed;
		bool p1, p2; // dummy
		QString newFileName = PreOperationDialog::getText(
		 "\""+targetFileName+"\"", FileInfoExt::fileName(targetFileName), Rename,
		 "/qtcmd/FilesSystem/RenameURLs",
		 okPressed, p1, p2
		);
		if ( okPressed ) {
			if ( newFileName.at(0) != '/' )
				newFileName.insert( 0, FileInfoExt::filePath(targetFileName) );
			targetFileName = newFileName;
			if ( mCurrentURL == FileInfoExt::filePath(targetFileName) ) {
				// need to replace name into selected list sourceFileName to targetFileName
				QValueList<QListViewItem *>::iterator it;
				it = mListViewExt->selectedItemsList()->find( mListViewExt->findName(sourceFileName) );
				(*it)->setText( 0, FileInfoExt::fileName(targetFileName) );
				// nazwy tych elementow trzeba zapisac na QStringList, a po wyw. signalResultOperation - ustawic nazwy
			}
		}
		else
			result = FileOverwriteDialog::No; // force skip file
	}

	if ( result != FileOverwriteDialog::Cancel )
		startProgressDlgTimer( TRUE );
}


void VirtualFS::slotShowDirNotEmptyDlg( const QString & dirName, int & result )
{
	startProgressDlgTimer( FALSE ); // stop

	QStringList btnsNames;
	btnsNames << tr("&Yes") << tr("&No") << tr("&All") << tr("N&one") << tr("&Cancel");
	result = MessageBox::common( this,
		tr("Delete directory")+" - QtCommander",
		dirName+"\n\n\t"+tr("Directory not empty")+".\n\t"+tr("Delete it recursively ?"),
		btnsNames, MessageBox::Yes, QMessageBox::Warning
	);

	if ( result != MessageBox::Cancel ) // continue operation
		startProgressDlgTimer( TRUE );
}


void VirtualFS::slotChangePropertiesOfFile( const UrlInfoExt & newUrlInfo, bool allSelected )
{
	if ( ! allSelected ) {
		QString currentFileName = mSelectedFilesList[mPropertiesDialog->currentItem()];
		if ( currentFileName.isEmpty() ) {
			mPropertiesDialog->close();
			return;
		}
		mSelectedFilesList.clear();
		mSelectedFilesList.append( currentFileName );

		if ( ! newUrlInfo.name().isEmpty() )
			mTargetURL = absoluteFileName(newUrlInfo.name()); // for newAttributName()
	}

	setAttributs( mSelectedFilesList, newUrlInfo,
	 mPropertiesDialog->isRecursive(), mPropertiesDialog->changesFor()
	); // virtual

	mPropertiesDialog->close();
}


void VirtualFS::slotCountingDirectory( int itemNum ) // use by PropertiesDialog
{
	mOperationHasFinished = FALSE;
	if ( itemNum < 0 )
		sizeFiles( mSelectedFilesList, FALSE ); // TODO parameter need to set by option (Real weigh in FS)
	else
		sizeFiles( mSelectedFilesList[itemNum], FALSE );
}


void VirtualFS::slotClosePropertiesDlg()
{
	delete mPropertiesDialog; mPropertiesDialog = 0;
	breakCurrentOperation();
}


void VirtualFS::slotCloseFindFileDlg()
{
	delete mFindFileDialog; mFindFileDialog = 0;
	breakCurrentOperation();
}


void VirtualFS::slotStartFind( const FindCriterion & findCriterion )
{
	if ( findCriterion.isEmpty() )
		mFindFileDialog->findFinished();
	startFindFile( findCriterion );
}


void VirtualFS::slotInsertMatchedItem( const UrlInfoExt & urlInfo )
{
	if ( mFindFileDialog )
		mFindFileDialog->insertMatchedItem( urlInfo );
}


void VirtualFS::slotUpdateFindStatus( uint matches, uint files, uint directories )
{
	if ( mFindFileDialog )
		mFindFileDialog->updateFindStatus( matches, files, directories );
}


void VirtualFS::slotJumpToTheFile( const QString & fileName )
{
	mListViewExt->open( FileInfoExt::filePath(fileName) );
	mListViewExt->moveCursorToName( fileName );
}


void VirtualFS::slotViewFile( const QString & fileName, bool editMode )
{
	openFile( fileName, editMode );
}


void VirtualFS::slotDeleteFile( const QString & fileName )
{
	deleteFiles( fileName );
}


void VirtualFS::slotGetUrlInfoFromList( const QString & fileName, UrlInfoExt & urlInfo )
{
	ListViewItemExt *item = (ListViewItemExt *)mListViewExt->findName( fileName );
	if ( item )
		urlInfo = item->entryInfo();
}


void VirtualFS::slotRereadFilesAssociation()
{
	mFilesAssociation->readFilesAssociation();
}

// -------- TYMCZASOWA FUNKCJA DLA TESTOW File Systemu
QString VirtualFS::cmdString( Operation op )
{
	QString s = "Unknown";

			if ( op == HostLookup ) s = "HostLookup";
	else if ( op == NoneOp )   s = "NoneOp";
	else if ( op == Connect )  s = "Connect";
	else if ( op == List )     s = "List";
	else if ( op == Open )     s = "Open";
	else if ( op == Cd )       s = "Cd";
	else if ( op == Rename )   s = "Rename";
	else if ( op == Remove )   s = "Remove";
	else if ( op == MakeDir )  s = "MakeDir";
	else if ( op == MakeLink ) s = "MakeLink";
	else if ( op == Get )      s = "Get";
	else if ( op == Put )      s = "Put";
	else if ( op == Touch )    s = "Touch";
	else if ( op == Copy )     s = "Copy";
	else if ( op == Move )     s = "Move";
	else if ( op == Weigh )    s = "Weigh";
	else if ( op == Find )     s = "Find";
	else if ( op == SetAttributs ) s = "SetAttributs";

	return s;
}

