
#include <qapplication.h>
#include <qtranslator.h>
#include <qtextcodec.h>

#include "qtcmd.h"


int main( int argc, char ** argv )
{
	QApplication a( argc, argv );
	//get parent dir.for binary file location dir.
	QString appPath = qApp->applicationDirPath();	
	QString translationFilePath = appPath.left(appPath.findRev('/') + 1);	
	//get locale in 'ab' format (not 'ab_AB')
	QString locale = QString(QTextCodec::locale()).left(2);
	// find corect translation path
	if (QFile::exists(translationFilePath + "share/locale/" + locale + "/LC_MESSAGES/qtcmd.qm"))
		translationFilePath += "share/locale/" + locale + "/LC_MESSAGES/";
	else // executed from source project directory
		translationFilePath += "translations/" + locale + "/";
	
	debug("translationFilePath=%s", translationFilePath.latin1());
	// init and sets translations for application
	QTranslator torApp;
	if (torApp.load( QString("qtcmd"), translationFilePath))
		a.installTranslator(&torApp);
	// init and sets translations for application settings (plugin)
	QTranslator torAppSetts;
	if (torAppSetts.load(QString("qtcmd-setts"), translationFilePath))
		a.installTranslator(&torAppSetts);
	// init and sets translations for view settings (plugin)
	QTranslator torViewSetts;
	if (torViewSetts.load(QString("qtcmd-view-setts"), translationFilePath))
		a.installTranslator(&torViewSetts);
	// init and sets translations for text view (plugin)
	QTranslator torTextView;
	if (torTextView.load(QString("qtcmd-view-text"), translationFilePath))
		a.installTranslator(&torTextView);
	// init and sets translations for library "uiext"
	QTranslator torLibUiExt;
	if (torLibUiExt.load(QString("qtcmd-uiext"), translationFilePath))
		a.installTranslator(&torLibUiExt);
	// init and sets translations for library "utils"
	QTranslator torLibUtils;
	if ( torLibUtils.load(QString("qtcmd-utils"), translationFilePath))
		a.installTranslator(&torLibUtils);
	// init and sets translations for library "dlgext"
	QTranslator torLibDlgExt;
	if (torLibDlgExt.load(QString("qtcmd-dlgext"), translationFilePath))
		a.installTranslator(&torLibDlgExt);


	QtCmd *mw = new QtCmd( a.argv()[ 1 ], a.argv()[ 2 ] );
	mw->setCaption( "QtCommander" );
	mw->show();

	a.connect( &a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()) );
	return a.exec();
}
