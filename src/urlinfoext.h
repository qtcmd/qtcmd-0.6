/***************************************************************************
                          urlinfoext.h  -  description
                             -------------------
    begin                : tue oct 14 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _URLINFOEXT_H_
#define _URLINFOEXT_H_

#include "enums.h"
#include "functions.h" // for fun.: formatNumber()

#include <qurlinfo.h>

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa rozszerzonej informacji o pliku.
 Klasa udost�pnia dodatkowe metody wzgl�dem klasy po kt�rej dziedziczy. Omini�te
 zosta�o ograniczenie tej klasy dla plik�w wi�kszych ni� 2GB. Dost�pne s�
 dodatkowe metody zwracaj�ce sformatowany (np. wg wagi) rozmiar pliku, oraz
 czas ostatniej modyfikacji. Zapisywane jest tu wskazanie linka i informacja
 o pustym katalogu.
 */
class UrlInfoExt : public QUrlInfo
{
public:
	/** Pierwszy konstruktor klasy.\n
	 @param name - nazwa,
	 @param size - rozmiar,
	 @param permissions - parawa dost�pu,
	 @param owner - nazwa w�a�ciciela,
	 @param group - nazwa groupy,
	 @param lastModified - data i czas ostatniej modyfikacji,
	 @param lastRead - data i czas ostatniego odczytu,
	 @param dir - TRUE mowi, �e element to katalog
	 @param file - TRUE oznacza, �e element jest to plikiem,
	 @param symLink - warto�� TRUE m�wi, �e element to link symboliczny,
	 @param readable - TRUE oznacza, �e plik lub katalog jest odczytywalny
	 @param executable - TRUE mowi, �e plik lub katalog jest uruchamialny,
	 @param empty - je�li element jest katalogiem i znaje si� na partycji nie FAT,
	  wtedy TRUE oznacza �e jest on, pusty, w przeciwnym razie mo�e zawiera� pliki
	 @param linkTarget - je�li element jest linkiem wtedy znajduje si� tutaj
	  jego dowi�zanie.\n
	 Inicjowane s� tu sk�adowe klasy.
	 */
	UrlInfoExt( const QString & name, long long size, int permissions, const QString & owner, const QString & group, const QDateTime & lastModified, const QDateTime & lastRead, bool dir, bool file, bool symLink, bool readable, bool executable, bool empty=FALSE, const QString & linkTarget=QString::null );

	/** Drugi konstruktor klasy.\n
	 Tworzony jest tu pusty obiekt.
	 */
	UrlInfoExt() { clear(); }

	/** Destruktor.\n
	 Brak definicji (jest pusta).
	 */
	virtual ~UrlInfoExt() {}

	/** Ustawia prawa dost�pu podane jako ci�g tekstowy.\n
	 @param permStr - prawa dost�pu.
	 */
	void setPermissionsStr( const QString & permStr );

	/** Zwraca rozmiar pliku.\n
	 @return rozmiar w bajtach.
	 */
	long long size() const { return mSize; }

	/** Ustawia rozmiar na podany.\n
	 @param newSize - rozmiar w bajtach.
	 */
	void setSize( long long newSize ) { mSize = newSize; }


	/** Zwraca prawa dost�pu w postaci ci�gu tekstowego.\n
	 @return prawa dost�pu.
	 */
	QString permissionsStr() const;

	/** Zwraca sformatowany rozmiar pliku, wg podanej regu�y.\n
	 @param kof - rodzaj formatowania, patrz: @see KindOfFormating.
	 */
	QString sizeStr( KindOfFormating kof=NONEformat ) const { return formatNumber(mSize, kof); }

	/** Zwraca czas ostatniej modyfikacji w postaci sformatowanego ci�gu
	 tekstowego.\n
	 @return czas ostatniej modyfikacji.
	 */
	QString lastModifiedStr() const { return lastModified().toString( "yyyy-MM-dd hh:mm" ); }
//	QString lastModifiedStr( KindOfFormatingDate kofd ) const { return formatDate(lastModified(), kofd); }


	/** Zwraca informacj� o tym czy katalog jest pusty.\n
	 @return TRUE, je�li katalog jest pusty, w przeciwnym razie FALSE.\n
	 Przy czym zwracana jest prawda tylko je�li, element jest katalogiem i
	 znajduje si� on na Linuksowej partycji.
	 */
	bool isEmpty() const { return mEmptyFile; }
// 	void setEmpty( bool empty ) { mEmptyFile = empty; }


	/** Zwraca informacj� o tym czy element jest uruchamialny.\n
	 @return TRUE, je�li element jest uruchamialny, w przeciwnym razie FALSE.
	 */
	bool isExecutable() const { return (permissions() & 0111); }

//	bool specialFile( int specialFile ) const { return (mSpecialFile & specialFile); }
//	bool specialPermission( int specialPermission ) const { return (permissions() & specialPermission); }
//	bool isSymLinkToFile() const { return (isSymLink() && isFile()); }
//	bool isSymLinkToDir() const  { return (isSymLink() && isDir());  }


	/** Metoda zeruje element.
	 */
	void clear();


	/** Zwraca czas ostatniego odczytu.\n
	 @param czas ostatniego odczytu.
	 */
	QDateTime lastRead() const { return mLastRead; }

	/** Ustawia czas ostatniego odczytu na podany.\n
	 @param lastRead - czas ostatniego odczytu
	 */
	void setLastRead( const QDateTime & lastRead ) { mLastRead = lastRead; }


	/** Zwraca dowi�zanie linka (je�li element jest linkiem).\n
	 @return dowi�zanie linka.
	 */
	QString linkTarget() const { return mLinkTargetFileStr; }

private:
	long long mSize;
	bool mEmptyFile;
//	int mSpecialFile;

	QDateTime mLastRead;
	QString mLinkTargetFileStr;

};

#endif
