/***************************************************************************
                          listviewitemext.cpp  -  description
                             -------------------
    begin                : Mon Jul 9 2001
    copyright            : (C) 2002 by Piotr Mierzwiński
    email                : peterm@go2.pl

    Copyright: See COPYING file that comes with this project
 ***************************************************************************/

#include "fileinfoext.h" // for isArchive()
#include "listviewext.h"
#include "listviewitemext.h"

//#include "../icons/folder.xpm"
//#include "../icons/folder_lock.xpm"
#include "../icons/folder_open.xpm"
#include "../icons/package_open.xpm"
//#include "../icons/folder_link.xpm"

#include <qpainter.h>


ListViewItemExt::ListViewItemExt( ListViewExt *parent )
	: QListViewItem( parent ), mListView(parent)
{
	mListViewItemExt = 0;
	mShowDirSize = FALSE;
	mHeader = parent->header();

// 	parent->setPaletteBackgroundColor( Qt::white );
	parent->setColor( QColorGroup::Text, black ); // setTextColor
	parent->setColor( QColorGroup::Highlight, white ); // setHighlightBgColor
	parent->setColor( QColorGroup::HighlightedText, blue );
	//setHighlightedTextColor, highlightedTextColor()
}


ListViewItemExt::ListViewItemExt( ListViewItemExt *parent ) : QListViewItem( parent ) {}


ListViewItemExt::ListViewItemExt( ListViewExt *parent, const UrlInfoExt & urlInfo )
	: QListViewItem( parent, urlInfo.name() ), mEntryInfo( urlInfo )
{
	mListViewItemExt = 0;
	mShowDirSize = FALSE;
	mIsDir = urlInfo.isDir();

	if ( parent->showIcons() )
		setPixmap( *parent->icon( urlInfo ) );
}


ListViewItemExt::ListViewItemExt( ListViewItemExt *parent, const UrlInfoExt &urlInfo )
	: QListViewItem( parent, urlInfo.name() ), mEntryInfo( urlInfo )
{
	mShowDirSize = FALSE;
	mListViewItemExt = parent;
	mIsDir = mEntryInfo.isDir();

	if ( listViewExt()->showIcons() )
		setPixmap( *listViewExt()->icon(urlInfo) );
}


void ListViewItemExt::show()
{
	if ( height() > 0 )
		return;

	QFontMetrics fm( QFont(
	 ((ListViewExt *)listView())->fontFamily(), // listViewExt()->fontFamily() powoduje: "syntax error before ->"
	 listViewExt()->fontSize(),
	 QFont::Light)
	);

	QListViewItem::setHeight( fm.height()+4 ); // 4 for margin
}


void ListViewItemExt::hide()
{
	if ( height() > 0 )
		QListViewItem::setHeight(0);
}


QString ListViewItemExt::key( int column, bool ascending ) const
{
	bool dots = (text(ListViewExt::NAMEcol) == "..");
	char prefix;

	if ( ascending )
		prefix = dots ? '0' : ( mIsDir ? '1' : '2' );
	else
		prefix = dots ? '4' : ( mIsDir ? '3' : '2' );

	QString key = (column != 1) ? text( column ) : text( column ).rightJustify( 24, '0' );

	return prefix + key;
}


QString ListViewItemExt::text( int column ) const
{
	if ( mEntryInfo.name().isEmpty() )
		return QString("");

	switch ( column ) {
		case 0:
			return mEntryInfo.name();
		case 1:
			return (mEntryInfo.isDir()) ? (mShowDirSize ? mEntryInfo.sizeStr(BKBMBGBformat) : QString("<DIR>")) : mEntryInfo.sizeStr();
		case 2:
			return mEntryInfo.lastModifiedStr();
		case 3:
			return mEntryInfo.permissionsStr();
		case 4:
			return mEntryInfo.owner();
		case 5:
			return mEntryInfo.group();
	}

	return QString("???");
}


void ListViewItemExt::setText( int column, const QString & str )
{
//	int clmn = listViewExt()->sectionMap( column );

			 if ( column == ListViewExt::NAMEcol  )  mEntryInfo.setName( str );
	// QString in Qt-3.1.x not supports 'long long' type !
//	else if ( clmn == ListViewExt::SIZEcol  )  mEntryInfo.setSize( QString(str).toDouble() );
	else if ( column == ListViewExt::SIZEcol  )  mEntryInfo.setSize( QString(str).toLongLong() );
	else if ( column == ListViewExt::TIMEcol  )  mEntryInfo.setLastModified( QDateTime::fromString(str, Qt::LocalDate/*Qt::ISODate*/) );
	else if ( column == ListViewExt::PERMcol  )  mEntryInfo.setPermissionsStr( str );
	else if ( column == ListViewExt::OWNERcol )  mEntryInfo.setOwner( str );
	else if ( column == ListViewExt::GROUPcol )  mEntryInfo.setGroup( str );
}


void ListViewItemExt::setPixmap( QPixmap p )
{
	QListViewItem::setPixmap( ListViewExt::NAMEcol, p );
}


void ListViewItemExt::setup()
{
	if ( listViewExt()->isTreeView() ) {
		setExpandable( FALSE );
		if ( mIsDir )
			if ( mEntryInfo.isReadable() && ! mEntryInfo.isEmpty() )
				setExpandable( TRUE );
	}

	QListViewItem::setup();
}


void ListViewItemExt::setOpen( bool open )
{
	bool isTreeView = listViewExt()->isTreeView();

	if ( isTreeView ) {
		QListViewItem::setOpen( open );
		if ( open ) {
			if ( FileInfoExt::isArchive(text(0)) )
				setPixmap( QPixmap(package_open) );
			else
				setPixmap( QPixmap(folder_open) );
		}
		if ( mEntryInfo.name() == "" )
			hide();
	}
}


QString ListViewItemExt::fullName( bool addSlashIfDir )
{
	QString name;
	ListViewExt *lve = listViewExt();

	if ( lve == NULL )
		return name;

	if ( lve->isTreeView() ) {
		if ( mIsDir || mEntryInfo.isFile() )
			name = mListViewItemExt->fullName();
	}
	else // the ListView
		name = lve->mCurrentURL;

	name += text(0); // add the current item name

	if ( mIsDir && addSlashIfDir )
		if ( name != lve->mHost )
			name += '/';

	return name;
}


void ListViewItemExt::paintCell( QPainter *p, const QColorGroup &cg, int column, int width, int alignment )
{
	/*
	if (column == ListViewExt::NAMEcol)
	{
		if (height() == 0)
			debug("invisible, name=%s", text( mListView->sectionMap(column) ).latin1());
		else
			debug("visible, name=%s, height=%d", text( mListView->sectionMap(column) ).latin1(), height());
	}
	*/
	if ( ! p /*|| height() == 0*/ )
		return;

	mListView = listViewExt();
	mHeader   = mListView->header();

	if ( mListView->mEnableTwoColorsOfBg ) {
		QColor color = itemPos()%(height()*2) ? mListView->mSecondColorOfBg : mListView->backgroundColor();
		p->fillRect( 0, 0, width, height(), color );  // set background color
	}
	else
		p->fillRect( 0, 0, width, height(), mListView->backgroundColor() );  // draw background color

	if ( mListView->mShowGrid )  {
		//p->setPen( Qt::DotLine );
		p->setPen( mListView->mGridColor );
		p->drawLine( 0, height()-1,  width, height()-1 );
		p->drawLine( width-1, 0,  width-1, height()-1 );
	}

	int r = mListView ? mListView->itemMargin() : 1;
	int marg = mListView ? mListView->itemMargin() : 1;

	if ( mListView->showIcons() && column == ListViewExt::NAMEcol )  {
		const QPixmap *icon = pixmap( 0 );
		if ( icon )  {
			p->drawPixmap( r, ((height() - icon->height()) / 2)-1, *icon );
			r += icon->width() + listView()->itemMargin();
		}
	}
	else
		r += 2;

	mTxt = text( mListView->sectionMap(column) );

	if ( ! mTxt.isEmpty() ) {
		int Align, fontWeight;

		fontWeight = QFont::Light;
		if ( column == ListViewExt::NAMEcol && isDir() )  fontWeight = QFont::Bold;

		if ( mListView->sectionMap(column) == ListViewExt::SIZEcol )
			Align = (isDir()) ? AlignCenter : AlignRight | AlignVCenter;
		else
			Align = alignment | AlignVCenter;

		uint margin = 3;
		if ( mListView->sectionMap(column) == ListViewExt::SIZEcol )
			if ( isDir() )  margin = 0;
			else
				mListView->formatNumberStr( mTxt );

		int cfi = (column == ListViewExt::NAMEcol) ? mListView->mIconWidth : 0;
		int w= mHeader->sectionSize( column ) - cfi - margin;

		if ( isSelected() )
			p->setPen( mListView->highlightedTextColor() );  // set color of select
		else
		{
			p->setPen( mListView->textColor() );
			if ( column == ListViewExt::NAMEcol )
			{
				QString sDevPath = "/dev/"; // FIXME tymczasowe rozw.
				if ( FileInfoExt::specialFile(sDevPath+mTxt, FileInfoExt::CHRDEV) )
					p->setPen( mListView->characterDevColor() );
				else
				if ( FileInfoExt::specialFile(sDevPath+mTxt, FileInfoExt::BLKDEV) )
					p->setPen( mListView->blockDevColor() );
				else
				if ( FileInfoExt::specialFile(sDevPath+mTxt, FileInfoExt::FIFO) )
					p->setPen( mListView->fifoColor() );
				else
				if ( FileInfoExt::specialFile(sDevPath+mTxt, FileInfoExt::SOCKET) )
					p->setPen( mListView->socketColor() );
				else
				if ( entryInfo().isSymLink() )
				{
					p->setPen( mListView->brokenSymlinkColor() );
					if ( entryInfo().isFile() || entryInfo().isDir() )
						p->setPen( mListView->symlinkColor() );
				}
				else
				if ( entryInfo().isExecutable() && ! entryInfo().isDir() )
					p->setPen( mListView->executableFileColor() );
			}
		}

		p->setFont(  QFont(mListView->mFontFamily, mListView->mFontSize, fontWeight)  );
		p->drawText(  r, 1, width-marg-r, height(), Align, trimedString( mTxt, column, w, fontWeight )  );
	}
}


void ListViewItemExt::paintFocus( QPainter *p, const QColorGroup &, const QRect & r )
{
	mListView = listViewExt();
	mHeader   = mListView->header();
	bool isTreeView = mListView->isTreeView();

	int xT = (isTreeView) ? r.x()+1 : r.x();
	int wT = (isTreeView) ? mHeader->headerWidth()-xT : mHeader->headerWidth();

	if ( mListView->isFullCursor() )
		p->fillRect( r, mListView->cursorColor() );
	else  {  //  draws a two colors double frame
		p->setPen( mListView->frameOutCursorColor() );
		p->drawRect( xT, r.y(), wT, height() );
		p->setPen( mListView->frameInsCursorColor() );
		p->drawRect( xT+1, r.y() + 1, wT - 2, height() - 2 );
	}

	if ( isSelected() )
		p->setPen( mListView->highlightedTextColorUnderCursor() );
	else
		p->setPen( mListView->textColorUnderCursor() );

	int cfx, cfi; // correction for x pos., correction for icon (only first column)
	int fontWeight, w, clmn;
	unsigned int x, y = r.y() + 1, align, margin;

	// draw contents of all columns
	for (int column=0; column<6; column++ )
	{
		align = AlignLeft | AlignVCenter;  margin = 3;  cfx = 0;  cfi = 0;
		clmn = mListView->sectionMap( column );
		mTxt = text( clmn );

		if ( column == ListViewExt::NAMEcol )  // draw text (and pixmap) for first column (name of file)
		{
			if ( mListView->showIcons() )  {
				const QPixmap *icon = pixmap( 0 );
				if ( icon )
					p->drawPixmap( (isTreeView) ? xT : 1-mHeader->offset(), y, *icon );
			}
			fontWeight = ( isDir() ) ? QFont::Bold : QFont::Light;
			cfx = (mListView->showIcons()) ? 1 : 0;
			cfi = mListView->mIconWidth;
		}
		else
			fontWeight = QFont::Light;

		if ( clmn == ListViewExt::SIZEcol ) {
			if ( isDir() )  {
				align = AlignCenter;  margin = 0;  cfx = -1;
			}
			else {
				align = AlignRight | AlignVCenter;  cfx = 1;
				mListView->formatNumberStr( mTxt );
			}
		}

		if ( isTreeView && column == ListViewExt::NAMEcol ) {
			x = xT + cfi + margin + cfx - 3;
			w = wT - cfi - margin;
		}
		else {
			x = mHeader->sectionPos( column ) - mHeader->offset() + cfi + margin;
			w = mHeader->sectionSize( column ) - cfi - margin;
		}
		int cw = mHeader->sectionSize( column ) - cfi - margin;

		p->setFont(  QFont( mListView->mFontFamily, mListView->mFontSize, fontWeight )  );
		p->drawText(  x-cfx, y,  w, r.height(),  align, trimedString(mTxt, column, cw, fontWeight)  );
	}
}


QString ListViewItemExt::trimedString( const QString &str, const int &column, const int &columnWidth, const int &fontWeight )
{
	bool truncate = false;
	QString txtStr = str, tmpTxt = "...";
	QFontMetrics fm( QFont( mListView->mFontFamily, mListView->mFontSize, fontWeight ) );

	int treeMargin = 0;
	if ( column == ListViewExt::NAMEcol && mListView->isTreeView() )
		treeMargin = 20 * depth(); // default: mListView->treeStepSize() == 20

	if ( fm.width( txtStr )+treeMargin > columnWidth )  {
		truncate = true;
		int i = 0;
		while ( fm.width( tmpTxt + txtStr[ i ] )+treeMargin < columnWidth )
			tmpTxt += txtStr[ i++ ];

		tmpTxt.remove( 0, 3 );
		if ( tmpTxt.isEmpty() )  tmpTxt = txtStr.left( 1 );

		tmpTxt += "...";
	}
	if ( truncate )
		txtStr = tmpTxt;

	return txtStr;
}

// ////////////// --------- ListViewExtToolTip --------- ///////////////

ListViewExtToolTip::ListViewExtToolTip( QWidget * parent, ListViewExt *lv )
	: QToolTip( parent ), mView(lv)
{
}

void ListViewExtToolTip::maybeTip( const QPoint & pos )
{
	if ( ! parentWidget() || ! mView )
		return;

	QListViewItem *item = mView->itemAt( pos );
	if ( ! item )
		return;

	QHeader *header = mView->header();

	int column = header->sectionAt( pos.x()+header->offset() );
	bool itemIsDir = ((ListViewItemExt *)item)->isDir();
	bool isNameColumn = (column == ListViewExt::NAMEcol);
	int fontWeight = (itemIsDir) ? QFont::Bold : QFont::Light;
	int ic = (isNameColumn) ? mView->iconWidth() : 0;
	int xtc = (mView->isTreeView() && isNameColumn) ? (mView->treeStepSize() * (item->depth()-1)) + ic : 0;
	int xc = 3 + ic + xtc;
	int columnWidth = header->sectionSize( column ) - xc;
	int xItemPos = header->sectionPos( column ) - header->offset() + xc;
	QRect r = QRect( xItemPos, mView->itemPos(item) - mView->contentsY(), columnWidth, item->height() );

	QString txt = item->text( column );

	if ( column == ListViewExt::SIZEcol )
		if ( ! itemIsDir )
			mView->formatNumberStr( txt );

	int xtcw = 0;
	if ( mView->isTreeView() && isNameColumn )
		xtcw += 4;

	if ( r.contains( pos ) )  {
		QFontMetrics fm( QFont( mView->fontFamily(), mView->fontSize(), fontWeight ) );
		if ( fm.width( txt )+xtcw > columnWidth )
			tip( r, txt );
		else
		if ( column == ListViewExt::NAMEcol )
			if ( ((ListViewItemExt *)item)->entryInfo().isSymLink() )
				tip( r, ((ListViewItemExt *)item)->entryInfo().linkTarget() );
	}
}
