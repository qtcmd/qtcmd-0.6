/***************************************************************************
                          tar.h  -  description
                             -------------------
    begin                : tue dec 23 2003
    copyright            : (C) 2003 by Piotr Mierzwi�ski
    email                : peterm@go2.pl

    copyright            : See COPYING file that comes with this project
 ***************************************************************************/

#ifndef _TAR_H_
#define _TAR_H_

#include "enums.h"
#include "virtualarch.h"

/**
 * Author:
 * Piotr Mierzwi�ski
 **/
/** @short Klasa obs�uguj�ca archiwa tar, tar.bz2 oraz tar.gz.
 Przygotowywane s� tutaj polecenia wykonywane na archiwach (np. listowania),
 kt�re s� p�niej uruchamiane na rzecz archiwum. Polecenia wykonywane s�
 asynchronicznie (po uruchomieniu nie blokuj� interfejsu u�ytkownika). Klasa
 zajmuje si� tak�e wst�pn� obr�bk� danych wyj�ciowych podstawowych w wyniku
 uruchomienia podanego polecenia, np. listowania, wypakowywania, itp.
*/
class Tar : public VirtualArch
{
	Q_OBJECT
public:
	/** Konstruktor klasy.\n
	 @param parent - wska�nik na rodzica,
	 @param kindOfArchive - rodzaj archiwum, patrz typ @see KindOfArchive.
	 @param compressionLevel - poziom kompresji,
	 @param name - nazwa dla obiektu.\n
	 Inicjowane s� tutaj niezb�dne sk�adowe klasy.
	 */
	Tar( QWidget *parent, KindOfArchive kindOfArchive, int compressionLevel=0, const char *name=0 );

	/** Destruktor.\n
	 Brak definicji.
	 */
	~Tar() {}

	/** Metoda powoduje asynchroniczne uruchamienie procesu.\n
	 @param archOperation - operacja do wykonania na archiwum
	 @param filesList - lista plik�w, na kt�rej nale�y wykona� podan� oparacj�
	 */
	void runProcess( ArchOperation archOperation, const QStringList & filesList );

	/** Metoda wykonuje zmian� katalogu w archiwum.\n
	 @param dirName - nazwa nowego katalogu w archiwum.\n
	 Przy czym zmiana, nie powoduje listowania.
	 */
	void cd( const QString & dirName );

	/** Metoda powoduje listowanie podanego archiwum.\n
	 @param dirName nazwa archiwum do wylistowania, przy czym mo�e by� tu r�wnie�
	 podana nazwa katalogu z wewn�trza archiwum.
	 */
	void list( const QString & dirName );

	/** Lista plik�w z archiwum dla podgl�du pliku przy u�yciu klawisza F3.\n
	 @param archiveName - nazwa archiwum.\n
	 Uwaga. Brak definicji.
	 */
	void quickList( const QString & archiveName ) {}

	/** Metoda uruchamia do��czanie podanych plik�w do archiwum.\n
	 @param filesList - lista nazw plik�w kt�re nale�y do��czy� do archiwum.
	 */
	void append( const QStringList & filesList );

	/** Metoda uruchamia usuni�cie podanych plik�w z archiwum.\n
	 @param filesList - lista nazw plik�w do usuni�cia z archiwum.
	 */
	void remove( const QStringList & filesList );

	/** Metoda uruchamia wypakowywanie podanych plik�w z archiwum.\n
	 @param filesList - lista plik�w, kt�re nale�y wypakowa� z archiwum,
	 @param all - r�wne TRUE, oznacza �e nale�y wypakowa� ca�e archiwum.
	 */
	void extract( const QStringList & filesList, bool all=FALSE );

	/** Zwraca kod b��du dla ostatniej operacji na archiuwm.
	 @return kod b��du.
	 */
	Error errorCode() const { return mErrorCode; }

	/** Zwraca nazw� aktualnie przetwarzanego pliku z absolutn� �cie�k�.\n
	 @return nazwa aktualnie przetwarzanego pliku.
	 */
	QString nameOfProcessedFile() const { return mNameOfProcessedFile; }

	/** Zwraca bie��cy stan operacji na archiwum.\n
	 @return stan operacji na archiwum (warto�� typu @see VirtualArch::ArchState).
	 */
	int state() const { return mState; }

	/** Pobierane s� tu wyniki operacji wa�enia, tj. waga ca�o�ci, ilo�� plik�w
	 i katalog�w \n
	 @param weight - zmienna, w kt�rej zapisana b�dzie waga,
	 @param files - zmienna do zapisania ilo�ci plik�w,
	 @param dirs - zmienna do zapisania ilo�ci katalog�w.
	 */
	void getWeighingStat( long long & weigh, uint & files, uint & dirs );

private:
	Error mErrorCode;
	QString mNameOfProcessedFile;
	QStringList mArgumentsList, mListFilesToProcessed, mFilesListBuffer;
	ArchOperation mArchOperation;

	KindOfArchive mKindOfArchive;
	int mCompressionLevel, mDirHierachyLevel;
	ArchState mState;

	QString mBuffer, mErrBuffer;

protected:
	/** Zwraca identyfikator bie��cej operacji na archiwum.\n
	 @return identyfikator bie��cej operacji.
	 */
	ArchOperation archiveOperation() const { return mArchOperation; }

private slots:
	/** Slot wywo�ywany w momencie przerywania dzialania procesu.
	 */
	void slotBreakProcess();

	/** Slot wywo�ywany w chwili zako�czenia dzia�ania uruchomionego procesu.\n
	 Zawarta jest tutaj wst�pna obs�uga b��d�w po nie udanym zako�czeniu procesu.
	 Dla polecenia 'OPEN' wys�ywane s� sygna�y: @em commandFinished() oraz
	 @em done(), po czym uruchamiane polecenie listowania.
	 */
	void slotProcessExited();

	/** Slot wywo�ywany w chwili przesy�ania danych przez proces na standardowe
	 wyj�cie.
	 */
	void slotReadFromStdout();

	/** Slot wywo�ywany, gdy proces przesy�a dane na standardowe wyj�cie b��du.
	 */
	void slotReadFromStderr();

	/** W slocie ustawiane s� argumenty dla procesu, po czym jest on uruchamiany.\n
	 W przypadku niepowodzenia wysy�any jest sygna� @em commandFinished(), a kod
	 b��du przybiera warto�� 'CannotRunProc'.
	 */
	void slotRunProcess();

	/** Nast�puje tutaj przetworzenie bufora wyj�ciowego po operacji listowania.\n
	Poszczeg�lne pola z informacj� o pliku s� ustawiane w kolejno�ci wymaganej
	przez metode wydobywaj�c� pliki z okre�lonego poziomu w hierarchi katalog�w
	w archiwum przez metod� @em VirtualArch::listFiles(). Ka�da linia dodawana
	jest do listy ci�g�w (sk�adowa klasy), przekazywanej do wcze�niej wymienionej
	metody.\n Slot jest wywolywany w spos�b nie blokuj�cy interfejsu u�ytkownika.
	\n \n Patrz te�: @see VirtualArch::listFiles().
	 */
	void slotOpen();

};

#endif
