%if %{_target_vendor} == redhat
	%define rqrd qt >= 3.3
%endif
%if %{_target_vendor} == mandriva
	%define rqrd qt3 >= 3.3
%endif

%define real_name qtcmd

%define _bindir    %{prefix}/bin
%define _datadir   %{prefix}/share
%define _docdir    %{_datadir}/doc
%define _libdir    %{_prefix}/lib
%define _plugindir %{_prefix}/lib/%{real_name}/plugins
%define _applnkdir %{_datadir}/applnk/Utils
%define _icondir   %{_datadir}/icons/locolor/32x32/apps
%define _qmdir     %{_datadir}/locale/$LANG/LC_MESSAGES
%define _appdir    %{_builddir}/%{real_name}-%{version}
%define _appdoc    %{_docdir}/$LANG/

%define f_list  file.list.%{real_name}

Summary:   Twin-panel file manager
Name:      qtcmd
Version:   0.6.4
Release:   1%{?dist}
License:   GPL
Vendor:    PioM Repository
Url:       http://www.qtcmd.nes.pl/index.php?pg=download

Packager:  Piotr Mierzwi�ski <peterm@go2.pl>
Group:     Utils

Source0:    %{real_name}-%{version}.tar.bz2

BuildRoot: %{_tmppath}/%{real_name}-%{version}-buildroot
Prefix:    /usr

Requires:  %{rqrd}
BuildRequires: libqt3 >= 3.3, libqt3-devel >= 3.3

%description
QtCommander is an old school twin-panel files manager with many extras.
Application supports full local filesystem, posses simple ftp client (no
recursive operations) and additional simple archives support (listing only).

Authors:
	Piotr Mierzwi�ski <peterm@go2.pl>

%prep
%setup -q -n %{real_name}-%{version}
CFLAGS="$RPM_OPT_FLAGS" CXXFLAGS="$RPM_OPT_FLAGS"
./configure --docdir=%{prefix}/share/doc --prefix=%{prefix} $LOCALFLAGS


mkdir -p $RPM_BUILD_ROOT%{_applnkdir}
%{__cat} <<EOF > %{_appdir}/%{real_name}.desktop
#%{__cat} <<EOF > %{buildroot}/%{real_name}.desktop
[Desktop Entry]
Name=QtCommander
Comment=Twin-panel file manager
Icon=%{real_name}.png
Exec=%{real_name}
Terminal=false
Type=Application
EOF


%build
# Setup for parallel builds
numprocs=`egrep -c ^cpu[0-9]+ /proc/stat || :`
if [ "$numprocs" = "0" ]; then
  numprocs=1
fi

#qmake -makefile -after DESTDIR=%{buildroot} qtcmd.pro
qmake -makefile -after DESTDIR="" qtcmd.pro
make -j$numprocs

# strip all binary files
#for i in `find -type f -perm +1`; do strip -s $i; done
# !! rpmbuild make strip

%install
LNG=`echo $LC_MESSAGES | cut -f1 -d'_'`

%{__install} -d -m0755 %{buildroot}%{_docdir}/%{real_name}-%{version}/
%{__install} -d -m0755 %{buildroot}%{_datadir}/locale/$LNG/LC_MESSAGES/
%{__install} -d -m0755 %{buildroot}%{_icondir}/
%{__install} -d -m0755 %{buildroot}%{_applnkdir}/

%{__install} -p -m0644 %{_appdir}/%{real_name}.desktop    $RPM_BUILD_ROOT%{_applnkdir}
%{__install} -p -m0644 %{_appdir}/icons/%{real_name}.png  $RPM_BUILD_ROOT%{_icondir}

# fix for Mandrake 10.x
if [ -f /etc/mandrake-release ]; then
    mdk=`egrep 'Mandrake' /etc/mandrake-release || :`
    if [ "$mdk" != "" ]; then
        cd %{buildroot}%{_datadir}/locale
        ln -sf $LNG $LANG
    fi
fi
# make appdoc link to current version
cd %{buildroot}%{_docdir}
ln -sf %{real_name}-%{version} %{real_name}

export INSTALL_ROOT=$RPM_BUILD_ROOT
cd %{_appdir}

%makeinstall
cd $RPM_BUILD_ROOT
find . -type d | sed '1,2d;s,^\.,\%attr(-\,root\,root) \%dir ,' > $RPM_BUILD_DIR/%{f_list}
find . -type f | sed 's,^\.,\%attr(-\,root\,root) ,' >> $RPM_BUILD_DIR/%{f_list}
find . -type l | sed 's,^\.,\%attr(-\,root\,root) ,' >> $RPM_BUILD_DIR/%{f_list}

%clean
%{__rm} -rf %{_builddir}/%{f_list}
%{__rm} -rf %{buildroot}
%{__rm} -rf %{_appdir}

# remove only %{buildroot} :(

%post
cat /etc/ld.so.conf | grep '%{prefix}/lib' > /dev/null
if [ $? -eq 1 ]; then
    echo "%{prefix}/lib/qtcmd" >> /etc/ld.so.conf
fi
/sbin/ldconfig

%files -f %{_builddir}/%{f_list}

%defattr(644,root,root,755)

#%doc AUTHORS ChangeLog COPYING README TODO
# %attr(755,root,root) %{_bindir}/*

