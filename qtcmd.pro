
CONFIG += warn_on \
	qt \
	thread \
	ordered
TEMPLATE = subdirs 
SUBDIRS += src/libs/qtcmduiext \
           src/libs/qtcmdutils \
           src/plugins/view/settings \
           src/plugins/view/video \
           src/plugins/view/text \
           src/plugins/view/sound \
           src/plugins/view/image \
           src/plugins/view/binary \
           src/plugins/fp/settings \
           doc \
	   translations \
	   src

!exists(.qmake.cache){
    error("Run configure first!")
}
